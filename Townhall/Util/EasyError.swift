//
//  EasyError.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Yamen Emon on 5/8/21.
//

import Foundation

protocol EasyErrorProtocol: LocalizedError {
    
    var title: String? { get }
    var code: Int { get }
}
struct EasyError: EasyErrorProtocol {
    
    var title: String?
    var code: Int
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }
    
    private var _description: String
    
    init(title: String?, description: String, code: Int) {
        self.title = title ?? "Error"
        self._description = description
        self.code = code
    }
}
