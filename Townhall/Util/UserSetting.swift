////
////  UserSetting.swift
////  Easy.com.bd Recharge & Bill Payment
////
////  Created by Raihan on 18/10/21.
////
//
//import Foundation
//import UIKit
//
//class UserSettings{
//    
//    static let sharedInstance = UserSettings()
//    private init(){}
//    
////    var mainNav: UINavigationController?
//   
////    var scheduleDate: String?{
////        get{
////            return UserDefaults.standard.string(forKey: Constants.UserDefaults_Schedule_key)
////        }set(scheduleDate){
////
////            guard let schedule = scheduleDate else {
////                UserDefaults.standard.removeObject(forKey: Constants.UserDefaults_Schedule_key)
////                UserDefaults.standard.synchronize()
////                return
////            }
////
////            UserDefaults.standard.set(schedule, forKey: Constants.UserDefaults_Schedule_key)
////            UserDefaults.standard.synchronize()
////        }
////    }
////    var notificationNumber: Int?{
////        get{
////            return UserDefaults.standard.integer(forKey: Constants.UserDefaults_Notification_Number)
////        }set(notificationNumber){
////            
////            guard let notification = notificationNumber else {
////                UserDefaults.standard.removeObject(forKey: Constants.UserDefaults_Notification_Number)
////                UserDefaults.standard.synchronize()
////                return
////            }
////            
////            UserDefaults.standard.set(notification, forKey: Constants.UserDefaults_Notification_Number)
////            UserDefaults.standard.synchronize()
////        }
////    }
//    
////    var apiToken: String?{
////        get{
////            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.APITOKEN.rawValue)
////        }set(apiToken){
////
////            guard let apiToken = apiToken else {
////                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.APITOKEN.rawValue)
////                UserDefaults.standard.synchronize()
////                return
////            }
////
////            UserDefaults.standard.set(apiToken, forKey: Constant.UserDefaultKeys.APITOKEN.rawValue)
////            UserDefaults.standard.synchronize()
////        }
////    }
//    
////    var deviceKey: String?{
////           get{
////               return UserDefaults.standard.string(forKey: Constants.UserDefaults_Device_key)
////           }set(deviceKey){
////
////               guard let deviceKey = deviceKey else {
////                   UserDefaults.standard.removeObject(forKey: Constants.UserDefaults_Device_key)
////                   UserDefaults.standard.synchronize()
////                   return
////               }
////
////               UserDefaults.standard.set(deviceKey, forKey: Constants.UserDefaults_Device_key)
////               UserDefaults.standard.synchronize()
////           }
////       }
//    
//    //
////    var appleUname: String?{
////           get{
////            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.Apple_User_Name.rawValue)
////           }set(appleUname){
////               
////               guard let appleUname = appleUname else {
////                   UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.Apple_User_Name.rawValue)
////                   UserDefaults.standard.synchronize()
////                   return
////               }
////               
////               UserDefaults.standard.set(appleUname, forKey: Constant.UserDefaultKeys.Apple_User_Name.rawValue)
////               UserDefaults.standard.synchronize()
////           }
////       }
////    var appleEmail: String?{
////           get{
////            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.Apple_User_Email.rawValue)
////           }set(appleEmail){
////               
////               guard let appleEmail = appleEmail else {
////                   UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.Apple_User_Email.rawValue)
////                   UserDefaults.standard.synchronize()
////                   return
////               }
////               
////               UserDefaults.standard.set(appleEmail, forKey: Constant.UserDefaultKeys.Apple_User_Email.rawValue)
////               UserDefaults.standard.synchronize()
////           }
////       }
////    var BQrPinKey: String?{
////        get{
////            return UserDefaults.standard.string(forKey: Constants.UserDefaults_BN_QR_PIN_KEY)
////        }set(qrPinKey){
////
////            guard let qrPinKey = qrPinKey else {
////                UserDefaults.standard.removeObject(forKey: Constants.UserDefaults_BN_QR_PIN_KEY)
////                UserDefaults.standard.synchronize()
////                return
////            }
////
////            UserDefaults.standard.set(qrPinKey, forKey: Constants.UserDefaults_BN_QR_PIN_KEY)
////            UserDefaults.standard.synchronize()
////        }
////    }
//    
//    //
//    
//    
////    var user: UserData?{
////        get{
////
////            let decoded  = UserDefaults.standard.data(forKey: Constants.UserDefaults_USER_DATA)
////            if let decoded = decoded{
////                return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserData
////            }else{
////                return nil
////            }
////
////        }set(userData){
////            guard let userData = userData else{
////                UserDefaults.standard.removeObject(forKey: Constants.UserDefaults_USER_DATA)
////                UserDefaults.standard.synchronize()
////                return
////            }
////            //sendFCMTokenToServer()
////            let encodedUserData = NSKeyedArchiver.archivedData(withRootObject: userData)
////            UserDefaults.standard.set(encodedUserData, forKey: Constants.UserDefaults_USER_DATA)
////            UserDefaults.standard.synchronize()
////        }
////    }
//    
//    //var fcmToken: String?
//    
////    func invalidateSession(){
////        mainNav = nil
////        fcmToken = nil
////        user = nil
////        userRecharge = nil
////        apiToken = nil
////        EasySingleton.shared.isSocialLogin = false
////    }
//    
////    func logout(){
////        WebServiceHandler.shared.logout(completion: { (result) in
////            switch result{
////                case .success( _):
////               // if response.code == 200{
////                    self.invalidateSession()
////                    (UIApplication.shared.delegate as? AppDelegate)?.loadInitialView(animationOption: .transitionFlipFromRight)
////               // }
////            case .failure(let err):
////                debugPrint(err.localizedDescription)
////            }
////
////        }, shouldShowLoader: true)
////
////    }
////    func logoutSingleDevice(){
////        self.invalidateSession()
////        (UIApplication.shared.delegate as? AppDelegate)?.loadInitialView(animationOption: .transitionFlipFromRight)
////    }
////    var userRecharge : RechargeEntity?{
////        get{
////            let decoded  = UserDefaults.standard.data(forKey: Constants.UserDefaults_USER_ReCHARGE_DATA)
////            if let decoded = decoded{
////                return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? RechargeEntity
////            }else{
////                return nil
////            }
////        }set(entity){
////            guard let entity = entity else{
////                UserDefaults.standard.removeObject(forKey: Constants.UserDefaults_USER_ReCHARGE_DATA)
////                UserDefaults.standard.synchronize()
////                return
////            }
////
////            let encodedUserData = NSKeyedArchiver.archivedData(withRootObject: entity)
////            UserDefaults.standard.set(encodedUserData, forKey: Constants.UserDefaults_USER_ReCHARGE_DATA)
////            UserDefaults.standard.synchronize()
////        }
////    }
////    var favs:[String]?{
////        get{
////            let decoded  = UserDefaults.standard.data(forKey: Constants.UserDefaults_Fav_Services)
////            if let decoded = decoded{
////                return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [String]
////            }else{
////                return ["Top Up","Movie Tickets","DTH","Utility Bill"]
////            }
////        }set(favs){
////            guard let favs = favs else{
////                UserDefaults.standard.removeObject(forKey: Constants.UserDefaults_Fav_Services)
////                UserDefaults.standard.synchronize()
////                return
////            }
////
////            let encodedUserData = NSKeyedArchiver.archivedData(withRootObject: favs)
////            UserDefaults.standard.set(encodedUserData, forKey: Constants.UserDefaults_Fav_Services)
////            UserDefaults.standard.synchronize()
////        }
////    }
//    
//    var ruid: String?
//    
//   
//}
