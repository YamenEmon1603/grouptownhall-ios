//
//  Constants.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import UIKit
struct Constant {
    static let isDev = false
    static let IS_IPHONE   = ((UIDevice.current.model == "iPhone") || (UIDevice.current.model == "iPhone Simulator"))
    static let IS_IPHONE_X  =    (IS_IPHONE && UIScreen.main.bounds.height >= 812.0)
    
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let factX = UIScreen.main.bounds.size.width/360
    static let factY = UIScreen.main.bounds.size.height/720
    static let gap_X  = 14*UIScreen.main.bounds.size.width/360
    static let gap_Y  = 5*UIScreen.main.bounds.size.height/720

    enum BaseURL{
        
        case easycore
        case easyasset
        
        var Sandbox: String {
            switch self {
            case .easycore:
                return "https://easycore.publicdemo.xyz/public/api/v1/"
            case .easyasset:
                return "https://easycore.publicdemo.xyz/public"
            }
        }
        
        var Production: String {
            switch self {
            case .easycore:
                return "https://core.easy.com.bd/api/v1/"
            case .easyasset:
                return ""
            }
        }
    }
//    static let factX = UIScreen.main.bounds.size.width/320
//    static let factY = UIScreen.main.bounds.size.height/640
    static let ratio = (UIScreen.main.bounds.width / UIScreen.main.bounds.height)/0.5
    
    enum UserDefaultKeys:String{
        case APITOKEN = "api_token_key"
        case DeviceKEY = "api_device_key"
        case ActiveLanguageIndex = "active_lang_index"
        case USER_DATA = "user_data_key"
        case USER_RECHARGE_DATA = "user_data_rechage_key"
        case Fav_Services = "favorite_services"
        case Schedule_key = "schedule"
        case Notification_Number = "notification_number"
        case Apple_User_Name = "apple_user_name"
        case Apple_User_Email = "apple_user_email"
        case BN_QR_PIN_KEY = "qr_pin_key"
        case OWN_NUMBER_KEY = "own_number_key"
        
       case GoogleMapKey = "AIzaSyBNWnzOwFKE6vK6MeINFRs8SMYCDNXLkVE"
        
    }
    enum APIMethodNames : String {
        case getDeviceKey = "get-device-key"
        case socialLogin = "social-login"
        case userStatus = "user-status"
        case userLogin = "user-login"
        case registration = "registration"
        case otpVerification = "otp-verification"
        case logOutAllDevice = "logout-from-all-devices"
        case forgotPassOTP = "forgot-password-otp"
        case forgotPassRestore = "forgot-password-restore"
        case oldUserOtpMobileRestore = "old-user-otp-for-mobile-restore"
        case logout = "logout"
        case loginDevices = "get-logged-in-devices"
        case resetPass = "password-update"
        case notification = "user-activity-notification"
        case clearNotification = "clear-user-activity-notification"
        case profileUpdate = "profile-update"
        case profilePicUpdate = "profile-picture-upload"
        case exclusiveOfferes = "easy/get-exclusive-offers"
        case dynamicReport = "report/dynamic-recent-transaction"
        case userProfile = "get-user-profile"
        
        //MARK: VR ROUTER
        case addFavouriteNumber = "vr/add-favourite-number"
        case getRecentTransaction = "vr/get-recent-transaction"
        case getFavouriteNumber = "vr/get-favorite-number"
        case operatorList = "vr/operator-list"
        case getPlans = "vr/get-plans"
        case deleteFavNumber = "vr/delete-favourite-number"
        case updateFavNumber = "vr/update-favourite-number"
        case transactionRating = "user-feedback/transaction-rating"
        case paymentStatus = "payment-status"
        case topUp = "vr/top-up"
        case holdRechargeUser = "vr/initiate-hold-recharge-user"
        case createRechargeReq = "recharge-request/create"
        case rechargeRequestList =  "recharge-request/list"
        case rechargeRequestRequested = "recharge-request/requested-list"
        case rechargeRequestInfo = "recharge-request/info"
        case rechargeRequestReject = "recharge-request/reject"
        case rechargeRequestReceiveHistory = "recharge-request/receive-history"
        
        //MARK: PRIVACY ROUTER
        case getContact = "easy/fm/get-contact"
        case getTopics = "easy/fm/get-topics"
        case getTerms =  "easy/fm/get-terms"
        case getPrivacy = "easy/fm/get-privacy"
        
        //MARK: - Utility
        case services = "dynamic/services"
        case billInfo = "dynamic/bill-info"
        case billPay = "dynamic/bill-payment"
        
        //MARK: - BanglaQR
        
        case qrPaymentCreate = "qr/payment-create"
        case qrPaymentInit = "qr/payment-init"
        case qrOtpVerify = "qr/otp-verify"
        case qrOtpSendResend = "qr/otp-send"
        case qrPinSet = "qr/set-pin"
        case deleteCard = "payment/delete-card"
        case autoDebit = "payment/auto-debit"
        case qrChangePin = "qr/pin-change"
        
        //MARK: - Townhall
        case content = "townhall/content"
        case thVersionCheck = "townhall/app-release-version"
        case townHallimageUpload = "townhall/image-upload"
        case userImageStatusCheck = "townhall/townhall-user-check"
        case townHallgameInfo = "townhall/game-qr-user"
    }
    enum socialLoginType: String{
        case facebook = "facebook"
        case google = "google"
        case apple = "apple"
    }
   
    
    
    static let DateFormat1 =                               "yyyy-MM-dd"
    static let DateFormat2  =                              "MMMM yyyy"
    static let DateFormat3  =                              "yyyy-MM-dd HH:mm:ss"
    static let DateFormat4   =                             "dd MMM"
    static let DateFormat5   =                             "dd MMM yyyy"
    static let DateFormat6   =                             "MMM yyyy"
    static let DateFormat7  =                              "MMM dd, yyyy"
    static let DateFormat8  =                              "MMMM yy"
    static let DateFormat9 =                               "yyyy/MM/dd"
    static let DateFormat10  =                              "HH:mm a dd MMM"
    static let DateFormat11  =                              "dd-MMM-yyyy"
    static let DateFormat12  =                              "EEE,dd MMM yyyy | hh:mm a"
    static let DateFormat13  =                              "dd-MMM-yyyy | hh:mm a"
    static let DateFormat14  =                              "dd-MM-yyyy | hh:mm a"
  
    static let DateFormat15  =                              "dd-MM-yyyy"
    static let DateFormat16  =                              "dd/MM/yyyy"
    static let DateFormat17  =                              "EEE,MMM d | hh:mm a"
    static let DateFormat18  =                              "dd-MMM-yyyy | hh:mm a"
    
    static let taka = "৳"
    static let HELP_AND_SUPPORT_STORYBOARD_NAME = "HelpAndSupport"
    static let HELP_RECHARGE_STORYBOARD_NAME = "HelpRecharge"
    static let HELP_INFORMATION_DETAILS_STORYBOARD_NAME = "HelpInformationDetails"
    static let HELP_RECHARGE_STORYBOARD_ID = "helprecharge"
    static let HELP_AND_SUPPORT_STORYBOARD_ID = "helpandsupport"
    static let HELP_INFORMATION_DETAILS_STORYBOARD_ID = "information_details"
    
    
    
}


