//
//  EasyDataStore.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import UIKit
class EasyDataStore {
    private init(){
        
    }
    static let sharedInstance = EasyDataStore()
    var fcmToken: String?
    var seletedNumbersArray:[SelectNumber] =  []
    var mainNav: UINavigationController?
    var easyMobileOperators:[MBOperator] = []
    var dashboardContent: DashboardContentResponse?
    var easyRechargePlans: [RechargePlan] = []
//    {
//        didSet{
//            for (index,op) in easyMobileOperators.enumerated(){
//                EasyManager.sharedInstance.downloadImage(with: op.imageUrl ?? "") { image in
//                    self.easyMobileOperators[index].logoImage = image
//                }
//            }
//            
//        }
//    }
    var  favoriteData: [FevoriteContact]?
    var  recentData: [RecentTransaction]?
    var  mapDispImages = ["disp1","disp2","disp3","disp4"]
    var isSocialLogin = false
    var ruid: String?
    
    var ownNumber: SelectNumber?{
        get{
            
            let decoded  = UserDefaults.standard.data(forKey: Constant.UserDefaultKeys.OWN_NUMBER_KEY.rawValue)
            if let decoded = decoded{
                return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? SelectNumber
            }else{
                return nil
            }
            
        }set(setOwnNumber){
            guard let myNumber = setOwnNumber else{
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.OWN_NUMBER_KEY.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            //sendFCMTokenToServer()
            let encodedOwnData = NSKeyedArchiver.archivedData(withRootObject: myNumber)
            UserDefaults.standard.set(encodedOwnData, forKey: Constant.UserDefaultKeys.OWN_NUMBER_KEY.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    var socialTapStatus : Bool?{
        get{
            return UserDefaults.standard.bool(forKey: "SocialTapped")
        }set(socialTapStatus){
            
            guard let status = socialTapStatus else {
                UserDefaults.standard.removeObject(forKey: "SocialTapped")
                UserDefaults.standard.synchronize()
                return
            }
            
            UserDefaults.standard.set(status, forKey: "SocialTapped")
            UserDefaults.standard.synchronize()
        }
    }
    var apiToken: String?{
        get{
            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.APITOKEN.rawValue)
        }set(apiToken){
            
            guard let apiToken = apiToken else {
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.APITOKEN.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            
            UserDefaults.standard.set(apiToken, forKey: Constant.UserDefaultKeys.APITOKEN.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    var deviceKey: String?{
        get{
            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.DeviceKEY.rawValue)
        }set(deviceKey){
            
            guard let deviceKey = deviceKey else {
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.DeviceKEY.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            
            UserDefaults.standard.set(deviceKey, forKey: Constant.UserDefaultKeys.DeviceKEY.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    var BQrPinKey: String?{
        get{
            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.BN_QR_PIN_KEY.rawValue)
        }set(qrPinKey){
            
            guard let qrPinKey = qrPinKey else {
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.BN_QR_PIN_KEY.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            
            UserDefaults.standard.set(qrPinKey, forKey:  Constant.UserDefaultKeys.BN_QR_PIN_KEY.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    //
    var user: UserData?{
        get{
            
            guard let decoded  = UserDefaults.standard.data(forKey: Constant.UserDefaultKeys.USER_DATA.rawValue) else{
                return nil
            }
            return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserData
            
        }set(userData){
            guard let userData = userData else{
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.USER_DATA.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            //sendFCMTokenToServer()
            let encodedUserData = NSKeyedArchiver.archivedData(withRootObject: userData)
            UserDefaults.standard.set(encodedUserData, forKey: Constant.UserDefaultKeys.USER_DATA.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    var appleUname: String?{
        get{
            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.Apple_User_Name.rawValue)
        }set(appleUname){
            
            guard let appleUname = appleUname else {
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.Apple_User_Name.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            
            UserDefaults.standard.set(appleUname, forKey: Constant.UserDefaultKeys.Apple_User_Name.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    var appleEmail: String?{
        get{
            return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.Apple_User_Email.rawValue)
        }set(appleEmail){
            
            guard let appleEmail = appleEmail else {
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.Apple_User_Email.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            
            UserDefaults.standard.set(appleEmail, forKey: Constant.UserDefaultKeys.Apple_User_Email.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    var notificationNumber: Int?{
        get{
            return UserDefaults.standard.integer(forKey: Constant.UserDefaultKeys.Notification_Number.rawValue)
        }set(notificationNumber){
            
            guard let notification = notificationNumber else {
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKeys.Notification_Number.rawValue)
                UserDefaults.standard.synchronize()
                return
            }
            
            UserDefaults.standard.set(notification, forKey: Constant.UserDefaultKeys.Notification_Number.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
}
class SelectNumber : NSObject, Codable, NSCoding  {
    
    var number : String?
    var operatorObj: MBOperator?
    var  connetctionType: String?
    var  amount: Int?
    var scheduleTime : Date?
    var name : String?
    var isfav : Bool?
    enum CodingKeys: String, CodingKey {
        case number = "number"
        case operatorObj = "operatorObj"
        case connetctionType = "connetctionType"
        case amount = "amount"
        case scheduleTime = "scheduleTime"
        case name =  "name"
        case isfav = "isfav"
        
    }
    
    init(number: String?,operatorObj:MBOperator?,connetctionType:ConectionType?,amount :Int?,scheduleTime : Date?,name: String? = "",isfav:Bool? = false) { // Constructor
        self.number = number
        self.operatorObj = operatorObj
        self.connetctionType = connetctionType?.value
        self.amount = amount
        self.scheduleTime = scheduleTime
        self.name = name
        self.isfav = isfav
    }
    func encode(with coder: NSCoder) {
        coder.encode(number, forKey: "number")
        coder.encode(operatorObj, forKey: "operatorObj")
        coder.encode(connetctionType, forKey: "connetctionType")
        coder.encode(amount, forKey: "amount")
        coder.encode(scheduleTime, forKey: "scheduleTime")
        coder.encode(name, forKey: "name")
        coder.encode(isfav, forKey: "isfav")
    }
    
    required convenience init?(coder: NSCoder) {
        let number = coder.decodeObject(forKey: "number") as? String
        let name = coder.decodeObject(forKey: "name") as? String
        let operatorObj  = coder.decodeObject(forKey: "operatorObj") as? MBOperator
        let connetctionType =  coder.decodeObject(forKey: "connetctionType") as? String ?? "prepaid" //ConectionType(coder.decodeObject(forKey: "connetctionType") as? String ?? "prepaid")
        let amount = coder.decodeObject(forKey: "amount") as? Int
        let scheduleTime = coder.decodeObject(forKey: "scheduleTime") as? Date
        let isfav = coder.decodeObject(forKey: "isfav") as? Bool
        self.init(number: number ,operatorObj:operatorObj,connetctionType:ConectionType(connetctionType),amount :amount,scheduleTime : scheduleTime,name:name,isfav:isfav)
    }
}


class ProgramTimer{
    var startDate : Date?
    var endDate : Date?
    init(start : Date?, end : Date?  ) {
        startDate = start
        endDate = end
        
    }
}
