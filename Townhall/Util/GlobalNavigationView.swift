//
//  GlobalNavigationView.swift
//  Townhall
//
//  Created by Raihan on 12/15/21.
//

import Foundation
import UIKit

// MARK: - NavigationBar Delegate
public protocol GlobalNavigationBarBtnActionDelegate: AnyObject {
    func backBtnPressed()
}

class GlobalNavigationView: UIView {
    
    var navDelegate : GlobalNavigationBarBtnActionDelegate!
    
    lazy var navBackBtnImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    lazy var navBackBtnTitle: UILabel = {
        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.textColor = .black
        title.font = .EasyFont(ofSize: 13, style: .bold)
        return title
    }()
    
    lazy var backBtn : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var separatorView : UIView  = {
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
   
    override init(frame: CGRect) {
        super.init(frame:frame)
    }
    
    convenience  init(title:String,image:UIImage) {
        self.init(frame:.zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        setupView(title:title,image:image)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView(title:String,image:UIImage) {
        
        self.addSubview(navBackBtnImageView)
        navBackBtnImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        navBackBtnImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.25).isActive = true
        navBackBtnImageView.widthAnchor.constraint(equalTo: navBackBtnImageView.heightAnchor, multiplier: 1).isActive = true
        navBackBtnImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        navBackBtnImageView.image = image
        
        self.addSubview(navBackBtnTitle)
        navBackBtnTitle.leadingAnchor.constraint(equalTo: navBackBtnImageView.trailingAnchor, constant: 10).isActive = true
        navBackBtnTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        navBackBtnTitle.bottomAnchor.constraint(equalTo:self.bottomAnchor, constant: 0).isActive = true
        navBackBtnTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -50).isActive = true
        navBackBtnTitle.text = title
        
        self.addSubview(backBtn)
        backBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        backBtn.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        backBtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        backBtn.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5).isActive = true
        backBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        
        let _ = self.addBorder(side: .bottom, color: .lightPeriwinkle, width: 0.5)
    }

    @objc func backBtnPressed(){
        navDelegate.backBtnPressed()
    }
    
}
