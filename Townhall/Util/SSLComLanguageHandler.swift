//
//  SSLComLanguageHandler.swift
//  ssl_commerz_revamp
//
//  Created by Mausum Nandy on 5/19/21.
//

import Foundation
// MARK: Language Handler
protocol SSLComLanguageDelegate {
    func languageDidChange(to language: SDKLanguage)
}
class SSLComLanguageHandler {
    
    static let sharedInstance = SSLComLanguageHandler()
    private init(){}
    
  
    var delegate: SSLComLanguageDelegate?
    
    
    /// To get currently active SDKLanguage
    /// - Returns: Currently active SDKLanguage
    func getCurrentLanguage() -> SDKLanguage{
        if(UserDefaults.standard.dictionaryRepresentation().keys.contains(Constant.UserDefaultKeys.ActiveLanguageIndex.rawValue)){
            return SDKLanguage(index: UserDefaults.standard.integer(forKey: Constant.UserDefaultKeys.ActiveLanguageIndex.rawValue))
        }else{
            return .English
        }
    }
    
    
    var languageShortName :String {
        get{
          return self.getCurrentLanguage().identifier
        }
    }
    
    /// To switch current Active SDKLanguge
    /// - Parameter language: Pass the SDKLanguage Case you wish to Switch To
    func switchLanguage(to language: SDKLanguage){
        UserDefaults.standard.set(language.index, forKey: Constant.UserDefaultKeys.ActiveLanguageIndex.rawValue)
        UserDefaults.standard.synchronize()
        if let delegate = self.delegate{
            delegate.languageDidChange(to: language)
        }
      
        
    }
  
    
    /// getLocalizedDigits
    /// - Parameter numbers: any number String e.g: "500","10.02","0.01"
    /// - Returns: Localised Number String 
    func getLocalizedDigits(_ numbers: String?) -> String? {
          var numbers = numbers
          let formatter = NumberFormatter()
          formatter.minimumIntegerDigits = 1
          formatter.minimumFractionDigits = 0
          formatter.maximumFractionDigits = 2
          formatter.locale = NSLocale(localeIdentifier: getCurrentLanguage().identifier) as Locale
          
          for i in 0..<10 {
              let num = NSNumber(value: i)
              numbers = numbers?.replacingOccurrences(of: num.stringValue, with: formatter.string(from: num) ?? "")
          }
          numbers = ((numbers?.count ?? 0) > 0) ? numbers : "0"
         
          return numbers
      }
    
    
    
    
}
public enum SDKLanguage{
    
    case English
    case Bangla
    
    init(index: Int){
        switch index {
        case 0:
            self = .English
        case 1:
            self = .Bangla
        default:
            self = .English
        }
    }
    
    var index: Int{
        switch self {
        case .English:
            return 0
        case .Bangla:
            return 1
        }
    }
    
    var identifier: String{
        switch self {
        case .English:
            return "en"
        case .Bangla:
            return "bn"
        }
    }
    var file:String?{
        switch self {
        case .English:
            return Bundle.main.path(forResource: "English", ofType: "strings")
        case .Bangla:
            return Bundle.main.path(forResource: "Bangla", ofType: "strings")
        }
    }
   
}
extension String{
    func easyLocalized() -> String {
        guard let path = SSLComLanguageHandler.sharedInstance.getCurrentLanguage().file else {
            return self.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        let localisationDict = NSDictionary(contentsOfFile: path)
        if let localisedString =  localisationDict?[self.trimmingCharacters(in: .whitespacesAndNewlines)] as? String{
            return localisedString
        }
        return self

    }
}
