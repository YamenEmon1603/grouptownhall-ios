//
//  EasyUtils.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import UIKit
class EasyUtils {
    private init(){
        
    }
    static let shared = EasyUtils()
   
    
  
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
        
    }
    
    
   
   
    @objc func date(toTimestamp dateStr: String?, dateFormate: String?, expectedDateFormate: String) -> String {
        let dateFormatter = DateFormatter()
        // This is important - we set our input date format to match our input string
        // if the format doesn't match you'll get nil from your string, so be careful
        dateFormatter.dateFormat = dateFormate//"yyyy-MM-dd"
        
        //`date(from:)` returns an optional so make sure you unwrap when using.
        let dateFromString: Date? = dateFormatter.date(from: dateStr ?? "")
        
        let formatter = DateFormatter()
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        //        if EasyUtils.getLanguageIndex() == 1 {
        //                formatter.locale = Locale(identifier: "en")
        //            }
        formatter.dateFormat = expectedDateFormate//"MMM dd, yyyy"
        guard dateFromString != nil else { return ""}
        
        //Using the dateFromString variable from before.
        let stringDate: String = formatter.string(from: dateFromString!)
        return stringDate
    }
    @objc func forDate(toTimestamp dateStr: String?, dateFormate: String?, expectedDateFormate: String) -> String {
        let dateFormatter = DateFormatter()
        // This is important - we set our input date format to match our input string
        // if the format doesn't match you'll get nil from your string, so be careful
        dateFormatter.dateFormat = dateFormate//"yyyy-MM-dd"
        
        //`date(from:)` returns an optional so make sure you unwrap when using.
        let dateFromString: Date? = dateFormatter.date(from: dateStr ?? "")
        
        let formatter = DateFormatter()
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        if SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .Bangla {
            formatter.locale = Locale(identifier: "bn")
        }
        formatter.dateFormat = expectedDateFormate//"MMM dd, yyyy"
        guard dateFromString != nil else { return ""}
        
        //Using the dateFromString variable from before.
        let stringDate: String = formatter.string(from: dateFromString!)
        return stringDate
    }
    func getLocalizedDigits(_ numbers: String?) -> String? {
        var numbers = numbers
        //        if ShouldLocalizeDigit == nil {
        //            return numbers
        //        }
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        if SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .Bangla{
            formatter.locale = NSLocale(localeIdentifier: "bn") as Locale
        }
        for i in 0..<10 {
            let num = NSNumber(value: i)
            numbers = numbers?.replacingOccurrences(of: num.stringValue, with: formatter.string(from: num) ?? "")
        }
        numbers = ((numbers?.count ?? 0) > 0) ? numbers : "0"
        return numbers
    }
    func getBlurView(view: UIView)  -> UIVisualEffectView{
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectV = UIVisualEffectView(effect: blurEffect)
        blurEffectV.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        blurEffectV.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectV.alpha = 0.5
        return blurEffectV
    }
    func logoutSingleDevice(message:String = ""){
        self.invalidateSession()
        if #available(iOS 13.0, *) {
            let sceneDelegate = UIApplication.shared.connectedScenes
                    .first!.delegate as! SceneDelegate
            sceneDelegate.loadLandingViewController(message: message)
        } else {
            (UIApplication.shared.delegate as? AppDelegate)?.loadLandingViewController(message: message)
        }
        //
       // EasyManager.sharedInstance.mainNav?.viewControllers.append(LandingPageViewController())
    }
   
    func invalidateSession(){
        EasyDataStore.sharedInstance.mainNav = nil
        EasyDataStore.sharedInstance.fcmToken = nil
        EasyDataStore.sharedInstance.user = nil
//       userRecharge = nil
        EasyDataStore.sharedInstance.apiToken = nil
        EasyDataStore.sharedInstance.isSocialLogin = false
    }
    func logout(){
        WebServiceHandler.shared.logout(completion: { (result) in
            switch result{
                case .success( _):
               // if response.code == 200{
                   
                    self.logoutSingleDevice()
               // }
            case .failure(let err):
                debugPrint(err.localizedDescription)
            }
            
        }, shouldShowLoader: true)

    }
    func spaced(text:String,kernValue: Double = 1.0)->NSMutableAttributedString{
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
              return attributedString
    }
}
