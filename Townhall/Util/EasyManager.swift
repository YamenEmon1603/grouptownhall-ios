//
//  EasyManager.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import UIKit
import Kingfisher
class EasyManager{
    //Base Design for Iphone 8 Plus
    static let factX = UIScreen.main.bounds.size.width/375
    
    static let factY = UIScreen.main.bounds.size.height/736
    
    //
    private init(){
        
    }
    static let sharedInstance = EasyManager()
    static let dataStore = EasyDataStore.sharedInstance
    var mainNav:UINavigationController?
    var isFromRechargeRequest = false
    var appVersion:String{
        get{
            return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String  ?? ""
        }
    }
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
        
    }
     func timeInterval(timeAgo:String, dateFormat:String ) -> String
    {
        let df = DateFormatter()
        
        df.dateFormat = dateFormat
        let dateWithTime = df.date(from: timeAgo)
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateWithTime!, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" : "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" : "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" : "\(day)" + " " + "days ago"
        }else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" : "\(hour)" + " " + "hours ago"
        }else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" : "\(minute)" + " " + "minutes ago"
        }else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second ago" : "\(second)" + " " + "seconds ago"
        } else {
            return "a moment ago"
            
        }
    }
    
    func getOprators(completion:@escaping (Result<[MBOperator],Error>)-> Void){
       /* WebServiceHandler.shared.getOprators(completion: { (result) in
            switch result{
                case .success(let response):
                    if response.code == 200{
                        if let data = response.data{
                            EasyManager.dataStore.easyMobileOperators = data
                          
                            completion(.success(data))
                        }
                        else{
                            completion(.failure(EasyError.init(title: "Error", description: "Can not find any operator list", code: response.code ?? 0)))
                        }
                    }
                    else {
                        completion(.failure(EasyError.init(title: "Error", description: response.message ?? "", code: response.code ?? 0)))
                    }
                    break
                case .failure(let error):
                    debugPrint(error)
                    completion(.failure(error))
                    break
            }
        }, shouldShowLoader: false)*/
    }
//    func downloadImage(`with` urlString : String, complition: @escaping(_ image:UIImage?)->Void?){
//        guard let url = URL.init(string: urlString) else {
//            return
//        }
//        let resource = ImageResource(downloadURL: url)
//
//        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
//            switch result {
//            case .success(let value):
//                print("Image: \(value.image). Got from: \(value.cacheType)")
//                complition(value.image)
//            case .failure(let error):
//                print("Error: \(error)")
//            }
//        }
//    }
    
    
    func prepareMostPopular(op : MBOperator? = nil,conectionType:String = "", amount: Int = 0,max:Int? = nil,isPolular:Bool?=nil,offerType:OfferType?=nil, filteredData:(_ nextPlan: RechargePlan? ,_ plans:[RechargePlan]?)->Void){
        var nextPlan: RechargePlan?
        var filtered = EasyDataStore.sharedInstance.easyRechargePlans
        if let op = op{
            filtered = filtered.filter({$0.operatorShortName == op.operatorShortName})
        }
        if conectionType.lowercased() == "prepaid"{
            filtered = filtered.filter({$0.isForPrepaid == 1})
        }
        if conectionType.lowercased() == "postpaid"{
            filtered = filtered.filter({$0.isForPostpaid == 1})
        }
        if offerType != nil{
            filtered = filtered.filter({$0.offerType == offerType?.rawValue})
           
        }
        if amount > 0{
            filtered = filtered.filter({(item) in
                if let maxAmount =  item.amountMax{
                    return maxAmount > amount
                }
                return false

            })
        }
        
        
        if isPolular == true{
            if let max = max {
                var five = filtered.filter({$0.isPopular == 1})
                if five.count < max {
                    let sorted = filtered.sorted(by: {$0.usageCnt! > $1.usageCnt!}).filter({$0.isPopular == 0})
                    five.append(contentsOf: sorted.prefix(max - five.count))
                }
                if amount > 9 {
                    nextPlan = five.first{$0.amountMax! > amount}
                }
                filteredData( nextPlan,five)
                return
            }else{
                filteredData( nextPlan,filtered)
                return
            }
        }
       
    
        filteredData (nextPlan ,filtered)
      
    }
}
