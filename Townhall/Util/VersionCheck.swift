//
//  VersionCheck.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 22/11/21.
//

import Foundation
import Foundation
import Alamofire

class VersionCheck{
    public static let shared = VersionCheck()
    

    
    static func getCurrentAppVersion() -> String{
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
            return appVersion
        }
        return "0.0"
    }
}
