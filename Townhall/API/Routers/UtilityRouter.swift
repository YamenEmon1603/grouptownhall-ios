//
//  UtilityRouter.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 16/11/21.
//

import Foundation
enum UtilityRouter{
    
      case serviceList
      case billInfo(params:[String:Any])
      case billPayment(params:[String:Any])
      
      var requestBuilder : RequestBuilder
      {
          switch self {
          case .serviceList:
              return .init(baseUrl: .easycore, path: .services, method: .post, params: [:], additionalHeaders: nil)
          case .billInfo(params: let params):
              return .init(baseUrl: .easycore, path: .billInfo, method: .post, params: params, additionalHeaders: nil)
          case .billPayment(params: let params):
              return .init(baseUrl: .easycore, path: .billPay, method: .post, params: params, additionalHeaders: nil)
          }
      }

}
