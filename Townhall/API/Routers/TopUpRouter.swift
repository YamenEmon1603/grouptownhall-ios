//
//  UserInfoRequestModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Yamen Emon on 4/8/21.
//

import Foundation
enum TopUpRouter {
  /*  case addFavoriteNumber(params : UpdateFavRequestModel)
    case getRecentRechargeTransaction
    case getFavoriteNumber
    case getOprators
    case getRechargePlans
    case topUp
    case deletefav(params:[String:Any])
    case updatefav(params : UpdateFavRequestModel)
    case ratingTransaction(rating: String, transactionId: String)
    case transactionStatus(transactionId:String)
    case initiateHoldRechargeUser(transactionId:String, allowOffer:Int)
    case getRechargeReq(info : RechargeRequestModel)
    
    
    case getRechargeRequestList(page:Int)
    case getRechargeRequestSent(page:Int)
    case rechargeRequestDetails(ruid:String)
    case rechargeRequestReject(requestId:String)
    case receivedHistory(page:Int)
    case requestTopUP(reqModel:RechargeRequestPayData,amount:String,ruid:String,isRechargeRequest: Bool)
    
    var requestBuilder: RequestBuilder{
        switch self {
        case .addFavoriteNumber(let params):
            return .init(baseUrl: .easycore, path: .addFavouriteNumber, method: .post, params: params.dictionary, additionalHeaders: nil)
        case .getRecentRechargeTransaction:
            return .init(baseUrl: .easycore, path: .getRecentTransaction, method: .post, params: [:], additionalHeaders: nil)
        case .getFavoriteNumber :
            return .init(baseUrl: .easycore, path: .getFavouriteNumber, method: .post, params: [:], additionalHeaders: nil)
        case .getOprators :
            return .init(baseUrl: .easycore, path: .operatorList, method: .post, params: [:], additionalHeaders: nil)
        case .getRechargePlans :
            return .init(baseUrl: .easycore, path: .getPlans, method: .post, params: [:], additionalHeaders: nil)
        case .deletefav(params: let params):
            return .init(baseUrl: .easycore, path: .deleteFavNumber, method: .post, params: params, additionalHeaders: nil)
        case .updatefav(params: let params):
            return .init(baseUrl: .easycore, path: .updateFavNumber, method: .post, params: params.dictionary, additionalHeaders: nil)
        case .ratingTransaction(rating: let rating, transactionId: let transaction) :
            return .init(baseUrl: .easycore, path: .transactionRating, method: .post, params: ["transaction_id":transaction,"rating":rating], additionalHeaders: nil)
        case .transactionStatus( transactionId: let transaction) :
            return .init(baseUrl: .easycore, path: .paymentStatus, method: .post, params: ["tran_id":transaction], additionalHeaders: nil)
        case .topUp:
            // Setting up connection type  and  operator for user account while first time recharging self number
//            if UserSettings.sharedInstance.userRecharge  == nil {
//                if let selfNumber = UserSettings.sharedInstance.selectedNumbers.first(where: {$0.phone == UserSettings.sharedInstance.user?.mobile}){
//                    UserSettings.sharedInstance.userRecharge = selfNumber
//                }
//            }
           
            var param : [String:Any] = [:]
           var shRecharge = ""
            let f = DateFormatter()
            for (index,entity) in  EasyDataStore.sharedInstance.seletedNumbersArray.enumerated() {
                param["data[\(index)][phone]"] = entity.number?.replacingOccurrences(of: " ", with: "")
                param["data[\(index)][amount]"] = entity.amount ?? 0
                param["data[\(index)][operator]"] = entity.operatorObj?.operatorId ?? 0
                param["data[\(index)][type]"] = entity.connetctionType ?? ""
               // shRecharge = entity.scheduleTime
                shRecharge = EasyUtils.shared.forDate(toTimestamp: f.string(from: entity.scheduleTime ?? Date()), dateFormate: "dd-MM-yyyy | hh:mm a", expectedDateFormate: "dd MMM yyyy hh:mm a")
            }
            if shRecharge != ""{
                param["schedule_recharge"] = shRecharge
            }
            param["is_mobile"] = true
            
            return .init(baseUrl: .easycore, path: .topUp, method: .post, params: param, additionalHeaders: nil)
        
        case .initiateHoldRechargeUser(transactionId: let transactionId, allowOffer: let allowOffer):
            return .init(baseUrl: .easycore, path: .holdRechargeUser, method: .post, params: ["transaction_id":transactionId,"allow_offer":allowOffer], additionalHeaders: nil)
            
            case .getRechargeReq(info : let model):
                return .init(baseUrl: .easycore, path: .createRechargeReq, method: .post, params: model.dictionary, additionalHeaders: nil)
                
  
            
        case .getRechargeRequestList(page : let page):
            return .init(baseUrl: .easycore, path: .rechargeRequestList, method: .post, params: ["is_mobile":1, "token" :  EasyDataStore.sharedInstance.apiToken ?? "","device_key": EasyDataStore.sharedInstance.deviceKey ?? "","page":page], additionalHeaders: nil)
            
        case .getRechargeRequestSent(page : let page):
            return .init(baseUrl: .easycore, path: .rechargeRequestRequested, method: .post, params: ["is_mobile":1, "token" : EasyDataStore.sharedInstance.apiToken ?? "","page":page], additionalHeaders: nil)
           
        case .rechargeRequestDetails(ruid: let ruid):
            return .init(baseUrl: .easycore, path: .rechargeRequestInfo, method: .post, params: ["ruid":ruid,"is_mobile":1, "token" : EasyDataStore.sharedInstance.apiToken ?? ""], additionalHeaders: nil)
      
        case .rechargeRequestReject(requestId: let requestId):
            return .init(baseUrl: .easycore, path: .rechargeRequestReject, method: .post, params: ["token" :EasyDataStore.sharedInstance.apiToken ?? "","request_id": requestId], additionalHeaders: nil)
            
        case .receivedHistory(page : let page):
            return .init(baseUrl: .easycore, path: .rechargeRequestReceiveHistory, method: .post, params: ["is_mobile":1, "token" : EasyDataStore.sharedInstance.apiToken ?? "","page":page], additionalHeaders: nil)
            
        case .requestTopUP(reqModel: let reqModel, amount:let amount,ruid:let ruid ,isRechargeRequest: let isRechargeRequest):
        
            var param : [String:Any] = [:]
           
                let index = 0
            
            param["data[\(index)][phone]"] = reqModel.requestBody?.msisdn ?? ""
            param["data[\(index)][amount]"] = amount
                param["data[\(index)][operator]"] = reqModel.requestBody?.operatorID ?? 0

            param["data[\(index)][type]"] = reqModel.requestBody?.connectionType
          
            param["is_mobile"] = true
            param["ruid"] = ruid
            param["is_recharge_request"] = isRechargeRequest
           //param["ruid"] = reqModel.ruid

            return .init(baseUrl: .easycore, path: .topUp, method: .post, params: param, additionalHeaders: nil)
        
        }
    }
   */
}

