//
//  EasyBanglaQRRouter.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 25/11/21.
//

import Foundation

enum EasyBanglaQRRouter {
    case qrPaymentCreate(transactionAmount:String,transactionFeeAmount: String,payload: String,merchantName: String,merchantCity: String,merchantMcc: String,tag62First:String,tag62Second: String, merchantId:String,pin: String)
    case qrPaymentInit(transactionId:String)
    case qrOtpVerify(otp:String)
    case qrOtpSendResend
    case qrPinSet(banglaQrPinKey: String,pin : String,confirmPin: String)
    case deleteCard(cardIndex:String, userRefer: String)
    case autoDebit(params:AutoDebitRequestModel)
    case qrChangePin(oldQrPin: String,qrPin: String, confirmQrPin: String)
    var requestBuilder:RequestBuilder{
        switch self {
        case .qrPaymentCreate(let transactionAmount,let transactionFeeAmount,let payload,let merchantName,let merchantCity,let merchantMcc,let tag62First,let tag62Second, let merchantId, pin: let pin):
            return .init(baseUrl: .easycore, path: .qrPaymentCreate, method: .post, params: ["transaction_amount":transactionAmount,"transaction_fee_amount":transactionFeeAmount,"payload":payload,"merchant_name":merchantName,"merchant_city":merchantCity,"merchant_mcc":merchantMcc,"tag_62_first_data":tag62First,"tag_62_second_data":tag62Second, "merchant_id":merchantId,"is_mobile":true,"qr_pin": pin,"pay_with_save_card": 1], additionalHeaders: nil)
        case .qrPaymentInit(let transactionId):
            return .init(baseUrl: .easycore, path: .qrPaymentInit, method: .post, params: ["transaction_id": transactionId,"is_mobile":true], additionalHeaders: nil)
        case .qrOtpVerify(otp:let otp):
            return .init(baseUrl: .easycore, path: .qrOtpVerify, method: .post, params: ["otp":otp], additionalHeaders: nil)
        case  .qrOtpSendResend:
            return .init(baseUrl: .easycore, path: .qrOtpSendResend, method: .post, params: [:], additionalHeaders: nil)
            
        case .qrPinSet(let banglaQrPinKey,let pin,let confirmPin):
            return .init(baseUrl: .easycore, path: .qrPinSet, method: .post, params: ["bangla_qr_pin_key":banglaQrPinKey,"qr_pin":pin,"confirm_qr_pin":confirmPin], additionalHeaders: nil)
        case .deleteCard(cardIndex: let cardIndex, userRefer: let userRefer):
            return .init(baseUrl: .easycore, path: .deleteCard, method: .post, params: ["user_refer": userRefer,"card_index":cardIndex,"is_mobile":1], additionalHeaders: nil)
        case .autoDebit(params: let params):
            return .init(baseUrl: .easycore, path: .autoDebit, method: .post, params: params.dictionary, additionalHeaders: nil)
        case .qrChangePin(let oldQrPin,let qrPin, let confirmQrPin):
            return .init(baseUrl: .easycore, path: .qrChangePin, method: .post, params: ["old_qr_pin":oldQrPin,"qr_pin":qrPin ,"confirm_qr_pin":confirmQrPin], additionalHeaders: nil)
        }
    }
}
