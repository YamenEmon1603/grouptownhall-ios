//
//  RequestBuilder.swift
//  Easy-Merchant
//
//  Created by Mausum Nandy on 4/11/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation
import Alamofire

struct RequestBuilder : URLRequestConvertible {
    let baseUrl: Constant.BaseURL
    let path: Constant.APIMethodNames
    let method: HTTPMethod
    let params: [String: Any]?
    let additionalHeaders: HTTPHeaders?
    
    init (baseUrl: Constant.BaseURL, path: Constant.APIMethodNames, method: HTTPMethod, params: [String: Any]?, additionalHeaders: HTTPHeaders?) {
        self.path = path
        self.method = method
        self.params = params
        self.additionalHeaders = additionalHeaders
        self.baseUrl = baseUrl
    }
    
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try (Constant.isDev) ? self.baseUrl.Sandbox.asURL() : self.baseUrl.Production.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path.rawValue))
        urlRequest.httpMethod = self.method.rawValue
        urlRequest.headers = getHeaders()
        urlRequest = try URLEncoding.default.encode(urlRequest, with: self.params)
        urlRequest.timeoutInterval = 100
        return urlRequest
    }
    private func getHeaders() -> HTTPHeaders {
        
        var headers: HTTPHeaders = [
            HTTPHeaderField.acceptType.rawValue : ContentType.json.rawValue,
            HTTPHeaderField.isTownHallUser.rawValue : "1",
            HTTPHeaderField.userAgent.rawValue: "iOS-Application",
            HTTPHeaderField.deviceModel.rawValue:UIDevice.modelName,
            HTTPHeaderField.deviceOS.rawValue:"iOS " + UIDevice.current.systemVersion,
            HTTPHeaderField.lang.rawValue:SSLComLanguageHandler.sharedInstance.languageShortName
        ]
        headers.add(name: HTTPHeaderField.appVersion.rawValue, value: EasyManager.sharedInstance.appVersion)
        
        
        if let apiToken = EasyManager.dataStore.apiToken{
            headers.add(name: HTTPHeaderField.token.rawValue, value: apiToken)
        }
        if let deviceID = EasyManager.dataStore.deviceKey{
            headers.add(name: HTTPHeaderField.deviceKey.rawValue, value: deviceID)
            
        }else{
            headers.add(name: HTTPHeaderField.deviceKey.rawValue, value: UIDevice.current.identifierForVendor?.uuidString ?? "")
        }
        
        
        if let additionalHeaders = additionalHeaders{
            for header in additionalHeaders{
                headers.add(header)
            }
        }
        debugPrint("headers", headers)
        return headers
    }
    
    
    enum HTTPHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
        case deviceKey = "device-key"
        case token = "token"
        case lang = "lang"
        case userAgent = "User-Agent"
        case deviceModel = "Device-Model"
        case deviceOS = "Device-OS"
        case appVersion = "App-Version"
        case isTownHallUser =  "is-town-hall"
    }
    
    enum ContentType: String {
        case json = "application/json"
        case formData = "multipart/form-data"
        case urlEncoded = "application/x-www-form-urlencoded"
    }
}





