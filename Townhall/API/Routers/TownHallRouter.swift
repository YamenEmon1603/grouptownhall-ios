//
//  TownHall.swift
//  Townhall
//
//  Created by Mausum Nandy on 9/12/21.
//

import Foundation
enum TownHallRouter{
    
    case content(id: Int)
    case versionCheck
    case gameQrInfo(model:PlayQrRequestModel)
    case imageUpload(phone:String)
    case imageStatusCheck(phone:String)
    var requestBuilder : RequestBuilder
    {
        switch self {
        case .content(id: let id):
            return .init(baseUrl: .easycore, path: .content, method: .get, params: ["content_id": id], additionalHeaders: nil)
        case .gameQrInfo(model: let model):
            return .init(baseUrl: .easycore, path: .townHallgameInfo, method: .post, params:model.dictionary, additionalHeaders: nil)
        case .versionCheck :
            return .init(baseUrl: .easycore, path: .thVersionCheck, method: .post, params: UserDeviceRequestModel().dictionary, additionalHeaders: nil)
        case .imageUpload(phone:let phone):
            return .init(baseUrl: .easycore, path: .townHallimageUpload, method: .post, params: ["mobile_no":phone], additionalHeaders: nil)
        case .imageStatusCheck(phone: let phone):
            return .init(baseUrl: .easycore, path: .userImageStatusCheck, method: .get, params:  ["mobile":phone], additionalHeaders: nil)
        }
    }
    
}
