//
//  UserInfoRequestModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Yamen Emon on 4/8/21.
//

import Foundation
/*enum TopUpRouter {
    case postFavoriteNumber(params : [String : Any])
    case getRecentRechargeTransaction
    case getFavoriteNumber
    case getOprators
    case getRechargePlans
    case topUP
    case deletefav(params:[String:Any])
    case updatefav(params : [String : Any])
    case ratingTransaction(rating: String, transactionId: String)
    case transactionStatus(transactionId:String)
    case initiateHoldRechargeUser(transactionId:String, allowOffer:Int)
    case getRechargeReq(info : RechargeRequestModel)
    
    var requestBuilder: RequestBuilder{
        switch self {
        case .postFavoriteNumber(let params):
            return .init(baseUrl: .easycore, path: .addFavouriteNumber, method: .post, params: params, additionalHeaders: nil)
        case .getRecentRechargeTransaction:
            return .init(baseUrl: .easycore, path: .getRecentTransaction, method: .post, params: [:], additionalHeaders: nil)
        case .getFavoriteNumber :
            return .init(baseUrl: .easycore, path: .getFavouriteNumber, method: .post, params: [:], additionalHeaders: nil)
        case .getOprators :
            return .init(baseUrl: .easycore, path: .operatorList, method: .post, params: [:], additionalHeaders: nil)
        case .getRechargePlans :
            return .init(baseUrl: .easycore, path: .getPlans, method: .post, params: [:], additionalHeaders: nil)
        case .deletefav(params: let params):
            return .init(baseUrl: .easycore, path: .deleteFavNumber, method: .post, params: params, additionalHeaders: nil)
        case .updatefav(params: let params):
            return .init(baseUrl: .easycore, path: .updateFavNumber, method: .post, params: params, additionalHeaders: nil)
        case .ratingTransaction(rating: let rating, transactionId: let transaction) :
            return .init(baseUrl: .easycore, path: .transactionRating, method: .post, params: ["transaction_id":transaction,"rating":rating], additionalHeaders: nil)
        case .transactionStatus( transactionId: let transaction) :
            return .init(baseUrl: .easycore, path: .paymentStatus, method: .post, params: ["tran_id":transaction], additionalHeaders: nil)
        case .topUP:
            // Setting up connection type  and  operator for user account while first time recharging self number
//            if UserSettings.sharedInstance.userRecharge  == nil {
//                if let selfNumber = UserSettings.sharedInstance.selectedNumbers.first(where: {$0.phone == UserSettings.sharedInstance.user?.mobile}){
//                    UserSettings.sharedInstance.userRecharge = selfNumber
//                }
//            }
           
            var param : [String:Any] = [:]
//            var shRecharge = ""
//            for (index,entity) in  UserSettings.sharedInstance.selectedNumbers.enumerated() {
//                entity.phone = entity.phone?.replacingOccurrences(of: " ", with: "")
//                param["data[\(index)][phone]"] = entity.phone ?? ""
//                param["data[\(index)][amount]"] = entity.amount ?? 0
//                param["data[\(index)][operator]"] = entity.operatorId ?? 0
//                param["data[\(index)][type]"] = entity.type ?? ""
//                shRecharge = entity.scRechargeMob ?? ""
//            }
//            if shRecharge != ""{
//                let dateStr  = EasySingleton.shared.date(toTimestamp: shRecharge, dateFormate: "dd-MM-yyyy | hh:mm a", expectedDateFormate: "dd MMM yyyy hh:mm a")
//                param["schedule_recharge"] = dateStr
//            }
//            param["is_mobile"] = true
            
            return .init(baseUrl: .easycore, path: .topUp, method: .post, params: param, additionalHeaders: nil)
        
        case .initiateHoldRechargeUser(transactionId: let transactionId, allowOffer: let allowOffer):
            return .init(baseUrl: .easycore, path: .holdRechargeUser, method: .post, params: ["transaction_id":transactionId,"allow_offer":allowOffer], additionalHeaders: nil)
            case .getRechargeReq(info : let model):
                return .init(baseUrl: .easycore, path: .createRechargeReq, method: .post, params: model.dictionary, additionalHeaders: nil)
        
        }
    }
}
*/
