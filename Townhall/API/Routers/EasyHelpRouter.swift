//
//  EasyHelpRouter.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 31/10/21.
//

import Foundation

enum EasyHelpRouter {
    case getContact
    case getTopics(link:String)
    case terms
    case privacy
    
    var requestBuilder:RequestBuilder{
        switch self {
        case .getContact:
            return .init(baseUrl: .easycore, path: .getContact, method: .post, params: [:], additionalHeaders: nil)
        case .getTopics(let link):
            return .init(baseUrl: .easycore, path: .getTopics, method: .post, params: ["link_url": link], additionalHeaders: nil)
     
        case .terms:
            return .init(baseUrl: .easycore, path: .getTerms, method: .post, params: [:], additionalHeaders: nil)
        case .privacy:
            return .init(baseUrl: .easycore, path: .getPrivacy, method: .post, params: [:], additionalHeaders: nil)
        }
    }
}
