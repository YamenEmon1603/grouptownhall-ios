//
//  UserRouter.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
enum UserRouter{
    case getDeviceKey
    case userStatus(requestModel: LoginRequestModel)
    case getFbGoogleUserInfo(token: String, provider: String)
    case getAppleUserInfo(appleUserInfor : AppleUserInfo)
    case login(requestModel: LoginRequestModel)
    case registration(registrationInfo : RegistrationInfo)
    case registrationOtpVerification(mobile: String , otp : String)
    case registrationEmailOtpVerification(email: String , otp : String)
    case registrationOTPVerify(userInfo:UserInfo)
    case forgotPasswordOtp(mobile : String)
    case forgotEmailPasswordOtp(email : String)
    case forgotPasswordRestore(info:ForgetPasswordRestore)
    case forgotPasswordEmailRestore(info:ForgotPasswordEmailRestore)
    case oldUserOtpForMobileRestore(info:LoginRequestModel)
    case oldUserNumberSetOtpVeification(info:OldUserNumberSetOtpVeification)
    case logout
    case logoutWith(params : [String:Any])
    case loggedInDevices
    case logoutFromAllDevices
    case resetPassword(params : [String:Any])
    case notifications
    case clearNotifications
    case userProfile
    case profileEditRequest(info:ProfileEditRequest)
    case updateProfilePic
    case dynamicReport(info:DynamicReport)
    case exclusiveOffers
    
 
    
    
    var requestBuilder: RequestBuilder{
        
        switch self {
            
        case .getDeviceKey:
            return .init(baseUrl: .easycore, path: .getDeviceKey, method: .post, params: UserDeviceRequestModel().dictionary, additionalHeaders: nil)
        case .getFbGoogleUserInfo(token: let token, provider:let provider):
            return .init(baseUrl: .easycore, path: .socialLogin, method: .post, params: ["access_token":token,"provider":provider], additionalHeaders: nil)
        case .getAppleUserInfo(let appleUserInfo):
            return .init(baseUrl: .easycore, path: .socialLogin, method: .post, params: appleUserInfo.dictionary, additionalHeaders: nil)
        case .userStatus(let reqModel):
            return .init(baseUrl: .easycore, path: .userStatus, method: .post, params: reqModel.dictionary, additionalHeaders: nil)
        
        case .login(let reqModel):
            return .init(baseUrl: .easycore, path: .userLogin, method: .post, params: reqModel.dictionary, additionalHeaders: nil)
      
        case .registration(registrationInfo : let registrationInfo):
            return .init(baseUrl: .easycore, path: .registration, method: .post, params: registrationInfo.dictionary, additionalHeaders: nil)
        case .registrationOTPVerify(userInfo: let userInfo):
            return .init(baseUrl: .easycore, path: .otpVerification, method: .post, params: userInfo.dictionary, additionalHeaders: nil)
        
        case .registrationOtpVerification( let mobile , let otp):
            return .init(baseUrl: .easycore, path: .otpVerification, method: .post, params: ["mobile" : mobile, "otp" :otp], additionalHeaders: nil)
        case .forgotPasswordOtp(let mobile) :
            return  .init(baseUrl: .easycore, path: .forgotPassOTP, method: .post, params: ["mobile":mobile], additionalHeaders: nil)
        case .forgotPasswordRestore(let info):
            return  .init(baseUrl: .easycore, path: .forgotPassRestore, method: .post, params: info.dictionary, additionalHeaders: nil)
        case .forgotPasswordEmailRestore(let info):
            return  .init(baseUrl: .easycore, path: .forgotPassRestore, method: .post, params: info.dictionary, additionalHeaders: nil)
            
        case .oldUserOtpForMobileRestore(let info):
            return  .init(baseUrl: .easycore, path: .oldUserOtpMobileRestore, method: .post, params: info.dictionary, additionalHeaders: nil)
        case .oldUserNumberSetOtpVeification(let info):
            return  .init(baseUrl: .easycore, path: .otpVerification, method: .post, params: info.dictionary, additionalHeaders: nil)
        case .logout:
            return .init(baseUrl: .easycore, path: .logout, method: .post, params: [:], additionalHeaders: nil)
        case  .logoutWith(params : let params):
            return .init(baseUrl: .easycore, path: .logout, method: .post, params: params, additionalHeaders: nil)
        case .logoutFromAllDevices:
            return .init(baseUrl: .easycore, path: .logOutAllDevice, method: .post, params: [:], additionalHeaders: nil)
        case .loggedInDevices:
            return .init(baseUrl: .easycore, path: .loginDevices, method: .post, params: [:], additionalHeaders: nil)
        case .resetPassword(params: let params):
            return .init(baseUrl: .easycore, path: .resetPass, method: .post, params: params, additionalHeaders: nil)
        case .notifications:
            return .init(baseUrl: .easycore, path: .notification, method: .post, params: [:], additionalHeaders: nil)
        case .clearNotifications:
            return .init(baseUrl: .easycore, path: .clearNotification, method: .post, params: [:], additionalHeaders: nil)
        case .profileEditRequest(let info):
            return.init(baseUrl: .easycore, path: .profileUpdate, method: .post, params: info.dictionary, additionalHeaders: nil)
        case .updateProfilePic:
            return .init(baseUrl: .easycore, path: .profilePicUpdate, method: .post, params: [:], additionalHeaders: nil)
        case .exclusiveOffers:
            return .init(baseUrl: .easycore, path: .exclusiveOfferes, method: .post, params: [:], additionalHeaders: nil)
        case .dynamicReport(let info):
            return .init(baseUrl: .easycore, path: .dynamicReport, method: .post, params: info.dictionary, additionalHeaders: nil)
        case .forgotEmailPasswordOtp(email: let email):
            return  .init(baseUrl: .easycore, path: .forgotPassOTP, method: .post, params: ["email":email], additionalHeaders: nil)
        case .registrationEmailOtpVerification(email: let email, otp: let otp):
            return .init(baseUrl: .easycore, path: .otpVerification, method: .post, params: ["email" : email, "otp" :otp], additionalHeaders: nil)
        case .userProfile:
            return .init(baseUrl: .easycore, path: .userProfile, method: .post, params: [:], additionalHeaders: nil)
        }
       
    }
}
