//
//  TownHallWebserviceHandler.swift
//  Townhall
//
//  Created by Mausum Nandy on 9/12/21.
//


import Foundation
import Alamofire
extension WebServiceHandler{
    func getContent(completion:@escaping(Result<DashboardContentResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: TownHallRouter.content(id: 1).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getExperienceContent(completion:@escaping(Result<ExperienceConentModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: TownHallRouter.content(id: 2).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func townHallVersionCheck(completion:@escaping(Result<GetDeviceKeyResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: TownHallRouter.versionCheck.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func townhallImageUpload(phone:String,serial : Int,imageData:Data,completion:@escaping(Result<EasyBaseResponse, AFError>) -> Void, progressBlock:@escaping ((_ completed : Double)->Void), shouldShowLoader: Bool) {
        
        performUploadRequest(imageData: imageData, fieldName: "user_image", requestBuilder: TownHallRouter.imageUpload(phone: phone).requestBuilder, completion: completion,progressBlock:progressBlock, showLoader: shouldShowLoader,additionalParam: ["mobile_no":phone,"image_serial":"\(serial)"])
    }
    func gameQrInfo(with reqModel:PlayQrRequestModel, completion:@escaping(Result<GameQrResponse, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: TownHallRouter.gameQrInfo(model: reqModel).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    
    
    func checkUserStatus(phone:String,completion:@escaping(Result<UserStatusCheckResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TownHallRouter.imageStatusCheck(phone: phone).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
}
