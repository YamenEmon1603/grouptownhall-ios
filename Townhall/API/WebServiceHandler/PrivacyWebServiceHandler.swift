//
//  PrivacyWebServiceHandler.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 31/10/21.
//

import Foundation
import Alamofire

extension WebServiceHandler{
    //MARK:- MISC
    
    func getHelpAndSuportContact(completion:@escaping(Result<EasyHelpResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyHelpRouter.getContact.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getTopics(link:String,completion:@escaping(Result<HelpTopicsResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyHelpRouter.getTopics(link: link).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getTerms(completion:@escaping(Result<CMSResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyHelpRouter.terms.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getPrivacy(completion:@escaping(Result<CMSResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyHelpRouter.privacy.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
}
