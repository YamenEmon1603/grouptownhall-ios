//
//  UtilityWebserviceHandler.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 16/11/21.
//

import Foundation
import Alamofire
extension WebServiceHandler {
    func getUtilityServiceList(completion:@escaping(Result<UtilityServiceListResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UtilityRouter.serviceList.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func fetchUtilityBillInfo(params:[String:Any],completion:@escaping(Result<BillInfoResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
            performRequest(requestBuilder: UtilityRouter.billInfo(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
      }
    func payUtilityBill(params:[String:Any],completion:@escaping(Result<SSLComerceResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UtilityRouter.billPayment(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
}
