//
//  BanglaQRWebServiceHandler.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 25/11/21.
//

import Foundation
import Alamofire

extension WebServiceHandler{
    //MARK:- BANGLA QR
    
    func qrPaymentCreate(transactionAmount:String,transactionFeeAmount: String,payload: String,merchantName: String,merchantCity: String,merchantMcc: String,tag62First:String,tag62Second: String, merchantId:String,pin:String,completion:@escaping(Result<QrPaymentResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.qrPaymentCreate(transactionAmount: transactionAmount, transactionFeeAmount: transactionFeeAmount,payload: payload, merchantName: merchantName, merchantCity: merchantCity, merchantMcc: merchantMcc, tag62First: tag62First, tag62Second: tag62Second, merchantId: merchantId, pin : pin).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func qrPaymentInit(transactionId:String, completion:@escaping(Result<EasyBaseResponse, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.qrPaymentInit(transactionId: transactionId).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func qrOtpVerify(otp:String, completion:@escaping(Result<QrOtpverifyReponse, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.qrOtpVerify(otp:otp).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func qrOtpSendResend(completion:@escaping(Result<EasyBaseResponse, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.qrOtpSendResend.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    
    func qrPinSet(banglaQrPinKey: String,pin:String,confirmPin:String,completion:@escaping(Result<LoginResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.qrPinSet(banglaQrPinKey: banglaQrPinKey, pin: pin, confirmPin: confirmPin).requestBuilder, completion: completion, showLoader: shouldShowLoader)
        
    }
    func qrChangePin(oldQrPin: String,qrPin:String,confirmQrPin:String,completion:@escaping(Result<LoginResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.qrChangePin(oldQrPin: oldQrPin, qrPin: qrPin, confirmQrPin: confirmQrPin).requestBuilder, completion: completion, showLoader: shouldShowLoader)
        
    }
    
    func deleteSavedCard(cardIndex: String,userRefer:String,completion:@escaping(Result<EasyBaseResponse, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.deleteCard(cardIndex: cardIndex, userRefer: userRefer).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    
    func autoDebit(params: AutoDebitRequestModel,completion:@escaping(Result<AutoDebitResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: EasyBanglaQRRouter.autoDebit(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
}
