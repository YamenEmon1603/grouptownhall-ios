//
//  WebServiceHandler.swift
//  Easy-Merchant
//
//  Created by Mausum Nandy on 4/11/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation
import Alamofire

class WebServiceHandler: NSObject {
    
    static let shared = WebServiceHandler()
    private var reachability = NetworkReachabilityManager.init()
   
    private  override init(){
        super.init()
        reachability?.startListening { [weak self] status in
            switch status {
            
            case .unknown:
                print("network status unknown")
            case .notReachable:
                print("network status not reachable")
                self?.showNoInternetMessage()
            case .reachable(_):
                print("network status reachable")
            }
        }
    }
    
    @discardableResult
    func performRequest<T:Decodable>(requestBuilder: RequestBuilder, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, AFError>)->Void, showLoader: Bool) -> DataRequest? {
        
        guard self.reachability?.isReachable == true else {
            return nil
        }
        if showLoader {EasyLoader.sharedInstance.startAnimation()}
        return AF.request(requestBuilder).responseDecodable (decoder: decoder){ (response: DataResponse<T, AFError>) in
            guard response.response?.statusCode != 401 else {
                self.requestNotAuthenticated()
                if showLoader {EasyLoader.sharedInstance.stopAnimation()}
                return
            }
            print("\(((Constant.isDev) ? requestBuilder.baseUrl.Sandbox : requestBuilder.baseUrl.Production)+requestBuilder.path.rawValue)\nParams:\n \(String(describing: requestBuilder.params)) \nresponse:\n" + (String.init(data: response.data ?? Data.init(), encoding: .utf8) ?? ""))
            completion(response.result)
            if showLoader {EasyLoader.sharedInstance.stopAnimation()}
        }
    }
    
    func performUploadRequest<T:Decodable>(imageData:Data,fieldName:String,requestBuilder: RequestBuilder, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, AFError>)->Void, progressBlock:((_ completed : Double)->Void)? = nil, showLoader: Bool,additionalParam:[String:String]? = nil){
        guard self.reachability?.isReachable == true else {
            return
        }
        
        if showLoader {EasyLoader.sharedInstance.startAnimation()}
        AF.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: fieldName, fileName: "\(fieldName).jpg", mimeType: "image/jpg")
            if let additionalParam = additionalParam {
                for (key, value) in additionalParam {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
                
            }
        }, with: requestBuilder).uploadProgress { progress in
            progressBlock?(progress.fractionCompleted)
        }.responseDecodable (decoder: decoder){ (response: DataResponse<T, AFError>) in
            guard response.response?.statusCode != 401 else {
                self.requestNotAuthenticated()
                if showLoader {EasyLoader.sharedInstance.stopAnimation()}
                return
            }
            print("Date:\(Date()) \(((Constant.isDev) ? requestBuilder.baseUrl.Sandbox : requestBuilder.baseUrl.Production)+requestBuilder.path.rawValue)\nParams:\n \(String(describing: requestBuilder.params)) \nresponse:\n" + (String.init(data: response.data ?? Data.init(), encoding: .utf8) ?? ""))
            completion(response.result)
            if showLoader {EasyLoader.sharedInstance.stopAnimation()}
        }
    }
    private func requestNotAuthenticated(){
        
    }
    
    private func showNoInternetMessage(){
        
    }
    
    deinit {
        reachability?.stopListening()
    }
   
    
   
 
    
    

}

