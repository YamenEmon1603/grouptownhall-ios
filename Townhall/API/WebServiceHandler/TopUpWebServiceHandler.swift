//
//  TopUpWebServiceHandler.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/23/21.
//

import Foundation
import Alamofire
extension WebServiceHandler {
    /* func getOprators(completion:@escaping(Result<OperatorListResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.getOprators.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getRechargePlans(completion:@escaping(Result<RechargePlanResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.getRechargePlans.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getFavoriteNumbers(completion:@escaping(Result<GetFavoriteNumberResponse, AFError>) -> Void, shouldShowLoader: Bool)  {
        performRequest(requestBuilder: TopUpRouter.getFavoriteNumber.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getRecentTransactions(completion:@escaping(Result<GetRecentTransectionResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.getRecentRechargeTransaction.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func deleteFavContact(params : [String : Any] ,completion:@escaping(Result<AddToFavoriteResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.deletefav(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func updateFavContact(params : UpdateFavRequestModel ,completion:@escaping(Result<AddToFavoriteResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.updatefav(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func addFavoriteNumber(params : UpdateFavRequestModel,completion:@escaping(Result<AddToFavoriteResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.addFavoriteNumber(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func topUp(completion:@escaping(Result<SSLComerceResponse, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: TopUpRouter.topUp.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func transectionStatus(transactionId:String,completion:@escaping(Result<PaymentStatusReponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: TopUpRouter.transactionStatus(transactionId: transactionId).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    //MARK: - Favorite Number
    func postFavoriteNumber(params : UpdateFavRequestModel ,completion:@escaping(Result<AddToFavoriteResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.addFavoriteNumber(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
//    func deleteFavContact(params : [String : Any] ,completion:@escaping(Result<AddToFavoriteResponse, AFError>) -> Void, shouldShowLoader: Bool){
//        performRequest(requestBuilder: TopUpRouter.deletefav(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
//    }
//    func updateFavContact(params : [String : Any] ,completion:@escaping(Result<AddToFavoriteResponse, AFError>) -> Void, shouldShowLoader: Bool){
//        performRequest(requestBuilder: TopUpRouter.updatefav(params: params).requestBuilder, completion: completion, showLoader: shouldShowLoader)
//    }
    //MARK:- Recharge Request
    
   func getRechargeReq(info:RechargeRequestModel,completion:@escaping(Result<EasyBaseResponse, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: TopUpRouter.getRechargeReq(info: info).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getRechargeRequestList(page:Int,completion:@escaping(Result<ReceivedHistoryResponse, AFError>) ->Void , shouldShowLoader: Bool)  {
        performRequest(requestBuilder: TopUpRouter.getRechargeRequestList(page: page).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getRechargeRequestSent(page:Int,completion:@escaping(Result<ReceivedHistoryResponse, AFError>) ->Void , shouldShowLoader: Bool)  {
        performRequest(requestBuilder: TopUpRouter.getRechargeRequestSent(page: page).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
   
    func rechargeRequestDetails(ruid: String,completion:@escaping(Result<RechargePayResponse, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.rechargeRequestDetails(ruid: ruid).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    //
    func rechargeRequestReject(requestId: String,completion:@escaping(Result<EasyBaseResponse, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: TopUpRouter.rechargeRequestReject(requestId: requestId).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getReceivedHistory(page: Int,completion:@escaping(Result<ReceivedHistoryResponse, AFError>) ->Void , shouldShowLoader: Bool)  {
        performRequest(requestBuilder: TopUpRouter.receivedHistory(page: page).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func rechargeRequestTopUp(reqModel : RechargeRequestPayData,amount:String,ruid:String,isRechargeRequest:Bool,completion:@escaping(Result<SSLComerceResponse, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: TopUpRouter.requestTopUP(reqModel: reqModel, amount: amount,ruid: ruid,isRechargeRequest: isRechargeRequest).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
*/
    
}
