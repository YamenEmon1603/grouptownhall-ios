//
//  UserWebServiceHandler.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import Alamofire
extension WebServiceHandler{
    func getDeviceKey(completion:@escaping(Result<GetDeviceKeyResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.getDeviceKey.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getUserStatus(for reqModel:LoginRequestModel, completion:@escaping(Result<GetUserStatusResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.userStatus(requestModel: reqModel).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func oldUserOtpForMobileRestore(for reqModel:LoginRequestModel,completion:@escaping(Result<GetUserStatusResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.oldUserOtpForMobileRestore(info: reqModel).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
  
   
    func login(with reqModel:LoginRequestModel, completion:@escaping(Result<LoginResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.login(requestModel: reqModel).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
   
    
    func verifyFPOtp(mobile:String,otp:String ,completion:@escaping(Result<ForgotPasswordOtpVerification, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.registrationOtpVerification(mobile: mobile, otp: otp).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func verifyFEmailFPOtp(email:String,otp:String ,completion:@escaping(Result<ForgotPasswordOtpVerification, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.registrationEmailOtpVerification(email: email, otp: otp).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func sendForgotPasswordOtp(mobile:String,completion:@escaping(Result<GetUserStatusResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.forgotPasswordOtp(mobile: mobile).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func sendEmailForgotPasswordOtp(email:String,completion:@escaping(Result<GetUserStatusResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.forgotEmailPasswordOtp(email: email).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func forgotPasswordRestore(reqModel:ForgetPasswordRestore,completion:@escaping(Result<LoginResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.forgotPasswordRestore(info: reqModel).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func forgotPasswordEmailRestore(reqModel: ForgotPasswordEmailRestore,completion:@escaping(Result<LoginResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.forgotPasswordEmailRestore(info: reqModel).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    
    func registration(reqModel :  RegistrationInfo,completion:@escaping(Result<LoginResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.registration(registrationInfo: reqModel).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    
    func getFBGoogleUserStatus(for token: String,provider:String, completion:@escaping(Result<LoginResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.getFbGoogleUserInfo(token: token, provider: provider).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getAppleUserStatus(appleUserInfor : AppleUserInfo, completion:@escaping(Result<LoginResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.getAppleUserInfo(appleUserInfor: appleUserInfor).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getProfileDetails(completion:@escaping(Result<GetProfileDetailsResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.userProfile.requestBuilder, completion: completion, showLoader:shouldShowLoader)
    }
    
    func editProfile(info:ProfileEditRequest,completion:@escaping(Result<ProfileEditResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.profileEditRequest(info: info).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func uploadProfilePic(imageData:Data,completion:@escaping(Result<ProfileEditResponse, AFError>) -> Void, shouldShowLoader: Bool) {
        performUploadRequest(imageData: imageData, fieldName: "profile_picture", requestBuilder: UserRouter.updateProfilePic.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getTransactionHistory(info:DynamicReport,completion:@escaping(Result<transactionHistoryResponseModel, AFError>) ->Void , shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.dynamicReport(info: info).requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func logout(completion:@escaping(Result<EasySingleLogout, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.logout.requestBuilder, completion: completion, showLoader: true)
    }
    func logoutWithParam(_ params:[String:Any],completion:@escaping(Result<EasyBaseResponse, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.logoutWith(params:params).requestBuilder, completion: completion, showLoader: true)
    }
    func logoutFromAllDevice(completion:@escaping(Result<EasySingleLogout, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.logoutFromAllDevices.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getLoggedInDevices(completion:@escaping(Result<LoggedInDeviceResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.loggedInDevices.requestBuilder, completion: completion, showLoader: true)
    }
    func resetPassword(_ params:[String:Any],completion:@escaping(Result<LoginResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.resetPassword(params: params).requestBuilder, completion: completion, showLoader: true)
    }
    //MARK: - Notifications
    
    func getNotifications(completion:@escaping(Result<NotificationsResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.notifications.requestBuilder, completion: completion, showLoader: true)
    }
    func clearNotifications(completion:@escaping(Result<NotificationsResponseModel, AFError>) -> Void, shouldShowLoader: Bool){
        performRequest(requestBuilder: UserRouter.clearNotifications.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
    func getExclusiveOffers(completion:@escaping(Result<OffersResponseModel, AFError>) -> Void, shouldShowLoader: Bool) {
        performRequest(requestBuilder: UserRouter.exclusiveOffers.requestBuilder, completion: completion, showLoader: shouldShowLoader)
    }
//    func getFavoriteNumber(completion:@escaping(Result<GetFavoriteNumberResponse, AFError>) -> Void, shouldShowLoader: Bool) {
//        performRequest(requestBuilder: UserRouter.getFavoriteNumber.requestBuilder, completion: completion, showLoader: shouldShowLoader)
//    }
//    
}
