//
//  QRInteractorModule.swift
//  Easy.com.bd
//
//  Created by Jamil Hasnine Tamim on 1/10/19.
//  Copyright © 2019 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit

class QRInteractorModule: QRInputInteractorProtocol{
    var presenter: QROutputInteractorProtocol?
}
