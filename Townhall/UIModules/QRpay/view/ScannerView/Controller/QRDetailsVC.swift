//
//  QRDetailsVC.swift
//  BanglaQR
//
//  Created by Yamen Emon on 29/10/20.
//

import UIKit
import SSLCommerzSDK

class QRDetailsVC: EasyBaseViewController, GlobalNavigationBarBtnActionDelegate {
    
    func backBtnPressed() {
        self.dismiss(animated: true, completion: {
            if let _delegate = self.scannerDelegate {
                _delegate.failCallBack("User back button pressed after parsing QR data")
            }
        })
    }
    
    var scannerDelegate : QrScannerDelegate!
    var header : HeaderModel?
    var arrayOfTextFields : [TextFieldModel]?
    var mainModel : BanglaQRInitialization?
    let factX = UIScreen.main.bounds.size.width/320
    var navigationTitle : UILabel!
    var keyboardObserver: KeyboardObserver!
    var qrInitializerModel : BanglaQRInitialization?
    var sslCommerz:SSLCommerz?
    
    
    lazy var roundView: UIView = {
        let roundView =  UIView()
        roundView.backgroundColor = .blueViolet
        roundView.translatesAutoresizingMaskIntoConstraints = false
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .EasyFont(ofSize: 30, style: .bold)
        label.textColor = .white
        label.text = String(header?.merchantName?.prefix(1) ?? "")
        roundView.addSubview(label)
        label.centerXAnchor.constraint(equalTo: roundView.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: roundView.centerYAnchor).isActive = true
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.0) {
            roundView.layer.cornerRadius = max(roundView.frame.width,roundView.frame.height)/2
            roundView.clipsToBounds = true
        }
        
        return roundView
    }()
//    lazy var navView: EasyNavigationBarView = {
//        let navView = EasyNavigationBarView(title: "marchent_details".easyLocalized().uppercased(), withBackButton: true)
//
//        navView.backgroundColor = .white
//        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
//
//        return navView
//    }()
    
    
    
    // MARK: TOP MARCHANT VIEW
    lazy  var topContainerView: UIView = {
        let topContainerView =  UIView()
        topContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        // MARK: TOP NAV VIEW
        
        let navView = GlobalNavigationView(title: "marchent_details".easyLocalized(), image: UIImage(named: "icback")!)
        
        
        navView.navDelegate = self
        topContainerView.addSubview(navView)
        navView.translatesAutoresizingMaskIntoConstraints = false
        navView.topAnchor.constraint(equalTo: topContainerView.topAnchor).isActive = true
        navView.leadingAnchor.constraint(equalTo: topContainerView.leadingAnchor).isActive = true
        navView.trailingAnchor.constraint(equalTo: topContainerView.trailingAnchor).isActive = true
        navView.heightAnchor.constraint(equalTo: topContainerView.heightAnchor, multiplier: 0.2).isActive = true
        
        
        // MARK: MERCHANT INFORMATION
        topContainerView.addSubview(roundView)
        roundView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 10).isActive = true
        roundView.centerXAnchor.constraint(equalTo: topContainerView.centerXAnchor).isActive = true
        roundView.heightAnchor.constraint(equalTo: topContainerView.heightAnchor, multiplier: 0.4).isActive = true
        roundView.widthAnchor.constraint(equalTo: roundView.heightAnchor, multiplier:1).isActive = true
        
        topContainerView.addSubview(merchantStack)
        let stackTop = merchantStack.topAnchor.constraint(equalTo: roundView.bottomAnchor ,constant: 10)
        let stackLeading = merchantStack.leadingAnchor.constraint(equalTo: topContainerView.leadingAnchor,constant: 20)
        let stackTrailing = merchantStack.trailingAnchor.constraint(equalTo: topContainerView.trailingAnchor, constant: -20)
        // let stackBottom = merchantStack.bottomAnchor.constraint(equalTo: bottomSeparatorView.bottomAnchor,constant: -10)
        
        NSLayoutConstraint.activate([stackTop,stackLeading,stackTrailing])
        merchantStack.addArrangedSubview(merchantName)
        merchantStack.addArrangedSubview(merchantCity)
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.hexStringToUIColor(hex: "f5f5f5")
        let _ = separatorView.addBorder(side: .top, color: .lightPeriwinkleTwo, width: 0.5)
        let _ = separatorView.addBorder(side: .bottom, color: .lightPeriwinkleTwo, width: 0.5)
        topContainerView.addSubview(separatorView)
        separatorView.leadingAnchor.constraint(equalTo: topContainerView.leadingAnchor).isActive = true
        separatorView.trailingAnchor.constraint(equalTo: topContainerView.trailingAnchor).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: topContainerView.bottomAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 6).isActive = true
        
        return topContainerView
    }()
    lazy var paymentButton : UIButton = {
        let button  = UIButton()
        button.setTitle("Pay Now", for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.cornerRadius = 5
        button.titleEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(paymentTapped(_ :)), for: .touchUpInside)
        return button
    }()
    lazy var navTitle : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "marchent_details".easyLocalized()
        label.textColor = .black
        label.textAlignment = .left
        label.font = .EasyFont(ofSize: 12, style: .medium)
        return label
    }()
    lazy var merchantStack : UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 3
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        return stackView
    }()
    lazy var merchantName : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        if let headerInfo  = header{
            label.text = headerInfo.merchantName
        }
        label.font = .EasyFont(ofSize: 20, style: .medium)
        label.textColor = .black
        return label
    }()
    lazy var merchantDate : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        if let headerInfo  = header{
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            label.text = formatter.string(from: date)
        }
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.textColor = .white
        return label
    }()
    lazy var merchantCity : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        if let headerInfo  = header{
            label.text = headerInfo.merchantCity
        }
        label.font = .EasyFont(ofSize: 14, style: .regular)
        label.textColor = .battleshipGrey
        return label
    }()
    lazy var separatorView : UIView  = {
        let bottomView = UIView()
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.autoresizingMask = .flexibleWidth
        bottomView.backgroundColor = .lightPeriwinkle
        return bottomView
    }()
    
    lazy var bottomSeparatorView : UIView  = {
        let bottomView = UIView()
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.autoresizingMask = .flexibleWidth
        bottomView.backgroundColor = .lightPeriwinkleTwo
        return bottomView
    }()
    
    
    // MARK: BOTTOM PAYMENT INFO VIEW
    
    lazy  var bottomContainerView: UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy  var paymentInfoScrollView: UIScrollView = {
        let scView =  UIScrollView()
        scView.bounces = false
        scView.alwaysBounceHorizontal =  false
        scView.translatesAutoresizingMaskIntoConstraints = false
        return scView
    }()
    lazy  var scrollContainerView: UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var paymentInfoStackView : UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .fill
        
        return stackView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardObserver = KeyboardObserver(for: self)
        hideKeyboardWhenTappedAround()
        
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        keyboardObserver.add()
    }
    
    
    @objc override func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func setupView(){
        
        // MARK: TOP CONTAINER
        self.navigationController?.navigationBar.isHidden = true
        
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(topContainerView)
        topContainerView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        topContainerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        topContainerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        topContainerView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.30).isActive = true
        topContainerView.backgroundColor = .white
        
        // MARK: BottomContainerView
        
        self.view.addSubview(bottomContainerView)
        bottomContainerView.topAnchor.constraint(equalTo: self.topContainerView.bottomAnchor).isActive = true
        bottomContainerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        bottomContainerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        bottomContainerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        bottomContainerView.backgroundColor = .white
        bottomContainerView.addSubview(paymentInfoScrollView)
        
        bottomContainerView.addConstraintWithFormat("H:|-[v0]-|", views: paymentInfoScrollView)
        bottomContainerView.addConstraintWithFormat("V:|-[v0]-|", views: paymentInfoScrollView)
        paymentInfoScrollView.addSubview(scrollContainerView)
        scrollContainerView.topAnchor.constraint(equalTo: self.paymentInfoScrollView.topAnchor).isActive = true
        scrollContainerView.leadingAnchor.constraint(equalTo: self.paymentInfoScrollView.leadingAnchor).isActive = true
        scrollContainerView.trailingAnchor.constraint(equalTo: self.paymentInfoScrollView.trailingAnchor).isActive = true
        scrollContainerView.bottomAnchor.constraint(equalTo: self.paymentInfoScrollView.bottomAnchor).isActive = true
        scrollContainerView.widthAnchor.constraint(equalTo: paymentInfoScrollView.widthAnchor).isActive = true
        scrollContainerView.addSubview(paymentInfoStackView)
        paymentInfoStackView.topAnchor.constraint(equalTo: self.scrollContainerView.topAnchor).isActive = true
        paymentInfoStackView.leadingAnchor.constraint(equalTo: self.scrollContainerView.leadingAnchor).isActive = true
        paymentInfoStackView.trailingAnchor.constraint(equalTo: self.scrollContainerView.trailingAnchor).isActive = true
        paymentInfoStackView.bottomAnchor.constraint(equalTo: self.scrollContainerView.bottomAnchor, constant: 0).isActive = true
        
        if let textFiledsInfo = arrayOfTextFields?.filter({$0.isVisible == true}){
            for item in textFiledsInfo{
                if item.isEditable == true{
                    let view = PaymentInfoItemView(textInfo: item)
                    view.tag = item.index?.rawValue ?? 0
                    paymentInfoStackView.addArrangedSubview(view)
                    view.heightAnchor.constraint(equalToConstant: self.view.frame.height * 0.075).isActive = true
                }else{
                    let view = PaymentInfoLabelView(textInfo: item)
                    paymentInfoStackView.addArrangedSubview(view)
                    view.heightAnchor.constraint(equalToConstant: self.view.frame.height * 0.075).isActive = true
                }
                
            }
        }
        let btnProceed = UIButton()
        btnProceed.translatesAutoresizingMaskIntoConstraints = false
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setTitle( "proceed".easyLocalized(), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 14, style: .medium)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.titleEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        self.view.addSubview(btnProceed)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        btnProceed.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        btnProceed.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        btnProceed.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        btnProceed.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1).isActive = true
        self.view.bringSubviewToFront(btnProceed)
    }
    
    @objc func clickButtonLeft(_ sender:UIButton){
        
        self.dismiss(animated: true, completion: {
            if let _delegate = self.scannerDelegate {
                _delegate.failCallBack("User back button pressed after parsing QR data")
            }
        })
    }
    @objc func btnProceedAction(_ sender : UIButton){
        let hasEditable = arrayOfTextFields?.first(where: {$0.isEditable == true})
        if hasEditable == nil{
            let pinVc = UIStoryboard(name: "PIN", bundle: nil).instantiateViewController(withIdentifier: "PINVC") as! EasyQRPinVC
            pinVc.delegate = self
            self.presentAsPopUp(vc: pinVc)
        }else{
            var shouldProceed: Bool = true
            paymentInfoStackView.arrangedSubviews.forEach { (child) in
                if let item = child as? PaymentInfoItemView{
                    if  let index = arrayOfTextFields?.firstIndex(where: {$0.index?.rawValue == item.tag}){
                        if item.infoTextField.text?.isEmpty == true || item.infoTextField.text == "0.0" || item.infoTextField.text == "0"{
                            self.showToast(message: "\(arrayOfTextFields?[index].textName ?? "Input") \("field_is_required".easyLocalized())")
                            shouldProceed = false
                            return
                            
                        }else{
                            arrayOfTextFields?[index].textValue = item.infoTextField.text
                            arrayOfTextFields?[index].isEditable = false
                        }
                        
                    }
                }
            }
            if shouldProceed == true{
                let detailVC =  QRDetailsVC()
                detailVC.scannerDelegate = self.scannerDelegate
                detailVC.header = self.header
                detailVC.arrayOfTextFields = self.arrayOfTextFields
                detailVC.qrInitializerModel = self.mainModel
                self.navigationController?.pushViewController(detailVC, animated: true)
            }
            
        }
    }
    @objc func paymentTapped(_ sender:UIButton){
        debugPrint("Payment Tapped")
        paymentInfoStackView.arrangedSubviews.forEach { (child) in
            if let item = child as? PaymentInfoItemView{
                arrayOfTextFields?.first(where: {$0.index?.rawValue == item.tag})?.textValue = item.infoTextField.text
            }
        }
        
        self.dismiss(animated: true, completion: {
            if let _delegate = self.scannerDelegate {
                var keyValuePair : [[String:String]] = [[:]]
                keyValuePair.append(["name": "Merchant Name"  ,"value" : self.header?.merchantName ?? ""])
                keyValuePair.append(["name": "Merchant City"  ,"value" : self.header?.merchantCity ?? ""])
                keyValuePair.append( ["name": "Merchant Id"  ,"value" : self.header?.merchantId ?? ""])
                keyValuePair.append(["name": "Merchant Pan Id"  ,"value" : self.header?.merchantPanId ?? ""])
                
                
                self.arrayOfTextFields?.forEach({ (model) in
                    keyValuePair.append(["name":  model.textName ?? "" ,"value" : model.textValue ?? ""])
                    
                })
                debugPrint(keyValuePair)
                _delegate.successCallBack(keyValuePair)
            }
        })
    }
    
    func qrPaymentCreate(pin: String){
        WebServiceHandler.shared.qrPaymentCreate(transactionAmount: getTextValueString(index: .amount) , transactionFeeAmount: "",payload: getTextValueString(index: .payload), merchantName: header?.merchantName ?? "", merchantCity: header?.merchantCity ?? "", merchantMcc: getTextValueString(index: .mcc) , tag62First: getTextValueString(index: .storeId), tag62Second: getTextValueString(index: .terminalId), merchantId: header?.merchantId ?? "" , pin: pin, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    if response.data?.payWithSaveCard == true{
                        let paymentVc = PaymentMetodViewController()
                        paymentVc.paymentData = response
                        self.navigationController?.pushViewController(paymentVc, animated: true)
                    }else{
                        self.loadSDK(with: response)
                    }
                    
                }else{
                    self.showToastError(message:response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    func getTextValueString(index: FieldType)->String{
        return arrayOfTextFields?.first(where: {$0.index == index})?.textValue ?? ""
    }
    func loadSDK(with response: QrPaymentResponseModel) {
        if response.code == 200{
            if let ssldata = response.data, let storeid = ssldata.storeID , let storePass = ssldata.storePassword , let transId = ssldata.transactionID{
                
                let integrationInfo =  IntegrationInformation(storeID: storeid, storePassword: storePass, totalAmount: Double(ssldata.amount ?? "0.0") ?? 0.0, currency: "BDT", transactionId: transId, productCategory: ssldata.serviceTitle ?? "")
                if let ipnUrl = ssldata.ipnURL{
                    integrationInfo.ipnURL = ipnUrl
                }
                sslCommerz = SSLCommerz.init(integrationInformation: integrationInfo)
                
                sslCommerz?.delegate = self
                sslCommerz?.customerInformation = .init(customerName: EasyDataStore.sharedInstance.user?.name ?? "", customerEmail: EasyDataStore.sharedInstance.user?.name ?? "", customerAddressOne: EasyDataStore.sharedInstance.user?.address ?? "", customerCity: "dhk", customerPostCode: "1234", customerCountry: "BD", customerPhone: EasyDataStore.sharedInstance.user?.mobile ?? "")
                if let refer = ssldata.userRefer{
                    sslCommerz?.customerInformation?.userRefer = refer
                }
                
                sslCommerz?.additionalInformation = .init()
                sslCommerz?.additionalInformation?.paramB = ssldata.valueB
                sslCommerz?.additionalInformation?.paramC = ssldata.valueC
                sslCommerz?.start(in: self, shouldRunInTestMode: Constant.isDev)
            }
            else{
                self.showToastError(message:response.message ?? "")
            }
        }else{
            self.showToastError(message:response.message ?? "")
        }
    }
    
    func qrPaymentInit(transId: String){
        WebServiceHandler.shared.qrPaymentInit(transactionId: transId, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.showTransactionSuccess(message: response.message ?? "", transactionId: transId)
                }else{
                    
                    self.showTransactionFailed(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    
    func showTransactionSuccess(message: String,transactionId: String) {
        
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "go_home".easyLocalized(), tag: 1001)
        let btn = PopupButton (type: .bordered, title: "make_another_payment".easyLocalized(), tag: 1002)
        popup.options = [.image(image: .success),.title(str:   "qr_payment_successful".easyLocalized(),color:.trueGreen),.subtitle(str: message,color: .slateGrey),.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
    }
    
    func showTransactionFailed(message:String) {
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "go_home".easyLocalized(), tag: 1001)
        let btn = PopupButton (type: .bordered, title: "make_another_payment".easyLocalized(), tag: 1002)
        popup.options = [.image(image: .failed),.title(str:  "qr_payment_failed".easyLocalized(),color:.tomato),.subtitle(str: message,color: .slateGrey),.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
    }
    
    
}




extension QRDetailsVC : EasyPopUpDelegate{
    func errorAlert(){
        
    }
    
    func popupDismissedWith(button: PopupButton, consent: Bool?, rating: Int?) {
        if button.tag == 1001{
            self.navigationController?.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: Notification.Name("ShowQR"), object: nil)
            })
        }
    }
}
extension QRDetailsVC : PINVCDelegate{
    func didEnter(pin: String) {
        qrPaymentCreate(pin: pin)
    }
}

extension QRDetailsVC : SSLCommerzDelegate {
    func sdkClosed(reason: String) {
        
    }
    func transactionCompleted(withTransactionData transactionData: SSLCommerzSDK.TransactionDetails?) {
        if(transactionData?.status?.lowercased() == "valid" || transactionData?.status?.lowercased() == "validated" ){
            debugPrint(transactionData?.tran_id ?? "")
            qrPaymentInit(transId: transactionData?.tran_id ?? "")
        }else{
            showTransactionFailed(message:"transaction_failed".easyLocalized())
        }
    }
}

class PaymentInfoItemView: UIView, UITextFieldDelegate{
    lazy var fieldName : UILabel =  {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .slateGrey
        label.font = .EasyFont(ofSize: 14, style: .regular)
        return label
    }()
    lazy var infoTextField : TextField =  {
        let textField = TextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        textField.borderStyle = .none
        
        return textField
    }()
    
    init(textInfo: TextFieldModel) {
        super.init(frame: .zero)
        
        
        // self.addSubview(fieldName)
        self.addSubview(infoTextField)
        infoTextField.text = textInfo.textValue == "0.0" ? "" : textInfo.textValue
        infoTextField.isEnabled = textInfo.isEditable ?? false
        infoTextField.tag = textInfo.index?.rawValue ?? 0
        infoTextField.placeholder = String(format: "enter_amount_qr".easyLocalized(), textInfo.textName ?? "")
        //String(format: by_checking_this_box_for_recharge[index],bonusAmount
        infoTextField.textColor = .black
        infoTextField.font = .EasyFont(ofSize: 14, style: .medium)
        infoTextField.backgroundColor = textInfo.isEditable == true ? .clear : .gray
        
        
        infoTextField.keyboardType = textInfo.index?.keyboardType ?? .default
        infoTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5).isActive = true
        infoTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5).isActive = true
        infoTextField.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        infoTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        
        infoTextField.layer.borderWidth = 0.5
        infoTextField.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        infoTextField.layer.cornerRadius = 10
        infoTextField.clipsToBounds = true
        infoTextField.backgroundColor = .paleBG
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == FieldType.amount.rawValue {
            
        }
    }
}

class PaymentInfoLabelView: UIView{
    var textInfo: TextFieldModel!
    lazy var lblKey: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .EasyFont(ofSize: 14, style: .regular)
        label.textColor = .slateGrey
        label.text = textInfo.textName
        
        return label
    }()
    lazy var lblValue: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = textInfo.index == .amount ? .EasyFont(ofSize: 16, style: .bold) : .EasyFont(ofSize: 14, style: .bold)
        label.numberOfLines = 1
        label.textAlignment = .right
        label.textColor = textInfo.index == .amount ? .blueViolet : .black
        label.text =  textInfo.index?.infoType == .amount ?  "\(Constant.taka) \(String(format: "%.2f", Double(textInfo.textValue ?? "") ?? 0.0)  )" : textInfo.textValue
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(textInfo: TextFieldModel) {
        self.init(frame: .zero)
        self.textInfo = textInfo
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 10).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -10).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        
        
        
        stackView.addArrangedSubview(lblKey)
        stackView.addArrangedSubview(lblValue)
        
        
        
        
        dashedView.backgroundColor = .white
        dashedView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(dashedView)
        dashedView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        dashedView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        dashedView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        dashedView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    let dashedView = UIView()
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        dashedView.createDottedLine(width: 1.0, color: UIColor.blueyGrey.cgColor)
    }
}

extension UIView{
    func addConstraintWithFormat(_ format : String, views : UIView...) {
        
        var viewsDictionary = [String : UIView]()
        
        for(index, view) in views.enumerated(){
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
extension QRDetailsVC : KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        var contentInset:UIEdgeInsets = self.paymentInfoScrollView.contentInset
        contentInset.bottom = height + 20
        paymentInfoScrollView.contentInset = contentInset
    }
    
    func keybaordWillHide() {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        paymentInfoScrollView.contentInset = contentInset
    }
}





class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
