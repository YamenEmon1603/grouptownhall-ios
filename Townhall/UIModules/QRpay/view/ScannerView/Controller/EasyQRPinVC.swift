//
//  EasyQRPinVC.swift
//  Easy.com.bd
//
//  Created by Raihan on 2/28/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation

import UIKit
protocol PINVCDelegate{
    func didEnter(pin:String)
}
class EasyQRPinVC: EasyBaseViewController, KeyboardObserverProtocol, ButtonAccessoryViewDelegate {
   
    
   
    var delegate :PINVCDelegate?
    var keyboardObserver: KeyboardObserver!
    @IBOutlet weak var lblEnterPin: UILabel!
    @IBOutlet weak var lblpaymentSecurity: UILabel!
    static let storyboardId = "PINVC"
    @IBOutlet var pinTextField: [UITextField]!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    var borders : [UIView]?
    override func viewDidLoad() {
        super.viewDidLoad()
        borders = [UIView]()
       keyboardObserver = KeyboardObserver(for: self)
        pinTextField.first?.becomeFirstResponder()
        pinTextField.forEach { (tf) in
            tf.inputAccessoryView = ButtonAccessoryView(title: "done".easyLocalized(), delegate: self)
        }
    
        setLanguageText()
    }
    
    func setLanguageText(){
        lblEnterPin.text = "enter_pin_number".easyLocalized()
        lblpaymentSecurity.text = "in_order_to_payment".easyLocalized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
      
    }
    
    @IBAction func closeButonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyboardWillShow(with height: CGFloat) {
        viewHeight.constant = height+200
    }
    
    func keybaordWillHide() {
        
    }
   
    

}
extension EasyQRPinVC:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
  /*      print("STARTED")
        let tag = textField.tag
        if let border = borders?.first(where: {$0.tag == tag}){
            border.backgroundColor = .warmPurple
        }else{
            let border = textField.addBorderWithRetrun(side: .bottom, color: .warmPurple, width: 2,leftPadding: 3)
            border.layer.cornerRadius = 10.0
            border.tag = tag
            borders?.append(border )
        }*/
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        /*   if let border = borders?.first(where: {$0.tag == textField.tag}){
            border.backgroundColor = .white
        }*/
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var shouldProcess = false //default to reject
        var shouldMoveToNextField = false //default to remaining on the current field
        
        let insertStringLength = string.count
       
        if insertStringLength == 0 {
            //backspace
            textField.text = ""

            shouldProcess = false //Process if the backspace character was pressed
        } else {
            if textField.text?.count == 0 {
                shouldProcess = true //Process if there is only 1 character right now
            }
        }
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                let prevTag = textField.tag - 1
                print(prevTag)
                if let prevResponder =  textField.superview?.superview?.viewWithTag(prevTag)?.subviews.first(where: {$0 is UITextField}){
                    prevResponder.becomeFirstResponder()
//                    btnProceed.backgroundColor  = .blueyGrey
//                    fakeBgView.backgroundColor = .blueyGrey
                } else {
//                    btnProceed.backgroundColor  = .warmPurple
//                    fakeBgView.backgroundColor = .warmPurple
                    
                    //return NO;
                }
            }
        }
        
        //here we deal with the UITextField on our own
        if var mstring = textField.text, shouldProcess {
            //grab a mutable copy of what's currently in the UITextField
           
            if mstring.count == 0 {
                //nothing in the field yet so append the replacement string
                mstring += string
                
                shouldMoveToNextField = true
            } else {
                //adding a char or deleting?
                if insertStringLength > 0 {
                    mstring.insert(contentsOf: string, at: string.index(string.startIndex, offsetBy: range.location))
                } else {
                    //delete case - the length of replacement string is zero for a delete
                    if let subRange = Range<String.Index>(range, in: mstring) { mstring.removeSubrange(subRange) }
                }
                
            }
            
            //set the text now
            textField.text = mstring
           
            if shouldMoveToNextField {
                let nextTag = textField.tag + 1
                let nextResponder =  textField.superview?.superview?.viewWithTag(nextTag)?.subviews.first(where: {$0 is UITextField})//textField.superview?.viewWithTag(nextTag)
                if nextResponder != nil {
                    nextResponder?.becomeFirstResponder()
//                    btnProceed.backgroundColor  = .blueyGrey
//                    fakeBgView.backgroundColor = .blueyGrey
                    //return YES;
                } else {
//                    btnProceed.backgroundColor  = .warmPurple
//                    fakeBgView.backgroundColor = .warmPurple
//                    view.endEditing(true)
                    //return NO;
                }
            }
        }
        
        return false
    }
    func tapAction(_ sender: ButtonAccessoryView) {
        var otp = ""
        var hasEnteredOTP = true
        
        pinTextField.sort { (textField0, textField1) -> Bool in
            textField0.tag < textField1.tag
        }
        
        for textField in pinTextField{
            
            guard let otpDigit = textField.text, !otpDigit.isEmpty else {
                hasEnteredOTP = false
                break
            }
            
            otp.append(otpDigit)
        }
        if hasEnteredOTP{
            self.dismiss(animated: true) {
                self.delegate?.didEnter(pin: otp)
            }
           
        }else{
            view.endEditing(true)
            showToast(message: "invalid_pin".easyLocalized())
           
           
        }
    }
}

