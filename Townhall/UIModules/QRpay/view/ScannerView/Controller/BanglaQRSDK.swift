//
//  BanglaQRSDK.swift
//  BanglaQrSDK
//
//  Created by Yamen Emon on 14/1/21.
//

import Foundation
import UIKit
public class BanglaQR {
    public init() {
        
    }
    public func startSDK(controller:UIViewController,mainModel:BanglaQRInitialization,sdkDelegate:QrScannerDelegate){
        let initializer = mainModel
        let scannerController = ScannerController(qrInitializer: initializer)
        scannerController.isFlashButtonHidden = false
        scannerController.scanPayLabelText = "Scan and Pay"
        scannerController.scannerDelegate = sdkDelegate
       // let navController = UINavigationController.init(rootViewController: scannerController)
        controller.navigationController?.pushViewController(scannerController, animated: true)
    }
}
