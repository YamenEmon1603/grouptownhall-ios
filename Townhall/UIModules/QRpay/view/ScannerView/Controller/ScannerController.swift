//
//  ScannerController.swift
//  BanglaQR
//
//  Created by Yamen Emon on 1/11/20.
//

import UIKit
import AVFoundation
 public protocol QrScannerDelegate: class {
    func cancelCallBack(_ message:String)
    func failCallBack(_ message: String)
    func successCallBack(_ payloadModel:[[String:String]])
}


class ScannerController: EasyBaseViewController,QRViewProtocol {
    var presenter: QRPresenterProtocol?
    
    
    
    private var qrScannerView: QRScannerView! {
        didSet {
            qrScannerView.configure(delegate: self, input: .init(isBlurEffectEnabled: true))
        }
    }
    var isFlashButtonHidden = false
    var scanPayLabelText = ""
    var scannerDelegate : QrScannerDelegate?
        
    //MARK: - PRIVATE PROPERTY
    private var flashButton: FlashButton!
    private var closeButton : UIButton!
    private var scanPayLabel : UILabel!
    private var imageArr = ["visa","mastercard","unionpay","americanexpress","visa","mastercard","unionpay","americanexpress"]
    private var vendorScroller : UIScrollView?
    private let tagObject = ["26","27","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","62","88"]
    private var emvParserModels = [EMVParserModel]()
    private var childEmvParserModels = [EMVParserModel]()
    private var mainModel : BanglaQRInitialization!
    private var globalPayload : String = ""
    
    //MARK: - REQUIRED INIT METHOD

    required convenience init() {
        self.init(qrInitializer:nil)
    }
    
    init(qrInitializer:BanglaQRInitialization?) {
        if let data = qrInitializer {
            self.mainModel = data
            super.init(nibName: nil, bundle: nil)
        }
        else{
            fatalError("You have not initialize qr Initializer properly") //CACEL CALL BACK
        }
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func failCallBack(_ message: String) {
        if let _delegate = scannerDelegate {
            _delegate.failCallBack(message)
        }
    }
    
    class func startSDK(controller:UIViewController,mainModel:BanglaQRInitialization,sdkDelegate:QrScannerDelegate){
        let initializer = mainModel
        let scannerController = ScannerController(qrInitializer: initializer)
        scannerController.isFlashButtonHidden = false
        scannerController.scanPayLabelText = "scan_qr_to_pay2".easyLocalized()
        scannerController.scannerDelegate = sdkDelegate
        let navController = UINavigationController.init(rootViewController: scannerController)
        navController.modalPresentationStyle = .fullScreen
        controller.present(navController, animated: true, completion: nil)
    }
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        qrScannerView = QRScannerView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        view.addSubview(qrScannerView)
        
        let topView = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height*0.3))
        view.addSubview(topView)
        topView.image = UIImage(named: "qrTopView")
        
        let qrImageView = UIImageView(frame: CGRect(x: (self.view.frame.size.width - 60)*0.5, y: 49, width: 60, height: 60))
        qrImageView.image = UIImage(named: "qrImage")
        view.addSubview(qrImageView)
        
        
        scanPayLabel = UILabel(frame: CGRect(x: (self.view.frame.size.width - (self.view.frame.size.width*0.60))*0.5, y: qrImageView.frame.origin.y + qrImageView.frame.size.height, width: self.view.frame.size.width*0.60, height: 40))
        scanPayLabel.text = scanPayLabelText
        scanPayLabel.textColor = .white
        scanPayLabel.font = .EasyFont(ofSize: 15, style: .bold)
        scanPayLabel.textAlignment = .center
        scanPayLabel.numberOfLines = 2
        scanPayLabel.adjustsFontSizeToFitWidth =  true
        view.addSubview(scanPayLabel)
        
        
        closeButton = FlashButton(frame: CGRect(x: 5, y: 30, width: 60, height: 60))
        closeButton.setImage(UIImage.imageFromBundle(ScannerController.self, imageName: "icback"), for: .normal)
        view.addSubview(closeButton)
        closeButton.addTarget(self, action: #selector(tapCloseButton(_:)), for: .touchUpInside)
        view.bringSubviewToFront(closeButton)
        closeButton.tintColor = .white
        
        flashButton = FlashButton(frame: CGRect(x: self.view.frame.size.width - 50/*closeButton.frame.origin.x - 60*/, y: 49, width: 40, height: 40))
        flashButton.setImage(UIImage.imageFromBundle(ScannerController.self, imageName: "path"), for: .normal)
        view.addSubview(flashButton)
        flashButton.addTarget(self, action: #selector(tapFlashButton(_:)), for: .touchUpInside)
        view.bringSubviewToFront(flashButton)
        flashButton.tintColor = .white
        flashButton.isHidden = isFlashButtonHidden
        
        let bottomView = UIImageView(frame: CGRect(x: 0, y: view.frame.size.height*0.7, width: view.frame.size.width, height: view.frame.size.height*0.3))
        view.addSubview(bottomView)
        bottomView.image = UIImage(named: "qrBottomView")
        
        
        let callNumberBtnView = UIButton(frame: CGRect(x: 50, y: view.frame.size.height*0.8, width: view.frame.size.width - 100, height: 50))
        view.addSubview(callNumberBtnView)
        callNumberBtnView.setTitle("calleeNumber".easyLocalized(), for: .normal)
        callNumberBtnView.setImage(UIImage(named: "callImage"), for: .normal)
        callNumberBtnView.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        callNumberBtnView.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: .normal)
        callNumberBtnView.titleLabel?.font = .EasyFont(ofSize: 18, style: .bold)
        callNumberBtnView.addTarget(self, action: #selector(dialNumber), for: .touchUpInside)
        
        let descripTitle = UILabel(frame: CGRect(x: (view.frame.size.width - view.frame.size.width/2)*0.5, y: callNumberBtnView.frame.origin.y + callNumberBtnView.frame.size.height, width: view.frame.size.width/2, height: 60))
        descripTitle.text = "description_text".easyLocalized()
        view.addSubview(descripTitle)
        descripTitle.font = .EasyFont(ofSize: 14, style: .regular)
        descripTitle.textColor = .hazel
        descripTitle.textAlignment = .center
        descripTitle.numberOfLines = 3
        descripTitle.sizeToFit()
        descripTitle.frame = CGRect(x: (view.frame.size.width - view.frame.size.width/1.55)*0.5, y: callNumberBtnView.frame.origin.y + callNumberBtnView.frame.size.height, width: view.frame.size.width/1.55, height: descripTitle.frame.size.height)
        
        
        /*
        if imageArr.count > 0 {
            
            let vendorContainer = UIView(frame: CGRect(x: 0, y: view.frame.size.height - 150, width: view.frame.size.width - 20, height: 100))
            view.addSubview(vendorContainer)
            
            
            vendorScroller = UIScrollView(frame: CGRect(x: 0, y: 0, width: vendorContainer.frame.size.width, height: vendorContainer.frame.size.height))
            vendorContainer.addSubview(vendorScroller!)
            vendorScroller?.showsHorizontalScrollIndicator = false
            
            var x = 40
            for items in imageArr {
                if !items.isEmpty {
                    if UIImage.imageFromBundle(QRScannerView.self, imageName: items) != nil {
                        let imageView = UIImageView(frame: CGRect(x: x, y: 0, width: 60, height: 60))
                        imageView.image = UIImage.imageFromBundle(ScannerController.self, imageName: items)
                        imageView.contentMode = .scaleAspectFit
                        vendorScroller?.addSubview(imageView)
                    }
                    x += 80
                    
                    vendorScroller?.contentSize = CGSize(width: x, height: 100)
                }
            }
        }
        */
    }
    
    @objc func dialNumber() {
        
        if let url = URL(string: "tel://\(09612222777)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        qrScannerView.stopRunning()
        scannerDelegate?.cancelCallBack("User dismiss the controller")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        qrScannerView.startRunning()
    }
    
    // MARK: - Actions
    @objc func tapFlashButton(_ sender: UIButton) {
        qrScannerView.setTorchActive(isOn: !sender.isSelected)
    }
    
    @objc func tapCloseButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            if let _delegate = self.scannerDelegate {
                _delegate.failCallBack("User cancelled the scanner")
            }
        })
        
    }
    func Alertfailed(message: String){
        let alertPrompt = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "scan_again".easyLocalized(), style: UIAlertAction.Style.cancel, handler: {_ in
            self.qrScannerView.configure(delegate: self, input: .init(isBlurEffectEnabled: true))
            self.qrScannerView.startRunning()
        })
        alertPrompt.addAction(cancelAction)
        present(alertPrompt, animated: true, completion: nil)
    }
    
}

// MARK: - QRScannerViewDelegate
extension ScannerController: QRScannerViewDelegate {
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError, cameraPermissionStatus: AuthorizationStatus) {
        if cameraPermissionStatus != .notDetermined{
            self.dismiss(animated: true) {
                self.scannerDelegate?.failCallBack("\(error.localizedDescription)")
            }
        }else{
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                    if response {
                        //access granted
                    } else {
                        DispatchQueue.main.async {
                            self.dismiss(animated: true) {
                                self.scannerDelegate?.failCallBack("User Dined Camera Permisson")
                            }
                        }
                        
                    }
                }
        }
    }

    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
        self.dismiss(animated: true) {
            self.scannerDelegate?.failCallBack("\(error.localizedDescription)")
        }
    }
    
    func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
        launchApp(payload: code)
    }
    
    func qrScannerView(_ qrScannerView: QRScannerView, didChangeTorchActive isOn: Bool) {
        flashButton.isSelected = isOn
    }
}

extension ScannerController {
    
    private func launchApp(payload: String) {
        
        if presentedViewController != nil {
            return
        }
        debugPrint("DECODED DATA : \(payload)")
        var matchingStatus = ""
        if (payload.count >= 4) {
            let lastFourDigit = payload.suffix(4)
            let dateSplitter1 = payload.components(separatedBy: lastFourDigit)
            let forCRCCheckPayload = dateSplitter1[0]
            debugPrint(forCRCCheckPayload)
            let array: [UInt8] = Array(forCRCCheckPayload.utf8)
            
            let crcData = crc16Java(array)
            debugPrint("crc DATA : \(crcData)")
            let convertedData = String(format: "%04X", crcData)
            debugPrint("CONVERTED DATA : \(convertedData)")
            
            if convertedData == lastFourDigit {
                matchingStatus = "CRC matched"
                globalPayload = payload
                cutPayload(payloads: payload)
                
            }
            else{
                qrScannerView.stopRunning()
                
                matchingStatus = "crc_not_matched".easyLocalized()
                let alertPrompt = UIAlertController(title: "crc_status".easyLocalized(), message: matchingStatus, preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "scan_again".easyLocalized(), style: UIAlertAction.Style.cancel, handler: {_ in
                    self.qrScannerView.configure(delegate: self, input: .init(isBlurEffectEnabled: true))
                    self.qrScannerView.startRunning()
                })
                let defaultAction = UIAlertAction(title: "cancel".easyLocalized(), style: UIAlertAction.Style.destructive, handler: {_ in
                    self.dismiss(animated: true, completion: {
                        if let _delegate = self.scannerDelegate {
                            _delegate.failCallBack("CRC not matched,so user cancelled")
                        }
                    })
                })
                alertPrompt.addAction(cancelAction)
                alertPrompt.addAction(defaultAction)
                present(alertPrompt, animated: true, completion: nil)
            }
        }else{
        
            showToastError(message: "invalid_QR".easyLocalized())
            EasyUtils.shared.delay(1.0) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Helper methods
    private func crc16Java(_ arrayData: Array<UInt8>, seed: UInt16? = nil) -> UInt16 {
        var crc = 0xFFFF
        
        for (index,_) in arrayData.enumerated() {
            crc = ((crc  >> 8) | (crc  << 8) ) & 0xffff;
            crc ^= (Int(arrayData[index]) & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return UInt16(crc)
    }
    //MARK: - CUT PAYLOAD
    private func cutPayload(payloads:String){
        DispatchQueue.main.async {
            var payload = payloads
            let tag = payload.substring(0..<2)
            let tagLength =  payload.substring(2..<4)
            
            guard tagLength.count > 0 else {
                return
            }
            var len = 4
            if let lenght = NSInteger(tagLength){
                len = len + lenght
                let tagValue = payload.substring(4..<len)
                let emvParserModel =  EMVParserModel(tag: tag, length: lenght, value: tagValue, childEMVParserModel: [])
                
                debugPrint("tag \(tag) -> taglenght \(tagLength) -> tagValue -> \(tagValue)")
                
                if self.tagObject.contains(tag) {
                    if tagValue.isEmpty == false {
                        self.childEmvParserModels.removeAll()
                        self.cutChildPayload(parentEmvParserModel: emvParserModel, payload: tagValue)
                    }
                }
                else {
                    self.emvParserModels.append(emvParserModel)
                }
            }
            payload = payload.substring(len..<payload.count)
            if payload.isEmpty == false {
                self.cutPayload(payloads: payload)
            }
            else {
                self.qrScannerView.stopRunning()
                let modelData = self.emvParserModels
                let parser = PayloadParser(emvModels: modelData, mainModel: self.mainModel, payloadString: self.globalPayload)
                let data = parser.parseQRModelData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    if data?.header.isTagValueMatched == true{
                        let detailVC =  QRDetailsVC()
                        detailVC.scannerDelegate = self.scannerDelegate
                        detailVC.header = data?.header
                        detailVC.arrayOfTextFields = data?.textFields
                        detailVC.qrInitializerModel = self.mainModel
                        self.navigationController?.pushViewController(detailVC, animated: true)
                    }else{
                        self.showToastError(message: "invalid_merchant_id".easyLocalized())
                        EasyUtils.shared.delay(1.0) {
                            self.dismiss(animated: true, completion: nil)
   
                        }
                        
                    }
                }
                
                self.emvParserModels.removeAll()
                payload = ""
            }
        }
    }
    
    private func cutChildPayload(parentEmvParserModel: EMVParserModel, payload: String) {
        
        var payload = payload
        let tag = payload.substring(0..<2)
        let tagLength =  payload.substring(2..<4)
        
        guard tagLength.count > 0 else {
            return
        }
        
        var len = 4
        
        if let lenght = NSInteger(tagLength){
            len = len + lenght
            let tagValue = payload.substring(4..<len)
            
            let emvParserModel = EMVParserModel(tag: tag, length: lenght, value: tagValue, childEMVParserModel: [])
            childEmvParserModels.append(emvParserModel)
            
            debugPrint("                                     \(tag) -> \(tagLength) -> \(tagValue)")
            payload = payload.substring(len..<payload.count)
            if !payload.isEmpty {
                cutChildPayload(parentEmvParserModel: parentEmvParserModel, payload: payload)
            } else {
                var tempModel = parentEmvParserModel
                tempModel.childEMVParserModel = childEmvParserModels
                emvParserModels.append(tempModel)
            }
        }
    }
    
}

extension String{
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
}
