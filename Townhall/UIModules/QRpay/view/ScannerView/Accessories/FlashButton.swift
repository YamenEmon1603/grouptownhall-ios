//
//  FlashButton.swift
//  QRScannerSample
//
//  Created by daichiro on 2019/11/08.
//  Copyright © 2019 mercari.com. All rights reserved.
//

import UIKit

final class FlashButton: UIButton {
    // MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        settings()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        settings()
    }
}

// MARK: - Private
private extension FlashButton {
    func settings() {
        titleLabel?.font = .boldSystemFont(ofSize: 16)
        isSelected = false
    }
}
