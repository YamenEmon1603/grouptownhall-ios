//
//  ArektaClass.swift
//  BanglaQR
//
//  Created by Raihan on 1/7/21.
//

import Foundation

class PayloadParser{
    let mainModel: BanglaQRInitialization
    let emvModels:[EMVParserModel]
    var cardModel = HeaderModel()
    var textfieldsList : [TextFieldModel]
   
    init(emvModels:[EMVParserModel],mainModel: BanglaQRInitialization,payloadString:String) {
        self.emvModels = emvModels
        self.mainModel = mainModel
        
        self.textfieldsList = []
        FieldType.allCases.forEach { (type) in
            let model = TextFieldModel()
            model.index = type
            model.isEditable = false
            model.isVisible = false
            model.textName = type.textName
            model.textValue = ""
            textfieldsList.append(model)
        }
        if let index = fetchFrom(dataList: textfieldsList, type: .payload){
            textfieldsList[index].textValue = payloadString
        }
    }
    
    private  func fetchFrom(dataList:[TextFieldModel],type:FieldType)->Int?{
        if let index =   dataList.firstIndex(where: {$0.index == type}){
            return index
        }
        return nil
    }
   
    
    func parseQRModelData() -> (header: HeaderModel, textFields: [TextFieldModel])?{

        
        emvModels.forEach { (model) in
            switch(model.tag){
            case "01":
                if let index = fetchFrom(dataList: textfieldsList, type: .amount){
                    textfieldsList[index].isVisible = true
                    switch model.value {
                    case "11":
                        //Amount Field is enabled
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = "0.0"
                        break
                    case "12":
                        //Amount Field is disabled
                        textfieldsList[index].isEditable = false
                        if  let amountStr = emvModels.first(where:{$0.tag == "54"} )?.value, let amount = Double(amountStr){
                            debugPrint(amount)
                            textfieldsList[index].textValue = amountStr
                        }else{
                            debugPrint("Amount not found")
                            textfieldsList[index].textValue = "0.0"
                            
                        }
                        
                        
                    default:
                        break
                    }
                }
            case "04":
                if let lastbit = calculateCheckDigit(card: model.value){
                    cardModel.merchantPanId = model.value+lastbit
                }
                
            case "02":
                if(model.value.hasPrefix("402004")){
                    cardModel.merchantPanId = model.value
                    cardModel.isTagValueMatched = true
                }else{
                    cardModel.isTagValueMatched = false
                }
               
            case "12":
                if model.value.count == 28{
                    let tempVal = model.value.substring(0..<model.value.count-11)
                    let amexCardValue = tempVal.substring(0..<tempVal.count-1)
                    cardModel.merchantPanId = amexCardValue
                }
                
            case "15":
                if (model.value.count == 31) {
                    let unionPayval = model.value.substring(0..<model.value.count - 15)
                    cardModel.merchantPanId = unionPayval
                }
            case "28" :
                cardModel.merchantId = model.value
                
//            case "04":
//                if mainModel.fundingSource == FundingSource.MASTERCARD{
//                    cardModel.isTagValueMatched = true
//                    if let lastbit = calculateCheckDigit(card: model.value){
//                        cardModel.merchantPanId = model.value+lastbit
//                    }
//
//                    //Handle MasterCard
//                }
//            case "02":
//                if mainModel.fundingSource == FundingSource.VISA{
//                    cardModel.isTagValueMatched = true
//                    //Handle Visa
//                    cardModel.merchantPanId = model.value
//                }
//            case "12":
//                if mainModel.fundingSource == FundingSource.AMEX{
//                    cardModel.isTagValueMatched = true
//                    if model.value.count == 28{
//                        let tempVal = model.value.substring(0..<model.value.count-11)
//                        let amexCardValue = tempVal.substring(0..<tempVal.count-1)
//                        cardModel.merchantPanId = amexCardValue
//                    }else{
//                        cardModel.isTagValueMatched = false
//                    }
//                }
//            case "15":
//                if mainModel.fundingSource == FundingSource.UNIONPAY{
//                    cardModel.isTagValueMatched  = true
//                    if (model.value.count == 31) {
//                        let unionPayval = model.value.substring(0..<model.value.count - 15)
//                        cardModel.merchantPanId = unionPayval
//                    } else {
//                        cardModel.isTagValueMatched  = false
//                    }
//                }
//            case "28" :
//                if mainModel.fundingSource == FundingSource.CASA{
//                    cardModel.isTagValueMatched  = true
//                    cardModel.merchantPanId = model.value //
//                }
            case "55":
                // tipConveyance visible
                if let index = fetchFrom(dataList: textfieldsList, type: .tipConvenseAmount){
                    textfieldsList[index].isVisible = true
                    switch model.value {
                    case "01":
                        //Tip field enabled
                        textfieldsList[index].textName = "Tip"
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = "0.0"
                        break
                    case "02":
                        //Tip field disabled
                        textfieldsList[index].textName = "Convenience Fee"
                        textfieldsList[index].isEditable = false
                        
                        if  let amountStr = emvModels.first(where:{$0.tag == "56"} )?.value, let amount = Double(amountStr){
                            debugPrint(amount)
                            textfieldsList[index].textValue = amountStr
                        }else{
                            debugPrint("Amount not found")
                            textfieldsList[index].textValue = "0.0"
                        }
                    case "03":
                        //Tip field disabled
                        textfieldsList[index].isEditable = false
                        if let j = fetchFrom(dataList: textfieldsList, type: .tipConvensePercentage){
                            if  let percentage = emvModels.first(where:{$0.tag == "57"} )?.value, let amount = Double(percentage){
                                textfieldsList[index].textName = "Convenience Fee \(percentage)%"
                                textfieldsList[j].isVisible = true
                                textfieldsList[j].textValue = percentage
                                debugPrint(amount)
                            }else{
                                debugPrint("Amount not found")
                            }
                        }
                        
                    default:
                        break
                    }
                }
                
            case "52":
                
                //  qrResponse.mcc = model.value
                if let index = fetchFrom(dataList: textfieldsList, type: .mcc){
                    textfieldsList[index].isVisible = false
                    textfieldsList[index].textValue = model.value
                }
                debugPrint(model.value)
            case "59":
                //merchantName = model.value
                debugPrint("Merchant Name",model.value)
                cardModel.merchantName = model.value
            case "60":
                //merchant city
                debugPrint("Merchant City",model.value)
                cardModel.merchantCity = model.value
            case "62":
                //Additional Info
                handleAddtionalQRModelData(addtionalModel: model)
                break
            default:
                break
            }
        }
        debugPrint(cardModel)
        debugPrint(textfieldsList)
        return (cardModel,textfieldsList)
       
    }
    
    private func handleAddtionalQRModelData(addtionalModel:EMVParserModel){
        addtionalModel.childEMVParserModel.forEach { (emv) in
            switch emv.tag{
            case "01":
                if let index = fetchFrom(dataList: textfieldsList, type: .billNumber){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
            case "02":
                if let index = fetchFrom(dataList: textfieldsList, type: .mobileNumber){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
            case "03":
                if let index = fetchFrom(dataList: textfieldsList, type: .storeId){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
            case "04":
                if let index = fetchFrom(dataList: textfieldsList, type: .loyaltyNumber){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
            case "05":
                if let index = fetchFrom(dataList: textfieldsList, type: .referenceId){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
            case "06":
                if let index = fetchFrom(dataList: textfieldsList, type: .customerId){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
            case "07":
                if let index = fetchFrom(dataList: textfieldsList, type: .terminalId){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
            case "08":
                if let index = fetchFrom(dataList: textfieldsList, type: .purpose){
                    textfieldsList[index].isVisible = true
                    if emv.value == "***"{
                        textfieldsList[index].isEditable = true
                        textfieldsList[index].textValue = ""
                    }else{
                        textfieldsList[index].textValue = emv.value
                        textfieldsList[index].isEditable = false
                    }
                }
                
            default:
                break
            }
        }
    }
    
    func calculateCheckDigit(card: String?) -> String? {
        guard let  cardval = card else{
            return nil
        }

        var digit: String
        /* convert to array of int for simplicity */
       
        var digits :[Int] = []
//        for (i, in card.indices) {
//            digits[i] = Character.getNumericValue(card[i])
//        }
        var index = 0
        cardval.forEach { (char) in
            if let int = Int(String(char)){
                digits.append(int)
                index += 1
            }
           
        }
        /* double every other starting from right - jumping from 2 in 2 */
//        run {
            var i = digits.count - 1
            while (i >= 0) {
                digits[i] += digits[i]

                /* taking the sum of digits grater than 10 - simple trick by substract 9 */
                if (digits[i] >= 10) {
                    digits[i] = digits[i] - 9
                }
                i -= 2
            }
//        }
        var sum = 0
        for (_,item) in digits.enumerated() {
            sum += item
        }
        /* multiply by 9 step */
        sum *= 9

        /* convert to string to be easier to take the last digit */
        digit = String(sum)  + ""
        return String(digit.suffix(1))
    }
   
}

