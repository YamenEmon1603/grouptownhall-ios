//
//  BanglaQRInitialization.swift
//  BanglaQR
//
//  Created by Yamen Emon on 1/11/20.
//

import Foundation

public enum FundingSource :Int {
    
    case VISA, MASTERCARD, AMEX, UNIONPAY, CASA
    func fundingSourceName() -> String{
        return "\(self)"
    }
}

public struct BanglaQRInitialization {
    
    var fundingSource : FundingSource
    var primaryColor : String = "346DF6"
    var secondaryColor : String = "F0F0F0"
    
   public init(fundingSource : FundingSource, primaryColor : String = "346DF6",secondaryColor : String = "F0F0F0") {
        self.fundingSource = fundingSource
        self.primaryColor = primaryColor
        self.secondaryColor = secondaryColor
    }
}


