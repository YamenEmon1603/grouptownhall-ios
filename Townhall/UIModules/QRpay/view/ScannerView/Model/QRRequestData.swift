//
//  QRRequestData.swift
//  Easy.com.bd
//
//  Created by Raihan on 2/28/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
enum MerchantInfo:Int,CaseIterable{
    case merchantName,merchantCity,storeLabel,terminalLabel
    var stringValue : String{
        switch self {
        case .merchantName:
            return "Merchant Name"
        case .merchantCity:
            return "Merchant City"
        case .storeLabel:
            return "Store Label"
        case .terminalLabel:
            return "Terminal Label"
        }
       
    }
}

class QRRequestData : Encodable {
    var merchantName : String = ""
    var merchantCity: String = ""
    var merchantId : String  = ""
    var mcc : String = ""
    var storeLabel: String = ""
    var terminalLabel: String = ""
    var payoad: String = ""
    var amount : String = ""
    var merchantPanId : String = ""
    
    enum CodingKeys: String, CodingKey {
        case merchantName = "merchant_name"
        case merchantCity = "merchant_city"
        case mcc
        case merchantId = "merchant_id"
        case storeLabel = "tag_62_first_data"
        case terminalLabel = "tag_62_second_data"
        case payload = "qr_data"
        case amount
        case merchantPanId = "merchant_pan_id"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(merchantName, forKey: CodingKeys.merchantName)
        try container.encode(merchantCity, forKey: CodingKeys.merchantCity)
        try container.encode(merchantId, forKey: CodingKeys.merchantId)
        try container.encode(mcc, forKey: CodingKeys.mcc)
        try container.encode(storeLabel, forKey: CodingKeys.storeLabel)
        try container.encode(terminalLabel, forKey: CodingKeys.terminalLabel)
        try container.encode(payoad, forKey: CodingKeys.payload)
        try container.encode(amount, forKey: CodingKeys.amount)
        try container.encode(merchantPanId, forKey: CodingKeys.merchantPanId)
    }
   
}
