//
//  QrPaymentResponseModel.swift
//  Easy.com.bd
//
//  Created by Raihan on 3/3/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
// MARK: - QrPaymentResponseModel
class QrPaymentResponseModel: Codable {
    let ui: String?
    let message, status: String?
    let data: QrPaymentResponse?
    let code: Int?
    
    init(ui: String?, message: String?, status: String?, data: QrPaymentResponse?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.data = data
        self.code = code
    }
}

// MARK: - DataClass
class QrPaymentResponse: Codable {
    let storeID, storePassword, amount, transactionID: String?
    let serviceTitle: String?
    let ipnURL: String?
    let valueB, valueC, userRefer: String?
    let paymentMethods: PaymentMethods?
    let payWithSaveCard: Bool?
    
    enum CodingKeys: String, CodingKey {
        case storeID = "store_id"
        case storePassword = "store_password"
        case amount
        case transactionID = "transaction_id"
        case serviceTitle = "service_title"
        case ipnURL = "ipn_url"
        case valueB = "value_b"
        case valueC = "value_c"
        case userRefer = "user_refer"
        case paymentMethods = "payment_methods"
        case payWithSaveCard = "pay_with_save_card"
    }
    
    init(storeID: String?, storePassword: String?, amount: String?, transactionID: String?, serviceTitle: String?, ipnURL: String?, valueB: String?, valueC: String?, userRefer: String?, paymentMethods: PaymentMethods?, payWithSaveCard: Bool?) {
        self.storeID = storeID
        self.storePassword = storePassword
        self.amount = amount
        self.transactionID = transactionID
        self.serviceTitle = serviceTitle
        self.ipnURL = ipnURL
        self.valueB = valueB
        self.valueC = valueC
        self.userRefer = userRefer
        self.paymentMethods = paymentMethods
        self.payWithSaveCard = payWithSaveCard
    }
}

// MARK: - PaymentMethods
class PaymentMethods: Codable {
    let cards: [Card]?
    let others: [OtherMethods]?
    
    init(cards: [Card]?, others: [OtherMethods]?) {
        self.cards = cards
        self.others = others
    }
}

// MARK: - Card
class Card: Codable {
    let cardIndex, cardNo, bankName, channelType: String?
    let channel: String?
    let isDefaultCard, isMoto: Int?
    let logo, expiryDate: String?
    
    enum CodingKeys: String, CodingKey {
        case cardIndex = "card_index"
        case cardNo = "card_no"
        case bankName = "bank_name"
        case channelType = "channel_type"
        case channel
        case isDefaultCard = "is_default_card"
        case isMoto = "is_moto"
        case logo
        case expiryDate = "expiry_date"
    }
    
    init(cardIndex: String?, cardNo: String?, bankName: String?, channelType: String?, channel: String?, isDefaultCard: Int?, isMoto: Int?, logo: String?, expiryDate: String?) {
        self.cardIndex = cardIndex
        self.cardNo = cardNo
        self.bankName = bankName
        self.channelType = channelType
        self.channel = channel
        self.isDefaultCard = isDefaultCard
        self.isMoto = isMoto
        self.logo = logo
        self.expiryDate = expiryDate
    }
}

// MARK: - Other
class OtherMethods: Codable {
    let channel, channelType, channelName: String?
    let logUrl: String?
    let isDisplay: Int?
    let position: Int?
    
    enum CodingKeys: String, CodingKey {
        case channel
        case channelType = "channel_type"
        case channelName = "channel_name"
        case logUrl = "logo_url"//imgPath
        case isDisplay =  "is_display"
        case position
    }
    
    init(channel: String?, channelType: String?, channelName: String?, logUrl: String?,isDisplay: Int?,position: Int?) {
        self.channel = channel
        self.channelType = channelType
        self.channelName = channelName
        self.logUrl = logUrl
        self.isDisplay = isDisplay
        self.position = position
    }
}

enum GwType: String, Codable {
    case card = "card"
    case ib = "ib"
    case mb = "mb"
}
