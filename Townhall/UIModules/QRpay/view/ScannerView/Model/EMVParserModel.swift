//
//  EMVParserModel.swift
//  BanglaQR
//
//  Created by Yamen Emon on 29/10/20.
//

import Foundation

public struct EMVParserModel : Decodable {
    var tag: String
    var length: Int
    var value: String
    var childEMVParserModel: [EMVParserModel] = []
}
