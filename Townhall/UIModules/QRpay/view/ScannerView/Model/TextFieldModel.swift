//
//  TextFieldModel.swift
//  BanglaQR
//
//  Created by Raihan on 1/6/21.
//

import Foundation
import UIKit

enum FieldType : Int,CaseIterable{
    case tipConvenseAmount,tipConvensePercentage,billNumber,mobileNumber,storeId,loyaltyNumber,referenceId,customerId,terminalId,purpose,totalAmount,mcc,payload,amount
    
    var textName:String{
        switch self {
       
        case .tipConvenseAmount:
            return "tip_convense_amount_qr".easyLocalized()
        case .tipConvensePercentage:
            return "tip_convense_percentage_qr".easyLocalized()
        case .billNumber:
            return "bill_number_qr".easyLocalized()
        case .mobileNumber:
            return "mobile_number_qr".easyLocalized()
        case .storeId:
            return "store_id_qr".easyLocalized()
        case .loyaltyNumber:
            return "loyality_number_qr".easyLocalized()
        case .referenceId:
            return "reference_id_qr".easyLocalized()
        case .customerId:
            return "customer_id_qr".easyLocalized()
        case .terminalId:
            return "terminal_id_qr".easyLocalized()
        case .purpose:
            return "purpose_qr".easyLocalized()
        case .totalAmount:
            return "total_amount_qr".easyLocalized()
        case .mcc:
            return "mcc_qr".easyLocalized()
        case .payload:
           return "Payload"
        case .amount:
            return "amount_qr".easyLocalized()
        }
    }
    var infoType: InfoType{
        switch self {
        case .tipConvenseAmount:
            return .amount
        case .tipConvensePercentage:
            return .numeric
        case .billNumber:
            return .numeric
        case .mobileNumber:
            return .numeric
        case .storeId:
            return .text
        case .loyaltyNumber:
            return .numeric
        case .referenceId:
            return .text
        case .customerId:
            return .text
        case .terminalId:
            return .text
        case .purpose:
            return .text
        case .totalAmount:
            return .amount
        case .mcc:
            return .numeric
        case .payload:
            return .text
        case .amount:
            return .amount
        }
    }
    var keyboardType : UIKeyboardType{
        switch self.infoType {
        case .numeric:
            return .numberPad
        case .amount:
            return .decimalPad
        default:
            return .default
        }
    }
}
enum InfoType {
    case text,numeric,amount
}
class TextFieldModel{
    var textValue : String?
    var textName : String?
    var isEditable : Bool?
    var isVisible : Bool?
    var index : FieldType?
}


class HeaderModel {
    var merchantName : String?
    var merchantCity : String?
    var merchantId : String?
    var currentDate : String?
    var merchantPanId:String?
    var isTagValueMatched : Bool = false
}



