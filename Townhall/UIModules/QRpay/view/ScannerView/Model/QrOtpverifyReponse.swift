//
//  QrOtpverifyReponse.swift
//  Easy.com.bd
//
//  Created by Raihan on 3/2/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation


// MARK: - QrOtpVerify
class QrOtpverifyReponse: Codable {
    let ui, message, status: String?
    let data: QrOtpverify?
    let code: Int?

    init(ui: String?, message: String?, status: String?, data: QrOtpverify?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.data = data
        self.code = code
    }
}

// MARK: - DataClass
class QrOtpverify: Codable {
    let banglaQrPinKey: String?

    enum CodingKeys: String, CodingKey {
        case banglaQrPinKey = "bangla_qr_pin_key"
    }

    init(banglaQrPinKey: String?) {
        self.banglaQrPinKey = banglaQrPinKey
    }
}
