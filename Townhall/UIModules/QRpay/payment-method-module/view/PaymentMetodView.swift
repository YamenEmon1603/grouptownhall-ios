//
//  PaymentMetodView.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 1/3/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import Kingfisher

class PaymentMetodView: UIView {

    override private init(frame: CGRect) {
        super.init(frame: frame)
    }
    private var vc : PaymentMetodViewController!
  
    
    
    convenience init(vc : PaymentMetodViewController) {
        self.init(frame:.zero)
        self.vc = vc
        self.backgroundColor = .white
        self.translatesAutoresizingMaskIntoConstraints = false
        setUI()
    }
    
    required internal init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"select_payment_method".easyLocalized(), rightItem: nil)
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.6)
        navView.translatesAutoresizingMaskIntoConstraints = false
        return navView
    }()
    func setUI() {

        
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,  heightMultiplier: 0.08)
       
        //
        
        let baseScrollHolder = UIView()
        baseScrollHolder.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(baseScrollHolder)
        baseScrollHolder.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        baseScrollHolder.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        baseScrollHolder.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        baseScrollHolder.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        let baseScroll = UIScrollView()
        baseScroll.translatesAutoresizingMaskIntoConstraints = false
        baseScrollHolder.addSubview(baseScroll)
        baseScroll.topAnchor.constraint(equalTo: baseScrollHolder.topAnchor).isActive = true
        baseScroll.leadingAnchor.constraint(equalTo: baseScrollHolder.leadingAnchor).isActive = true
        baseScroll.trailingAnchor.constraint(equalTo: baseScrollHolder.trailingAnchor).isActive = true
        baseScroll.bottomAnchor.constraint(equalTo: baseScrollHolder.bottomAnchor).isActive = true
        
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        baseScroll.addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: baseScroll.topAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: baseScroll.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: baseScroll.trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: baseScroll.bottomAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo: baseScrollHolder.widthAnchor).isActive = true
        
        containerView.addSubview(savedCardView)
        savedCardView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive  = true
        savedCardView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        savedCardView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        savedCardView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.hexStringToUIColor(hex: "f5f5f5")
        let _ = separatorView.addBorder(side: .top, color: .lightPeriwinkleTwo, width: 0.5)
        let _ = separatorView.addBorder(side: .bottom, color: .lightPeriwinkleTwo, width: 0.5)
        containerView.addSubview(separatorView)
        separatorView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        separatorView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: savedCardView.bottomAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 6).isActive = true
//
        containerView.addSubview(otherMethodsView)
        otherMethodsView.topAnchor.constraint(equalTo: separatorView.bottomAnchor).isActive = true
        otherMethodsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        otherMethodsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        otherMethodsView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
    
    //MARK:- Other Methods
    lazy var otherMethodsLabel: UILabel = {
        let otherMethodsLabel = UILabel()
        otherMethodsLabel.translatesAutoresizingMaskIntoConstraints = false
        otherMethodsLabel.text = "others_method".easyLocalized()
        otherMethodsLabel.font = .EasyFont(ofSize: 16, style: .regular)
        otherMethodsLabel.textColor = .blueyGrey
        return otherMethodsLabel
    }()
    
    lazy var btnViewAll: UIButton = {
        let btnViewAll = UIButton()
        btnViewAll.translatesAutoresizingMaskIntoConstraints = false
        btnViewAll.setTitle("view_all_payment".easyLocalized(), for: .normal)
        btnViewAll.setTitleColor(.blueViolet, for: .normal)
        btnViewAll.titleLabel?.font = .EasyFont(ofSize: 14, style: .medium)
       
        btnViewAll.layer.cornerRadius = 10
       
        btnViewAll.backgroundColor = UIColor.blueViolet.withAlphaComponent(0.2)
        btnViewAll.addTarget(vc, action: #selector(vc.btnViewAllMethod(_:)), for: .touchUpInside)
        return btnViewAll
    }()
    
    lazy var otherMethodsView: UIView = {
        let otherMethodsView = UIView()
        otherMethodsView.translatesAutoresizingMaskIntoConstraints = false
        otherMethodsView.addSubview(otherMethodsLabel)
        otherMethodsLabel.leadingAnchor.constraint(equalTo: otherMethodsView.leadingAnchor, constant: 10).isActive = true
        otherMethodsLabel.topAnchor.constraint(equalTo: otherMethodsView.topAnchor, constant: 10).isActive = true
        otherMethodsLabel.trailingAnchor.constraint(equalTo: otherMethodsView.trailingAnchor, constant: -10).isActive = true

       
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        otherMethodsView.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: otherMethodsView.leadingAnchor,constant: 10).isActive = true
        stackView.trailingAnchor.constraint(equalTo: otherMethodsView.trailingAnchor,constant: -10).isActive = true
        stackView.topAnchor.constraint(equalTo: otherMethodsLabel.bottomAnchor,constant: 10).isActive = true
        stackView.bottomAnchor.constraint(equalTo: otherMethodsView.bottomAnchor).isActive = true
        
        if let otherMethods = vc.paymentData?.data?.paymentMethods?.others?.filter({$0.isDisplay == 1}).sorted(by: { (item1, item2) -> Bool in
            if let pos1 = item1.position,let pos2 = item2.position{
                return pos1 < pos1
            }
            return false
        }){
            for (index,method) in otherMethods.enumerated(){
                let itemView = OtherMethodsItem(icon:method.logUrl ?? "")
                itemView.tag = index
                itemView.didPickMethod = vc.didPickMethod(_:)
                itemView.heightAnchor.constraint(equalToConstant: 60).isActive = true
                stackView.addArrangedSubview(itemView)
            }
        }
        

        stackView.addArrangedSubview(btnViewAll)
      
        return otherMethodsView
    }()
    
    //MARK:- Saved Card View
    lazy var savedCardLabel: UILabel = {
        let savedCardLabel = UILabel()
        savedCardLabel.translatesAutoresizingMaskIntoConstraints = false
        savedCardLabel.text = "my_saved_cards".easyLocalized()
        savedCardLabel.font = .EasyFont(ofSize: 16, style: .regular)
        savedCardLabel.textColor = .blueyGrey
        return savedCardLabel
    }()
    lazy var btnAddCard: UIButton = {
        let btnAddCard = UIButton()
        btnAddCard.translatesAutoresizingMaskIntoConstraints = false
        btnAddCard.setImage(UIImage(named: "icplus"), for: .normal)
        btnAddCard.setTitle("pay_using_another_card".easyLocalized(), for: .normal)
        btnAddCard.setTitleColor(.blueViolet, for: .normal)
        btnAddCard.titleLabel?.font = .EasyFont(ofSize: 14, style: .medium)
        btnAddCard.layer.borderWidth = 1
        btnAddCard.layer.cornerRadius = 10
        btnAddCard.contentHorizontalAlignment = .left
        btnAddCard.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        btnAddCard.titleEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)
        btnAddCard.layer.borderColor = UIColor.blueViolet.cgColor
        btnAddCard.backgroundColor = UIColor.blueViolet.withAlphaComponent(0.2)
        btnAddCard.addTarget(vc, action: #selector(vc.btnAddCardAction(_:)), for: .touchUpInside)
        return btnAddCard
    }()
    lazy var cardStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    lazy var savedCardView: UIView = {
        let savedCardView = UIView()
        savedCardView.translatesAutoresizingMaskIntoConstraints = false
        savedCardView.addSubview(savedCardLabel)
        savedCardLabel.leadingAnchor.constraint(equalTo: savedCardView.leadingAnchor, constant: 10).isActive = true
        savedCardLabel.topAnchor.constraint(equalTo: savedCardView.topAnchor).isActive = true
        savedCardLabel.trailingAnchor.constraint(equalTo: savedCardView.trailingAnchor, constant: -10).isActive = true
       
        savedCardView.addSubview(btnAddCard)
        btnAddCard.bottomAnchor.constraint(equalTo: savedCardView.bottomAnchor,constant: -20).isActive = true
        btnAddCard.leadingAnchor.constraint(equalTo: savedCardView.leadingAnchor, constant: 10).isActive = true
        btnAddCard.trailingAnchor.constraint(equalTo: savedCardView.trailingAnchor, constant: -10).isActive = true
        btnAddCard.heightAnchor.constraint(equalTo: savedCardView.heightAnchor,multiplier: 0.2).isActive = true
       

        
        let scrollHolder : UIView = UIView()
        scrollHolder.translatesAutoresizingMaskIntoConstraints = false
        scrollHolder.backgroundColor = .white
        savedCardView.addSubview(scrollHolder)
        scrollHolder.topAnchor.constraint(equalTo: savedCardLabel.bottomAnchor,constant: 5).isActive = true
        scrollHolder.leadingAnchor.constraint(equalTo: savedCardView.leadingAnchor).isActive = true
        scrollHolder.trailingAnchor.constraint(equalTo: savedCardView.trailingAnchor).isActive = true
        scrollHolder.bottomAnchor.constraint(equalTo:btnAddCard.topAnchor,constant: -20).isActive = true

        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollHolder.addSubview(scrollView)
        scrollView.leadingAnchor.constraint(equalTo: scrollHolder.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: scrollHolder.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: scrollHolder.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: scrollHolder.bottomAnchor).isActive = true

        let itemHolder = UIView()
        itemHolder.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(itemHolder)
        itemHolder.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        itemHolder.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        itemHolder.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        itemHolder.heightAnchor.constraint(equalTo: scrollHolder.heightAnchor, multiplier: 1).isActive = true
        itemHolder.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true


        itemHolder.addSubview(cardStack)
        cardStack.leadingAnchor.constraint(equalTo: itemHolder.leadingAnchor,constant: 10).isActive = true
        cardStack.trailingAnchor.constraint(equalTo: itemHolder.trailingAnchor).isActive = true
        cardStack.topAnchor.constraint(equalTo: itemHolder.topAnchor).isActive = true
        cardStack.bottomAnchor.constraint(equalTo: itemHolder.bottomAnchor).isActive = true
        let cardWidth = UIScreen.main.bounds.width * 0.85
        if let cards =  vc.paymentData?.data?.paymentMethods?.cards{
            for (index,card) in cards.enumerated() {
                let itemView = CardView(card: card) //bankName: card.bankName ?? "", cardNo: card.cardNo ?? "", expiryDate: card.expiryDate ?? "", logo: card.logo ?? ""
                itemView.selectCardAction = vc.selectCardAction(_:)
                itemView.deleteCardAction = vc.deleteCardAction(_:)
                itemView.tag = index
                itemView.widthAnchor.constraint(equalToConstant: cardWidth).isActive = true

                cardStack.addArrangedSubview(itemView)
            }
        }
        return savedCardView
    }()
    
    
    //MARK:- Nav View
    lazy var backButton: UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "icback"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 20)
        backButton.contentHorizontalAlignment = .left
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.addTarget(vc, action: #selector(vc.btnBackAction(_:)) , for:.touchUpInside )
        return backButton
    }()
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "select_payment_method".easyLocalized()
        titleLabel.font = .EasyFont(ofSize: 13, style: .bold)
        return titleLabel
    }()
    

    
}
class OtherMethodsItem: UIView {
    override internal init(frame: CGRect) {
        super.init(frame: frame)
    }
    var didPickMethod : ((_ item : Int)->Void)? = nil
    convenience init(icon : String) {
        self.init(frame:.zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightPeriwinkleTwo.cgColor
        self.layer.cornerRadius = 10
        
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.kf.setImage(with: URL(string: icon))
        self.addSubview(imageView)
        imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: self.heightAnchor ,multiplier: 0.8).isActive = true
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1).isActive = true
        imageView.backgroundColor = .clear
        
        let button = UIButton()
        let image = UIImage(named:"arrow-point-to-right")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = .blueViolet
        self.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -20).isActive = true
        button.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        button.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.30).isActive = true
        button.widthAnchor.constraint(equalTo:  button.heightAnchor, multiplier:1).isActive = true
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(selectMethod(_:)))
        tapgesture.cancelsTouchesInView = false
        self.addGestureRecognizer(tapgesture)
    }
    @objc func selectMethod(_ sender : UITapGestureRecognizer){
     
        self.didPickMethod?(self.tag)
    }
    required internal init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CardView: UIView ,UIGestureRecognizerDelegate{
    
    override internal init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required internal init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var selectCardAction : ((_ item : Int)->Void)? = nil
    var deleteCardAction : ((_ item : Int)->Void)? = nil
    convenience init(card:Card) {
      
        self.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let imageView = UIImageView()
        switch card.channel?.lowercased() {
        case CardType.VISA.rawValue.lowercased():
            imageView.image = UIImage(named: "visaCardBg")
        case CardType.MASTERCARD.rawValue.lowercased():
            imageView.image = UIImage(named: "masterCardBg")
        case CardType.AMEX.rawValue.lowercased():
            imageView.image =  UIImage(named: "americanExpressCardBg")
        case CardType.UNION.rawValue.lowercased():
            imageView.image =  UIImage(named: "unionPayCardBg")
        default:
            imageView.image = UIImage(named: "cardBg")
        }
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        let lblBankName = UILabel()
        lblBankName.font = .EasyFont(ofSize: 16, style: .medium)
        lblBankName.textColor = .white
        lblBankName.text = card.bankName
        lblBankName.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lblBankName)
        lblBankName.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 14.5).isActive = true
        lblBankName.topAnchor.constraint(equalTo: self.topAnchor, constant: 21).isActive = true
        
        let lblCardNo = UILabel()
        lblCardNo.font = .EasyFont(ofSize: 13, style: .medium)
        lblCardNo.textColor = .white
        lblCardNo.text = card.cardNo
       
        lblCardNo.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lblCardNo)
        lblCardNo.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 14.5).isActive = true
        lblCardNo.topAnchor.constraint(equalTo: lblBankName.bottomAnchor, constant: 5.3).isActive = true
      
       
        let lblExpiryDate = UILabel()
        lblExpiryDate.font = .EasyFont(ofSize: 9, style: .medium)
        lblExpiryDate.textColor = UIColor.white.withAlphaComponent(0.5)
        lblExpiryDate.text = "EXPIRY\nDATE"
        lblExpiryDate.numberOfLines = 2
        lblExpiryDate.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lblExpiryDate)
        lblExpiryDate.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 14.5).isActive = true
        lblExpiryDate.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20.8).isActive = true
        
        let lblDate = UILabel()
        lblDate.font = .EasyFont(ofSize: 20, style: .medium)
        lblDate.textColor = UIColor.white.withAlphaComponent(0.5)
        lblDate.text = card.expiryDate
        lblDate.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lblDate)
        lblDate.leadingAnchor.constraint(equalTo: lblExpiryDate.trailingAnchor, constant: 8).isActive = true
        lblDate.centerYAnchor.constraint(equalTo: lblExpiryDate.centerYAnchor).isActive = true
        
        let cardLogiv = UIImageView()
        cardLogiv.translatesAutoresizingMaskIntoConstraints = false
        cardLogiv.kf.setImage(with: URL(string: card.logo ?? ""))
        cardLogiv.contentMode = .scaleAspectFit
        self.addSubview(cardLogiv)
        cardLogiv.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        cardLogiv.centerYAnchor.constraint(equalTo: lblExpiryDate.centerYAnchor).isActive = true
        cardLogiv.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.2).isActive = true
        cardLogiv.heightAnchor.constraint(equalTo:  cardLogiv.widthAnchor, multiplier: 1).isActive = true
        
        let button = UIButton()
        button.setImage(UIImage(named: "delete"), for: .normal)
        self.addSubview(button)
   
        button.addTarget(self, action: #selector(deleteAction(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        button.topAnchor.constraint(equalTo:  self.topAnchor).isActive = true
        button.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4).isActive = true
        button.widthAnchor.constraint(equalTo:  button.heightAnchor, multiplier:1).isActive = true
       
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(selectCard(_:)))
        tapgesture.delegate = self
        tapgesture.cancelsTouchesInView = false
        self.addGestureRecognizer(tapgesture)
    }
    @objc func deleteAction(_ sender : UIButton){
        self.deleteCardAction?(self.tag)
    }
    @objc func selectCard(_ sender : UITapGestureRecognizer){
     
        self.selectCardAction?(self.tag)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIButton == true {
            
            return false
         }
         return true
    }

}
