//
//  PaymentMetodViewController.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 1/3/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import SSLCommerzSDK

enum CardType : String {
    case VISA = "VISA"
    case MASTERCARD = "MASTERCARD"
    case AMEX = "AMERICAN EXPRESS"
    case UNION = "UNION PAY"
    case UNKNOWN = ""
}


class PaymentMetodViewController: EasyBaseViewController {
    
    func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: {
            if let _delegate = self.scannerDelegate {
                _delegate.failCallBack("User back button pressed after parsing QR data")
            }
        })
    }
    
    var sdkData: SSLComerceData?
    var sslCommerz : SSLCommerz?
    var scannerDelegate : QrScannerDelegate?
    
    lazy var paymentView: PaymentMetodView = {
        let paymentView = PaymentMetodView(vc: self)
        paymentView.navView.backButtonCallBack = backBtnPressed
        return paymentView
    }()
    var paymentData : QrPaymentResponseModel?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.addSubview(paymentView)

        paymentView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        paymentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        paymentView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        paymentView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    func selectCardAction(_ item : Int)  {
        if let card = paymentData?.data?.paymentMethods?.cards?[item]{
            if card.isMoto == 1{
                self.requestAutoDebit(card: card)
            }else{
                let vc = CVVViewController()
                vc.cvvCount = card.channel == CardType.AMEX.rawValue ? 4 : 3
                vc.delegate = self
                vc.card = card
                self.presentAsPopUp(vc: vc)
            }
        }
       
    }
    func deleteCardAction(_ item : Int){
        if let card = paymentData?.data?.paymentMethods?.cards?[item]{
//            let vc = UIStoryboard(name: "CommonAlerts", bundle: nil).instantiateViewController(withIdentifier: "bottomSheetVC") as! BottomSheetViewController
//            
//            vc.sheetTitle = "delete_card_message".easyLocalized()
//            vc.subtitle = ""
//            vc.buttoOneName = "btn_cancel_title".easyLocalized()
//            vc.button2Name = "delete_btn".easyLocalized()
//            vc.btnAction1 = {
//                debugPrint("btn1")
//               
//            }
//            vc.btnAction2 = {
//                self.requestDelete(card: card, at: item)
//            }
//            presentAsPopUp(vc: vc)
        }
    }
    func didPickMethod(_ item : Int){
        if let method = paymentData?.data?.paymentMethods?.others?[item]{
            requestAutoDebit(otherMethod: method)
        }
    }
    @objc func btnViewAllMethod(_ sender : UIButton){
        print("btnViewAllMethod")
        if let paymentResponse = paymentData{
            loadSDK(with: paymentResponse)
        }
       
    }
    @objc func btnAddCardAction(_ sender : UIButton){
        print("btnAddCardAction")
        if let paymentResponse = paymentData{
            loadSDK(with: paymentResponse)
        }
    }
    @objc func btnBackAction(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    func showPaymentGateway(data : AutoDebitResponse){
        let vc = PaymentGatewayViewController()
        vc.delegate = self
        vc.gwData = data
        
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .formSheet
        nav.popoverPresentationController?.sourceView = self.view
        nav.popoverPresentationController?.sourceRect = vc.view.frame
        nav.presentationController?.delegate = vc
        self.present(nav, animated: true, completion: nil)
    }
    
    func loadSDK(with response: QrPaymentResponseModel) {
        if response.code == 200{
            if let ssldata = response.data, let storeid = ssldata.storeID , let storePass = ssldata.storePassword , let transId = ssldata.transactionID{
                
                let integrationInfo =  IntegrationInformation(storeID: storeid, storePassword: storePass, totalAmount: Double(ssldata.amount ?? "0.0") ?? 0.0, currency: "BDT", transactionId: transId, productCategory: ssldata.serviceTitle ?? "")
                if let ipnUrl = ssldata.ipnURL{
                    integrationInfo.ipnURL = ipnUrl
                }
                sslCommerz = SSLCommerz.init(integrationInformation: integrationInfo)
                
                sslCommerz?.delegate = self
                sslCommerz?.customerInformation = .init(customerName: EasyDataStore.sharedInstance.user?.name ?? "", customerEmail: EasyDataStore.sharedInstance.user?.name ?? "", customerAddressOne: EasyDataStore.sharedInstance.user?.address ?? "", customerCity: "dhk", customerPostCode: "1234", customerCountry: "BD", customerPhone: EasyDataStore.sharedInstance.user?.mobile ?? "")
                if let refer = ssldata.userRefer{
                    sslCommerz?.customerInformation?.userRefer = refer
                }
                
                sslCommerz?.additionalInformation = .init() //.init(paramA: "", paramB: ssldata.valueB, paramC: ssldata.valueC, paramD: "")
                sslCommerz?.additionalInformation?.paramB = ssldata.valueB
                    sslCommerz?.additionalInformation?.paramC = ssldata.valueC
                sslCommerz?.start(in: self, shouldRunInTestMode: Constant.isDev)
            }
            else{
                self.showToastError(message:response.message ?? "")
            }
        }else{
            self.showToastError(message:response.message ?? "")
        }
    }
 
    func requestDelete(card : Card,at index: Int){
        WebServiceHandler.shared.deleteSavedCard(cardIndex: card.cardIndex ?? "", userRefer: paymentData?.data?.userRefer ?? "", completion: { (result) in
            switch result {
            case .success(let response):
                if response.code == 200{
                    self.showToast(message: response.message ?? "")
                    if let cardView = self.paymentView.cardStack.arrangedSubviews[index] as? CardView{
                        self.paymentView.cardStack.removeArrangedSubview(cardView)
                    }
                   
                }else{
                    self.showToastError(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    
    func requestAutoDebit(card:Card,cvv:String = "")  {
        let requestObject = AutoDebitRequestModel(action: "autoDebit", channelType: card.channelType , channel: card.channel, cardIndex: card.cardIndex, cvv: cvv, totalAmount: paymentData?.data?.amount , transId:paymentData?.data?.transactionID, serviceTitle: "bangla-qr")
        WebServiceHandler.shared.autoDebit(params: requestObject, completion: { (result) in
            switch result{
            case .success(let response):
                if  response.code == 200{
                    if let data = response.data{
                        self.showPaymentGateway(data: data)
                    }
                   
                }else{
                    self.showToastError(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    func requestAutoDebit(otherMethod: OtherMethods)  {
        let requestObject = AutoDebitRequestModel(action: "autoDebit", channelType: otherMethod.channelType , channel: otherMethod.channel, cardIndex: "", cvv: "", totalAmount: paymentData?.data?.amount , transId:paymentData?.data?.transactionID, serviceTitle: "bangla-qr")
        WebServiceHandler.shared.autoDebit(params: requestObject, completion: { (result) in
            switch result{
            case .success(let response):
                if  response.code == 200{
                    if let data = response.data{
                        self.showPaymentGateway(data: data)
                    }
                   
                }else{
                    self.showToastError(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    func checkStatus(){
        WebServiceHandler.shared.qrPaymentInit(transactionId: paymentData?.data?.transactionID ?? "", completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.showTransactionSuccess(message: response.message ?? "", transactionId: self.paymentData?.data?.transactionID ?? "")
                }else{
                    
                    self.showTransactionFailed(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
}
//MARK: -OnlinePaymentDelegate

extension PaymentMetodViewController : OnlinePaymentDelegate{
    func gateway(didFinish with: PaymentStatus) {
        switch with {
        case .success:
            checkStatus()

        case .failure:
            checkStatus()
            //showTransactionFailed(message: qr_payment_failed[EasyUtils.getLanguageIndex()])
        case .cancel:
            showToast(message: "qr_payment_failed".easyLocalized())
            EasyUtils.shared.delay(1.0) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
}

//MARK:- CVVViewDelegate
extension PaymentMetodViewController : CVVViewDelegate{
    func hasEnterCvv(cvv: String, card: Card) {
        requestAutoDebit(card: card, cvv: cvv)
    }
    
   
    
    
}

//MARK: - CommonAlertActionProtocol

extension PaymentMetodViewController{
//    func showTransactionSuccess(message: String,transactionId: String) {
//        let message = message
//        
//        let data = CommonAlertData(type: .success, title: "qr_payment_successful".easyLocalized(), message: message, postive: "done".easyLocalized(), negative: "",transactionId: transactionId)
//        
//        let popModule = CommonAlertRouterModule.createModule(with: data, delegate: self)
//        
//        self.presentAsPopUp(vc: popModule)
//    }
//    
//    func showTransactionFailed(message:String) {
//        let message = message
//        let data = CommonAlertData(type: .wrong, title: "qr_payment_failed".easyLocalized(), message: message, postive: "try_again".easyLocalized(), negative:"go_home".easyLocalized())
//        let popModule = CommonAlertRouterModule.createModule(with: data, delegate: self)
//        self.presentAsPopUp(vc: popModule)
//    }
//    
//    func postiveAction(for type: AlertType) {
//        if type == .success{
//            self.dismiss(animated: true, completion: nil)
//        }else{
//            self.dismiss(animated: true, completion: {
//                NotificationCenter.default.post(name: Notification.Name("ShowQR"), object: nil)
//            })
//        }
//
//
//    }
    
    func negativeAction(for type: AlertType) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showTransactionSuccess(message: String,transactionId: String) {
        let message = message
        
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "done".easyLocalized(), tag: 1001)
        //let btn = PopupButton (type: .bordered, title: "go_home".easyLocalized(), tag: 1002)
        popup.options = [.image(image: .success),.title(str:   "qr_payment_successful".easyLocalized(),color:.trueGreen),.subtitle(str: message,color: .slateGrey),.buttons(buttons: [go])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showTransactionFailed(message:String) {
      
        let message = message
        
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "try_again".easyLocalized(), tag: 1004)
        let btn = PopupButton (type: .bordered, title: "go_home".easyLocalized().easyLocalized(), tag: 1003)
        popup.options = [.image(image: .failed),.title(str:  "qr_payment_failed".easyLocalized(),color:.tomato),.subtitle(str: message,color: .slateGrey),.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
        
    }
    
    
}
extension PaymentMetodViewController : EasyPopUpDelegate{
    
    func popupDismissedWith(button: PopupButton, consent: Bool?, rating: Int?) {
        if button.tag == 1001{
            self.dismiss(animated: true, completion: nil)
        }else if button.tag == 1003{
            self.dismiss(animated: true, completion: nil)
        }else if button.tag == 1004{
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: Notification.Name("ShowQR"), object: nil)
            })

            
        }else{
            
            navigationController?.dismiss(animated: true, completion: nil)
        }
    }
}
extension PaymentMetodViewController : SSLCommerzDelegate {
    func transactionCompleted(withTransactionData transactionData: SSLCommerzSDK.TransactionDetails?){
        if(transactionData?.status?.lowercased() == "valid" || transactionData?.status?.lowercased() == "validated" ){
            checkStatus()
        }else{
            checkStatus()
           
        }
    }

    func sdkClosed(reason: String){
        
    }
    

}
