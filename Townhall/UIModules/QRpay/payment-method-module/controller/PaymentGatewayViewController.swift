//
//  PaymentGatewayViewController.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 4/3/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import WebKit
class PaymentGatewayViewController: EasyBaseViewController {
    let webView = WKWebView()
    var gwData : AutoDebitResponse?
    //var urls : PaymentLinks?
    var delegate : OnlinePaymentDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(webView)
        let leading = webView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor)
        let trailing = webView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
        let top = webView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor)
        let bottom = webView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        NSLayoutConstraint.activate([leading,trailing,top,bottom])
        if let data = gwData{
            EasyLoader.sharedInstance.startAnimation()
            webView.navigationDelegate = self
            if let link = URL(string: data.redirectGatewayURL ?? ""){
                let request = URLRequest(url: link)
                webView.load(request)
            }else{
                EasyLoader.sharedInstance.stopAnimation()
                self.dismiss(animated: true, completion: nil)
            }

        }else{
            EasyLoader.sharedInstance.stopAnimation()
            self.dismiss(animated: true, completion: nil)
        }
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    
    
}

//MARK: - WebviewDelegate

extension PaymentGatewayViewController : WKNavigationDelegate{
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString {
            print(urlStr)
            if urlStr == gwData?.successURL{
                decisionHandler(.cancel)
                self.dismiss(animated: true) {
                    self.delegate?.gateway(didFinish: .success)
                }
            }else if urlStr == gwData?.failURL{
                decisionHandler(.cancel)
                self.dismiss(animated: true) {
                    self.delegate?.gateway(didFinish: .failure)
                }
            }else if urlStr == gwData?.cancelURL{
                decisionHandler(.cancel)
                self.dismiss(animated: true) {
                    self.delegate?.gateway(didFinish: .cancel)
                }
            }else{
                decisionHandler(.allow)
            }
        }else{
            decisionHandler(.allow)
        }
        
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        EasyLoader.sharedInstance.stopAnimation()
        
    }
}

//MARK: - WebviewDelegate
extension PaymentGatewayViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        self.dismiss(animated: true) {
            self.delegate?.gateway(didFinish: .cancel)
        }

    }
}

protocol OnlinePaymentDelegate{
    func gateway(didFinish with: PaymentStatus)
}
enum PaymentStatus{
    case success
    case failure
    case cancel
}
