//
//  CVVViewController.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 2/3/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
protocol  CVVViewDelegate{
    func hasEnterCvv(cvv:String,card:Card)
}
class CVVViewController: EasyBaseViewController,UITextFieldDelegate {
    var delegate:CVVViewDelegate?
    var cvvCount: Int = 0
    var card:Card?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        self.cvvTextField.becomeFirstResponder()
    }
    @objc func btnCrossAction(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = cvvCount
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    lazy var labelNavTitle : UILabel = {
        let labelNavTitle =  UILabel()
        labelNavTitle.translatesAutoresizingMaskIntoConstraints = false
        labelNavTitle.font = .EasyFont(ofSize: 16, style: .regular)
        labelNavTitle.text = "enter_CVV".easyLocalized()
        return  labelNavTitle
    }()
    
    lazy var crossBtn : UIButton = {
        let crossBtn = UIButton()
        crossBtn.translatesAutoresizingMaskIntoConstraints = false
        crossBtn.setImage(#imageLiteral(resourceName: "crossgrayround"), for: .normal)
        crossBtn.addTarget(self, action: #selector(btnCrossAction(_ :)), for: .touchUpInside)
        return crossBtn
    }()
    lazy var cvvTextField: UITextField = {
        let cvvTextField = UITextField()
        cvvTextField.borderStyle = .none
        cvvTextField.translatesAutoresizingMaskIntoConstraints = false
        cvvTextField.font = .EasyFont(ofSize: 18, style: .bold)
        cvvTextField.keyboardType = .numberPad
        cvvTextField.inputAccessoryView = nil
        cvvTextField.inputAccessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        cvvTextField.tintColor = .black
        cvvTextField.delegate = self
        return cvvTextField
    }()
    lazy var tfHolderView: UIView = {
        let tfHolderView = UIView()
        tfHolderView.translatesAutoresizingMaskIntoConstraints = false
        tfHolderView.layer.cornerRadius = 10
        tfHolderView.layer.borderColor = UIColor.blueViolet.cgColor
        tfHolderView.layer.borderWidth = 2
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12, style: .medium)
        label.textColor = .blueyGrey
        label.text = "CVV"
        label.translatesAutoresizingMaskIntoConstraints = false
        tfHolderView.addSubview(label)
        label.leadingAnchor.constraint(equalTo: tfHolderView.leadingAnchor, constant: 10).isActive = true
        label.topAnchor.constraint(equalTo: tfHolderView.topAnchor, constant: 5).isActive = true
        
        let iconView = UIImageView(image: #imageLiteral(resourceName: "cvv"))
        iconView.contentMode = .scaleAspectFit
        iconView.translatesAutoresizingMaskIntoConstraints = false
        tfHolderView.addSubview(iconView)
        iconView.trailingAnchor.constraint(equalTo: tfHolderView.trailingAnchor, constant: -10).isActive = true
        iconView.centerYAnchor.constraint(equalTo: tfHolderView.centerYAnchor).isActive = true
        iconView.heightAnchor.constraint(equalTo: tfHolderView.heightAnchor, multiplier: 0.65).isActive = true
        iconView.widthAnchor.constraint(equalTo: iconView.heightAnchor,multiplier: 1 ).isActive = true
        
        
        tfHolderView.addSubview(cvvTextField)
        cvvTextField.leadingAnchor.constraint(equalTo: tfHolderView.leadingAnchor, constant: 10).isActive = true
        cvvTextField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 5).isActive = true
        cvvTextField.bottomAnchor.constraint(equalTo: tfHolderView.bottomAnchor, constant: -5).isActive = true
        cvvTextField.trailingAnchor.constraint(equalTo: iconView.leadingAnchor).isActive = true
    
        
        return tfHolderView
    }()
    
    func setUI()  {
        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(containerView)
        containerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        containerView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.65).isActive = true
        containerView.layer.cornerRadius = 15
        containerView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        
        containerView.addSubview(labelNavTitle)
        labelNavTitle.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20).isActive = true
        labelNavTitle.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20).isActive = true
        
        containerView.addSubview(crossBtn)
        crossBtn.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
        crossBtn.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20).isActive = true
        crossBtn.heightAnchor.constraint(equalToConstant:25).isActive = true
        crossBtn.widthAnchor.constraint(equalToConstant:25).isActive = true
        
        containerView.addSubview(tfHolderView)
        tfHolderView.topAnchor.constraint(equalTo: labelNavTitle.bottomAnchor, constant: 23).isActive = true
        tfHolderView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20).isActive = true
        tfHolderView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20).isActive = true
        tfHolderView.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.15).isActive = true
    }
    

}
extension CVVViewController : ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        self.view.endEditing(true)
        if cvvTextField.text?.isEmpty == true{
            self.showToastError(message: "empty_cvv".easyLocalized())
            
            return
        }
        if cvvTextField.text?.count != cvvCount{
            self.showToastError(message: "invalid_cvv".easyLocalized())
            
            return
        }
        self.dismiss(animated: true, completion: {
            if let card = self.card{
                self.delegate?.hasEnterCvv(cvv: self.cvvTextField.text ?? "",card: card)
            }
           
        })
    }
    
    
}
