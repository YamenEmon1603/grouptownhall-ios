//
//  QRProtocolModule.swift
//  Easy.com.bd
//
//  Created by Jamil Hasnine Tamim on 1/10/19.
//  Copyright © 2019 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit

protocol QRViewProtocol: class {
    var presenter: QRPresenterProtocol?{get set}
}

protocol QRPresenterProtocol: class {
    var view: QRViewProtocol?{get set}
    var interactor: QRInputInteractorProtocol?{get set}
    var router: QRRouterProtocol?{get set}
    func pushToAnotherVC(nc: UINavigationController, scannedString: String)
}

protocol QRRouterProtocol: class {
    static func presentQRModule(fromView view: AnyObject)
    func pushToAnotherVC(nc: UINavigationController, scannedString: String)
}

protocol QRInputInteractorProtocol: class {
    var presenter: QROutputInteractorProtocol?{get set}
}

protocol QROutputInteractorProtocol: class {
    
}
