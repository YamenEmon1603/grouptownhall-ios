//
//  PinViewController.swift
//  Easy.com.bd
//
//  Created by Raihan on 3/1/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class PinViewController: EasyBaseViewController {
  
    
    @IBOutlet weak var pinContainer: UIView!
    @IBOutlet weak var retypePinContainer: UIView!
    
    @IBOutlet weak var llSetPin: UILabel!
    @IBOutlet weak var llSubText: UILabel!

    @IBOutlet weak var llSiDigitsPin: UILabel!
    @IBOutlet weak var scView: UIScrollView!
    @IBOutlet weak var pinHolder: UIView!
    var otp : String?
    var bQrPin : String?
    var newPin : String?
    var retypeNewPin : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        pinContainer.addSubview(pinView)
        pinView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        retypePinContainer.addSubview(retypePinView)
        retypePinView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
    }
    lazy var   pinView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_new_pin".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        textView.shadow = false
        textView.limit = 6
        textView.keyBoardType = .numberPad
        textView.isSecureText = true
        textView.layer.borderWidth = 0
        pinHolder.layer.borderWidth = 1
        pinHolder.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        pinHolder.backgroundColor = .Easywhite
        textView.textChangedCallBack = {text in
            if text.count != 6 {
                self.llSiDigitsPin.isHidden = false
            }else{
                self.llSiDigitsPin.isHidden = true
            }
        }
        return textView
    }()
    lazy var   retypePinView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "retype_new_pin".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        textView.shadow = false
        textView.limit = 6
        
        textView.keyBoardType = .numberPad
        textView.isSecureText = true
        return textView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        llSetPin.text = "set_pin".easyLocalized()
        llSubText.text = "please_set_a_new_pin".easyLocalized()
        llSiDigitsPin.text = "six_digits_pin".easyLocalized()
  
        hideKeyboardWhenTappedAround()
    }
    
    func languageSwitch(index: Int){
        
       
    }
    func proceedAction(){
        
        pinSet()
        
        
        
    }
    @IBAction func btnProceedAction(_ sender: Any) {
        
        proceedAction()
    }
    func pinSet(){
        if fieldValidation() == true{
            if let Bpin = bQrPin{
                WebServiceHandler.shared.qrPinSet(banglaQrPinKey: Bpin , pin: newPin ?? "", confirmPin: retypeNewPin ?? "", completion: { (result) in
                    switch result{
                    case .success(let response):
                        if response.code == 200{
                            self.dismiss(animated: true) {
                                EasyDataStore.sharedInstance.user = response.data
                                NotificationCenter.default.post(name: Notification.Name("PinChanged"), object: nil) //PinChanged
                                (self.navigationController as? SetPinNavVC)?.setPinDelegate?.setpinSuccess()
                            }
                            
                        }else{
                            self.dismiss(animated: true) {
                                (self.navigationController as? SetPinNavVC)?.setPinDelegate?.setPinFailed()
                            }
                        }
                    case .failure(let error):
                        debugPrint(error)
                    }
                    
                }, shouldShowLoader: true)
            }
        }
    }
    
    func fieldValidation()->Bool{
        if pinView.text?.isEmpty == true || pinView.text == ""{
            self.showToast(message: "enter_new_pin".easyLocalized())
            return false
        }else if retypePinView.text?.isEmpty == true || retypePinView.text == ""{
            self.showToast(message: "retype_new_pin".easyLocalized())
            return false
        }else if pinView.text != retypePinView.text{
            self.showToast(message:"retype_pin_not_matched".easyLocalized())
            return false
        }else if pinView.text?.count ?? 0 != 6 || retypePinView.text?.count ?? 0 != 6{
            self.showToast(message:"pin_no_needed_to_be_six_digit".easyLocalized())
            return false
        }else{
            newPin = pinView.text
            retypeNewPin = retypePinView.text
            return true
        }
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnPrivacyAction(_ sender: UIButton) {
        let helpModule = HelpInformationDetailsRouterModule.createModule(type: .privecy)
        helpModule.modalPresentationStyle = .fullScreen
        self.present(helpModule, animated: true, completion: nil)
        
    }
    @IBAction func btnTermsAction(_ sender: Any) {
        let helpModule = HelpInformationDetailsRouterModule.createModule(type: .terms)
        helpModule.modalPresentationStyle = .fullScreen
        self.present(helpModule, animated: true, completion: nil)
    }
    
   
 
   
  
    

}
extension PinViewController:QrScannerDelegate{
    func cancelCallBack(_ message: String) {
        
    }
    
    func failCallBack(_ message: String) {
        
    }
    
    func successCallBack(_ payloadModel: [[String : String]]) {
        
    }
    func showTransactionSuccess(message: String,transactionId: String) {
        let message = message
        
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "proceed_to_qr_scan".easyLocalized(), tag: 1001)
        let btn = PopupButton (type: .bordered, title: "done".easyLocalized(), tag: 1002)
        popup.options = [.image(image: .success),.title(str:   "pin_set_successful".easyLocalized(),color:.trueGreen),.subtitle(str: message,color: .slateGrey),.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showTransactionFailed(message:String) {
      
        let message = message
        
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "make_another_payment".easyLocalized(), tag: 1003)
        let btn = PopupButton (type: .bordered, title: "go_home".easyLocalized().easyLocalized(), tag: 1004)
        popup.options = [.image(image: .failed),.title(str:  "pin_set_failed".easyLocalized(),color:.tomato),.subtitle(str: message,color: .slateGrey),.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func postiveAction(for type: AlertType) {
//        navigationController?.dismiss(animated: true, completion: nil)
        let initializer = BanglaQRInitialization.init(fundingSource: .VISA, primaryColor: "346DF6", secondaryColor: "F4F6F7")
        ScannerController.startSDK(controller: self, mainModel: initializer, sdkDelegate: self)
    }
    
    func negativeAction(for type: AlertType) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
  
    
}
extension PinViewController : EasyPopUpDelegate{
    
    func popupDismissedWith(button: PopupButton, consent: Bool?, rating: Int?) {
        if button.tag == 1001{
            let initializer = BanglaQRInitialization.init(fundingSource: .VISA, primaryColor: "346DF6", secondaryColor: "F4F6F7")
            ScannerController.startSDK(controller: self, mainModel: initializer, sdkDelegate: self)
        }else if button.tag == 1002{
            self.navigationController?.dismiss(animated: true)
        }else if button.tag == 1003{
            navigationController?.dismiss(animated: true, completion: nil)
        }else if button.tag == 1004{
            navigationController?.dismiss(animated: true, completion: nil)
            
        }else{
            
            navigationController?.dismiss(animated: true, completion: nil)
        }
    }
}
extension PinViewController:  ButtonAccessoryViewDelegate {
    func tapAction(_ sender: ButtonAccessoryView) {
        self.view.endEditing(true)
        proceedAction()
    }
    
    
    
}
