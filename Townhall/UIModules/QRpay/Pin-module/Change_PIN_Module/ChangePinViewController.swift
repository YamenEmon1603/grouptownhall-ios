//
//  PinViewController.swift
//  Easy.com.bd
//
//  Created by Raihan on 3/1/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class ChangePinViewController: EasyBaseViewController,UITextFieldDelegate {
    @IBOutlet weak var pinNewContainer: UIView!
    
    @IBOutlet weak var vwCurrentpin: UIView!
    @IBOutlet weak var vwNewPinContainer: UIView!
    @IBOutlet weak var vwRetypeNewPinContainer: UIView!
    

    @IBOutlet weak var llSetPin: UILabel!
    @IBOutlet weak var llSubText: UILabel!
    
//    @IBOutlet weak var tfCurrentPin: UITextField!
//    @IBOutlet weak var tfNewPin: UITextField!
//    @IBOutlet weak var tfRetypePin: UITextField!
    @IBOutlet weak var llSixDigitsPin: UILabel!
    @IBOutlet weak var scView: UIScrollView!
    @IBOutlet weak var lblTermsAndService: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    var otp : String?
    var bQrPin : String?
    var newPin : String?
    @IBOutlet weak var btnProcced: UIButton!
    var currentPin : String?
    var retypeNewPin : String?
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
        
        vwCurrentpin.addSubview(currentPinView)
        currentPinView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        vwNewPinContainer.addSubview(newPinView)
        newPinView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        vwRetypeNewPinContainer.addSubview(retypePinView)
        retypePinView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        llSetPin.text = "change_pin".easyLocalized()
        llSubText.text = "please_set_a_new_pin".easyLocalized()
        
        llSixDigitsPin.text = "six_digits_pin".easyLocalized()
//        tfCurrentPin.placeholder = "enter_current_pin".easyLocalized()
//        tfNewPin.placeholder = "enter_new_pin".easyLocalized()
//        tfRetypePin.placeholder = "retype_new_pin".easyLocalized()
        btnProcced.setTitle("proceed".easyLocalized(), for: .normal)
        lblTermsAndService.text = "term_of_service".easyLocalized()
        lblPrivacyPolicy.text = "privacy_policy".easyLocalized()
      
        hideKeyboardWhenTappedAround()
    }
    lazy var   currentPinView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_current_pin".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        textView.shadow = false
        textView.limit = 6
        textView.keyBoardType = .numberPad
        textView.isSecureText = true
        textView.layer.borderWidth = 0
        vwCurrentpin.layer.borderWidth = 1
        vwCurrentpin.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        vwCurrentpin.backgroundColor = .Easywhite
        textView.textChangedCallBack = {text in
            if text.count != 6 {
                self.llSixDigitsPin.isHidden = false
            }else{
                self.llSixDigitsPin.isHidden = true
            }
        }
        return textView
    }()
    lazy var   newPinView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_new_pin".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        textView.shadow = false
        textView.limit = 6
        
        textView.keyBoardType = .numberPad
        textView.isSecureText = true
        return textView
    }()
    lazy var retypePinView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "retype_new_pin".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        textView.shadow = false
        textView.limit = 6
        
        textView.keyBoardType = .numberPad
        textView.isSecureText = true
        return textView
    }()
                                         
   
    func proceedAction(){
        
        qrChangePinRequest()
        
        
        
    }
    @IBAction func btnProceedAction(_ sender: Any) {
        
        proceedAction()
    }
//    func pinSet(){
//        if fieldValidation() == true{
//            if let Bpin = bQrPin{
//                WebServiceHandler.shared.qrPinSet(banglaQrPinKey: Bpin , pin: newPin ?? "", confirmPin: retypeNewPin ?? "", completion: { (result) in
//                    switch result{
//                    case .success(let response):
//                        if response.code == 200{
//                            self.dismiss(animated: true) {
//                                UserSettings.sharedInstance.user = response.userData
//
//                                (self.navigationController as? SetPinNavVC)?.setPinDelegate?.setpinSuccess()
//                            }
//
//                        }else{
//                            self.dismiss(animated: true) {
//                                (self.navigationController as? SetPinNavVC)?.setPinDelegate?.setPinFailed()
//                            }
//                        }
//                    case .failure(let error):
//                        debugPrint(error)
//                    }
//
//                }, shouldShowLoader: true)
//            }
//        }
//    }
    
    func fieldValidation()->Bool{
        if currentPinView.text?.isEmpty == true || currentPinView.text == ""{
            self.showToast(message: "enter_new_pin".easyLocalized())
            return false
        }else if retypePinView.text?.isEmpty == true || retypePinView.text == ""{
            self.showToast(message: "retype_new_pin".easyLocalized())
            return false
        }else if newPinView.text?.isEmpty == true || newPinView.text == ""{
            self.showToast(message: "new_pin".easyLocalized())
            return false
        }else if newPinView.text != retypePinView.text{
            self.showToast(message:"retype_pin_not_matched".easyLocalized())
            return false
        }else if newPinView.text?.count ?? 0 != 6 || retypePinView.text?.count ?? 0 != 6 || currentPinView.text?.count ?? 0 != 6 {
            self.showToast(message:"pin_no_needed_to_be_six_digit".easyLocalized())
            return false
        }else{
            newPin = newPinView.text
            retypeNewPin = retypePinView.text
            currentPin = currentPinView.text
            return true
        }
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnPrivacyAction(_ sender: UIButton) {
        let helpModule = HelpInformationDetailsRouterModule.createModule(type: .privecy)
        helpModule.modalPresentationStyle = .fullScreen
        self.present(helpModule, animated: true, completion: nil)
        
    }
    @IBAction func btnTermsAction(_ sender: Any) {
        let helpModule = HelpInformationDetailsRouterModule.createModule(type: .terms)
        helpModule.modalPresentationStyle = .fullScreen
        self.present(helpModule, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        scView.isScrollEnabled = false
//        if !tfCurrentPin.isSecureTextEntry || !tfRetypePin.isSecureTextEntry || !tfNewPin.isSecureTextEntry {
//            tfNewPin.isSecureTextEntry = true
//            tfCurrentPin.isSecureTextEntry = true
//            tfRetypePin.isSecureTextEntry = true
//
//        }
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        llSixDigitsPin.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    func qrChangePinRequest(){
        if fieldValidation() == true{
            self.view.endEditing(true)
        WebServiceHandler.shared.qrChangePin(oldQrPin: currentPin ?? "", qrPin: newPin ?? "", confirmQrPin: retypeNewPin ?? "", completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                  
                    EasyDataStore.sharedInstance.user = response.data
                        
                        self.showTransactionSuccess(message: response.message ?? "", transactionId: "")
                    
                    
                }else{
                    self.showTransactionFailed(message: response.message ?? "")
//                    self.dismiss(animated: true) {
//                        (self.navigationController as? SetPinNavVC)?.setPinDelegate?.setPinFailed()
//                    }
                }
            case .failure(let error):
                debugPrint(error)
            }
        }, shouldShowLoader: true)
        }
    }
 
   
  
    

}
extension ChangePinViewController:QrScannerDelegate{
    func cancelCallBack(_ message: String) {
        
    }
    
    func failCallBack(_ message: String) {
        
    }
    
    func successCallBack(_ payloadModel: [[String : String]]) {
        
    }
    func showTransactionSuccess(message: String,transactionId: String) {
        self.showToast(message: message)
        EasyUtils.shared.delay(2.0) {
            self.dismiss(animated: true, completion: nil)
        }

    }
    
    func showTransactionFailed(message:String) {
        self.showToast(message: message)
        newPinView.text = ""
        currentPinView.text = ""
        retypePinView.text = ""
        newPin = ""
        currentPin = ""
        retypeNewPin = ""
        llSixDigitsPin.isHidden = false
    }
    
    func postiveAction(for type: AlertType) {
//        navigationController?.dismiss(animated: true, completion: nil)
        navigationController?.popToRootViewController(animated: true)
    }
    
    func negativeAction(for type: AlertType) {
      
    }
    
  
    
}
extension ChangePinViewController:  ButtonAccessoryViewDelegate {
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedAction()
    }
    
    
    
}
