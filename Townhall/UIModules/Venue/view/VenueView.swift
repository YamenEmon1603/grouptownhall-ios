//
//  VenueView.swift
//  Townhall
//
//  Created by Mausum Nandy on 9/12/21.
//

import UIKit

class VenueView: EasyBaseView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let mapView: UIView = UIView()
    
    convenience init(){
        self.init(frame: .zero)
        backgroundColor = .iceBlue
        let containerView = UIView()
        self.addSubview(containerView)
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, top:0,bottom: -.calculatedHeight(70))
        
        containerView.addSubview(mapView)
        
        mapView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,  heightMultiplier: 0.53)
        
        containerView.addSubview(imageLocView)
        imageLocView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        imageLocView.topAnchor.constraint(equalTo: mapView.bottomAnchor,constant: 5).isActive = true
        
        
    }
    lazy var imageLocView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.addShadow(location: .top, color: .black, opacity: 0.2, radius: 2)
        view.addSubview(imageScroll)
        imageScroll.addAnchorToSuperview(leading:0, trailing: 0, top: 20,  heightMultiplier: 0.45)
        
        view.addSubview(locationNameLabel)
        locationNameLabel.addAnchorToSuperview(leading: 20)
        locationNameLabel.topAnchor.constraint(equalTo:imageScroll.bottomAnchor,constant: 13).isActive = true
        
        view.addSubview(locationAddressLabel)
        locationAddressLabel.addAnchorToSuperview(leading: 20)
        locationAddressLabel.topAnchor.constraint(equalTo:locationNameLabel.bottomAnchor,constant: 5).isActive = true
        
        view.addSubview(btnCall)
        btnCall.addAnchorToSuperview( trailing: 0)
        btnCall.topAnchor.constraint(equalTo:locationNameLabel.topAnchor).isActive = true
        
        locationNameLabel.trailingAnchor.constraint(equalTo: btnCall.leadingAnchor, constant: -10).isActive = true
        locationAddressLabel.trailingAnchor.constraint(equalTo: locationNameLabel.trailingAnchor).isActive = true
        return view
    }()
    let imageScroll: UIView = {
        let imageScroll = UIView()
        imageScroll.backgroundColor = .clear
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        imageScroll.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        holderView.heightAnchor.constraint(equalTo: imageScroll.heightAnchor).isActive = true
       
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        holderView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 20, trailing: 0,top:0, bottom: 0)
        

       
        for item in EasyDataStore.sharedInstance.mapDispImages{
            let img = UIImageView(image: UIImage(named: item))
            img.translatesAutoresizingMaskIntoConstraints = false
            EasyUtils.shared.delay(0.2) {
                img.layer.cornerRadius = 10
                img.clipsToBounds = true
            }
            
            stackView.addArrangedSubview(img)
            img.widthAnchor.constraint(equalTo:imageScroll.widthAnchor , multiplier: 0.65).isActive = true
        }
        
        return imageScroll
    }()
    let locationNameLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .medium)
        label.textColor = .black
        label.numberOfLines = 0
        label.text = EasyDataStore.sharedInstance.dashboardContent?.townhall?.vanue
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let locationAddressLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .regular)
        label.textColor = .slateGrey
        label.numberOfLines = 0
        label.text = EasyDataStore.sharedInstance.dashboardContent?.townhall?.address
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let btnCall:UIButton = {
        let btnCall = UIButton()
        btnCall.setImage(UIImage(named: "mapdir"), for: .normal)
        return btnCall
    }()
}
