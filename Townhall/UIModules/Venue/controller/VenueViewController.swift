//
//  VenueViewController.swift
//  Townhall
//
//  Created by Mausum Nandy on 9/12/21.
//

import UIKit
import GoogleMaps
import CoreLocation
class VenueViewController: EasyBaseViewController {
    lazy var mView: VenueView = {
        let mView = VenueView()
        mView.btnCall.addTarget(self, action: #selector(openMap(_:)), for: .touchUpInside)
        return mView
    }()
    private let locationManager = CLLocationManager()
    var gmapView  : GMSMapView!
    var bounds = GMSCoordinateBounds()
    let latitude = 23.827862576287487
    let longitude  = 90.42696481986577
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
       gMapSetup()
    }
    func gMapSetup(){
      
        
        
        // placesClient = GMSPlacesClient.shared()
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locationManager.distanceFilter = 210
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }else{

        }
        gmapView = GMSMapView(frame: .zero)//GMSMapView.map(withFrame: .zero, camera: camera)

        gmapView.settings.zoomGestures = true
      
       
        gmapView.translatesAutoresizingMaskIntoConstraints = false
        self.mView.mapView.addSubview(gmapView)
        gmapView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        let marker = GMSMarker(position:CLLocationCoordinate2D(latitude: latitude ,longitude:  longitude) )
        
        bounds = bounds.includingCoordinate(marker.position)
        EasyUtils.shared.delay(0.1) {
            let target = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
            self.gmapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: 16)
        }
        marker.title = "title"
        marker.map = gmapView
        marker.appearAnimation = .pop
        gmapView.delegate = self
    }
    @objc func openMap(_ sender: UIButton){
        //23.827862576287487, 90.42696481986577
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app
            
            if let url = URL(string: "comgooglemaps-x-callback://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:])
            }}
        else {
            //Open in browser
            if let urlDestination = URL.init(string: "https://www.google.com/maps/dir/?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving") {
                
                UIApplication.shared.open(urlDestination)
            }
        }

    }

  

}
extension VenueViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("User still thinking..");
        case .denied:
            print("User denied");
        case .authorizedWhenInUse:
            locationManager.requestLocation()
            locationManager.startMonitoringSignificantLocationChanges()
        case .authorizedAlways:
            locationManager.requestLocation()
            locationManager.startMonitoringSignificantLocationChanges()
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let newLocation = locations.last{
            gmapView.settings.myLocationButton = false // show current location button
            
            bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude))
            let update = GMSCameraUpdate.fit(bounds, withPadding: 120)
            self.gmapView.animate(with: update)
        }
       

    }
}
extension VenueViewController : GMSMapViewDelegate{
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        return true
    }
}
