//
//  RegistrationView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/7/21.
//

import Foundation


import UIKit


class RegistrationView: UIView {
 
    var proceedCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .iceBlue
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.07)
        

        
        containerView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 8).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
   
    //MARK: COMPONENTS
   
    
 
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        
       
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
       
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
       
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 15
        holderView.addSubview(lblRegisterHeader)
        holderView.addSubview(lblRegisterSubtitle)
        holderView.addSubview(stackView)
        
        lblRegisterHeader.addAnchorToSuperview(leading: 20, trailing: -20, top: 20)
        lblRegisterSubtitle.addAnchorToSuperview(leading: 20, trailing: -20)
        
        lblRegisterSubtitle.topAnchor.constraint(equalTo: lblRegisterHeader.bottomAnchor, constant: 8).isActive = true
        lblRegisterSubtitle.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -20).isActive = true
        
        
        stackView.addAnchorToSuperview(leading: 20, trailing: -20, bottom: 0)
        
      
        
        stackView.addArrangedSubview(viewEnterName)
        viewEnterName.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
        
        stackView.addArrangedSubview(viewEnterMail)
        viewEnterMail.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
        
        stackView.addArrangedSubview(viewEnterNumber)
        viewEnterNumber.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
        
        stackView.addArrangedSubview(viewEnterPassword)
        viewEnterPassword.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
        
        stackView.addArrangedSubview(viewRetypePassword)
        viewRetypePassword.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
        
        stackView.addArrangedSubview(viewReferralId)
        viewReferralId.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true

        return formView
    }()
    
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    //MARK: InputViews
    
    lazy var viewEnterName : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_name".easyLocalized())
       
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        return textView
    }()

    lazy var viewEnterMail : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_email".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        textView.keyBoardType = .emailAddress
       
        return textView
    }()
    
    lazy var viewEnterNumber : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_number".easyLocalized())
        textView.keyBoardType = .phonePad
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
      
        return textView
    }()
    
    lazy var viewEnterPassword : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_password".easyLocalized())
        textView.isSecureText = true
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        
        return textView
    }()
    
    lazy var viewRetypePassword : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "re_type_password".easyLocalized())
        textView.isSecureText = true
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        
        return textView
    }()
    
    lazy var viewReferralId : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "referrel_id".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        
        return textView
    }()
    
    
    lazy var lblRegisterHeader: UILabel = {
        let lblTitle = UILabel()
        lblTitle.font = .EasyFont(ofSize: 18, style: .bold)
        lblTitle.textColor = .black
        lblTitle.text = "get_registered_with_easy".easyLocalized()
        lblTitle.textAlignment = .left
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        return lblTitle
    }()
    lazy var lblRegisterSubtitle : UILabel = {
        let lblTitle = UILabel()
        lblTitle.font = .systemFont(ofSize: 16, weight: .regular)
        lblTitle.textColor = .gray
        lblTitle.text = "enter_your_personal_information".easyLocalized()
        lblTitle.textAlignment = .left
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        return lblTitle
    }()
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "", leadingBtn: 10)
        return navView
    }()
    

}




extension RegistrationView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}
