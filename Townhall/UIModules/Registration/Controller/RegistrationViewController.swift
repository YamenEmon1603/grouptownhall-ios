//
//  RegistrationViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/7/21.
//

import UIKit

class RegistrationViewController: EasyBaseViewController {
    
    var identity : String?
    var name : String?
    var from : String?
    var loginId: String?
    
    var keyboardObserver : KeyboardObserver!
    lazy var mView: RegistrationView = {
        let view = RegistrationView()
        view.navView.backButtonCallBack = backAction
        view.proceedCallBack = proceed
        return view
    }()
    
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        keyboardObserver = KeyboardObserver(for: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
        if from == "facebook" || from == "google" || from == "apple"{
            EasyDataStore.sharedInstance.isSocialLogin = true
            mView.viewEnterName.text = name ?? ""
            mView.viewReferralId.isHidden = true
            mView.viewEnterPassword.isHidden = true
            mView.viewRetypePassword.isHidden = true
        }
        if identity?.isEmail ?? false{
            mView.viewEnterMail.text = identity ?? ""
        }else if identity?.isPhoneNumber ??  false{
            mView.viewEnterNumber.text = identity ?? ""
        }
    }
    
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func proceed(){
        var user : RegistrationInfo?
        guard let name = mView.viewEnterName.text, name != "" else{
            self.showToastError(message: "enter_name".easyLocalized())
            return
        }
        guard let email = mView.viewEnterMail.text, email != "" else{
            self.showToastError(message: "enter_email".easyLocalized())
            return
        }
        guard let phoneNo = mView.viewEnterNumber.text , phoneNo != "" else{
            self.showToastError(message: "enter_number".easyLocalized())
            return
        }
        guard phoneNo.isPhoneNumber && phoneNo.isElevenDigit else {
            self.showToastError(message: "invalid_phone_no".easyLocalized())
            
            return
        }
        guard email.isEmail else{
            self.showToastError(message: "invalid_email_address".easyLocalized())
            return
        }
        if EasyDataStore.sharedInstance.isSocialLogin == false{
            guard let password = mView.viewEnterPassword.text, password != "" else{
                self.showToastError(message: "enter_password".easyLocalized())
                return
            }
            if password.count < 6{
                self.showToastError(message: "invalid_password".easyLocalized())
                return
            }
            guard let retypePassword = mView.viewRetypePassword.text, retypePassword != ""  else {
                self.showToastError(message: "re_type_password".easyLocalized())
                
                return
            }
            guard password == retypePassword else{
                self.showToastError(message: "password_dont_match".easyLocalized())
                
                return
            }
            user = RegistrationInfo(name: name, email: email, password: password, passwordConfirmation: retypePassword, referrerKey: mView.viewReferralId.text ?? "", mobile: phoneNo, provider: "", socialLoginId: "")
        }else{
            user = RegistrationInfo(name: mView.viewEnterName.text ?? "" , email: email , password: "", passwordConfirmation: "", referrerKey: "", mobile: phoneNo, provider: from, socialLoginId: loginId ?? "")
        }
        guard  let user = user else {
            showToast(message: "Something went wrong!")
            return
        }
        requestRegister(user: user)
    }
    
    
    
}
extension RegistrationViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        var contentInset:UIEdgeInsets = mView.scrollView.contentInset
        contentInset.bottom = height + 20
        mView.scrollView.contentInset = contentInset
    }
    
    func keybaordWillHide() {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        mView.scrollView.contentInset = contentInset
    }
    
    
}


extension RegistrationViewController{
    
    func requestRegister(user: RegistrationInfo) {
        WebServiceHandler.shared.registration(reqModel: user, completion: { (result) in
            switch result{
            case .success(let registrationResponse):
                if registrationResponse.code == 200{
                    self.identity = user.mobile ?? ""
                    EasyManager.dataStore.isSocialLogin = user.provider?.isEmpty != false
                    let vc = OTPEntryViewController()
                    vc.identity = user.mobile
                    self.navigationController?.pushViewController(vc, animated: true)
                    break
                }else{
                    self.showToast(message: registrationResponse.message ?? "")
                }
                
                
            case .failure(let error) :
                self.showToast(message: error.localizedDescription)
                break
            }
            
        }, shouldShowLoader: true)
    }
}
