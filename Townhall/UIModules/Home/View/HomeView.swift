//
//  HomeView.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 11/3/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import Lottie

protocol HomeViewDelegate{
    func profileTabAction()
    func notificationTapAction()
    func topupAction()
    func utilityAction()
    func playAction()
    func  topUpOffersAction()
}

class HomeView: EasyBaseView {
    
    private let animationView = AnimationView()
    var delegate : HomeViewDelegate?
    override private init(frame: CGRect) {
        super.init(frame: frame)
    }
    @objc func showAnimationTapped(bgView:UIView){
        let logoAnimation = AnimationView(name: "lottie")
        logoAnimation.contentMode = .scaleAspectFit
        logoAnimation.loopMode = .repeat(2)
        logoAnimation.translatesAutoresizingMaskIntoConstraints = false
        bgView.addSubview(logoAnimation)
        
        logoAnimation.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        logoAnimation.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        logoAnimation.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width).isActive = true
        logoAnimation.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width).isActive = true
        

        
        logoAnimation.play(completion: {_ in
            logoAnimation.removeFromSuperview()
        })
    }
    convenience init(_ delegate: HomeViewDelegate) {
        self.init(frame:.zero)
        backgroundColor = .paleGreyThree
        self.delegate = delegate
        
        let containerView = UIView()
        addSubview(containerView)
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: -.calculatedHeight(70))
        
        containerView.addSubview(backgroundImageView)
        backgroundImageView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0)
        
        containerView.addSubview(bgGroupImageView)
        bgGroupImageView.addAnchorToSuperview(leading: 0, trailing: 0)
        bgGroupImageView.centerYAnchor.constraint(equalTo: backgroundImageView.centerYAnchor,constant: -.calculatedHeight(50)).isActive = true
        
//        showAnimationTapped(bgView:bgGroupImageView)
        
        //MARK:- TOP VIEW
        containerView.addSubview(topView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.30)
        
        
        //MARK:- MIDDLE VIEW
        containerView.addSubview(middleView)
        middleView.addAnchorToSuperview(leading: 0, trailing: 0, heightMultiplier: 0.5)
        middleView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: middleView.bottomAnchor,constant: -50).isActive = true
        
     
        //MARK:- BOTTOM VIEW
        containerView.addSubview(bottomView)
        bottomView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
        bottomView.topAnchor.constraint(equalTo: middleView.bottomAnchor, constant: -5).isActive = true
           
        EasyManager.sharedInstance.delay(0.1) {
            self.profileImageView.layer.cornerRadius = min(self.profileImageView.frame.width, self.profileImageView.frame.height)/2
            self.profileImageView.clipsToBounds = true
            self.profileImageView.layer.borderColor = UIColor.paleGrey.cgColor
            self.profileImageView.layer.borderWidth = 0.5
//            self.topUpBtn.centerVertically(padding: .calculatedHeight(20))
//            self.utilityBtn.centerVertically(padding: .calculatedHeight(20))
        }
    }
    
    required internal init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var topView : UIView  = {
        let view = UIView()
        view.backgroundColor = .clear
        view.addSubview(navigationProfileBarView)
        navigationProfileBarView.addAnchorToSuperview(leading: .calculatedWidth(20), trailing: -.calculatedWidth(20), top: .calculatedHeight(35), heightMultiplier: 0.25)
        
        return view
    }()
    
    lazy var bgGroupImageView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "homeTopViewImage")
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    lazy var navigationProfileBarView : UIView  = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.addSubview(profileImageView)
        profileImageView.addAnchorToSuperview(leading: 10,heightMultiplier: 0.7,  centeredVertically: 0,  heightWidthRatio: 1)
        view.addSubview(appLogo)
        appLogo.addAnchorToSuperview(heightMultiplier: 0.66, widthMutiplier: 0.17, centeredVertically: 0, centeredHorizontally: 0 )
        view.addSubview(notificationLogo)
        notificationLogo.addAnchorToSuperview(trailing: 0, heightMultiplier: 1,centeredVertically: 0,  heightWidthRatio: 1)
        
        return view
    }()
    
    lazy var appLogo: UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "homeHeader")
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    lazy var notificationLogo: UIButton = {
        let notifyBtn = UIButton()
        notifyBtn.translatesAutoresizingMaskIntoConstraints = false
        notifyBtn.setImage(UIImage(named: "notifications"), for: .normal)
        notifyBtn.contentMode = .scaleToFill
        notifyBtn.addTarget(self, action: #selector(notificationTapAction(_:)), for: .touchUpInside)
        return notifyBtn
    }()
    @objc func notificationTapAction(_ sender : UIButton){
        self.delegate?.notificationTapAction()
    }
    
    lazy var profileImageView : UIButton = {
        let profBtn = UIButton()
        profBtn.translatesAutoresizingMaskIntoConstraints = false
        profBtn.setImage(UIImage(named: "homeTopViewImage"), for: .normal)
        profBtn.contentMode = .scaleAspectFit
        profBtn.layer.masksToBounds = true
        profBtn.addTarget(self, action: #selector(profileBtnTapAction(_:)), for: .touchUpInside)
        return profBtn
    }()
    @objc func profileBtnTapAction(_ sender: UIButton){
        self.delegate?.profileTabAction()
    }
    
    lazy var middleView : UIView  = {
        let middleView = UIView()
        middleView.addSubview(midHeaderLabel)
        midHeaderLabel.addAnchorToSuperview(leading: 20, trailing: -20, top: -.calculatedHeight(0))
        midHeaderLabel.backgroundColor = .clear
        
       
        
        middleView.addSubview(infoView)
        infoView.addAnchorToSuperview(leading: 20, trailing: -20,heightMultiplier: 0.17)
        infoView.topAnchor.constraint(equalTo: midHeaderLabel.bottomAnchor, constant: 15).isActive = true
       
        middleView.addSubview(serviceself)
        middleView.backgroundColor = .clear
        serviceself.addAnchorToSuperview(leading: 20, trailing: -20, bottom: -20)
        serviceself.topAnchor.constraint(equalTo: infoView.bottomAnchor, constant: .calculatedHeight(50)).isActive = true
        
        
        
        return middleView
    }()
    lazy var infoView : UIView = {
        let view = UIView()
        view.addSubview(dateView)
        dateView.addAnchorToSuperview(leading: 0, top: 0, bottom: 0)
        dateView.widthAnchor.constraint(equalTo: dateView.heightAnchor).isActive = true
        view.addSubview(dayVenueStack)
        dayVenueStack.addAnchorToSuperview(top:3,bottom:-3)
        dayVenueStack.leadingAnchor.constraint(equalTo: dateView.trailingAnchor,constant: 10).isActive = true
        
        view.addSubview(countDownTimerView)
        countDownTimerView.addAnchorToSuperview(trailing: -5, top: 2.5, bottom: -2.5,  widthMutiplier: 0.33)
        countDownTimerView.leadingAnchor.constraint(equalTo: dayVenueStack.trailingAnchor).isActive = true
        return view
    }()
    lazy var venueLabel : UILabel = {
        let venueLabel = UILabel()
        venueLabel.translatesAutoresizingMaskIntoConstraints = false
        venueLabel.textAlignment = .center
        venueLabel.font = .EasyFont(ofSize: 12.0, style: .medium)
        venueLabel.textColor = .white
        venueLabel.text = "venue :"
       
        return venueLabel
    }()
    lazy var dayLabel : UILabel = {
        let dayLabel = UILabel()
        dayLabel.textAlignment = .center
        dayLabel.font = .EasyFont(ofSize: 16.0, style: .bold)
        dayLabel.textColor = .white
        dayLabel.text = ""
       
        return dayLabel
    }()
    lazy var dayVenueStack : UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.spacing = 2
        
       
        stackView.addArrangedSubview(dayLabel)
        stackView.addArrangedSubview(venueLabel)
        return stackView
    }()
    let dayNameLabel = UILabel()
    let dateLabel = UILabel()
    
    lazy var dateView : UIView = {
        let dateView = UIView()
        dateView.backgroundColor = .sunYellow
        dateView.layer.cornerRadius = 6
        
        dayNameLabel.textAlignment = .center
        dayNameLabel.font = .EasyFont(ofSize: 12.0, style: .medium)
        dayNameLabel.textColor = .black
        dayNameLabel.text = "JAN"
        dateView.addSubview(dayNameLabel)
        dayNameLabel.addAnchorToSuperview(leading: 0, trailing: 0, top: 2.5, heightMultiplier: 0.3)
        
        
        dateLabel.textAlignment = .center
        dateLabel.font = .EasyFont(ofSize: 18.0, style: .bold)
        dateLabel.textColor = .black
        dateLabel.text = "13"//Date().asString(format: "dd")
        dateView.addSubview(dateLabel)
        dateLabel.addAnchorToSuperview(leading: 0, trailing: 0, bottom: -2.5, heightMultiplier: 0.7)
        return dateView
    }()

    lazy var countDownTimerView: UIView = {
       let view = UIView()
        view.backgroundColor = .clear
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 3
        view.addSubview(stack)
        stack.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        stack.addArrangedSubview(days)
        stack.addArrangedSubview(hours)
        stack.addArrangedSubview(minitues)
        return view
    }()
    let days : CountdownItem = {
        let days = CountdownItem()
        days.labeldes.text = "Days"
        return days
    }()
    let hours : CountdownItem = {
        let days = CountdownItem()
        days.labeldes.text = "Hours"
        return days
    }()
    let minitues : CountdownItem = {
        let days = CountdownItem()
        days.labeldes.text = "Min"
        return days
    }()
    lazy var backgroundImageView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "homeBg")
       // imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    lazy var midHeaderLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        let factX = UIScreen.main.bounds.size.height/720
        let transtext = NSString(string: "Welcome to\n")
        let attributedString = NSMutableAttributedString(string: transtext as String, attributes: [
            .font: UIFont.EasyFont(ofSize: 20, style: .bold),
            .foregroundColor: UIColor.white
        ])
        attributedString.addAttribute(.font, value: UIFont.EasyFont(ofSize: 16, style: .bold), range: transtext.range(of: "Welcome to"))
        
        label.attributedText = attributedString
        label.numberOfLines = 3
        label.sizeToFit()
        return label
    }()
    
    
    lazy var serviceself : UIView = {
        let serviceself  = UIView()
        serviceself.addSubview(rechargeBtnView)
        rechargeBtnView.backgroundColor = .white
        rechargeBtnView.addShadow(location: .bottom,color: UIColor.black,opacity: 0.35,radius: 10)
        rechargeBtnView.addAnchorToSuperview(leading: 0,top: 0, bottom: 0, widthMutiplier: 0.32)
        let instructLabel = UILabel()
        instructLabel.text = "Program Instructions"
        instructLabel.textAlignment = .center
        instructLabel.numberOfLines = 2
        instructLabel.translatesAutoresizingMaskIntoConstraints = false
        instructLabel.font = .EasyFont(ofSize: 13, style: .medium)
        instructLabel.sizeToFit()
        
        rechargeBtnView.addSubview(instructLabel)
        
        let instructImage = UIImageView()
        rechargeBtnView.addSubview(instructImage)
        instructImage.image = UIImage(named: "topup")
        instructImage.translatesAutoresizingMaskIntoConstraints = false
        instructImage.addAnchorToSuperview(widthMutiplier: 0.48, heightWidthRatio: 1)
        instructImage.contentMode = .scaleAspectFit
        instructImage.bottomAnchor.constraint(equalTo: instructLabel.topAnchor, constant: -10).isActive = true
        instructLabel.addAnchorToSuperview(leading: 10, trailing: -10)
        instructImage.addAnchorToSuperview(centeredHorizontally: 0)
        let instructlabelBottomConstarint = instructLabel.bottomAnchor.constraint(equalTo: rechargeBtnView.bottomAnchor,constant: 0)
        instructlabelBottomConstarint.isActive = true
        EasyUtils.shared.delay(0.5) {
            instructlabelBottomConstarint.constant = -(self.rechargeBtnView.frame.height - (instructLabel.frame.height+instructImage.frame.height+10)) / 2
        }
        let instructionTap = UITapGestureRecognizer(target:self,action: #selector(topUpBtnAction(_:)))
        rechargeBtnView.addGestureRecognizer(instructionTap)

        serviceself.addSubview(utilityBtnView)
        utilityBtnView.addShadow(location: .bottom,color: UIColor.black,opacity: 0.35,radius: 10)
        utilityBtnView.addAnchorToSuperview(top: 0, bottom: 0, widthMutiplier: 0.32)
        utilityBtnView.leadingAnchor.constraint(equalTo: rechargeBtnView.trailingAnchor, constant: 10).isActive = true
        
        
        let connectLabel = UILabel()
        connectLabel.text = "Connect With Bot"
        connectLabel.textAlignment = .center
        connectLabel.numberOfLines = 2
        connectLabel.translatesAutoresizingMaskIntoConstraints = false
        connectLabel.font = .EasyFont(ofSize: 13, style: .medium)
        connectLabel.sizeToFit()
        
        utilityBtnView.addSubview(connectLabel)
        
        let connectImage = UIImageView()
        utilityBtnView.addSubview(connectImage)
        connectImage.image = UIImage(named: "utility")
        connectImage.translatesAutoresizingMaskIntoConstraints = false
        connectImage.addAnchorToSuperview(widthMutiplier: 0.48, heightWidthRatio: 1)
        connectImage.bottomAnchor.constraint(equalTo: connectLabel.topAnchor, constant: -10).isActive = true
        connectImage.contentMode = .scaleAspectFit
        connectLabel.addAnchorToSuperview(leading: 10, trailing: -10)
        connectImage.addAnchorToSuperview(centeredHorizontally: 0)
        let connlabelBottomConstarint = connectLabel.bottomAnchor.constraint(equalTo: utilityBtnView.bottomAnchor,constant: 0)
        connlabelBottomConstarint.isActive = true
        EasyUtils.shared.delay(0.5) {
            connlabelBottomConstarint.constant = -(self.utilityBtnView.frame.height - (connectLabel.frame.height+connectImage.frame.height+10)) / 2
        }

        let connectTap = UITapGestureRecognizer(target:self,action: #selector(connectBtnAction(_:)))
        utilityBtnView.addGestureRecognizer(connectTap)
        
        
        
        serviceself.addSubview(gameBtnView)
        gameBtnView.addShadow(location: .bottom,color: UIColor.black,opacity: 0.35,radius: 10)
        gameBtnView.addAnchorToSuperview(top: 0, bottom: 0, widthMutiplier: 0.32)
        gameBtnView.leadingAnchor.constraint(equalTo: utilityBtnView.trailingAnchor, constant: 10).isActive = true
        
        let buttonLabel = UILabel()
        buttonLabel.text = "Play Game"
        buttonLabel.textAlignment = .center
        buttonLabel.numberOfLines = 2
        buttonLabel.translatesAutoresizingMaskIntoConstraints = false
        buttonLabel.font = .EasyFont(ofSize: 13, style: .medium)
        buttonLabel.sizeToFit()
        
        gameBtnView.addSubview(buttonLabel)
        
        let gameImage = UIImageView()
        gameBtnView.addSubview(gameImage)
        gameImage.image = UIImage(named: "dice")
        gameImage.translatesAutoresizingMaskIntoConstraints = false
        gameImage.addAnchorToSuperview(widthMutiplier: 0.48, heightWidthRatio: 1)
        gameImage.bottomAnchor.constraint(equalTo: buttonLabel.topAnchor, constant: -10).isActive = true
        buttonLabel.addAnchorToSuperview(leading: 10, trailing: -10)
        gameImage.addAnchorToSuperview(centeredHorizontally: 0)
        let labelBottomConstarint = buttonLabel.bottomAnchor.constraint(equalTo: gameBtnView.bottomAnchor,constant: 0)
        labelBottomConstarint.isActive = true
        EasyUtils.shared.delay(0.5) {
            labelBottomConstarint.constant = -(self.gameBtnView.frame.height - (buttonLabel.frame.height+gameImage.frame.height+10)) / 2
        }
        let playTap = UITapGestureRecognizer(target:self,action: #selector(playBtnAction(_:)))
        gameBtnView.addGestureRecognizer(playTap)
        
        return serviceself
    }()
    
    lazy var rechargeBtnView : UIView  = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        return view
    }()
    
    lazy var gameBtnView : UIView  = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        return view
    }()
    @objc func playBtnAction(_ sender: UITapGestureRecognizer){
        self.delegate?.playAction()
    }
   
    @objc func topUpBtnAction(_ sender: UITapGestureRecognizer){
        self.delegate?.topupAction()
    }
    
    @objc func connectBtnAction(_ sender: UITapGestureRecognizer){
        self.delegate?.utilityAction()
    }
    
    lazy var utilityBtnView : UIView  = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        return view
    }()
    
   
    
    @objc func utilityBtnAction(_ sender: UITapGestureRecognizer){
        self.delegate?.utilityAction()
    }
    lazy var bottomView : UIView  = {
        let bottomView = UIView()
        bottomView.addSubview(bottomImageself)
        bottomImageself.addShadow(location: .bottom,color: UIColor.black,opacity: 0.35,radius: 10)
        bottomImageself.addAnchorToSuperview(leading: 20, trailing: -20, top: 0, bottom: -30)
        
        bottomImageself.addSubview(bgImageBottomView)
        bgImageBottomView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        bottomImageself.addSubview(leftImageBottomView)
        leftImageBottomView.addAnchorToSuperview(leading: 15, heightMultiplier: 0.7,centeredVertically: 0,heightWidthRatio: 0.87)
        
        let stackView = UIStackView(frame: .zero)
        bottomImageself.addSubview(stackView)
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.addAnchorToSuperview(trailing: -20, centeredVertically: 0)
        stackView.leadingAnchor.constraint(equalTo: leftImageBottomView.trailingAnchor,constant: 15).isActive = true
        stackView.addArrangedSubview(offerTopLabel)
        stackView.addArrangedSubview(offerBottomLabel)
        
        return bottomView
    }()
    
    
    lazy var bottomImageself: UIView = {
        let imgView = UIView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .blueViolet
        imgView.layer.cornerRadius = 10
        imgView.layer.masksToBounds = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(showTopUpOffresAction(_:)))
        tapgesture.cancelsTouchesInView = false
        imgView.addGestureRecognizer(tapgesture)
        return imgView
    }()
    @objc func showTopUpOffresAction(_ gesture : UITapGestureRecognizer){
        self.delegate?.topUpOffersAction()
    }
    
    lazy var leftImageBottomView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "experience")
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    lazy var bgImageBottomView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "bannerBg")
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    lazy var offerTopLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Experience Zone"
        label.font = .EasyFont(ofSize: 18, style: .bold)
        label.textColor = .white
        return label
    }()
    
    lazy var offerBottomLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Explore our valuable products"
        label.font = .EasyFont(ofSize: 13.0, style: .medium)
        label.textColor = .white
        return label
    }()
}
class CountdownItem: UIView  {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(){
        self.init(frame: .zero)
        self.addSubview(labeldes)
       
        labeldes.adjustsFontSizeToFitWidth = true
        labeldes.addAnchorToSuperview(leading: 5, trailing: -5,  bottom: 0)
        
        self.addSubview(labelRemain)
        labelRemain.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,heightMultiplier: 0.7,heightWidthRatio: 1)
        
        labelRemain.bottomAnchor.constraint(equalTo: labeldes.topAnchor,constant: -3).isActive = true
    }
    let labeldes: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 8.0, style: .regular)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "Days"
        return label
    }()
    let labelRemain: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "8"
        label.layer.cornerRadius = 4
        label.clipsToBounds = true
        label.backgroundColor = .lighterPurple
        return label
    }()
}
