//
//  ConnectBottomView.swift
//  Townhall
//
//  Created by Mausum Nandy on 8/12/21.
//

import UIKit
import SwiftUI

class ConnectBottomView: UIView {

    override private init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    let items : [(name:String,icon:UIImage,tag:Int)] = [(name:"Contact with WhatsApp",icon:UIImage(named: "whatsapp")!,tag:0),(name:"Contact with Messenger",icon:UIImage(named: "messenger")!,tag:1),(name:"Contact with Telegram",icon:UIImage(named: "telegram")!,tag:2)]
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let buttonsStack = UIStackView()
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .black.withAlphaComponent(0.5)
        let closebtn = UIButton()
        addSubview(closebtn)
        closebtn.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        closebtn.addTarget(self, action: #selector(close), for: .touchUpInside)
        let contentView = UIView()
        contentView.backgroundColor = .paleLilac
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        closebtn.bottomAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        
        contentView.addSubview(buttonsStack)
        contentView.layer.cornerRadius = 10
        contentView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        buttonsStack.addAnchorToSuperview(leading: 15, trailing: -15, top: 20, bottom: -20)
        buttonsStack.axis = .vertical
        buttonsStack.spacing = 6
        buttonsStack.distribution = .fillEqually
        buttonsStack.translatesAutoresizingMaskIntoConstraints = false
        
        items.forEach { item in
            let v = UIView()
            v.tag = item.tag
            v.backgroundColor = .white
            v.layer.cornerRadius = 10
            v.translatesAutoresizingMaskIntoConstraints  = false
            v.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
            let img = UIImageView(image:item.icon)
            v.addSubview(img)
            img.addAnchorToSuperview(leading: 20,heightMultiplier: 0.7, centeredVertically: 0, heightWidthRatio: 1)
            let label = UILabel()
            label.font = .EasyFont(ofSize: 14, style: .medium)
            label.text = item.name
            v.addSubview(label)
            label.addAnchorToSuperview( trailing: -15, top: 5, bottom: -5)
            label.leadingAnchor.constraint(equalTo: img.trailingAnchor,constant: 15).isActive  = true
            let tapgesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
            v.addGestureRecognizer(tapgesture)
            
            buttonsStack.addArrangedSubview(v)
        }
        
    }
    @objc func handleTapGesture(_ gesture : UITapGestureRecognizer){
        if let v = gesture.view{
            print(v.tag)
            
            switch v.tag {
            case 0:
                print("WhatsAPP")
                if let link = EasyDataStore.sharedInstance.dashboardContent?.townhall?.bot?.whatsapp{
                    openSocial(mUrl: link)
                }
            case 1:
                print("Messenger")
                if let link = EasyDataStore.sharedInstance.dashboardContent?.townhall?.bot?.messenger{
                    openSocial(mUrl: link)
                }
            case 2:
                if let link = EasyDataStore.sharedInstance.dashboardContent?.townhall?.bot?.telegram{
                    let url = URL(fileURLWithPath: link)
                    let path  = url.deletingPathExtension().lastPathComponent
                    let appURL = NSURL(string: "tg://resolve?domain=\(path)")!
                    let webURL = NSURL(string: "https://t.me/\(path)")!
                    if UIApplication.shared.canOpenURL(appURL as URL) {
                        openSocial(mUrl : appURL.absoluteString ?? "")
                    }
                    else {
                        openSocial(mUrl : webURL.absoluteString ?? "")
                    }
                    
                    
                }
            default:
                break
            }
        }
          
    }
    func openSocial(mUrl : String){
       
        
        if let appURL = URL(string: "\(mUrl)"){
            self.removeFromSuperview()
            if UIApplication.shared.canOpenURL(appURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(appURL)
                }
            }
        }
        
    }
    @objc func close(){
        self.removeFromSuperview()
    }
}

