//
//  HomeVC.swift
//  Easy.com.bd
//
//  Created by Yamen Emon on 10/3/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class HomeViewController: EasyBaseViewController {
    //var mbOperators : [MBOperator]?
    //  var plans :RechargePlanResponse?
    var programTime : String = "12:00PM - 8:15PM"
    var timer:Timer?
    lazy var containerView : HomeView  = {
        let view = HomeView(self)
        return view
    }()
    let bottomShit : ConnectBottomView = ConnectBottomView()
    var programDate : Date = Date()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = containerView
        updateTimer()
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        fetchUser()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        containerView.showAnimationTapped(bgView: containerView.backgroundImageView)
        getContent()
        
       
    }
    @objc func date(toTimestamp dateStr: String?, dateFormate: String?, expectedDateFormate: String) -> String {
        let dateFormatter = DateFormatter()
            // This is important - we set our input date format to match our input string
            // if the format doesn't match you'll get nil from your string, so be careful
        dateFormatter.dateFormat = dateFormate//"yyyy-MM-dd"
        
            //`date(from:)` returns an optional so make sure you unwrap when using.
        let dateFromString: Date? = dateFormatter.date(from: dateStr ?? "")
        
        let formatter = DateFormatter()
        let getFormat = expectedDateFormate
        
        formatter.dateFormat = getFormat//"MMM dd, yyyy"
        guard dateFromString != nil else { return ""}
        
            //Using the dateFromString variable from before.
        let stringDate: String = formatter.string(from: dateFromString!)
        return stringDate
    }
    @objc func updateTimer (){
        // here we set the current date
        
        let timeS = self.programTime.components(separatedBy: ["-"])
        print(timeS)
        
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour, .minute, .month, .year, .day], from: Date())
        
        let currentDate = calendar.date(from: components)
        // here we set the due date. When the timer is supposed to finish
        let competitionDate = NSDateComponents()
        competitionDate.year = Int(programDate.asString(format: "yyyy")) ?? 2022
        competitionDate.month = Int(programDate.asString(format: "MM")) ?? 01
        competitionDate.day = Int(programDate.asString(format: "dd")) ?? 13
        competitionDate.hour = Int(date(toTimestamp: timeS[0], dateFormate: "hh:mma", expectedDateFormate: "hh")) ?? 22
        competitionDate.minute = 00
        let competitionDay = calendar.date(from: competitionDate as DateComponents)!
        
        //here we change the seconds to hours,minutes and days
        let CompetitionDayDifference = calendar.dateComponents([.day, .hour, .minute], from: currentDate!, to:competitionDay )
        
        
        //finally, here we set the variable to our remaining time
        let daysLeft = CompetitionDayDifference.day
        let hoursLeft = CompetitionDayDifference.hour
        let minutesLeft = CompetitionDayDifference.minute
        
       
        
        containerView.days.labelRemain.text = String(format: "%02d", daysLeft ?? 00) 
        containerView.hours.labelRemain.text = String(format: "%02d", hoursLeft ?? 00)
        containerView.minitues.labelRemain.text = String(format: "%02d", minutesLeft ?? 00)
        //Set countdown label text
        //countDownLabel.text = "\(daysLeft ?? 0) Days, \(hoursLeft ?? 0) Hours, \(minutesLeft ?? 0) Minutes"
    }
}
//MARK:-  HomeViewDelegate

extension HomeViewController : HomeViewDelegate{
    func playAction() {
        let vc = GameScannerViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func profileTabAction() {
        let vc = ProfileInfoViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func notificationTapAction() {
        let nc = UINavigationController(rootViewController: NotificationsViewController())
        nc.setNavigationBarHidden(true, animated: true)
        nc.modalPresentationStyle = .fullScreen
        self.present(nc, animated: true, completion: nil)
    }
    
    func topupAction() {
        let vc = ProgramInstructionVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func utilityAction() {
        self.tabBarController?.view.addSubview(bottomShit)
        bottomShit.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
    }
    
    func topUpOffersAction() {
        let vc = ExperienceZoneVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension HomeViewController{
    func fetchUser(){
        WebServiceHandler.shared.getProfileDetails(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    EasyDataStore.sharedInstance.user = response.data
                    if let user = EasyDataStore.sharedInstance.user{
                        if let own = EasyDataStore.sharedInstance.ownNumber,own.number?.isEmpty == false{
                        
                        }else{
                            EasyDataStore.sharedInstance.ownNumber = SelectNumber(number: user.mobile, operatorObj: nil, connetctionType: nil, amount: nil, scheduleTime: nil)
                        }
                        if let imgURL = URL(string: response.data?.profilePictureUrl ?? "") {
                            self.containerView.profileImageView.kf.setImage(with: imgURL, for: .normal)
                        }
                    }
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                debugPrint(error)
            }
        }, shouldShowLoader: false)
    }
    func getContent(){
        WebServiceHandler.shared.getContent(completion: { result in
            switch result{
            case .success(let response):
                if response.code == 200{
                   
                    EasyDataStore.sharedInstance.dashboardContent = response.data
                    if response.data?.imageUploadStatus == 1{
                        if let headerTitle = response.data?.townhall?.title{
                        let transtext = NSString(string: "Welcome to\n\(headerTitle)")
                        let attributedString = NSMutableAttributedString(string: transtext as String, attributes: [
                            .font: UIFont.EasyFont(ofSize: 20.0, style: .bold),
                            .foregroundColor: UIColor.white
                        ])
                        attributedString.addAttribute(.font, value: UIFont.EasyFont(ofSize: 16.0, style: .bold), range: transtext.range(of: "Welcome to"))
                        self.containerView.midHeaderLabel.attributedText = attributedString
                        }
                        
                        if let vanue = response.data?.townhall?.vanue{
                            self.containerView.venueLabel.text = "venue : \(vanue)"
                        }
                        let fDate = DateFormatter()
                        fDate.dateFormat = "dd MMMM yyyy"
                        
                        if let week = response.data?.townhall?.date,let date = fDate.date(from: week){
                            self.containerView.dayLabel.text = date.asString(format: "EEEE")
                            self.containerView.dayNameLabel.text = date.asString(format: "MMM").uppercased()
                            self.containerView.dateLabel.text = date.asString(format: "dd")
                            self.programDate = date
                            self.programTime = response.data?.townhall?.time ?? "12:00PM - 8:15PM"
                            self.updateTimer()
                        }
                        
                      
                    }else
                    if let mobile = EasyDataStore.sharedInstance.user?.mobile{
                        let vc = ImageUploadViewController()
                        vc.employeePhoneNumer = mobile
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                let alert  = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let tryBtn = UIAlertAction(title: "Try Again", style: .cancel) { alert in
                    self.getContent()
                }
                alert.addAction(tryBtn)
                self.present(alert, animated: true, completion: nil)
                
            }
        }, shouldShowLoader: true)
    }
}
