//
//  OffersViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit

class OffersViewController: EasyBaseViewController {

    lazy var mView : OffersView = {
        let mView = OffersView()
        mView.tableView.delegate = self
        mView.tableView.dataSource = self
        return mView
    }()
    let  noDataView : NoDataView = {
        let noDataView = NoDataView(image: UIImage(named: "noOffer")!, text: "no_offer_found".easyLocalized(), subtext: "there_are_currently_no_offer_for_you".easyLocalized())
        
        return noDataView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        fetchOffers()
    }
    
    var offers : [OfferData]?{
        didSet{
            selectedOffer = offers?.first
            
        }
    }
    var selectedOffer : OfferData?{
        didSet{
            if selectedOffer?.exclusiveOffers?.count == 0 || selectedOffer?.exclusiveOffers == nil {
                mView.tableView.isHidden = true
                mView.addSubview(noDataView)
                noDataView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
                noDataView.topAnchor.constraint(equalTo: mView.navView.bottomAnchor, constant: 0).isActive = true
            }else{
                noDataView.removeFromSuperview()
                mView.tableView.isHidden = false
            }
            mView.tableView.reloadData()
        }
    }
    

}
extension OffersViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  selectedOffer?.exclusiveOffers?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferCell.identifier, for: indexPath) as!  OfferCell
        cell.exclusiveOffer = selectedOffer?.exclusiveOffers?[indexPath.row]
        return cell
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}
extension OffersViewController{
    func fetchOffers() {
        WebServiceHandler.shared.getExclusiveOffers(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.offers = response.data
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToast(message: error.localizedDescription )
            }
        }, shouldShowLoader: true)
    }
}
