//
//  OfferCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit

class OfferCell: UITableViewCell {
    
    static let identifier = "offerCell"
    
    var exclusiveOffer:ExclusiveOffer?{
        didSet{
           
            lblOfferTitle.text = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? exclusiveOffer?.title : exclusiveOffer?.titleBn
            lblOffersubTitle.numberOfLines = 0
            lblOffersubTitle.text = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? exclusiveOffer?.exclusiveOfferDescription : exclusiveOffer?.descriptionBn
            lblOffersubTitle.sizeToFit()
            offerImage.image = nil
            if let imgUrl  =  exclusiveOffer?.banner , imgUrl != ""{
                offerImage.kf.setImage(with: URL(string:imgUrl))
            }
            if let code = exclusiveOffer?.code , code != ""{
               // btnUseCode.isHidden = false
                let string = "\(SSLComLanguageHandler.sharedInstance.getCurrentLanguage()  == .English ? exclusiveOffer?.codeTitle ?? "" : exclusiveOffer?.codeTitleBn ?? ""): \(code)"
                let _ = NSMutableAttributedString(string: string, attributes: [
                    .font: UIFont.EasyFont(ofSize: 13, style: .regular),
                  .foregroundColor: UIColor.blueyGrey
                ])

            }else{

            }
           
           
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        let container = UIView()
        contentView.addSubview(container)
        contentView.addSubview(offerStickerView)
        offerStickerView.addAnchorToSuperview(leading: 11, top: -10)
        offerStickerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(45)).isActive = true
        offerStickerView.widthAnchor.constraint(equalToConstant: .calculatedWidth(90)).isActive = true
        
        container.addAnchorToSuperview(leading: 15, trailing: -15, top: 5, bottom: -5)
        container.layer.borderWidth = 1
        container.layer.borderColor = UIColor.paleLilac.cgColor
        container.layer.cornerRadius = 10
        container.addSubview(dataView)
        
        
        
        dataView.addAnchorToSuperview(leading : 0 ,trailing: 0, top: 0, bottom: 0)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var dataView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        view.addSubview(offerImage)
        view.addSubview(lblOfferTitle)
        view.addSubview(lblOffersubTitle)
        view.addSubview(timelineView)
      
        offerImage.addAnchorToSuperview(trailing: -20,top:10,bottom: -10, widthMutiplier: 0.2)
        
        lblOfferTitle.addAnchorToSuperview(leading: 20, top: 15)
        lblOffersubTitle.addAnchorToSuperview(leading:20)
        lblOffersubTitle.trailingAnchor.constraint(equalTo: offerImage.leadingAnchor, constant: -8).isActive = true
        lblOffersubTitle.topAnchor.constraint(equalTo: lblOfferTitle.bottomAnchor, constant: 8).isActive = true
       
        timelineView.addAnchorToSuperview(bottom: -15)
        timelineView.leadingAnchor.constraint(equalTo: lblOffersubTitle.leadingAnchor, constant: 0).isActive = true
        timelineView.heightAnchor.constraint(equalToConstant: .calculatedHeight(20)).isActive = true
        timelineView.topAnchor.constraint(equalTo: lblOffersubTitle.bottomAnchor, constant: 5).isActive = true
        
     
        
        return view
    }()
    
    lazy var offerImage : UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "noOffer")
        return view
    }()
    
    lazy var timelineView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .clear
        view.addSubview(imgTimeline)
        view.addSubview(lblTimeline)
        imgTimeline.addAnchorToSuperview(leading: 2, top: 4, bottom: -4, widthMutiplier: 0.3)
        lblTimeline.addAnchorToSuperview(trailing: -4, top: 2, bottom: -2)
        lblTimeline.leadingAnchor.constraint(equalTo: imgTimeline.trailingAnchor, constant: 0).isActive = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.paleLilac.cgColor
        view.layer.cornerRadius = 4
        return view
    }()
    
    
    lazy var imgTimeline: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "miniCalender")
        return view
    }()
    
    
    lazy var lblTimeline: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 10, style: .regular)
        view.text = "107 Days"
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    
    lazy var lblOffersubTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 11, style: .regular)
        view.text = "Up to 1200Tk. CASHBACK when you,pay any kind of bill through easy. Up to 1200Tk cashback."
        view.textColor = .gunMetal
        view.textAlignment = .left
        view.numberOfLines = 0
        
        return view
    }()
    lazy var lblOfferTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 14, style: .bold)
        view.text = "Up to ৳1,200 Cashback"
        view.textColor = .gunMetal
        view.textAlignment = .left
        view.numberOfLines = 0
        
        return view
    }()

    lazy var offerStickerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .clear
        view.addSubview(imgOfferSticker)
        view.addSubview(lblOfferName)
        imgOfferSticker.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        lblOfferName.addAnchorToSuperview(leading: 4, trailing: -4, centeredVertically: -2)
        
        return view
    }()
    
    lazy var imgOfferSticker: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "hotOfferIcon")
        return view
    }()
    lazy var lblOfferName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 9, style: .medium)
        view.text = "EID SPECIAL"
        view.textColor = .white
        view.textAlignment = .center
        return view
    }()
    
    
    
}
