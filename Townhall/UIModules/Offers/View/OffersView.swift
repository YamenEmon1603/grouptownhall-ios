//
//  OffersView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit

class OffersView: EasyBaseView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .iceBlue
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: -.calculatedHeight(70))
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.075)
        
        contentView.backgroundColor = .white
        containerView.addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor, constant: 0).isActive = true
        

        contentView.addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0, top: 20, bottom: 0)
    }
    let contentView  = UIView()
    lazy var navView: UIView = {
        let navView = UIView()
        let imageView = UIImageView(image: UIImage(named: "easyoffer"))
        navView.addSubview(imageView)
        imageView.addAnchorToSuperview(leading: 20, top: 10,bottom: -10, centeredVertically: 0)
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        return navView
    }()
    
     
    lazy var tableView : UITableView = {
        let tableView = UITableView()
      
        tableView.register(OfferCell.self, forCellReuseIdentifier: OfferCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        return tableView
    }()
    let  noDataView : NoDataView = {
        let noDataView = NoDataView(image: UIImage(named: "noOffer")!, text: "No Offer Found!", subtext: "There are currently no offer for you.Please try for another.")
        return noDataView
    }()
   
}
