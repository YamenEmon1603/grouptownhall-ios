//
//  PhoneEntryViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/8/21.
//

import UIKit

class PhoneEntryViewController: EasyBaseViewController {
    
    lazy var mView: PhoneEntryView = {
        let mView = PhoneEntryView()
        mView.proceedCallBack = proceed
        mView.navView.backButtonCallBack = backAction
        return mView
    }()
    var keyboardObserver : KeyboardObserver!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
      //  keyboardObserver = KeyboardObserver(for: self)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.termsTapped))
        mView.termsAndConditionLabel.isUserInteractionEnabled = true
        mView.termsAndConditionLabel.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // keyboardObserver.add()
    }
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func termsTapped(sender:UITapGestureRecognizer) {
        let vc = WebPageVC()
        vc.type = .terms
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func proceed(){
        dismissKeyboard()
        guard let senderMobile =  mView.numberView.text,senderMobile.isEmpty == false else{
            self.showToast(message: "enter_your_personal_number".easyLocalized())
            return
        }
        if senderMobile.isPhoneNumber == true  {
            if senderMobile.isElevenDigit == false{
                self.showToastError(message: "invalid_phone_no".easyLocalized())
                return
            }
            getUserStatus(email: "", phone: senderMobile)
        }else if senderMobile.isEmail == false{
            if senderMobile.isPhoneNumber == false{
                self.showToastError(message: "invalid_email_or_phone".easyLocalized())
            }else{
                self.showToastError(message: "invalid_email_address".easyLocalized())
            }
           
            return
        }
        else if senderMobile.isEmail == true {
            getUserStatus(email: senderMobile, phone: "")
        }
        
    }
    func received(userStatus: GetUserStatusResponseModel) {
        if userStatus.code == 200 {
            if let ui = userStatus.ui{
                if ui == InitialUIBasedOnStatus.registration.rawValue{
                    let vc = RegistrationViewController()
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if ui == InitialUIBasedOnStatus.otp.rawValue {
                    if userStatus.data != nil{
                        let vc = OTPEntryViewController()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else if ui == InitialUIBasedOnStatus.password.rawValue{
                    let vc = PasswordEntryViewController()
                    guard let identity =  mView.numberView.text else{
                        return
                    }
                    vc.identity = identity
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            showToastError(message: userStatus.message ?? "Error Occured")
        }
    }
    
    func getUserStatus(email:String,phone:String) {
        var model:LoginRequestModel?
        if email.isEmpty == true {
            model = LoginRequestModel(mobile: phone)
        }
        else {
            model = LoginRequestModel(email: email)
        }
        guard let model = model else {
            return
        }
        WebServiceHandler.shared.getUserStatus(for: model , completion: { (result) in
            switch result{
            case .success(let userStatus):
                self.received(userStatus: userStatus)
                break
                
            case .failure(let error) :
                self.showToast(message: error.localizedDescription)
                break
            }
        }, shouldShowLoader: true)
    }
}
