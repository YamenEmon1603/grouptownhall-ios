//
//  ForgotPasswordAlertViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/7/21.
//

import UIKit
protocol ForgotPassDelegate {
    func dismissPassAlert()
}
class ForgotPasswordAlertViewController: EasyBaseViewController {
    var delegate : ForgotPassDelegate?
    lazy var mView: ForgotPasswordAlertView = {
        let view = ForgotPasswordAlertView()
        view.btnProceed.addTarget(self, action: #selector(proceedToChangePass(_:)), for: .touchUpInside)
        view.btnCancel.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
        return view
    }()
    override func viewDidLoad() {
        self.view =  mView

        super.viewDidLoad()
        
    }
    @objc func proceedToChangePass(_ sender : UIButton){

        self.dismiss(animated: true) {
            self.delegate?.dismissPassAlert()
        }
       
    }
    @objc func cancelTapped(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}

