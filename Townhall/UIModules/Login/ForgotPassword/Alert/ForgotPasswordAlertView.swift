//
//  ForgotPasswordAlertView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/7/21.
//

import Foundation
import UIKit

class ForgotPasswordAlertView : EasyBaseView{
    
    
    init(){
        super.init(frame: .zero)
       addSubview(viewContainer)
        viewContainer.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0,heightMultiplier: 0.5)
       
        self.backgroundColor = .black.withAlphaComponent(0.3)
        
    }
  
    lazy var lblInstruction: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .EasyFont(ofSize: 14.0, style: .regular)
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = "no_one_from_easy_will".easyLocalized()
        return label
    }()
    lazy var lblConfirmation : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .EasyFont(ofSize: 14.0, style: .bold)
        label.textColor = .black
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "if_you_still_wish_to_reset".easyLocalized()
        return label
    }()
  
     var btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.translatesAutoresizingMaskIntoConstraints = false
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.layer.cornerRadius = 10
        //btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    lazy var btnCancel:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "crossgrayround"), for: .normal)
        //btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return button
    }()
    lazy var viewBlurContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white.withAlphaComponent(0.2)
        view.addSubview(viewContainer)
        viewContainer.addAnchorToSuperview(leading: 0, trailing: 0,heightMultiplier: 0.5)
        //viewContainer.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 1).isActive = true
        viewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
        
        return view
    }()
    lazy var viewContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.addSubview(lblInstruction)
        view.addSubview(lblConfirmation)
        view.addSubview(btnProceed)
        view.addSubview(btnCancel)
        btnCancel.addAnchorToSuperview(trailing: -10,top: 20)
        
        btnCancel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        btnCancel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        lblInstruction.addAnchorToSuperview(leading: 10, trailing: -10,top: 40,heightMultiplier: 0.4)
        
        
        lblConfirmation.addAnchorToSuperview(leading: 10, trailing: -10,heightMultiplier: 0.2)
        lblConfirmation.topAnchor.constraint(equalTo: lblInstruction.bottomAnchor, constant: 8).isActive = true
        
        btnProceed.addAnchorToSuperview(leading: 10, trailing: -10,bottom: -30)
        btnProceed.heightAnchor.constraint(equalToConstant: 40).isActive = true
        btnProceed.topAnchor.constraint(greaterThanOrEqualTo: lblConfirmation.bottomAnchor,constant: 20).isActive = true
        
        
        return view
    }()
    
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}

