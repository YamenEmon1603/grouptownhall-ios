//
//  ForgotPasswordView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/7/21.
//

import Foundation
import UIKit


class ForgotPasswordView : EasyBaseView {
 
    var proceedCallBack : (()->Void)?
    var eye1CallBack : (()->Void)?
    var eye2CallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .iceBlue
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.07)
        

        
        containerView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 8).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
   
    //MARK: COMPONENTS
   
    
 
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        
       
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
       
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
       
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 15
        holderView.addSubview(lblForgotPasswordHeader)
        holderView.addSubview(lblForgotPasswordSubtitle)
        holderView.addSubview(stackView)
        
        lblForgotPasswordHeader.addAnchorToSuperview(leading: 20, trailing: -20, top: 20)
        lblForgotPasswordSubtitle.addAnchorToSuperview(leading: 20, trailing: -20)
        
        lblForgotPasswordSubtitle.topAnchor.constraint(equalTo: lblForgotPasswordHeader.bottomAnchor, constant: 8).isActive = true
        lblForgotPasswordSubtitle.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -20).isActive = true
        
        
        stackView.addAnchorToSuperview(leading: 20, trailing: -20, bottom: 0)
        
      
        
        stackView.addArrangedSubview(viewContainerEnterPass)
        viewContainerEnterPass.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        stackView.addArrangedSubview(viewContainerConfirmPass)
        viewContainerConfirmPass.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        return formView
    }()
    
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        btnProceed.titleEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    //MARK: InputViews
    
    lazy var viewContainerEnterPass: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.addSubview(viewEnterPassword)
        view.addSubview(btn1Eye)
        viewEnterPassword.addAnchorToSuperview(leading: 0, top: 0, bottom: 0,  widthMutiplier: 0.8)
        btn1Eye.addAnchorToSuperview(trailing: -8 , heightMultiplier: 0.9, centeredVertically:0,heightWidthRatio: 1)
        
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.addShadow(location: .bottom, color: UIColor.black, opacity: 0.2, radius: 5)
        return view
    }()
    lazy var viewContainerConfirmPass: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.addSubview(viewConfirmPassword)
        view.addSubview(btn2Eye)
        viewConfirmPassword.addAnchorToSuperview(leading: 0, top: 0, bottom: 0,  widthMutiplier: 0.8)
        btn2Eye.addAnchorToSuperview(trailing: -8 , heightMultiplier: 0.9, centeredVertically:0,heightWidthRatio: 1)
        
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.addShadow(location: .bottom, color: UIColor.black, opacity: 0.2, radius: 5)
        return view
    }()
    
    lazy var viewEnterPassword : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_password".easyLocalized())
        textView.backgroundColor = .clear
        textView.isSecureText = true
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        return textView
    }()

    lazy var viewConfirmPassword : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "confirm_password".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        textView.backgroundColor = .clear
        textView.isSecureText = true
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
       
        return textView
    }()
    

    lazy var btn1Eye :  UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "eyeclose")
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(btnEye1Action(_:)), for: .touchUpInside)
        return button
    }()
    @objc func btnEye1Action(_ sender:UIButton){
      eye1CallBack?()
    }
    
    lazy var btn2Eye :  UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "eyeclose")
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(btnEye2Action(_:)), for: .touchUpInside)
        return button
    }()
    @objc func btnEye2Action(_ sender:UIButton){
        eye2CallBack?()
    }
    
    
    lazy var lblForgotPasswordHeader: UILabel = {
        let lblTitle = UILabel()
        lblTitle.font = .EasyFont(ofSize: 18, style: .bold)
        lblTitle.textColor = .black
        lblTitle.text = "change_password".easyLocalized()
        lblTitle.textAlignment = .left
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        return lblTitle
    }()
    lazy var lblForgotPasswordSubtitle : UILabel = {
        let lblTitle = UILabel()
        lblTitle.font = .systemFont(ofSize: 16, weight: .regular)
        lblTitle.textColor = .gray
        lblTitle.text = "please_enter_a_new_password".easyLocalized()
        lblTitle.textAlignment = .left
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        return lblTitle
    }()
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "", leadingBtn: 10)
        return navView
    }()
    

}




extension ForgotPasswordView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}

