//
//  ForgotPasswordViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/7/21.
//

import UIKit


class ForgotPasswordViewController: EasyBaseViewController {
    var identity : String?
    var fpKey : String?
    lazy var mView: ForgotPasswordView = {
        let view = ForgotPasswordView()
        view.proceedCallBack = proceed
        view.navView.backButtonCallBack = backAction
        view.eye1CallBack = passWord1Hide
        view.eye2CallBack = passWord2Hide
        return view
    }()
    override func viewDidLoad() {
        self.view = mView
        
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    func passWord1Hide() {
        if mView.btn1Eye.isSelected == true{
            mView.btn1Eye.isSelected.toggle()
            mView.btn1Eye.setImage(UIImage(named: "eyeclose"), for: .normal)
            mView.viewEnterPassword.isSecureText = true
        }else{
            mView.btn1Eye.isSelected.toggle()
            mView.btn1Eye.setImage(UIImage(named: "eyeopen"), for: .normal)
            mView.viewEnterPassword.isSecureText = false
        }
    }
    func passWord2Hide() {
        if mView.btn2Eye.isSelected == true{
            mView.btn2Eye.isSelected.toggle()
            mView.btn2Eye.setImage(UIImage(named: "eyeclose"), for: .normal)
            mView.viewConfirmPassword.isSecureText = true
        }else{
            mView.btn2Eye.isSelected.toggle()
            mView.btn2Eye.setImage(UIImage(named: "eyeopen"), for: .normal)
            mView.viewConfirmPassword.isSecureText = false
        }
    }
    
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func proceed()  {
        guard let pass =  mView.viewEnterPassword.text ,pass.isEmpty == false else{
            showToast(message: "enter_password".easyLocalized())
            return
        }
        guard let rePass = mView.viewConfirmPassword.text ,rePass.isEmpty == false else{
            showToast(message: "retype_password".easyLocalized())
            return
        }
        
        if mView.viewEnterPassword.text != mView.viewConfirmPassword.text{
            showToast(message: "password_dont_match".easyLocalized())
           
        }else{
            resetPassword(password: pass, retypePassword: rePass)
        }
    }
    
}
extension ForgotPasswordViewController{
    func resetPassword(password: String, retypePassword: String) {
        self.dismissKeyboard()
        if identity?.isPhoneNumber == true{
            if let mobile = identity ,let key = fpKey{
                let reqModel = ForgetPasswordRestore(mobile: mobile, fpKey: key, password: password, retype: retypePassword)
                WebServiceHandler.shared.forgotPasswordRestore(reqModel: reqModel, completion: { (result) in
                    switch result{
                    case .success(let response):
                        if response.code == 200 {
                            //EasyDataStore.sharedInstance.apiToken = response.token
                            //EasyDataStore.sharedInstance.user = response.data
                            // TABBBBBBB
                            //let vc = EasyTabBarViewController()
                            if let targetVc = self.navigationController?.viewControllers[1]{
                                self.navigationController?.showToast(message: "Your password has been reset successfully")
                                self.navigationController?.popToViewController(targetVc , animated: true)
                            }
                            
                        }else{
                            
                            self.showToast(message:  response.message ?? "")
                        }
                        
                        break
                    case .failure( let error):
                        self.showToast(message: error.localizedDescription)
                        break
                    }
                }, shouldShowLoader: true)
            }
        }else if identity?.isEmail == true{
            if let email = identity ,let key = fpKey{
                let reqModel = ForgotPasswordEmailRestore(email: email, fpKey: key, password: password, retype: retypePassword)
                WebServiceHandler.shared.forgotPasswordEmailRestore(reqModel: reqModel, completion: { (result) in
                    switch result{
                    case .success(let response):
                        if response.code == 200 {
                            EasyDataStore.sharedInstance.apiToken = response.token
                            EasyDataStore.sharedInstance.user = response.data
                            let vc = EasyTabBarViewController()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            
                            self.showToast(message:  response.message ?? "")
                        }
                        debugPrint(response)
                        break
                    case .failure( _):
                        break
                    }
                }, shouldShowLoader: true)
            }
        }
    }
}
