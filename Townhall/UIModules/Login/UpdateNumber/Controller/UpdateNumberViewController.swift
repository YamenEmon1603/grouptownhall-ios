//
//  UpdateNumberViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/8/21.
//

import UIKit

class UpdateNumberViewController: EasyBaseViewController {
    
    var identity : String?
    var mkey:String?
    lazy var mView: UpdateNumberView = {
        let mView = UpdateNumberView()
        mView.proceedCallBack = proceed
        mView.navView.backButtonCallBack = backAction
        return mView
    }()
    var keyboardObserver : KeyboardObserver!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //keyboardObserver.add()
    }
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func proceed()  {
        dismissKeyboard()
        guard let senderMobile =  mView.numberView.text,senderMobile.isEmpty == false else{
            self.showToast(message: "enter_mobile_number_mail".easyLocalized())
            return
        }
        if senderMobile.isPhoneNumber == true  {
            if senderMobile.isElevenDigit == false{
                self.showToastError(message: "invalid_phone_no".easyLocalized())
                return
            }
            guard let identity = identity else {
                return
            }
            guard let mkey = mkey else {
                return
            }
            getUserStatus(email: identity, phone: senderMobile,mkey: mkey)
        }else {
            self.showToastError(message: "invalid_phone_no".easyLocalized())
        }
        
    }
    func received(userStatus: GetUserStatusResponseModel) {
        if userStatus.code == 200 {
            if let ui = userStatus.ui{
               if ui == InitialUIBasedOnStatus.otp.rawValue {
                    if userStatus.data != nil{
                        let vc = OTPEntryViewController()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }else{
            showToastError(message: userStatus.message ?? "Error Occured")
        }
    }
    
    func getUserStatus(email:String,phone:String,mkey:String) {
        var model:LoginRequestModel?
       
            model = LoginRequestModel(email: email, mobile: phone, mkey: mkey)
            
            guard let model = model else {
                return
            }
            WebServiceHandler.shared.oldUserOtpForMobileRestore(for: model , completion: { (result) in
                switch result{
                case .success(let userStatus):
                    self.received(userStatus: userStatus)
                    break
                    
                case .failure(let error) :
                    self.showToast(message: error.localizedDescription)
                    break
                }
            }, shouldShowLoader: true)
        
    }
}


