//
//  UpdateNumberView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/8/21.
//

import Foundation
import UIKit

class UpdateNumberView : EasyBaseView {
    var proceedCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.05)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
        addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.07)
        btnProceed.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var contentView: UIView = {
        let contentView = UIView()
        let scrollConatiner = UIView()
     
        contentView.addSubview(scrollConatiner)
        scrollConatiner.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        scrollConatiner.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor).isActive = true
        
        let scrollView = UIScrollView()
        scrollConatiner.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        let stackConatiner = UIView()
       
        scrollView.addSubview(stackConatiner)
        stackConatiner.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackConatiner.widthAnchor.constraint(equalTo: scrollConatiner.widthAnchor).isActive = true
        stackConatiner.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 1).isActive = true
        
        stackConatiner.addSubview(updateNumberLabel)
        updateNumberLabel.addAnchorToSuperview(leading: 20, trailing: -20,top: 5)
        updateNumberLabel.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.04).isActive = true
        
        stackConatiner.addSubview(addNumberToAccLabel)
        addNumberToAccLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        addNumberToAccLabel.topAnchor.constraint(equalTo: updateNumberLabel.bottomAnchor,constant: 8).isActive = true
        addNumberToAccLabel.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.04).isActive = true
        
        stackConatiner.addSubview(numberView)
        numberView.addAnchorToSuperview(leading: 20, trailing: -20)
        numberView.topAnchor.constraint(equalTo: addNumberToAccLabel.bottomAnchor,constant: 30).isActive = true
        numberView.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.08).isActive = true
        
        return contentView
    }()
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "", leadingBtn: 10)
        return navView
    }()

    let updateNumberLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 18.0, style: .bold)
        label.textColor = .black
        label.text = "update_your_number".easyLocalized()
        return label
    }()
    let addNumberToAccLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .regular)
        label.textColor = .black
        label.text = "add_number_to_your_account".easyLocalized()
        return label
    }()
    lazy var   numberView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_number".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        textView.limit = 11
        textView.keyBoardType = .phonePad
        return textView
    }()
    
   
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
}

extension UpdateNumberView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}
