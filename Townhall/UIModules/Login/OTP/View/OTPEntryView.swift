//
//  OTPEntryView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/8/21.
//

import UIKit
class OTPEntryView: EasyBaseView {
    var otpCallBack:((_ id: String) -> Void)?
    var proceedCallBack : (()->Void)?
    var changeNumberTapped : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.05)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
        addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.07)
        btnProceed.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var contentView: UIView = {
        let contentView = UIView()
        let scrollConatiner = UIView()
     
        contentView.addSubview(scrollConatiner)
        scrollConatiner.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        scrollConatiner.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor).isActive = true
        
        let scrollView = UIScrollView()
        scrollConatiner.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        let stackConatiner = UIView()
       
        scrollView.addSubview(stackConatiner)
        stackConatiner.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackConatiner.widthAnchor.constraint(equalTo: scrollConatiner.widthAnchor).isActive = true
        stackConatiner.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 1).isActive = true
        
        stackConatiner.addSubview(enterOTPLabel)
        enterOTPLabel.addAnchorToSuperview(leading: 20, trailing: -20,top: 5)
        enterOTPLabel.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.04).isActive = true
        
        stackConatiner.addSubview(otpSentToLabel)
        otpSentToLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        otpSentToLabel.topAnchor.constraint(equalTo: enterOTPLabel.bottomAnchor,constant: 8).isActive = true
        otpSentToLabel.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.04).isActive = true
        
        stackConatiner.addSubview(otpView)
        otpView.addAnchorToSuperview(leading: 20, trailing: -20)
        otpView.topAnchor.constraint(equalTo: otpSentToLabel.bottomAnchor,constant: 30).isActive = true
        otpView.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.12).isActive = true
       
        stackConatiner.addSubview(btnChangeNumber)
        btnChangeNumber.addAnchorToSuperview(leading: 20)
        btnChangeNumber.topAnchor.constraint(equalTo: otpView.bottomAnchor,constant: 5).isActive = true
        
        
        
        let stack = UIStackView()
        stack.axis = .horizontal
        
        stack.distribution = .fillEqually
        stackConatiner.addSubview(stack)

        stack.addAnchorToSuperview(trailing: -20)
        stack.topAnchor.constraint(equalTo: btnChangeNumber.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: btnChangeNumber.bottomAnchor).isActive = true
        stack.addArrangedSubview(btnResendOTP)
        btnResendOTP.isHidden = true
        stack.addArrangedSubview(timerView)
        
      
        return contentView
    }()
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "", leadingBtn: 10)
        return navView
    }()
  
    let enterOTPLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 18.0, style: .bold)
        label.textColor = .black
        label.text = "enter_otp".easyLocalized()
        return label
    }()
    let otpSentToLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .regular)
        label.textColor = .slateGrey
        label.text = "please_enter_sent_to".easyLocalized()
        return label
    }()
    lazy var   otpView: CustomOtpView = {
        let textView = CustomOtpView(numberOfField: 6) { str in
            self.otpCallBack?(str)
        }
        return textView
    }()
    let btnChangeNumber:UnderlineButton = {
        let btnForgotPassword = UnderlineButton(title: "change_number".easyLocalized())
        btnForgotPassword.button.addTarget(self, action: #selector(btnChangeNumberAction(_:)), for: .touchUpInside)
        return btnForgotPassword
    }()
    
    let btnResendOTP:UnderlineButton = {
        let btnForgotPassword = UnderlineButton(title: "resend_otp".easyLocalized())
        return btnForgotPassword
    }()
    lazy var timerView:UIView = {
        let timerView = UIView()
        timerView.backgroundColor = .blue
        
        timerView.addSubview(timmerLabel)
        timmerLabel.addAnchorToSuperview(trailing: -5, centeredVertically: 0)
       
        let timerImage = UIImageView()
        timerImage.image = UIImage(named: "timer")
        timerView.addSubview(timerImage)
        timerImage.addAnchorToSuperview(centeredVertically: 0)
        timerImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        timerImage.heightAnchor.constraint(equalTo: timerImage.widthAnchor, multiplier: 0.75).isActive = true
        timerImage.trailingAnchor.constraint(equalTo: timmerLabel.leadingAnchor, constant: -5).isActive = true
        
       
        timerView.addSubview(progressView)
        progressView.leadingAnchor.constraint(equalTo: timerImage.leadingAnchor, constant: -2).isActive = true
        progressView.trailingAnchor.constraint(equalTo: timmerLabel.trailingAnchor, constant: 2).isActive = true
        progressView.topAnchor.constraint(equalTo: timerImage.bottomAnchor, constant: 8).isActive = true
        progressView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        return timerView
    }()
    let timmerLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        
        label.textColor = .battleshipGrey
        label.text = "00:00"
        return label
    }()
    let progressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.progressTintColor = .blueViolet
        progressView.tintColor = .battleshipGrey
       
        progressView.translatesAutoresizingMaskIntoConstraints = false
        return progressView
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        btnProceed.titleEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
}

extension OTPEntryView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    @objc func btnChangeNumberAction(_ sender: ButtonAccessoryView) {
        changeNumberTapped?()
    }
    
}
