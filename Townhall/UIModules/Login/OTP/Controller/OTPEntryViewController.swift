//
//  OTPEntryViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/8/21.
//

import UIKit

class OTPEntryViewController: EasyBaseViewController {
    var timer:Timer?
    var counter = 120
    var identity : String?
    var forModule: OTPSource?
    var otpStr:String?
    lazy var mView: OTPEntryView = {
        let mView = OTPEntryView()
        mView.proceedCallBack = proceed
        mView.changeNumberTapped = btnChangeNumber
        mView.otpCallBack = otpFilledAction(_:)
        mView.otpSentToLabel.text = "\("please_enter_sent_to".easyLocalized()) \(identity ?? "")"
        mView.navView.backButtonCallBack = backAction
        mView.btnResendOTP.button.addTarget(self, action: #selector(resendOTPAction(_:)), for: .touchUpInside)
        return mView
    }()
    var keyboardObserver : KeyboardObserver!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        keyboardObserver = KeyboardObserver(for: self)
        if forModule == .setpin{
            mView.btnChangeNumber.isHidden = true
        }else{
            mView.btnChangeNumber.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    }
    func backAction(){
        if forModule == .setpin{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    func proceed()  {
        self.view.endEditing(true)
        if let otp = otpStr{
            verifyOtpFromServer(otp: otp)
        }else{
            showToast(message:"incorrect_otp".easyLocalized())
        }
    }
    
    func btnChangeNumber(){
        if let vc =  self.navigationController?.viewControllers.first(where: {$0 is PhoneEntryViewController}){
            self.navigationController?.popToViewController(vc, animated: true)
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
      
    }
    func otpFilledAction(_ id: String){
        otpStr = id
        proceed()
    }
    @objc func onTimerFires()
    {
        if counter != -1
        {
            mView.progressView.progress += 1/120
            mView.timmerLabel.text = SSLComLanguageHandler.sharedInstance.getLocalizedDigits(String(format: "%02d:%02d", counter/60, counter%60)) 
            counter -= 1
        }
        else
        {
            timer?.invalidate()
            mView.timerView.isHidden = true
            mView.btnResendOTP.isHidden =  false
        }
   }
    @objc func resendOTPAction(_ sender: UIButton){
        sendPasswordResetOtp()
    }
    func goToResetPassVc(fpKey : String,identity:String){
        let vc = ForgotPasswordViewController()
        vc.fpKey = fpKey
        vc.identity = identity
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension OTPEntryViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
       
    }
    
    func keybaordWillHide() {
        
    }
    
}

//MARK: API

extension OTPEntryViewController{
//
//    func verfyCompleted(with data: LoginResponseModel) {
//        if data.code == 200 {
//            EasyDataStore.sharedInstance.apiToken = data.token
//            EasyDataStore.sharedInstance.user = data.data
//            //view?.otpMatched()
//        }else{
//
//            showToast(message: data.message ?? "Error Occured")
//        }
//    }
    func verifyCompleted(with data: ForgotPasswordOtpVerification) {
        debugPrint(data.data?.fpKey ?? "")
        if data.code == 200 {
            if let ui = data.ui , let fpKey = data.data?.fpKey , ui == "reset-password"{
               
                goToResetPassVc(fpKey: fpKey, identity: self.identity ?? "")
            }
        }else{
            showToast(message: data.message ?? "Error Occured")
        }
    }
    func gotoSetPin(otp: String,bQrPin: String) {
        let pinVC = UIStoryboard(name: "PinStory", bundle: nil).instantiateViewController(withIdentifier: "pinVC") as! PinViewController
        pinVC.otp = otp
        pinVC.bQrPin = bQrPin
        self.navigationController?.pushViewController(pinVC,animated: true)
    }
    
    func verifyCompletedQrOtp(with data : QrOtpverifyReponse,otp:String,bQrPin: String){
        if data.code == 200{
            if let pinKey = data.data?.banglaQrPinKey{
                EasyDataStore.sharedInstance.BQrPinKey = pinKey
                gotoSetPin(otp:otp,bQrPin:bQrPin)
            }

        }else{
            self.showToast(message:  data.message ?? "Error Occured")
        }
    }
    
    
    func verifyOtpFromServer(otp: String) {
        let phone = (self.identity == nil || self.identity == "") ? EasyDataStore.sharedInstance.user?.mobile : self.identity
        if(forModule == .forgotPassword)
        {
            if phone?.isPhoneNumber == true{
                WebServiceHandler.shared.verifyFPOtp(mobile: phone ?? "", otp: otp, completion: { (result) in
                    switch result{
                    case .success(let response ):
                        self.verifyCompleted(with: response)
                        break
                    case .failure(let error):
                        debugPrint(error)
                        break
                    }
                    
                }, shouldShowLoader: true)
                
            }else if phone?.isEmail == true{
                WebServiceHandler.shared.verifyFEmailFPOtp(email: phone ?? "" , otp: otp , completion: { (result) in
                    switch result{
                    case .success(let response ):
                        self.verifyCompleted(with: response)
                        break
                    case .failure(let error):
                        debugPrint(error)
                        break
                    }
                    
                }, shouldShowLoader: true)
            }
            
       }
//        else if (forModule == .updateNumber) {
//            WebServiceHandler.shared.oldUserNumberSetOtpVeification(mobile: inputData?.mobile ?? "", email: inputData?.email ?? "", mKey: inputData?.mKey ?? "", otp:otp, completion: { (result) in
//                switch result{
//                case .success(let response ):
//                    self.presenter?.verfyCompleted(with: response)
//                    break
//                case .failure(let error):
//                    debugPrint(error)
//                    break
//                }
//            }, shouldShowLoader: true)
//        }
        else if (forModule == .setpin) {
            
            WebServiceHandler.shared.qrOtpVerify(otp: otp, completion: { (result) in
                switch result{
                case .success(let response ):
                    if response.code == 200{
                        if let bQrPin = response.data?.banglaQrPinKey{
                            self.verifyCompletedQrOtp(with : response, otp:otp,bQrPin: bQrPin)
                        }else{
                            self.showToast(message: "Something went wrong!")
                        }
                    }else{
                        self.showToast(message: response.message ?? "Error")
                    }
                   

                    break
                case .failure(let error):
                    self.showToast(message: error.localizedDescription )
                    break
                }

            }, shouldShowLoader: true)
        }
//        else{
//            if let userInfo = self.userInfo{
//                WebServiceHandler.shared.verifyRegistrationOTP(userInfo: userInfo, otp: otp, completion: { (result) in
//                    switch result{
//                    case .success(let response ):
//                        self.presenter?.verfyCompleted(with: response)
//                        break
//                    case .failure(let error):
//                        debugPrint(error)
//                        break
//                    }
//
//                }, shouldShowLoader: true)
//            }
//        }
      
    }
    
    func sendPasswordResetOtp() {
        if let  isNumb = identity?.isPhoneNumber , isNumb{
            WebServiceHandler.shared.sendForgotPasswordOtp(mobile: identity ?? "", completion: { (result) in
                switch result{
                case .success( _):
                    self.counter = 120
                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.onTimerFires), userInfo: nil, repeats: true)
                    self.mView.timerView.isHidden = false
                    self.mView.btnResendOTP.isHidden =  true
                    self.mView.progressView.progress = 0
                    break
                case .failure( _):
                    break
                }
            }, shouldShowLoader: true)
        }else  if let  isNumb = identity?.isEmail , isNumb{
            WebServiceHandler.shared.sendEmailForgotPasswordOtp(email: identity ?? "", completion: { (result) in
                switch result{
                case .success( _):
                    self.counter = 120
                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.onTimerFires), userInfo: nil, repeats: true)
                    self.mView.timerView.isHidden = false
                    self.mView.btnResendOTP.isHidden =  true
                    self.mView.progressView.progress = 0
                    break
                case .failure( _):
                    break
                }
            }, shouldShowLoader: true)
        }else{
            debugPrint("Not available")
        }
        
    }
    
}



