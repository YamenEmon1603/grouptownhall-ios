//
//  PasswordEntryView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/8/21.
//

import UIKit
class PasswordEntryView: EasyBaseView {
    var bottomContraint : NSLayoutConstraint?
    var proceedCallBack : (()->Void)?
    var forgotPassCallBack : (()->Void)?
    var eyeCallBack : (()->Void)?
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.05)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
        addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.07)
        btnProceed.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var btnEye :  UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "eyeclose")
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(btnEyeAction(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var contentView: UIView = {
        let contentView = UIView()
        let scrollConatiner = UIView()
     
        contentView.addSubview(scrollConatiner)
        scrollConatiner.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        scrollConatiner.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor).isActive = true
        
        let scrollView = UIScrollView()
        scrollConatiner.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        let stackConatiner = UIView()
       
        scrollView.addSubview(stackConatiner)
        stackConatiner.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackConatiner.widthAnchor.constraint(equalTo: scrollConatiner.widthAnchor).isActive = true
        stackConatiner.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 1).isActive = true
        
       
        
        stackConatiner.addSubview(imageView)
        imageView.addAnchorToSuperview(leading: 20, top: 5)
        imageView.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.07).isActive = true
        
        stackConatiner.addSubview(getStartedLabel)
        getStartedLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        getStartedLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor,constant: 8).isActive = true
        getStartedLabel.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.04).isActive = true
        
        stackConatiner.addSubview(enterInfoLabel)
        enterInfoLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        enterInfoLabel.topAnchor.constraint(equalTo: getStartedLabel.bottomAnchor,constant: 8).isActive = true
        enterInfoLabel.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.04).isActive = true
        
        stackConatiner.addSubview(viewContainerEnterPass)
        viewContainerEnterPass.addAnchorToSuperview(leading: 20, trailing: -20)
        viewContainerEnterPass.topAnchor.constraint(equalTo: enterInfoLabel.bottomAnchor,constant: 30).isActive = true
        viewContainerEnterPass.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.09).isActive = true
       
        stackConatiner.addSubview(btnForgotPassword)
        btnForgotPassword.addAnchorToSuperview(trailing: -20)
        btnForgotPassword.topAnchor.constraint(equalTo: viewContainerEnterPass.bottomAnchor,constant: 5).isActive = true
        
        
        
        stackConatiner.addSubview(termsAndConditionLabel)
        termsAndConditionLabel.addAnchorToSuperview(leading: 30, trailing: -30)
        bottomContraint = termsAndConditionLabel.bottomAnchor.constraint(equalTo: stackConatiner.bottomAnchor,constant: -20)
        bottomContraint?.isActive = true
        return contentView
    }()
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "", leadingBtn: 10)
        return navView
    }()
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "homeHeader")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    let getStartedLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 18.0, style: .bold)
        label.textColor = .black
        //label.text = "get_started_with_easy".easyLocalized()
        return label
    }()
    let enterInfoLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .regular)
        label.textColor = .slateGrey
        label.text = "Please enter your password"
        return label
    }()
    lazy var passTextView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "password".easyLocalized())
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        textView.isSecureText = true
        textView.backgroundColor = .clear
        return textView
    }()
    let btnForgotPassword:UnderlineButton = {
        let btnForgotPassword = UnderlineButton(title: "forgot_password".easyLocalized())
        btnForgotPassword.button.addTarget(self, action: #selector(btnForgotPassAction(_:)), for: .touchUpInside)
        return btnForgotPassword
    }()
    @objc func btnForgotPassAction(_ sender:UIButton){
        forgotPassCallBack?()
    }
    
    
    let termsAndConditionLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .regular)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.attributedText = SSLComLanguageHandler.sharedInstance.termsAndConditionsText()
      
        
        return label
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.titleEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    @objc func btnEyeAction(_ sender:UIButton){
      eyeCallBack?()
    }
    lazy var viewContainerEnterPass: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.addSubview(passTextView)
        view.addSubview(btnEye)
        passTextView.addAnchorToSuperview(leading: 0, top: 0, bottom: 0,  widthMutiplier: 0.8)
        btnEye.addAnchorToSuperview(trailing: -8 , heightMultiplier: 0.9, centeredVertically:0,heightWidthRatio: 1)
        
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.addShadow(location: .bottom, color: UIColor.black, opacity: 0.05, radius: 5)
        return view
    }()
}

extension PasswordEntryView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}

class UnderlineButton:UIView{
    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(title:String) {
        self.init(frame:.zero)
        addSubview(button)
        button.addAnchorToSuperview(leading: 8, trailing: -8, top: 2)
        button.setTitle(title, for: .normal)
        
        let underLineView = UIView()
        underLineView.backgroundColor = .blueViolet
        underLineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(underLineView)
        underLineView.addAnchorToSuperview(bottom: -2)
        underLineView.leadingAnchor.constraint(equalTo: button.leadingAnchor).isActive = true
        underLineView.trailingAnchor.constraint(equalTo: button.trailingAnchor).isActive = true
        underLineView.topAnchor.constraint(equalTo: button.bottomAnchor,constant: 2).isActive = true
        underLineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
    }
    let button:UIButton = {
        let button = UIButton()
        button.titleLabel?.font = .EasyFont(ofSize: 12, style: .medium)
        button.setTitleColor(.blueViolet, for: .normal)
        return button
    }()
}
