//
//  PasswordEntryViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/8/21.
//

import UIKit

class PasswordEntryViewController: EasyBaseViewController {
    var identity : String?
    lazy var mView: PasswordEntryView = {
        let mView = PasswordEntryView()
        mView.proceedCallBack = proceed
        mView.navView.backButtonCallBack = backAction
        mView.getStartedLabel.text = String(format:"hi_txt".easyLocalized() , identity ?? "")
        mView.forgotPassCallBack = forgotPasswordAlert
        mView.eyeCallBack = passWordHide
        return mView
    }()
    var keyboardObserver : KeyboardObserver!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        keyboardObserver = KeyboardObserver(for: self)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.termsTapped))
        mView.termsAndConditionLabel.isUserInteractionEnabled = true
        mView.termsAndConditionLabel.addGestureRecognizer(tap)
    }
    @objc func termsTapped(sender:UITapGestureRecognizer) {
        let vc = WebPageVC()
        vc.type = .terms
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
    }
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func proceed()  {
        //
        self.mView.passTextView.endEditing(true)
        if let pass = mView.passTextView.text, pass.isEmpty == false  , let id = identity {
            passwordApi(password: pass, identity: id)
        }else{
            showToast(message: "please_fill_the_password_field".easyLocalized())
        }
    }
    
    func passWordHide() {
        if mView.btnEye.isSelected == true{
            mView.btnEye.isSelected.toggle()
            mView.btnEye.setImage(UIImage(named: "eyeclose"), for: .normal)
            mView.passTextView.isSecureText = true
        }else{
            mView.btnEye.isSelected.toggle()
            mView.btnEye.setImage(UIImage(named: "eyeopen"), for: .normal)
            mView.passTextView.isSecureText = false
        }
    }
    func passwordApi(password:String,identity:String?){
        if let x = identity{
            var model : LoginRequestModel?
            if x.isEmail{
             model = LoginRequestModel(email: x, password: password)
            }else if x.isPhoneNumber{
                model = LoginRequestModel(mobile: x, password: password)
            }
            guard let model = model else {
                return
            }
            WebServiceHandler.shared.login(with: model, completion: { (result) in
                switch result{
                case .success(let response):
                    self.LoginResponse(loginResponse: response)
                    break
                case .failure( let error):
                    self.showToast(message: error.localizedDescription)
                    break
                }
            }, shouldShowLoader: true)
        }
    }
    func forgotPasswordAlert()  {
       
        let vc = ForgotPasswordAlertViewController()
  
        vc.delegate = self
        self.presentAsPopUp(vc: vc)
    
    }
    func LoginResponse(loginResponse : LoginResponseModel){
        if loginResponse.code == 200 {
            if let ui = loginResponse.ui , ui != ""{
                if  ui == InitialUIBasedOnStatus.mobile.rawValue{
                    if let mKey = loginResponse.data?.mKey {
                        
                        // OLD USER ER JONNO UPDATE NUMBER ER SCREEN ASHBE
                        let vc =  UpdateNumberViewController()
                        vc.mkey = mKey
                        vc.identity = identity
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }else{

                EasyManager.dataStore.apiToken = loginResponse.token
                EasyManager.dataStore.user = loginResponse.data
                let vc = EasyTabBarViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else{
            showToast(message:  loginResponse.message ?? "Error Occured")
        }
    }
    
}


extension PasswordEntryViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        mView.bottomContraint?.constant = 20 - height
    }
    
    func keybaordWillHide() {
        mView.bottomContraint?.constant = -20
    }
    
    
}
extension PasswordEntryViewController : ForgotPassDelegate{
    func dismissPassAlert() {
        sendPasswordResetOtp()
    }
    
    
}

extension PasswordEntryViewController {
    func gotoOtpVc(){
        let vc = OTPEntryViewController()
        vc.identity = identity
        vc.forModule = .forgotPassword
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func sendPasswordResetOtp() {
        if let  isNumb = identity?.isPhoneNumber , isNumb{
            WebServiceHandler.shared.sendForgotPasswordOtp(mobile: identity ?? "", completion: { (result) in
                       switch result{
                       case .success(let response):
                        if response.code == 200 {
                            self.gotoOtpVc()
                            
                        }else{
                            self.showToast(message:  response.message ?? "Error Occured")
                        }
                       
                           break
                       case .failure( _):
                           break
                       }
                   }, shouldShowLoader: true)
        }else  if let  isEmail = identity?.isEmail , isEmail{
            WebServiceHandler.shared.sendEmailForgotPasswordOtp(email: identity ?? "", completion: { (result) in
                switch result{
                case .success(let response):
                    if response.code == 200 {
                        self.gotoOtpVc()
                    }else{
                        self.showToast(message:  response.message ?? "Error Occured")
                    }
                    break
                case .failure( _):
                    break
                }
            }, shouldShowLoader: true)
        }else{
            //UserSettings.sharedInstance.mainNav?.showToast(message: <#T##String#>)
        }
       
    }
}



