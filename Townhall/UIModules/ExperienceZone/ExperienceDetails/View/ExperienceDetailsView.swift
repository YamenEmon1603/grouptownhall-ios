//
//  ExperienceDetailsView.swift
//  Townhall
//
//  Created by Raihan on 12/9/21.
//

import Foundation
import UIKit
import WebKit

class ExperienceDetailsView : UIView {
    
 
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        
      
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.08)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
       
        addSubview(containerView)
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
        containerView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
    }
    
    
    //MARK: COMPONENTS
    lazy var containerView : WKWebView = {
        let view = WKWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
   
   
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "Experience Zone", leadingBtn: 10)
        navView.lblTitle.font = .EasyFont(ofSize: 16.0, style: .medium)
        let _ = navView.addBorder(side: .bottom, color: .lightPeriwinkle, width: 0.5)
        return navView
    }()
  
    
    //MARK: InputViews
    
    
    
    
    
}
