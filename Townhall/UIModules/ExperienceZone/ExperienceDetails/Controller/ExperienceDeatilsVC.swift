//
//  ExperienceDeatilsVC.swift
//  Townhall
//
//  Created by Raihan on 12/9/21.
//

import UIKit
import Foundation
import WebKit
class ExperienceDeatilsVC: UIViewController, WKNavigationDelegate,WKUIDelegate {
    var navTitle : String?
    var weblink : String?
    lazy var mView: ExperienceDetailsView = {
        let view = ExperienceDetailsView()
        view.navView.backButtonCallBack = backAction
       
        return view
    }()
    var webView :WKWebView!

    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        EasyLoader.sharedInstance.startAnimation()
        setupWebView()

        // Do any additional setup after loading the view.
    }
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    fileprivate func setupWebView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        DispatchQueue.main.async {
            guard let url = URL(string: self.weblink ?? "") else { return }
            self.webView.load(URLRequest(url: url))
        }
        mView.navView.lblTitle.text = navTitle
        view.addSubview(webView)
        webView.topAnchor.constraint(equalTo: mView.containerView.topAnchor, constant: 0).isActive = true
        webView.leadingAnchor.constraint(equalTo: mView.containerView.leadingAnchor, constant: 0).isActive = true
        webView.trailingAnchor.constraint(equalTo: mView.containerView.trailingAnchor, constant: -0).isActive = true
        webView.bottomAnchor.constraint(equalTo: mView.containerView.bottomAnchor, constant: 10).isActive = true
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

      // it is done loading, do something!
        EasyLoader.sharedInstance.stopAnimation()
      }

}
