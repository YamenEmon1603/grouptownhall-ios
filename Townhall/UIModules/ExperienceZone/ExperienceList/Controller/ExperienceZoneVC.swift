//
//  ExperienceZoneVC.swift
//  Townhall
//
//  Created by Raihan on 12/9/21.
//

import UIKit

class ExperienceZoneVC: EasyBaseViewController {
    var items : [ExperienceItem] = []
    
    lazy var mView: ExperienceZoneView = {
        let view = ExperienceZoneView()
        
        view.navView.backButtonCallBack = backAction
        return view
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        
        
        self.mView.experienceTableView.delegate = self
        self.mView.experienceTableView.dataSource = self
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        experienceApi()
        
    }
    
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
//MARK: -UITableViewDelegate,UITableViewDataSource
extension ExperienceZoneVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExperienceTableViewCell.identifier, for: indexPath) as! ExperienceTableViewCell
        cell.data = items[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ExperienceDeatilsVC()
        vc.navTitle = items[indexPath.row].title
        vc.weblink = items[indexPath.row].link
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
}
extension ExperienceZoneVC{
    func experienceApi(){
        WebServiceHandler.shared.getExperienceContent(completion: { result in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.items = []
                    if let item = response.data?.townhall?.items{
                        self.items = item
                        self.mView.experienceTableView.reloadData()
                        
                    }
                    
                }else{
                    self.showToast(message: response.message ?? "")
                }
                
                
            case .failure(let error) :
                self.showToast(message: error.localizedDescription)
                break
            }
        }, shouldShowLoader: true)
    }
}
