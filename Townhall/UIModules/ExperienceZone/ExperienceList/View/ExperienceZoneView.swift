//
//  ExperienceZoneView.swift
//  Townhall
//
//  Created by Raihan on 12/9/21.
//

import Foundation
import UIKit
class ExperienceZoneView : UIView{
    var proceedCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView = UIScrollView()
    let stackView = UIStackView()
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.08)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
       
        addSubview(containerView)
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
        containerView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        containerView.addSubview(formView)
        formView.addAnchorToSuperview(leading: 0, trailing: 0, top: 10, bottom: 0)
        
        
        
    }
    
    lazy var experienceTableView: UITableView = {
        let table = UITableView(frame: .zero)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        table.backgroundColor = .white
        table.register(ExperienceTableViewCell.self, forCellReuseIdentifier: ExperienceTableViewCell.identifier)
        return table
    }()
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "Experience Zone", leadingBtn: 0)
        navView.lblTitle.font = .EasyFont(ofSize: 16.0, style: .medium)
        let _ = navView.addBorder(side: .bottom, color: .lightPeriwinkle, width: 0.5)
        return navView
    }()
   
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        

        formView.addSubview(experienceTableView)
        experienceTableView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        

        
        return formView
    }()
}
