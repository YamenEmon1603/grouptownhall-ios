//
//  ExperienceTableViewCell.swift
//  Townhall
//
//  Created by Raihan on 12/9/21.
//

import Foundation
import  UIKit
import Kingfisher


class ExperienceTableViewCell: UITableViewCell {
    static let identifier = "ExperienceCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    var  data : ExperienceItem?{
        didSet{
            if let item = data{
                lblTitle.text = item.title
                lblsub.text = item.shortDescription
                //bgGroupImageView.kf.setImage(with: item.background, for: .normal)
                
                if let imgUrl  =  item.background{
                    bgGroupImageView.kf.setImage(with: URL(string:imgUrl),placeholder:UIImage(named: "americanExpressCardBg"))
                }
                if let imgUrl  =  item.icon{
                    expImageView.kf.setImage(with: URL(string:imgUrl))
                }
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(bgGroupImageView)
        self.contentView.addSubview(listContainerView)
        listContainerView.addAnchorToSuperview(leading: 15, trailing: -15, top: 5, bottom: -5)
        listContainerView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        NSLayoutConstraint.activate([
            bgGroupImageView.leadingAnchor.constraint(equalTo: listContainerView.leadingAnchor),
            bgGroupImageView.trailingAnchor.constraint(equalTo: listContainerView.trailingAnchor),
            bgGroupImageView.topAnchor.constraint(equalTo: listContainerView.topAnchor),
            bgGroupImageView.bottomAnchor.constraint(equalTo: listContainerView.bottomAnchor),
            
        ])
        bgGroupImageView.layer.cornerRadius = 10
        //listContainerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(100)).isActive = true
        backgroundColor = .white
        selectionStyle = .none
        
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var listContainerView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        var stack = UIStackView()
        stack.axis = .vertical
        stack.addArrangedSubview(lblTitle)
        stack.addArrangedSubview(lblsub)
        
        // view.addSubview(bgGroupImageView)
        view.addSubview(expImageView)
        view.addSubview(stack)
        //        view.addSubview(lblTitle)
        //        view.addSubview(lblsub)
        
        //bgGroupImageView.addAnchorToSuperview(heightMultiplier: 1, widthMutiplier: 1, centeredVertically: 0, centeredHorizontally: 0)
        expImageView.addAnchorToSuperview(leading: 15,widthMutiplier: 0.17, centeredVertically: 0 ,heightWidthRatio: 1)
        
        stack.addAnchorToSuperview(trailing: -20)
        stack.leadingAnchor.constraint(equalTo: expImageView.trailingAnchor, constant: 15).isActive = true
        stack.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        //        lblsub.addAnchorToSuperview( trailing: -15, bottom: -20)
        //        lblsub.leadingAnchor.constraint(equalTo: expImageView.trailingAnchor, constant: 15).isActive = true
        //        lblsub.topAnchor.constraint(equalTo: lblTitle.bottomAnchor,constant: 4).isActive = true
        
        
        return view
    }()
    lazy var lblTitle: UILabel = {
        let lblTitle = UILabel()
        lblTitle.font = .EasyFont(ofSize: 16.0, style: .bold)
        lblTitle.text = ""
        lblTitle.textColor = .white
        lblTitle.numberOfLines = 2
        lblTitle.textAlignment = .left
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        return lblTitle
    }()
    lazy var viewContainer: UIView = {
        let view = UIView()
        
        
        return view
    }()
    lazy var lblsub : UILabel = {
        let lblsub = UILabel()
        lblsub.font = .EasyFont(ofSize: 12.0, style: .regular)
        lblsub.text = ""
        lblsub.numberOfLines = 2
        lblsub.textColor = .white
        lblsub.textAlignment = .left
        
        lblsub.sizeToFit()
        lblsub.translatesAutoresizingMaskIntoConstraints = false
        return lblsub
    }()
    lazy var bgGroupImageView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "")
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        return imgView
    }()
    lazy var expImageView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "")
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    
    
}

class ExperienceItemView : UIView{
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(data : ExperienceItem){
        self.init(frame: .zero)
        self.addSubview(bgGroupImageView)
        
        
        
        backgroundColor = .paleGrey
        if let imgUrl  =  data.background{
            bgGroupImageView.kf.setImage(with: URL(string:imgUrl))
        }
        if let imgUrl  =  data.icon{
            expImageView.kf.setImage(with: URL(string:imgUrl))
        }
        lblTitle.text = data.title
        lblsub.text = data.shortDescription
        EasyUtils.shared.delay(1.0) {
            self.bgGroupImageView.frame = self.frame
        }
        
        bgGroupImageView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
    }
    lazy var bgGroupImageView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "")
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        return imgView
    }()
    lazy var expImageView : UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.image = UIImage(named: "")
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    lazy var listContainerView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(expImageView)
        view.addSubview(lblTitle)
        view.addSubview(lblsub)
        bgGroupImageView.addAnchorToSuperview(leading: 0 ,top: 0)
        
        expImageView.addAnchorToSuperview(leading: 15,widthMutiplier: 0.17, centeredVertically: 0 ,heightWidthRatio: 1)
        
        lblTitle.addAnchorToSuperview(trailing: -20,top: 20)
        lblTitle.leadingAnchor.constraint(equalTo: expImageView.trailingAnchor, constant: 15).isActive = true
        
        lblsub.addAnchorToSuperview( trailing: -15, bottom: -20)
        lblsub.leadingAnchor.constraint(equalTo: expImageView.trailingAnchor, constant: 15).isActive = true
        lblsub.topAnchor.constraint(equalTo: lblTitle.bottomAnchor,constant: 4).isActive = true
        
        
        return view
    }()
    lazy var lblTitle: UILabel = {
        let lblTitle = UILabel()
        lblTitle.font = .EasyFont(ofSize: 16.0, style: .bold)
        lblTitle.text = ""
        lblTitle.textColor = .blue
        lblTitle.numberOfLines = 0
        lblTitle.textAlignment = .left
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        return lblTitle
    }()
    lazy var viewContainer: UIView = {
        let view = UIView()
        
        
        return view
    }()
    lazy var lblsub : UILabel = {
        let lblsub = UILabel()
        lblsub.font = .EasyFont(ofSize: 12.0, style: .regular)
        lblsub.text = ""
        lblsub.numberOfLines = 0
        lblsub.textColor = .blue
        lblsub.textAlignment = .left
        
        lblsub.sizeToFit()
        lblsub.translatesAutoresizingMaskIntoConstraints = false
        return lblsub
    }()
}
