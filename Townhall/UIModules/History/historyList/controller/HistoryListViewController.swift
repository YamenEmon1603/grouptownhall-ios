//
//  HistoryListViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/3/21.
//

import UIKit

class HistoryListViewController: EasyBaseViewController {
    var tableData: [String:[DayWiseTransaction]]?
    var type: DateFilter?
    var currentPage: Int = 0
    var maxPage = 0
    var isLoadingList : Bool = false
    var sortedKeys:[String] = []{
        didSet{
            if sortedKeys.count == 0{
                tableView.isHidden = true
            }else{
                tableView.isHidden = false
                tableView.reloadData()
            }
            
        }
    }
    var dataMonth:MonthWise?{
        didSet{
            let f = DateFormatter()
            f.dateFormat = "yyyy-MM-dd"
            maxPage = dataMonth?.numberOfPage ?? 0
            if let transData = dataMonth?.transactionData{
                tableData = Dictionary(grouping: transData, by:  { ($0.transactionAt?.components(separatedBy: " ").first ?? "" ) })
                sortedKeys =  tableData?.keys.sorted(by: {f.date(from: $0)! > f.date(from: $1)!}) ?? []
                debugPrint(sortedKeys)
                amountLabel.amountLabel.text = EasyUtils.shared.getLocalizedDigits("\(dataMonth?.sumAmount ?? 0.0)")
                
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .iceBlue
        //        self.view.addSubview(tableView)
        self.view.addSubview(totalTrasctionView)
        totalTrasctionView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,heightMultiplier: 0.1)
        let containerView  = UIView()
        self.view.addSubview(containerView)
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        containerView.topAnchor.constraint(equalTo: totalTrasctionView.bottomAnchor, constant: 0).isActive = true
        
        containerView.addSubview(noDataView)
        noDataView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        containerView.addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        if let reportType = self.type{
            getReport(datefilter: reportType)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let reportType = self.type{
            dataMonth = nil
            getReport(datefilter: reportType)
        }
    }
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(HistoryTableViewCell.self, forCellReuseIdentifier: HistoryTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        return tableView
    }()
    lazy var totalTrasctionView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .medium)
        label.textColor = .slateGrey
        label.text = "total_transection".easyLocalized()
        
        view.addSubview(label)
        label.addAnchorToSuperview(leading: 20, centeredVertically: 0)
        
        view.addSubview(amountLabel)
        amountLabel.addAnchorToSuperview(trailing: -20, centeredVertically: 0)
        label.trailingAnchor.constraint(equalTo:amountLabel.leadingAnchor , constant: -10).isActive = true
        return view
    }()
    let amountLabel:ViewWithTakaSign = {
        let view = ViewWithTakaSign(amount: "")
        view.amountLabel.font = .EasyFont(ofSize: 20.0, style: .bold)
        view.amountLabel.textColor = .black
        view.takasignLabel.font = .EasyFont(ofSize: 11.0, style: .bold)
        view.takasignLabel.textColor = .black
        return view
    }()
    
    let  noDataView : NoDataView = {
        let noDataView = NoDataView(image: UIImage(named: "noTransection")!, text: "no_transection_yet".easyLocalized(), subtext: "")
        
        return noDataView
    }()
    
    
    
}
extension HistoryListViewController:UITableViewDelegate,UITableViewDataSource {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == tableView,
              (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height, isLoadingList == false
        
        else { return }
        debugPrint(scrollView.contentOffset.y + scrollView.frame.size.height)
        debugPrint(scrollView.contentSize.height)
        currentPage = currentPage + 1
        if currentPage < maxPage,let reportType = self.type{
            getReport(datefilter: reportType,page: currentPage)
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData?[sortedKeys[section]]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HistoryTableViewCell.identifier, for: indexPath) as!  HistoryTableViewCell
        cell.transaction =  tableData?[sortedKeys[indexPath.section]]?[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = EasyPaddingLabel()
        //label.text = "SAT, 6 Nov 2020"
        let f = DateFormatter()
        f.dateFormat = "yyyy-MM-dd"
        let date = f.date(from: sortedKeys[section])
        label.text = EasyUtils.shared.forDate(toTimestamp: f.string(from: date ?? Date()), dateFormate: "yyyy-MM-dd", expectedDateFormate: "EEE, d MMM yyyy")
        label.sizeToFit()
        return label
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        
        if  let transaction = tableData?[sortedKeys[indexPath.section]]?[indexPath.row]{
            
            
            if transaction.statusEnum == 0 || transaction.statusEnum == 1 {
                let vc = HistoryDetailsViewController()
                vc.modalPresentationStyle = .overFullScreen
                vc.transaction = transaction
                self.present(vc, animated: true, completion: nil)
                debugPrint("PRESSED")
            }
        }
    }
}

extension HistoryListViewController{
    func getReport(datefilter:DateFilter,page:Int? = 0,singleDate:Bool? = false) {
        guard let info = getDateRange(dateFilte: datefilter, page: page ?? 0) else {
            return
        }
        WebServiceHandler.shared.getTransactionHistory(info: info, completion: { (result) in
            switch result {
            case .success(let response):
                if response.code == 200{
                    self.isLoadingList = false
                    if let data = response.data{
                        self.received(monthwise: data, singleDate: singleDate ?? false)
                    }
                    
                }else{
                    self.showToast(message: response.message ?? "")
                    
                }
            case .failure(let error):
                self.isLoadingList = false
                self.showToast(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    
    func received(monthwise: MonthWise, singleDate : Bool = false) {
        
        if dataMonth?.transactionData != nil{
            if let monthData = dataMonth?.transactionData, let fetchMonth = monthwise.transactionData{
                dataMonth?.transactionData = monthData + fetchMonth
                if let data = dataMonth{
                    dataMonth = data
                }
            }
            
        }else{
            dataMonth = monthwise
        }
    }
    
    enum DateFilter{
        case SevenDays,ThirtyDays,SixMonth,AllTime
        var monthWise:Int{
            switch self {
            case .SevenDays,.ThirtyDays:
                return 0
            case .SixMonth,.AllTime:
                return 1
            }
        }
        
    }
    func  getDateRange(dateFilte:DateFilter,page:Int) -> DynamicReport? {
        let dateFormatter = DateFormatter()
        let todayTime = "23:59:59"
        let previousTime = "00:00:01"
        
        var futureDate : Date?
        switch dateFilte {
        case .SevenDays:
            futureDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        case .ThirtyDays:
            futureDate  = Calendar.current.date(byAdding: .day, value: -30, to: Date())
        case .SixMonth:
            futureDate  = Calendar.current.date(byAdding: .month, value: -6, to: Date())
        case .AllTime:
            futureDate  = Calendar.current.date(byAdding: .year, value: -2, to: Date())
        }
        guard let futureDate = futureDate else {
            return nil
        }
        dateFormatter.dateFormat = "yyyy-MM-dd \(todayTime)"
        let  todayStr = dateFormatter.string(from: Date())
        dateFormatter.dateFormat = "yyyy-MM-dd \(previousTime)"
        let finalFutureDate = dateFormatter.string(from: futureDate)
        let info =  DynamicReport(fromDate: finalFutureDate, todate: todayStr, isMonthWise: dateFilte.monthWise, page:  page )
        return info
    }
}

