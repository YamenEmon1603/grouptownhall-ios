//
//  HistoryTableViewCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/3/21.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    static let identifier = "HistoryTableViewCell"
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    var transaction : DayWiseTransaction?{
        didSet{
            nameLabel.text = "\(EasyUtils.shared.getLocalizedDigits("\(transaction?.orderData?.dada?.first?.value ?? "")") ?? "")"
            
     
            var amount : Double = 0
            switch transaction?.amount{
            case .integer(let intData):
                amount = Double(intData)
            case .string(let str):
                amount = Double(str) ?? 0.0
            case .double(let douData):
                amount = douData
             default:
                amount = 0
            }
            let formattedAmount = String(format: "%.2f", amount)
            amountLabel.amountLabel.text = "\(EasyUtils.shared.getLocalizedDigits("\(formattedAmount)") ?? "")"
            
            serviceLabel.text = transaction?.serviceTitle
            
            if transaction?.statusEnum == 1{
                statusLabel.backgroundColor = .systemGreen
                statusLabel.text = transaction?.status
            }else if transaction?.statusEnum == 0{
                statusLabel.backgroundColor = .red
                statusLabel.text = transaction?.status
            }else if transaction?.statusEnum == 2{
                statusLabel.backgroundColor = UIColor.statusRed
                statusLabel.text = transaction?.status
            }else if transaction?.statusEnum == 3{
                statusLabel.backgroundColor = UIColor.blueViolet
                statusLabel.text = transaction?.status
            }else{
                statusLabel.backgroundColor = UIColor.red
                statusLabel.text = transaction?.status
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        let container = UIView()
        contentView.addSubview(container)
        container.addAnchorToSuperview(leading: 20, trailing: -20, top: 10, bottom: -10)
        container.addSubview(nameNumberStack)
        nameNumberStack.addAnchorToSuperview(leading: 0,  top: 0, bottom: 0)
        
        container.addSubview(statusAmountStack)
        statusAmountStack.addAnchorToSuperview(trailing: 0, top: 0, bottom: 0,widthMutiplier: 0.4)
        nameNumberStack.trailingAnchor.constraint(equalTo: statusAmountStack.leadingAnchor, constant: -10).isActive = true
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var  nameNumberStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 5
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        nameNumberStack.addArrangedSubview(nameLabel)
        nameNumberStack.addArrangedSubview(serviceLabel)
        return nameNumberStack
    }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.text = ""
        return label
    }()
    let serviceLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.textColor = .battleshipGrey
        label.text = ""
        return label
    }()
    lazy var  statusAmountStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 5
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        
        
        let statusView = UIView()
        statusView.addSubview(statusLabel)
        statusLabel.addAnchorToSuperview(trailing: 0,centeredVertically: 0)
        nameNumberStack.addArrangedSubview(statusView)
        nameNumberStack.addArrangedSubview(amountLabel)
        return nameNumberStack
    }()
    let statusLabel:EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        
        label.font = .EasyFont(ofSize: 10.0, style: .medium)
        label.leftInset = 8.0
        label.rightInset = 8.0
        label.topInset = 3.0
        label.bottomInset = 3.0
        label.textColor = .white
        label.textAlignment = .center
        label.text = ""
        
        label.sizeToFit()
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.layer.masksToBounds = true
        return label
    }()
    var amountLabel:ViewWithTakaSign = {
        let label = ViewWithTakaSign(amount: "0")
       
        return label
    }()
    
}
