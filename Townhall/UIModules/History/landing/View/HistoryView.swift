//
//  HistoryView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit

class HistoryView: EasyBaseView {
   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .iceBlue
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: -.calculatedHeight(70))
        
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.075)
        containerView.addSubview(pagingStack)
        pagingStack.addAnchorToSuperview(leading: 1, trailing: -1, heightMultiplier: 0.06)
        pagingStack.topAnchor.constraint(equalTo: navView.bottomAnchor, constant: 0).isActive = true
     
        containerView.addSubview(pageContainerView)
        pageContainerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        pageContainerView.topAnchor.constraint(equalTo: pagingStack.bottomAnchor, constant: 0).isActive = true
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "transaction_history".easyLocalized(), withBackButton: false)
        navView.backgroundColor = .white
        return navView
    }()
    
    lazy var pageContainerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .gray
        return v
    }()
    lazy var pagingStack : UIStackView = {
        let pagingStack = UIStackView()
        pagingStack.backgroundColor = .white
        pagingStack.translatesAutoresizingMaskIntoConstraints = false
        pagingStack.axis = .horizontal
        pagingStack.spacing = 0
        pagingStack.distribution = .fillEqually
        return pagingStack
    }()
    
    
}


class PagingItemView: UIView {
    var isActive:Bool = false{
        didSet{
            if isActive{
                label.textColor = .blueViolet
                activeView.backgroundColor = .blueViolet
                activeViewHeight?.constant = 2
              //  activeView.isHidden = false
            }else{
                label.textColor = .slateGrey
                activeView.backgroundColor = .paleLilac
                activeViewHeight?.constant = 1

                //activeView.isHidden = true
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .bold)
        label.text = ""
        label.textAlignment = .center
        label.textColor = .slateGrey
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var activeView: UIView = {
        let activeView = UIView()
        activeView.translatesAutoresizingMaskIntoConstraints = false
        activeView.backgroundColor = .paleLilac
        
        return activeView
    }()
    lazy var activeButton: UIButton = {
        let activeButton = UIButton()
        activeButton.translatesAutoresizingMaskIntoConstraints = false
        return activeButton
    }()
    var activeViewHeight:NSLayoutConstraint?
    convenience init(title: String) {
        self.init(frame:.zero)
        
        addSubview(label)
        label.text = title.uppercased()
        label.addAnchorToSuperview(leading:0 , trailing: 0, heightMultiplier: 0.8,  centeredVertically: 0)
        
        addSubview(activeView)
        activeView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        activeViewHeight = activeView.heightAnchor.constraint(equalToConstant: 1)
        activeViewHeight?.isActive = true
        //activeView.isHidden = true
        
        addSubview(activeButton)
        activeButton.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
    }
}
