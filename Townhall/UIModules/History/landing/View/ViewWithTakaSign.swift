//
//  ViewWithTakaSign.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/3/21.
//

import UIKit
enum HorizontalPosition {
    case left,center,right
}
class ViewWithTakaSign: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var amountText : String?{
        didSet{
            amountLabel.text = amountText
        }
    }
    convenience init(amount:String,postion:HorizontalPosition = .right) {
        self.init(frame:.zero)
        addSubview(amountLabel)
        switch postion {
        case .right:
            amountLabel.addAnchorToSuperview(trailing: 0, top: 0, bottom: 0)
        default:
            amountLabel.addAnchorToSuperview( top: 0, bottom: 0,centeredHorizontally: 0)
        }
        
        addSubview(takasignLabel)
        
        takasignLabel.translatesAutoresizingMaskIntoConstraints = false
        takasignLabel.centerYAnchor.constraint(equalTo:amountLabel.centerYAnchor , constant: 0).isActive = true
        takasignLabel.trailingAnchor.constraint(equalTo:amountLabel.leadingAnchor , constant: 0).isActive = true
        amountLabel.text = amount
    }
     let amountLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .bold)
        label.textColor = .black
        label.textAlignment = .right
        label.text = ""
        return label
    }()
    let takasignLabel :UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .bold)
        label.textColor = .black
        label.textAlignment = .right
        label.text = "৳"
        
        return label
    }()
}
