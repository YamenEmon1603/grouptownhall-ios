//
//  HistoryViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit

class HistoryViewController: EasyBaseViewController {
    
    let itemStrings = ["last_Seven_days".easyLocalized(),"last_thirty_days".easyLocalized(),"six_month".easyLocalized(),"all_time".easyLocalized()]
    let mView : HistoryView = {
        let mView = HistoryView()
        return mView
    }()
    lazy var pagingItems: [(pagingItem : PagingItemView ,vc:UIViewController)] = {
        var items : [(pagingItem : PagingItemView ,vc:UIViewController)] = []
        for (index,str) in itemStrings.enumerated(){
            let item = PagingItemView(title: str)
            item.activeButton.tag = 1200+index
            item.activeButton.addTarget(self, action: #selector(switchTabAction(_:)), for: .touchUpInside)
            let vc  = HistoryListViewController()
            
            switch index {
            case 0:
                vc.type = .SevenDays
            case 1:
                vc.type = .ThirtyDays
            case 2:
                vc.type = .SixMonth
            case 3:
                vc.type = .AllTime
            default:
                break
            }
            
            items.append((pagingItem : item ,vc:vc))
        }
        return items
    }()
    var thePageVC: EasyPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        thePageVC = EasyPageViewController()
        thePageVC.pageDelegate = self
        pagingItems.forEach { item in
            mView.pagingStack.addArrangedSubview(item.pagingItem)
            thePageVC.pages.append(item.vc)
        }
        pagingItems[0].pagingItem.isActive = true
        //        thePageVC.pages = [UIViewController(),UIViewController()]
        addChild(thePageVC)
        
        // we need to re-size the page view controller's view to fit our container view
        thePageVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        // add the page VC's view to our container view
        mView.pageContainerView.addSubview(thePageVC.view)
        
        thePageVC.view.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        thePageVC.didMove(toParent: self)
    }
    
    
    @objc func switchTabAction(_ sender: UIButton){
        thePageVC.setPage(index: sender.tag - 1200)
    }
    
}
extension HistoryViewController : EasyPageDelegate
{
    func pageDidChanged(to: Int) {
        for (index,_) in pagingItems.enumerated(){
            pagingItems[index].pagingItem.isActive = to == index
        }
        
    }
    
    
}
