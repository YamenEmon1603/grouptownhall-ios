//
//  HistoryDetailsView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit

class HistoryDetailsView: EasyBaseView {
    var color: UIColor = .blueViolet{
        didSet{
            navView.backgroundColor = color
            contentView.backgroundColor = color
        }
    }
    var deepColor:UIColor = .azul{
        didSet{
            self.backgroundColor = deepColor
            self.dateStatusView.backgroundColor = deepColor
        }
    }
    convenience init() {
        self.init(frame: .zero)
       
        self.backgroundColor = deepColor
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        self.addSubview(dateStatusView)
        dateStatusView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.05)
        dateStatusView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
        self.addSubview(contentView)
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
        contentView.topAnchor.constraint(equalTo: dateStatusView.bottomAnchor).isActive = true
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var contentView: UIView = {
        let contentView = UIView()
        contentView.backgroundColor = color
        contentView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        stackView.addArrangedSubview(viewDetails)
       // stackView.addArrangedSubview(viewTransactionRating)
        return contentView
        
    }()
    lazy var viewDetails:DetailsView = {
        let view = DetailsView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    
//    lazy var viewTransactionRating:TransactionRatingView = {
//        let view = TransactionRatingView()
//        view.translatesAutoresizingMaskIntoConstraints =  false
//        return view
//    }()
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"transaction_details".easyLocalized().uppercased())
        navView.backgroundColor = color
        navView.navTintColor = .white
        return navView
    }()
    lazy var dateStatusView : UIView = {
        let view = UIView()
        view.backgroundColor = deepColor
        view.addSubview(dateLabel)
        dateLabel.addAnchorToSuperview(leading: 20, centeredVertically: 0)
        
        let statusView = UIView()
        statusView.addSubview(statusLabel)
        statusLabel.addAnchorToSuperview(trailing: 0,centeredVertically: 0)
        view.addSubview(statusView)
        statusView.addAnchorToSuperview(trailing: -20, centeredVertically: 0)
        dateLabel.trailingAnchor.constraint(equalTo:statusView.leadingAnchor , constant: -10).isActive = true
        return view
    }()
    let dateLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .medium)
        label.textColor = .white
        label.text = ""
        return label
    }()
    let statusLabel:EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.font = .EasyFont(ofSize: 10.0, style: .medium)
        label.leftInset = 6.0
        label.rightInset = 6.0
        label.topInset = 2.0
        label.bottomInset = 2.0
        label.textColor = .white
        label.textAlignment = .center
        label.text = "Success"
        label.backgroundColor = .systemGreen
        label.sizeToFit()
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        return label
    }()
}

class DetailsView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(){
        self.init(frame:.zero)
        
        let topStack = UIStackView()
        topStack.axis = .vertical
        topStack.distribution = .fillEqually
        
        addSubview(topStack)
        topStack.addAnchorToSuperview(leading: 20, trailing: -20)
        topStack.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -20).isActive = true
        topStack.addArrangedSubview(transactionTypeLabel)
        topStack.addArrangedSubview(receiverNameLabel)
        topStack.addArrangedSubview(receiverNumberLabel)
        
        let bottomStack = UIStackView()
        bottomStack.axis = .vertical
        bottomStack.distribution = .fillEqually
        addSubview(bottomStack)
        bottomStack.addAnchorToSuperview(leading: 20, trailing: -20)
        bottomStack.topAnchor.constraint(equalTo: centerYAnchor, constant: 20).isActive = true
        bottomStack.addArrangedSubview(totalAmountKeyLabel)
        bottomStack.addArrangedSubview(amountLabel)
        
        addSubview(transActionIdLabel)
        transActionIdLabel.addAnchorToSuperview( centeredHorizontally: 0)
        transActionIdLabel.topAnchor.constraint(equalTo: bottomStack.bottomAnchor, constant: 20).isActive = true
       
        
    }
    let transactionTypeLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .regular)
        label.textColor = .white
        label.textAlignment = .center
        label.text = ""
        return label
    }()
    let receiverNameLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 26, style: .medium)
        label.textAlignment = .center
        label.textColor = .white
        label.text = ""
        return label
    }()
    let receiverNumberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .regular)
        label.textColor = .white.withAlphaComponent(0.7)
        label.textAlignment = .center
        label.text = "phone | operator"
        label.isHidden = true
        return label
    }()
   
    let totalAmountKeyLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .regular)
        label.textColor = .white.withAlphaComponent(0.7)
        label.textAlignment = .center
        label.text = "total_amount".easyLocalized()
        return label
    }()
    let amountLabel:ViewWithTakaSign = {
        let view = ViewWithTakaSign(amount: "", postion: .center)
        
        view.amountLabel.font = .EasyFont(ofSize: 26.0, style: .bold)
        view.amountLabel.textColor = .white
        view.amountLabel.textAlignment = .center
        view.takasignLabel.font = .EasyFont(ofSize: 12.0, style: .medium)
        view.takasignLabel.textColor = .white
        return view
    }()
    let transActionIdLabel:EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.leftInset = 11.0
        label.rightInset = 11.0
        label.topInset = 5.0
        label.bottomInset = 5.0
        label.textColor = .white
        label.textAlignment = .center
        label.text = ""//"Transaction ID : 3877756988DX6A"
        label.backgroundColor = .purpleBrown
        label.sizeToFit()
        label.layer.cornerRadius = 12
        label.layer.borderColor = UIColor.clear.cgColor
        label.clipsToBounds = true
        return label
    }()
}

class TransactionRatingView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(rating:Int){
        self.init(frame:.zero)
        let topView = UIView()
        addSubview(topView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.2)
        let _ = topView.addBorder(side: .top, color: .azul, width: 1)
        let _ = topView.addBorder(side: .bottom, color: .azul, width: 1)
        let topStack = UIStackView()
        topStack.axis = .vertical
        topStack.distribution = .fillEqually
        
        topView.addSubview(topStack)
        topStack.addAnchorToSuperview(leading: 20, trailing: -20,centeredVertically: 0)
       
        topStack.addArrangedSubview(transactionIdLabel)
        topStack.addArrangedSubview(tokenLabel)
        topStack.addArrangedSubview(transactionSourceLabel)
        
        let bottomView = UIView()
        bottomView.backgroundColor = .white
        addSubview(bottomView)
        bottomView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
        let starRatinView = StarRatingView(text: "rate_your_experience".easyLocalized(), subtext: "your_rating_for_this_transaction".easyLocalized(), maxrating: 5, currentRating: rating)
        bottomView.addSubview(starRatinView)
        
        starRatinView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.6)
      
        bottomView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview( widthMutiplier: 0.6, centeredHorizontally: 0,heightWidthRatio: 0.2)
        btnProceed.topAnchor.constraint(equalTo: starRatinView.bottomAnchor,constant: 15).isActive = true
    }
    let transactionIdLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .regular)
        label.textColor = .white.withAlphaComponent(0.7)
        label.textAlignment = .center
        label.text = "" //"Transaction ID : 3877756988DX6A"
        return label
    }()
    let tokenLabel:SRCopyableLabel = {
        let label = SRCopyableLabel()
        label.font = .EasyFont(ofSize: 10.0, style: .regular)
        label.textColor = .white.withAlphaComponent(0.7)
        label.textAlignment = .center
        label.text = "" //"Transaction ID : 3877756988DX6A"
        return label
    }()
    let transactionSourceLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .regular)
        label.textColor = .white.withAlphaComponent(0.7)
        label.textAlignment = .center
        label.text = "" //"VISA (**** 87DX) > PAYMENT "
        return label
    }()
   let starRatinView : StarRatingView = {
        let starRatinView = StarRatingView(text: "Rate Your Experience", subtext: "How satisfied are you with the transaction?", maxrating: 5, currentRating: 3)
        return starRatinView
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.layer.cornerRadius = 10
        //btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
}
class SRCopyableLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        isUserInteractionEnabled = true
    }
    
}
