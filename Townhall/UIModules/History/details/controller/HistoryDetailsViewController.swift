//
//  HistoryDetailsViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit

class HistoryDetailsViewController: EasyBaseViewController {
    var transaction:DayWiseTransaction?
    lazy var mView : HistoryDetailsView = {
        let mView = HistoryDetailsView()
        mView.navView.backButtonCallBack = backButtonAction
        
        return mView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        if let serviceTitle = transaction?.serviceTitle{
            mView.viewDetails.transactionTypeLabel.text = serviceTitle
        }
      
        if transaction?.statusEnum == 1{
            if let rating = transaction?.rating{
                debugPrint(transaction?.rating ?? 0.0)
                let ratingView = TransactionRatingView(rating: rating)
                mView.stackView.addArrangedSubview( ratingView)
                if let tid = transaction?.transactionId{
                   ratingView.transactionIdLabel.text = tid
                }
                if let paymentMethod = transaction?.paymentMethod{
                    ratingView.transactionSourceLabel.text = paymentMethod
                }
                
                if let token = transaction?.token{
                    ratingView.tokenLabel.text = "Token: \(token)"
                    ratingView.tokenLabel.isHidden = false
                }else{
                    ratingView.tokenLabel.text = ""
                    ratingView.tokenLabel.isHidden = true
                }
            }
            mView.color = .blueViolet
            mView.deepColor = .azul
        }else{
             mView.color = .bloodRed
             mView.deepColor = .easyMaron
        }
        if  transaction?.orderData?.dada?.first?.label == "Mobile No."{
            if let mobile = transaction?.orderData?.dada?.first?.value{
                mView.viewDetails.receiverNameLabel.text = EasyUtils.shared.getLocalizedDigits(mobile)
              
            }
        }else if transaction?.orderData?.dada?.first?.label == "Bill Number"{
            if let account = transaction?.orderData?.dada?.first?.value{
                mView.viewDetails.receiverNameLabel.text = account
            }
        }else{
            if let label = transaction?.orderData?.dada?.first?.value{
                mView.viewDetails.receiverNameLabel.text = label
            }
        }
        var amount : Double = 0.0
        switch transaction?.amount{
        case .integer(let intData):
            amount = Double(intData)
        case .string(let str):
            amount = Double(str) ?? 0
        case .double(let douData):
            amount = Double(douData) 
        default:
            amount = 0
        }


        let formattedAmount = String(format: "%.2f", amount)
        mView.viewDetails.amountLabel.amountLabel.text = "\(EasyUtils.shared.getLocalizedDigits("\(formattedAmount)") ?? "")"

        if transaction?.statusEnum == 1{
            mView.viewDetails.transActionIdLabel.isHidden = true
            mView.statusLabel.backgroundColor = .systemGreen
            mView.statusLabel.text = transaction?.subBillStatus//success[EasyUtils.getLanguageIndex()]//"Success"

        }else{
            mView.viewDetails.transActionIdLabel.isHidden = false
            if let tid = transaction?.transactionId{
                mView.viewDetails.transActionIdLabel.text = "\("transaction_id".easyLocalized()) \(tid)"
            }
            mView.statusLabel.backgroundColor = .tomato
             mView.statusLabel.text = transaction?.subBillStatus//failed[EasyUtils.getLanguageIndex()]

        }
        if let transectionAt = transaction?.transactionAt{

            mView.dateLabel.text = EasyUtils.shared.forDate(toTimestamp: transectionAt, dateFormate: Constant.DateFormat3, expectedDateFormate: Constant.DateFormat12)
        }


    }
    func backButtonAction(){
        self.dismiss(animated: true, completion: nil)
    }

    

}
