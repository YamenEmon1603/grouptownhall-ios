//
//  ImageUploadViewController.swift
//  Townhall
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit
import Vision
import SwiftUI
import Alamofire
class ImageUploadViewController: EasyBaseViewController {
    var capturedImages : [UIImage]?
    var uploadTimer: Timer?
    var counter = 0
    lazy var mView: ImageUploadView = {
        let mView = ImageUploadView()
        mView.proceedCallBack = procced
        mView.navView.backButtonCallBack = backAction
        mView.retakeImageCallback = retake(_:)
        return mView
    }()
    

    var employeePhoneNumer:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
    }
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
   
   @objc func runTimedCode(){
       counter += 1
       if counter % 2 == 0{
           mView.btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "Uploading Photos.. "), for: .normal)
       }else{
           mView.btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "Uploading Photos..."), for: .normal)
       }
       
    }

    
    func procced(){
        if let capturedImages = self.capturedImages , capturedImages.count == 5{
            mView.btnProceed.isEnabled = false
            //mView.btnProceed.backgroundColor = .paleLilac
            mView.btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "Uploading Photos.  "), for: .normal)
            uploadTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
           
            mView.itemViews.forEach { iv in
                iv.btnCapture.isEnabled = false
                iv.btnCapture.setImage(nil, for: .normal)
                iv.addSubview(iv.frorground)
                iv.frorground.addAnchorToSuperview(leading: 0, top: 0)
                iv.frorground.bottomAnchor.constraint(equalTo: iv.imageView.bottomAnchor).isActive = true
                iv.progressWidth = iv.frorground.widthAnchor.constraint(equalToConstant: iv.frame.width)
                iv.progressWidth?.isActive = true
            }
            uploadImage(index: 0)
            
        }else{
            let vc  = CaptureViewController()
            vc.modalPresentationStyle = .overCurrentContext
            vc.captureDelegate = self
            vc.employeePhoneNumer = self.employeePhoneNumer
            self.present(vc, animated: true)
        }
        
    }
    func retake(_ view : ImageItemView){
        let vc  = CaptureViewController()
        vc.modalPresentationStyle = .overCurrentContext
        vc.singleCaptureCallBack = { img in
            let cropped = self.cropImageToSquare(image: img)
            view.image = cropped
            self.capturedImages?[view.tag - 100] = cropped ?? img
            //            cropped?.detector.crop(type: .face, completion: { res in
            //                switch res {
            //                case .success(let images):
            //                    print(images.count)
            //                    view.image = images.first
            //                case .notFound:
            //                    print("not found")
            //                case .failure(let error):
            //                    print(error.localizedDescription)
            //                }
            //            })
        }
        vc.instruction = view.labelUploadPic.text ?? ""
        self.present(vc, animated: true)
    }
    func uploadImage(index: Int){
      
        if let capturedImages = capturedImages , index < capturedImages.count{
            guard let employeePhoneNumer = employeePhoneNumer else {
                showToast(message: "Something went wrong")
                return
            }
            guard let imageData = capturedImages[index].jpegData(compressionQuality: 1.0)else//UIImageJPEGRepresentation(tempImage, 0.7)
            {
                return
            }
           
            WebServiceHandler.shared.townhallImageUpload(phone: employeePhoneNumer, serial: index, imageData: imageData, completion: { result in
                switch result{
                case .success(let res):
                    if res.code == 200 {
                        //self.showToast(message: "upload done")
                        self.mView.itemViews[index].frorground.removeFromSuperview()
                        self.mView.itemViews[index].btnCapture.setImage(UIImage(named: "selectGreen"), for: .disabled)
                        self.uploadImage(index: index+1)
                    }else{
                        self.showToastError(message: res.message ?? "")
                        self.uploadTimer?.invalidate()
                        self.mView.btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "Upload Photos   "), for: .normal)
                        self.mView.itemViews[index].btnCapture.setImage(UIImage(named: "crossred"), for: .disabled)
                    }
                case .failure(let error):
                    self.showToastError(message: error.localizedDescription )
                }
            },progressBlock: { completed in
                print("Final \(completed)")
                self.mView.itemViews[index].progressWidth?.constant = self.mView.itemViews[index].frame.width * (1-completed)
                if index == 5{
                    self.uploadTimer?.invalidate()
                    self.mView.btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "Upload Photos   "), for: .normal)
                }
            }, shouldShowLoader: false)
        }else{
            (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
}
extension ImageUploadViewController : ImageCaptureDelegate{
    func captureController(_ controller: CaptureViewController, didCapture: [UIImage]) {
        //SHOW ALERT\\
        
        self.backAction()
        
//        mView.addSubview(bottomView)
//        bottomView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.5)
        
        
//        if didCapture.count == mView.itemViews.count {
//            capturedImages = didCapture
//            mView.btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "Upload Photos   "), for: .normal)
//            for i in 0..<didCapture.count {
//                let cropped = self.cropImageToSquare(image: didCapture[i])
//                mView.itemViews[i].image =  cropped
//            }
//
//        }
      
    }
    
    func cropImageToSquare(image: UIImage) -> UIImage? {
        
        var imageHeight = image.size.height
        var imageWidth = image.size.width
        
        if imageHeight > imageWidth {
            imageHeight = imageWidth
        }
        else {
            imageWidth = imageHeight
        }
        
        let size = CGSize(width: imageWidth, height: imageHeight)
        
        let refWidth : CGFloat = CGFloat(image.cgImage!.width)
        let refHeight : CGFloat = CGFloat(image.cgImage!.height)
        
        let x = (refWidth - size.width) / 2
        let y = (refHeight - size.height) / 2
        
        let cropRect = CGRect(x: x, y: y, width: size.height, height: size.width)
        if let imageRef = image.cgImage!.cropping(to: cropRect) {
            return UIImage(cgImage: imageRef, scale: 0, orientation: image.imageOrientation)
        }
        
        return nil
    }
}
