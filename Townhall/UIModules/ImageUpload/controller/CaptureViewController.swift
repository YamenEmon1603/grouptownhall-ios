//
//  CaptureViewController.swift
//  Townhall
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit
import AVFoundation
import Alamofire
protocol ImageCaptureDelegate{
    func captureController (_ controller: CaptureViewController, didCapture :  [UIImage] )
}
class CaptureViewController: EasyBaseViewController {
    //Camera Capture requiered properties
    var videoDataOutput: AVCaptureVideoDataOutput!
    var videoDataOutputQueue: DispatchQueue!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var captureDevice : AVCaptureDevice!
    let session = AVCaptureSession()
    var cameraOutput : AVCapturePhotoOutput!
    var capturedImages : [UIImage] = []
    var captureDelegate: ImageCaptureDelegate?
    var singleCaptureCallBack: ((_ image: UIImage )->Void)?
    var currentImage:UIImage?
    var employeePhoneNumer : String?
    var instruction: String = "Capture Straight"
    lazy var mView : CaptureView = {
        let v = CaptureView()
        v.backButtonCallBack =  {self.dismiss(animated: true)}//self.navigationController?.popViewController(animated: true)
        v.btnCapture.addTarget(self, action: #selector(didPressTakePhoto(_:)), for: .touchUpInside)
        v.btnRetake.addTarget(self, action: #selector(retakeAction), for: .touchUpInside)
        v.btnContinue.addTarget(self, action: #selector(captureDidfinish), for: .touchUpInside)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        mView.instructionLabel.text = "Please Wait"
        mView.btnCapture.isEnabled = false
        EasyUtils.shared.delay(2.0) {
            self.setupAVCapture()
            self.mView.instructionLabel.text = self.instruction
            self.mView.btnCapture.isEnabled = true
        }
        if singleCaptureCallBack == nil{
            mView.progressView.progress = 1
        }else{
            mView.progressView.isHidden = true
            
        }
        
    }
    
    lazy var bottomView: BottomAlertView = {
        let view =  BottomAlertView()
        view.proceedCallBack = alertGoHome
        return view
    }()
    func alertGoHome(){
       
        self.bottomView.removeFromSuperview()
        
        self.dismiss(animated: true) {
            self.captureDelegate?.captureController(self, didCapture: self.capturedImages)
        }
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        session.stopRunning()
    }
    func backButtonTapped(){
        
    }
    func changeInstruction(){
        print(capturedImages.count+1)
        mView.progressView.progress = capturedImages.count + 1
        
        switch capturedImages.count
        {
        case 0:
            mView.instructionLabel.text = "Capture Straight"
            mView.retake()
        case 1:
            mView.instructionLabel.text = "Capture Face Right"
        case 2:
            mView.instructionLabel.text = "Capture Face Left"
        case 3:
            mView.instructionLabel.text = "Capture Face Up"
        case 4:
            mView.instructionLabel.text = "Capture Face down"
        case 5:
            mView.instructionLabel.text = ""
        default:
            mView.instructionLabel.text = "Capture Straight"
        }
    }
    @objc func retakeAction(){
        currentImage = nil
        mView.retake()
    }
    @objc func captureDidfinish(){
        guard let currentImage = currentImage else {
            return
        }
        session.stopRunning()
        uploadImage(image: currentImage,index: capturedImages.count + 1)
        
        
        
    }
    func uploadImage(image:UIImage,index:Int){
        
        let imageSize =  Int(EasyDataStore.sharedInstance.dashboardContent?.townhall?.maxImageSize ?? "1024") ?? 1024
        guard let employeePhoneNumer = employeePhoneNumer else {
            showToast(message: "Something went wrong")
            return
        }
        let progressHud = ProgressHUD(text: "Processing")
        self.view.addSubview(progressHud)
        progressHud.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        progressHud.text = "Processing.."
        
        let imageData = image.compress(to: imageSize, allowedMargin: 0.2)
        progressHud.text = "Uploading Image.."
        
        
        WebServiceHandler.shared.townhallImageUpload(phone: employeePhoneNumer, serial: index, imageData: imageData, completion: { result in
            switch result{
            case .success(let res):
                if res.code == 200 {
                    if self.capturedImages.count < 4{
                        self.capturedImages.append(image)
                        self.changeInstruction()
                        progressHud.removeFromSuperview()
                        self.mView.retake()
                        self.session.startRunning()
                        
                    }else{
                        progressHud.removeFromSuperview()
                        self.mView.addSubview(self.bottomView)
                        self.bottomView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.4)
                        
                    }
                }else{
                    self.showToastError(message: res.message ?? "")
                    progressHud.removeFromSuperview()
                    self.mView.retake()
                    self.session.startRunning()
                }
            case .failure(let error):
                progressHud.removeFromSuperview()
                self.showToastError(message: error.localizedDescription )
            }
        },progressBlock: { completed in
            if completed == 1.0{
                progressHud.text = "Detecting face.Please wait..."
            }
        }, shouldShowLoader: false)
    }
    
}


extension CaptureViewController : AVCapturePhotoCaptureDelegate {
    func setupAVCapture(){
        session.sessionPreset = AVCaptureSession.Preset.photo
        guard let device = AVCaptureDevice
                .default(AVCaptureDevice.DeviceType.builtInWideAngleCamera,
                         for: .video,
                         position: AVCaptureDevice.Position.front) else {
                    return
                }
        captureDevice = device
        startPhotoCaptureSession()
        
    }
    func startPhotoCaptureSession(){
        session.sessionPreset = AVCaptureSession.Preset.photo
        cameraOutput = AVCapturePhotoOutput()
        if let input = try? AVCaptureDeviceInput(device: captureDevice) {
            if (session.canAddInput(input)) {
                session.addInput(input)
                if (session.canAddOutput(cameraOutput)) {
                    session.addOutput(cameraOutput)
                    previewLayer = AVCaptureVideoPreviewLayer(session: session)
                    previewLayer.frame = mView.contentView.bounds
                    previewLayer.videoGravity = .resizeAspectFill
                    mView.contentView.layer.addSublayer(previewLayer)
                    session.startRunning()
                    mView.contentView.bringSubviewToFront(mView.imageView)
                    mView.contentView.bringSubviewToFront(mView.instructionLabel)
                }
            } else {
                print("issue here : captureSesssion.canAddInput")
            }
        } else {
            print("some problem here")
        }
    }
    
    // callBack from take picture
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print("error occure : \(error.localizedDescription)")
        }
        
        if  let sampleBuffer = photoSampleBuffer,
            let previewBuffer = previewPhotoSampleBuffer,
            let dataImage =  AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer:  sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            print(UIImage(data: dataImage)?.size as Any)
            
            let dataProvider = CGDataProvider(data: dataImage as CFData)
            let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            let image = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: UIImage.Orientation.right)
            if singleCaptureCallBack == nil{
                self.currentImage = image
                mView.preViewIv.image = image
                mView.readyToContinue()
            }else{
                self.dismiss(animated: true) {
                    self.singleCaptureCallBack?(image)
                }
            }
            
            
            
            
        } else {
            print("some error here")
        }
    }
    
    @IBAction func didPressTakePhoto(_ sender: UIButton) {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        settings.previewPhotoFormat = previewFormat
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }
}

class CaptureView: UIView {
    var backButtonCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.11)
        
        self.addSubview(btnView)
        btnView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier: 0.1667)
        
        self.addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: btnView.topAnchor).isActive = true
        contentView.contentMode = .scaleAspectFit
        
        self.addSubview(preViewIv)
        preViewIv.addAnchorToSuperview(leading: 0, trailing: 0)
        preViewIv.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        preViewIv.bottomAnchor.constraint(equalTo: btnView.topAnchor).isActive = true
        preViewIv.contentMode = .scaleAspectFit
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var   contentView : UIView = {
        let v = UIView()
        v.addSubview(imageView)
        imageView.addAnchorToSuperview( widthMutiplier: 0.7, centeredVertically: -30, centeredHorizontally: 0, heightWidthRatio: 1)
        v.addSubview(instructionLabel)
        instructionLabel.addAnchorToSuperview( centeredHorizontally: 0)
        instructionLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor,constant: .calculatedHeight(80)).isActive = true
        return v
    }()
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "imgFocus")
        
        return imageView
    }()
    let preViewIv : UIImageView = {
        let preview = UIImageView()
        preview.contentMode = .scaleToFill
        preview.isHidden = true
        preview.backgroundColor = .black
        return preview
    }()
    lazy var navView : UIView = {
        let navView = UIView()
        navView.backgroundColor = .black
        //        navView.addSubview(backButton)
        //        backButton.addAnchorToSuperview(leading: 10 ,heightMultiplier: 0.8, centeredVertically: 0,  heightWidthRatio: 1.2)
        
        navView.addSubview(progressView)
        progressView.addAnchorToSuperview(widthMutiplier: 0.45, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 0.07)
        
        return navView
    }()
    let progressView : ImageProgressView = ImageProgressView()
    private let backButton:UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "backImage"), for: .normal)
        backButton.tintColor = .white
        backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        return backButton
    }()
    let instructionLabel : EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.leftInset = 15
        label.rightInset = 15
        label.topInset = 15
        label.bottomInset = 15
        label.textAlignment = .center
        label.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        label.textColor = .white
        label.font = .EasyFont(ofSize: 16, style: .regular)
        label.text = "Capture Straight"
        label.layer.borderWidth = 0
        label.layer.cornerRadius = 25
        label.clipsToBounds = true
        return label
    }()
    
    @objc func btnBackAction(_ sender:UIButton){
        backButtonCallBack?()
    }
    
    lazy var btnView : UIView = {
        let btnView = UIView()
        btnView.backgroundColor = .blueViolet
        btnView.addSubview(btnCapture)
        btnCapture.addAnchorToSuperview( heightMultiplier: 0.58, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
        return btnView
    }()
    
    
    let btnCapture: UIButton = {
        let btm = UIButton()
        btm.setImage(UIImage(named: "capture"), for: .normal)
        return btm
    }()
    
    let btnRetake:UIButton = {
        let btnRetake = UIButton()
        btnRetake.backgroundColor = .white
        btnRetake.setTitle("Retake", for: .normal)
        btnRetake.setTitleColor(.blueViolet, for: .normal)
        btnRetake.layer.cornerRadius = 10
        btnRetake.clipsToBounds = true
        return btnRetake
    }()
    let btnContinue:UIButton = {
        let btnRetake = UIButton()
        btnRetake.backgroundColor = .pumpkinOrange
        btnRetake.setTitle("Continue", for: .normal)
        btnRetake.setTitleColor(.white, for: .normal)
        btnRetake.layer.cornerRadius = 10
        btnRetake.clipsToBounds = true
        return btnRetake
    }()
    lazy var stackView : UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 10
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.addArrangedSubview(btnRetake)
        stackView.addArrangedSubview(btnContinue)
        return stackView
    }()
    func readyToContinue(){
        instructionLabel.isHidden = true
        btnCapture.removeFromSuperview()
        btnView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 30, trailing: -30,  heightMultiplier: 0.47, centeredVertically: 0)
        preViewIv.isHidden = false
        contentView.isHidden = true
    }
    func retake(){
        instructionLabel.isHidden = false
        stackView.removeFromSuperview()
        btnView.addSubview(btnCapture)
        btnCapture.addAnchorToSuperview( heightMultiplier: 0.58, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
        preViewIv.image = nil
        preViewIv.isHidden = true
        contentView.isHidden = false
    }
}
class ImageProgressView: UIView{
    let progressStack:UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 6
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        return stackView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(progressStack)
        progressStack.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var progress:Int?{
        didSet{
            guard let progress = progress else {
                return
            }
            
            progressStack.arrangedSubviews.forEach { v in
                v.removeFromSuperview()
            }
            for i in 1...5{
                let view = UIView()
                view.backgroundColor = .clear
                let pV = UIView()
                view.addSubview(pV)
                if i < progress{
                    pV.backgroundColor = .trueGreen
                    pV.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,  heightMultiplier: 0.5)
                }else if i > progress{
                    pV.backgroundColor = .greyishBrown
                    pV.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,  heightMultiplier: 0.5)
                }else{
                    pV.backgroundColor = .pumpkinOrange
                    pV.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
                }
                
                progressStack.addArrangedSubview(view)
            }
        }
    }
}
