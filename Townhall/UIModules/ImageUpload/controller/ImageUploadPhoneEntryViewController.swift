//
//  ImageUploadPhoneEntryViewController.swift
//  Townhall
//
//  Created by Mausum Nandy on 9/12/21.
//

import UIKit

class ImageUploadPhoneEntryViewController: EasyBaseViewController {

    lazy var mView: ImageUploadPhoneEntryView = {
        let mView = ImageUploadPhoneEntryView()
        mView.proceedCallBack = proceed
        mView.navView.backButtonCallBack = backAction
        return mView
    }()
    var keyboardObserver : KeyboardObserver!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
      //  keyboardObserver = KeyboardObserver(for: self)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.termsTapped))
        mView.termsAndConditionLabel.isUserInteractionEnabled = true
        mView.termsAndConditionLabel.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
       // keyboardObserver.add()
    }
    @objc func termsTapped(sender:UITapGestureRecognizer) {
        let vc = WebPageVC()
        vc.type = .terms
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
    func proceed(){
        dismissKeyboard()
        guard let senderMobile =  mView.numberView.text,senderMobile.isEmpty == false else{
            self.showToast(message: "Please enter employee’s phone number")
            return
        }
        if senderMobile.isPhoneNumber == false ||  senderMobile.isElevenDigit == false {
            self.showToastError(message: "invalid_phone_no".easyLocalized())
            return
        }
        WebServiceHandler.shared.checkUserStatus(phone: senderMobile, completion: { result in
            switch result {
            case .success(let response):
                if response.code == 200{
                    if response.data?.userExist == true && response.data?.imageUploadStatus == false{
                        let vc = ImageUploadViewController()
                        vc.employeePhoneNumer = senderMobile
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        if response.data?.userExist == false{
                            self.showToast(message: "User doesn't exists")
                        }
                        if response.data?.imageUploadStatus == true {
                            self.showToast(message: "User already has uploaded images")
                        }
                    }
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
       
    }

}
