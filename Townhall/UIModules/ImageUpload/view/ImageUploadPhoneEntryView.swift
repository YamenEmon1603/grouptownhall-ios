//
//  ImageUploadPhoneEntryView.swift
//  Townhall
//
//  Created by Mausum Nandy on 9/12/21.
//

import UIKit

class ImageUploadPhoneEntryView: EasyBaseView {

    var bottomContraint : NSLayoutConstraint?
    var proceedCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.05)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
        addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.09)
        btnProceed.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var contentView: UIView = {
        let contentView = UIView()
        let scrollConatiner = UIView()
     
        contentView.addSubview(scrollConatiner)
        scrollConatiner.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        scrollConatiner.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor).isActive = true
        
        let scrollView = UIScrollView()
        scrollConatiner.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        let stackConatiner = UIView()
       
        scrollView.addSubview(stackConatiner)
        stackConatiner.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackConatiner.widthAnchor.constraint(equalTo: scrollConatiner.widthAnchor).isActive = true
        stackConatiner.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 1).isActive = true
        
       
        
     
        
        stackConatiner.addSubview(getStartedLabel)
        getStartedLabel.addAnchorToSuperview(leading: 20, trailing: -20,top: 10)
        
        getStartedLabel.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.04).isActive = true
        
        stackConatiner.addSubview(enterInfoLabel)
        enterInfoLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        enterInfoLabel.topAnchor.constraint(equalTo: getStartedLabel.bottomAnchor,constant: 8).isActive = true
     
        
        stackConatiner.addSubview(numberView)
        numberView.addAnchorToSuperview(leading: 20, trailing: -20)
        numberView.topAnchor.constraint(equalTo: enterInfoLabel.bottomAnchor,constant: 30).isActive = true
        numberView.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.09).isActive = true
        
        
        stackConatiner.addSubview(termsAndConditionLabel)
        termsAndConditionLabel.addAnchorToSuperview(leading: 30, trailing: -30)
        bottomContraint = termsAndConditionLabel.bottomAnchor.constraint(equalTo: stackConatiner.bottomAnchor,constant: -20)
        bottomContraint?.isActive = true
        return contentView
    }()
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "", leadingBtn: 10)
        return navView
    }()
   
    let getStartedLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 18.0, style: .bold)
        label.textColor = .black
        label.text = "Enter phone number"
        return label
    }()
    let enterInfoLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .regular)
        label.textColor = .slateGrey
        label.text = "For other’s TownHall registration please enter Employee’s phone number"
        label.numberOfLines  = 0
        return label
    }()
    lazy var   numberView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_phone_number".easyLocalized())
        textView.keyBoardType = .numberPad
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self  )
        return textView
    }()
    
    let termsAndConditionLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .regular)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.attributedText = SSLComLanguageHandler.sharedInstance.termsAndConditionsText()
        return label
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        btnProceed.titleEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }

}
extension ImageUploadPhoneEntryView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}
