//
//  ImageUploadView.swift
//  Townhall
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit
struct ImageItem {var title:String;var image:UIImage}
class ImageUploadView: UIView {
    var retakeImageCallback: (( _ view : ImageItemView)->Void)?
    var bottomContraint : NSLayoutConstraint?
    var proceedCallBack : (()->Void)?
    var images : [ImageItem] = [ImageItem(title: "1.Capture StraightFace", image: UIImage(named: "imgStraight")!), ImageItem(title: "2. Capture faceRight", image: UIImage(named: "imgRightSide")!),ImageItem(title: "3. Capture FaceLeft", image: UIImage(named: "imgLeftSide")!),ImageItem(title: "4. Capture Face Up", image: UIImage(named: "imgDownside")!),ImageItem(title: "5. Capture Face Down", image: UIImage(named: "imgUpSide")!)]
    var itemViews: [ImageItemView] = []
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0)
        if EasyDataStore.sharedInstance.dashboardContent?.imageUploadStatus != 0 {
            self.addSubview(navView)
            navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.05)
            navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
            contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        }else{
            contentView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: .calculatedHeight(20)).isActive = true
        }
        addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.07)
        btnProceed.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var contentView: UIView = {
        let contentView = UIView()
        let scrollConatiner = UIView()
     
        contentView.addSubview(scrollConatiner)
        scrollConatiner.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        scrollConatiner.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor).isActive = true
        
        let scrollView = UIScrollView()
        scrollConatiner.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        let stackConatiner = UIView()
       
        scrollView.addSubview(stackConatiner)
        stackConatiner.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackConatiner.widthAnchor.constraint(equalTo: scrollConatiner.widthAnchor).isActive = true
        stackConatiner.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 1).isActive = true
        
       
        
        stackConatiner.addSubview(labelUploadPic)
        labelUploadPic.addAnchorToSuperview(leading: 20,trailing:-20, top: 5)
        
        
        stackConatiner.addSubview(getStartedLabel)
        getStartedLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        getStartedLabel.topAnchor.constraint(equalTo: labelUploadPic.bottomAnchor,constant: 8).isActive = true
        
        let rowOneStack  = UIStackView()
        rowOneStack.axis = .horizontal
        rowOneStack.distribution = .fillEqually
        rowOneStack.spacing = 8
        stackConatiner.addSubview(rowOneStack)
        rowOneStack.addAnchorToSuperview(leading: 20, trailing: -20)
       // rowOneStack.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.2).isActive = true
        rowOneStack.topAnchor.constraint(equalTo: getStartedLabel.bottomAnchor, constant: 20).isActive = true
        
        for i in  0...2{
            let v = ImageItemView(item: images[i])
            v.tag = 100+i
            rowOneStack.addArrangedSubview(v)
            v.retakeCallback = retakeImage(_:)
            itemViews.append(v)
        }
        let rowTwoStack  = UIStackView()
        rowTwoStack.axis = .horizontal
        rowTwoStack.distribution = .fillEqually
        rowTwoStack.spacing = 8
        stackConatiner.addSubview(rowTwoStack)
        rowTwoStack.addAnchorToSuperview(leading: 20, trailing: -20)
       // rowOneStack.heightAnchor.constraint(equalTo: scrollConatiner.heightAnchor, multiplier: 0.2).isActive = true
        rowTwoStack.topAnchor.constraint(equalTo: rowOneStack.bottomAnchor, constant: 20).isActive = true
        for i in 3...4{
            let v = ImageItemView(item: images[i])
            v.tag = 100+i
            rowTwoStack.addArrangedSubview(v)
            v.retakeCallback = retakeImage(_:)
            itemViews.append(v)
        }
        rowTwoStack.addArrangedSubview(UIView())
        
        
        
        return contentView
    }()
    func retakeImage(_ view: ImageItemView){
        retakeImageCallback?(view)
    }
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "", leadingBtn: 10)
        return navView
    }()
   
    let getStartedLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 30.0, style: .bold)
        label.textColor = .black
        
        let attributedString = NSMutableAttributedString(string: "Upload your face\nimage with following\n5 different style.", attributes: [
            .font: UIFont.EasyFont(ofSize: 30, style: .bold),
          .foregroundColor: UIColor(white: 0.0, alpha: 1.0)
        ])
        attributedString.addAttribute(.foregroundColor, value: UIColor.blueViolet, range: NSRange(location: 38, length: 18))
        label.attributedText = attributedString
        label.numberOfLines = 0
        return label
    }()
    let enterInfoLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .regular)
        label.textColor = .slateGrey
        label.text = "enter_your_personal_number".easyLocalized()
        return label
    }()
    let labelUploadPic: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 18.0, style: .bold)
        label.textColor = .black
        label.text = "Upload Pictures"
        return label
    }()
    
   
     let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "Capture Photo"), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
         btnProceed.titleEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }

}

class ImageItemView : UIView {
    var retakeCallback: ((_ view:ImageItemView )->Void)?
    var progressWidth:NSLayoutConstraint?
    var image:UIImage?{
        didSet{
            imageView.image = image
            isImageChanged = true
            
        }
    }
    var title:String?{
        didSet{
            labelUploadPic.text = title
        }
    }
    var isImageChanged:Bool = false{
        didSet{
            addSubview(btnCapture)
            btnCapture.addAnchorToSuperview( trailing: 5, top: -5,heightMultiplier: 0.24, heightWidthRatio: 1)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(item : ImageItem){
        self.init(frame: .zero)
        self.backgroundColor = .white
        self.layer.cornerRadius = 6
        self.clipsToBounds = true
        self.addSubview(labelUploadPic)
        labelUploadPic.text = item.title
        labelUploadPic.adjustsFontSizeToFitWidth = true
        labelUploadPic.addAnchorToSuperview(leading: 5, trailing: -5,  bottom: 0)
        
        self.addSubview(imageView)
        imageView.image = item.image
        imageView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,heightMultiplier: 0.8,heightWidthRatio: 1)
        
        imageView.bottomAnchor.constraint(equalTo: labelUploadPic.topAnchor,constant: -5).isActive = true
    }
    let labelUploadPic: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .regular)
        label.textColor = .slateGrey
        label.text = "1. Capture straight"
        
        return label
    }()
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "splashImageNew")
     
        return imageView
    }()
    let btnCapture: UIButton = {
        let btm = UIButton()
        btm.setImage(UIImage(named: "retakeicon"), for: .normal)
        btm.addTarget(self, action: #selector(retakeAction), for: .touchUpInside)
        return btm
    }()
    @objc func retakeAction(){
        retakeCallback?(self)
    }
    let frorground: UIView = {
        let v = UIView()
        v.backgroundColor = .black.withAlphaComponent(0.5)
        return v
    }()
}

class BottomAlertView : UIView {
    
    var proceedCallBack : (()->Void)?
   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(){
        self.init(frame: .zero)
        self.backgroundColor = .blueViolet
        self.layer.cornerRadius = 6
        self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.clipsToBounds = true
        self.addSubview(imageViewSubmit)
        self.addSubview(lblCompletedHeader)
        self.addSubview(lblCompletedSubtitle)
        self.addSubview(btnProceed)
        imageViewSubmit.addAnchorToSuperview(top: 27, heightMultiplier: 0.2, centeredHorizontally: 0, heightWidthRatio: 1)
        lblCompletedHeader.addAnchorToSuperview(leading: 30, trailing: -30)
        lblCompletedHeader.topAnchor.constraint(equalTo: imageViewSubmit.bottomAnchor, constant: 15).isActive = true
        
        
        lblCompletedSubtitle.addAnchorToSuperview(leading: 30, trailing: -30)
        lblCompletedSubtitle.bottomAnchor.constraint(equalTo: btnProceed.topAnchor, constant: 8).isActive = true
        lblCompletedSubtitle.topAnchor.constraint(equalTo: lblCompletedHeader.bottomAnchor, constant: 4).isActive = true
        btnProceed.addAnchorToSuperview(leading:30, trailing: -30)
        btnProceed.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        btnProceed.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
        
        
//        labelUploadPic.text = item.title
//        labelUploadPic.adjustsFontSizeToFitWidth = true
//        labelUploadPic.addAnchorToSuperview(leading: 5, trailing: -5,  bottom: 0)
//
//        self.addSubview(imageView)
//        imageView.image = item.image
//        imageView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,heightMultiplier: 0.8,heightWidthRatio: 1)
//
//        imageView.bottomAnchor.constraint(equalTo: labelUploadPic.topAnchor,constant: -5).isActive = true
    }
    let lblCompletedHeader: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 24.0, style: .bold)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "Submitted Successfully!"
        
        return label
    }()
    let lblCompletedSubtitle: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .regular)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "Your images submission  have been successfully done."
        
        return label
    }()
    let imageViewSubmit: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "submitCompleted")
     
        return imageView
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .pumpkinOrange
        btnProceed.setTitle("Go home", for: .normal)
        btnProceed.layer.cornerRadius = 12
        btnProceed.clipsToBounds = true
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 16, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
  
}
