//
//  WebPageVC.swift
//  Townhall
//
//  Created by Raihan on 12/27/21.
//

import UIKit

class WebPageVC: UIViewController {
    lazy var mView: WebPageView = {
        let view = WebPageView()
        view.navView.backButtonCallBack = backAction
        
        return view
    }()
    var type: HelpInformationDetailsType?
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        if type == .privecy{
            getPrivecyContent()
        }else{
            getTermsContnt()
        }
        // Do any additional setup after loading the view.
    }
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension WebPageVC{
    func getPrivecyContent(){
        WebServiceHandler.shared.getPrivacy(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    if let data = response.content{
                        if let htmlText = data.contentDescription{
                            self.mView.navView.title = data.title
                            self.mView.viewDetails.attributedText = htmlText.convertToAttributedString()
                        }
                        
                        
                    }
                    
                }
            case .failure(let error):
                debugPrint(error)
            }
        }, shouldShowLoader: true)
    }
    func getTermsContnt(){
        WebServiceHandler.shared.getTerms(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    if let data = response.content{
                        self.mView.navView.title = data.title
                        if let htmlText = data.contentDescription {
                            self.mView.viewDetails.attributedText = htmlText.convertToAttributedString()
                        }
                    }
                    
                }
            case .failure(let error):
                debugPrint(error)
            }
        }, shouldShowLoader: true)
    }
}
