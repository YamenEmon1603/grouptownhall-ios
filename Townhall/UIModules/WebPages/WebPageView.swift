//
//  WebPageView.swift
//  Townhall
//
//  Created by Raihan on 12/27/21.
//

import Foundation
import UIKit
class WebPageView: EasyBaseView {
   
    var proceedCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.08)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
        
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var contentView: UIView = {
        let contentView = UIView()
        contentView.addSubview(viewDetails)
        viewDetails.addAnchorToSuperview(leading: 15, trailing: -15, top: 0, bottom: 0)
        return contentView
    }()
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "", withBackButton: true, rightItem: nil)
        //navView.lblTitle.font = .EasyFont(ofSize: 16, style: .bold)
        navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        return navView
    }()
    lazy var viewDetails: UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.font = .EasyFont(ofSize: 14.0, style: .regular)
        view.text = ""
        view.isEditable = false
        return view
    }()

 
}

