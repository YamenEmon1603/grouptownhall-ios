//
//  GameScannerViewController.swift
//  Townhall
//
//  Created by Raihan on 12/22/21.
//

import UIKit
import AVFoundation
import Alamofire
protocol ImageGameCaptureDelegate{
    func captureController (_ controller: GameScannerViewController, didCapture :  [UIImage] )
}

class GameScannerViewController : EasyBaseViewController {
    //Camera Capture requiered properties
    private let metadataQueue = DispatchQueue(label: "metadata.session.qrreader.queue")
    var videoDataOutput: AVCaptureVideoDataOutput!
    var metadataOutput = AVCaptureMetadataOutput()
    var videoDataOutputQueue: DispatchQueue!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var captureDevice : AVCaptureDevice!
    let session = AVCaptureSession()
    var cameraOutput : AVCapturePhotoOutput!
    var capturedImages : [UIImage] = []
    var captureDelegate: ImageGameCaptureDelegate?
    var singleCaptureCallBack: ((_ image: UIImage )->Void)?
    var currentImage:UIImage?
    var instruction: String = "Capture Straight"
    
    lazy var mView : CaptureGameView = {
        let v = CaptureGameView()
        v.backButtonCallBack = backAction
        return v
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        EasyUtils.shared.delay(0.2) {
            self.setupAVCapture()
        }
        
        
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        session.stopRunning()
    }
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension GameScannerViewController : AVCaptureMetadataOutputObjectsDelegate {
    
    public func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        session.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = previewLayer?.transformedMetadataObject(for: metadataObject) as? AVMetadataMachineReadableCodeObject, metadataObject.type == .qr else { return }
            guard let stringValue = readableObject.stringValue else { return }
            print("\(stringValue)")
            
            DispatchQueue.main.async {
                self.gameInfo(code: stringValue)
            }
        }
    }
}
extension GameScannerViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        session.stopRunning()
        
        
        if let metatdataObject = metadataObjects.first {
            guard let readableObject = metatdataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            print(stringValue)
        }
        
        dismiss(animated: true)
    }
}

extension GameScannerViewController : AVCapturePhotoCaptureDelegate {
    func setupAVCapture(){
        session.sessionPreset = AVCaptureSession.Preset.photo
        guard let device = AVCaptureDevice
                .default(AVCaptureDevice.DeviceType.builtInWideAngleCamera,
                         for: .video,
                         position: AVCaptureDevice.Position.back) else {
                    return
                }
        captureDevice = device
        startPhotoCaptureSession()
        
    }
    func startPhotoCaptureSession(){
        session.sessionPreset = AVCaptureSession.Preset.photo
        cameraOutput = AVCapturePhotoOutput()
        guard session.canAddOutput(metadataOutput) else {
            
            return
        }
        if let input = try? AVCaptureDeviceInput(device: captureDevice) {
            if (session.canAddInput(input)) {
                session.addInput(input)
                if (session.canAddOutput(cameraOutput)) {
                    session.addOutput(cameraOutput)
                    metadataOutput.setMetadataObjectsDelegate(self, queue: metadataQueue)
                    session.addOutput(metadataOutput)
                    metadataOutput.metadataObjectTypes = [.qr]
                    
                    previewLayer = AVCaptureVideoPreviewLayer(session: session)
                    previewLayer.frame = mView.contentView.bounds
                    previewLayer.videoGravity = .resizeAspectFill
                    mView.contentView.layer.addSublayer(previewLayer)
                    session.startRunning()
                    mView.contentView.bringSubviewToFront(mView.imageView)
                }
                
                
            } else {
                print("issue here : captureSesssion.canAddInput")
            }
        } else {
            print("some problem here")
        }
    }
    
}

class CaptureGameView: UIView {
    var torchOn  =  false
    var backButtonCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0,top : 0 , bottom: 0)
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 4, heightMultiplier: 0.11)
        
        contentView.contentMode = .scaleAspectFit
        
//        self.addSubview(preViewIv)
//        preViewIv.addAnchorToSuperview(leading: 0, trailing: 0)
//        preViewIv.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
//        preViewIv.bottomAnchor.constraint(equalTo: btnView.topAnchor).isActive = true
//        preViewIv.contentMode = .scaleAspectFit
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var   contentView : UIView = {
        let v = UIView()
        v.addSubview(imageView)
        imageView.addAnchorToSuperview( widthMutiplier: 0.7, centeredVertically: -30, centeredHorizontally: 0, heightWidthRatio: 1)
        return v
    }()
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "imgFocus")
        
        return imageView
    }()
    let preViewIv : UIImageView = {
        let preview = UIImageView()
        preview.contentMode = .scaleToFill
        preview.isHidden = true
        preview.backgroundColor = .black
        return preview
    }()
    lazy var navView : UIView = {
        let navView = UIView()
        navView.backgroundColor = .clear
        navView.addSubview(backButton)
        navView.addSubview(flashButton)
        navView.addSubview(lblNavTitle)
        backButton.addAnchorToSuperview(trailing: -10 ,heightMultiplier: 0.5, centeredVertically: 0,  heightWidthRatio: 1.2)
        flashButton.addAnchorToSuperview(heightMultiplier: 0.5, centeredVertically: 0,  heightWidthRatio: 1.0)
        flashButton.trailingAnchor.constraint(equalTo: backButton.leadingAnchor, constant: 0).isActive = true
        lblNavTitle.addAnchorToSuperview(leading: 30,top:0,bottom: 0)
        //lblNavTitle.trailingAnchor.constraint(equalTo: flashButton.leadingAnchor, constant: 0).isActive = true
        lblNavTitle.widthAnchor.constraint(equalTo: navView.widthAnchor, multiplier: 0.6).isActive = true
        return navView
    }()
    lazy var lblNavTitle : UILabel = {
        let lblsub = UILabel()
        lblsub.translatesAutoresizingMaskIntoConstraints = false
        lblsub.font = .EasyFont(ofSize: 16.0, style: .bold)
        lblsub.text = "Scan QR to play"
        lblsub.numberOfLines = 0
        lblsub.textColor = .white
     
        
        
        return lblsub
    }()
    private let backButton:UIButton = {
        let backButton = UIButton()
       
        backButton.setImage(UIImage(named: "crosswhite"), for: .normal)
        backButton.tintColor = .white
        backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        return backButton
    }()
    private let flashButton:UIButton = {
        let flashButton = UIButton()
       
        flashButton.setImage(UIImage(named: "path"), for: .normal)
        flashButton.tintColor = .white
        flashButton.addTarget(self, action: #selector(btnFlashAction(_:)), for: .touchUpInside)
        return flashButton
    }()
    
    
    @objc func btnBackAction(_ sender:UIButton){
        backButtonCallBack?()
    }
    @objc func btnFlashAction(_ sender:UIButton){
        torchOn = !torchOn
        setTorchActive(isOn: torchOn)
    }
    public func setTorchActive(isOn: Bool) {
            assert(Thread.isMainThread)
            
            guard let videoDevice = AVCaptureDevice.default(for: .video),
                videoDevice.hasTorch, videoDevice.isTorchAvailable else {
                    return
            }
            try? videoDevice.lockForConfiguration()
        videoDevice.torchMode = isOn ? .on : .off
            videoDevice.unlockForConfiguration()
        }
    
  
}
extension GameScannerViewController{
    func gameInfo(code: String){
        let model = PlayQrRequestModel(gameCode: code, checkEligible: "yes")
        
        WebServiceHandler.shared.gameQrInfo(with: model, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                 
                    if response.data?.isEligible == true{
                        let vc = GameAlertVC()
                        vc.gameInfo = response.data?.gameInfo
                        vc.titleName = response.message
                        vc.data = .SuccessGame
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        let vc = GameAlertVC()
                        vc.gameInfo = response.data?.gameInfo
                        vc.titleName = response.message
                        vc.data = .FailedGame
                       
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }else{
                    self.tabBarController?.showToast(message: response.message ?? "Error")
                    self.backAction()
                }
            case .failure(let error):
                
                self.showToast(message: error.localizedDescription)
                
            }
        }, shouldShowLoader: true)
        
    }
}
