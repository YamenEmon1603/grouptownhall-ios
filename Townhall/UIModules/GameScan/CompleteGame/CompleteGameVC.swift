//
//  CompleteGameVC.swift
//  Townhall
//
//  Created by Raihan on 12/23/21.
//

import UIKit

class CompleteGameVC: EasyBaseViewController {
    var code:String?
    var gameInfo : GameInfo?
    lazy var mView : CompleteGameView = {
        let mView = CompleteGameView()
        mView.backCallBack = backAction
        mView.completeButtonCallBack = completeGame
        return mView
    }()
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        if let data  = gameInfo{
            if let img = data.gameUrl{
                mView.imageView.kf.setImage(with: URL(string:img))
            }
            if let gameName = data.gameTitle{
                mView.titleLabel.attributedText =   "Great! you are eligible\nto play the \(gameName) game".multicolorText(special: "the \(gameName) game", mainColor: .black, specialColor: .blueViolet, font: UIFont.boldSystemFont(ofSize: 30))
            }
           
        }
        
    }
    
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popToRootViewController(animated: true)
    }
    func completeGame(){
        if let gameCode = gameInfo?.gameCode{
            gameInfo(code: gameCode, eligible: "no")
        }
        
    }
    


}
extension String{
    func multicolorText(special: String, mainColor:UIColor,specialColor:UIColor,font:UIFont) ->NSAttributedString{
        //let index = getCurrentLanguage()
       // let localisedStr = "by_clicking_proceed".easyLocalized()
        let attributedString = NSMutableAttributedString(string: self, attributes: [
            .font: font,
          .foregroundColor: mainColor
        ])
        let str = NSString(string: self)
        let range : NSRange =  str.range(of: special)
       
        attributedString.addAttribute(.foregroundColor, value: specialColor, range: range )
    
        return attributedString
    }
}
extension CompleteGameVC{
    func gameInfo(code: String,eligible:String){
        let model = PlayQrRequestModel(gameCode: code, checkEligible: eligible)
        
        WebServiceHandler.shared.gameQrInfo(with: model, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    let vc = GameAlertVC()
                    vc.gameInfo = self.gameInfo
                    vc.data = .SuccessGame
                    vc.titleName = response.message
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let error):
                
                self.showToast(message: error.localizedDescription)
                
            }
        }, shouldShowLoader: true)
        
    }
}
