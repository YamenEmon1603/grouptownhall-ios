//
//  GameAlertView.swift
//  Townhall
//
//  Created by Raihan on 12/22/21.
//

import Foundation
import UIKit

class CompleteGameView : EasyBaseView {
    var completeButtonCallBack : (()->Void)?
    var backCallBack : (()->Void)?
        override init(frame:CGRect) {
            super.init(frame: frame)
        }
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        convenience init(){
            self.init( frame: .zero)
            addSubview(imageView)
            addSubview(titleLabel)
            addSubview(subtitleLabel)
            addSubview(subTextLabel)
            addSubview(gameCompleteButton)
            addSubview(backButton)
            backButton.addAnchorToSuperview(leading: 0, top: 30, widthMutiplier: 0.15, heightWidthRatio: 0.7)
            
            subtitleLabel.addAnchorToSuperview(leading: 30, trailing: -30)
            titleLabel.addAnchorToSuperview(leading: 30, trailing: -30)
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 20 ).isActive = true
            imageView.addAnchorToSuperview(leading: 30 , widthMutiplier: 0.39, heightWidthRatio: 0.82)
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor,constant: 14).isActive = true
            
             subtitleLabel.bottomAnchor.constraint(equalTo: self.subTextLabel.topAnchor,constant: -8).isActive = true
            
            
            subTextLabel.addAnchorToSuperview(leading: 30, trailing: -30)
            subTextLabel.bottomAnchor.constraint(equalTo: gameCompleteButton.topAnchor,constant: -50).isActive = true
            
            
            gameCompleteButton.addAnchorToSuperview(leading: 30, trailing: -30)
            subtitleBottomConstarint = gameCompleteButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor,constant: -10)
            gameCompleteButton.heightAnchor.constraint(equalToConstant: .calculatedHeight(56)).isActive = true
            subtitleBottomConstarint?.isActive = true
        }
        var subtitleBottomConstarint : NSLayoutConstraint?
        override func layoutSubviews() {
            super.layoutSubviews()
            subtitleBottomConstarint?.constant = -(self.frame.height - (subtitleLabel.frame.height + titleLabel.frame.height + imageView.frame.height + subTextLabel.frame.height + gameCompleteButton.frame.height + 20))/2
        }
        let titleLabel: UILabel = {
            let label = UILabel()
            //let text = NSMutableAttributedString()
//            label.attributedText =  "Great! you are eligible\nto play the dart game".multicolorText(special: "the dart game", mainColor: .black, specialColor: .blueViolet, font: UIFont.boldSystemFont(ofSize: 30))
            
            
            //label.textColor = .black
            label.textAlignment = .center
            label.numberOfLines = 0
            return label
        }()
        
        let subtitleLabel: UILabel = {
            let label = UILabel()
            label.attributedText = "You are able to play only one game\n".multicolorText(special: "only one game", mainColor: .black, specialColor: .pumpkinOrange, font: .EasyFont(ofSize: 18, style: .medium))
            //label.textColor = .slateGrey
            label.textAlignment = .center
            label.numberOfLines = 0
            return label
        }()
    lazy var backButton: UIButton = {
        let  backButton = UIButton()
        backButton.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "backImage")
        backButton.setImage(image, for: .normal)
      
        backButton.tintColor = .black
        backButton.addTarget(self, action: #selector(backBtnAction(_:)), for: .touchUpInside)
        return backButton
    }()
    let subTextLabel: UILabel = {
        let label = UILabel()
        label.text = "If you play this game for once then you can’t\nplay any other game."
        label.textColor = .slateGrey
        label.textAlignment = .center
        label.font = .EasyFont(ofSize: 13, style: .regular)
        label.numberOfLines = 0
        return label
    }()
    let imageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "dartBoard")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    lazy var gameCompleteButton: UIButton = {
        let  btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Play Game", for: .normal)
        btn.backgroundColor = .blueViolet
        btn.tintColor = .white
        btn.layer.cornerRadius = 12
        btn.addTarget(self, action: #selector(completeBtnAction(_:)), for: .touchUpInside)
        return btn
    }()
    @objc func completeBtnAction(_ sender:UIButton){
        completeButtonCallBack?()
    }
    @objc func backBtnAction(_ sender:UIButton){
        backCallBack?()
    }
       
    }

