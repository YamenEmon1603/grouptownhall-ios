//
//  GameAlertView.swift
//  Townhall
//
//  Created by Raihan on 12/22/21.
//

import Foundation
import UIKit

class GameAlertView : EasyBaseView {
    var backCallBack : (()->Void)?
    var profileEditCallBack : (()->Void)?
    var completeButtonCallBack : (()->Void)?
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0, bottom: 0)
        containerView.addSubview(topView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.80)
    
        containerView.addSubview(bottomView)
        bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor,constant: 0).isActive = true
        bottomView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
      
    }
   
   
    
    lazy var topView: UIView = {
        let topView = UIView()
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        topView.backgroundColor = .jungleGreen
        
       // topView.addSubview(backButton)
        
        topView.addSubview(topImageView)

        //backButton.addAnchorToSuperview(leading: 0, top: 30, widthMutiplier: 0.15, heightWidthRatio: 0.7)
      
        
        topImageView.addAnchorToSuperview(leading: 0, trailing: 0, top: 100, bottom: 0)
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 8
       
        topImageView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 20, trailing: -20)
        stackView.topAnchor.constraint(equalTo: topImageView.centerYAnchor, constant: -20).isActive = true
        stackView.heightAnchor.constraint(equalTo: topImageView.heightAnchor, multiplier: 0.4).isActive = true
        
        
        stackView.addArrangedSubview(lblGameName)
        stackView.addArrangedSubview(lblHeaderOne)
        stackView.addArrangedSubview(lblHeaderTwo)
        
        //lblSubTitle
        
        return topView
    }()
    
    lazy var bottomView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.backgroundColor =  .blue
        formView.addSubview(gameCompleteButton)
        gameCompleteButton.addAnchorToSuperview(top: 0,  widthMutiplier: 0.4, centeredHorizontally: 0)
        gameCompleteButton.heightAnchor.constraint(equalToConstant: .calculatedHeight(50)).isActive = true
        EasyUtils.shared.delay(0.2) {
            self.gameCompleteButton.layer.cornerRadius = self.gameCompleteButton.frame.height/2
        }
        return formView
    }()
    
  
    
    //MARK: InputViews
    
    //MARK: COMPONENTS
    lazy var gameCompleteButton: UIButton = {
        let  btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Go Home", for: .normal)
        //btn.backgroundColor = .lightBurgundy
        btn.tintColor = .white
        btn.addTarget(self, action: #selector(completeBtnAction(_:)), for: .touchUpInside)
        return btn
    }()
    lazy var backButton: UIButton = {
        let  backButton = UIButton()
        backButton.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "backArrow")
        backButton.setImage(image, for: .normal)
      
        backButton.tintColor = .white
        backButton.addTarget(self, action: #selector(backBtnAction(_:)), for: .touchUpInside)
        return backButton
    }()
    @objc func backBtnAction(_ sender:UIButton){
        backCallBack?()
    }
    @objc func completeBtnAction(_ sender:UIButton){
        completeButtonCallBack?()
    }
    
    let topImageView : UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .clear
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "")
        return view
    }()
   
    lazy var lblGameName: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = .EasyFont(ofSize: 20, style: .medium)
        lbl.textAlignment = .center
        lbl.text = ""
        return lbl
    }()
    lazy var lblHeaderOne : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .lightYellowGreen
        lbl.font = .EasyFont(ofSize: 30, style: .bold)
        lbl.textAlignment = .center
        lbl.text = "Congratulations!"
        return lbl
    }()
    lazy var lblHeaderTwo : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = .EasyFont(ofSize: 24, style: .regular)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.text = ""
        return lbl
    }()
    lazy var lblSubTitle : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = .EasyFont(ofSize: 12, style: .regular)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.text = ""
        return lbl
    }()

    
   

}




