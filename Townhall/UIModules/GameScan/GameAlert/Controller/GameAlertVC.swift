//
//  GameAlertVC.swift
//  Townhall
//
//  Created by Raihan on 12/22/21.
//

import UIKit
import Kingfisher
enum GameStatus : String {
    case SuccessGame
    case FailedGame
    var description: String {
            return self.rawValue
        }
}

class GameAlertVC : EasyBaseViewController {
    lazy var mView : GameAlertView = {
        let mView = GameAlertView()
        mView.backCallBack = backAction
        mView.completeButtonCallBack = completeGame
        return mView
    }()
    var titleName : String?
    var gameInfo : GameInfo?
    var data: GameStatus?{
        didSet{
            if data == .SuccessGame{
                mView.topView.backgroundColor = .jungleGreen
                mView.bottomView.backgroundColor = .jungleGreen
                mView.gameCompleteButton.backgroundColor = .jungleGreenTwo
                mView.lblHeaderOne.textColor = .lightYellowGreen
                mView.lblHeaderOne.text = "Congratulations!"
                mView.gameCompleteButton.setTitle("Complete", for: .normal)
            }else{
                mView.topView.backgroundColor = .rouge
                mView.bottomView.backgroundColor = .rouge
                mView.gameCompleteButton.backgroundColor = .lightBurgundy
                mView.lblHeaderOne.textColor = .white
                mView.lblHeaderOne.text = titleName
                
            }
          
            mView.lblHeaderTwo.text = gameInfo?.gameInfoDescription
            mView.lblGameName.text = gameInfo?.gameTitle
            if let imgUrl = gameInfo?.imageUrl{
                mView.topImageView.kf.setImage(with: URL(string:imgUrl))
            }
           
        }
    }

    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //fetchUserInfo()
    }
    func completeGame(){
        
        if data == .SuccessGame{
            let alert = UIAlertController(title: "Confirmation", message: "Do you want to complete your game?", preferredStyle:.alert)
            

            let proceed = UIAlertAction(title: "Complete", style: .default) { (action) in
                self.backAction()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
             
            }
            alert.addAction(proceed)
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
            
        }else{
            self.backAction()
        }
       
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
  

}
//MARK: — API CALL


