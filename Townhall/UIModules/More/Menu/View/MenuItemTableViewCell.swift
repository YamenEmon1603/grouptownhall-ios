//
//  TableViewCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/13/21.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {
    static let identifier = "MenuItemTableViewCell"

    var item : MenuItem? {
        didSet{
            iconView.image = item?.icon
            titleLabel.text = item?.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    let separatorView1 = UIView()
    var titlePos:NSLayoutConstraint?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = .white
        separatorView1.backgroundColor = .paleGrey
        self.contentView.addSubview(separatorView1)
        separatorView1.addAnchorToSuperview( trailing: 0,top: 0)
        separatorView1.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.contentView.addSubview(iconView)
        iconView.addAnchorToSuperview(leading: 20,top: 10,bottom: -10, widthMutiplier: 0.075,  heightWidthRatio: 1)
        self.contentView.addSubview(titleLabel)
        titleLabel.leadingAnchor.constraint(equalTo:iconView.trailingAnchor , constant: 20).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo:iconView.centerYAnchor ).isActive = true
        
        let separatorView = UIView()
        separatorView.backgroundColor = .paleGrey
        self.contentView.addSubview(separatorView)
        separatorView.addAnchorToSuperview( trailing: 0,bottom: 0)
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.leadingAnchor.constraint(equalTo:iconView.trailingAnchor , constant: 0).isActive = true
        separatorView1.leadingAnchor.constraint(equalTo:iconView.trailingAnchor , constant: 0).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let iconView : UIImageView = {
        let iconView = UIImageView()
        iconView.contentMode = .scaleAspectFit
        iconView.image = .init(named: "")
        return iconView
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.numberOfLines = 1
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let languageSwitch : LanguageSwitch = {
        let view = LanguageSwitch()
        return view
    }()
    
}

class HnSMenuItemTableViewCell: UITableViewCell {
    static let identifier = "HnSMenuItemTableViewCell"
    var item : MenuItem? {
        didSet{
            iconView.image = item?.icon
            //titleLabel.text = item?.title
        }
    }
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    let separatorView1 = UIView()
    var titlePos:NSLayoutConstraint?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = .white
        
        separatorView1.backgroundColor = .paleGrey
        self.contentView.addSubview(separatorView1)
        separatorView1.addAnchorToSuperview( trailing: 0,top: 0)
        separatorView1.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        
        self.contentView.addSubview(iconView)
        iconView.addAnchorToSuperview(leading: 20, widthMutiplier: 0.075,centeredVertically: 0,  heightWidthRatio: 1)
        
        self.contentView.addSubview(profileStack)
        profileStack.addAnchorToSuperview(  top: 10, bottom: -10)
        profileStack.leadingAnchor.constraint(equalTo:iconView.trailingAnchor , constant: 20).isActive = true
        
        self.contentView.addSubview(btnCall)
        btnCall.addAnchorToSuperview(trailing: -20, widthMutiplier: 0.075,centeredVertically: 0,  heightWidthRatio: 1)
        profileStack.trailingAnchor.constraint(equalTo: btnCall.leadingAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.backgroundColor = .paleGrey
        self.contentView.addSubview(separatorView)
        separatorView.addAnchorToSuperview( trailing: 0,bottom: 0)
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.leadingAnchor.constraint(equalTo:iconView.trailingAnchor , constant: 0).isActive = true
        separatorView1.leadingAnchor.constraint(equalTo:iconView.trailingAnchor , constant: 0).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var profileStack: UIStackView = {
        
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 10
        stack.addArrangedSubview(titleLabel)
        stack.addArrangedSubview(subtitleLabel)
        
        return stack
    }()
    let iconView : UIImageView = {
        let iconView = UIImageView()
        iconView.contentMode = .scaleAspectFit
        iconView.image = .init(named: "")
        return iconView
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.numberOfLines = 1
        label.text = "Help & Support"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let languageSwitch : LanguageSwitch = {
        let view = LanguageSwitch()
        return view
    }()
    let subtitleLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .EasyFont(ofSize: 12.0, style: .regular)
        label.textColor = .slateGrey
        label.numberOfLines = 1
        label.text = "01673537844, 01773537880"
        
        return label
    }()
    let btnCall:UIButton = {
        let btnCall = UIButton()
        btnCall.setImage(UIImage(named: "moreCall"), for: .normal)
      
        return btnCall
    }()
    
    
     
}
