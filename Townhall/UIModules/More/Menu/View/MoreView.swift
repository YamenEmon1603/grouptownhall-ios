//
//  MoreView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit

class MoreView: EasyBaseView {
    var profileCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        backgroundColor = .white
        addSubview(profileHeader)
        profileHeader.addAnchorToSuperview(leading: 20, trailing: -20, top: 40,  heightMultiplier: 0.1)
        addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: -20)
        tableView.topAnchor.constraint(equalTo: profileHeader.bottomAnchor, constant: 10).isActive = true
    }
  
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var profileHeader: GradientView = {
        let view = GradientView()
        view.startColor = .white
        view.endColor = .white
        view.horizontalMode = true
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.addSubview(profileImageView)
        view.addSubview(profileButton)
        
        profileButton.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        profileImageView.addAnchorToSuperview(leading: 0, heightMultiplier: 0.75,  centeredVertically: 0,  heightWidthRatio: 1)
        
        view.addSubview(profileStack)
        profileStack.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor,constant: 15).isActive = true
        profileStack.topAnchor.constraint(equalTo: profileImageView.topAnchor, constant: -5).isActive = true
        profileStack.bottomAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 5).isActive = true
        profileStack.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        return view
    }()
    lazy var profileImageView : UIImageView = {
       let profileImageView = UIImageView()
        profileImageView.contentMode = .scaleAspectFit
        profileImageView.image = UIImage(named: "")
        profileImageView.layer.borderColor = UIColor.Easywhite.cgColor
        profileImageView.layer.borderWidth = 1.5
        return profileImageView
    }()
    lazy var profileStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.addArrangedSubview(userNameLabel)
        stack.addArrangedSubview(numberLabel)
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    let userNameLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .medium)
        label.textColor = .black
        label.numberOfLines = 1
        label.text = ""
        return label
    }()
    let numberLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .regular)
        label.textColor = .slateGrey
        label.numberOfLines = 1
        label.text = ""
        return label
    }()
    let profileButton : UIButton = {
        let btn  = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(profileDetailsAction(_:)), for: .touchUpInside)
        return btn
        
    }()
    @objc func profileDetailsAction(_ sender:UIButton){
        profileCallBack?()
    }
    lazy var tableView : UITableView  = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.register(MenuItemTableViewCell.self, forCellReuseIdentifier: MenuItemTableViewCell.identifier)
        tableView.register(HnSMenuItemTableViewCell.self, forCellReuseIdentifier: HnSMenuItemTableViewCell.identifier)
        tableView.separatorStyle = .none
        return tableView
    }()

}

enum MenuItem {
    case programVenue
    case schedule
    case imageUpload
    case ChangePassword
    case HelpNSupport
    case showQR
    case SignOut
    var title:String{
        switch self {
        case .programVenue:
            return "Program Venue"
        case .schedule:
            return "Program Scheduling"
        case .imageUpload :
            return "TownHall Image upload"
        case .ChangePassword:
            return "change_password".easyLocalized()
        case .showQR:
            return "Show QR"
      
        case .HelpNSupport:
            return "Help_and_support_small".easyLocalized()
        case .SignOut:
            return "sign_out".easyLocalized()
        }
    }
    var icon: UIImage?{
        switch self {
        case .programVenue:
            return .init(named: "moreVenue")
        case .schedule:
            return .init(named: "moreSchedule")
        case .imageUpload :
            return .init(named: "morePhoto")
        case .ChangePassword:
            return .init(named: "changePassword")
        case .showQR:
            return .init(named: "qrApply")
        case .HelpNSupport:
            return .init(named: "moreSupport")
        case .SignOut:
            return .init(named: "logout")
     
        }
    }
}
