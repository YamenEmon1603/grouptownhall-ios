//
//  MoreViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/12/21.
//

import UIKit

class MoreViewController: EasyBaseViewController {
    let number1 = "01673537844"
    let number2 = "01773537880"
    lazy var mView : MoreView = {
        let mView = MoreView()
       
        return mView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        SSLComLanguageHandler.sharedInstance.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(updateTriggered), name: NSNotification.Name(rawValue: "profileUpdate"), object: nil)
        fetchUserInfo()
    }
    var data: UserData?{
        didSet{
            mView.userNameLabel.text = data?.name
            mView.numberLabel.text = data?.mobile
            mView.profileImageView.kf.setImage(with: URL(string: data?.profilePictureUrl ?? ""))
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        mView.profileImageView.layer.cornerRadius = min(mView.profileImageView.frame.width, mView.profileImageView.frame.height)/2
        mView.profileImageView.clipsToBounds = true
        mView.profileCallBack = profileGoAction
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        mView.tableView.delegate = self
        mView.tableView.dataSource = self
        mView.tableView.reloadData()
    }
    @objc func updateTriggered(){
        fetchUserInfo()
    }
    func profileGoAction(){
       // let vc = ProfileInfoViewController()
        //self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var menuItems : [(title:String,items:[MenuItem])] = [(title:"",items:[.programVenue,.schedule,.imageUpload,.showQR,.ChangePassword,.HelpNSupport,.SignOut])]
    let bottomAlert = HorizontalButtonAlertView(header: "you_can_logout_of_this_session".easyLocalized(), subHeader: "if_it_is_not_you".easyLocalized(), btn1Name: "cancel".easyLocalized(), btn2Name: "log_out".easyLocalized())
    func showBottomAlert() {
        self.view.addSubview(bottomAlert)
        bottomAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        bottomAlert.btnSecondCallBack = btnSecondAction
        bottomAlert.btnFirstCallBack = btnFirstAction
    }
    func btnSecondAction(){
        bottomAlert.removeFromSuperview()
        EasyUtils.shared.logout()
        
        
        
    }
    func btnFirstAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        bottomAlert.removeFromSuperview()
    }
    @objc func btnCallAction(_ sender:UIButton){
        
        makeCall()
    }
    func makeCall() {
        print ("------Phone Number-----")
  

        let phone = "tel://";
    
        let url:NSURL = NSURL(string:phone+number1)!
        let url2:NSURL = NSURL(string: phone+number2)!

        let alert = UIAlertController(title: "Help & Support", message: "Please choose which number you want to call", preferredStyle: UIAlertController.Style.actionSheet)
        

        let firstNumberAction = UIAlertAction(title: number1, style: .default) { (action) in
           
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }

        let secondNumberAction = UIAlertAction(title: number2, style: .default) { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url2 as URL)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
         
        }


        alert.addAction(firstNumberAction)
        alert.addAction(secondNumberAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
   }
    
    
    
   


}
extension MoreViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if menuItems[indexPath.section].items[indexPath.row] == .HelpNSupport{
            let cell = tableView.dequeueReusableCell(withIdentifier: HnSMenuItemTableViewCell.identifier, for: indexPath) as! HnSMenuItemTableViewCell
            cell.separatorView1.backgroundColor = .clear
            cell.item = menuItems[indexPath.section].items[indexPath.row]
            cell.btnCall.addTarget(self, action: #selector(btnCallAction(_:)), for: .touchUpInside)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuItemTableViewCell.identifier, for: indexPath) as! MenuItemTableViewCell
        cell.item = menuItems[indexPath.section].items[indexPath.row]
        if indexPath.section == 0 && indexPath.row == 0{
            cell.separatorView1.backgroundColor = .paleGrey
        }else{
            cell.separatorView1.backgroundColor = .clear
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 6, width:
                                                    tableView.bounds.size.width, height: 19))
        headerLabel.font = .EasyFont(ofSize: 14.0, style: .medium)
        headerLabel.textColor = .black
        headerLabel.text = self.menuItems[section].title.uppercased()
        
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch  menuItems[indexPath.section].items[indexPath.row] {
        case .programVenue:
            (self.tabBarController as? EasyTabBarViewController)?.customTabBar.switchTab(from: 4, to: 1)
        case .schedule:
            (self.tabBarController as? EasyTabBarViewController)?.customTabBar.switchTab(from: 4, to: 3)
        case .SignOut:
            (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
            showBottomAlert()
        case .ChangePassword:
            let vc = ChangePasswordViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        case .imageUpload:
            let vc = ImageUploadPhoneEntryViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        case .HelpNSupport:
            makeCall()
            break
        case .showQR:
            let vc = ShowQRVC()
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            break
        }
    }
}
extension MoreViewController: SSLComLanguageDelegate{
    func languageDidChange(to language: SDKLanguage) {
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
    }
    
    
}
//MARK: — API CALL

extension MoreViewController{
    func fetchUserInfo(){
        if  EasyDataStore.sharedInstance.user != nil{
            self.data = EasyDataStore.sharedInstance.user
            return
        }
        WebServiceHandler.shared.getProfileDetails(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    EasyDataStore.sharedInstance.user = response.data
                    self.data = response.data
                 
                    
                }else{
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let error):
                
                self.showToast(message: error.localizedDescription)
                
            }
        }, shouldShowLoader: true)
    }
}
