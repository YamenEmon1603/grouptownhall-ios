//
//  RequestReceivedVC.swift
//  Easy.com.bd
//
//  Created by Raihan on 16/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class RequestReceivedVC: EasyBaseViewController {
    var nextPage : Int?
    var requestReceivedData: [HistoryData] = []
    
    lazy var mView : RequestReceivedView = {
        let view = RequestReceivedView()
        view.tableView.delegate = self
        view.tableView.dataSource = self
        return view
    }()
    override func viewDidLoad() {
        view = mView
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        requestReceivedData = []
        RequestReceivedlistApi(page: 1)
        
    }
    let  noDataView : NoDataView = {
        let noDataView = NoDataView(image: UIImage(named: "noTransection")!, text: "no_data_available".easyLocalized(), subtext: "")
        
        return noDataView
    }()
    let bottomAlert = HorizontalButtonBottomUpView(header: "confirm_rejection".easyLocalized().capitalized, subHeader: "will_you_decline_the_recharge_request".easyLocalized(), btn1Name: "cancel".easyLocalized().uppercased(), btn2Name: "decline".easyLocalized().uppercased())
    
   
    func btnSecondAction(_ id : String){
        debugPrint("DO Rejection logic")
        RequestReceivedRejectAPI(id: id)
        bottomAlert.removeFromSuperview()
    }
    func btnFirstAction(){
        bottomAlert.removeFromSuperview()
    }
    
    func btnAcceptAction(_ id: Int){
        let vc  = RechargeReqPayVC()
        let rData  = self.requestReceivedData[id].ruid
        vc.ruid = rData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func btnRejectAction(_ id: Int){
        self.view.addSubview(bottomAlert)
        bottomAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        bottomAlert.btnSecondCallBack =  {
            if let rid  = self.requestReceivedData[id].id{
                self.btnSecondAction("\(rid)")
            }
           
        }
        bottomAlert.btnFirstCallBack = btnFirstAction
    }
    
    
}
extension RequestReceivedVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestReceivedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RequestReceivedCell.identifier, for: indexPath) as!  RequestReceivedCell
        let data =  requestReceivedData[indexPath.row].requestBody
        
        cell.lblName.text = data?.name
        cell.lblAmount.text = data?.amount
        cell.lblOperatorAndType.text = "\(data?.requestBodyOperator?.capitalized ?? "") | \(data?.connectionType?.capitalized ?? "")"
        
        if let date = requestReceivedData[indexPath.row].createdAt{
            let weekStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "EEE")
            let dateStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "d MMM yyyy")
          
            cell.lblDate.text = "\(weekStr.uppercased()), \(dateStr)"
        }
     
        cell.btnAccept.tag = indexPath.row
        cell.btnReject.tag = indexPath.row
        cell.acceptCallBack = btnAcceptAction
        cell.rejectCallback = btnRejectAction
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == mView.tableView,
              (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height, nextPage != nil
        else { return }
        self.RequestReceivedlistApi(page : nextPage ?? 0)
        
        
    }

}

//MARK:- API

extension RequestReceivedVC {
    func RequestReceivedlistApi(page:Int){
        
        WebServiceHandler.shared.getRechargeRequestList(page: page, completion: { (response) in
            switch response{
            case .success(let response):
                if response.code == 200{
                    if let _ = response.data?.paginator?.nextPageURL{
                        self.nextPage = response.data?.paginator?.currentPage ?? 0 + 1
                    }else{
                        self.nextPage = nil
                    }
                    if let data  = response.data?.data{
                        self.requestReceivedData.append(contentsOf: data)
                        self.mView.tableView.reloadData()
                        if data.isEmpty{
                            self.showToast(message: "no_data_available".easyLocalized())
                            self.mView.tableView.addSubview(self.noDataView)
                        }
                        if self.requestReceivedData.isEmpty{
                            self.mView.addSubview(self.noDataView)
                            self.noDataView.addAnchorToSuperview(leading: 20, trailing: -20, top: 20, bottom: -20)
                        }else{
                            self.noDataView.removeFromSuperview()
                        }
                    }
                   
                }else{
                    self.showToast(message: response.message ?? "Error occured")
                }
                break
                
            case .failure(let error) :
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
        
    }
    
    func RequestReceivedRejectAPI(id:String?){
        if let id = id{
            WebServiceHandler.shared.rechargeRequestReject(requestId: id, completion: { (response) in
                switch response{
                case .success(let response):
                    if response.code == 200{
                        self.showToast(message: response.message ?? "Success")
                        self.requestReceivedData = []
                        self.RequestReceivedlistApi(page: 1)
                    }else{
                        self.showToast(message: response.message ?? "Error occured")
                    }
                    break
                    
                case .failure(let error) :
                    debugPrint(error)
                    break
                }
            }, shouldShowLoader: true)
        }
    }
    
    
}

