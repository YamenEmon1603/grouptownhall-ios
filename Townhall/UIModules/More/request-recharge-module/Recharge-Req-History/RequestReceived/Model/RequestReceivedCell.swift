//
//  RequestReceivedCell.swift
//  Easy.com.bd
//
//  Created by Raihan on 17/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class RequestReceivedCell : UITableViewCell {
    var acceptCallBack : ((_ id: Int) -> Void)?
    var rejectCallback : ((_ id: Int) -> Void)?
    
    static let identifier = "reqSentCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        let container = UIView()
        contentView.addSubview(container)
        container.addAnchorToSuperview(leading: 0, trailing: 0, top: 1, bottom: -1)
        //        container.layer.borderWidth = 1
        //        container.layer.borderColor = UIColor.slateGrey.cgColor
        //        container.layer.cornerRadius = 10
        container.addSubview(dataView)
        let _  = container.addBorder(side: .bottom, color: UIColor.veryLightBlue, width: 1)
        
        
        dataView.addAnchorToSuperview(leading : 0 ,trailing: 0, top: 0, bottom: 0)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var dataView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        
        view.addSubview(lblTaka)
        view.addSubview(lblAmount)
        view.addSubview(sepView)
        view.addSubview(lblName)
        view.addSubview(statusView)
        view.addSubview(bottomStackView)
        
        lblAmount.addAnchorToSuperview(top: 15)
        
        lblAmount.bottomAnchor.constraint(equalTo: bottomStackView.topAnchor, constant: -4).isActive = true
        lblAmount.leadingAnchor.constraint(equalTo: lblTaka.trailingAnchor, constant: 0).isActive = true
        
        lblTaka.addAnchorToSuperview(leading:20,top: 15)
        lblTaka.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblTaka.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        
        sepView.topAnchor.constraint(equalTo: lblAmount.topAnchor, constant: 4).isActive = true
        sepView.leadingAnchor.constraint(equalTo: lblAmount.trailingAnchor, constant: 4).isActive = true
        sepView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        sepView.bottomAnchor.constraint(equalTo: lblAmount.bottomAnchor, constant: -4).isActive = true
        
        
        lblName.addAnchorToSuperview(top: 15,widthMutiplier: 0.5)
        
        
        
        
        lblName.leadingAnchor.constraint(equalTo: sepView.trailingAnchor, constant: 4).isActive = true
        lblName.bottomAnchor.constraint(equalTo: bottomStackView.topAnchor, constant: -4).isActive = true
        lblName.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        
        bottomStackView.trailingAnchor.constraint(equalTo: statusView.leadingAnchor, constant: -8).isActive = true
        
        bottomStackView.addAnchorToSuperview(leading: 20, bottom: -15)
        
        
        statusView.addAnchorToSuperview( trailing: -15, widthMutiplier: 0.3, centeredVertically: 0)
        statusView.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        
        
        return view
    }()
    
    lazy var sepView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .coolGrey
        return view
    }()
    
    lazy var statusView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .clear
        view.addSubview(buttonStackView)
        buttonStackView.addAnchorToSuperview(leading: 0, trailing: -0, top: 0, bottom: 0)
        return view
    }()
    
    
    lazy var lblTaka: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 11, style: .regular)
        view.text = "৳"
        view.textColor = .clearBlue
        view.textAlignment = .center
        view.numberOfLines = 1
        return view
    }()
    lazy var lblAmount: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 14, style: .bold)
        view.text = "nil"
        view.textColor = .clearBlue
        view.textAlignment = .left
        view.numberOfLines = 1
        view.sizeToFit()
        return view
    }()
    
    lazy var bottomStackView:UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        
        stackView.addArrangedSubview(lblOperatorAndType)
        lblOperatorAndType.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        stackView.addArrangedSubview(lblDate)
        lblDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
     
        
        return stackView
    }()
    lazy var lblDate: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 11, style: .regular)
        view.text = "nil"
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    
    lazy var lblOperatorAndType: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 11, style: .regular)
        view.text = "nil | nil"
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    
    lazy var lblName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 14, style: .medium)
        view.text = "nil"
        view.textColor = .black
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    
    
    lazy var buttonStackView:UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.distribution = .fillEqually
        stackView.addArrangedSubview(btnAccept)
        lblDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        stackView.addArrangedSubview(btnReject)
        lblDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        return stackView
    }()
    lazy var btnAccept : UIButton = {
        let btn = UIButton()
        btn.setTitle("accept".easyLocalized().uppercased(), for: .normal)
        EasyUtils.shared.delay(0.1) {
            btn.layer.cornerRadius = 4
            btn.clipsToBounds = true
        }
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .clearBlue
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = .EasyFont(ofSize: 11, style: .medium)
        btn.addTarget(self, action: #selector(btnAcceptAction(_:)), for: .touchUpInside)
        return btn
    }()
    lazy var btnReject : UIButton = {
        let btn = UIButton()
        btn.setTitle("reject".easyLocalized().uppercased(), for: .normal)
        EasyUtils.shared.delay(0.1) {
            btn.layer.cornerRadius = 4
            btn.clipsToBounds = true
        }
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .veryLightPink
        btn.setTitleColor(.tomato, for: .normal)
        btn.titleLabel?.font = .EasyFont(ofSize: 11, style: .medium)
        btn.addTarget(self, action: #selector(btnRejectAction(_:)), for: .touchUpInside)
        return btn
    }()
    @objc func btnRejectAction(_ sender:UIButton){
        rejectCallback?(sender.tag)
    }
    @objc func btnAcceptAction(_ sender:UIButton){
        acceptCallBack?(sender.tag)
    }
    
    
}
