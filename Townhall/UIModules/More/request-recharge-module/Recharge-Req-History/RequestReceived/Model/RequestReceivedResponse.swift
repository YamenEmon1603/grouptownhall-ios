////
////  RequestReceivedResponse.swift
////  Easy.com.bd
////
////  Created by Raihan on 23/8/21.
////  Copyright © 2021 SSL Wireless. All rights reserved.
////
//
//import Foundation
//
//// MARK: - RequesReceivedResponse
//class RequesReceivedResponse : EasyBaseResponse {
//    var data: DataRR?
// 
//
//    enum CodingKeys: String, CodingKey {
//        case data = "data"
//    }
//
//    required init(from decoder: Decoder) throws {
//        try super.init(from: decoder)
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        do{
//            self.data = try values.decode(DataRR.self, forKey: .data)
//        }catch{
//            self.data = nil
//        }
//    }
//}
//
//// MARK: - DataClass
//struct DataRR : Codable {
//    let paginator: Paginator?
//    let paginationLastPage, totalCount: Int?
//    let data: [RequesReceivedData]?
//
//    enum CodingKeys: String, CodingKey {
//        case paginator
//        case paginationLastPage = "pagination_last_page"
//        case totalCount = "total_count"
//        case data
//    }
//}
//
//// MARK: - Datum
//struct RequesReceivedData: Codable {
//    let ruid, transactionID, notificationMethod, createdAt: String?
//    let status,id: Int?
//    let requestBody: RequestBody?
//   
//
//    enum CodingKeys: String, CodingKey {
//        case ruid
//        case id
//        case transactionID = "transaction_id"
//        case notificationMethod = "notification_method"
//        case status
//        case requestBody = "request_body"
//        case createdAt = "created_at"
//    }
//}
//
//// MARK: - RequestBody
//struct RequestBody: Codable {
//    let amount, requestBodyOperator, connectionType, operatorID: String?
//    let requesterIdentifier, name, requestedIdentifier, msisdn: String?
//
//    enum CodingKeys: String, CodingKey {
//        case amount
//        case requestBodyOperator = "operator"
//        case connectionType = "connection_type"
//        case operatorID = "operator_id"
//        case requesterIdentifier = "requester_identifier"
//        case name
//        case requestedIdentifier = "requested_identifier"
//        case msisdn
//    }
//}
//
//// MARK: - Paginator
//struct Paginator: Codable {
//    let currentPage, totalPages: Int?
//    let previousPageURL, nextPageURL: String?
//    let recordPerPage: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case currentPage = "current_page"
//        case totalPages = "total_pages"
//        case previousPageURL = "previous_page_url"
//        case nextPageURL = "next_page_url"
//        case recordPerPage = "record_per_page"
//    }
//}
//
