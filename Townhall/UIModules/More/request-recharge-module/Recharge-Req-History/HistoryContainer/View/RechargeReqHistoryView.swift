//
//  RechargeReqHistoryView.swift
//  Easy.com.bd
//
//  Created by Raihan on 16/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit

class RechargeReqHistoryView: UIView {

    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        
        containerView.addSubview(topView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.15)

        containerView.addSubview(pageContainerView)
        pageContainerView.topAnchor.constraint(equalTo: topView.bottomAnchor,constant: 0).isActive = true
        pageContainerView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
      
    }

    //MARK: COMPONENTS
    lazy var pageContainerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .gray
        return v
    }()
    
    lazy var navView: LoginNavBar = {
        let view = LoginNavBar(title: "recharge_request".easyLocalized().uppercased())
        return view
    }()
    
    lazy var RequestReceivedView: PagingItemView = {
        let view = PagingItemView(title: "request_received".easyLocalized().uppercased())
        return view
    }()
    lazy var RequestSentView: PagingItemView = {
        let view = PagingItemView(title: "request_sent".easyLocalized().uppercased())
        return view
    }()
    lazy var RequestHistoryView: PagingItemView = {
        let view = PagingItemView(title: "received_history".easyLocalized().uppercased())
        return view
    }()
    lazy var topView: UIView = {
        let topView = UIView()
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        topView.backgroundColor = .white
        topView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0,trailing: 0,top: 0, heightMultiplier: 0.5)
      
        
        
        let pagingStack = UIStackView()
        pagingStack.translatesAutoresizingMaskIntoConstraints = false
        pagingStack.axis = .horizontal
        pagingStack.spacing = 0
        pagingStack.distribution = .fillEqually
        topView.addSubview(pagingStack)
        let _ = pagingStack.addBorder(side: .bottom, color: .lightPeriwinkle, width: 1)
        pagingStack.addAnchorToSuperview(leading: 1, trailing: -1,  bottom: 0, heightMultiplier: 0.5)
        
        pagingStack.addArrangedSubview(RequestReceivedView)
        RequestReceivedView.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.33).isActive = true
        RequestReceivedView.isActive = true
        
        pagingStack.addArrangedSubview(RequestSentView)
        RequestSentView.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.33).isActive = true
        
        pagingStack.addArrangedSubview(RequestHistoryView)
        RequestHistoryView.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.33).isActive = true
        
        
        topView.addShadow(location: .bottom, color: .black, opacity: 0.15, radius: 2.0)
        return topView
    }()
   
}


