//
//  RechargeHistContainerVC.swift
//  Easy.com.bd
//
//  Created by Raihan on 16/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit


class RechargeHistContainerVC : EasyBaseViewController {
   var isTabbarHidden = false
    lazy var mView : RechargeReqHistoryView = {
        let view = RechargeReqHistoryView()
       
        view.RequestReceivedView.activeButton.addTarget(self, action: #selector(activateTab(_:)), for: .touchUpInside)
        view.RequestSentView.activeButton.addTarget(self, action: #selector(activateTab(_:)), for: .touchUpInside)
        view.RequestHistoryView.activeButton.addTarget(self, action: #selector(activateTab(_:)), for: .touchUpInside)
        view.navView.backButtonCallBack = backAction
        return view
    }()
    var thePageVC: EasyPageViewController!
  
    lazy var reqReceivedvc: RequestReceivedVC = {
        let vc = RequestReceivedVC()
        //etaskCommentvc.pId = Int(parentsTaskID ?? "0")
        return vc
    }()
    
    lazy var reqSentvc: RequestSentVC = {
        let vc = RequestSentVC()
        //etaskCommentvc.pId = Int(parentsTaskID ?? "0")
        return vc
    }()
    lazy var reqHistoryvc: ReceivedHistoryVC = {
        let vc = ReceivedHistoryVC()
        //etaskCommentvc.pId = Int(parentsTaskID ?? "0")
        return vc
    }()
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(isTabbarHidden)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        
        
        thePageVC = EasyPageViewController()
        thePageVC.pageDelegate = self
//        let ETaskInfoVc = etaskInfovc
//
//        let ETaskCommentVC = etaskCommentvc
        thePageVC.pages = [reqReceivedvc,reqSentvc,reqHistoryvc]
        addChild(thePageVC)
        
        // we need to re-size the page view controller's view to fit our container view
        thePageVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        // add the page VC's view to our container view
        mView.pageContainerView.addSubview(thePageVC.view)
        
        thePageVC.view.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        thePageVC.didMove(toParent: self)
        
        
     
    }
    override func viewWillAppear(_ animated: Bool) {
        EasyDataStore.sharedInstance.ruid = nil
        self.navigationController?.navigationBar.isHidden = true
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
      
    }
    override func viewWillDisappear(_ animated: Bool) {
       
    }
  
    
    @objc func activateTab(_ sender: UIButton){
        if sender.superview == mView.RequestReceivedView{
            thePageVC.setPage(index: 0)
        }else if sender.superview == mView.RequestSentView{
            thePageVC.setPage(index: 1)
        }else if sender.superview == mView.RequestHistoryView{
            thePageVC.setPage(index: 2)
        }
    }
  
    
}
extension RechargeHistContainerVC : EasyPageDelegate
{
    func pageDidChanged(to: Int) {
        switch to {
        case 0:
            mView.RequestReceivedView.isActive = true
            mView.RequestSentView.isActive = false
            mView.RequestHistoryView.isActive = false
        case 1:
            mView.RequestReceivedView.isActive = false
            mView.RequestSentView.isActive = true
            mView.RequestHistoryView.isActive = false
        case 2:
            mView.RequestReceivedView.isActive = false
            mView.RequestSentView.isActive = false
            mView.RequestHistoryView.isActive = true
        default:
            break
        }
    }
}


