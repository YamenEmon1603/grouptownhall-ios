//
//  ReqReceivedHistTableViewCell.swift
//  Easy.com.bd
//
//  Created by Raihan on 16/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class ReqHistTableViewCell: UITableViewCell {
    
    static let identifier = "reqReceivedCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        let container = UIView()
        contentView.addSubview(container)
        container.addAnchorToSuperview(leading: 0, trailing: 0, top: 1, bottom: -1)
        //        container.layer.borderWidth = 1
        //        container.layer.borderColor = UIColor.slateGrey.cgColor
        //        container.layer.cornerRadius = 10
        container.addSubview(dataView)
        let _  = container.addBorder(side: .bottom, color: UIColor.veryLightBlue, width: 1)
        
        
        dataView.addAnchorToSuperview(leading : 0 ,trailing: 0, top: 0, bottom: 0)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var dataView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        
        view.addSubview(lblTaka)
        view.addSubview(lblAmount)
        view.addSubview(sepView)
        view.addSubview(lblName)
        view.addSubview(statusView)
        view.addSubview(bottomStackView)
        
        lblAmount.addAnchorToSuperview(top: 15)
        
        lblAmount.bottomAnchor.constraint(equalTo: bottomStackView.topAnchor, constant: -4).isActive = true
        lblAmount.leadingAnchor.constraint(equalTo: lblTaka.trailingAnchor, constant: 0).isActive = true
        
        lblTaka.addAnchorToSuperview(leading:20,top: 15)
        lblTaka.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblTaka.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        
        sepView.topAnchor.constraint(equalTo: lblAmount.topAnchor, constant: 4).isActive = true
        sepView.leadingAnchor.constraint(equalTo: lblAmount.trailingAnchor, constant: 4).isActive = true
        sepView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        sepView.bottomAnchor.constraint(equalTo: lblAmount.bottomAnchor, constant: -4).isActive = true
     
        
        lblName.addAnchorToSuperview(top: 15,widthMutiplier: 0.5)
        
        
        
        
        lblName.leadingAnchor.constraint(equalTo: sepView.trailingAnchor, constant: 4).isActive = true
        lblName.bottomAnchor.constraint(equalTo: bottomStackView.topAnchor, constant: -4).isActive = true
        lblName.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        
        bottomStackView.trailingAnchor.constraint(equalTo: statusView.leadingAnchor, constant: -8).isActive = true
        
        bottomStackView.addAnchorToSuperview(leading: 20, bottom: -15)
        
        
        statusView.addAnchorToSuperview( trailing: -15, widthMutiplier: 0.2, centeredVertically: 0)
        statusView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        
        return view
    }()
    
    lazy var sepView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .coolGrey
        return view
    }()
    
    lazy var statusView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .trueGreen
        view.addSubview(lblStatusTitle)
        view.layer.cornerRadius = 2
        lblStatusTitle.addAnchorToSuperview(leading: 4, trailing: -4, top: 4, bottom: -4)
        return view
    }()
    
    
    lazy var lblTaka: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 11, style: .regular)
        view.text = "৳"
        view.textColor = .clearBlue
        view.textAlignment = .center
        view.numberOfLines = 1
        return view
    }()
    lazy var lblAmount: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 14, style: .bold)
        view.text = "nil"
        view.textColor = .clearBlue
        view.textAlignment = .left
        view.numberOfLines = 1
        view.sizeToFit()
        return view
    }()
    
    lazy var bottomStackView:UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        //stackView.distribution = .fillEqually
        stackView.addArrangedSubview(lblOperatorAndType)
        lblOperatorAndType.heightAnchor.constraint(equalToConstant: 20).isActive = true
        stackView.addArrangedSubview(lblDate)
        lblDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        return stackView
    }()
    lazy var lblDate: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 11, style: .regular)
        view.text = "nil"
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    lazy var lblOperatorAndType: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 11, style: .regular)
        view.text = "nil | nil"
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    
    
    lazy var lblName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 14, style: .medium)
        view.text = "nil"
        view.textColor = .black
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    
    lazy var lblStatusTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font =  .EasyFont(ofSize: 10, style: .medium)
        view.text = "nil".uppercased()
        view.textColor = .white
        view.textAlignment = .center
        view.numberOfLines = 0
        
        return view
    }()
    
    
    
    
    
}
