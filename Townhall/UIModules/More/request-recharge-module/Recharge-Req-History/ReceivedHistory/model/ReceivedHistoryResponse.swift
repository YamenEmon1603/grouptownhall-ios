//
//  ReceivedHistoryResponse.swift
//  Easy.com.bd
//
//  Created by Raihan on 23/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation

// MARK: - RequesReceivedResponse
class ReceivedHistoryResponse : EasyBaseResponse {
    var data: ReceivedHistoryData?
 

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(ReceivedHistoryData.self, forKey: .data)
        }catch{
            print(error)
            self.data = nil
        }
    }
}

// MARK: - DataClass
struct ReceivedHistoryData : Codable {
    let paginator: Paginator?
    let paginationLastPage, totalCount: Int?
    let data: [HistoryData]?

    enum CodingKeys: String, CodingKey {
        case paginator
        case paginationLastPage = "pagination_last_page"
        case totalCount = "total_count"
        case data
    }
    
}

// MARK: - Datum
class HistoryData: Codable {
    let id: Int?
    let ruid, transactionID, notificationMethod: String?
    let status: Int?
    let statusValue: String?
    let requestBody: historyDataBody?
    let createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id, ruid
        case transactionID = "transaction_id"
        case notificationMethod = "notification_method"
        case status
        case statusValue = "status_value"
        case requestBody = "request_body"
        case createdAt = "created_at"
    }
}

// MARK: - RequestBody
class historyDataBody: Codable {
    let name, requesterIdentifier: String?
    let amount: String?
    let rechargeAmount : String?
    let requestBodyOperator, operatorID, msisdn, connectionType: String?
    let requestedIdentifier: String?

    enum CodingKeys: String, CodingKey {
        case name
        case requesterIdentifier = "requester_identifier"
        case amount
        case rechargeAmount = "recharge_amount"
        case requestBodyOperator = "operator"
        case operatorID = "operator_id"
        case msisdn
        case connectionType = "connection_type"
        case requestedIdentifier = "requested_identifier"
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
                do{
                    self.amount = try values.decode(String.self, forKey: .amount)
                }catch{
                    do{
                        self.amount = try String(values.decode(Int.self, forKey: .amount))
                    }catch{
                        do{
                            self.amount = try String(values.decode(Double.self, forKey: .amount))
                        }catch{
                            self.amount = nil
                        }
                    }
                }
        let values2 = try decoder.container(keyedBy: CodingKeys.self)
                do{
                    self.rechargeAmount = try values.decode(String.self, forKey: .rechargeAmount)
                }catch{
                    do{
                        self.rechargeAmount = try String(values.decode(Int.self, forKey: .rechargeAmount))
                    }catch{
                        do{
                            self.rechargeAmount = try String(values.decode(Double.self, forKey: .rechargeAmount))
                        }catch{
                            self.rechargeAmount = nil
                        }
                    }
                }
        //----
        let values3 = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.operatorID = try values.decode(String.self, forKey: .operatorID)
        }catch{
            do{
                self.operatorID = try String(values.decode(Int.self, forKey: .operatorID))
            }catch{
                do{
                    self.operatorID = try String(values.decode(Double.self, forKey: .operatorID))
                }catch{
                    self.operatorID = nil
                }
            }
        }
        //--
        self.name = try values.decode(String.self, forKey: .name)
        self.requesterIdentifier = try values.decode(String.self, forKey: .requesterIdentifier)
        self.requestBodyOperator = try values.decode(String.self, forKey: .requestBodyOperator)
        //self.operatorID = try values.decode(String.self, forKey: .operatorID)
        self.msisdn = try values.decode(String.self, forKey: .msisdn)
        self.connectionType = try values.decode(String.self, forKey: .connectionType)
        self.requestedIdentifier = try values.decode(String.self, forKey: .requestedIdentifier)
    }
}
   

// MARK: - Paginator
struct Paginator: Codable {
    let currentPage, totalPages: Int?
    let previousPageURL, nextPageURL: String?
    let recordPerPage: Int?

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case totalPages = "total_pages"
        case previousPageURL = "previous_page_url"
        case nextPageURL = "next_page_url"
        case recordPerPage = "record_per_page"
    }
}

