//
//  ReceivedHistoryVC.swift
//  Easy.com.bd
//
//  Created by Raihan on 16/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class ReceivedHistoryVC: EasyBaseViewController {
    var nextPage : Int?
    var receivedHistoryList: [HistoryData] = []
    
    lazy var mView : ReceivedHistoryView = {
        let view = ReceivedHistoryView()
        view.tableView.delegate = self
        view.tableView.dataSource = self
        return view
    }()
    override func viewDidLoad() {
        view = mView
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        receivedHistoryList = []
        receivedHistoryApi(page: 1)
    }
    let  noDataView : NoDataView = {
        let noDataView = NoDataView(image: UIImage(named: "noTransection")!, text: "no_data_available".easyLocalized(), subtext: "")
        
        return noDataView
    }()
  

}
extension ReceivedHistoryVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receivedHistoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReqHistTableViewCell.identifier, for: indexPath) as!  ReqHistTableViewCell
        cell.lblOperatorAndType.isHidden = false
        if let data = receivedHistoryList[indexPath.row].requestBody{
            cell.lblName.text = data.name
           
            cell.lblOperatorAndType.text = "\(data.requestBodyOperator?.capitalized ?? "") | \(data.connectionType?.capitalized ?? "")"
        }
        if let date = receivedHistoryList[indexPath.row].createdAt{
            let weekStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "EEE")
            let dateStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "d MMM yyyy")
          
            cell.lblDate.text = "\(weekStr.uppercased()), \(dateStr)"
        }
        
        
        
        if receivedHistoryList[indexPath.row].status == 0{
            cell.statusView.backgroundColor = .tomato
            cell.lblAmount.text = receivedHistoryList[indexPath.row].requestBody?.amount
        }else if receivedHistoryList[indexPath.row].status == 1{
            cell.statusView.backgroundColor = .trueGreen
            if let intAmount = receivedHistoryList[indexPath.row].requestBody?.rechargeAmount{
                cell.lblAmount.text = "\(Int(Double(intAmount) ?? 0))"
            }
            
        }else{
            cell.statusView.backgroundColor = .marigold
            cell.lblAmount.text = receivedHistoryList[indexPath.row].requestBody?.amount
        }
        
        cell.lblStatusTitle.text = receivedHistoryList[indexPath.row].statusValue
    
        return cell
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == mView.tableView,
              (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height, nextPage != nil
        else { return }
        self.receivedHistoryApi(page : nextPage ?? 0)
        
        
    }
   
    
}
extension ReceivedHistoryVC {
    func receivedHistoryApi(page : Int){
        
        WebServiceHandler.shared.getReceivedHistory(page: page, completion: { (response) in
            switch response{
            case .success(let response):
                if response.code == 200{
                    if let _ = response.data?.paginator?.nextPageURL{
                        self.nextPage = response.data?.paginator?.currentPage ?? 0 + 1
                    }else{
                        self.nextPage = nil
                    }
                    if let data  = response.data?.data{
                        self.receivedHistoryList.append(contentsOf: data)
                        self.mView.tableView.reloadData()
                        if data.isEmpty{
                            self.showToast(message: "no_data_available".easyLocalized())
                        }
                        if self.receivedHistoryList.isEmpty{
                            self.mView.addSubview(self.noDataView)
                            self.noDataView.addAnchorToSuperview(leading: 20, trailing: -20, top: 20, bottom: -20)
                        }else{
                            self.noDataView.removeFromSuperview()
                        }
                    }
       
                }else{
                    self.showToast(message: response.message ?? "Error Occured")
                }
                break
                
            case .failure(let error) :
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
        
    }
}
