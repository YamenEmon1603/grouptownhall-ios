//
//  ReceivedHistoryView.swift
//  Easy.com.bd
//
//  Created by Raihan on 17/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit

class ReceivedHistoryView : UIView {
   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom:0)

        let contentView  = UIView()
        contentView.backgroundColor = .white
        containerView.addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0 , bottom: 0)
 
        
        //contentView.addSubview(noDataView)
       // noDataView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        contentView.addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
    }
   
  
    
     
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.register(ReqHistTableViewCell.self, forCellReuseIdentifier: ReqHistTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        return tableView
    }()
  
   
}

