////
////  RechargeRequestSentResponse.swift
////  Easy.com.bd
////
////  Created by Raihan on 23/8/21.
////  Copyright © 2021 SSL Wireless. All rights reserved.
////
//
//import Foundation
//
//
//// MARK: - RequesReceivedResponse
//class RechargeRequestSentResponse : EasyBaseResponse {
//    var data: RequestSentData?
// 
//
//    enum CodingKeys: String, CodingKey {
//        case data = "data"
//    }
//
//    required init(from decoder: Decoder) throws {
//        try super.init(from: decoder)
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        do{
//            self.data = try values.decode(RequestSentData.self, forKey: .data)
//        }catch{
//            self.data = nil
//        }
//    }
//}
//
//// MARK: - DataClass
//struct RequestSentData : Codable {
//    let paginator: Paginator?
//    let paginationLastPage, totalCount: Int?
//    let data: [RequesSentData]?
//
//    enum CodingKeys: String, CodingKey {
//        case paginator
//        case paginationLastPage = "pagination_last_page"
//        case totalCount = "total_count"
//        case data
//    }
//}
//
//// MARK: - Datum
//struct RequesSentData: Codable {
//    let id: Int?
//    let ruid, transactionID, notificationMethod: String?
//    let status: Int?
//    let statusValue: String?
//    let requestBody: SentBody?
//    let createdAt: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id, ruid
//        case transactionID = "transaction_id"
//        case notificationMethod = "notification_method"
//        case status
//        case statusValue = "status_value"
//        case requestBody = "request_body"
//        case createdAt = "created_at"
//    }
//}
//
//// MARK: - RequestBody
//struct SentBody: Codable {
//    let amount, requestBodyOperator, connectionType, operatorID: String?
//    let requesterIdentifier, name, requestedIdentifier, msisdn: String?
//
//    enum CodingKeys: String, CodingKey {
//        case amount
//        case requestBodyOperator = "operator"
//        case connectionType = "connection_type"
//        case operatorID = "operator_id"
//        case requesterIdentifier = "requester_identifier"
//        case name
//        case requestedIdentifier = "requested_identifier"
//        case msisdn
//    }
//}
