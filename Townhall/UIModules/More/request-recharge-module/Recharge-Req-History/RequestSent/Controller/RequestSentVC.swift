//
//  RequestSentVC.swift
//  Easy.com.bd
//
//  Created by Raihan on 16/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class RequestSentVC: EasyBaseViewController {
    var nextPage : Int?
    var sentListData : [HistoryData] = []
    
    lazy var mView : ReceivedHistoryView = {
        let view = ReceivedHistoryView()
        view.tableView.delegate = self
        view.tableView.dataSource = self
        return view
    }()
    override func viewDidLoad() {
        view = mView
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        sentListData = []
        RequestRequestSentApi(page: 1)
    }
    let  noDataView : NoDataView = {
        let noDataView = NoDataView(image: UIImage(named: "noTransection")!, text: "no_data_available".easyLocalized(), subtext: "")
        
        return noDataView
    }()
}
extension RequestSentVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sentListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReqHistTableViewCell.identifier, for: indexPath) as!  ReqHistTableViewCell
        cell.lblOperatorAndType.isHidden = true
        if let data = sentListData[indexPath.row].requestBody{
            cell.lblName.text = data.requestedIdentifier
            cell.lblAmount.text = data.amount
        }
        
        if let date = sentListData[indexPath.row].createdAt{
            let weekStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "EEE")
            let dateStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "d MMM yyyy")
          
            cell.lblDate.text = "\(weekStr.uppercased()), \(dateStr)"
        }
        
        if sentListData[indexPath.row].status == 0{
            cell.statusView.backgroundColor = .tomato
        }else if sentListData[indexPath.row].status == 1{
            cell.statusView.backgroundColor = .trueGreen
        }else{
            cell.statusView.backgroundColor = .marigold
        }
        
        cell.lblStatusTitle.text = sentListData[indexPath.row].statusValue
      
        return cell
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == mView.tableView,
              (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height, nextPage != nil
        else { return }
        self.RequestRequestSentApi(page : nextPage ?? 0)
        
        
    }
}

//MARK:- API

extension RequestSentVC {
    func RequestRequestSentApi(page : Int){
        
        WebServiceHandler.shared.getRechargeRequestSent(page: page, completion: { (response) in
            switch response{
            case .success(let response):
                if response.code == 200{
                    if response.data?.paginator?.nextPageURL?.isEmpty == false{
                        self.nextPage = page + 1
                    }else{
                        self.nextPage = nil
                    }
                    if let data  = response.data?.data{
                        self.sentListData.append(contentsOf: data)
                        self.mView.tableView.reloadData()
                        if data.isEmpty{
                            self.showToast(message: "no_data_available".easyLocalized())
                        }
                        if self.sentListData.isEmpty{
                            self.mView.addSubview(self.noDataView)
                            self.noDataView.addAnchorToSuperview(leading: 20, trailing: -20, top: 20, bottom: -20)
                        }else{
                            self.noDataView.removeFromSuperview()
                        }
                    }
                   

                    
                }else{
                    self.showToast(message: response.message ?? "Error Occured")
                }
                break
                
            case .failure(let error) :
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
        
    }
}
