////
////  RCNumberPresenterModule.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 3/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class RCNumberPresenterModule: RCNumberPresenterProtocol, RCNumberOutputInteractorProtocol{
//    func showAddedNumber(vc: UIViewController) {
//        let rsModule = RSRouterModule.createModule(nc: vc.navigationController!, operators: (self.interactor?._operators!)!, enity:nil)
//        vc.navigationController?.pushViewController(rsModule, animated: true)
////        if #available(iOS 13.0, *) {
////            vc.dismiss(animated: false, completion: {
////                self.delegate?.didConfirm(number:  (self.interactor?.rechargeEntity!)!)
////            })
////        } else {
////
////        }
//        
//    }
//    
//    var delegate: NumberConfimationProtocol?
//    
//    var view: RCNubmerViewProtocol?
//    
//    var router: RCNumberRouterProtocol?
//    
//    var interactor: RCNumberInputInteractorProtocol?
//    
//    func shouldShowAllPlans(vc: UIViewController) {
//        if let op = interactor?._operators?.first(where: {$0.operatorId == interactor?.rechargeEntity?.operatorId}){
//            router?.showAllPlans(vc: vc, with: interactor?.plans ?? [RechargePlan](), operatorList: interactor?._operators ?? [MBOperator](), selectedOperator: op, and: self)
//        }else{
//            router?.showAllPlans(vc: vc, with: interactor?.plans ?? [RechargePlan](), operatorList: interactor?._operators ?? [MBOperator](), and: self)
//        }
//       
//    }
//    
//    func shouldPopOperatorSelection(vc: UIViewController) {
//        router?.popOperatorSelection(vc: vc, _operators: interactor?._operators ?? [MBOperator]())
//    }
//    
//    func viewWillAppear() {
//        view?.entity = interactor?.rechargeEntity
//        prepareMostPopular()
//        
//    }
//    func prepareMostPopular(){
//        if var filtered = interactor?.plans{
//            if let op = interactor?._operators?.first(where: {$0.operatorId == interactor?.rechargeEntity?.operatorId}){
//                filtered = filtered.filter({$0.operatorShortName == op.operatorShortName})
//            }
//            if interactor?.rechargeEntity?.operatorId == 13{
//                filtered = filtered.filter({$0.operatorShortName?.lowercased() == "gp"})
//            }
//            
//            if interactor?.rechargeEntity?.type?.lowercased() == "prepaid" || interactor?.rechargeEntity?.type?.lowercased() == "skitto" {
//                filtered = filtered.filter({$0.isForPrepaid == 1})
//            }
//            if interactor?.rechargeEntity?.type?.lowercased() == "postpaid"{
//                filtered = filtered.filter({$0.isForPostpaid == 1})
//            }
//            if let amount = interactor?.rechargeEntity?.amount ,amount > 0{
//                filtered = filtered.filter({(item) in
//                    if let maxAmount =  item.amountMax{
//                        return maxAmount > amount
//                    }
//                    return false
//                    
//                })
//            }
//            var five = filtered.filter({$0.isPopular == 1})
//            if five.count < 5 {
//                let sorted = filtered.sorted(by: {$0.usageCnt! > $1.usageCnt!}).filter({$0.isPopular == 0})
//                five.append(contentsOf: sorted.prefix(5 - five.count))
//            }
//            view?.plans = five
//        }
//       
//            //data.enumerated().compactMap{ $0.offset < 5 ? $0.element : nil }
//        
//    }
//    
//    
//    
//    func pushToAnotherVC(vc: UIViewController) {
//       
//           let rq = RechargeRequestViewController()
//        rq.rechargeEntity = interactor?.rechargeEntity
//            vc.navigationController?.pushViewController(rq, animated: true)
//    }
//    
//    func pushToInputNumber(vc: UIViewController) {
//        //router?.pushToInputNumber(vc: vc)
////        vc.dismiss(animated: true, completion: {
////            self.delegate?.addNewNumber(entity: (self.interactor?.rechargeEntity)!)
////        })
//        vc.navigationController?.popViewController(animated: true)
//        self.delegate?.addNewNumber(entity: (self.interactor?.rechargeEntity)!)
//    }
//    
//    
//}
//
//extension RCNumberPresenterModule : OfferSelectionProtocol{
//    func didSelect(rechargeOffer: RechargePlan) {
//        
//        
//        interactor?.rechargeEntity?.plan = rechargeOffer
//        interactor?.rechargeEntity?.amount = rechargeOffer.amountMax
//        prepareMostPopular()
//        view?.resetQuickAmountFrom()
//        view?.enterAmountString = "\(rechargeOffer.amountMax ?? 0)"
//    }
//}
