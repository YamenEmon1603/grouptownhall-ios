////
////  ConfirmMobileNumberViewController.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 3/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import UIKit
//import Kingfisher
//
//class RCNumberViewController: UIViewController,OperatorSelectionProtocol, RCNubmerViewProtocol, UITextFieldDelegate {
//    
//    
//    
//    
//    @IBOutlet weak var availableOfferHeight: NSLayoutConstraint!
//    @IBOutlet weak var viewAvailOffer: UIView!
//    @IBOutlet weak var viewOperatorTypeContainer: UIView!
//    @IBOutlet weak var confirmTableView: UITableView!
//    @IBOutlet weak var llAvailOfferText: UILabel!
//    @IBOutlet weak var btnAvailSelect: UIButton!
//    
//    @IBOutlet weak var viewOfferAvail: UIView!
//    
//    @IBOutlet weak var viewNumberAdded: UIView!
//    @IBOutlet weak var llNumberAdded: UILabel!
//    
//    @IBOutlet weak var llOperator: UILabel!
//    @IBOutlet weak var llToolbarText: CustomLabel!
//    
//    @IBOutlet weak var viewTopContainerHeight: NSLayoutConstraint!
//    @IBOutlet weak var viewMiddleContainerHeight: NSLayoutConstraint!
//    
//    @IBOutlet weak var tfEnterAmount: UITextField!
//    @IBOutlet weak var viewBottomCOntainerHeight: NSLayoutConstraint!
//    
//    @IBOutlet weak var viewBackContainer: UIView!
//    @IBOutlet weak var qv1: UIView!
//    @IBOutlet weak var qv2: UIView!
//    @IBOutlet weak var qv3: UIView!
//    @IBOutlet weak var qv4: UIView!
//    
//    @IBOutlet weak var llQ1Text: UILabel!
//    @IBOutlet weak var llQ2Text: UILabel!
//    @IBOutlet weak var llQ3Text: UILabel!
//    @IBOutlet weak var llQ4Text: UILabel!
//    
//    @IBOutlet weak var viewEnterAmount: UIView!
//    @IBOutlet weak var llScheduleRecharge: CustomLabel!
//    @IBOutlet weak var btnScheduleRecharge: UIButton!
//    
//    @IBOutlet weak var viewToolbarLine: UIView!
//    @IBOutlet weak var llAddAnotherNumber: CustomLabel!
//    @IBOutlet weak var btnAddAnother: UIButton!
//    @IBOutlet weak var viewQuickAMountContainer: UIView!
//    
//    @IBOutlet weak var viewConfirmEnterNumber: UIView!
//    @IBOutlet weak var viewEnterMobileNumber: UIView!
//    @IBOutlet weak var tfEnterMobileNumber: UITextField!
//    @IBOutlet weak var viewToolbarpContainerHeight: NSLayoutConstraint!
//    @IBOutlet weak var viewBackAlert: UIView!
//    @IBOutlet weak var lblNumber: UILabel!
//    
//    @IBOutlet weak var lblMostPopularOffers: CustomLabel!
//    @IBOutlet weak var lblProceedToRecharge: CustomLabel!
//    @IBOutlet weak var lblDoYouWantToGoBack: UILabel!
//    @IBOutlet weak var lblThePhoneNumberAndAmount: UILabel!
//    @IBOutlet weak var btnYes: UIButton!
//    @IBOutlet weak var btnStay: UIButton!
//    @IBOutlet weak var btnViewAll: UIButton!
//    @IBOutlet weak var ivOperatorLogo: UIImageView!
//    @IBOutlet weak var operatorName: UILabel!
//    @IBOutlet weak var dtPicker: UIDatePicker!
//    @IBOutlet weak var btnRemoveTimer: UIButton!
//    @IBOutlet weak var lblScheduleTitle: UILabel!
//    @IBOutlet weak var scheduleTitleheight: NSLayoutConstraint!
//    @IBOutlet weak var lblSpeciallyForYou: UILabel!
//    @IBOutlet weak var lblChangeOperator: UILabel!
//    @IBOutlet weak var removeScheduleWidthConstraint: NSLayoutConstraint!
//    
//    var langIndex :Int?
//    var schRechargeDate = ""
//    var rawDate : Date?
//    var fromOfferSelection = false
//    
//    
//    
//    var entity: RechargeEntity?{
//        didSet{
//            
//            // llOperator.text = entity?.type?.uppercased()
//            lblNumber.text = EasySingleton.shared.getLocalizedDigits(entity?.phone)
//            if entity?.type?.lowercased() == "skitto" {
//                operatorName.text = "Grameenphone"
//            }else{
//                operatorName.text = presenter?.interactor?.getOperatorName(id: entity?.operatorId ?? 0)
//            }
//           
//            
//            let url = EasySingleton.shared.getOperatorLogo(by: entity?.operatorId ?? 0)
//            ivOperatorLogo.setImage(with: url)
//            if entity?.type == "skitto" || entity?.type == "Skitto"{
//                llOperator.text = "skitto"
//            }else{
//                llOperator.text = entity?.type?.uppercased()
//            }
//           
//            if entity?.amount == 0{
//                tfEnterAmount.text = ""
//            }else{
//                tfEnterAmount.text = "\(entity?.amount ?? 0)"
//            }
//            
//            if tfEnterAmount.text != "0"{
//                btnScheduleRecharge.isEnabled = true
//            }
//        }
//    }
//    
//    var _operators: [MBOperator]?
//    var presenter: RCNumberPresenterProtocol?
//    var plans: [RechargePlan]?{
//        didSet{
//           // filterValue(amount: entity?.amount ?? 0)
//            confirmTableView.reloadData()
//        }
//    }
//    
//    var enterAmountString: String?{
//        didSet{
//            let amount = Int(enterAmountString ?? "0") ?? 0
//            btnScheduleRecharge.isEnabled = true
//            btnAddAnother.isEnabled = true
//            llScheduleRecharge.textColor = .primaryBlue
//            llAddAnotherNumber.textColor = .primaryBlue
//            presenter?.interactor?.rechargeEntity?.amount = Int(enterAmountString ?? "0")
//            tfEnterAmount.text = enterAmountString
//            //presenter?.prepareMostPopular()
//            filterValue(amount: amount)
//        }
//    }
//    
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//       
//        if #available(iOS 13.4, *) {
//            dtPicker.preferredDatePickerStyle = .wheels
//        } else {
//            // Fallback on earlier versions
//        }
//     
//        let number = UserSettings.sharedInstance.selectedNumbers
//        if number.count != 0 {
//            viewNumberAdded.isHidden = false
//            debugPrint("viewDidload")
//            debugPrint(UserSettings.sharedInstance.selectedNumbers.count)
//            llNumberAdded.text =  "\(number.count) \(number_added[langIndex ?? 0])"
//        }else{
//            viewNumberAdded.isHidden = true
//        }
//        
//        roundQuickView()
//        initView()
//        //btnAddAnother.isEnabled = false
//        // btnScheduleRecharge.isEnabled = false
//        confirmTableView.delegate = self
//        confirmTableView.dataSource = self
//        confirmTableView.reloadData()
//        
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        availableOfferHeight.constant = 0
//        languageSwitch(index: EasyUtils.getLanguageIndex())
//        presenter?.viewWillAppear()
//        let number = UserSettings.sharedInstance.selectedNumbers
//        var shouldHideLbl = false
//        if BACK_FROM_RS == true{
//            BACK_FROM_RS = false
//            if let lastNumber = number.last{
//                presenter?.interactor?.rechargeEntity = lastNumber
//                entity = lastNumber
//                if number.count > 1{
//                    UserSettings.sharedInstance.selectedNumbers.removeLast()
//                }else{
//                    shouldHideLbl = true
//                }
//               
//                
//                filterValue(amount: entity?.amount ?? 0)
//            }
//        }
//        if UserSettings.sharedInstance.selectedNumbers.count > 0 {
//            
//            llNumberAdded.text =  "\(UserSettings.sharedInstance.selectedNumbers.count) \(number_added[langIndex ?? 0])"
//        }else{
//            shouldHideLbl = true
//            
//        }
//        viewNumberAdded.isHidden = shouldHideLbl
////        if BACK_FROM_RS == true{
////            BACK_FROM_RS = false
////            if UserSettings.sharedInstance.selectedNumbers.count == 1{
////                viewNumberAdded.isHidden = true
////            }
////        }
//        
//        if SCHEDULE_DATE != ""{
//            let dateSc = SCHEDULE_DATE
//            
//            schRechargeDate = dateSc
//            
//            var dpDateStr = ""
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd-MM-yyyy | hh:mm a"
//            
//            if dateFormatter.date(from: dateSc) != nil{
//                dpDateStr = EasySingleton.shared.date(toTimestamp: dateSc, dateFormate: Constants.DateFormat14, expectedDateFormate: Constants.DateFormat13)
//                debugPrint(dpDateStr)
//            }
//            removeScheduleWidthConstraint.constant = 30
//            llScheduleRecharge.text = dpDateStr
//            llScheduleRecharge.textAlignment = .left
//            scheduleTitleheight.constant = 12
//            dtPicker.isHidden = true
//            btnRemoveTimer.isHidden = false
//            
//        }else{
//            llScheduleRecharge.textAlignment = .center
//        }
//    }
//    
//    func languageSwitch(index: Int){
//        langIndex = index
//        //llNumberAdded.text = number_added[index]
//        llToolbarText.text = recharge_request[index].uppercased()
//        tfEnterAmount.placeholder = enter_amount[index]
//        if schRechargeDate == ""{
//            llScheduleRecharge.text = schedule_recharge[index]
//        }
//        llAddAnotherNumber.text = add_another_number[index]
//        lblMostPopularOffers.text = most_popular_offers[index]
//        
//        lblDoYouWantToGoBack.text = do_you_want_to_go_back[index]
//        lblThePhoneNumberAndAmount.text = Your_phone_number_and_amount[index]
//        lblProceedToRecharge.text = proceed[index]
//        btnYes.setTitle(yes[index], for: .normal)
//        btnStay.setTitle(stay[index], for: .normal)
//        lblSpeciallyForYou.text = specially_for_you[index]
//        btnAvailSelect.setTitle(apply[index], for: .normal)
//        lblChangeOperator.text = change_operator[index]
//        btnViewAll.setTitle(view_all_cap[index], for: .normal)
//        //btnViewAll.setTitle(view_all_cap[index], for: .normal)
//        llQ1Text.text = EasySingleton.shared.getLocalizedDigits("50")
//        llQ2Text.text = EasySingleton.shared.getLocalizedDigits("100")
//        llQ3Text.text = EasySingleton.shared.getLocalizedDigits("200")
//        llQ4Text.text = EasySingleton.shared.getLocalizedDigits("500")
//        lblScheduleTitle.text = schedule_on[index]
//        
//        
//        ////
//        if let attributedTitle = btnViewAll.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: view_all_cap[index])
//            btnViewAll.setAttributedTitle(mutableAttributedTitle, for: .normal)
//        }
//        
//        /////
//        
//    }
//    func filterValue(amount: Int){
//        if amount > 9{
//            let offerData = plans?.filter{$0.amountMax ?? 0 > amount}.sorted(by: { (item1, item2) -> Bool in
//                if let amount1 = item1.amountMax, let amount2  = item2.amountMax{
//                    return amount2 > amount1
//                }
//                return  false
//            })
//            
//            if let offerAmount = offerData?.first?.amountMax{
//                let amountForOffer = offerAmount - amount
//                let offer = offerData?.first?.offerTitle
//                let nextOffer = "Add \(amountForOffer)TK more to get \(offer ?? "")"
//                if fromOfferSelection == false{
//                viewAvailOffer.isHidden = false
//                availableOfferHeight.constant = 70
//                }else{
//                    fromOfferSelection = false
//                    viewAvailOffer.isHidden = true
//                    availableOfferHeight.constant = 0
//                }
//                llAvailOfferText.text = nextOffer
//                presenter?.prepareMostPopular()
//                
//            }else{
//                availableOfferHeight.constant = 0
//            }
//            
//            
//            
//            
//        }
//    }
//    
//    @IBAction func btnApplyOffer(_ sender: Any) {
//        
//        let entereBdt = Int(tfEnterAmount.text ?? "0") ?? 0
//        let testData = plans?.filter{$0.amountMax! > entereBdt}.sorted(by: { (item1, item2) -> Bool in
//            if let amount1 = item1.amountMax, let amount2  = item2.amountMax{
//                return amount2 > amount1
//            }
//            return  false
//        })
//        if let offerAmount = testData?.first?.amountMax{
//            _ = offerAmount - entereBdt
//            tfEnterAmount.text = "\(offerAmount)"
//            presenter?.interactor?.rechargeEntity?.amount = offerAmount
//            availableOfferHeight.constant = 0
//            resetQuickAMount()
//            
//        }
//        
//        
//    }
//    
//    @IBAction func btnNumberAdded(_ sender: Any) {
//        FROM_NUMBER_ADDED_BUTTON = true
//        presenter?.showAddedNumber(vc: self)
//    }
//    
//    
//    @IBAction func btnYesAction(_ sender: Any) {
//        SCHEDULE_DATE = ""
//        UserSettings.sharedInstance.selectedNumbers.removeAll()
//        self.navigationController?.popViewController(animated: true)
//        
//    }
//    
//    @IBAction func btnStayAction(_ sender: Any) {
//        UIView.transition(with: viewBackContainer!,
//                          duration: 0.5,
//                          options: [.showHideTransitionViews],
//                          animations: {
//                            
//                            self.viewBackContainer.isHidden = true
//                          },
//                          completion: nil)
//        UIView.transition(with: viewBackAlert!,
//                          duration: 0.3,
//                          options: [.transitionFlipFromTop],
//                          animations: {
//                            
//                            self.viewBackAlert.isHidden = true
//                          },
//                          completion: nil)
//    }
//    
//    @IBAction func btnNumberAddAction(_ sender: Any) {
//        
//        debugPrint(UserSettings.sharedInstance.selectedNumbers.count)
//        if UserSettings.sharedInstance.selectedNumbers.count+2 <= 3 {
//            if let amount = tfEnterAmount.text ,amount != "" {
//                if let connectionType = presenter?.interactor?.rechargeEntity?.type{
//                    if connectionType.lowercased() == "postpaid"{
//                        if let amountInt = Int(amount){
//                            if amountInt < 2501{
//                                
//                                presenter?.interactor?.rechargeEntity?.amount = Int(amount)
//                                presenter?.pushToInputNumber(vc: self)
//                                
//                            }else{
//                                self.showToast(message: "\(ten_to_two_thousand_five_for_postpaid[EasyUtils.getLanguageIndex()])")
//                            }
//                        }
//                    }else if connectionType.lowercased() == "prepaid" || connectionType.lowercased() == "skitto"{
//                        if let amountInt = Int(amount){
//                            if amountInt < 1001{
//                                presenter?.interactor?.rechargeEntity?.amount = Int(amount)
//                                presenter?.pushToInputNumber(vc: self)
//                            }else{
//                                self.showToast(message: "\(ten_to_thousand_for_prepaid[EasyUtils.getLanguageIndex()])")
//                            }
//                        }
//                    }
//                }
//                
//                
//                
//            }else{
//                showToast(message: Enter_amount_first[EasyUtils.getLanguageIndex()])
//            }
//            
//        }else{
//            showToast(message: maximum_three_number_allowed[EasyUtils.getLanguageIndex()])
//        }
//        
//    }
//    
//    
//    
//    @IBAction func btnChangeOperatorAction(_ sender: Any) {
//        presenter?.shouldPopOperatorSelection(vc: self)
//    }
//    
//    
//    @IBAction func btnScheduleRecharge(_ sender: UIButton) {
//        if SCHEDULE_DATE == ""{
//            debugPrint("hello")
//            dtPicker.isHidden = false
//            dtPicker.backgroundColor = .white
//            let currentDate = NSDate()  //get the current date
//            let calendar = Calendar.current
//            
//            //dtPicker.minimumDate = currentDate as Date  //set the current date/time as a minimum
//            let today = currentDate as Date
//            let dateDelay = calendar.date(byAdding: .minute, value: 5, to: today)
//            dtPicker.date = dateDelay! as Date
//            dtPicker.minimumDate = dateDelay
//            //dtPicker.minuteInterval = 15
//        }else{
//            lblScheduleTitle.text = schedule_on[EasyUtils.getLanguageIndex()]
//            SCHEDULE_DATE = ""
//            llScheduleRecharge.textAlignment = .center
//            llScheduleRecharge.text = schedule_recharge[langIndex!]
//            scheduleTitleheight.constant = 0
//            btnRemoveTimer.isHidden = true
//            //btnScheduleRecharge.isEnabled = true
//            dtPicker.isHidden = true
//        }
//        
//    }
//    
//    @IBAction func datePickerChnage(_ sender: UIDatePicker) {
//        let dateFormatter = DateFormatter()
//        
//        dateFormatter.dateStyle = DateFormatter.Style.short
//        dateFormatter.timeStyle = DateFormatter.Style.short
//        dateFormatter.dateFormat = "dd-MMM-yyyy | hh:mm a"
//        //EEE, MMM d | HH:mm
//        
//        let strDate = dateFormatter.string(from: dtPicker.date)
//        
//        var dpDateStr = ""
//        
//        
//        if dateFormatter.date(from: strDate) != nil{
//            let dtFormat = DateFormatter()
//            dtFormat.dateFormat = "dd-MMM-yyyy | hh:mm a"
//            
//            dpDateStr = dtFormat.string(from: dtPicker.date)
//            
//            
//        }
//        
//        rawDate = dtPicker.date
//        
//        //dateFormatter.dateStyle = DateFormatter.Style.medium
//        //dateFormatter.timeStyle = DateFormatter.Style.short
//        
//        
//        let strDateForLabel = dateFormatter.string(from: dtPicker.date)
//        //llScheduleRecharge.text = strDateForLabel
//        removeScheduleWidthConstraint.constant = 30
//        llScheduleRecharge.text = dpDateStr
//        llScheduleRecharge.textAlignment = .left
//        scheduleTitleheight.constant = 12
//        SCHEDULE_DATE = strDateForLabel
//        schRechargeDate = strDate
//        dtPicker.isHidden = true
//        btnRemoveTimer.isHidden = false
//        // btnScheduleRecharge.isEnabled = false
//    }
//    @IBAction func btnRemoveTime(_ sender: Any) {
//        SCHEDULE_DATE = ""
//        llScheduleRecharge.text = schedule_recharge[langIndex!]
//        
//        llScheduleRecharge.textAlignment = .center
//        removeScheduleWidthConstraint.constant = 0
//        scheduleTitleheight.constant = 0
//        btnRemoveTimer.isHidden = true
//        //btnScheduleRecharge.isEnabled = true
//        dtPicker.isHidden = true
//    }
//    
//    func roundQuickView(){
//        tfEnterAmount.delegate = self
//        qv1.layer.cornerRadius = qv1.frame.size.width / 2
//        qv2.layer.cornerRadius = qv2.frame.size.width / 2
//        qv3.layer.cornerRadius = qv3.frame.size.width / 2
//        qv4.layer.cornerRadius = qv4.frame.size.width / 2
//        
//        qv1.layer.shadowColor = UIColor.black.cgColor
//        qv1.layer.shadowOffset = .zero
//        qv1.layer.shadowOpacity = 0.2
//        qv1.layer.shadowRadius = 2.0
//        
//        qv2.layer.shadowColor = UIColor.black.cgColor
//        qv2.layer.shadowOffset = .zero
//        qv2.layer.shadowOpacity = 0.2
//        qv2.layer.shadowRadius = 2.0
//        
//        qv3.layer.shadowColor = UIColor.black.cgColor
//        qv3.layer.shadowOffset = .zero
//        qv3.layer.shadowOpacity = 0.2
//        qv3.layer.shadowRadius = 2.0
//        
//        qv4.layer.shadowColor = UIColor.black.cgColor
//        qv4.layer.shadowOffset = .zero
//        qv4.layer.shadowOpacity = 0.2
//        qv4.layer.shadowRadius = 2.0
//        
//        qv1.layer.borderWidth = 1
//        qv1.layer.borderColor = UIColor.clear.cgColor
//        qv2.layer.borderWidth = 1
//        qv2.layer.borderColor = UIColor.clear.cgColor
//        qv3.layer.borderWidth = 1
//        qv3.layer.borderColor = UIColor.clear.cgColor
//        qv4.layer.borderWidth = 1
//        qv4.layer.borderColor = UIColor.clear.cgColor
//        
//        llQ1Text.textColor = ColorStore.text_color_quick_amount
//        llQ2Text.textColor = ColorStore.text_color_quick_amount
//        llQ3Text.textColor = ColorStore.text_color_quick_amount
//        llQ4Text.textColor = ColorStore.text_color_quick_amount
//    }
//    func resetQuickAmountFrom(){
//        roundQuickView()
//    }
//    
//    func initView(){
//        tfEnterMobileNumber.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textDidBeginEditing)))
//        if #available(iOS 10.0, *) {
//            tfEnterAmount.keyboardType = .asciiCapableNumberPad
//        } else {
//            tfEnterAmount.keyboardType = .phonePad
//        }
//    }
//    
//    @objc func textDidBeginEditing(sender:UITextField) -> Void
//    {
//        presenter?.pushToInputNumber(vc: self)
//        // handle begin editing event
//    }
//    
//    @objc func backgroundTapped(_ sender: UITapGestureRecognizer)
//    {
//        self.view.endEditing(true)
//        viewEnterAmount.layer.borderColor = ColorStore.border_color_tf.cgColor
//        UIView.transition(with: viewQuickAMountContainer!,
//                          duration: 0.5,
//                          options: [.showHideTransitionViews],
//                          animations: {
//                            
//                            self.viewQuickAMountContainer!.isHidden = false
//                          },
//                          completion: nil)
//    }
//    
//    @IBAction func btnBackAction(_ sender: Any) {
//        
//        UIView.transition(with: viewBackContainer!,
//                          duration: 0.5,
//                          options: [.showHideTransitionViews],
//                          animations: {
//                            
//                            self.viewBackContainer.isHidden = false
//                          },
//                          completion: nil)
//        
//        UIView.transition(with: viewBackAlert!,
//                          duration: 0.5,
//                          options: [.transitionFlipFromBottom],
//                          animations: {
//                            
//                            self.viewBackAlert.isHidden = false
//                          },
//                          completion: nil)
//    }
//    
//    @IBAction func btnProceedAction(_ sender: Any) {
//        if let amount = tfEnterAmount.text ,amount != "" {
//            if let connectionType = presenter?.interactor?.rechargeEntity?.type{
//                if connectionType.lowercased() == "postpaid"{
//                    if let amountInt = Int(amount){
//                        if amountInt < 2501{
//                            
//                            presenter?.interactor?.rechargeEntity?.amount = Int(amount)
//                            presenter?.interactor?.rechargeEntity?.scRechargeMob = schRechargeDate
//                            presenter?.interactor?.rechargeEntity?.scRawDate = rawDate
//                            presenter?.interactor?.rechargeEntity?.type = entity?.type
//                            presenter?.pushToAnotherVC(vc: self)
//                            
//                            
//                        }else{
//                            self.showToast(message: "\(ten_to_two_thousand_five_for_postpaid[EasyUtils.getLanguageIndex()])")
//                        }
//                    }
//                }else if connectionType.lowercased() == "prepaid" || connectionType.lowercased() == "skitto"{
//                    if let amountInt = Int(amount){
//                        if amountInt < 1001{
//                            presenter?.interactor?.rechargeEntity?.amount = Int(amount)
//                            presenter?.interactor?.rechargeEntity?.scRechargeMob = schRechargeDate
//                            presenter?.interactor?.rechargeEntity?.scRawDate = rawDate
//                            presenter?.interactor?.rechargeEntity?.type = entity?.type
//                            presenter?.pushToAnotherVC(vc: self)
//                        }else{
//                            self.showToast(message: "\(ten_to_thousand_for_prepaid[EasyUtils.getLanguageIndex()])")
//                        }
//                    }
//                }
//            }
//            
//            
//            
//        }else{
//            showToast(message: Enter_amount_first[EasyUtils.getLanguageIndex()])
//        }
//        
//    }
//    
//    
//    @IBAction func btnQuickAction(_ sender: UIButton) {
//        presenter?.interactor?.rechargeEntity?.plan = nil
//        switch sender.tag {
//        case 300:
//            quickAmountAction(amount: "50", view: qv1, textLabel: llQ1Text)
//            filterValue(amount: 50)
//            break
//        case 301:
//            quickAmountAction(amount: "100", view: qv2, textLabel: llQ2Text)
//            filterValue(amount: 100)
//            break
//        case 302:
//            quickAmountAction(amount: "200", view: qv3, textLabel: llQ3Text)
//            filterValue(amount: 200)
//            break
//        case 303:
//            quickAmountAction(amount: "500", view: qv4, textLabel: llQ4Text)
//            filterValue(amount: 500)
//            break
//        default:
//            break
//        }
//    }
//    
//    func quickAmountAction(amount: String, view: UIView, textLabel: UILabel){
//        btnScheduleRecharge.isEnabled = true
//        btnAddAnother.isEnabled = true
//        llScheduleRecharge.textColor = .primaryBlue
//        llAddAnotherNumber.textColor = .primaryBlue
//        qv1.backgroundColor = .white
//        qv2.backgroundColor = .white
//        qv3.backgroundColor = .white
//        qv4.backgroundColor = .white
//        
//        llQ1Text.textColor = ColorStore.text_color_quick_amount
//        llQ2Text.textColor = ColorStore.text_color_quick_amount
//        llQ3Text.textColor = ColorStore.text_color_quick_amount
//        llQ4Text.textColor = ColorStore.text_color_quick_amount
//        
//        if amount !=  "" { enterAmountString = amount}
//        roundQuickView()
//        view.backgroundColor = .white
//        view.layer.borderWidth = 2
//        view.layer.borderColor = UIColor.primaryBlue.cgColor
//        textLabel.textColor = .primaryBlue
//        
//        //        view.backgroundColor = ColorStore.active_color
//        //        textLabel.textColor = .white
//        if(enterAmountString?.count ?? 0 > 0){
//            viewEnterAmount.layer.borderColor = UIColor.primaryBlue.cgColor
//        }
//        presenter?.prepareMostPopular()
//    }
//    
//    func resetQuickAMount(){
//        qv1.backgroundColor = .white
//        qv2.backgroundColor = .white
//        qv3.backgroundColor = .white
//        qv4.backgroundColor = .white
//        
//        qv1.layer.borderWidth = 1
//        qv1.layer.borderColor = UIColor.clear.cgColor
//        qv2.layer.borderWidth = 1
//        qv2.layer.borderColor = UIColor.clear.cgColor
//        qv3.layer.borderWidth = 1
//        qv3.layer.borderColor = UIColor.clear.cgColor
//        qv4.layer.borderWidth = 1
//        qv4.layer.borderColor = UIColor.clear.cgColor
//        
//        llQ1Text.textColor = ColorStore.text_color_quick_amount
//        llQ2Text.textColor = ColorStore.text_color_quick_amount
//        llQ3Text.textColor = ColorStore.text_color_quick_amount
//        llQ4Text.textColor = ColorStore.text_color_quick_amount
//    }
//    
//    @IBAction func btnViewAllAction(_ sender: UIButton) {
//        presenter?.shouldShowAllPlans(vc: self)
//    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if tfEnterAmount == textField{
//            presenter?.interactor?.rechargeEntity?.plan = nil
//        } 
//        viewEnterAmount.layer.borderColor = UIColor.primaryBlue.cgColor
//        
//    }
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        let maxLength = 11
//        let currentString: NSString = textField.text! as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//        debugPrint(newString.length,"count")
//        if(newString.length > 1){
//            //            UIView.transition(with: viewQuickAMountContainer!,
//            //                              duration: 0.5,
//            //                              options: [.transitionCrossDissolve],
//            //                              animations: {
//            //
//            //                                self.viewQuickAMountContainer!.isHidden = true
//            //                              },
//            //                              completion: nil)
//            self.llScheduleRecharge.textColor = .primaryBlue
//            self.llAddAnotherNumber.textColor = .primaryBlue
//            btnAddAnother.isEnabled = true
//            btnScheduleRecharge.isEnabled = true
//            viewAvailOffer.isHidden = false
//            let amnt = Int(newString as String) ?? 0
//            
//            resetQuickAMount()
//            //            if amnt > 10 {
//            //                enterAmountString = "\(amnt)"
//            //            }
//            presenter?.interactor?.rechargeEntity?.amount = Int(amnt )
//            presenter?.prepareMostPopular()
//            self.filterValue(amount: amnt)
//        }else{
//            UIView.transition(with: viewQuickAMountContainer!,
//                              duration: 0.5,
//                              options: [.showHideTransitionViews],
//                              animations: {
//                                
//                                self.viewQuickAMountContainer!.isHidden = false
//                              },
//                              completion: nil)
//            resetQuickAMount()
//            self.llScheduleRecharge.textColor = ColorStore.text_color_schedule
//            self.llAddAnotherNumber.textColor = ColorStore.text_color_schedule
//            btnAddAnother.isEnabled = false
//            btnScheduleRecharge.isEnabled = false
//            viewEnterAmount.layer.borderColor = ColorStore.border_color_tf.cgColor
//            viewAvailOffer.isHidden = true
//            self.availableOfferHeight.constant = 0
//        }
//        return newString.length <= maxLength
//        
//        
//    }
//    
//    
//}
//
//extension RCNumberViewController: UITableViewDelegate, UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return plans?.count ?? 0
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "mostPopularCell", for: indexPath) as! MostPopularTableViewCell
//        cell.item = plans?[indexPath.item]
//        
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let count = plans?.count,   indexPath.row < count  , let plan = plans?[indexPath.row]{
//            fromOfferSelection = true
//            resetQuickAMount()
//            enterAmountString = "\(plan.amountMax ?? 0)"
//            
//            presenter?.interactor?.rechargeEntity?.plan = plan
//        }
//
//    }
//    
//}
//extension RCNumberViewController : UIPopoverPresentationControllerDelegate{
//    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle
//    {
//        return .none
//    }
//}
//
//extension RCNumberViewController : operatorSelectionProtocol{
//    func didPick(item: MBOperator, type: String?){
//        entity?.operatorId = item.operatorId
//        entity?.type = type
//        presenter?.interactor?.rechargeEntity?.operatorId = item.operatorId
//        operatorName.text = item.operatorName
//        if entity?.type == "skitto" || entity?.type == "Skitto"{
//            llOperator.text = "skitto"
//        }else{
//            llOperator.text = entity?.type?.uppercased()
//        }
//     
//        let url = EasySingleton.shared.getOperatorLogo(by: item.operatorId ?? 0)
//        ivOperatorLogo.setImage(with: url)
//        presenter?.prepareMostPopular()
//        
//    }
//    
//    
//}
//extension RCNumberViewController: UIPickerViewDelegate, UIPickerViewDataSource{
//    
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 2
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return 60
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return String(format: "%02d", row)
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if component == 0{
//            let minute = row
//            debugPrint("minute: \(minute)")
//        }else{
//            let second = row
//            debugPrint("second: \(second)")
//        }
//    }
//}
//
