////
////  ConfirmMobileProtocolModule.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 3/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import Foundation
//import UIKit
//
// protocol RCNubmerViewProtocol: class {
//    var presenter: RCNumberPresenterProtocol?{get set}
//    var _operators : [MBOperator]?{get set}
//    var entity : RechargeEntity?{get set}
//    var plans :[RechargePlan]?{get set}
//    var enterAmountString :String? {get set}
//    func resetQuickAmountFrom()
//}
//
//protocol RCNumberPresenterProtocol: class {
//    var view: RCNubmerViewProtocol?{get set}
//    var router: RCNumberRouterProtocol?{get set}
//    var interactor: RCNumberInputInteractorProtocol?{get set}
//    var delegate : NumberConfimationProtocol?{get set}
//    func pushToAnotherVC(vc: UIViewController)
//    func showAddedNumber(vc: UIViewController)
//    func pushToInputNumber(vc: UIViewController)
//    func shouldPopOperatorSelection(vc : UIViewController)
//    func shouldShowAllPlans(vc :UIViewController)
//    func viewWillAppear()
//    func prepareMostPopular()
//}
//
//protocol RCNumberRouterProtocol: class {
//    var navigation: UINavigationController? {get set}
//    static func createModule(with _operators : [MBOperator], entity: RechargeEntity,plans:[RechargePlan],delegate: NumberConfimationProtocol, connection: String )-> RCNumberViewController
//  
//    func pushToSummery(vc: UIViewController, with enity : RechargeEntity , operators: [MBOperator])
//    func pushToInputNumber(vc: UIViewController)
//    func popOperatorSelection(vc : UIViewController , _operators : [MBOperator])
//    func showAllPlans(vc:UIViewController, with plans :[RechargePlan],operatorList: [MBOperator] , and selectionDelegate : OfferSelectionProtocol)
//    func showAllPlans(vc:UIViewController, with plans :[RechargePlan],operatorList: [MBOperator] ,selectedOperator:MBOperator, and selectionDelegate : OfferSelectionProtocol)
//}
//
//protocol RCNumberInputInteractorProtocol: class {
//    var presenter: RCNumberOutputInteractorProtocol?{get set}
//    var _operators:[MBOperator]?{get set}
//    var rechargeEntity : RechargeEntity?{get set}
//    var SchRecharge: String?{get set}
//    var plans : [RechargePlan]?{get set}
//    var connection:String?{get set}
//    func getOperatorName(id:Int)->String
//  
//}
//
//protocol RCNumberOutputInteractorProtocol: class {
//    
//}
//
