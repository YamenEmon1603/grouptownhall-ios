////
////  RCNumberRouterModule.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 3/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class RCNumberRouterModule: RCNumberRouterProtocol{
//   
//    var navigation: UINavigationController?
//    static func createModule(with _operators: [MBOperator], entity: RechargeEntity, plans: [RechargePlan], delegate: NumberConfimationProtocol, connection: String) -> RCNumberViewController {
//           let presenter: RCNumberPresenterProtocol & RCNumberOutputInteractorProtocol = RCNumberPresenterModule()
//           let router: RCNumberRouterProtocol = RCNumberRouterModule()
//           let interactor: RCNumberInputInteractorProtocol = RCNumberInteractorModule()
//        let storyboardName = "RCNumber"
//           let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
//        
//           let view: RCNubmerViewProtocol = storyboard.instantiateViewController(withIdentifier: "rcNumberVC") as! RCNumberViewController
//           
//           //connecting
//           
//           view.presenter = presenter
//           presenter.interactor = interactor
//           presenter.view = view
//           presenter.router = router
//           interactor.presenter = presenter
//           interactor._operators = _operators
//           interactor.rechargeEntity = entity
//           interactor.plans =  plans
//        interactor.connection = connection
//           presenter.delegate = delegate
//           return view as! RCNumberViewController
//    }
//       
//   
//    
//    func showAllPlans(vc: UIViewController, with plans: [RechargePlan], operatorList: [MBOperator], selectedOperator: MBOperator, and selectionDelegate: OfferSelectionProtocol) {
//        let seeAllRouterModule = SelectOfferRouterModule.createModule(with: plans, operatorList: operatorList, selectedOperator: selectedOperator, selectionDelegate: selectionDelegate)
//        seeAllRouterModule.modalPresentationStyle = .fullScreen
//        vc.present(seeAllRouterModule, animated: true, completion: nil)
//    }
//    func showAllPlans(vc: UIViewController, with plans: [RechargePlan], operatorList: [MBOperator], and selectionDelegate: OfferSelectionProtocol) {
//        let seeAllRouterModule = SelectOfferRouterModule.createModule(with: plans, operatorList: operatorList , selectionDelegate : selectionDelegate)
//        seeAllRouterModule.modalPresentationStyle = .fullScreen
//        vc.present(seeAllRouterModule, animated: true, completion: nil)
//    }
//    
//  
//    
//  
//    
//
//    
//    func popOperatorSelection(vc: UIViewController, _operators: [MBOperator]) {
//        let router = CMNumberRouterModule.createOperatorSelectModule(with: _operators)
//        router.modalPresentationStyle = .custom
//        
//        router.view.isOpaque = false
//        router.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//        router.popoverPresentationController?.delegate = vc as? UIPopoverPresentationControllerDelegate
//        router.popoverPresentationController?.sourceView = vc.view
//        router.popoverPresentationController?.sourceRect = CGRect(origin:CGPoint(x: 0, y: vc.view.frame.maxY), size:CGSize(width: vc.view.bounds.width, height: vc.view.bounds.width) )
//        router.pickerDelegate = vc as? operatorSelectionProtocol
//        vc.present(router, animated: true, completion: nil)
//    }
//    
//    
//    
//    
//    
//   
//    func pushToSummery(vc: UIViewController, with enity: RechargeEntity, operators: [MBOperator]) {
//        vc.dismiss(animated: true, completion: nil)
//    }
//    
//    func pushToInputNumber(vc: UIViewController) {
//        let inputNumberModule = InputNumberRouterModule.createModule()
//        //nc.modalPresentationStyle = .overFullScreen
//        inputNumberModule.modalPresentationStyle = .fullScreen
//        
//        vc.present(inputNumberModule, animated: true, completion: nil)
//    }
//    
//    
//}
