//
//  RechargePayResponse.swift
//  Easy.com.bd
//
//  Created by Raihan on 23/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
class RechargePayResponse : EasyBaseResponse {
    var data: RechargeRequestPayData?
 

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(RechargeRequestPayData.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}



// MARK: - Datum
class RechargeRequestPayData: Codable {
    let id: Int?
    let ruid, transactionID, notificationMethod: String?
    let status: Int?
    let statusValue: String?
    let requestBody: paymentBody?
    let createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id, ruid
        case transactionID = "transaction_id"
        case notificationMethod = "notification_method"
        case status
        case statusValue = "status_value"
        case requestBody = "request_body"
        case createdAt = "created_at"
    }
}

// MARK: - RequestBody
class paymentBody: Codable {
    let amount, requestBodyOperator, connectionType, operatorID: String?
    let requesterIdentifier, name, requestedIdentifier, msisdn: String?

    enum CodingKeys: String, CodingKey {
        case amount
        case requestBodyOperator = "operator"
        case connectionType = "connection_type"
        case operatorID = "operator_id"
        case requesterIdentifier = "requester_identifier"
        case name
        case requestedIdentifier = "requested_identifier"
        case msisdn
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
                do{
                    self.amount = try values.decode(String.self, forKey: .amount)
                }catch{
                    do{
                        self.amount = try String(values.decode(Int.self, forKey: .amount))
                    }catch{
                        do{
                            self.amount = try String(values.decode(Double.self, forKey: .amount))
                        }catch{
                            self.amount = nil
                        }
                    }
                }
        let values3 = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.operatorID = try values.decode(String.self, forKey: .operatorID)
        }catch{
            do{
                self.operatorID = try String(values.decode(Int.self, forKey: .operatorID))
            }catch{
                do{
                    self.operatorID = try String(values.decode(Double.self, forKey: .operatorID))
                }catch{
                    self.operatorID = nil
                }
            }
        }
        self.name = try values.decode(String.self, forKey: .name)
        self.requesterIdentifier = try values.decode(String.self, forKey: .requesterIdentifier)
        self.requestBodyOperator = try values.decode(String.self, forKey: .requestBodyOperator)
        //self.operatorID = try values.decode(String.self, forKey: .operatorID)
        self.msisdn = try values.decode(String.self, forKey: .msisdn)
        self.connectionType = try values.decode(String.self, forKey: .connectionType)
        self.requestedIdentifier = try values.decode(String.self, forKey: .requestedIdentifier)
    }
}
