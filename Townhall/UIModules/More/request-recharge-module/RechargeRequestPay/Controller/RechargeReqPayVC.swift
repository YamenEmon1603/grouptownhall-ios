//
//  RechargeReqPayVC.swift
//  Easy.com.bd
//
//  Created by Raihan on 17/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import SSLCommerzSDK

class RechargeReqPayVC: EasyBaseViewController {
    var payData: RechargeRequestPayData? = nil {
        didSet{
         
            self.mView.lblUserName.text = payData?.requestBody?.name
            self.mView.lblUserIdentifier.text = payData?.requestBody?.requesterIdentifier
            if let name  = payData?.requestBody?.name{
                    self.mView.lblProfileNameChar.text = "\(name.prefix(1))"
                }
                
            self.mView.phoneNumberView.lblValue.text = payData?.requestBody?.msisdn
            self.mView.operatorView.lblValue.text = payData?.requestBody?.requestBodyOperator?.capitalized
            self.mView.typeView.lblValue.text = payData?.requestBody?.connectionType?.capitalized
            self.mView.dateView.lblValue.text = payData?.createdAt
            if let date = payData?.createdAt{
                let weekStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "EEE")
                let dateStr  = EasyUtils.shared.date(toTimestamp: date, dateFormate: "yyyy-MM-dd HH:mm:ss", expectedDateFormate: "d MMM yyyy")
                self.mView.dateView.lblValue.text = "\(weekStr.uppercased()), \(dateStr)"
            }
            
            self.mView.viewAmount.text = payData?.requestBody?.amount
            
        }
    }
    var sdkData: SSLComerceData?
    var sslCommerz:SSLCommerz?
    var ruid : String?
    lazy var mView : RechargeReqPayView = {
        let view = RechargeReqPayView()
        view.navView.backButtonCallBack = backButtonAction
        view.proceedCallBack = proceedAction
        return view
    }()
    override func viewDidLoad() {
        view = mView
        super.viewDidLoad()
        EasyDataStore.sharedInstance.ruid = nil
        hideKeyboardWhenTappedAround()
        if let id = ruid{
            RequestRequestSentApi(ruid: id)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
    }
    func proceedAction() {
        
       dismissKeyboard()
        if let data = payData{
            guard let amount = Int(mView.viewAmount.text ?? "0"), amount >= 10 else {
                self.showToast(message: "minimum_recharge_amount".easyLocalized())
                return
            }
            
            if data.requestBody?.connectionType?.lowercased() == ConectionType.PostPaid.value.lowercased(){
                if amount > 2500{
                    self.showToast(message: "\("ten_to_two_thousand_five_for_postpaid".easyLocalized())")
                }else{
                    requestRecharge(reqModel: data, amount: "\(amount)")
                }
            }else if data.requestBody?.connectionType?.lowercased() == ConectionType.Prepaid.value.lowercased() || data.requestBody?.connectionType?.lowercased() == "skitto"{
                if amount > 1000{
                    self.showToast(message: "\("ten_to_thousand_for_prepaid".easyLocalized())")
                }else{
                    requestRecharge(reqModel: data, amount: "\(amount)")
                }
            }
        }
    }
    func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func SSLCommerzInit(with response: SSLComerceResponse) {
        if response.code == 200{
            if let ssldata = response.sslData, let storeid = ssldata.storeId , let storePass = ssldata.storePassword , let transId = ssldata.transactionId,  let ipnUrl = ssldata.ipnUrl{
                
                
                let integrationInfo =  IntegrationInformation(storeID: storeid, storePassword: storePass, totalAmount: ssldata.amount ?? 0.0, currency: "BDT", transactionId: transId, productCategory: ssldata.serviceTitle ?? "")
                integrationInfo.ipnURL = ipnUrl
                
                
                sslCommerz = SSLCommerz.init(integrationInformation: integrationInfo)
                
                sslCommerz?.delegate = self
                sslCommerz?.customerInformation = .init(customerName: EasyDataStore.sharedInstance.user?.name ?? "", customerEmail: EasyDataStore.sharedInstance.user?.name ?? "", customerAddressOne: EasyDataStore.sharedInstance.user?.address ?? "", customerCity: "dhk", customerPostCode: "1234", customerCountry: "BD", customerPhone: EasyDataStore.sharedInstance.user?.mobile ?? "")
                
                sslCommerz?.additionalInformation = .init()
               sslCommerz?.additionalInformation?.paramB = ssldata.valueB
               sslCommerz?.additionalInformation?.paramC = ssldata.valueC
                
                
                sslCommerz?.start(in: self, shouldRunInTestMode:  Constant.isDev)
            }
            else{
                //view?.show(message:  "Something went wrong,please try again later")
                self.showToastError(message: "Something went wrong,please try again later")
            }
            
            
        }else{
            // view?.show(message: response.message ?? "Error Occured")
            self.showToastError(message:  response.message ?? "Error Occured")
           
        }
        
    }
    
}

//MARK:- API

extension RechargeReqPayVC {
    func requestRecharge(reqModel : RechargeRequestPayData, amount:String) {
        ///ADD 'RUID' IN parameter \\ ******************
        WebServiceHandler.shared.rechargeRequestTopUp(reqModel: reqModel,amount: amount,ruid: self.ruid ?? "",isRechargeRequest : true, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    self.sdkData = response.sslData
                    self.SSLCommerzInit(with: response)
                }else{
                    self.showToast(message: response.message ?? "Error occured")
                }
                break
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
              
                break
            }
        }, shouldShowLoader: true)
    }
    
    
    func RequestRequestSentApi(ruid:String?){
        if let id = ruid{
            WebServiceHandler.shared.rechargeRequestDetails(ruid: id, completion: { (response) in
                switch response{
                case .success(let response):
                    if response.code == 200{
                        self.payData = response.data
                       
                    }else{
                        self.showToastError(message:  response.message ?? "Error Occured")
                        self.navigationController?.popViewController(animated: true)
                    }
                    break
                    
                case .failure(let error) :
                    debugPrint(error)
                    break
                }
            }, shouldShowLoader: true)
            
        }
    }
}
extension RechargeReqPayVC: SSLCommerzDelegate {
    func sdkClosed(reason: String) {
        self.showToast(message: reason)
    }
    func transactionCompleted(withTransactionData transactionData: SSLCommerzSDK.TransactionDetails?) {
        if(transactionData?.status?.lowercased() == "valid" || transactionData?.status?.lowercased() == "validated" ){
            debugPrint(transactionData?.tran_id ?? "")
            
            rechargeStatus(transaction: transactionData?.tran_id ?? "")
        }else{
            self.showTransactionFailed()
            //view?.shouldShowError()
        }
    }
    func rechargeStatus(transaction:String){
        WebServiceHandler.shared.transectionStatus(transactionId: transaction, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    
                    self.showTransactionSuccess(transactionId: transaction)
        
                }else{
                    self.showTransactionFailed()
                }
                
                break
            case .failure(let error):
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
    }
    
    func showTransactionSuccess(transactionId: String) {
        
        
        let message = "rr_alert_success_message".easyLocalized().replacingOccurrences(of: "{{amount}}", with: "\(Int(sdkData?.amount ?? 0.0))").replacingOccurrences(of: "{{account_no}}", with: "\(mView.phoneNumberView.lblValue.text ?? "")")



            let vc = RRAlertViewController()
            vc.type = .success
            vc.successViewAlert.goHomeCallBack = self.goHome
        vc.successViewAlert.lblSuccessMessage.text = "success".easyLocalized()
            vc.successViewAlert.lblSubMessage.text = message
        vc.successViewAlert.btnGoHome.setTitle("done".easyLocalized(), for: .normal)
            self.presentAsPopUp(vc: vc)

//        }else{
//            let vc = RRAlertViewController()
//            vc.type = .error
//            vc.successViewAlert.lblSuccessMessage.text = response.message
//            vc.successViewAlert.lblSubMessage.text = failed_recharge_sub[EasyUtils.getLanguageIndex()]
//            self.presentAsPopUp(vc: vc)
//        }
    }
    func goHome(){
        self.dismiss(animated: true) {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func showTransactionFailed() {
        
        let message = "rr_alert_failed_message".easyLocalized().replacingOccurrences(of: "{{amount}}", with: "\(sdkData?.amount ?? 0.0)").replacingOccurrences(of: "{{account_no}}", with: "\(mView.phoneNumberView.lblValue.text ?? "")")


        let vc = RRAlertViewController()
        vc.type = .error
        vc.successViewAlert.lblSuccessMessage.text = "failed".easyLocalized()
        vc.successViewAlert.goHomeCallBack = self.goHome
        vc.successViewAlert.lblSubMessage.text = message
        vc.successViewAlert.btnGoHome.setTitle("done".easyLocalized(), for: .normal)
        self.presentAsPopUp(vc: vc)
    }
    
}

//extension RechargeReqPayVC : CommonAlertActionProtocol {
//    func postiveAction(for type: AlertType) {
//        if type == .success{
//            self.navigationController?.popToRootViewController(animated: true)
//        }else{
//
//        }
//    }
//
//    func negativeAction(for type: AlertType) {
//
//        self.navigationController?.popToRootViewController(animated: true)
//
//    }
//}
