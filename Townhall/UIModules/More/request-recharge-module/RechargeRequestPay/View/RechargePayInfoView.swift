//
//  RechargePayInfoView.swift
//  Easy.com.bd
//
//  Created by Raihan on 17/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit

class RechargePayInfoView : UIView {
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    convenience init(title:String,value : String) {
        self.init(frame:.zero)
        backgroundColor = .white
     
        
        addSubview(stackView)
        addSubview(sepView)
        sepView.addAnchorToSuperview(leading: 0, trailing: 0 , bottom: 0)
        sepView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        stackView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        stackView.bottomAnchor.constraint(equalTo: sepView.topAnchor, constant: 0).isActive = true
        stackView.addArrangedSubview(lblHeader)
        stackView.addArrangedSubview(lblValue)
        
        lblHeader.text = title
        lblValue.text = value
        
    }
   
  
    private let stackView:UIStackView = {
      let stackView = UIStackView()
        stackView.axis = .horizontal
        
        stackView.spacing = 6
        return stackView
    }()
    lazy var lblHeader: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .slateGrey
        lbl.font = .EasyFont(ofSize: 14, style: .regular)
        lbl.textAlignment = .left
        lbl.text = ""
        return lbl
    }()
    lazy var lblValue: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font =  .EasyFont(ofSize: 14, style: .medium)
        lbl.textAlignment = .right
        lbl.text = ""
        return lbl
    }()
    lazy var sepView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        EasyUtils.shared.delay(0.1) {
            view.createDottedLine(width: 1, color: UIColor.lightPeriwinkle.cgColor)
        }
      
    
        return view
    }()

}



