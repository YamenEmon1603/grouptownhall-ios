//
//  RechargeReqPayView.swift
//  Easy.com.bd
//
//  Created by Raihan on 17/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation

import UIKit


class RechargeReqPayView : UIView {
    var backCallBack : (()->Void)?
    var proceedCallBack : (()->Void)?
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        //containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        containerView.addSubview(topView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.35)
    
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: topView.bottomAnchor,constant: 0).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        containerView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.08)
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
   
   
    
    lazy var topView: UIView = {
        let topView = UIView()
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        topView.backgroundColor = .white
       
        topView.addSubview(navView)
        topView.addSubview(profileRoundView)
        topView.addSubview(lblUserName)
        topView.addSubview(sepView)
        topView.addSubview(lblUserIdentifier)
        
        navView.addAnchorToSuperview(leading: 0,trailing: 0, top: 0,heightMultiplier: 0.2)
        
        profileRoundView.addAnchorToSuperview(heightMultiplier: 0.3, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
        lblUserName.addAnchorToSuperview(leading: 20, trailing: -20)
        lblUserName.topAnchor.constraint(equalTo: profileRoundView.bottomAnchor, constant: 13).isActive = true
        lblUserIdentifier.addAnchorToSuperview(leading: 20, trailing: -20)
        lblUserIdentifier.topAnchor.constraint(equalTo: lblUserName.bottomAnchor, constant: 4).isActive = true
        
        lblUserIdentifier.bottomAnchor.constraint(greaterThanOrEqualTo: sepView.bottomAnchor, constant: -20).isActive = true
        
        sepView.addAnchorToSuperview(leading: 20, trailing: -20,bottom: 0)
        sepView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        return topView
    }()
    lazy var navView: LoginNavBar = {
        let view = LoginNavBar(title: "request_details".easyLocalized().uppercased())
        let _ = view.addBorder(side: .bottom, color: UIColor.lightPeriwinkle, width: 1)
        return view
    }()
    lazy var sepView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        
        view.createDottedLine(width: 1, color: UIColor.lightPeriwinkle.cgColor)
    
        return view
    }()
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        
       
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0, bottom: 0)
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
       
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
       
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        holderView.addSubview(stackView)
        holderView.addSubview(viewAmount)
        
        
        stackView.addAnchorToSuperview(leading: 20, trailing: -20, top: 0)
        stackView.bottomAnchor.constraint(equalTo: viewAmount.topAnchor, constant: -20).isActive = true
        
       stackView.addArrangedSubview(phoneNumberView)
        phoneNumberView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        stackView.addArrangedSubview(operatorView)
        operatorView.heightAnchor.constraint(equalToConstant: 40).isActive = true
         
        stackView.addArrangedSubview(typeView)
        typeView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        stackView.addArrangedSubview(dateView)
        dateView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        

        viewAmount.addAnchorToSuperview(leading: 15, trailing: -15, bottom: 0)
        viewAmount.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        return formView
    }()
    

    //MARK: COMPONENTS
    
    lazy var profileRoundView : UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.backgroundColor = .clearBlue
        view.layer.borderColor = UIColor.white.cgColor
        DispatchQueue.main.async {
            view.layer.cornerRadius = view.frame.size.height/2
            view.clipsToBounds = true
        }
        view.addSubview(lblProfileNameChar)
        lblProfileNameChar.addAnchorToSuperview( centeredVertically: 0, centeredHorizontally: 0)
       
        return view
    }()
    lazy var lblProfileNameChar : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = .EasyFont(ofSize: 26, style: .medium)
        lbl.textAlignment = .center
        lbl.text = ""
        return lbl
    }()
   
    lazy var lblUserName: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = .EasyFont(ofSize: 18, style: .medium)
        lbl.textAlignment = .center
        lbl.text = "Nil"
        return lbl
    }()
    lazy var lblUserIdentifier: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .battleshipGrey
        lbl.font = .EasyFont(ofSize: 14, style: .regular)
        lbl.textAlignment = .center
        lbl.text = "Nil"
        return lbl
    }()

    
    lazy var phoneNumberView : RechargePayInfoView = {
        let view = RechargePayInfoView(title: "request_for".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    lazy var operatorView : RechargePayInfoView = {
        let view = RechargePayInfoView(title: "req_Operator".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    lazy var typeView : RechargePayInfoView = {
        let view = RechargePayInfoView(title: "type".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    lazy var dateView : RechargePayInfoView = {
        let view = RechargePayInfoView(title: "date".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
       
        return view
    }()
    lazy var viewAmount : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "recharge_amount".easyLocalized())
       
        textView.keyBoardType = .numberPad
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.paleGrey.cgColor
        textView.backgroundColor = .easyWhite
        //textView.borderChange = true
        textView.addShadow(location: .bottom, color: UIColor.clear, opacity: 0.2, radius: 5)
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
      
        return textView
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setTitle("proceed".easyLocalized(), for: .normal)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    

}

extension RechargeReqPayView : ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }

}
