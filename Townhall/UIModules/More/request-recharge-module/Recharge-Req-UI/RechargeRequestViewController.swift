//
//  RechargeRequestViewController.swift
//  Easy.com.bd
//
//  Created by Raihan on 17/7/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class RechargeRequestViewController: EasyBaseViewController {
   
    
    
    
    var rechargeEntity : SelectNumber?
    var keyboardObserver : KeyboardObserver!
    lazy var mView: RechargeRequestEntryView = {
        let view = RechargeRequestEntryView()
        view.proceedCallBack = RechargeApi
        view.navView.backButtonCallBack = backAction
        
        return view
    }()
    
   
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        keyboardObserver = KeyboardObserver(for: self)
        
        mView.lblSelectedNumber.text = rechargeEntity?.number //EasySingleton.shared.getLocalizedDigits(rechargeEntity?.phone)
        
        //let url = EasySingleton.shared.getOperatorLogo(by: rechargeEntity?.operatorId ?? 0)
        //mView.numberDetailsIcon.setImage(with: url)
        mView.numberDetailsIcon.image = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorShortName == rechargeEntity?.operatorObj?.operatorShortName})?.logoImage
        
        if rechargeEntity?.connetctionType?.lowercased() == "skitto"{
            mView.lblOpeartor.text = rechargeEntity?.connetctionType?.lowercased()
        }else{
            mView.lblOpeartor.text = rechargeEntity?.connetctionType?.uppercased()
        }
        
        if rechargeEntity?.amount == 0{
            mView.lblAmount.text = ""
        }else{
            mView.lblAmount.text = "\(rechargeEntity?.amount ?? 0)"
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
    }
    
    
    func backAction(){
        EasyDataStore.sharedInstance.seletedNumbersArray = []
        self.navigationController?.popToRootViewController(animated: true)
    }
    func RechargeApi(){
        dismissKeyboard()
        guard let name =  mView.viewEnterName.text, name.isEmpty == false else{
            self.showToast(message: "enter_name".easyLocalized())
            return
        }
        guard let senderMobile =  mView.viewEnterMobileNoOrEmail.text,senderMobile.isEmpty == false else{
            self.showToast(message: "enter_mobile_number_mail".easyLocalized())
            return
        }
        
        guard let receiverMobile =  mView.viewEnterMobileNoOrEmail2.text,receiverMobile.isEmpty == false else{
            self.showToast(message:  "enter_req_rec_mobile_number_mail".easyLocalized())
            return
        }
        
        if senderMobile.isPhoneNumber == true  {
            if senderMobile.isElevenDigit == false{
                showToastError(message: "invalid_phone_no".easyLocalized())
                return
            }
            
        }else if senderMobile.isEmail == false{
            showToastError(message: "invalid_email_address".easyLocalized())
            return
            
        }
        if receiverMobile.isPhoneNumber == true  {
            if receiverMobile.isElevenDigit == false{
                showToastError(message: "invalid_phone_no".easyLocalized())
                return
            }
            
        }else if receiverMobile.isEmail == false{
            showToastError(message: "invalid_email_address".easyLocalized())
            return
            
        }
        let operatorName = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorId == rechargeEntity?.operatorObj?.operatorId})?.operatorName?.lowercased() ?? ""
        let amount = String(rechargeEntity?.amount ?? 0)
        var entityType = rechargeEntity?.connetctionType?.lowercased()
        var opName = operatorName
        var opId = rechargeEntity?.operatorObj?.operatorId ?? 0
        
        
        if rechargeEntity?.connetctionType?.lowercased() == "skitto" && operatorName.lowercased() == "grameenphone"{
            entityType = "prepaid"
            opName = "grameenphone"
            opId = 13
        }
        
        //name:name, requesterIdentifier: senderMobile, amount: amount , msisdn: rechargeEntity?.number ?? "", operatorName: opName, operatorId: "\(opId)", connectionType: entityType ?? "", requestedIdentifier: receiverMobile
        
        let model = RechargeRequestModel(name: name, requesterIdentifier: senderMobile, amount: amount, msisdn: rechargeEntity?.number ?? "", operatorName: opName, operatorId: "\(opId)", connectionType: entityType ?? "", requestedIdentifier: receiverMobile)
        
        WebServiceHandler.shared.getRechargeReq(info: model, completion: { (response) in
            switch response{
            case .success(let response):
                if response.code == 200{

                    let vc = RRAlertViewController()
                    vc.type = .success
                    vc.successViewAlert.goHomeCallBack = self.goHome
                    vc.successViewAlert.lblSuccessMessage.text = response.message
                    vc.successViewAlert.lblSubMessage.text = "success_recharge_sub".easyLocalized()
                    self.presentAsPopUp(vc: vc)
                    
                }else{
                    let vc = RRAlertViewController()
                    vc.type = .error
                    vc.successViewAlert.lblSuccessMessage.text = response.message
                    vc.successViewAlert.lblSubMessage.text = "failed_recharge_sub".easyLocalized()
                    self.presentAsPopUp(vc: vc)
                }
                break
                
            case .failure(let error) :
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
        
    }
    
    func goHome(){
        self.dismiss(animated: true) {
            EasyDataStore.sharedInstance.seletedNumbersArray = []
            self.navigationController?.popToRootViewController(animated: true)
              
         
        }
      
    }
}
extension RechargeRequestViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        var contentInset:UIEdgeInsets = mView.scrollView.contentInset
        contentInset.bottom = height + 20
        mView.scrollView.contentInset = contentInset
    }
    
    func keybaordWillHide() {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        mView.scrollView.contentInset = contentInset
    }
    
    
}

