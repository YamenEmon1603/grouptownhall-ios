//
//  RechargeRequestEntryView.swift
//  Easy.com.bd
//
//  Created by Raihan on 17/7/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit


class RechargeRequestEntryView : EasyBaseView {
 
    var proceedCallBack : (()->Void)?
  
    
    lazy var navView: LoginNavBar = {
        let view = LoginNavBar(title: "request_recharge".easyLocalized().uppercased(),leadingBtn: 0)
            let _ = view.addBorder(side: .bottom, color: UIColor.lightPeriwinkle, width: 1)
            return view
    }()
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier:  0.07)
        
        containerView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.1)
        
        containerView.addSubview(formView)
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 5).isActive = true
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
   
    //MARK: COMPONENTS
   
    
 
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        
       
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 8, bottom: 0)
       
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
       
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 15
        
//        holderView.addSubview(lblRequestRechargeHeader)
//        holderView.addSubview(lblEnterFollowingDetails)
        holderView.addSubview(stackView)
        
//        lblRequestRechargeHeader.addAnchorToSuperview(leading: 20, trailing: -20, top: 0)
//        lblEnterFollowingDetails.addAnchorToSuperview(leading: 20, trailing: -20)
        
//        lblEnterFollowingDetails.topAnchor.constraint(equalTo: lblRequestRechargeHeader.bottomAnchor, constant: 8).isActive = true
//        lblEnterFollowingDetails.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -20).isActive = true
        
        
        stackView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0, bottom: 0)
        
        stackView.addArrangedSubview(numberDetailsView)
        numberDetailsView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        stackView.addArrangedSubview(containerRequestFrom)
        lblRequestFromTitle.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        stackView.addArrangedSubview(containerEnterName)
        containerEnterName.heightAnchor.constraint(equalToConstant: 56).isActive = true
    
        
        stackView.addArrangedSubview(containerMobileNoOrEmail)
        containerMobileNoOrEmail.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        
        
      
        
        stackView.addArrangedSubview(containerRequestTo)
        containerRequestTo.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        stackView.addArrangedSubview(containerMobileNoOrEmail2)
        containerMobileNoOrEmail2.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        
        
        return formView
    }()
    
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setTitle("proceed".easyLocalized(), for: .normal)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)//EasyUtils.FontUtils.EasyFont(ofSize: 13, style: .bold)
        
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    //MARK: InputViews
    
    lazy var viewEnterName : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_name".easyLocalized())
        textView.shadow = false
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        textView.text =  EasyDataStore.sharedInstance.user?.name ?? ""
        textView.backgroundColor = .easyWhite
        
        return textView
    }()
    lazy var containerEnterName : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewEnterName)
        viewEnterName.addAnchorToSuperview(leading: 15, trailing: -15, top: 0, bottom: 0)
       
        return view
    }()

    lazy var viewEnterMobileNoOrEmail : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_mobile_number_mail".easyLocalized())
        textView.shadow = false
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        textView.text =  EasyDataStore.sharedInstance.user?.mobile ?? EasyDataStore.sharedInstance.user?.email ?? ""
        textView.backgroundColor = .easyWhite
        return textView
    }()
    lazy var containerMobileNoOrEmail : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewEnterMobileNoOrEmail)
        viewEnterMobileNoOrEmail.addAnchorToSuperview(leading: 15, trailing: -15, top: 0, bottom: 0)
       
        return view
    }()
    
    lazy var viewEnterMobileNoOrEmail2 : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_mobile_number_mail".easyLocalized())
        textView.shadow = false
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        textView.backgroundColor = .easyWhite
        return textView
    }()
    
    lazy var containerMobileNoOrEmail2 : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewEnterMobileNoOrEmail2)
        viewEnterMobileNoOrEmail2.addAnchorToSuperview(leading: 15, trailing: -15, top: 0, bottom: 0)
       
        return view
    }()
   
    
    
    lazy var lblRequestRechargeHeader: UILabel = {
        let lblTitle = UILabel()
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font =  .EasyFont(ofSize: 18, style: .bold)
        lblTitle.textColor = .black
        lblTitle.text = "req_recharge".easyLocalized()
        lblTitle.textAlignment = .left
        lblTitle.sizeToFit()
        return lblTitle
    }()
    lazy var lblEnterFollowingDetails : UILabel = {
        let lblTitle = UILabel()
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font =  .EasyFont(ofSize: 16, style: .medium)
        lblTitle.textColor = .slateGrey
        lblTitle.text = "enter_following_details".easyLocalized()
        lblTitle.textAlignment = .left
       
        return lblTitle
    }()
    
    
    lazy var lblRequestFromTitle: UILabel = {
        let lblTitle = UILabel()
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font =  .EasyFont(ofSize: 10, style: .medium)
        lblTitle.textColor = .slateGrey
        lblTitle.backgroundColor = .easyWhite
        lblTitle.text = "request_from".easyLocalized().uppercased()
        lblTitle.textAlignment = .left
       
        return lblTitle
    }()
    lazy var containerRequestFrom : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .easyWhite
        view.addSubview(lblRequestFromTitle)
        lblRequestFromTitle.addAnchorToSuperview(leading: 15, trailing: -15, top: 0, bottom: 0)
       
        return view
    }()
    
    lazy var lblRequestToTitle: UILabel = {
        let lblTitle = UILabel()
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font =  .EasyFont(ofSize: 10, style: .medium)
        lblTitle.textColor = .slateGrey
        lblTitle.text = "request_to".easyLocalized().uppercased()
        lblTitle.textAlignment = .left
       
        return lblTitle
    }()
    lazy var containerRequestTo : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .easyWhite
        view.addSubview(lblRequestToTitle)
        lblRequestToTitle.addAnchorToSuperview(leading: 15, trailing: -15, top: 0, bottom: 0)
       
        return view
    }()
  
    lazy var numberDetailsView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.layer.borderColor = UIColor.primaryBlue.cgColor
        //view.layer.borderWidth = 1
        //view.layer.cornerRadius = 10
        view.backgroundColor = .white
        //view.addShadow(location: .bottom, color: UIColor.black, opacity: 0.2, radius: 5)
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        view.addSubview(numberDetailsIcon)
      
        view.addSubview(stackView)
        view.addSubview(amountStackView)
      
        
        numberDetailsIcon.addAnchorToSuperview(leading: 16 ,centeredVertically: 0)
        numberDetailsIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
        numberDetailsIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        numberDetailsIcon.trailingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: -12).isActive = true
      
        stackView.addAnchorToSuperview(top: 8, bottom: -8)
        stackView.trailingAnchor.constraint(equalTo: amountStackView.leadingAnchor, constant: 8).isActive = true
        
        amountStackView.addAnchorToSuperview(trailing:-16,centeredVertically: 0)
        amountStackView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        stackView.addArrangedSubview(lblOpeartor)
        stackView.addArrangedSubview(lblSelectedNumber)
        
        return view
    }()
    
    lazy var numberDetailsIcon : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "appleIcon")
        view.contentMode = .scaleAspectFit
       
        return view
    }()
    lazy var lblOpeartor: UILabel = {
        let lblOpeartor = UILabel()
        lblOpeartor.translatesAutoresizingMaskIntoConstraints = false
        lblOpeartor.font =  .EasyFont(ofSize: 10, style: .bold)
        lblOpeartor.textColor = .slateGrey
        lblOpeartor.text = ""
        lblOpeartor.textAlignment = .left
       
        return lblOpeartor
    }()
    lazy var lblSelectedNumber: UILabel = {
        let lblOpeartor = UILabel()
        lblOpeartor.translatesAutoresizingMaskIntoConstraints = false
        lblOpeartor.font =  .EasyFont(ofSize: 16, style: .bold)
        lblOpeartor.textColor = .black
        lblOpeartor.text = ""
        lblOpeartor.textAlignment = .left
       
        return lblOpeartor
    }()
    lazy var lblAmount: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font =  .EasyFont(ofSize: 16, style: .bold)
        lbl.textColor = .black
        lbl.text = ""
       
        lbl.textAlignment = .left
       
        return lbl
    }()
    lazy var amountStackView:UIStackView = {
            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .horizontal
            stackView.spacing = 2
        stackView.addArrangedSubview(containerTaka)
        stackView.addArrangedSubview(lblAmount)
            return stackView
        }()
    
    lazy var lblTaka: UILabel = {
        let lblOpeartor = UILabel()
        lblOpeartor.translatesAutoresizingMaskIntoConstraints = false
        lblOpeartor.font =  .EasyFont(ofSize: 10, style: .regular)
        lblOpeartor.textColor = .black
        lblOpeartor.text = "৳"
        lblOpeartor.textAlignment = .center
       
        return lblOpeartor
    }()
    lazy var containerTaka : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTaka)
        lblTaka.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        lblTaka.widthAnchor.constraint(equalToConstant: 10).isActive = true
        return view
    }()
    
    
    

}




extension RechargeRequestEntryView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}

//class LoginNavBar:UIView{
//    override init(frame: CGRect) {
//        super.init(frame: .zero)
//    }
//    var backButtonCallBack:(()->Void)?
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    convenience init(title : String = "", leadingBtn:CGFloat = 10) {
//        self.init(frame:.zero)
//        backgroundColor = .clear
//        lblTitle.text = title
//        addSubview(backButton)
//        addSubview(lblTitle)
//        backButton.addAnchorToSuperview(leading: leadingBtn ,heightMultiplier: 0.8, centeredVertically: 0,  heightWidthRatio: 1.2)
//        lblTitle.addAnchorToSuperview(trailing: -15, top: 0, bottom: 0)
//        lblTitle.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 0).isActive = true
//        
//    }
//    private let backButton:UIButton = {
//        let backButton = UIButton()
//        backButton.setImage(UIImage(named: "backImage"), for: .normal)
//        backButton.tintColor = .black
//        backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
//        return backButton
//    }()
//    private let lblTitle : UILabel = {
//        let lbl = UILabel()
//        lbl.text = ""
//        lbl.textAlignment = .left
//        lbl.font = EasyUtils.FontUtils.EasyFont(ofSize: 11, style: .bold)
//        lbl.textColor = .black
//        lbl.numberOfLines = 1
//        return lbl
//    }()
//    @objc func btnBackAction(_ sender:UIButton){
//        backButtonCallBack?()
//    }
//}
