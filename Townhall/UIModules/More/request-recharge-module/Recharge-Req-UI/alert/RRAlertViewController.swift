//
//  RRAlertViewController.swift
//  Easy.com.bd
//
//  Created by Raihan on 19/7/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit

class RRAlertViewController: EasyBaseViewController {
    
    var type : AlertType?{
        didSet{
           
            switch type {
            case .success :
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "success", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                successViewAlert.successIcon.image = advTimeGif
                successViewAlert.lblSuccessMessage.textColor = .systemGreen
                EasyUtils.shared.delay(2.0) {
                    self.successViewAlert.successIcon.image = UIImage(named: "successEnd")
                }
                
               
                break
            case .error :
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "failed", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                successViewAlert.successIcon.image  = advTimeGif
                EasyUtils.shared.delay(2.0) {
                    self.successViewAlert.successIcon.image  = UIImage(named: "failedEnd")
                }
                
                successViewAlert.lblSuccessMessage.textColor = .red
                break
            case .wrong:
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "failed", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                successViewAlert.successIcon.image = advTimeGif
                EasyUtils.shared.delay(2.0) {
                    self.successViewAlert.successIcon.image = UIImage(named: "failedEnd")
                }
                successViewAlert.lblSuccessMessage.textColor = .red
               
                break
            default:
                successViewAlert.successIcon.image = #imageLiteral(resourceName: "failed")
                  break
            }
        }
    }
    
    lazy var successViewAlert: RechargeRequestAlertView = {
        let view = RechargeRequestAlertView()
        //view.goHomeCallBack = goHome
        return view
    }()

    override func viewDidLoad() {
        self.view = successViewAlert
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

