//
//  RechargeRequestAlertView.swift
//  Easy.com.bd
//
//  Created by Raihan on 19/7/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation

import UIKit


class RechargeRequestAlertView : UIView {
 
    var goHomeCallBack : (()->Void)?
  
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .clear
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        //containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 20, trailing: -20,centeredVertically: 0)
        containerView.heightAnchor.constraint(equalToConstant: 220).isActive = true
        containerView.addSubview(formView)
        formView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,bottom: 0)
        
    }
   
    //MARK: COMPONENTS
   
    
 
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.layer.cornerRadius = 10
        formView.backgroundColor = .white
        formView.addSubview(successIcon)
        formView.addSubview(lblSuccessMessage)
        formView.addSubview(btnGoHome)
        formView.addSubview(lblSubMessage)
        
        successIcon.addAnchorToSuperview(top: 20, centeredHorizontally: 0)
        successIcon.heightAnchor.constraint(equalToConstant: 60).isActive = true
        successIcon.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        
        lblSuccessMessage.addAnchorToSuperview(leading: 10, trailing: -10)
        lblSuccessMessage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lblSuccessMessage.topAnchor.constraint(equalTo: successIcon.bottomAnchor, constant: 0).isActive = true

        
        lblSuccessMessage.bottomAnchor.constraint(equalTo:  lblSubMessage.topAnchor, constant: -10).isActive = true
        lblSubMessage.addAnchorToSuperview(leading: 10, trailing: -10)
        lblSubMessage.bottomAnchor.constraint(equalTo:  btnGoHome.topAnchor, constant: -10).isActive = true
        lblSubMessage.heightAnchor.constraint(equalToConstant: 20).isActive = true
       

        btnGoHome.addAnchorToSuperview(leading: 20, trailing: -20,bottom: -30)
        btnGoHome.heightAnchor.constraint(equalToConstant: 50).isActive = true
       
        return formView
    }()
    
     let btnGoHome:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.layer.cornerRadius = 10
        btnProceed.setTitle("go_home".easyLocalized(), for: .normal)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        
        btnProceed.addTarget(self, action: #selector(btnGoHomeAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnGoHomeAction(_ sender:UIButton){
        goHomeCallBack?()
    }
    
    //MARK: InputViews
    
    lazy var lblSuccessMessage : UILabel = {
        let lblSuccess = UILabel()
        lblSuccess.translatesAutoresizingMaskIntoConstraints = false
        lblSuccess.font =  .EasyFont(ofSize: 16, style: .medium)
        lblSuccess.textColor = .slateGrey
        lblSuccess.text = "enter_following_details".easyLocalized()
        lblSuccess.textAlignment = .center
        lblSuccess.textColor = .green
        lblSuccess.numberOfLines = 2
       
        return lblSuccess
    }()
    lazy var lblSubMessage : UILabel = {
        let lblSuccess = UILabel()
        lblSuccess.translatesAutoresizingMaskIntoConstraints = false
        lblSuccess.font =  .EasyFont(ofSize: 12, style: .regular)
        lblSuccess.textColor = .slateGrey
        lblSuccess.text = "enter_following_details".easyLocalized()
        lblSuccess.textAlignment = .center
        lblSuccess.numberOfLines = 1
       
        return lblSuccess
    }()

    lazy var successIcon : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "successcheck")
        view.contentMode = .scaleAspectFit
       
        return view
    }()


}









