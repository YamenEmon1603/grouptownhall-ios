////
////  RequestRouterModule.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 2/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class RequestRouterModule: RequestRouterProtocol{
//   
//    
//    func pushToQuickie(vc: UIViewController, operators: [MBOperator], delegate: QuickyDoneProtocol) {
//        let quickieModule = QuickieRouterModule.createModule(with: operators, delegate: delegate)
//             //nc.modalPresentationStyle = .overFullScreen
//             quickieModule.modalPresentationStyle = .fullScreen
//      
//             vc.present(quickieModule, animated: true, completion: nil)
//    }
//    
//    var navigationController : UINavigationController?
//    
//    
//    
//    
//    static func createModule(nc : UINavigationController) -> RequestViewController {
//        let presenter: RequestPresenterProtocol & RequestOutputInteractorProtocol = RequestPresenterModule()
//        let router: RequestRouterProtocol = RequestRouterModule()
//        let interactor: RequestInputInteractorProtcol = RequestInteractorModule()
//        let storyboradName = "Request"
//        let storyboard = UIStoryboard(name:storyboradName , bundle: nil)
//        let view: RequestViewProtocol = storyboard.instantiateViewController(withIdentifier: "requestVC") as! RequestViewController
//        
//        //connecting
//        
//        view.presenter = presenter
//        presenter.interactor = interactor
//        presenter.view = view
//        presenter.router = router
//        interactor.presenter = presenter
//        router.navigationController = nc
//        return view as! RequestViewController
//    }
//
//    
//    func pushToAnotherVC(vc: UIViewController, tag: String) {
//        switch tag {
//        case Constants.input_mobile_number:
//            
//            break
//        case Constants.quickie_vc:
//            
//            break
//        case Constants.see_all_offer:
//            
//            break
//        default:
//            break
//        }
//        
//    }
//    
//  
//    
//    
//    
//    func showAllPlans(with vc: UIViewController, plans: [RechargePlan], operators: [MBOperator], selectedOperator:MBOperator? = nil, and selectionDelegate: OfferSelectionProtocol) {
//        if let op = selectedOperator{
//            let seeAllRouterModule = SelectOfferRouterModule.createModule(with: plans, operatorList: operators, selectedOperator: op, selectionDelegate: selectionDelegate)
//            seeAllRouterModule.modalPresentationStyle = .fullScreen
//            
//            vc.present(seeAllRouterModule, animated: true, completion: nil)
//        }else{
//            let seeAllRouterModule = SelectOfferRouterModule.createModule(with: plans, operatorList: operators , selectionDelegate : selectionDelegate)
//            //nc.modalPresentationStyle = .overFullScreen
//            seeAllRouterModule.modalPresentationStyle = .fullScreen
//            
//            vc.present(seeAllRouterModule, animated: true, completion: nil)
//        }
//   
//    }
//
//    func showOperatorSelection(vc: UIViewController, operatorList: [MBOperator], plans: [RechargePlan], entity: RechargeEntity, delegate: OperatorSelectionProtocol) {
////        topMostVC().dismiss(animated: true, completion: {
////            let operatorSelectionModule = OSRouterModule.createModule(with: operatorList,selectedNumber: entity, plans: plans, delegate: delegate)
////            operatorSelectionModule.modalPresentationStyle = .fullScreen
////            vc.present(operatorSelectionModule, animated: true, completion: nil)
////        })
////
//   
//         let operatorSelectionModule = OSRouterModule.createModule(with: operatorList,selectedNumber: entity, plans: plans, delegate: delegate)
//                operatorSelectionModule.modalPresentationStyle = .fullScreen
//                vc.present(operatorSelectionModule, animated: true, completion: nil)
//    }
//    func pushToRechargeSummery(vc: UIViewController, operatorList: [MBOperator], plans: [RechargePlan], entity: RechargeEntity) {
//        
//    }
//    func pushToRechargeSummery(vc: UIViewController, operatorList: [MBOperator], plans: [RechargePlan], entity: RechargeEntity, confirmationDelegate: NumberConfimationProtocol) {
//      
//    }
//    
//    func pushToNumberConfirmation(vc: UIViewController, operatorList: [MBOperator], plans: [RechargePlan], entity: RechargeEntity, confirmationDelegate: NumberConfimationProtocol, connection: String) {
//        let confirmNumberModule = RCNumberRouterModule.createModule(with: operatorList, entity: entity, plans: plans,delegate:confirmationDelegate, connection: connection)
//        confirmNumberModule.modalPresentationStyle = .fullScreen
////        vc.present(confirmNumberModule, animated: true, completion: nil)
//        vc.navigationController?.pushViewController(confirmNumberModule, animated: true)
//    }
//    
//    
//    func pushToRechargeSummery(vc: UIViewController,operators: [MBOperator], enity: RechargeEntity?) {
//        let rsModule = RSRouterModule.createModule(nc: self.navigationController!, operators: operators, enity: enity)
//        navigationController?.pushViewController(rsModule, animated: true)
//      
//    }
//    func topMostVC() -> UIViewController {
//        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
//        while (topController.presentedViewController != nil) {
//            topController = topController.presentedViewController!
//        }
//        return topController
//    }
//}
