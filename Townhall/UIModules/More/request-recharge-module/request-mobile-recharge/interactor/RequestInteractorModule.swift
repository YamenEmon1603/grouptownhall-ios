////
////  RequestInteractorModule.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 2/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class RequestInteractorModule: RequestInputInteractorProtcol{
//    var rechargeEntity: RechargeEntity?
//    var rechargePlans: [RechargePlan]?
//    var selectedPlan : RechargePlan?
//    var ConnectionType: String?
//    
//    var rechargePlansResponse :RechargePlanResponse?
//    var mbOperators: [MBOperator]?
//    var presenter: RequestOutputInteractorProtocol?
//    
//    func getOprators() {
//        if self.mbOperators?.count == 0 || self.mbOperators == nil {
//            WebServiceHandler.shared.getOprators(completion: { (result) in
//                switch result{
//                case .success(let response):
//                    if response.code == 200{
//                        if let data = response.data{
//                            EasySingleton.shared.easyMobileOperators = data
//                        }
//                        
//                        self.mbOperators = response.data
//                    }
//                    break
//                case .failure(let error):
//                    debugPrint(error)
//                    break
//                }
//            }, shouldShowLoader: false)
//        }
//    }
//    
//    func getRechargePlans() {
//        
//        if let rps = rechargePlansResponse{
//            self.presenter?.received(response: rps )
//        }else{
//            WebServiceHandler.shared.getRechargePlans(completion: { (result) in
//                switch result {
//                case .success(let response):
//                    self.rechargePlansResponse = response
//                    self.presenter?.received(response: response)
//                    break
//                case .failure(let error):
//                    debugPrint(error)
//                    break
//                    
//                }
//            }, shouldShowLoader: true)
//        }
//        
//        
//    }
//    
//
//}
//
