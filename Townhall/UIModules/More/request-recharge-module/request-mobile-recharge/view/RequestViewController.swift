////
////  MobileRechargeViewController.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 2/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import UIKit
//import ContactsUI
//
//class RequestViewController: UIViewController, RequestViewProtocol, UITextFieldDelegate, OperatorSelectionProtocol,CNContactPickerDelegate{
//  
//    
//  
//    
//    
//    
//    @IBOutlet weak var lblSpeciallyForYou: UILabel!
//    @IBOutlet weak var availableOfferHeight: NSLayoutConstraint!
//    @IBOutlet weak var llAvailOfferText: UILabel!
//    @IBOutlet weak var btnAvailSelect: UIButton!
//    //@IBOutlet weak var btnAvailDeselect: UIButton!
//    @IBOutlet weak var viewOfferAvail: UIView!
//    @IBOutlet weak var btnProceed: UIButton!
//    
//    @IBOutlet weak var viewBottom: UIView!
//    
//    @IBOutlet weak var btnPrepaid: UIButton!
//    @IBOutlet weak var llPrePaid: UILabel!
//    
//    @IBOutlet weak var btnPostPaid: UIButton!
//    @IBOutlet weak var llPostPaid: UILabel!
//    
//    @IBOutlet weak var tfEnterNumber: UITextField!
//    @IBOutlet weak var tfEnterAmount: UITextField!
//    
//    @IBOutlet weak var qv1: UIView!
//    @IBOutlet weak var qv2: UIView!
//    @IBOutlet weak var qv3: UIView!
//    @IBOutlet weak var qv4: UIView!
//    
//    @IBOutlet weak var llQ1Text: UILabel!
//    @IBOutlet weak var llQ2Text: UILabel!
//    @IBOutlet weak var llQ3Text: UILabel!
//    @IBOutlet weak var llQ4Text: UILabel!
//    
//    @IBOutlet weak var mostPopularTableView: UITableView!
//    
//    @IBOutlet weak var llScheduleRecharge: CustomLabel!
//    @IBOutlet weak var btnScheduleRecharge: UIButton!
//    
//    @IBOutlet weak var llAddAnotherNumber: CustomLabel!
//    @IBOutlet weak var btnAddAnother: UIButton!
//    @IBOutlet weak var viewQuickAMountContainer: UIView!
//    @IBOutlet weak var viewEnterAmount: UIView!
//    
//    @IBOutlet weak var lblMobileRecharge: CustomLabel!
//    @IBOutlet weak var lblMostPopularOffer: CustomLabel!
//    @IBOutlet weak var lblProceedToRecharge: CustomLabel!
//    
//   
//    @IBOutlet weak var btnViewAll: UIButton!
//   
//    
//    var presenter: RequestPresenterProtocol?
//    
//    var first5Plans: [RechargePlan]?{
//        didSet{
//            mostPopularTableView.reloadData()
////            if let amount  = enterAmountString{
////                let amnt = Int(amount) ?? 0
////                filterValue(amount: amnt)
////            }
//        }
//    }
//    var fromOfferSelection = false
//    var radioPrePost : Bool = true
//    var PaymentType: String = "prepaid"
//    var enterPhoneNumberString: String?{
//        didSet{
//            tfEnterNumber.text = enterPhoneNumberString
//            if let amount  = enterAmountString{
//                let amnt = Int(amount ) ?? 0
//                filterValue(amount: amnt)
//            }
//            
//            ///
//            
//            if(tfEnterNumber.text == ""){
//                showToast(message: EasyUtils.getPleaseEnterMessage(enter_number))
//            }else{
//                
//                presenter?.interactor?.ConnectionType = PaymentType
//                
//                presenter?.pushRechargeSummery(vc: self)
//                
//            }
//            
//            //
//        }
//    }
//    var enterAmountString: String?{
//        set{
//            if newValue == "0"{
//                
//                tfEnterAmount.text = ""
//            }else{
//                tfEnterAmount.text = newValue
//            }
//            //tfEnterAmount.text = newValue
//             presenter?.prepareMostPopular(op: presenter?.interactor?.mbOperators?.first(where: {$0.operatorId == presenter?.interactor?.rechargeEntity?.operatorId}) , conectionType: presenter?.interactor?.rechargeEntity?.type ?? "", amount: Int(newValue ?? "0") ?? 0)
//            let amount = Int(newValue ?? "0") ?? 0
//            filterValue(amount: amount)
//        }
//        get{return tfEnterAmount.text!}
//        
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        initView()
//        navigationController?.setNavigationBarHidden(true, animated: true)
//        roundQuickView()
//        tfEnterNumber.addTarget(self, action: #selector(enterNumberChange(_ :)), for: .allEditingEvents)
//        mostPopularTableView.delegate = self
//        mostPopularTableView.dataSource = self
//        if let selectedPlan = presenter?.interactor?.selectedPlan{
//            tfEnterAmount.text = "\(selectedPlan.amountMax ?? 0)"
//        }
//    }
//
//    func removeTextField(){
//        tfEnterNumber.text = ""
//        tfEnterAmount.text = ""
//        resetQuickAMount()
//        
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//
//        
//        availableOfferHeight.constant = 0
//        languageSwitch(index: EasyUtils.getLanguageIndex())
//        presenter?.viewWillAppear()
//        presenter?.interactor?.getRechargePlans()
//        (self.tabBarController as? TabBarViewModule)?.customTabBar.isHidden = true
//    }
//    
//    func didSelect(_ mbOperator:MBOperator, connectionType:String){
//        PaymentType = connectionType
//       
//        presenter?.prepareMostPopular(op: mbOperator, conectionType: connectionType, amount: Int(enterAmountString ?? "0") ?? 0)
//        if(tfEnterNumber.text == ""){
//            showToast(message: EasyUtils.getPleaseEnterMessage(enter_number))
//        }else{
//            presenter?.interactor?.rechargeEntity = RechargeEntity(type: connectionType, phone: tfEnterNumber.text ?? "", amount:  Int(enterAmountString ?? "0") ?? 0, operatorId: mbOperator.operatorId ?? 0, plan:nil, scRechargeMob: "", scRawDate: nil)
//            presenter?.interactor?.ConnectionType = PaymentType
//            presenter?.pushRechargeSummery(vc: self)
//            
//        }
//    }
//    func did(select mbOperator: MBOperator) {
//        debugPrint(mbOperator)
//    }
//    
//    @objc func enterNumberChange(_ sender:UITextField){
//
//        if sender.text?.count == 11{
//            self.presenter?.router?.showOperatorSelection(vc: self, operatorList: self.presenter?.interactor?.mbOperators ?? [], plans: [], entity: RechargeEntity(type: "", phone: sender.text ?? "", amount: 0, operatorId: 0, plan: nil, scRechargeMob: "", scRawDate: nil), delegate: self)
//        }else{
//            
//        }
//    }
//    func languageSwitch(index: Int){
//        lblMobileRecharge.text = recharge_request[index].uppercased()
//       
//        btnViewAll.setTitle(view_all_cap[index], for: .normal)
//        tfEnterNumber.placeholder = enter_number[index]
//        tfEnterAmount.placeholder = enter_amount[index]
//        lblMostPopularOffer.text = most_popular_offers[index]
//        llScheduleRecharge.text = schedule_recharge[index]
//        llAddAnotherNumber.text = add_another_number[index]
//        llPrePaid.text = prepaid[index]
//        llPostPaid.text  = postpaid[index]
//        btnAvailSelect.setTitle(apply[index], for: .normal)
//        lblSpeciallyForYou.text = specially_for_you[index]
//        lblProceedToRecharge.text = proceed_to_recharge[index]
//        
//        ////
//        if let attributedTitle = btnViewAll.attributedTitle(for: .normal) {
//            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
//            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: view_all_cap[index])
//            btnViewAll.setAttributedTitle(mutableAttributedTitle, for: .normal)
//        }
//        llQ1Text.text = EasySingleton.shared.getLocalizedDigits("50")
//        llQ2Text.text = EasySingleton.shared.getLocalizedDigits("100")
//        llQ3Text.text = EasySingleton.shared.getLocalizedDigits("200")
//        llQ4Text.text = EasySingleton.shared.getLocalizedDigits("500")
//        
//        /////
//        
//    }
//    
//    @IBAction func btnApplyOfferedAmout(_ sender: Any) {
//        
//        if tfEnterNumber.text != ""{
//            let entereBdt = Int(tfEnterAmount.text ?? "0") ?? 0
//        let testData = first5Plans?.filter{$0.amountMax! > entereBdt}
//            if let offerAmount = testData?.first?.amountMax{
//                _ = offerAmount - entereBdt
//                tfEnterAmount.text = "\(offerAmount)"
//                availableOfferHeight.constant = 0
//                resetQuickAMount()
//            
//            }
//            
//        }else{
//            showToast(message: enter_number[EasyUtils.getLanguageIndex()])
//        }
//        
//    }
//    
//    @IBAction func btnSeeAllAction(_ sender: Any) {
//        // presenter?.pushToAnotherVC(vc: self, tag: Constants.see_all_offer)
//        presenter?.interactor?.rechargeEntity = nil
//        presenter?.showAllPlans(vc: self)
//    }
//    
//    @IBAction func btnQuickAction(_ sender: UIButton) {
//        presenter?.interactor?.rechargeEntity?.plan = nil
//        switch sender.tag {
//        case 400:
//            quickAmountAction(amount: "50", view: qv1, textLabel: llQ1Text)
//            break
//        case 401:
//            quickAmountAction(amount: "100", view: qv2, textLabel: llQ2Text)
//            break
//        case 402:
//            quickAmountAction(amount: "200", view: qv3, textLabel: llQ3Text)
//            break
//        case 403:
//            quickAmountAction(amount: "500", view: qv4, textLabel: llQ4Text)
//            break
//        default:
//            break
//        }
//    }
//    func filterValue(amount: Int){
//        if amount > 9 {
//            let offerData = presenter?.interactor?.rechargePlans?.filter{$0.amountMax! > amount}
//            if let offerAmount = offerData?.first?.amountMax{
//                let amountForOffer = offerAmount - amount
//                let offer = offerData?.first?.offerTitle
//                let nextOffer = "Add \(amountForOffer)TK more to get \(offer ?? "nil")"
//                if fromOfferSelection == false{
//                viewOfferAvail.isHidden = false
//                availableOfferHeight.constant = 70
//                }else{
//                    fromOfferSelection = false
//                    viewOfferAvail.isHidden = true
//                    availableOfferHeight.constant = 0
//                }
//                llAvailOfferText.text = nextOffer
//            
//            }
//            first5Plans = offerData?.enumerated().compactMap{ $0.offset < 5 ? $0.element : nil }
//            
//        
//        }
//    }
//    
//
//    
//    @IBAction func btnAvailSelectDeselect(_ sender: Any) {
//    }
//    
//    
//    @IBAction func btnContactAction(_ sender: Any) {
//       
//        
//        let contacVC = CNContactPickerViewController()
//              contacVC.delegate = self
//              self.present(contacVC, animated: true, completion: nil)
//    }
//    
//    @IBAction func btnActionForMobileRecharge(_ sender: Any) {
//        if(tfEnterNumber.text == ""){
//            showToast(message: EasyUtils.getPleaseEnterMessage(enter_number))
//        }else if (tfEnterAmount.text == "" || tfEnterAmount.text == "0"){
//            showToast(message: EasyUtils.getPleaseEnterMessage(enter_amount))
//        }else{
//           
//            if(presenter?.interactor?.rechargeEntity?.amount == 0){
//                presenter?.interactor?.rechargeEntity?.amount = Int(tfEnterAmount.text ?? "")
//            }
//            presenter?.interactor?.ConnectionType = PaymentType
//            presenter?.pushRechargeSummery(vc: self)
//            
//        }
//    
//       
//    }
//    
//    @IBAction func btnSwitchPrepaidPostPaid(_ sender: Any) {
//        self.radioPrePost = !radioPrePost
//        if radioPrePost{
//            btnPrepaid.setImage(UIImage(named: "radioselected"), for: .normal)
//            llPrePaid.textColor = UIColor.hexStringToUIColor(hex: "662e91")
//            btnPostPaid.setImage(UIImage(named: "radiodeselect"), for: .normal)
//            llPostPaid.textColor = UIColor.hexStringToUIColor(hex: "455065")
//            PaymentType = "prepaid"
//        }else{
//            btnPrepaid.setImage(UIImage(named: "radiodeselect"), for: .normal)
//            llPrePaid.textColor = UIColor.hexStringToUIColor(hex: " 455065")
//            btnPostPaid.setImage(UIImage(named: "radioselected"), for: .normal)
//            llPostPaid.textColor = UIColor.hexStringToUIColor(hex: "662e91")
//            PaymentType = "postpaid"
//        }
//    }
//    
//    
//    @IBAction func btnBackAction(_ sender: Any) {
//      
//        UserSettings.sharedInstance.selectedNumbers.removeAll()
//        self.navigationController?.popViewController(animated: true)
//    }
//    
//    
//
//    
////    // MARK: Delegate method CNContectPickerDelegate
//    
//     func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
//         self.dismiss(animated: true, completion: nil)
//     }
//    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
//        debugPrint("\(contactProperty.key) : \((contactProperty.value as! CNPhoneNumber).stringValue)")
//        var  numberString = (contactProperty.value as! CNPhoneNumber).stringValue.withoutSpecialCharacters.deletingPrefix("88").replacingOccurrences(of: "-", with: "")
//        numberString = numberString.replacingOccurrences(of: " ", with: "")
//        
//        selectionDone(numberString: numberString)
//    }
//    func selectionDone(numberString: String){
//        SSLLoader.sharedInstance.startAnimation()
//        tfEnterNumber.text = numberString
//        EasySingleton.shared.delay(0.5) {
//            SSLLoader.sharedInstance.stopAnimation()
//            self.presenter?.router?.showOperatorSelection(vc: self, operatorList: self.presenter?.interactor?.mbOperators ?? [], plans: [], entity: RechargeEntity(type: "", phone: numberString, amount: 0, operatorId: 0, plan: nil, scRechargeMob: "", scRawDate: nil), delegate: self)
//        }
//        
//       
//     
//    }
//    
//    func initView(){
//        tfEnterAmount.delegate = self
//        if #available(iOS 10.0, *) {
//            tfEnterAmount.keyboardType = .asciiCapableNumberPad
//        } else {
//            tfEnterAmount.keyboardType = .numberPad
//        }
//       
//    }
//    
//    func roundQuickView(){
//        qv1.layer.cornerRadius = qv1.frame.size.width / 2
//        qv2.layer.cornerRadius = qv2.frame.size.width / 2
//        qv3.layer.cornerRadius = qv3.frame.size.width / 2
//        qv4.layer.cornerRadius = qv4.frame.size.width / 2
//        
//        qv1.layer.shadowColor = UIColor.black.cgColor
//        qv1.layer.shadowOffset = .zero
//        qv1.layer.shadowOpacity = 0.2
//        qv1.layer.shadowRadius = 1.0
//        
//        qv2.layer.shadowColor = UIColor.black.cgColor
//        qv2.layer.shadowOffset = .zero
//        qv2.layer.shadowOpacity = 0.2
//        qv2.layer.shadowRadius = 1.0
//        
//        qv3.layer.shadowColor = UIColor.black.cgColor
//        qv3.layer.shadowOffset = .zero
//        qv3.layer.shadowOpacity = 0.2
//        qv3.layer.shadowRadius = 1.0
//        
//        qv4.layer.shadowColor = UIColor.black.cgColor
//        qv4.layer.shadowOffset = .zero
//        qv4.layer.shadowOpacity = 0.2
//        qv4.layer.shadowRadius = 1.0
//        
//        
//        qv1.layer.borderWidth = 1
//        qv1.layer.borderColor = UIColor.clear.cgColor
//        qv2.layer.borderWidth = 1
//        qv2.layer.borderColor = UIColor.clear.cgColor
//        qv3.layer.borderWidth = 1
//        qv3.layer.borderColor = UIColor.clear.cgColor
//        qv4.layer.borderWidth = 1
//        qv4.layer.borderColor = UIColor.clear.cgColor
//    }
//    func resetQuickAmountFrom(){
//        resetQuickAMount()
//    }
//    
//    func quickAmountAction(amount: String, view: UIView, textLabel: UILabel){
//        qv1.backgroundColor = .white
//        qv2.backgroundColor = .white
//        qv3.backgroundColor = .white
//        qv4.backgroundColor = .white
//        
//        llQ1Text.textColor = ColorStore.text_color_quick_amount
//        llQ2Text.textColor = ColorStore.text_color_quick_amount
//        llQ3Text.textColor = ColorStore.text_color_quick_amount
//        llQ4Text.textColor = ColorStore.text_color_quick_amount
//        roundQuickView()
//        enterAmountString = amount
//        view.backgroundColor = .white
//        view.layer.borderWidth = 1
//        view.layer.borderColor = UIColor.primaryBlue.cgColor
//        textLabel.textColor = .primaryBlue
//        if  let count = enterAmountString?.count, count > 0{
//            viewEnterAmount.layer.borderColor = UIColor.primaryBlue.cgColor
//        }
//    }
//    
//    func resetQuickAMount(){
//        qv1.backgroundColor = .white
//        qv2.backgroundColor = .white
//        qv3.backgroundColor = .white
//        qv4.backgroundColor = .white
//        
//        qv1.layer.borderWidth = 1
//        qv1.layer.borderColor = UIColor.clear.cgColor
//        qv2.layer.borderWidth = 1
//        qv2.layer.borderColor = UIColor.clear.cgColor
//        qv3.layer.borderWidth = 1
//        qv3.layer.borderColor = UIColor.clear.cgColor
//        qv4.layer.borderWidth = 1
//        qv4.layer.borderColor = UIColor.clear.cgColor
//        
//        llQ1Text.textColor = ColorStore.text_color_quick_amount
//        llQ2Text.textColor = ColorStore.text_color_quick_amount
//        llQ3Text.textColor = ColorStore.text_color_quick_amount
//        llQ4Text.textColor = ColorStore.text_color_quick_amount
//        viewEnterAmount.layer.borderColor = UIColor.hexStringToUIColor(hex: "DEE3EA").cgColor
//    }
//    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if(textField == tfEnterAmount){
//            presenter?.interactor?.rechargeEntity?.plan = nil
//        }
//        viewEnterAmount.layer.borderColor = ColorStore.active_color.cgColor
//        
//    }
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        let maxLength = 11
//        let currentString: NSString = textField.text! as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//        debugPrint(newString.length,"count")
//        if(newString.length > 1){
//            UIView.transition(with: viewQuickAMountContainer!,
//                              duration: 0.5,
//                              options: [.transitionCrossDissolve],
//                              animations: {
//                                
//                                self.viewQuickAMountContainer!.isHidden = true
//                                let amnt = Int(newString as String) ?? 0
//                                self.filterValue(amount: amnt)
//                                
//                                
//            },
//                              completion: nil)
//          
//            resetQuickAMount()
//            presenter?.prepareMostPopular(op: presenter?.interactor?.mbOperators?.first(where: {$0.operatorId == presenter?.interactor?.rechargeEntity?.operatorId}) , conectionType: presenter?.interactor?.rechargeEntity?.type ?? "", amount: Int((newString) as String) ?? 0)
//        }else{
//            UIView.transition(with: viewQuickAMountContainer!,
//                              duration: 0.5,
//                              options: [.showHideTransitionViews],
//                              animations: {
//                                
//                                self.viewQuickAMountContainer!.isHidden = false
//                                self.viewOfferAvail.isHidden = true
//                                self.availableOfferHeight.constant = 0
//            },
//                              completion: nil)
//            resetQuickAMount()
//            //self.llScheduleRecharge.textColor = ColorStore.text_color_schedule
//            //self.llAddAnotherNumber.textColor = ColorStore.text_color_schedule
//        }
//        return newString.length <= maxLength
//        
//        
//    }
//    func shouldSelectOperator() {
//        self.presenter?.showOperatorSelection(vc: self)
//    }
//    
//    func shouldShowRechargeSummer() {
//        quickAmountAction(amount: "", view: UIView(), textLabel: UILabel())
//        presenter?.showRechargeSummery(vc: self)
//    }
//}
//
//extension RequestViewController: UITableViewDelegate, UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return first5Plans?.count ?? 0
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "mostPopularCell", for: indexPath) as! MostPopularTableViewCell
//        cell.item = first5Plans?[indexPath.item]
//        
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        fromOfferSelection = true
//        resetQuickAMount()
//        tfEnterAmount.text = "\(String(describing: first5Plans?[indexPath.row].amountMax ?? 0))"
//        presenter?.interactor?.selectedPlan = first5Plans?[indexPath.item]
//        filterValue(amount: first5Plans?[indexPath.row].amountMax ?? 0)
//    }
//    
//}
