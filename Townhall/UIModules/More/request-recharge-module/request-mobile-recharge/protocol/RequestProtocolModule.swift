////
////  MobileRechargeProtocolModule.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 2/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//protocol RequestViewProtocol: class {
//    var presenter: RequestPresenterProtocol?{get set}
//    var first5Plans: [RechargePlan]? {get set}
//    var enterAmountString: String?{get set}
//    var enterPhoneNumberString:String?{get set}
//    func shouldSelectOperator()
//    func shouldShowRechargeSummer()
//    func removeTextField()
//    func resetQuickAmountFrom()
//}
//
//protocol RequestPresenterProtocol: class {
//    var view: RequestViewProtocol?{get set}
//    var router: RequestRouterProtocol?{get set}
//    var interactor: RequestInputInteractorProtcol?{get set}
//    func pushToAnotherVC(vc: UIViewController, tag: String)
//   
//    func pushToQuickie(vc: UIViewController)
//    func viewWillAppear()
//
//    func showAllPlans(vc:UIViewController)
//    func showOperatorSelection(vc: UIViewController)
//    func pushRechargeSummery(vc:UIViewController)
//    func showRechargeSummery(vc: UIViewController)
//    func prepareMostPopular(op : MBOperator? ,conectionType:String, amount: Int)
//}
//
//protocol RequestRouterProtocol: class {
//    var navigationController : UINavigationController?{get set}
//    static func createModule(nc : UINavigationController)-> RequestViewController
//    func pushToAnotherVC(vc: UIViewController, tag: String)
//    func pushToQuickie(vc: UIViewController, operators: [MBOperator], delegate : QuickyDoneProtocol)
//    
//    func showAllPlans(with vc :UIViewController,  plans: [RechargePlan], operators:[MBOperator], selectedOperator:MBOperator?, and  selectionDelegate : OfferSelectionProtocol)
//    func showOperatorSelection(vc: UIViewController, operatorList: [MBOperator], plans: [RechargePlan], entity: RechargeEntity ,delegate: OperatorSelectionProtocol)
//    func pushToNumberConfirmation(vc: UIViewController, operatorList: [MBOperator], plans: [RechargePlan], entity: RechargeEntity , confirmationDelegate : NumberConfimationProtocol,connection: String)
//    func pushToRechargeSummery(vc: UIViewController,operators: [MBOperator], enity: RechargeEntity?) 
//}
//
//protocol RequestInputInteractorProtcol: class {
//    var presenter: RequestOutputInteractorProtocol?{get set}
//    var mbOperators:[MBOperator]?{get set}
//    var rechargePlans: [RechargePlan]?{get set}
//    var rechargeEntity : RechargeEntity?{get set}
//    var selectedPlan : RechargePlan?{get set}
//    var rechargePlansResponse :RechargePlanResponse?{get set}
//    var ConnectionType : String?{get set}
//    func getOprators()
//    func getRechargePlans()
//    
//    
//}
//
//protocol RequestOutputInteractorProtocol: class {
//    func received(response: RechargePlanResponse)
//}
