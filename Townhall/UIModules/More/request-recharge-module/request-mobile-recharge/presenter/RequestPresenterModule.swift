////
////  RequestPresenterModule.swift
////  Easy.com.bd
////
////  Created by Jamil Hasnine Tamim on 2/10/19.
////  Copyright © 2019 SSL Wireless. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class RequestPresenterModule: RequestPresenterProtocol, RequestOutputInteractorProtocol, OperatorSelectionProtocol {
//    
//    
//    var view: RequestViewProtocol?
//    
//    var router: RequestRouterProtocol?
//    
//    var interactor: RequestInputInteractorProtcol?
//    
//    func pushToAnotherVC(vc: UIViewController, tag: String) {
//        router?.pushToAnotherVC(vc: vc, tag: tag)
//    }
//    
//    func pushToQuickie(vc: UIViewController) {
//        if let mbos = interactor?.mbOperators{
//            router?.pushToQuickie(vc: vc, operators: mbos, delegate: self)
//        }
//        
//    }
//    func viewWillAppear() {
//        if(interactor?.mbOperators == nil){
//            interactor?.getOprators()
//        }
//        if(interactor?.rechargePlans == nil){
//            interactor?.getRechargePlans()
//        }
//    }
//    func pushToInputNumber(vc: UIViewController, amount : Int , type:String ) {
//        if let mbos = interactor?.mbOperators {
//            interactor?.rechargeEntity = RechargeEntity(type: type, amount: amount,plan: interactor?.selectedPlan)
////            router?.pushToInputNumber(vc:vc , operators: mbos, enity: interactor!.rechargeEntity!, plans: interactor?.rechargePlans ?? [RechargePlan](), inputNumberDelegate: self)
//        }
//        
//    }
//    
//    func received(response: RechargePlanResponse) {
//        if response .code == 200 {
//            interactor?.rechargePlans = response.data
//            prepareMostPopular()
//        }
//    }
//    func getOperatorImgUrl(id: String) -> String {
//        return EasySingleton.shared.getOperatorLogo(by: id)
//    }
//    
//    func showAllPlans(vc: UIViewController) {
//        if let id = interactor?.rechargeEntity?.operatorId, let mbOperator = interactor?.mbOperators?.first(where: {$0.operatorId == id}){
//            router?.showAllPlans(with: vc, plans: interactor?.rechargePlans ?? [RechargePlan](), operators: interactor?.mbOperators ?? [MBOperator](), selectedOperator: mbOperator, and: self)
//        }else{
//            router?.showAllPlans(with: vc, plans: interactor?.rechargePlans ?? [RechargePlan](), operators: interactor?.mbOperators ?? [MBOperator](), selectedOperator: nil, and: self)
//        }
//        
//    }
//    
//    func showOperatorSelection(vc: UIViewController) {
//        router?.showOperatorSelection(vc: vc, operatorList: interactor?.mbOperators ?? [MBOperator](), plans: interactor?.rechargePlans ?? [RechargePlan](), entity: interactor!.rechargeEntity!, delegate: vc as! OperatorSelectionProtocol)
//    }
//    func pushRechargeSummery(vc: UIViewController) {
//        if(interactor?.rechargeEntity?.operatorId == 0 ){
//            router?.showOperatorSelection(vc: vc, operatorList: interactor?.mbOperators ?? [MBOperator](), plans: interactor?.rechargePlans ?? [RechargePlan](), entity:  interactor!.rechargeEntity!, delegate: vc as! OperatorSelectionProtocol)
//        }else{
//            view?.removeTextField()
//            router?.pushToNumberConfirmation(vc: vc, operatorList: interactor?.mbOperators ?? [MBOperator](), plans: interactor?.rechargePlans ?? [RechargePlan](), entity: interactor!.rechargeEntity!, confirmationDelegate: self, connection: interactor?.ConnectionType ?? "")
//        }
//        
//    }
//    func showRechargeSummery(vc: UIViewController) {
//        debugPrint(UserSettings.sharedInstance.selectedNumbers.count)
//        router?.pushToRechargeSummery(vc:vc , operators: interactor?.mbOperators ?? EasySingleton.shared.easyMobileOperators, enity: interactor!.rechargeEntity)
//        interactor?.mbOperators = nil
//        interactor?.rechargePlans = nil
//        interactor?.selectedPlan = nil
//        interactor?.rechargeEntity = nil
//        view?.enterAmountString = ""
//        view?.enterPhoneNumberString = ""
//        
//    }
//    func prepareMostPopular(op : MBOperator? = nil,conectionType:String = "", amount: Int = 0){
//        if var filtered = self.interactor?.rechargePlans{
//            if let op = op{
//                filtered = filtered.filter({$0.operatorShortName == op.operatorShortName})
//            }
//            if conectionType.lowercased() == "prepaid"{
//                filtered = filtered.filter({$0.isForPrepaid == 1})
//            }
//            if conectionType.lowercased() == "postpaid"{
//                filtered = filtered.filter({$0.isForPostpaid == 1})
//            }
//            if amount > 0{
//                filtered = filtered.filter({(item) in
//                    if let maxAmount =  item.amountMax{
//                        return maxAmount > amount
//                    }
//                    return false
//
//                })
//            }
//            var five = filtered.filter({$0.isPopular == 1})
//            if five.count < 5 {
//                let sorted = filtered.sorted(by: {$0.usageCnt! > $1.usageCnt!}).filter({$0.isPopular == 0})
//                five.append(contentsOf: sorted.prefix(5 - five.count))
//            }
//            view?.first5Plans = five
//        }
//       
//            
//        
//    }
//    
//}
//extension RequestPresenterModule : OfferSelectionProtocol{
//    func didSelect(rechargeOffer: RechargePlan) {
//        view?.enterAmountString = "\(rechargeOffer.amountMax ?? 0)"
//        view?.resetQuickAmountFrom()
//        
//        interactor?.selectedPlan = rechargeOffer
//    }
//}
//extension RequestPresenterModule : InputNumberProtocol {
//    func userInput(entity: RechargeEntity) {
//        interactor?.rechargeEntity = entity
//        view?.enterAmountString = "\(entity.amount ?? 0 )"
//        view?.enterPhoneNumberString = entity.phone ?? ""
//        if(entity.operatorId == 0){
//            view?.shouldSelectOperator()
//        }else if let mbOperator = interactor?.mbOperators?.first(where: {$0.operatorId == entity.operatorId}){
//            view?.first5Plans  = interactor?.rechargePlans?.filter({$0.operatorShortName == mbOperator.operatorShortName}).enumerated().compactMap{ $0.offset < 5 ? $0.element : nil }
//        }
//        
//    }
//    
//    
//}
//
//
//extension RequestPresenterModule :  NumberConfimationProtocol{
//    func didConfirm(number entity: RechargeEntity) {
//        interactor?.rechargeEntity = entity
//        view?.shouldShowRechargeSummer()
//    }
//    
//    func addNewNumber(entity :RechargeEntity){
//        interactor?.rechargeEntity = nil
//        interactor?.selectedPlan = nil
//        interactor?.ConnectionType = nil
//        if let ee = UserSettings.sharedInstance.selectedNumbers.first(where: {$0.phone == entity.phone}){
//            debugPrint(entity.type ?? "")
//            //MARK : -SAME NUMBER CHECK AND ADDING TO ARRAY
//            if let index = UserSettings.sharedInstance.selectedNumbers.firstIndex(of: ee), let oldAmount = ee.amount, let newAmount = entity.amount{
//                if entity.type?.lowercased() == "postpaid"{
//                    let tAmount =  oldAmount +  newAmount
//                    if tAmount > 2501{
//                        UserSettings.sharedInstance.mainNav?.showToast(message: ten_to_two_thousand_five_for_postpaid[EasyUtils.getLanguageIndex()])
//                    }else{
//                        
//                        UserSettings.sharedInstance.selectedNumbers[index].amount = oldAmount +  newAmount
//                    }
//                }else if entity.type?.lowercased() == "prepaid" || entity.type?.lowercased() == "skitto"{
//                    let tAmount =  oldAmount +  newAmount
//                    if tAmount > 1001{
//                        UserSettings.sharedInstance.mainNav?.showToast(message: ten_to_thousand_for_prepaid[EasyUtils.getLanguageIndex()])
//                    }else{
//                        UserSettings.sharedInstance.selectedNumbers[index].amount = oldAmount +  newAmount
//                    }
//                }
//                
//            }
//            
//        }else{
//            UserSettings.sharedInstance.selectedNumbers.append(entity)
//        }
//        
//        view?.removeTextField()
//        prepareMostPopular()
//    }
//    func showAddedNumber(){
//        interactor?.rechargeEntity = nil
//        view?.shouldShowRechargeSummer()
//    }
//}
//
//extension RequestPresenterModule:QuickyDoneProtocol{
//    func didSelect(entities: [RechargeEntity]) {
//        
//        //interactor?.rechargeEntity = entities.first
//        UserSettings.sharedInstance.selectedNumbers = entities
//        view?.shouldShowRechargeSummer()
//    }
//}
