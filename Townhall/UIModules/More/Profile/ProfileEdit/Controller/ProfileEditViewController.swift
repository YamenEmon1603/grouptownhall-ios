//
//  ProfileEditViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 9/8/21.
//

import UIKit
import Foundation
import MobileCoreServices

class ProfileEditViewController:  EasyBaseViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    var keyboardObserver : KeyboardObserver!
    let imagePicker = UIImagePickerController()
    var selectedDateStr : String?
    lazy var mView : ProfileEditView = {
        let view = ProfileEditView()
        view.dobCallBack = dobAction
        view.navView.backButtonCallBack = backAction
        view.profilePhotoEditCallBack = photoEditAction
        view.saveUpdateCallBack = saveUpdate
        return view
    }()
    var data: UserData?{
        didSet{
            mView.viewFullName.text = data?.name
          
            mView.viewEmail.text = data?.email
            if let dateDob  = data?.dateOfBirth{
                mView.dobText.text = EasyUtils.shared.date(toTimestamp: dateDob, dateFormate: Constant.DateFormat15, expectedDateFormate: Constant.DateFormat11)
                mView.lblDobHeader.isHidden = false
                mView.lblDobHeader.textColor = .blueViolet
            }
            mView.addressText.text = data?.address
           
            mView.profileImageView.kf.setImage(with: URL(string: data?.profilePictureUrl ?? ""))
        }
    }
    
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        imagePicker.delegate = self
        
        mView.addressText.delegate = self    // Give TextViewMessage delegate Method
        
        mView.addressText.text = "address".easyLocalized()
        keyboardObserver = KeyboardObserver(for: self)
        fetchUserInfo()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        keyboardObserver.add()
       
        self.mView.viewFullName.resignFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func photoEditAction(){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = mView.profileEditButton
            alert.popoverPresentationController?.sourceRect = mView.profileEditButton.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            mView.profileImageView.contentMode = .scaleAspectFill
            mView.profileImageView.image = pickedImage
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func dobAction(){
        showScheduleAlert()
    }
    
    let scheduleAlert = DateSelectionView()
    func showScheduleAlert() {
        self.view.addSubview(scheduleAlert)
        scheduleAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        scheduleAlert.btnDoneCallBack = btnScheduleDoneAction
        scheduleAlert.datePicker.timeZone = NSTimeZone.local
        scheduleAlert.datePicker.datePickerMode = .date
        
        scheduleAlert.datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        scheduleAlert.datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
        scheduleAlert.datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
    }
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        //EEE, MMM d | HH:mm
        
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        
        print("Selected value \(selectedDate)")
        selectedDateStr = selectedDate
    }
    func btnScheduleDoneAction(){
        
        mView.dobText.text = selectedDateStr
        mView.lblDobHeader.isHidden = false
        mView.dobText.text = selectedDateStr
        scheduleAlert.removeFromSuperview()
    }
    func saveUpdate(){
        self.mView.viewFullName.endEditing(true)
        if let img = mView.profileImageView.image?.jpegData(compressionQuality: 0.7){
            if let imgUpload = UIImage(data: img) {
            uploadProfilePic(image: imgUpload)
            }
        }
        guard let fullName = mView.viewFullName.text else {
            showToast(message: "enter_name".easyLocalized())
            return
        }
        guard let email = mView.viewEmail.text else {
            showToast(message: "enter_email".easyLocalized())
            return
        }
        guard let dob = mView.dobText.text else {
            showToast(message: "enter_email".easyLocalized())
            return
        }
        guard let address = mView.addressText.text else {
            showToast(message: "enter_email".easyLocalized())
            return
        }
        if email.isEmail == false{
            showToast(message: "invalid_email_address".easyLocalized())
        }
        let date = EasyUtils.shared.date(toTimestamp: dob, dateFormate: Constant.DateFormat11, expectedDateFormate: Constant.DateFormat16)
        let info = ProfileEditRequest(name: fullName, email: email, address: address, dob: date)
        profileEditRequest(info: info)
        
    }
    
}

extension ProfileEditViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        var contentInset:UIEdgeInsets = mView.scrollView.contentInset
        contentInset.bottom = height + 20
        mView.scrollView.contentInset = contentInset
       
    }
    
    func keybaordWillHide() {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        mView.scrollView.contentInset = contentInset
    }
    
    
}
extension ProfileEditViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        mView.scrollView.setContentOffset(mView.viewCustomAddress.frame.origin, animated: true)
        if !mView.addressText.text!.isEmpty && mView.addressText.text! == "address".easyLocalized() {
            mView.addressText.text = ""
            mView.lblAddressHeader.isHidden = false
            mView.lblAddressHeader.textColor = .blueViolet
            mView.addressText.textColor = .black
           
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if mView.addressText.text.isEmpty {
            mView.addressText.textColor = .slateGrey
            mView.addressText.text = "address".easyLocalized()
            mView.lblAddressHeader.isHidden = true
            mView.lblAddressHeader.textColor = .slateGrey
        }else{
            mView.lblAddressHeader.isHidden = false
            mView.lblAddressHeader.textColor = .blueViolet
        }
    }
}


extension ProfileEditViewController{
    func fetchUserInfo(){
        WebServiceHandler.shared.getProfileDetails(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    EasyDataStore.sharedInstance.user = response.data
                    self.data = response.data
                
                   
                }else{
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let error):
                
                self.showToast(message: error.localizedDescription)
                
            }
        }, shouldShowLoader: true)
    }
}
//
extension ProfileEditViewController{
    func profileEditRequest(info:ProfileEditRequest) {
       
        WebServiceHandler.shared.editProfile(info: info, completion: { (result) in
            switch result{
            case .success(let response):
                debugPrint(response)
                if response.code == 200{
                    EasyDataStore.sharedInstance.user = response.data
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "profileUpdate"), object: self)
                   
                    self.showToast(message: response.message ?? "")
                    

                }else{
                    self.showToast(message: response.message ?? "")
                }
                break
            case .failure(let error_):
                self.showToast(message: error_.localizedDescription)
                break
            }
        }, shouldShowLoader: true)
        

    }
    
    func uploadProfilePic(image: UIImage) {

            // Use resizedImage

            if   let imageData = image.jpegData(compressionQuality: 0.25)//UIImageJPEGRepresentation(tempImage, 0.7)
            {

                WebServiceHandler.shared.uploadProfilePic(imageData: imageData, completion: { (result) in
                    switch result{
                    case .success(let response):
                        if response.code == 200{
                            EasyManager.dataStore.user = response.data
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "profileUpdate"), object: self)
                            //self.presenter?.view?.editPorfileResult(responseMsg:response.message ?? "")

                        }else{

                            self.showToast(message:  response.message ?? "")
                        }
                    case .failure(let error):

                        self.showToast(message:  error.localizedDescription)
                    }

                }, shouldShowLoader: true)
            }

    }
}
