//
//  ProfileEditView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 9/8/21.
//

import Foundation
import UIKit


class ProfileEditView : EasyBaseView, ButtonAccessoryViewDelegate {
    func tapAction(_ sender: ButtonAccessoryView) {
        saveUpdateCallBack?()
    }
    
    var backCallBack : (()->Void)?
    var saveUpdateCallBack : (()->Void)?
    var  dobCallBack : (()->Void)?
    var profilePhotoEditCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        //containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0, bottom: 0)
        //containerView.addSubview(topView)
        containerView.addSubview(btnSaveUpdate)
        containerView.addSubview(navView)
        containerView.addSubview(formView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 10, heightMultiplier: 0.1)
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        btnSaveUpdate.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 0).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnSaveUpdate.topAnchor,constant: 0).isActive = true
        
    }
    
    
    
    lazy var topView: UIView = {
        let topView = UIView()
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        topView.backgroundColor = .white
        topView.addSubview(profileImageView)
        topView.addSubview(profileEditButton)
        profileEditButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        profileEditButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        profileEditButton.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: -30).isActive = true
        profileEditButton.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: -30).isActive = true
        
        
        
        
        profileImageView.addAnchorToSuperview(heightMultiplier: 0.7, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
        return topView
    }()
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0 ,top:0,bottom: 0)
        
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        
        
        scrollView.addSubview(holderView)
        scrollView.addSubview(topView)
        
        topView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0)
        topView.heightAnchor.constraint(equalTo: formView.heightAnchor, multiplier: 0.19).isActive = true
        
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        holderView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
        
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
        
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8*Constant.factY
        holderView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 15, trailing: -15, top:  8*Constant.factY, bottom: 0)
        
        stackView.addArrangedSubview(viewFullName)
        viewFullName.heightAnchor.constraint(equalToConstant: 60).isActive = true
        stackView.addArrangedSubview(viewEmail)
        viewEmail.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        stackView.addArrangedSubview(viewCustomDob)
        viewCustomDob.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        stackView.addArrangedSubview(viewCustomAddress)
        viewCustomAddress.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        
        
        return formView
    }()
    
    
    
    //MARK: InputViews
    
    //MARK: COMPONENTS
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "edit_profile".easyLocalized())
        navView.backgroundColor = .white
        return navView
    }()
    
    
    let profileImageView : UIImageView = {
        let view = UIImageView()
        view.layer.borderWidth = 1
        view.backgroundColor = .slateGrey
        view.layer.borderColor = UIColor.white.cgColor
        DispatchQueue.main.async {
            view.layer.cornerRadius = view.frame.size.height/2
            view.clipsToBounds = true
        }
     
        
        view.contentMode = .scaleAspectFill
        view.image = UIImage(named: "spiderman")
        return view
    }()
    
    
    private let btnSaveUpdate:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
       
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "save_update".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnSaveUpdateAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnSaveUpdateAction(_ sender:UIButton){
        saveUpdateCallBack?()
    }
    
    
    
    lazy var viewFullName : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "full_name".easyLocalized())
        textView.shadow = false
        textView.accessoryView = ButtonAccessoryView(title: "save_update".easyLocalized(), delegate: self)
        return textView
    }()
    
    lazy var viewEmail : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "email".easyLocalized())
        textView.shadow = false
        textView.accessoryView = ButtonAccessoryView(title: "save_update".easyLocalized(), delegate: self)
        return textView
    }()
    
    
    
    
    let lblDobHeader : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "date_of_birth".easyLocalized()
        label.font = .EasyFont(ofSize: 11, style: .medium)
        label.textColor = .blueViolet
        return label
    }()
    
    lazy var viewCustomDob : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .Easywhite
        view.layer.cornerRadius = 10
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        view.layer.borderWidth = 1
        
        view.addSubview(lblDobHeader)
        view.addSubview(dobText)
        view.addSubview(dobImageView)
        view.addSubview(dobButton)
        lblDobHeader.addAnchorToSuperview(leading: 12, trailing: -8, top: 8)
        
        lblDobHeader.bottomAnchor.constraint(equalTo: dobText.topAnchor, constant: -2).isActive = true
        lblDobHeader.isHidden = true
        dobText.addAnchorToSuperview(leading: 12,bottom: -4)
        
        dobText.trailingAnchor.constraint(equalTo: dobImageView.leadingAnchor, constant: 0).isActive = true
        dobImageView.addAnchorToSuperview(trailing: -15,centeredVertically: 0, heightWidthRatio: 1)
        
        dobButton.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,bottom: 0)
        
        return view
    }()
    
    lazy var dobText : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.placeholder = "date_of_birth".easyLocalized()
        view.font = .EasyFont(ofSize: 18, style: .medium)
        return view
    }()
    
    let dobImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "calender")
        return view
    }()
    
    lazy var dobButton : UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clear
        btnProceed.addTarget(self, action: #selector(btnDateAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    lazy var profileEditButton: UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "photoEdit")
        button.setImage(image, for: .normal)
        button.layer.borderWidth = 1
        button.backgroundColor = .slateGrey
        button.layer.borderColor = UIColor.white.cgColor
        DispatchQueue.main.async {
            button.layer.cornerRadius = button.frame.size.height/2
            button.clipsToBounds = true
        }
        button.tintColor = .white
        
        button.addTarget(self, action: #selector(profilePhotoEditAction(_:)), for: .touchUpInside)
        return button
    }()
    
    ///
    let lblAddressHeader : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "address".easyLocalized()
        label.font = .EasyFont(ofSize: 11, style: .medium)
        label.textColor = .blueViolet
        return label
    }()
    lazy var viewCustomAddress : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .Easywhite
        view.layer.cornerRadius = 10
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        view.layer.borderWidth = 1
        
        view.addSubview(lblAddressHeader)
        view.addSubview(addressText)
        lblAddressHeader.addAnchorToSuperview(leading: 12, trailing: -8, top: 4)
        
        lblAddressHeader.bottomAnchor.constraint(equalTo: addressText.topAnchor, constant: -2).isActive = true
       // lblAddressHeader.isHidden = true
        addressText.addAnchorToSuperview(leading: 12,trailing: -12,bottom: -12,heightMultiplier: 0.7)
        
        return view
    }()
    
    lazy var addressText : UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .Easywhite
        view.font = .EasyFont(ofSize: 18, style: .medium)
        view.textColor = .black
        view.inputAccessoryView = ButtonAccessoryView(title: "save_update".easyLocalized(), delegate: self)
        return view
    }()
    
    //
    @objc func profilePhotoEditAction(_ sender:UIButton){
        profilePhotoEditCallBack?()
    }
    
    @objc func btnDateAction(_ sender:UIButton){
        dobCallBack?()
    }
    
}




