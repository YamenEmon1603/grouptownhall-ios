//
//  ProfileInfoViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/8/21.
//

import UIKit

class ProfileInfoViewController: EasyBaseViewController {
    lazy var mView : ProfileInfoView = {
        let mView = ProfileInfoView()
        mView.backCallBack = backAction
        mView.profileEditCallBack = profileEdit
        return mView
    }()
    
    var data: UserData?{
        didSet{
            mView.lblUserName.text = data?.name
            mView.mobileNumberView.lblValue.text = data?.mobile
            mView.emailView.lblValue.text = data?.email
            if let dateDob  = data?.dateOfBirth{
                mView.dateOfBirthView.lblValue.text = EasyUtils.shared.date(toTimestamp: dateDob, dateFormate: Constant.DateFormat15, expectedDateFormate: Constant.DateFormat11)
            }
            mView.addressView.lblValue.text = data?.address
            if let dateSince = data?.memberSince{
                mView.memberSinceView.lblValue.text = EasyUtils.shared.date(toTimestamp: dateSince, dateFormate: Constant.DateFormat3, expectedDateFormate: Constant.DateFormat11)
            }
            mView.profileImageView.kf.setImage(with: URL(string: data?.profilePictureUrl ?? ""))
        }
    }

    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateTriggered), name: NSNotification.Name(rawValue: "profileUpdate"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        fetchUserInfo()
    }
    @objc func updateTriggered(){
        fetchUserInfo()
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
    func profileEdit(){
        
        let vc  =  ProfileEditViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
//MARK: — API CALL

extension ProfileInfoViewController{
    func fetchUserInfo(){
        WebServiceHandler.shared.getProfileDetails(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    EasyDataStore.sharedInstance.user = response.data
                    self.data = response.data
                   
                   
                }else{
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let error):
               
                self.showToast(message: error.localizedDescription)
                
            }
        }, shouldShowLoader: true)
    }
}
