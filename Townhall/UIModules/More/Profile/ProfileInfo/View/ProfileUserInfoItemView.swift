//
//  ProfileUserInfoView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/8/21.
//


import Foundation
import UIKit

class ProfileUserInfoItemView: UIView {
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    convenience init(title:String,value : String) {
        self.init(frame:.zero)
        backgroundColor = .white
        let _ = addBorder(side: .bottom, color: .paleLilac, width: 1)
        addSubview(stackView)
       
        
        stackView.addAnchorToSuperview(leading: 15, trailing: -10, top: 10,bottom: -10)
      
        stackView.addArrangedSubview(lblHeader)
        stackView.addArrangedSubview(lblValue)
        
        lblHeader.text = title
        lblValue.text = value
    
    }
   
  
    private let stackView:UIStackView = {
      let stackView = UIStackView()
        stackView.axis = .vertical
        
        stackView.spacing = 6
        return stackView
    }()
    lazy var lblHeader: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .slateGrey
        lbl.font = .EasyFont(ofSize: 11, style: .regular)
        lbl.textAlignment = .left
        lbl.text = ""
        return lbl
    }()
    lazy var lblValue: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = .EasyFont(ofSize: 14, style: .medium)
        lbl.textAlignment = .left
        lbl.text = ""
        return lbl
    }()
   



}
