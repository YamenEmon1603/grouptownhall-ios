//
//  ProfileInfoView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/8/21.
//

import Foundation
import UIKit


class ProfileInfoView : EasyBaseView {
    var backCallBack : (()->Void)?
    var profileEditCallBack : (()->Void)?
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0, bottom: 0)
        containerView.addSubview(topView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.35)
    
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: topView.bottomAnchor,constant: 0).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
      
    }
   
   
    
    lazy var topView: UIView = {
        let topView = GradientView()
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        topView.startColor = .realBlue
        topView.endColor = .blueViolet
        topView.horizontalMode = true
        topView.addSubview(backButton)
        topView.addSubview(profileEditButton)
        topView.addSubview(profileImageView)
        topView.addSubview(lblUserName)
        
        
        backButton.addAnchorToSuperview(leading: 0, top: 30, widthMutiplier: 0.15, heightWidthRatio: 0.8)
        topView.addSubview(profileEditButton)
        profileEditButton.addAnchorToSuperview(trailing: 0, top: 30, widthMutiplier: 0.15, heightWidthRatio: 0.8)
        
        profileImageView.addAnchorToSuperview(heightMultiplier: 0.4, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
        lblUserName.addAnchorToSuperview(leading: 20, trailing: -20)
        lblUserName.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 13).isActive = true
        lblUserName.bottomAnchor.constraint(greaterThanOrEqualTo: topView.bottomAnchor, constant: -20).isActive = true
        
   
       
        
        return topView
    }()
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        
       
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        formView.addSubview(headerView)
        headerView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        headerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        scrollView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 0).isActive = true
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
       
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
       
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 0
        holderView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
       stackView.addArrangedSubview(mobileNumberView)
       
        stackView.addArrangedSubview(emailView)
      
         
        stackView.addArrangedSubview(dateOfBirthView)
     
        
        stackView.addArrangedSubview(addressView)
       
        
        stackView.addArrangedSubview(memberSinceView)
    
        return formView
    }()
    
  
    
    //MARK: InputViews
    
    //MARK: COMPONENTS
    lazy var backButton: UIButton = {
        let  backButton = UIButton()
        backButton.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "backArrow")
        backButton.setImage(image, for: .normal)
      
        backButton.tintColor = .white
        backButton.addTarget(self, action: #selector(backBtnAction(_:)), for: .touchUpInside)
        return backButton
    }()
    @objc func backBtnAction(_ sender:UIButton){
        backCallBack?()
    }
    lazy var profileEditButton: UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "profileEdit")
        button.setImage(image, for: .normal)
      
        button.tintColor = .white
        
        button.addTarget(self, action: #selector(profileEditAction(_:)), for: .touchUpInside)
        return button
    }()
    @objc func profileEditAction(_ sender:UIButton){
        profileEditCallBack?()
    }
    let profileImageView : UIImageView = {
        let view = UIImageView()
        view.layer.borderWidth = 1
        view.backgroundColor = .white
        view.layer.borderColor = UIColor.white.cgColor
        DispatchQueue.main.async {
            view.layer.cornerRadius = view.frame.size.height/2
            view.clipsToBounds = true
        }
       
        view.contentMode = .scaleAspectFill
        view.image = UIImage(named: "")
        return view
    }()
   
    lazy var lblUserName: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = .EasyFont(ofSize: 20, style: .medium)
        lbl.textAlignment = .center
        lbl.text = ""
        return lbl
    }()
    lazy var headerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .Easywhite
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.paleLilac.cgColor
      
        var  lbl = UILabel()
        lbl.text = "profile_info".easyLocalized().uppercased()
        lbl.font = .EasyFont(ofSize: 10, style: .medium)
        lbl.textColor = .battleshipGrey
        view.addSubview(lbl)
       
        lbl.addAnchorToSuperview(leading: 10,trailing: -10,top: 4, bottom: -4, heightMultiplier: 0.5, centeredVertically: 0)
        
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        
        return view
    }()
    
    lazy var mobileNumberView : ProfileUserInfoItemView = {
        let view = ProfileUserInfoItemView(title: "mobile_number".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    lazy var emailView : ProfileUserInfoItemView = {
        let view = ProfileUserInfoItemView(title: "email".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    lazy var dateOfBirthView : ProfileUserInfoItemView = {
        let view = ProfileUserInfoItemView(title: "date_of_birth".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    lazy var addressView : ProfileUserInfoItemView = {
        let view = ProfileUserInfoItemView(title: "address".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    
    lazy var memberSinceView : ProfileUserInfoItemView = {
        let view = ProfileUserInfoItemView(title: "member_since".easyLocalized(), value: "Nil")
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()

}




