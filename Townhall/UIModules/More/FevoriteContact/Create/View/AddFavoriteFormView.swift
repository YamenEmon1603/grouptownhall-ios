//
//  AddFavoriteFormView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit

class AddFavoriteFormView: EasyBaseView, ButtonAccessoryViewDelegate {
    var proceedCallBack : (()->Void)?
    var contactCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        
        addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 8).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true

    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"add_new_favourite_Contact".easyLocalized().uppercased())
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleGrey, width: 0.5)
        return navView
    }()
    let scrollView = UIScrollView()
    lazy var formView: UIView = {
        let formView = UIView()
        formView.backgroundColor = .white
        formView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
       
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
       
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 15
      
        holderView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 20, trailing: -20,top: 20, bottom: 0)
        
      
        
        stackView.addArrangedSubview(numberInputView)
        numberInputView.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        stackView.addArrangedSubview(selectOperatorView)
        selectOperatorView.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
       stackView.addArrangedSubview(nickNameView)
        nickNameView.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
      
        return formView
    }()

    lazy var numberInputView: UIView = {
        let view = UIView()
        view.backgroundColor = .Easywhite
        view.addSubview(numberTextView)
        numberTextView.addAnchorToSuperview(leading: 0,  top: 0, bottom: 0)
        view.layer.cornerRadius = 10
        view.layer.borderWidth  = 1
        view.layer.borderColor = UIColor.paleGrey.cgColor
        
        let separatorView = UIView()
        separatorView.backgroundColor = .paleLilac
        view.addSubview(separatorView)
        
        separatorView.addAnchorToSuperview( top: 10, bottom: -10)
        numberTextView.trailingAnchor.constraint(equalTo: separatorView.leadingAnchor, constant: 0).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        
        view.addSubview(phoneBookButton)
        phoneBookButton.addAnchorToSuperview(trailing: -10,heightMultiplier: 0.6, centeredVertically: 0,heightWidthRatio: 1)
        phoneBookButton.leadingAnchor.constraint(equalTo: separatorView.trailingAnchor, constant: 10).isActive = true
        
        
        return view
    }()
    lazy var  numberTextView : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_number".easyLocalized(),placeHolderColor: .slateGrey)
        textView.keyBoardType = .phonePad
        textView.backgroundColor = .clear
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self )
       
        return textView
    }()
    lazy var phoneBookButton : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setImage(UIImage(named: "phoneBookIcon"), for: .normal)
        
        view.addTarget(self, action: #selector(contactAction(_:)), for: .touchUpInside)
        return view
    }()
    @objc func contactAction(_ sender:UIButton){
        contactCallBack?()
    }
    lazy var selectOperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .Easywhite
        view.addSubview(selectOperatorTextView)
        selectOperatorTextView.addAnchorToSuperview(leading: 0,  top: 0, bottom: 0)
        view.layer.cornerRadius = 10
        view.layer.borderWidth  = 1
        view.layer.borderColor = UIColor.paleGrey.cgColor
        view.addSubview(selectOperatorButton)
        selectOperatorButton.addAnchorToSuperview(trailing: -20,heightMultiplier: 0.2, centeredVertically: 0,heightWidthRatio: 1)
        selectOperatorTextView.trailingAnchor.constraint(equalTo: selectOperatorButton.leadingAnchor, constant: -10).isActive = true
        return view
    }()
    lazy var selectOperatorTextView : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "select_operator".easyLocalized(),placeHolderColor: .slateGrey)
        textView.keyBoardType = .phonePad
        textView.backgroundColor = .clear
        textView.isEnabled = false
        return textView
    }()
    lazy var selectOperatorButton : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setImage(UIImage(named: "arrow-point-to-right"), for: .normal)
        return view
    }()
    lazy var nickNameView: UIView = {
        let view = UIView()
        view.backgroundColor = .Easywhite
        view.addSubview(nickNameTextView)
        nickNameTextView.addAnchorToSuperview(leading: 0, trailing: -10, top: 0, bottom: 0)
        view.layer.cornerRadius = 10
        view.layer.borderWidth  = 1
        view.layer.borderColor = UIColor.paleGrey.cgColor
        
        return view
    }()
    lazy var nickNameTextView : EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "enter_nickname".easyLocalized(),placeHolderColor: .slateGrey)
        textView.backgroundColor = .clear
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
       
        return textView
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
}
