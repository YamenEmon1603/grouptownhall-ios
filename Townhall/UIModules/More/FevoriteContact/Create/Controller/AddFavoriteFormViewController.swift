//
//  AddFavoriteFormView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit
import ContactsUI
enum FavouriteContact{
    case Add
    case Update
}

class AddFavoriteFormViewController: EasyBaseViewController {
    var contact: FevoriteContact?{
        didSet{
            self.mView.numberTextView.text = contact?.phone
            self.mView.nickNameTextView.text = contact?.phone
            mView.selectOperatorTextView.text = contact?._operator?.localisedName
        }
    }
    
    lazy var mView : AddFavoriteFormView = {
        let mView = AddFavoriteFormView()
        mView.navView.backButtonCallBack = backButtonAction
        mView.proceedCallBack = proceedButtonAction
        let operatorSelectionTap = UITapGestureRecognizer(target: self, action: #selector(selectOperatorAction))
        mView.selectOperatorView.addGestureRecognizer(operatorSelectionTap)
        mView.contactCallBack = contactPickAction
        return mView
    }()
    var keyboardObserver : KeyboardObserver!
    var selectedConnectionType : ConectionType?
    var selectedOperratorId : Int?
    var favContact : FavouriteContact?
    
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        keyboardObserver = KeyboardObserver(for: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
    }
    
    func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func proceedButtonAction(){
        guard let phone =  mView.numberTextView.text, phone.isEmpty == false else {
            showToast(message: "enter_number".easyLocalized())
            return
        }
        guard let oper =  selectedOperratorId else {
            showToast(message: "select_operator".easyLocalized())
            return
        }
        guard let name =  mView.nickNameTextView.text, name.isEmpty == false else {
            showToast(message: "enter_nickname".easyLocalized())
            return
        }
        guard let connection =  selectedConnectionType?.value else {
            showToast(message: "select_connection".easyLocalized())
            return
        }
   
    }
    @objc func selectOperatorAction(){
        OperatorSelectionViewController.show(on: self, delegate: self)
    }
    func contactPickAction(){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
}
extension AddFavoriteFormViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        var contentInset:UIEdgeInsets = mView.scrollView.contentInset
        contentInset.bottom = height + 20
        mView.scrollView.contentInset = contentInset
    }
    
    func keybaordWillHide() {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        mView.scrollView.contentInset = contentInset
    }
    
    
}
extension AddFavoriteFormViewController:OperatorSelectionDelegate{
    func didSelect(operatorObject: MBOperator, connectionType: ConectionType) {
        mView.selectOperatorTextView.text = operatorObject.localisedName
        selectedConnectionType = connectionType
        selectedOperratorId = operatorObject.operatorId
    }
    
}
extension AddFavoriteFormViewController : CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {

    }
    func selectionDone(numberString: String,nameString:String){
        
        self.dismiss(animated: true) {
            self.mView.numberTextView.text = numberString
            self.mView.nickNameTextView.text = nameString
        }
        
    }
    
}

//MARK: — Easy FAVOURITE CONTACTS
extension AddFavoriteFormViewController {
   
}
