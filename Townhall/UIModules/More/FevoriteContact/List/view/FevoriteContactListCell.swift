//
//  FevoriteContactListCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit
import Kingfisher

class FevoriteContactListCell: UITableViewCell {
    
    static let identifier = "FevoriteContactListCell"
    
    var contact : FevoriteContact?{
        didSet{
            nameLabel.text = contact?.name
            numberLabel.text =  EasyUtils.shared.getLocalizedDigits(contact?.phone)//contact?.phone?
            if let imgUrl =  EasyManager.dataStore.easyMobileOperators.first(where: {$0.operatorId == contact?.operatorId})?.imageUrl, imgUrl.isEmpty == false{
                logoView.kf.setImage(with: URL(string:imgUrl))
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        contentView.addSubview(logoView)
        logoView.addAnchorToSuperview(leading: 20, widthMutiplier:  0.1, centeredVertically: 0,heightWidthRatio: 1)
        contentView.addSubview(nameNumberStack)
        nameNumberStack.addAnchorToSuperview( top: 10, bottom: -10)
        nameNumberStack.leadingAnchor.constraint(equalTo: logoView.trailingAnchor,constant: 10).isActive = true
        let buttonStack = UIStackView()
        buttonStack.axis = .horizontal
        buttonStack.distribution = .fillEqually
        buttonStack.spacing = 13
        buttonStack.addArrangedSubview(editBtn)
        buttonStack.addArrangedSubview(deleteBtn)
        
        contentView.addSubview(buttonStack)
        buttonStack.addAnchorToSuperview( trailing: -20,widthMutiplier: 0.2)
        buttonStack.topAnchor.constraint(equalTo: nameNumberStack.topAnchor, constant: 0).isActive = true
        buttonStack.bottomAnchor.constraint(equalTo: nameNumberStack.bottomAnchor, constant: 0).isActive = true
        nameNumberStack.trailingAnchor.constraint(equalTo: buttonStack.leadingAnchor, constant: -10).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
        logoView.image = UIImage(named: "gp")
        return logoView
    }()
    lazy var  nameNumberStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 5
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        nameNumberStack.addArrangedSubview(nameLabel)
        nameNumberStack.addArrangedSubview(numberLabel)
        return nameNumberStack
    }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.text = "Fasi Uddin Raihan"
        return label
    }()
    let numberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.textColor = .battleshipGrey
        label.text = "Mobile Recharge"
        return label
    }()
    let editBtn:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "favedit"), for: .normal)
       
        return button
    }()
    let deleteBtn:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "crossred"), for: .normal)
       
        return button
    }()
}
