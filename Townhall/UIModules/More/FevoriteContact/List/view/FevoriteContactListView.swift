//
//  FevoriteContactListView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit

class FevoriteContactListView: EasyBaseView {

    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
        tableView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"favourite_contacts".easyLocalized().easyLocalized(),rightItem: addButton)
        navView.backgroundColor = .white
        return navView
    }()
    lazy var tableView : UITableView = {
        let tableView = UITableView()
       tableView.register(FevoriteContactListCell.self, forCellReuseIdentifier: FevoriteContactListCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView()
        
        return tableView
    }()
    let addButton : UIButton = {
        let button = UIButton()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.blueViolet.cgColor
        button.backgroundColor = .blueViolet
        button.layer.cornerRadius = 6
        button.setTitle("add_new_".easyLocalized().uppercased(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 11.0, style: .bold)
 
        return button
    }()

}
