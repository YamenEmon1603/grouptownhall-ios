//
//  FevContactListViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit

class FevContactListViewController: EasyBaseViewController {
    var contacts: [FevoriteContact]?{
        didSet{
            mView.tableView.reloadData()
        }
    }
    
    lazy var mView : FevoriteContactListView = {
        let mView = FevoriteContactListView()
        mView.navView.backButtonCallBack = backAction
        mView.addButton.addTarget(self, action: #selector(addNewButtonAction(_:)), for: .touchUpInside)
        mView.tableView.delegate = self
        mView.tableView.dataSource = self
        return mView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view = mView
      
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        getFavoriteConatcts()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
    @objc func addNewButtonAction(_ sender : UIButton){
        let vc = AddFavoriteFormViewController()
        vc.favContact = .Add
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func editButtonAction(_ sender : UIButton){
        let vc = AddFavoriteFormViewController()
        if let contact = contacts?[sender.tag]{
            vc.contact = contact
            vc.favContact = .Update
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func amountCrossBtnAction(_ sender : UIButton){
        if let contact = contacts?[sender.tag].phone{
            deleteFav(phone: contact)
        }
    }
    
}
extension FevContactListViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FevoriteContactListCell.identifier, for: indexPath) as!  FevoriteContactListCell
        cell.contact = contacts?[indexPath.row]
     
        cell.deleteBtn.addTarget(self, action: #selector(amountCrossBtnAction(_:)), for: .touchUpInside)
        cell.editBtn.addTarget(self, action: #selector(editButtonAction(_:)), for: .touchUpInside) //addNewButtonAction
       cell.editBtn.tag = indexPath.row
       cell.deleteBtn.tag = indexPath.row
     
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = EasyPaddingLabel()
        label.text = "my_favouriter_contact_list".easyLocalized().uppercased()
        return label
    }
    
    
    
}
extension FevContactListViewController{
    
    func getFavoriteConatcts() {
   
    }

    
    func deleteFav(phone: String) {
       
    }

}

