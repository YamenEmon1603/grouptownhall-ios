//
//  ChangePasswordViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/8/21.
//

import UIKit

class ChangePasswordViewController :  EasyBaseViewController {
    
    lazy var mView: ChangePasswordView = {
        let view = ChangePasswordView()
        view.proceedCallBack = proceed
        view.navView.backButtonCallBack = backAction
        view.eye1CallBack = passWord1Hide
        view.eye2CallBack = passWord2Hide
        view.eye3CallBack = passWord3Hide
        return view
    }()
    override func viewDidLoad() {
        self.view = mView
        
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
    }
    func passWord1Hide() {
        if mView.btn1Eye.isSelected == true{
            mView.btn1Eye.isSelected.toggle()
            mView.btn1Eye.setImage(UIImage(named: "eyeclose"), for: .normal)
            mView.viewEnterPassword.isSecureText = true
        }else{
            mView.btn1Eye.isSelected.toggle()
            mView.btn1Eye.setImage(UIImage(named: "eyeopen"), for: .normal)
            mView.viewEnterPassword.isSecureText = false
        }
    }
    func passWord2Hide() {
        if mView.btn2Eye.isSelected == true{
            mView.btn2Eye.isSelected.toggle()
            mView.btn2Eye.setImage(UIImage(named: "eyeclose"), for: .normal)
            mView.viewCurrentPassword.isSecureText = true
        }else{
            mView.btn2Eye.isSelected.toggle()
            mView.btn2Eye.setImage(UIImage(named: "eyeopen"), for: .normal)
            mView.viewCurrentPassword.isSecureText = false
        }
    }
    func passWord3Hide() {
        if mView.btn3Eye.isSelected == true{
            mView.btn3Eye.isSelected.toggle()
            mView.btn3Eye.setImage(UIImage(named: "eyeclose"), for: .normal)
            mView.viewRetypePassword.isSecureText = true
        }else{
            mView.btn3Eye.isSelected.toggle()
            mView.btn3Eye.setImage(UIImage(named: "eyeopen"), for: .normal)
            mView.viewRetypePassword.isSecureText = false
        }
    }
    
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
    func proceed()  {
        self.dismissKeyboard()
       
        guard let oldPass = mView.viewCurrentPassword.text,oldPass.isEmpty == false else {
            showToast(message: "Enter old password")
            return
        }
        guard let newPass = mView.viewEnterPassword.text,newPass.isEmpty == false else {
            showToast(message: "Enter new password")
            return
        }
        guard let retypePass = mView.viewRetypePassword.text,retypePass.isEmpty == false else {
            showToast(message: "Retype new password")
            return
        }
        if newPass == retypePass{
            resetPassWord(oldPass: oldPass, newPass: newPass, retypePass: retypePass)
        }else{
            showToast(message: "Password not matched")
        }
    }
    
}

//MARK: — API CALL

extension ChangePasswordViewController{
    func resetPassWord(oldPass: String, newPass: String, retypePass: String) {
        let params : [String:Any] = ["old_password":oldPass,"password":newPass,"password_confirmation":retypePass]
        WebServiceHandler.shared.resetPassword(params, completion: { (result) in
          
            switch result{
            case .success(let response):
                if response.code == 200{
                    EasyUtils.shared.logoutSingleDevice(message: response.message ?? "")
                }else{
                    self.showToast(message: response.message ?? "")
                }
                
                
            case .failure(let err):
                self.showToast(message: err.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
}
