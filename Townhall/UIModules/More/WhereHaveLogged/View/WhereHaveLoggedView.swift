//
//  WhereHaveLoggedView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 10/8/21.
//

import Foundation
import UIKit

class WhereHaveLoggedView : EasyBaseView, ButtonAccessoryViewDelegate {
    func tapAction(_ sender: ButtonAccessoryView) {
        signOutAllCallBack?()
    }
    
    var backCallBack : (()->Void)?
    var signOutAllCallBack : (()->Void)?
   
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        //containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0, bottom: 0)
        containerView.addSubview(navView)
        containerView.addSubview(btnSignOutAll)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 25, heightMultiplier: 0.07)
        btnSignOutAll.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
    
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 8).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnSignOutAll.topAnchor,constant: -8).isActive = true
        
    }
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.backgroundColor = .yellow
        formView.addSubview(signInListTable)
        
        signInListTable.addAnchorToSuperview(leading: 0, trailing: 0 ,top:0,bottom: 0)
        
        
       
        
        
        return formView
    }()
    
    
    
    //MARK: InputViews
    
    //MARK: COMPONENTS
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "where_youre_logged_in".easyLocalized().uppercased())
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 1)
        return navView
    }()
    
   
    
    
    private let btnSignOutAll:UIButton = {
        let button = UIButton()
        button.backgroundColor = .blueViolet
        button.setTitle("Log_out_from_all_devices".easyLocalized().uppercased(), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(btnSignOutAllAction(_:)), for: .touchUpInside)
        return button
    }()
    @objc func btnSignOutAllAction(_ sender:UIButton){
        signOutAllCallBack?()
    }
    
    lazy var signInListTable: UITableView = {
        let table = UITableView(frame: .zero)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        table.backgroundColor = .white
        table.register(SignInListTableViewCell.self, forCellReuseIdentifier: SignInListTableViewCell.identifier)
        return table
    }()


}




