//
//  WhereHaveLoggedViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 10/8/21.
//

import UIKit

class WhereHaveLoggedViewController : EasyBaseViewController{
    let bottomAlert = HorizontalButtonAlertView(header: "you_can_logout_of_this_session".easyLocalized(), subHeader: "if_it_is_not_you".easyLocalized(), btn1Name: "cancel".easyLocalized(), btn2Name: "log_out".easyLocalized())
    
    let bottomAllSignOutAlert = HorizontalButtonAlertView(header: "are_you_sure_to_sign_out".easyLocalized(), subHeader: "", btn1Name: "cancel".easyLocalized(), btn2Name: "sign_out".easyLocalized())
    
    lazy var mView : WhereHaveLoggedView = {
        let view = WhereHaveLoggedView()
        view.navView.backButtonCallBack = backAction
        view.signOutAllCallBack = signOutFromAllAction
        return view
    }()
    
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        mView.signInListTable.dataSource = self
        mView.signInListTable.delegate = self
        getLoggedIndevices()
        
    }
    var userDevices: [UserDevice]?{
        didSet{
            mView.signInListTable.reloadData()
            if let index = userDevices?.firstIndex(where: {$0.sessionKey == EasyDataStore.sharedInstance.deviceKey}){
                userDevices?.swapAt(index, 0)
                mView.signInListTable.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func showSignOutFromAllAlert(){
        self.view.addSubview(bottomAllSignOutAlert)
        bottomAllSignOutAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        bottomAllSignOutAlert.btnSecondCallBack =  signOutAll
        bottomAllSignOutAlert.btnFirstCallBack = signOutAllFirstAction
    }
    func signOutFromAllAction(){
        showSignOutFromAllAlert()
    }
    
    func signOutAll(){
        logoutFromAllDevice()
    }
    func signOutAllFirstAction(){
        bottomAllSignOutAlert.removeFromSuperview()
    }
    
    
    func showBottomAlert(index:Int) {
        if let key = userDevices?[index].sessionKey{
            self.view.addSubview(bottomAlert)
            bottomAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
            bottomAlert.btnSecondCallBack = {
                self.logoutWithSessionKey(sessionKey: key)
            }
            bottomAlert.btnFirstCallBack = btnFirstAction
        }
        
    }
   
    func btnFirstAction(){
       // (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        bottomAlert.removeFromSuperview()
    }
    
    @objc func signOutCurrentAction(_ sender:UIButton){
        
        showBottomAlert(index: sender.tag)
       
    }
    
}
extension WhereHaveLoggedViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDevices?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SignInListTableViewCell.identifier, for: indexPath) as! SignInListTableViewCell
        cell.userDevice = userDevices?[indexPath.row]
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(signOutCurrentAction(_:)), for: .touchUpInside)
        //cell.projectMileStone = filteredProjectMilestone?[indexPath.row]
        //cell.contentView.isUserInteractionEnabled = false
        return cell
    }
    
    
    
}


//MARK: — API CALL

extension WhereHaveLoggedViewController{
    func logoutWithSessionKey(sessionKey: String) {
        let params : [String:Any] = ["session_key":sessionKey]
        WebServiceHandler.shared.logoutWithParam(params, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    self.bottomAlert.removeFromSuperview()
                    EasyUtils.shared.logoutSingleDevice()
                }else{
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let err):
                self.showToast(message: err.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    //logoutFromAllDevice
    func logoutFromAllDevice(){
        
        WebServiceHandler.shared.logoutFromAllDevice(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    self.bottomAllSignOutAlert.removeFromSuperview()
                    EasyUtils.shared.logoutSingleDevice()
                }else{
                   
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let err):
                self.showToast(message: err.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    
    func getLoggedIndevices() {
        WebServiceHandler.shared.getLoggedInDevices(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    self.userDevices = response.data
                }else{
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let err):
                self.showToast(message: err.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
}
