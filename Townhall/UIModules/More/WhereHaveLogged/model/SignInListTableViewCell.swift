//
//  SignInListTableViewCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 10/8/21.
//

import UIKit

class SignInListTableViewCell : UITableViewCell {
    
    static let identifier = "signInList"
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    var userDevice : UserDevice?{
        didSet{
            lblBrowser.text = userDevice?.browser?.uppercased()
            lblDeviceTitle.text = userDevice?.device
            operatorImageView.image = userDevice?.os?.lowercased() == "ios" || userDevice?.os?.lowercased() == "android" ? #imageLiteral(resourceName: "mobileBlue") : #imageLiteral(resourceName: "pcBlue")
            //debugPrint(userDevice?.loggedTime)
            
           let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: userDevice?.loggedTime ?? "")
            dateFormatter.dateFormat = "- EE, dd MMM YY"
            //lblStatus.textColor = .darkGray
        
            if let devKey = userDevice?.sessionKey{
                if devKey == EasyDataStore.sharedInstance.deviceKey{
                    lblStatus.textColor = .systemGreen
                    lblStatus.text =  "active_now".easyLocalized()
                    lblDate.text = ""
                }else{
                    lblDate.text = dateFormatter.string(from: date ?? Date()).uppercased()
                    lblStatus.text = ""
                }
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        let container = UIView()
        contentView.addSubview(container)
        container.addAnchorToSuperview(leading: 15, trailing: -15, top: 5, bottom: -5)
        container.addSubview(dataView)
        
        
        
        dataView.addAnchorToSuperview(leading : 0 ,trailing: 0, top: 0, bottom: 0)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var dataView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        view.addSubview(operatorImageView)
        view.addSubview(lblDeviceTitle)
        view.addSubview(SubDataView)
        view.addSubview(btnEdit)
        
        operatorImageView.addAnchorToSuperview(leading: 5,centeredVertically: 0)
        operatorImageView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        operatorImageView.widthAnchor.constraint(equalToConstant: .calculatedWidth(30)).isActive = true
 

        lblDeviceTitle.addAnchorToSuperview( top: 15)
        lblDeviceTitle.leadingAnchor.constraint(equalTo: operatorImageView.trailingAnchor, constant: 8).isActive = true

        lblDeviceTitle.bottomAnchor.constraint(equalTo: SubDataView.topAnchor, constant: -8).isActive = true

        lblDeviceTitle.trailingAnchor.constraint(equalTo: btnEdit.leadingAnchor, constant: -8).isActive = true

        
        SubDataView.addAnchorToSuperview( bottom: -15)
        SubDataView.leadingAnchor.constraint(equalTo: operatorImageView.trailingAnchor, constant: 8).isActive = true

        

        btnEdit.addAnchorToSuperview(trailing: -5,centeredVertically: 0)
        btnEdit.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        btnEdit.widthAnchor.constraint(equalToConstant: .calculatedWidth(30)).isActive = true
//
        
    
        
        
        return view
    }()
    
    lazy var operatorImageView : UIImageView = {
       let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "")
        return view
    }()
    lazy var lblBrowser: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 10, style: .medium)
        view.text = "Chrome • "
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        return view
    }()
    lazy var lblDate: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 10, style: .medium)
        view.text = ""
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        return view
    }()
    lazy var lblStatus: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 10, style: .medium)
        view.text = "active_now".easyLocalized()
        view.textColor = .trueGreen
        view.textAlignment = .left
        return view
    }()
    lazy var SubDataView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .clear
        let stack = UIStackView()
        stack.axis = .horizontal
        
        stack.spacing = 4
        stack.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stack)
        stack.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stack.addArrangedSubview(lblBrowser)
        stack.addArrangedSubview(lblDate)
        stack.addArrangedSubview(lblStatus)
        
        return view
    }()
    
    lazy var lblDeviceTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 14, style: .medium)
        view.text = "iPhone - Dhaka, Bangladesh"
        view.textColor = .black
        view.textAlignment = .left
        view.numberOfLines = 1
       
        return view
    }()
    lazy var btnEdit :  UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "moredot")
        button.setImage(image, for: .normal)
        return button
    }()
    
}



