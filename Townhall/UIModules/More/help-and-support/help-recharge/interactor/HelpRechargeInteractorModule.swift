//
//  RechargeHelpInteractorProtocol.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpRechargeInteractorModule : HelpRechargeInputInteractorProtocol{
    var category: ActiveHelpCategory?
    
    func gettopics() {
        WebServiceHandler.shared.getTopics(link: category?.linkUrl ?? "", completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.presenter?.view?.topics = response.data
                }else{
                    self.presenter?.view?.showError(str: response.message ?? "Error")
                }
            case .failure(let error):
                debugPrint(error)
                self.presenter?.view?.showError(str: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    
    var presenter: HelpRechargePresenterProtocol?
    
    
}

