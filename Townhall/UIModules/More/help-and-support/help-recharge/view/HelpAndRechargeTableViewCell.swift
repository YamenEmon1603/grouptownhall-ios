//
//  HelpAndRechargeTableViewCell.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import UIKit

class HelpAndRechargeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var helpRechargeTitle: UILabel!
    @IBOutlet weak var helpRechargeDetails: UILabel!
    @IBOutlet weak var helpRechargeImage: UIImageView!
    @IBOutlet weak var knowMoreBtn: UIButton!
    var topic : ActiveHelpCategory?{
        didSet{
            let langIndex = SSLComLanguageHandler.sharedInstance.getCurrentLanguage()
            helpRechargeTitle.text = langIndex == .English ? topic?.title : topic?.titleBn
            helpRechargeDetails.text = langIndex == .English ? topic?.subtitle : topic?.subtitleBn
            knowMoreBtn.setTitle(langIndex == .English ? topic?.linkTitle : topic?.linkTitleBn, for: .normal)
            helpRechargeImage.downloadSVGImage(url: topic?.banner ?? "")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
