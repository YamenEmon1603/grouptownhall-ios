//
//  HelpRechargeViewController.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/20/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import UIKit

class HelpRechargeViewController: EasyBaseViewController,HelpRechargeViewProtocolModule {
    func showError(str: String) {
        showToast(message: str)
    }
    
    var topics: ActiveHelpCategory?{
        didSet{
            lblHelpAndRecharge.text = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? topics?.title : topics?.titleBn
            helpRechargeTable.reloadData()
        }
    }
    
    var presenter: HelpRechargePresenterProtocol?
    
    @IBOutlet weak var helpRechargeTable: UITableView!
    @IBOutlet weak var lblHelpAndRecharge: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helpRechargeTable.delegate = self
        helpRechargeTable.dataSource = self
        self.helpRechargeTable.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        lblHelpAndRecharge.text = "help_recharge".easyLocalized()
        //languageSwitch(index: EasyUtils.getLanguageIndex())
        presenter?.interactor?.gettopics()
        
    }
    
    @IBAction func helpRechargeBack(_ sender: Any) {
        
        //showToast(message: "Help Recharge Back Tappped")
        self.dismiss(animated: true, completion: nil)
    }
    

  
}
extension HelpRechargeViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topics?.activeChild?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "help_recharge", for: indexPath) as! HelpAndRechargeTableViewCell
        cell.topic = topics?.activeChild?[indexPath.row]
        cell.knowMoreBtn.tag = indexPath.row
       
        cell.knowMoreBtn.addTarget(self, action: #selector(knowMore(_:)), for: .touchUpInside)
        
        
        return cell
    }
    @objc func knowMore(_ sender: UIButton){
        if let data = topics?.activeChild?[sender.tag] {
            presenter?.router?.pushToHelpInformationDetails(vc: self, data: data)
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
               
        
    }
    
    
}

