//
//  HelpRechargeRouterModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpRechargeRouterModule: HelpRechargeRouterProtocol {
    
    
    
    static func createModule(category: ActiveHelpCategory) -> HelpRechargeViewController {
        let presenter : HelpRechargePresenterProtocol & HelpRechargeOutputInteractorProtocol = HelpRechargePresenterModule()
        let interactor : HelpRechargeInputInteractorProtocol = HelpRechargeInteractorModule()
        let router : HelpRechargeRouterProtocol = HelpRechargeRouterModule()
        
        let storyboard = UIStoryboard(name: Constant.HELP_RECHARGE_STORYBOARD_NAME, bundle: nil)
        let view : HelpRechargeViewProtocolModule = storyboard.instantiateViewController(withIdentifier: Constant.HELP_RECHARGE_STORYBOARD_ID) as! HelpRechargeViewController
        
        /// connecting
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        interactor.category = category
        
        return view as! HelpRechargeViewController
    }
    
    
    
    
    
    func pushToHelpInformationDetails(vc: UIViewController) {
        
    }
    func pushToHelpInformationDetails(vc: UIViewController, data: ActiveHelpCategory) {
        let helpInformationDetailsModule = HelpInformationDetailsRouterModule.createModule(data: data)
        helpInformationDetailsModule.modalPresentationStyle = .fullScreen
        vc.present(helpInformationDetailsModule, animated: true, completion: nil)
    }
    
    
}
