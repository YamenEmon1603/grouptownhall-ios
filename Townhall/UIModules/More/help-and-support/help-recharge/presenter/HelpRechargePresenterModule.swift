//
//  HelpRechargePresenterModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpRechargePresenterModule : HelpRechargePresenterProtocol, HelpRechargeOutputInteractorProtocol{
    var view: HelpRechargeViewProtocolModule?
    
    var router: HelpRechargeRouterProtocol?
    
    var interactor: HelpRechargeInputInteractorProtocol?
    
    func pushToHelpInformationDetails(vc: UIViewController) {
      //  router?.pushToHelpInformationDetails(vc: vc)
    }
    
    
}
