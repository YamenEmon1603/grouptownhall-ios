//
//  HelpTopicsResponseModel.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 10/5/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation

// MARK: - HelpTopicsResponseModel
class HelpTopicsResponseModel: Codable {
    let ui: String?
    let message: String?
    let status: String?
    let data: ActiveHelpCategory?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case data = "data"
        case code = "code"
    }

    init(ui: String?, message: String?, status: String?, data: ActiveHelpCategory?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.data = data
        self.code = code
    }
}


