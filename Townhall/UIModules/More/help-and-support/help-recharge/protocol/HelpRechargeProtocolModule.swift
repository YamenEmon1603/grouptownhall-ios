//
//  HelpRechargeProtocol.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

protocol HelpRechargeViewProtocolModule : class {
    var presenter : HelpRechargePresenterProtocol?{get set}
    func showError(str:String)
    var topics : ActiveHelpCategory?{get set}
}
protocol HelpRechargePresenterProtocol : class {
    var view: HelpRechargeViewProtocolModule?{get set}
    var router: HelpRechargeRouterProtocol?{get set}
    var interactor: HelpRechargeInputInteractorProtocol?{get set}
    func pushToHelpInformationDetails(vc: UIViewController)
}

protocol HelpRechargeRouterProtocol: class {
    static func createModule(category:ActiveHelpCategory)-> HelpRechargeViewController
    func pushToHelpInformationDetails(vc: UIViewController,data:ActiveHelpCategory)
    
}

protocol HelpRechargeInputInteractorProtocol: class {
    var presenter: HelpRechargePresenterProtocol?{get set}
    var category:ActiveHelpCategory?{get set}
    func gettopics()
}

protocol HelpRechargeOutputInteractorProtocol: class {
    
}
