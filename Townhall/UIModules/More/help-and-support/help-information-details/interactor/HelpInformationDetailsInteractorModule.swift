//
//  HelpInformationDetailsInteractorModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpInformationDetailsInteractorModule : HelpInformationDetailsInputInteractorProtocol{
    func fetchContent() {
        if let type = dataType{
            switch type {
            case .privecy:
                getPrivecyContent()
            case .terms:
                getTermsContnt()
            }
        }
    }
    
    var dataType: HelpInformationDetailsType?
    
    var presenter: HelpInformationDetailsPresenterProtocol?
    var data: ActiveHelpCategory?
    
    func getPrivecyContent(){
        WebServiceHandler.shared.getPrivacy(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    if let data = response.content{
                        
                        self.presenter?.view?.viewTitle = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English  ? data.title : data.titleBn
                        if let htmlText = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English  ? data.contentDescription : data.descriptionBn{
                            self.presenter?.view?.viewDescription = htmlText.convertToAttributedString()
                        }
                    }
                    
                }
            case .failure(let error):
                debugPrint(error)
            }
        }, shouldShowLoader: true)
    }
    func getTermsContnt(){
        WebServiceHandler.shared.getTerms(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200 {
                    if let data = response.content{
                        self.presenter?.view?.viewTitle = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? data.title : data.titleBn
                        if let htmlText = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? data.contentDescription : data.descriptionBn{
                            self.presenter?.view?.viewDescription = htmlText.convertToAttributedString()
                        }
                    }
                    
                }
            case .failure(let error):
                debugPrint(error)
            }
        }, shouldShowLoader: true)
    }
}
