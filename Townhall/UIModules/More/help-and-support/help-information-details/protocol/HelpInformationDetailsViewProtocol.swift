//
//  HelpInformationDetailsViewProtocol.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

protocol HelpInformationDetailsViewProtocol : class {
    var presenter : HelpInformationDetailsPresenterProtocol?{get set}
    var viewTitle:String?{get set}
    var viewDescription:NSAttributedString?{get set}
}

protocol HelpInformationDetailsPresenterProtocol : class {
    var view : HelpInformationDetailsViewProtocol?{set get}
    var router: HelpInformationDetailsRouterProtocol?{get set}
    var interactor: HelpInformationDetailsInputInteractorProtocol?{get set}
    func pushToProfileEdit(vc: UIViewController)
    func shouldMakeViewReady()
    
}

protocol HelpInformationDetailsRouterProtocol : class{
    static func createModule(data:ActiveHelpCategory)-> HelpInformationDetailsViewController
    static func createModule(type:HelpInformationDetailsType)-> HelpInformationDetailsViewController
    func pushToAnotherVC(vc: UIViewController)
}
protocol HelpInformationDetailsInputInteractorProtocol: class {
    var presenter: HelpInformationDetailsPresenterProtocol?{get set}
    var data:ActiveHelpCategory?{get set}
    var dataType:HelpInformationDetailsType?{get set}
    func fetchContent()
}

protocol HelpInformationDetailsOutputInteractorProtocol: class {
    
}

enum HelpInformationDetailsType {
    case privecy
    case terms
}
