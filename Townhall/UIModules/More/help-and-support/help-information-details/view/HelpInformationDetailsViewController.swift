//
//  HelpInformationDetailsViewController.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import UIKit

class HelpInformationDetailsViewController: EasyBaseViewController,HelpInformationDetailsViewProtocol {
   
    
    var presenter: HelpInformationDetailsPresenterProtocol?
    @IBOutlet weak var lblHelpDetailsTitle: UILabel!
    @IBOutlet weak var tfInformationDetails: UITextView!
    @IBOutlet weak var lblWasThisInformationHelpful: UILabel!
    @IBOutlet weak var lblWeValueYourFeedback: UILabel!
    @IBOutlet weak var lblTermsOfUseAndPrivacy: UILabel!
    @IBOutlet weak var vwIsThisInformationHelpful: UIView!
    @IBOutlet weak var constraintIsThisInformationHelpful: NSLayoutConstraint!
    
    var viewTitle: String?{
        didSet{
            debugPrint(presenter?.interactor?.dataType ?? "")
            vwIsThisInformationHelpful.isHidden = false
            constraintIsThisInformationHelpful.constant = 71
            lblHelpDetailsTitle.text = viewTitle
            if let type = presenter?.interactor?.dataType{
                switch type {
                case .privecy:
                    debugPrint("PRIVACY")
                    lblTermsOfUseAndPrivacy.isHidden = false
                    lblTermsOfUseAndPrivacy.text  = viewTitle
                    vwIsThisInformationHelpful.isHidden = true
                    constraintIsThisInformationHelpful.constant = 0
                case .terms:
                    debugPrint("TERM")
                    lblTermsOfUseAndPrivacy.isHidden = false
                    lblTermsOfUseAndPrivacy.text = viewTitle
                    vwIsThisInformationHelpful.isHidden = true
                    constraintIsThisInformationHelpful.constant = 0
                    
                }
            }
            
        }
    }
    
    var viewDescription: NSAttributedString?{
        didSet{
            tfInformationDetails.attributedText = viewDescription
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //languageSwitch(index: EasyUtils.getLanguageIndex())
        lblWasThisInformationHelpful.text = "was_this_information_helpful".easyLocalized()
        lblTermsOfUseAndPrivacy.text = "terms_of_use_and_privacy_policy".easyLocalized()
        lblWeValueYourFeedback.text = "we_value_your_feedback".easyLocalized()
        presenter?.shouldMakeViewReady()
       
    }
 
    
    @IBAction func likeFeedback(_ sender: Any) {
        showToast(message: "Liked")
        
    }
    
    @IBAction func dislikeFeedback(_ sender: Any) {
        showToast(message: "Disliked")
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        //showToast(message: "Back btn")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
