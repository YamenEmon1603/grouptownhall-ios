//
//  HelpInformationDetailsRouterModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class  HelpInformationDetailsRouterModule : HelpInformationDetailsRouterProtocol{
    static func createModule(type: HelpInformationDetailsType) -> HelpInformationDetailsViewController {
        let presenter : HelpInformationDetailsPresenterProtocol & HelpInformationDetailsOutputInteractorProtocol = HelpInformationDetailsPresenterModule()
        let interactor : HelpInformationDetailsInputInteractorProtocol = HelpInformationDetailsInteractorModule()
        let router : HelpInformationDetailsRouterProtocol = HelpInformationDetailsRouterModule()
        
        
        let storyboard = UIStoryboard(name: Constant.HELP_INFORMATION_DETAILS_STORYBOARD_NAME, bundle: nil)
        let view : HelpInformationDetailsViewProtocol = storyboard.instantiateViewController(withIdentifier: Constant.HELP_INFORMATION_DETAILS_STORYBOARD_ID) as! HelpInformationDetailsViewController
        
        /// connecting
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        interactor.dataType = type
        return view as! HelpInformationDetailsViewController
    }
    
    static func createModule(data:ActiveHelpCategory) -> HelpInformationDetailsViewController {
        let presenter : HelpInformationDetailsPresenterProtocol & HelpInformationDetailsOutputInteractorProtocol = HelpInformationDetailsPresenterModule()
        let interactor : HelpInformationDetailsInputInteractorProtocol = HelpInformationDetailsInteractorModule()
        let router : HelpInformationDetailsRouterProtocol = HelpInformationDetailsRouterModule()
        
        
        let storyboard = UIStoryboard(name: Constant.HELP_INFORMATION_DETAILS_STORYBOARD_NAME, bundle: nil)
        let view : HelpInformationDetailsViewProtocol = storyboard.instantiateViewController(withIdentifier: Constant.HELP_INFORMATION_DETAILS_STORYBOARD_ID) as! HelpInformationDetailsViewController
        
        /// connecting
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        interactor.data = data
        return view as! HelpInformationDetailsViewController
        
        
    }
    
    func pushToAnotherVC(vc: UIViewController) {
        
    }
    
    
}
