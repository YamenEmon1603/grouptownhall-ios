//
//  HelpInformationDetailsPresenterModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpInformationDetailsPresenterModule: HelpInformationDetailsPresenterProtocol,HelpInformationDetailsOutputInteractorProtocol {
    func shouldMakeViewReady() {
        if let category = interactor?.data{
            
            view?.viewTitle = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? category.title : category.titleBn
            if let htmlText = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? category.activeCategoryDescription : category.descriptionBn{
                view?.viewDescription = htmlText.convertToAttributedString()
            }
            
        }else{
            interactor?.fetchContent()
        }
    }
    
    var view: HelpInformationDetailsViewProtocol?
    
    var router: HelpInformationDetailsRouterProtocol?
    
    var interactor: HelpInformationDetailsInputInteractorProtocol?
    
    func pushToProfileEdit(vc: UIViewController) {
        router?.pushToAnotherVC(vc: vc)
    }
    
    
 
    
}

extension String{
    func convertToAttributedString()-> NSAttributedString?{
        if let encodedData = self.data(using: String.Encoding.utf8){
            do {
                let  attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
                return attributedString
            } catch let error as NSError {
                debugPrint(error.localizedDescription)
            } catch {
                debugPrint("error")
            }
            
        }
        return nil
    }
}
