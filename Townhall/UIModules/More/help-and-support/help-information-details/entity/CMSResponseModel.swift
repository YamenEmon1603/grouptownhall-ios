//
//  CMSResponseModel.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 10/6/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation
// MARK: - CMSResponseModel
class CMSResponseModel: Codable {
    let ui: String?
    let message: String?
    let status: String?
    let content: EasyCommonContent?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case content = "data"
        case code = "code"
    }

    init(ui: String?, message: String?, status: String?, content: EasyCommonContent?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.content = content
        self.code = code
    }
}

// MARK: - Content
class EasyCommonContent: Codable {
    let id: Int?
    let title: String?
    let titleBn: String?
    let subtitle: String?
    let subtitleBn: String?
    let titleDesc: String?
    let titleDescBn: String?
    let contentDescription: String?
    let descriptionBn: String?
    let createdAt: String?
    let updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case titleBn = "title_bn"
        case subtitle = "subtitle"
        case subtitleBn = "subtitle_bn"
        case titleDesc = "title_desc"
        case titleDescBn = "title_desc_bn"
        case contentDescription = "description"
        case descriptionBn = "description_bn"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, title: String?, titleBn: String?, subtitle: String?, subtitleBn: String?, titleDesc: String?, titleDescBn: String?, contentDescription: String?, descriptionBn: String?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.title = title
        self.titleBn = titleBn
        self.subtitle = subtitle
        self.subtitleBn = subtitleBn
        self.titleDesc = titleDesc
        self.titleDescBn = titleDescBn
        self.contentDescription = contentDescription
        self.descriptionBn = descriptionBn
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
