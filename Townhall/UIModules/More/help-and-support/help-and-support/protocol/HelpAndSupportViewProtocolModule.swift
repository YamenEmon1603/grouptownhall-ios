//
//  HelpAndSupportViewProtocol.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

protocol HelpAndSupportViewProtocolModule : class{
    var presenter : HelpAndSupportPresenterProtocol?{get set}
    func showMessage(str:String)
    var helpData:EasyHelp?{get set}
}
protocol HelpAndSupportPresenterProtocol : class {
    var view : HelpAndSupportViewProtocolModule?{get set}
    var interactor : HelpAndSupportInputInterectorProtocol?{set get}
    var router : HelpAndSupportRouterProtocol?{set get}
    func pushToViewAll(vc: UIViewController,category:ActiveHelpCategory)
    func pushToInformationDetails(vc : UIViewController,data:ActiveHelpCategory)
    
}

protocol HelpAndSupportRouterProtocol : class{
    static func createModule()-> HelpAndSupportViewController
    func pushToViewAll(vc: UIViewController,category:ActiveHelpCategory)
    func pushToInformationDetails(vc : UIViewController,data:ActiveHelpCategory)
    
}

protocol HelpAndSupportInputInterectorProtocol : class{
    var presenter: HelpAndSupportPresenterProtocol?{get set}
    func getHelpAndSuport()
}
protocol HelpAndSupportOutputInteractorProtocol : class {
    
}
protocol ActiveHelpCategoryDelegate{
   func didSelect(category:ActiveHelpCategory)
}
