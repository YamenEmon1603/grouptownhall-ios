//
//  PopularHelpTableViewCell.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/20/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import UIKit

class PopularHelpTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lblPopularHelpTopicsTitle3: UILabel!
    @IBOutlet weak var lblPopularHelpTopicsSubtitle3: UILabel!
    @IBOutlet weak var btnKnowMore3: UIButton!
    
    var data:ActiveHelpCategory?{
        didSet{
            
            lblPopularHelpTopicsTitle3.text = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? data?.title : data?.titleBn
            lblPopularHelpTopicsSubtitle3.text = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? data?.subtitle : data?.subtitleBn
            ivIcon.downloadSVGImage(url:data?.banner ?? "")
            btnKnowMore3.setTitle(SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? data?.linkTitle : data?.linkTitleBn, for: .normal)
           
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let _ = contentView.addBorder(side: .bottom, color: .blueyGrey, width: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
