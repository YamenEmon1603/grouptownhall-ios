//
//  ActiveHelpCategoryCollectionViewCell.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 10/5/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import UIKit

class ActiveHelpCategoryCollectionViewCell: UICollectionViewCell {
    static let identifier = "ActiveHelpCategoryCollectionViewCell"
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivBanner: UIImageView!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    var data: ActiveHelpCategory?{
        didSet{
            let langIndex = SSLComLanguageHandler.sharedInstance.getCurrentLanguage()
            lblTitle.text = langIndex == .English ? data?.title : data?.titleBn
            lblSubTitle.text = langIndex == .English ? data?.subtitle : data?.subtitleBn
            ivBanner.downloadSVGImage(url:data?.banner ?? "")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.borderColor = UIColor.borderColor.cgColor
        self.contentView.layer.borderWidth = 1
        self.contentView.layer.cornerRadius = 10
        
        // Initialization code
    }

}
