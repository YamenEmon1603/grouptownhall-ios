//
//  CategoryTableViewCell.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/20/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var lblChooseACategoryYouNeedHelpWith: UILabel!
    var data : [ActiveHelpCategory]?{
        didSet{
            categoriesCollectionView.reloadData()
        }
    }
    var delegate:ActiveHelpCategoryDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        categoriesCollectionView.register(UINib(nibName:"ActiveHelpCategoryCollectionViewCell" , bundle: nil), forCellWithReuseIdentifier: ActiveHelpCategoryCollectionViewCell.identifier)
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        categoriesCollectionView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension CategoryTableViewCell : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ActiveHelpCategoryCollectionViewCell.identifier, for: indexPath) as! ActiveHelpCategoryCollectionViewCell
        cell.data = data?[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height : CGFloat = collectionView.frame.height
        if let count = data?.count{
            let colLine = ceil(CGFloat(count) / 2.0)
            debugPrint("colLine",colLine)
            height = height / colLine
        }
        return CGSize(width: collectionView.frame.width / 2 - 5 , height: height - 8 )
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 2)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cateData = data?[indexPath.row]{
            delegate?.didSelect(category: cateData)
        }
        
    }
}
