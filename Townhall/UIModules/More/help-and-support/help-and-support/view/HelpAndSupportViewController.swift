//
//  HelpAndSupportViewController.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/20/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import UIKit
import SVGKit
class HelpAndSupportViewController: EasyBaseViewController,HelpAndSupportViewProtocolModule {
    
    var helpData: EasyHelp?{
        didSet{
            helpAndSupportTable.reloadData()
        }
    }
    
    
    @IBOutlet weak var helpAndSupportTable: UITableView!
    @IBOutlet weak var lblHelpAndSupport: EasyPaddingLabel!
    @IBOutlet weak var btnSupport: UIButton!
    
    let headers :[String] = ["choose_a_category_you_need".easyLocalized(),"popular_help_topics".easyLocalized()]
    var presenter: HelpAndSupportPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helpAndSupportTable.delegate = self
        helpAndSupportTable.dataSource = self
        self.helpAndSupportTable.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        lblHelpAndSupport.text = "help_and_support".easyLocalized()
        btnSupport.setTitle("twenty_four_by_seven_support".easyLocalized(), for: .normal)
        presenter?.interactor?.getHelpAndSuport()
    }
    
  
 
    
    @IBAction func helpAndSupportBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func helpAndSupportsBtn(_ sender: UIButton) {
       
        let actionSheetController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
//        actionSheetController.setValue(NSAttributedString(string: "How you would like to contact us?", attributes: [NSAttributedString.Key.font : EasyUtils.FontUtils.EasyFont(ofSize: 16, style: .medium),NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
        
        let cancelAction = UIAlertAction(title: "btn_cancel_title".easyLocalized(), style: .cancel) { action -> Void in
           
        }
        actionSheetController.addAction(cancelAction)

        
        let callAction = UIAlertAction(title: "\(helpData?.supportPhone ?? "")", style: .default) { action -> Void in
            self.dialNumber(number : self.helpData?.supportPhone ?? "")
        }
        actionSheetController.addAction(callAction)
        let emailAction = UIAlertAction(title: "\(helpData?.supportEmail ?? "")", style: .default) { action -> Void in
            if let url = URL(string: "mailto:\(self.helpData?.supportEmail ?? "")") ,
                   UIApplication.shared.canOpenURL(url){
                     if #available(iOS 10.0, *) {
                       UIApplication.shared.open(url)
                     } else {
                       UIApplication.shared.openURL(url)
                     }
                   }else {
                    self.showMessage(str: "Failed to open mail app")
                   }
        }
        actionSheetController.addAction(emailAction)
        actionSheetController.view.tintColor = .blueyGrey

        // We need to provide a popover sourceView when using it on iPad
        actionSheetController.popoverPresentationController?.sourceView = sender 
        actionSheetController.popoverPresentationController?.sourceRect = sender.bounds
        
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
       
        
    }
    func showMessage(str:String) {
        showToast(message: str)
    }
    @objc func showDetails(_ sender : UIButton){
        if let data = helpData?.activePopularTopics?[sender.tag]{
            presenter?.router?.pushToInformationDetails(vc: self, data: data)
        }
    }
    func dialNumber(number : String) {

         if let url = URL(string: "tel://\(number)"),
           UIApplication.shared.canOpenURL(url) {
              if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
               } else {
                   UIApplication.shared.openURL(url)
               }
           } else {
            
            showMessage(str: "Calling function is unavailable")
           }
        }
    
}
extension HelpAndSupportViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = .white
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 8, width:tableView.bounds.size.width, height: 19))
        headerLabel.font = UIFont(name: "Roboto-Medium", size: 16)
        headerLabel.textColor = UIColor.black
        headerLabel.text = headers[section]
        
        headerView.addSubview(headerLabel)
        
        let _ = headerView.addBorder(side: .top, color: UIColor.paleLilac, width: 5.0)
        let _  = headerView.addBorder(side: .bottom, color: UIColor.paleLilac, width: 0.5)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  35
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section  == 0 ? 1 : helpData?.activePopularTopics?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "category", for: indexPath) as! CategoryTableViewCell
           
            cell.data = helpData?.activeCategories
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "popular", for: indexPath) as! PopularHelpTableViewCell
            cell.data = helpData?.activePopularTopics?[indexPath.row]
            cell.btnKnowMore3.tag = indexPath.row
            cell.btnKnowMore3.addTarget(self, action: #selector(showDetails(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height : CGFloat = tableView.frame.height * 0.25
        if let count = helpData?.activePopularTopics?.count{
            let colLine = ceil(CGFloat(count) / 2.0)
            debugPrint("colLine",colLine)
            height = height * colLine
        }
        
        return indexPath.section  == 0 ?  height : 135
    }
}
extension HelpAndSupportViewController: ActiveHelpCategoryDelegate{
    func didSelect(category: ActiveHelpCategory) {
        presenter?.pushToViewAll(vc: self, category: category)
    }
}


extension UIImageView{
    func downloadSVGImage(url:String){
        do {
            if let destination = URL.init(string: url){
                let imgData = try NSData(contentsOf: destination, options: NSData.ReadingOptions())
                EasyManager.sharedInstance.delay(0.1) {
                    let image = SVGKImage(data: imgData as Data)
                    self.image = image?.uiImage
                }

            }
           
        } catch {

        }
    }
}
