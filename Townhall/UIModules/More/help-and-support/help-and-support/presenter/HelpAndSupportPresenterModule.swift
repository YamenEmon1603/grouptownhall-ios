//
//  HelpAndSupportPresenterModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpAndSupportPresenterModule : HelpAndSupportPresenterProtocol,HelpAndSupportOutputInteractorProtocol{
    
    
   
    
    var view: HelpAndSupportViewProtocolModule?
    
    var interactor: HelpAndSupportInputInterectorProtocol?
    
    var router: HelpAndSupportRouterProtocol?
    
    func pushToInformationDetails(vc: UIViewController,data:ActiveHelpCategory) {
        router?.pushToInformationDetails(vc: vc, data: data)
       }
    func pushToViewAll(vc: UIViewController, category: ActiveHelpCategory) {
        router?.pushToViewAll(vc: vc, category: category)
    }
  
    
    
}
