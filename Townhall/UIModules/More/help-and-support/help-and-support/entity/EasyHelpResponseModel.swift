//
//  EasyHelpResponseModel.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 10/5/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation
// MARK: - EasyHelpResponseModel
class EasyHelpResponseModel: Codable {
    let ui: String?
    let message: String?
    let status: String?
    let data: EasyHelp?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case data = "data"
        case code = "code"
    }

    init(ui: String?, message: String?, status: String?, data: EasyHelp?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.data = data
        self.code = code
    }
}

// MARK: - DataClass
class EasyHelp: Codable {
    let id: Int?
    let supportPhone: String?
    let supportPhoneBn: String?
    let supportEmail: String?
    let supportEmailBn: String?
    let createdAt: String?
    let updatedAt: String?
    let activeCategories: [ActiveHelpCategory]?
    let activePopularTopics: [ActiveHelpCategory]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case supportPhone = "support_phone"
        case supportPhoneBn = "support_phone_bn"
        case supportEmail = "support_email"
        case supportEmailBn = "support_email_bn"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case activeCategories = "active_categories"
        case activePopularTopics = "active_popular_topics"
    }

    init(id: Int?, supportPhone: String?, supportPhoneBn: String?, supportEmail: String?, supportEmailBn: String?, createdAt: String?, updatedAt: String?, activeCategories: [ActiveHelpCategory]?, activePopularTopics: [ActiveHelpCategory]?) {
        self.id = id
        self.supportPhone = supportPhone
        self.supportPhoneBn = supportPhoneBn
        self.supportEmail = supportEmail
        self.supportEmailBn = supportEmailBn
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.activeCategories = activeCategories
        self.activePopularTopics = activePopularTopics
    }
}

// MARK: - ActiveCategory
class ActiveHelpCategory: Codable {
    let id: Int?
    let contactId: Int?
    let parentId: Int?
    let type: String?
    let title: String?
    let titleBn: String?
    let subtitle: String?
    let subtitleBn: String?
    let banner: String?
    let activeCategoryDescription: String?
    let descriptionBn: String?
    let linkTitle: String?
    let linkTitleBn: String?
    let linkUrl: String?
    let priority: Int?
    let fav: Int?
    let status: Int?
    let createdAt: String?
    let updatedAt: String?
    let parent: ActiveHelpCategory?
    let activeChild: [ActiveHelpCategory]?
   
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case contactId = "contact_id"
        case parentId = "parent_id"
        case type = "type"
        case title = "title"
        case titleBn = "title_bn"
        case subtitle = "subtitle"
        case subtitleBn = "subtitle_bn"
        case banner = "banner"
        case activeCategoryDescription = "description"
        case descriptionBn = "description_bn"
        case linkTitle = "link_title"
        case linkTitleBn = "link_title_bn"
        case linkUrl = "link_url"
        case priority = "priority"
        case fav = "fav"
        case status = "status"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case parent = "parent"
        case activeChild = "active_child"
    }

    init(id: Int?, contactId: Int?, parentId: Int?, type: String?, title: String?, titleBn: String?, subtitle: String?, subtitleBn: String?, banner: String?, activeCategoryDescription: String?, descriptionBn: String?, linkTitle: String?, linkTitleBn: String?, linkUrl: String?, priority: Int?, fav: Int?, status: Int?, createdAt: String?, updatedAt: String?, parent: ActiveHelpCategory?,activeChild:[ActiveHelpCategory]?) {
        self.id = id
        self.contactId = contactId
        self.parentId = parentId
        self.type = type
        self.title = title
        self.titleBn = titleBn
        self.subtitle = subtitle
        self.subtitleBn = subtitleBn
        self.banner = banner
        self.activeCategoryDescription = activeCategoryDescription
        self.descriptionBn = descriptionBn
        self.linkTitle = linkTitle
        self.linkTitleBn = linkTitleBn
        self.linkUrl = linkUrl
        self.priority = priority
        self.fav = fav
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.parent = parent
        self.activeChild = activeChild
    }
}
