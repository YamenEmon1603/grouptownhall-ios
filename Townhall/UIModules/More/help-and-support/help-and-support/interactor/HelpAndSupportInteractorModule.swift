//
//  HelpAndSupportInteractorModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpAndSupportInteractorModule : HelpAndSupportInputInterectorProtocol{
    var presenter: HelpAndSupportPresenterProtocol?
    
    func getHelpAndSuport() {
        WebServiceHandler.shared.getHelpAndSuportContact(completion: { (result) in
            switch result{
            case .success(let reponse):
                if reponse.code == 200{
                    self.presenter?.view?.helpData = reponse.data
                }else{
                    self.presenter?.view?.showMessage(str: reponse.message ?? "Error")
                }
            case .failure(let error):
                self.presenter?.view?.showMessage(str: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    


    
}
