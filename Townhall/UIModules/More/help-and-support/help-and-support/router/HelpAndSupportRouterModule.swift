//
//  HelpAndSupportRouterModule.swift
//  ScrollableSegment
//
//  Created by Raihan on 10/21/19.
//  Copyright © 2019 raihan.iOS. All rights reserved.
//

import Foundation
import UIKit

class HelpAndSupportRouterModule : HelpAndSupportRouterProtocol{
   
    
    
    
    static func createModule() -> HelpAndSupportViewController {
        let presenter : HelpAndSupportPresenterProtocol & HelpAndSupportOutputInteractorProtocol = HelpAndSupportPresenterModule()
        let interactor : HelpAndSupportInputInterectorProtocol = HelpAndSupportInteractorModule()
        let router : HelpAndSupportRouterProtocol = HelpAndSupportRouterModule()
        
        let storyboard = UIStoryboard(name: Constant.HELP_AND_SUPPORT_STORYBOARD_NAME, bundle: nil)
        
        let view : HelpAndSupportViewProtocolModule = storyboard.instantiateViewController(withIdentifier: Constant.HELP_AND_SUPPORT_STORYBOARD_ID) as! HelpAndSupportViewController
        /// connecting
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        
        return view as! HelpAndSupportViewController
        
    }
   
    
    func pushToViewAll(vc: UIViewController, category: ActiveHelpCategory) {
        let viewAllHelpRechargeModule = HelpRechargeRouterModule.createModule(category: category)
        viewAllHelpRechargeModule.modalPresentationStyle = .fullScreen
        vc.present(viewAllHelpRechargeModule, animated: true, completion: nil)
    }
    func pushToInformationDetails(vc: UIViewController,data:ActiveHelpCategory) {
        let helpInformationDetailsModule = HelpInformationDetailsRouterModule.createModule(data: data)
        helpInformationDetailsModule.modalPresentationStyle = .fullScreen
        vc.present(helpInformationDetailsModule, animated: true, completion: nil)
    }
    
    
    
    
    
}
