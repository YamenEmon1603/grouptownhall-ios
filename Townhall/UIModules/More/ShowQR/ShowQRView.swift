//
//  ShowQRView.swift
//  Townhall
//
//  Created by Raihan on 1/4/22.
//

import Foundation
import UIKit

class ShowQRView : EasyBaseView {
    var proceedCallBack : (()->Void)?
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        
        
        
        addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 8).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0,bottom:0)

    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"Show QR")
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleGrey, width: 0.5)
        return navView
    }()
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.backgroundColor = .white
        formView.translatesAutoresizingMaskIntoConstraints = false
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        holderView.addSubview(imageView)
        imageView.addAnchorToSuperview( heightMultiplier: 0.4,centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
        
      
        return formView
    }()
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.borderWidth = 2
        
        
        return imageView
    }()
}
