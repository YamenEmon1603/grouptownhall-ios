//
//  ShowQRVC.swift
//  Townhall
//
//  Created by Raihan on 1/4/22.
//

import UIKit

class ShowQRVC: EasyBaseViewController {
    lazy var mView : ShowQRView = {
        let v = ShowQRView()
        v.navView.backButtonCallBack = backAction
        return v
    }()
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        if let mobile  = EasyDataStore.sharedInstance.ownNumber?.number{
            mView.imageView.image = generateQRCode(from: "\(mobile)")
            
        }
        // Do any additional setup after loading the view.
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
    }
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }

}
