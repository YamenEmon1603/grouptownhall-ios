//
//  EasyTabBarViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/11/21.
//

import UIKit

class EasyTabBarViewController: UITabBarController,QrScannerDelegate {
    func cancelCallBack(_ message: String) {
        print("cancelCallBack")
    }
    
    func failCallBack(_ message: String) {
        print("failCallBack")
    }
    
    func successCallBack(_ payloadModel: [[String : String]]) {
        print("successCallBack")
    }
    
    var selectedTabIndex:Int = 0
    var customTabBar: TabBarMenu!
    var tabBarHeight: CGFloat = .calculatedHeight(69.0)
    let tabItems: [TabBarItem] = [.home,.offers,.QRPay,.history,.more]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        self.loadTabBar()
        self.customTabBar.activateTab(tab: self.selectedTabIndex)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.callScanMethod),
            name: NSNotification.Name(rawValue: "ShowQR"),
            object: nil)
    }
    @objc func callScanMethod(){
        startBanglaQRSdk()
    }
    func startBanglaQRSdk(){
        let initializer = BanglaQRInitialization.init(fundingSource: .VISA, primaryColor: "346DF6", secondaryColor: "F4F6F7")
        ScannerController.startSDK(controller: self, mainModel: initializer, sdkDelegate: self)
    }
    func loadTabBar() {
        //        setupCustomtab()
        self.setupCustomTabMenu(tabItems) { (controllers) in
            self.viewControllers = controllers
        }
        
        
    }
    func setTabBarHidden(_ isHidden: Bool)  {
        if isHidden{
            customTabBar.removeFromSuperview()
        }else{
            loadTabBar()
            self.customTabBar.activateTab(tab: selectedIndex)
        }
    }
    
    func setupCustomTabMenu(_ menuItems: [TabBarItem], completion: @escaping ([UIViewController]) -> Void) {
        let frame = tabBar.frame
        var controllers = [UIViewController]()// hide the tab bar
        tabBar.isHidden = true
        self.customTabBar = TabBarMenu(menuItems: tabItems, frame: frame)
        self.customTabBar.translatesAutoresizingMaskIntoConstraints = false
        self.customTabBar.clipsToBounds = true
        self.customTabBar.itemTapped = self.changeTab// Add it to the view
        self.customTabBar.addShadow(location: .top, color: .black, opacity: 0.15, radius: 5.0)
        self.view.addSubview(customTabBar)// Add positioning constraints to place the nav menu right where the tab bar should be
        //        self.customTabBar.isHidden = true
        NSLayoutConstraint.activate([
            self.customTabBar.leadingAnchor.constraint(equalTo: tabBar.leadingAnchor),
            self.customTabBar.trailingAnchor.constraint(equalTo: tabBar.trailingAnchor),
            self.customTabBar.widthAnchor.constraint(equalToConstant: tabBar.frame.width),
            self.customTabBar.heightAnchor.constraint(equalToConstant: tabBarHeight), // Fixed height for nav menu
            self.customTabBar.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
        for i in 0 ..< tabItems.count {
            controllers.append(tabItems[i].viewController) // we fetch the matching view controller and append here
        }
        
        self.view.layoutIfNeeded() // important step
        completion(controllers) // setup complete. handoff here
    }
    func changeTab(tab: Int) {
        if tab == 2 {
            if EasyDataStore.sharedInstance.user?.qrPin == true{
                self.startBanglaQRSdk()
            }else{
                self.showSetPinAlert()
                
            }
            return
        }else{
            self.selectedIndex = tab
        }
        
    }
    
    func showSetPinAlert() {
        
        
        self.view.addSubview(bottomAlert)
        bottomAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        bottomAlert.btnSecondCallBack = btnSecondAction
        bottomAlert.btnFirstCallBack = btnFirstAction
        
    }
    let bottomAlert = HorizontalButtonAlertView(header: "you_need_to_set_pin_to_use_qr".easyLocalized(), subHeader: "please_verify_your_mobile_number_and_set_a_pin".easyLocalized(), btn1Name: "cancel".easyLocalized(), btn2Name: "proceed".easyLocalized())
    func showBottomAlert() {
        bottomAlert.removeFromSuperview()
    }
    func btnSecondAction(){
        bottomAlert.removeFromSuperview()
        self.qrOtpSend()
        
    }
    func btnFirstAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        bottomAlert.removeFromSuperview()
    }
    
    func qrOtpSend(){
        WebServiceHandler.shared.qrOtpSendResend(completion: { (result) in
            switch result{
            case .success( _ ):
                let nav = SetPinNavVC()
                if let mobile = EasyDataStore.sharedInstance.user?.mobile{
                    
                    //                    let otpModule = OTPRouterModule.createModule(using: "", mobile: mobile ,module:.setpin, navigationController:nav)
                    let vc = OTPEntryViewController()
                    vc.identity = mobile
                    vc.forModule = .setpin
                    nav.setPinDelegate = self
                    nav.viewControllers = [vc]
                    nav.setNavigationBarHidden(true, animated: true)
                    self.present(nav, animated: true, completion: nil)
                }
                break
            case .failure(let error):
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
    }
    
    
    
}
extension EasyTabBarViewController : EasyPopUpDelegate,SetPinDelegate{
    
    func popupDismissedWith(button: PopupButton, consent: Bool?, rating: Int?) {
        if button.tag == 1001{
            
        }else if button.tag == 1002{
            self.qrOtpSend()
        }else{
            
            navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    func setpinSuccess() {
        print("setpinSuccess")
        //showTransactionSuccess(message: "Pin Set Success", transactionId: "")
        showToast(message: "Pin Set Success")
    }
    
    func setPinFailed() {
        print("setPinFailed")
        showToast(message: "Pin Set failed")
        
    }
}
class SetPinNavVC: UINavigationController {
    var setPinDelegate :SetPinDelegate?
}

protocol SetPinDelegate {
    func setpinSuccess()
    func setPinFailed()
}
protocol QRNavVCDelete{
    func openQR()
}
