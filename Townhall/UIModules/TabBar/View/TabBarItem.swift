//
//  TabBarItem.swift
//  CustomTababar
//
//  Created by Mausum Nandy on 24/2/21.
//


import Foundation
import UIKit
enum TabBarItem:  CaseIterable {
    case home
    case offers
    case QRPay
    case history
    case more
    
    var viewController : UINavigationController{
        switch self {
        case .home:
            let homeRouterModule = HomeViewController()
            let nav =  UINavigationController(rootViewController: homeRouterModule)
            nav.setNavigationBarHidden(true, animated: true)
            return nav
        case .offers:
            let vc = VenueViewController()
            return UINavigationController(rootViewController: vc)
        case .QRPay:
            let vc = UIViewController()
            return UINavigationController(rootViewController: vc)
        case .history:
            let vc = ProgramScheduleVC()
            let nav =  UINavigationController(rootViewController: vc)
            nav.setNavigationBarHidden(true, animated: true)
            return nav
        case .more:
            let vc = MoreViewController()
            return UINavigationController(rootViewController: vc)

        }
    }
    var title:String{
        switch self {
        case .home:
            return "home_txts".easyLocalized()
        case .offers:
            return "Venue"
        case .QRPay:
            return "qr_pay_txts".easyLocalized()
        case .history:
            return "Schedule"
        case .more:
            return "more_texts".easyLocalized()
        }
    }
    var icon : UIImage{
        switch self {
        case .home:
            return #imageLiteral(resourceName: "home")
        case .offers:
            return #imageLiteral(resourceName: "offers")
        case .QRPay:
            return UIImage(named: "banglaQRTab")!
        case .history:
            return #imageLiteral(resourceName: "history")
        case .more :
            return UIImage(named: "more")!
        }
    }
    var selectedIcon : UIImage{
        switch self {
        case .home:
            return #imageLiteral(resourceName: "homeselected")
        case .offers:
            return #imageLiteral(resourceName: "offerselected")
        case .QRPay:
            return UIImage(named: "banglaQRTab")!
        case .history:
            return #imageLiteral(resourceName: "historySelected")
        case .more :
            return #imageLiteral(resourceName: "moreselected")
        }
    }
}
