//
//  TabBarMenu.swift
//  CustomTababar
//
//  Created by Mausum Nandy on 24/2/21.
//

import UIKit
struct Appearence {
    var activeColor : UIColor = .blue
    var inactiveColor: UIColor = .gray
    var font: UIFont = UIFont.systemFont(ofSize: 12)
}
class TabBarMenu: UIView {
    
    var itemTapped: ((_ tab: Int) -> Void)?
    var activeItem: Int = 0
    var menuItems: [TabBarItem] = []
    var menuItemsViews:[TabItemView] = []
    var previous : TabItemView?
    
    var appearence:Appearence = Appearence(activeColor: .blueViolet, inactiveColor: .steel, font: UIFont.EasyFont(ofSize: 12, style: .regular))
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    convenience init(menuItems: [TabBarItem], frame: CGRect) {
        self.init(frame: frame)
        
        
        
        self.menuItems = menuItems
        self.layer.backgroundColor = UIColor.white.cgColor
        let  middle = Int(floor(Double(menuItems.count)/2.0))
        print(menuItems[middle])
        for i in 0 ..< menuItems.count {
            let itemWidth = self.bounds.width / CGFloat(menuItems.count)
            let itemView = TabItemView(menuItem: menuItems[i], frame: .zero, appearance: self.appearence,isCenter:i==middle)//self.createTabItem(item: menuItems[i])
            itemView.translatesAutoresizingMaskIntoConstraints = false
            itemView.clipsToBounds = true
            itemView.tag = i
            self.addSubview(itemView)
            NSLayoutConstraint.activate([
                //itemView.widthAnchor.constraint(equalTo: self.widthAnchor,multiplier: 0.20),
                itemView.widthAnchor.constraint(equalToConstant: itemWidth),
                itemView.heightAnchor.constraint(equalToConstant: itemWidth),                //itemView.heightAnchor.constraint(equalTo: itemView.widthAnchor, multiplier: 1.0),
               // itemView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
//            itemView.leadingAnchor.constraint(equalTo: previous?.trailingAnchor ?? self.leadingAnchor).isActive = true
//            itemView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            
            
            if i == middle-1 ||  i == menuItems.count - 1{
                itemView.leadingAnchor.constraint(equalTo: previous?.trailingAnchor ?? self.leadingAnchor,constant: -10).isActive = true
            }else if i == middle + 1 {
                itemView.leadingAnchor.constraint(equalTo: previous?.trailingAnchor ?? self.leadingAnchor,constant: 10).isActive = true
            }else if i == middle{
                itemView.leadingAnchor.constraint(equalTo: previous?.trailingAnchor ?? self.leadingAnchor,constant: 10).isActive = true
            }
            else{
                itemView.leadingAnchor.constraint(equalTo: previous?.trailingAnchor ?? self.leadingAnchor).isActive = true
            }
            
           
            
            if i != middle{
                itemView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            }else{
                
                itemView.topAnchor.constraint(equalTo: self.topAnchor,constant: -15).isActive = true
                
            }
            
            
            itemView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap)))
            menuItemsViews.append(itemView)
            previous = itemView
        }
        
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        // self.activateTab(tab: self.activeItem) // activate the first tab
    }
    
    @objc func handleTap(_ sender: UIGestureRecognizer) {
        self.switchTab(from: self.activeItem, to: sender.view!.tag)
    }
    func switchTab(from: Int, to: Int) {
        self.deactivateTab(tab: from)
        self.activateTab(tab: to)
    }
    func activateTab(tab: Int) {
        
        let tabToActivate = menuItemsViews[tab] //self.subviews[tab]
        tabToActivate.itemTitleLabel.textColor = self.appearence.activeColor
        tabToActivate.itemIconView.image = menuItems[tab].selectedIcon
        
        
        self.itemTapped?(tab)
        self.activeItem = tab
    }
    func deactivateTab(tab: Int) {
        let inactiveTab = menuItemsViews[tab]
        inactiveTab.itemTitleLabel.textColor = self.appearence.inactiveColor
        inactiveTab.itemIconView.image = menuItems[tab].icon
        
    }
    
}

class TabItemView: UIView {
    let itemTitleLabel = UILabel(frame: CGRect.zero)
    let itemIconView = UIImageView(frame: CGRect.zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    convenience init(menuItem:TabBarItem,frame:CGRect,appearance : Appearence,isCenter:Bool = false){
        self.init(frame: frame)
        
        itemTitleLabel.text = menuItem.title
        itemTitleLabel.font = appearance.font
        itemTitleLabel.textColor = appearance.inactiveColor
        itemTitleLabel.textAlignment = .center
        itemTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        itemTitleLabel.adjustsFontSizeToFitWidth = true
        itemTitleLabel.clipsToBounds = true
        
        itemIconView.image = menuItem.icon.withRenderingMode(.automatic)
        itemIconView.translatesAutoresizingMaskIntoConstraints = false
        itemIconView.clipsToBounds = true
        itemIconView.contentMode = .scaleAspectFit
        
        //self.layer.backgroundColor = UIColor.white.cgColor
        self.translatesAutoresizingMaskIntoConstraints = false
        //self.clipsToBounds = true
        if isCenter == false{
            self.addSubview(itemIconView)
            self.addSubview(itemTitleLabel)
            NSLayoutConstraint.activate([
                
                itemIconView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                itemIconView.topAnchor.constraint(equalTo: self.topAnchor,constant: 10),
                itemIconView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.30),
                itemIconView.heightAnchor.constraint(equalTo: itemIconView.widthAnchor,multiplier: 1),
                itemTitleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                itemTitleLabel.topAnchor.constraint(equalTo: itemIconView.bottomAnchor,constant: 7)
                
            ])
        }else{
            self.addSubview(itemIconView)
            itemIconView.addShadow(location: .bottom, color: .black, opacity: 0.15, radius: 5.0)
            NSLayoutConstraint.activate([
                //  itemIconView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                itemIconView.topAnchor.constraint(equalTo: self.topAnchor),
                itemIconView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 8),
                itemIconView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -8),
                itemIconView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -15),
                //                itemIconView.widthAnchor.constraint(equalTo: self.widthAnchor,multiplier: 1),
                //                itemIconView.heightAnchor.constraint(equalTo: itemIconView.widthAnchor,multiplier: 1),
                
            ])
        }
        
    }
    
}
