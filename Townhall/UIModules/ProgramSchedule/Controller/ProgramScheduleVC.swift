    //
    //  ProgramScheduleVC.swift
    //  Townhall
    //
    //  Created by Yamen Emon on 7/12/21.
    //

import UIKit

class ProgramScheduleVC: EasyBaseViewController {
    
    
    lazy var mView: ProgramScheduleView = {
        let obj : ScheduleObject = ScheduleObject(name: "Schedule Name", time: "4:00PM - 5:00PM", circleColor: true)
        let view = ProgramScheduleView(title: "Title", programScheduleObj : obj)
        return view
    }()
    var todayCurrentTime : Float?
    override func viewDidLoad() {
        
        super.viewDidLoad()
            // Do any additional setup after loading the view.
        self.view = mView
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mView.arr = EasyDataStore.sharedInstance.dashboardContent?.townhall?.schedule ?? []
        mView.scheduleItem()
    }
}
