//
//  ProgramScheduleView.swift
//  Townhall
//
//  Created by Yamen Emon on 7/12/21.
//

import UIKit
import SwiftUI

struct ScheduleObject {
    
    let name:String?
    let time:String?
    let circleColor : Bool?
  
    init(name: String, time: String,circleColor: Bool) {
        self.name = name
        self.time = time
        self.circleColor = circleColor
    }
}

class ProgramScheduleView: UIView {
    
    var arr = [Schedule]()
    
    var todayCurrentTime : Date?
    var todayDate : Date?
    var hellDate : Date?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(title: String, programScheduleObj : ScheduleObject) {
        self.init(frame:.zero)
        
//
    
        
        backgroundColor = .iceBlue
        
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: -.calculatedHeight(70))
        
        containerView.addSubview(lblHeaderTitle)
        lblHeaderTitle.addAnchorToSuperview(leading: 20, trailing: -20, top: 0)
        lblHeaderTitle.heightAnchor.constraint(equalToConstant: .calculatedHeight(70)).isActive = true
        
        containerView.addSubview(scroller)
        scroller.translatesAutoresizingMaskIntoConstraints = false
        scroller.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        scroller.topAnchor.constraint(equalTo: lblHeaderTitle.bottomAnchor, constant: 0).isActive = true
        scheduleItem()
    }
    let scroller = UIScrollView()
    let containerView = UIView()
    lazy var lblHeaderTitle: UILabel = {
        let lblTitle = UILabel()
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font = .EasyFont(ofSize: 20.0, style: .bold)
        lblTitle.text = "Program Scheduling"
        lblTitle.textColor = .black
        lblTitle.numberOfLines = 0
        lblTitle.textAlignment = .left
     
        return lblTitle
    }()
    func scheduleItem(){
        let cellHeight =  UIScreen.main.bounds.height*0.15
        var tempValue = -1
        let colorArr = [UIColor.lighterPurple1,UIColor.kelleyGreen,UIColor.orangeYellow,UIColor.clearBlue]
         todayCurrentTime = getCurrentTime()
        for (index,item) in arr.enumerated() {
            print(item)
            //
            let arr = item.time?.split(separator: "-")
            let first = arr?.first?.trimmingCharacters(in: .whitespaces) ?? ""
            let last = arr?.last?.trimmingCharacters(in: .whitespaces) ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mma"
            let fdate = dateFormatter.date(from: first)
            let ldate = dateFormatter.date(from: last)
            
            
            let today = Date()
            let dformatter = DateFormatter()
            dformatter.dateFormat = "dd MMM yyyy"
            let todayStr = dformatter.string(from: today)
            todayDate = dformatter.date(from: todayStr)
            
            if  let hellStr = EasyDataStore.sharedInstance.dashboardContent?.townhall?.date{
                hellDate = dformatter.date(from: hellStr)
            }
     
            if tempValue == 3 {
                tempValue = 0
            }
            else {
                tempValue = tempValue + 1
            }
            
            let cell = UIView()
            scroller.addSubview(cell)
            cell.backgroundColor = .clear
            cell.translatesAutoresizingMaskIntoConstraints = false
            cell.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15).isActive = true
            cell.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
            cell.heightAnchor.constraint(equalToConstant: cellHeight+1).isActive = true
            cell.topAnchor.constraint(equalTo: scroller.topAnchor, constant: CGFloat(index)*cellHeight).isActive = true
            
            let scrollHeight = (CGFloat(index)*cellHeight) + cellHeight
            
            
            let stepperLine = UIView()
            stepperLine.translatesAutoresizingMaskIntoConstraints = false
            cell.addSubview(stepperLine)
            stepperLine.backgroundColor = .paleGrey
            if index == 0 {
                stepperLine.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 0).isActive = true
                stepperLine.topAnchor.constraint(equalTo: cell.topAnchor, constant: cellHeight/2).isActive = true
                
            }
            else {
                stepperLine.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 0).isActive = true
                stepperLine.topAnchor.constraint(equalTo: cell.topAnchor, constant: 0).isActive = true
                
            }
            stepperLine.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 0).isActive = true
            stepperLine.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: 0).isActive = true
            stepperLine.widthAnchor.constraint(equalToConstant: 2).isActive = true
            
            let circleView = UIView()
            circleView.translatesAutoresizingMaskIntoConstraints = false
            stepperLine.addSubview(circleView)
            if index == 0 {
                circleView.centerXAnchor.constraint(equalTo: stepperLine.centerXAnchor).isActive = true
                circleView.centerYAnchor.constraint(equalTo: stepperLine.topAnchor).isActive = true
                
            }
            else {
                circleView.centerXAnchor.constraint(equalTo: stepperLine.centerXAnchor).isActive = true
                circleView.centerYAnchor.constraint(equalTo: stepperLine.centerYAnchor).isActive = true
            }
            circleView.widthAnchor.constraint(equalToConstant: .calculatedWidth(17)).isActive = true
            circleView.heightAnchor.constraint(equalToConstant: .calculatedHeight(17)).isActive = true
            circleView.backgroundColor = .iceBlue
            if let hellday = hellDate{
                if todayDate?.compare(hellday) == .orderedSame{
                    if let today = todayCurrentTime, let startDate = fdate, let endDate = ldate{
                        
                        if today.compare(startDate) == .orderedDescending && today.compare(endDate) == .orderedAscending{
                            circleView.backgroundColor = colorArr[tempValue]
                        }else{
                            circleView.backgroundColor = .iceBlue
                        }
                    }
                }
            }
           
            
           
//            if item.circleColor == true {
//                circleView.backgroundColor = colorArr[tempValue]
//            }
//            else{
//
//
//            }
            circleView.layer.borderColor = UIColor.paleGrey.cgColor
            circleView.layer.borderWidth = 2
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                circleView.layer.cornerRadius = circleView.frame.size.width/2
            }
            
            let contentView = UIView()
            contentView.translatesAutoresizingMaskIntoConstraints = false
            cell.addSubview(contentView)
            contentView.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 20).isActive = true
            contentView.trailingAnchor.constraint(equalTo: cell.trailingAnchor, constant: -10).isActive = true
            contentView.topAnchor.constraint(equalTo: cell.topAnchor, constant: 10).isActive = true
            contentView.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: -10).isActive = true
            contentView.backgroundColor = colorArr[tempValue]
            contentView.layer.cornerRadius = 10
            
            
            
            let topLabel = UILabel()
            contentView.addSubview(topLabel)
            topLabel.translatesAutoresizingMaskIntoConstraints = false
            topLabel.addAnchorToSuperview(leading: 10, trailing: -10, top: 0)
            topLabel.text = item.title ?? ""
            topLabel.textColor = .white
            topLabel.font = .EasyFont(ofSize: 18, style: .bold)
            topLabel.numberOfLines = 2
            topLabel.backgroundColor = .clear
            topLabel.preferredMaxLayoutWidth = 2
            
            
            let bottomView = UIView()
            contentView.addSubview(bottomView)
            bottomView.addAnchorToSuperview(leading: 0, trailing: 0,heightMultiplier: 0.41)
            bottomView.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 0).isActive = true
            bottomView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
            bottomView.backgroundColor = .white
            bottomView.clipsToBounds = true
            bottomView.layer.cornerRadius = 10
            bottomView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            
            
            let timeLabel = UILabel()
            bottomView.addSubview(timeLabel)
            timeLabel.translatesAutoresizingMaskIntoConstraints = false
            timeLabel.addAnchorToSuperview(leading: 10, trailing: -10)
            timeLabel.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 0).isActive = true
            timeLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
            timeLabel.clipsToBounds = true
            timeLabel.text = item.time ?? ""
            timeLabel.textColor = .slateGrey
            timeLabel.font = .EasyFont(ofSize: 12, style: .medium)
            
            
            
            
            scroller.contentSize = CGSize(width: self.frame.size.width, height: scrollHeight)
            
        }
    }
    func  getCurrentTime() -> Date {

        let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mma"//"EE" to get short style
            let mydt = dateFormatter.string(from: date)
        let final = dateFormatter.date(from: mydt) ?? date
            
            return final

    }
    
}
