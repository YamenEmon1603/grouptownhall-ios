//
//  ServiceListCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 18/11/21.
//

import UIKit

class ServiceListCell: UICollectionViewCell {
    static let identifier = "ServiceListCell"
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        self.contentView.addSubview(logoIv)
        self.logoIv.addAnchorToSuperview(heightMultiplier: 0.5,centeredVertically: -5, centeredHorizontally: 0, heightWidthRatio: 1)
        
        
        self.contentView.addSubview(label)
        label.addAnchorToSuperview(leading: 8, trailing: -8,bottom: -15 )
        label.topAnchor.constraint(equalTo: logoIv.bottomAnchor ,constant: 10).isActive = true
        
       
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var logoIv: UIImageView = {
        let iv = UIImageView()
        
        return iv
    }()
    let label : UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .regular)
        label.textColor = .slateGrey
        label.textAlignment = .center
        return label
    }()
}
