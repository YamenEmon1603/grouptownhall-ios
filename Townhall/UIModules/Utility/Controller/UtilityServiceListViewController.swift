//
//  UtilityBillListViewController.swift
//  Padma Bank
//
//  Created by Raihan on 4/4/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import Kingfisher
class UtilityServiceListViewController: EasyBaseViewController {
    var serviceList : [ServiceList]?
    var selectedService:ServiceList?
    lazy var  contentView: UIlityServiceListView = {
        let view = UIlityServiceListView()
        view.navView.backButtonCallBack = {
            (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
            self.navigationController?.popViewController(animated: true)
            
        }
        view.collectionView.delegate  = self
        view.collectionView.dataSource = self
        view.collectionView.register(ServiceListCell.self, forCellWithReuseIdentifier: ServiceListCell.identifier)
        return view
    }()
    let typeSelectionView = ConectionTypeSelctionView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = contentView
        self.typeSelectionView.infoLabel.text = "Select Bill Type"
        getUtilityServiceList()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
    }
    
    
}
extension UtilityServiceListViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  serviceList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ServiceListCell.identifier , for: indexPath) as! ServiceListCell
        cell.logoIv.setImageFromURl(imageUrl: serviceList?[indexPath.row].logo ?? "", cacheEnabled: true)
        cell.label.text = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? serviceList?[indexPath.row].title : serviceList?[indexPath.row].bnTitle
        let _ =   cell.contentView.addBorder(side: .right, color: indexPath.item % 2 == 0 ?  .paleLilac : .clear, width: 1)
        if let count  = serviceList?.count {
            let _ =  cell.contentView.addBorder(side: .bottom, color: (indexPath.item  == count - 1 || (count % 2 == 0 && indexPath.item  == count - 2)) ? .clear :  .paleLilac, width: 1)
        }
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(collectionView.frame.width)
        return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.width/2)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let service = serviceList?[indexPath.item]
        if let types = service?.utilityBillTypes{
            if types.count == 1{
                let vc = BillFormViewController()
                vc.billType =  types.first
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                typeSelectionView.billTypes = types.map({ SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? $0.title ?? "" : $0.bnTitle ?? ""})
                self.contentView.addSubview(typeSelectionView)
                typeSelectionView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
                typeSelectionView.billTypeSelectionCallback = { index in
                    print(index)
                    let billType = types[index]
                }
            }
        }
    }
    
    
}


//MARK: -APICALL

extension UtilityServiceListViewController{
    func getUtilityServiceList(){
        WebServiceHandler.shared.getUtilityServiceList(completion: { result in
            switch result {
            case .success(let response):
                if response.code == 200{
                    if let data = response.billData?.first?.serviceLists?.filter({$0.serviceCategoryID == 1}){
                        self.serviceList = data
                        self.contentView.collectionView.reloadData()
                    }
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
        
    }
}

class UIlityServiceListView : UIView{
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.075)
        
        containerView.addSubview(collectionView)
        collectionView.addAnchorToSuperview(leading: 15, trailing: -15, bottom: 0)
        collectionView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 15).isActive = true
        
        
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "utility_bill".easyLocalized(), rightItem: nil)
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        
        return navView
    }()
    
    lazy var  collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        let cv = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.allowsSelection = true
        return cv
    }()
}
