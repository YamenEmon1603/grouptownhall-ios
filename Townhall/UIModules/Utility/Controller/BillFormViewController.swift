//
//  BillFormViewController.swift
//  Padma Bank
//
//  Created by Mausum Nandy on 4/25/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import Kingfisher

class BillFormViewController: EasyBaseViewController {
    
    lazy var billFormView: BillFormView = {
        let formView = BillFormView()
        formView.navView.title = "\(SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? billType?.serviceList?.title ?? "" : billType?.serviceList?.bnTitle ?? "" ) \("utility_bill".easyLocalized())"
        formView.imageView.setImageFromURl(imageUrl: billType?.serviceList?.logo ?? "", cacheEnabled: true)
        formView.navView.backButtonCallBack = {self.navigationController?.popViewController(animated: true)}
        formView.proceedCallBack = viewBillAction
        return formView
    }()
    
    var billType : UtilityBillType?

    var selectedOptions: [Int: [SelectOptionItem]] = [:]
    var keyboardObserver : KeyboardObserver!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = billFormView
        setForm()
        keyboardObserver = KeyboardObserver(for: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
    }
    
    
    func  textFieldDidBeginEditing( _ sender : EasyTextFieldView){
        if sender.text?.isEmpty == true{
            sender.text = selectedOptions[sender.tag - 100]?[0].value
            selectedOptions[sender.tag - 100]?[0].isSelected = true
        }
    }
    
    
    private func setForm(){
        if let params = billType?.parameterLists {
            let lang = SSLComLanguageHandler.sharedInstance.getCurrentLanguage()
            for (index,param) in params.enumerated() {
                if (param.type != "hidden" ){
                    let textView = EasyTextFieldView(title: (lang == .English ? param.level : param.bnLevel) ?? "")
                    textView.backgroundColor = UIColor.iceBlue
                    textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: billFormView)
                    textView.shadow = false
                    if param.type == "text"{
                        textView.keyBoardType = param.dataType?.keyboardType ?? .default
                    }else if param.type == "select"{
                        let picker = UIPickerView()
                        picker.tag = index
                        textView.customInputView = picker
                        picker.delegate = self
                        picker.dataSource = self
                        textView.textEditingBeginCallBack =  textFieldDidBeginEditing(_:)
                        selectedOptions[index] =  param.selectOptions
                    }
                    
                    textView.tag = 100+index
                    billFormView.stackView.addArrangedSubview(textView)
                    textView.heightAnchor.constraint(equalToConstant: .calculatedHeight(60)).isActive = true
                }
                
            }
        }
        
    }
    
    func viewBillAction(){
        self.view.endEditing(true)
        var paramsForApi :[String:Any] = [:]
       
        if let params = billType?.parameterLists {
            let lang = SSLComLanguageHandler.sharedInstance.getCurrentLanguage()
            paramsForApi["utility_bill_id"] = "\(params.first?.utilityBillID ?? 0)"
            for (index,param) in params.enumerated() {
                if (param.type != "hidden" ){
                    if let view = billFormView.stackView.arrangedSubviews.first(where:{$0.tag == index+100}) as? EasyTextFieldView{
                        if param.type == "text" {
                            guard let text = view.text , text.isEmpty == false else{
                                showToast(message: String(format: "text_required".easyLocalized() , (lang == .English ? param.level ?? "" : param.bnLevel ?? "")))
                                return
                            }
                            paramsForApi[param.fieldName ?? ""] = view.text
                        }else if param.type == "select"{
                            guard let option = selectedOptions[index]?.first(where:{$0.isSelected == true}) else{
                                showToast(message: String(format: "select_required".easyLocalized() , (lang == .English ? param.level ?? "" : param.bnLevel ?? "")))
                                return
                            }
                            paramsForApi[param.fieldName ?? ""] = option.name
                        }else{
                            self.showToast(message: "\(lang == .English ? param.level ?? "" : param.bnLevel ?? "") is required")
                        }
                        
                    }
                    
                }
            }
            requestBillInfo(params: paramsForApi)
        }
    }
    
    
}
extension BillFormViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        var contentInset:UIEdgeInsets = billFormView.scrollView.contentInset
        contentInset.bottom = height + 20
        billFormView.scrollView.contentInset = contentInset
    }
    
    func keybaordWillHide() {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        billFormView.scrollView.contentInset = contentInset
    }
    
    
}

//MARK:- UIPickerViewDelegate,UIPickerViewDataSource
extension BillFormViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return selectedOptions[pickerView.tag]?.count ?? 0
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label1 = UILabel()
        label1.text = selectedOptions[pickerView.tag]?[row].value
        label1.font = .EasyFont(ofSize: 15, style: .regular)
        label1.textAlignment = .center
        label1.textColor = .black
        return label1
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let tf = self.view.viewWithTag(pickerView.tag+100) as? EasyTextFieldView, let options = selectedOptions[pickerView.tag]{
            tf.text = selectedOptions[pickerView.tag]?[row].value
            for (index,_ ) in options.enumerated(){
                selectedOptions[pickerView.tag]?[index].isSelected = index == row
            }
            
        }
    }
    
    
}

//MARK:- API CALL
extension BillFormViewController{
    func requestBillInfo(params:[String:Any]) {
        WebServiceHandler.shared.fetchUtilityBillInfo(params: params, completion: { result in
            switch result {
            case .success(let response):
                if response.code == 200{
                    let vc = BillDetailsViewController()
                    vc.billType = self.billType
                    vc.billInfo = response.billData
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
      /*  let str = "{\"data\": {\"status\": \"api_success\",\"status_code\": \"000\",\"status_title\": \"API response is successful from Biller\",\"lid\": \"DEDE5E54C20D4FB93202002251066\",\"transaction_id\":\"T15826130425E5445405E545\",\"data\": {          \"account_number\": \"31511262\",\"bill_number\": \"111631511262\",\"zone_code\": \"14\", \"due_date\": \"13-DEC-16\",\"organization_code\": \"0\",\"tariff\": \"A\",\"settlement_account\": \"revenue\",            \"bill_amount\": \"2248\",\"vat_amount\": \"113\",\"stamp_amount\": \"10\",\"lpc_amount\": \"119\",            \"total_amount\": \"2490\"}},\"message\": [],\"code\": 200}"
        
        if let data = str.data(using: .utf8){
            do {
                let res  = try JSONDecoder().decode(BillInfoResponseModel.self, from: data)
                let vc = BillDetailsViewController()
               
                self.navigationController?.pushViewController(vc, animated: true)
            } catch  {
                print(error)
               
            }
            
        }*/
        
        
       
    }
}






class BillFormView : UIView {
    
    var proceedCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView = UIScrollView()
    let stackView = UIStackView()
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.07)
        
        
        
        containerView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 8).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
    
    //MARK: COMPONENTS
    
    
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        
        let holderView = UIView()
        holderView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(holderView)
        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
        
        holderView.addSubview(imageView)
        imageView.addAnchorToSuperview( top: 15,widthMutiplier: 0.3, centeredHorizontally: 0, heightWidthRatio: 1)
        
        
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 15
        
        holderView.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 15, trailing: -15, bottom: 0)
        stackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 15).isActive = true
        
        
        
        return formView
    }()
    
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    
    let imageView  = UIImageView()
    
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:  "utility_bill".easyLocalized(), rightItem: nil)
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        
        return navView
    }()
    
    
}




extension BillFormView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}
