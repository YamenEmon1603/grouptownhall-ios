//
//  BillDetailsViewController.swift
//  Padma Bank
//
//  Created by Mausum Nandy on 4/26/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import UIKit
import SSLCommerzSDK
class BillDetailsViewController: EasyBaseViewController {
    
    lazy var detailsView : BillFormView = {
        let formView = BillFormView()
        formView.navView.title = "\(SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? billType?.serviceList?.title ?? "" : billType?.serviceList?.bnTitle ?? "" ) \("utility_bill".easyLocalized())"
        formView.imageView.setImageFromURl(imageUrl: billType?.serviceList?.logo ?? "", cacheEnabled: true)
        formView.navView.backButtonCallBack = {self.navigationController?.popViewController(animated: true)}
        formView.stackView.spacing = 0
        formView.proceedCallBack = requestBillPayment
        return formView
    }()
    var billInfo : BillInfo?
    var billType : UtilityBillType?
    var sslCommerz : SSLCommerz!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = detailsView
        makeBill()
        
    }
    
    func makeBill(){
        if let info  = billInfo?.bill{
            info.sorted(by: { $0.isAmountField == false && $1.isAmountField == true }).forEach { elem in
                let v = BillItemView(title: elem.key ?? "", value: elem.isAmountField  == false ?  "\(elem.value ?? "")" : "৳\(elem.value ?? "")")
                v.heightAnchor.constraint(equalToConstant: .calculatedHeight(55)).isActive = true
                detailsView.stackView.addArrangedSubview(v)
            }
            detailsView.stackView.addArrangedSubview(labelNote)
        }
        
    }
    let labelNote : EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.font = .EasyFont(ofSize: 15.0, style: .medium)
        label.textColor = .black
        label.textAlignment = .left
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.text = "gateway_fee_note".easyLocalized()
        label.sizeToFit()
        return label
    }()
    
}


//MARK: - API CALL
extension BillDetailsViewController{
    func requestBillPayment() {
        let params : [String:Any] = ["utility_bill_id" : billType?.id ?? 0 , "transaction_id" : billInfo?.transactionId ?? "" , "is_mobile" : 1]
        WebServiceHandler.shared.payUtilityBill(params: params, completion: { result in
            switch result {
            case .success(let response):
                if response.code == 200 ,let data = response.sslData{
                    self.loadSDK(ssldata: data)
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    func errorAlert(){
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "go_home".easyLocalized(), tag: 1001)
        let btn = PopupButton (type: .bordered, title: "try_again".easyLocalized(), tag: 1002)
        popup.options = [.image(image: .failed),.title(str: "rr_payment_failed".easyLocalized(),color:.tomato),.subtitle(str: "please_try_again".easyLocalized(),color: .slateGrey),.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
    }
  
}

//MARK: - SSLCommerzDelegate
extension BillDetailsViewController : SSLCommerzDelegate,EasyPopUpDelegate{
    func popupDismissedWith(button: PopupButton, consent: Bool?, rating: Int?) {
        if button.tag == 1001{
            (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
            self.navigationController?.popToRootViewController(animated: true)
        }else{
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func loadSDK(ssldata:SSLComerceData){
        if let storeid = ssldata.storeId , let storePass = ssldata.storePassword , let transId = ssldata.transactionId,  let ipnUrl = ssldata.ipnUrl{
            let integrationInfo =  IntegrationInformation(storeID: storeid, storePassword: storePass, totalAmount: ssldata.amount ?? 0.0, currency: "BDT", transactionId: transId, productCategory: ssldata.serviceTitle ?? "")
            integrationInfo.ipnURL = ipnUrl
            sslCommerz = SSLCommerz.init(integrationInformation: integrationInfo)
            sslCommerz.delegate = self
            if let user = EasyDataStore.sharedInstance.user{
                sslCommerz.customerInformation = CustomerInformation(customerName:user.name ?? "", customerEmail: user.email ?? "", customerAddressOne: user.address ?? "", customerCity: "", customerPostCode: "", customerCountry: "BD", customerPhone:user.mobile ?? "")
            }
            
            
            sslCommerz.additionalInformation = AdditionalInformation()
            sslCommerz.additionalInformation?.paramB = ssldata.valueB
            sslCommerz.additionalInformation?.paramC = ssldata.valueC
            
            sslCommerz.start(in: self, shouldRunInTestMode: Constant.isDev)
            
        }
        else{
            showToast(message: "Something went wrong,please try again later")
        }
    }
    func sdkClosed(reason: String) {
        self.showToast(message: reason)
    }
    func transactionCompleted(withTransactionData transactionData: TransactionDetails?) {
        if(transactionData?.status?.lowercased() == "valid" || transactionData?.status?.lowercased() == "validated" ){
            let popup = EasyPopUpViewController()
            popup.delegate = self
            let lang = SSLComLanguageHandler.sharedInstance
            let go = PopupButton(type: .fill, title: "done".easyLocalized(), tag: 1001)
            let message = String(format: "bill_alert_success_message".easyLocalized() , lang.getLocalizedDigits(transactionData?.amount) ?? "" , lang.getCurrentLanguage() == .English ? billType?.serviceList?.title ?? "" : billType?.serviceList?.bnTitle ?? "", transactionData?.tran_id ?? ""  )
            popup.options = [.image(image: .failed),.title(str: "rr_payment_successful".easyLocalized(),color:.trueGreen),.subtitle(str: message, color: .battleshipGrey),.buttons(buttons: [go])]
            popup.modalPresentationStyle = .overCurrentContext
            self.present(popup, animated: true, completion: nil)
        }else{
            errorAlert()
        }
    }
}



class BillItemView: UIView {
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let labelKey : UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .regular)
        label.textColor = .battleshipGrey
        label.textAlignment = .left
        return label
    }()
    let labelValue : UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.textAlignment = .right
        
        return label
    }()
    let dashedView = UIView()
    convenience init(title:String , value:String){
        self.init(frame: .zero)
        
        addSubview(dashedView)
        dashedView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        dashedView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        labelKey.text  = title
        addSubview(labelKey)
        labelKey.addAnchorToSuperview(leading: 10 ,widthMutiplier: 0.5, centeredVertically: 0)
        
        labelValue.text = value
        addSubview(labelValue)
        labelValue.addAnchorToSuperview( trailing: -10, widthMutiplier: 0.5, centeredVertically: 0)
        
        translatesAutoresizingMaskIntoConstraints = false
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        dashedView.createDottedLine(width: 1.0, color: UIColor.iceBlue.cgColor)
    }
}

extension UIView {
    func createDottedLine(width: CGFloat, color: CGColor) {
        let caShapeLayer = CAShapeLayer()
        caShapeLayer.strokeColor = color
        caShapeLayer.lineWidth = width
        caShapeLayer.lineDashPattern = [2,3]
        let cgPath = CGMutablePath()
        let cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: self.frame.width, y: 0)]
        cgPath.addLines(between: cgPoint)
        caShapeLayer.path = cgPath
        layer.addSublayer(caShapeLayer)
    }
}

