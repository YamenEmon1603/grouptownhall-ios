    //
    //  ProgramInstructionView.swift
    //  Townhall
    //
    //  Created by Yamen Emon on 9/12/21.
    //

import UIKit

class ProgramInstructionView: UIView {
    var backButtonCallBack:(()->Void)?
    
    

    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar(title: "Program Instructions", leadingBtn: 0)
        navView.lblTitle.font = .EasyFont(ofSize: 17.0, style: .medium)
        let _ = navView.addBorder(side: .bottom, color: .lightPeriwinkle, width: 0.5)
        return navView
    }()
    
    var staticArrData = [
        "Get Registered with easy app",
        "Please upload your pictures",
        "Connect with our chatbot",
        "Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s",
        "Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s"]
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupView() {
        
        
        
        
    }
   
    func buttonAction(sender : UIButton) {
        if sender.tag == 1 {
            
        }
        else if sender.tag == 2 {
            
        } else {
            
        }
    }
    
}
