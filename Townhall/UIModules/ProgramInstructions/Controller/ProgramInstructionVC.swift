//
//  ProgramInstructionVC.swift
//  Townhall
//
//  Created by Yamen Emon on 9/12/21.
//

import UIKit

class ProgramInstructionVC: EasyBaseViewController {
    
    override var hidesBottomBarWhenPushed: Bool {
        get {
            return navigationController?.topViewController == self
        }
        set {
            super.hidesBottomBarWhenPushed = newValue
        }
    }
    @objc func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.view = mView
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        setupView()
    }
    
    func setupView(){
        
        for view in self.view.subviews {
            view.removeFromSuperview()
        }
        
        var staticArrData = [
            "Get Registered with easy app",
            "Please upload your pictures",
            "Connect with our chatbot",]
        
        let navView = UIView(frame: CGRect(x: 0, y: statusBarHeight, width: self.view.frame.size.width, height: Constant.screenHeight * 0.08))
        self.view.addSubview(navView)
        navView.backgroundColor = .clear
        
        
        let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 50*Constant.factX, height: navView.frame.size.height))
        navView.addSubview(backBtn)
        backBtn.backgroundColor = .clear
        backBtn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let nameLabel = UILabel(frame: CGRect(x: backBtn.frame.origin.x + backBtn.frame.size.width + 10*Constant.factX, y: 0, width: navView.frame.size.width -  backBtn.frame.size.width - 10, height: navView.frame.size.height))
        nameLabel.text = "Program Instructions"
        nameLabel.font = .EasyFont(ofSize: 15, style: .bold)
        navView.addSubview(nameLabel)
        
        
        let image = UIImage(named: "icback")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        backBtn.addSubview(imageView)
        imageView.center = backBtn.center
        imageView.image = image
        
        let containerView = UIView(frame: CGRect(x: 0, y: statusBarHeight+navView.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height - (statusBarHeight + navView.frame.size.height + bottomBarHeight + 10*Constant.factY)))
        self.view.addSubview(containerView)
        containerView.backgroundColor = .clear
        
        
        let scroller  = UIScrollView(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
        containerView.addSubview(scroller)
        
        var currentY : CGFloat = 0
        let circleHeightWidth : CGFloat = .calculatedWidth(36)
        let cellHeight = circleHeightWidth + Constant.gap_Y
        
        if let data = EasyDataStore.sharedInstance.dashboardContent?.townhall?.instructions {
            for itemData in data {
                if let str = itemData.instructionDescription {
                    staticArrData.append(str)
                }
            }
        }
        
        for (index,str) in staticArrData.enumerated() {
            print(str)
            
            let cell = UIView(frame: CGRect(x: 10*Constant.factX, y: currentY, width: containerView.frame.size.width - 10*Constant.factX, height: cellHeight))
            scroller.addSubview(cell)
            cell.backgroundColor = .white
            
            
            let circleLabel = UILabel(frame: CGRect(x: 0, y: Constant.gap_Y, width: circleHeightWidth, height: circleHeightWidth))
            cell.addSubview(circleLabel)
            circleLabel.text = "\(index + 1)"
            
            if index < 2 {
                circleLabel.backgroundColor = .washedOutGreen
                circleLabel.textColor = .trueGreen
                
            }else if index == 2{
                if  EasyDataStore.sharedInstance.socialTapStatus == true{
                    circleLabel.backgroundColor = .washedOutGreen
                    circleLabel.textColor = .trueGreen
                }else{
                    circleLabel.backgroundColor = .iceBlue
                    circleLabel.textColor = .clearBlue
                }
            }else {
                circleLabel.backgroundColor = .iceBlue
                circleLabel.textColor = .clearBlue
            }
            
            circleLabel.font = .EasyFont(ofSize: 13, style: .bold)
            circleLabel.textAlignment = .center
            circleLabel.clipsToBounds = true
            circleLabel.layer.cornerRadius = circleLabel.frame.size.width/2
            
            
            let textLabel = UILabel(frame: CGRect(x: circleLabel.frame.origin.x + circleLabel.frame.size.width + 10*Constant.factX, y: circleLabel.frame.minY+5, width: cell.frame.size.width - (15*Constant.factX + circleLabel.frame.origin.x + circleLabel.frame.size.width + 5*Constant.factX), height: 30*Constant.factY))
            cell.addSubview(textLabel)
            
            textLabel.backgroundColor = .clear
            textLabel.numberOfLines = 5
            
            let imageAttachment = NSTextAttachment()
            imageAttachment.image = UIImage(named:"green_check_mark")
            imageAttachment.bounds = CGRect(x: 0, y: 0, width: imageAttachment.image!.size.width + 2, height: imageAttachment.image!.size.height + 2)
            let attachmentString = NSAttributedString(attachment: imageAttachment)
            let completeText = NSMutableAttributedString(string: "\(str) ")
            //            if index < 3 {
            //                completeText.append(attachmentString)
            //            }
            
            if index < 2 {
                completeText.append(attachmentString)
                
                
            }else if index == 2{
                if  EasyDataStore.sharedInstance.socialTapStatus == true{
                    completeText.append(attachmentString)
                    
                }else{
                    
                }
            }else {
                
            }
            textLabel.attributedText = completeText
            textLabel.sizeToFit()
            
            
            
            
            if index == 2 {
                let btnView = UIView(frame: CGRect(x: textLabel.frame.origin.x, y: textLabel.frame.origin.y + textLabel.frame.size.height + 5 * Constant.factY, width: cell.frame.size.width - (textLabel.frame.origin.x + 10*Constant.factX), height: 40*Constant.factY))
                cell.translatesAutoresizingMaskIntoConstraints = false
                cell.addSubview(btnView)
                btnView.backgroundColor = .clear
                
                let buttonWidth = (btnView.frame.size.width - 15*Constant.factX)/3
                
                
                let whatsAppView = UIView(frame: CGRect(x:0, y: 5*Constant.factY, width: buttonWidth, height: 30*Constant.factY) )
                whatsAppView.translatesAutoresizingMaskIntoConstraints = true
                btnView.addSubview(whatsAppView)
                whatsAppView.backgroundColor = .iceBlue
                whatsAppView.layer.cornerRadius = whatsAppView.frame.size.height/2
                
                
                let messengerAppView = UIView(frame: CGRect(x: whatsAppView.frame.origin.x + buttonWidth + 5*Constant.factX, y: 5*Constant.factY, width: buttonWidth, height: 30*Constant.factY) )
                messengerAppView.translatesAutoresizingMaskIntoConstraints = true
                btnView.addSubview(messengerAppView)
                messengerAppView.backgroundColor = .iceBlue
                messengerAppView.layer.cornerRadius = messengerAppView.frame.size.height/2
                
                let telegramAppView = UIView(frame: CGRect(x: messengerAppView.frame.origin.x + buttonWidth + 5*Constant.factX, y: 5*Constant.factY, width: buttonWidth, height: 30*Constant.factY) )
                telegramAppView.translatesAutoresizingMaskIntoConstraints = true
                btnView.addSubview(telegramAppView)
                telegramAppView.backgroundColor = .iceBlue
                telegramAppView.layer.cornerRadius = telegramAppView.frame.size.height/2
                
                
                
                let whatsAppBtn = UIButton(frame: CGRect(x: 0, y: 0, width: whatsAppView.frame.size.width, height: whatsAppView.frame.size.height))
                whatsAppView.addSubview(whatsAppBtn)
                
                whatsAppBtn.setImage(UIImage(named: "whatsAppIcon"), for: .normal)
                whatsAppBtn.setTitle("WhatsApp", for: .normal)
                whatsAppBtn.setTitleColor(.black, for: .normal)
                whatsAppBtn.titleLabel?.font =  .EasyFont(ofSize: 12, style: .medium)
                
                let messengerBtn = UIButton(frame: CGRect(x: 0, y: 0, width: messengerAppView.frame.size.width, height: messengerAppView.frame.size.height))
                messengerAppView.addSubview(messengerBtn)
                messengerBtn.setImage(UIImage(named: "messengerIcon"), for: .normal)
                messengerBtn.setTitle(" Messenger", for: .normal)
                messengerBtn.setTitleColor(.black, for: .normal)
                messengerBtn.titleLabel?.font =  .EasyFont(ofSize: 12, style: .medium)
                
                let telegramBtn = UIButton(frame: CGRect(x: 0, y: 0, width: telegramAppView.frame.size.width, height: telegramAppView.frame.size.height))
                telegramAppView.addSubview(telegramBtn)
                telegramBtn.setImage(UIImage(named: "telegramIcon"), for: .normal)
                telegramBtn.setTitle(" Telegram", for: .normal)
                telegramBtn.setTitleColor(.black, for: .normal)
                telegramBtn.titleLabel?.font =  .EasyFont(ofSize: 12, style: .medium)
                
                
                whatsAppBtn.tag = 1
                messengerBtn.tag = 2
                telegramBtn.tag = 3
                
                whatsAppBtn.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
                messengerBtn.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
                telegramBtn.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
                
                
                let height  = btnView.frame.origin.y + btnView.frame.size.height
                
                cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.frame.size.width, height: height)
                
                currentY = currentY + cell.frame.size.height + 5
            }
            else {
                var height  = textLabel.frame.origin.y + textLabel.frame.size.height + Constant.gap_Y*2
                if circleHeightWidth > height {
                    height = circleHeightWidth + Constant.gap_Y*2
                }
                cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.frame.size.width, height: height)
                
                currentY = currentY + cell.frame.size.height + 5
            }
            
            print("Cell Height : \(currentY)")
            scroller.contentSize = CGSize(width: self.view.frame.size.width, height: currentY)
        }
        
        
    }
    @objc func buttonAction(sender : UIButton) {
        EasyDataStore.sharedInstance.socialTapStatus = true
        setupView()
        
        switch sender.tag {
            case 1:
                print("WhatsAPP")
                if let link = EasyDataStore.sharedInstance.dashboardContent?.townhall?.bot?.whatsapp{
                    openSocial(mUrl: link)
                }
            case 2:
                print("Messenger")
                if let link = EasyDataStore.sharedInstance.dashboardContent?.townhall?.bot?.messenger{
                    openSocial(mUrl: link)
                }
            case 3:
            if let link = EasyDataStore.sharedInstance.dashboardContent?.townhall?.bot?.telegram{
                let url = URL(fileURLWithPath: link)
                let path  = url.deletingPathExtension().lastPathComponent
                let appURL = NSURL(string: "tg://resolve?domain=\(path)")!
                let webURL = NSURL(string: "https://t.me/\(path)")!
                if UIApplication.shared.canOpenURL(appURL as URL) {
                    openSocial(mUrl : appURL.absoluteString ?? "")
                }
                else {
                    openSocial(mUrl : webURL.absoluteString ?? "")
                }
                
                
            }
            default:
                break
            }
            
        }
        func openSocial(mUrl : String){
            
            
            if let appURL = URL(string: "\(mUrl)"){
                if UIApplication.shared.canOpenURL(appURL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(appURL)
                    }
                }
            }
            
        }
    }
