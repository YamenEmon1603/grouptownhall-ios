//
//  ForceUpdateView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 22/11/21.
//

import Foundation
import UIKit
class ForceUpdateView: EasyBaseView {
    var bottomContraint : NSLayoutConstraint?
    var updateCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .clear
        
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 20, trailing: -20, top: 60, bottom: -60)
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var contentView: UIView = {
        let contentView = UIView()
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 10
        contentView.addSubview(imageViewUpdate)
        contentView.addSubview(updateHeaderLabel)
        contentView.addSubview(subTextView)
        contentView.addSubview(btnUpdate)
      
        
        
        imageViewUpdate.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.4, heightWidthRatio: 1)
        imageViewUpdate.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20).isActive = true
        
        updateHeaderLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        updateHeaderLabel.topAnchor.constraint(equalTo: imageViewUpdate.bottomAnchor, constant: 20).isActive = true
        updateHeaderLabel.heightAnchor.constraint(equalToConstant: .calculatedHeight(40)).isActive = true
        
        subTextView.addAnchorToSuperview(leading: 20, trailing: -20)
        subTextView.topAnchor.constraint(equalTo: updateHeaderLabel.bottomAnchor, constant: 10).isActive = true
        subTextView.bottomAnchor.constraint(equalTo: btnUpdate.topAnchor, constant: -20).isActive = true
        
        btnUpdate.addAnchorToSuperview(leading: 20, trailing: -20,bottom: -20)
        btnUpdate.heightAnchor.constraint(equalToConstant: .calculatedHeight(50)).isActive = true
        
        return contentView
    }()
    
    let imageViewUpdate: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "rocket")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    let updateHeaderLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 20.0, style: .bold)
        label.textColor = .black
        label.textAlignment = .center
        label.text = "new_update_is_available".easyLocalized()
        return label
    }()
    
    lazy var   subTextView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.font = .EasyFont(ofSize: 14, style: .regular)
        textView.textAlignment = .justified
        textView.textColor = .slateGrey
        return textView
    }()
    
    
    private let btnUpdate:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "update_now".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 14, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.layer.cornerRadius = 10
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        updateCallBack?()
    }
}

