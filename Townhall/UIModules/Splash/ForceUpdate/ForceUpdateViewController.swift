//
//  ForceUpdateViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 22/11/21.
//

import UIKit

class ForceUpdateViewController : EasyBaseViewController {
    var newVersion = ""
    var oldVersion = ""
    lazy var mView: ForceUpdateView = {
        let mView = ForceUpdateView()
        mView.updateCallBack = update
       
        return mView
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        mView.subTextView.text = String(format: "the_version_of_this_application".easyLocalized() ,  EasyUtils.shared.getLocalizedDigits(oldVersion) ?? "", EasyUtils.shared.getLocalizedDigits(newVersion) ?? "")
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
   
    func update(){

        UIApplication.shared.open(URL(string: "itms://itunes.apple.com/app/id1602373315")!, options: [:])
    }
}


