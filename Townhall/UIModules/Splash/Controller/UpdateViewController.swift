//
//  UpdateViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/7/21.
//

import UIKit

class UpdateViewController: EasyBaseViewController {
    lazy var updateView: UpdateView = {
        let updateView = UpdateView()
        return updateView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view =  updateView

    }
}
