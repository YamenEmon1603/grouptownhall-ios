//
//  SplashScreenViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import UIKit

class SplashScreenViewController: EasyBaseViewController {
    
    lazy var mView: SplashScreenView = {
        let mView = SplashScreenView()
        return mView
    }()
    lazy var updateView: UpdateView = {
        let updateView = UpdateView()
        return updateView
    }()
    var timer:Timer?
    var timeLeft:Float = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        mView.backDropImageView.slideInFromLeft()
       // mView.logoImageView.slideInFromLeft(duration: 0.5, completionDelegate: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //checkUserDevice()
    }
    @objc func onTimerFires()
    {
        timeLeft += 1
        //mView.progressView.setProgress(Float(self.timeLeft)/Float(30), animated: true)
        if timeLeft >= 30 {
            timer?.invalidate()
            timer = nil
            checkUserDevice()
        }
    }
  
    func checkUserDevice() {
        
        WebServiceHandler.shared.townHallVersionCheck(completion: { result in
            switch result{
            case .success(let response):
                if response.code == 200{
                    if let appVersionInt = Int( VersionCheck.getCurrentAppVersion().replacingOccurrences(of: ".", with: "")), let ServerVersionInt = Int(response.data?.appVersion?.replacingOccurrences(of: ".", with: "") ?? ""){
                        if let keyString = response.data?.deviceKey{
                            EasyDataStore.sharedInstance.deviceKey = keyString
                        }
                        if appVersionInt >= ServerVersionInt{
                            if EasyManager.dataStore.apiToken == nil{
                                let vc =  LandingPageViewController()
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                let vc = EasyTabBarViewController()
                                self.navigationController?.viewControllers = [vc]
                            }
                        }else{
                            let vc = ForceUpdateViewController()
                            vc.newVersion = response.data?.appVersion ?? "0.0"
                            vc.oldVersion = VersionCheck.getCurrentAppVersion()
                            self.presentAsPopUp(vc: vc)
                        }
                    }else{
                        let vc = ForceUpdateViewController()
                        vc.newVersion = response.data?.appVersion ?? "0.0"
                        vc.oldVersion = VersionCheck.getCurrentAppVersion()
                        self.presentAsPopUp(vc: vc)
                    }
                    
                }else{
                    self.toastMessage(response.message ?? "Error")
                }
            case .failure(let error):
                self.showToast(message: error.localizedDescription)
            }
        }, shouldShowLoader: false)
    }
    
    func getOperators() {
        EasyManager.sharedInstance.getOprators { (result) in
            switch result {
            case .success(_) :
                /* WebServiceHandler.shared.getRechargePlans(completion: { (result) in
                 switch result{
                 case .success(let response):
                 EasyManager.dataStore.easyRechargePlans = response.data ?? []
                 if EasyManager.dataStore.apiToken == nil{
                 let vc =  LandingPageViewController()
                 self.navigationController?.pushViewController(vc, animated: true)
                 }else{
                 let vc = EasyTabBarViewController()
                 self.navigationController?.viewControllers = [vc]
                 }
                 break
                 case .failure(_):
                 break
                 }
                 }, shouldShowLoader: false)*/
                break
            case .failure(_):
                break
            }
        }
    }
    
}
extension UIView {
    // Name this function in a way that makes sense to you...
    // slideFromLeft, slideRight, slideLeftToRight, etc. are great alternative names
    func slideInFromLeft(duration: TimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromLeftTransition.delegate = delegate as! CAAnimationDelegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = CATransitionType.push
        slideInFromLeftTransition.subtype = CATransitionSubtype.fromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        slideInFromLeftTransition.fillMode = CAMediaTimingFillMode.removed
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
}
