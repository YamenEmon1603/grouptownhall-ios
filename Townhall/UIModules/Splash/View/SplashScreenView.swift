//
//  SplashScreenView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import UIKit

class SplashScreenView: EasyBaseView{
  
    
     init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
         addSubview(backDropImageView)
         backDropImageView.addAnchorToSuperview(leading: 0, trailing: 40, top: 0, bottom: 0)
         
        addSubview(logoImageView)
         
         logoImageView.addAnchorToSuperview( centeredVertically: 0, centeredHorizontally: 0)

        
        addSubview(imageView)
         imageView.addAnchorToSuperview(centeredHorizontally: 0)
         imageView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: .calculatedHeight(-25)).isActive = true
        
         
         
       
     }
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "splashImageNew")
        return imageView
    }()
    let logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "launchLogo")
        return imageView
    }()
    //launchBackDrop
    let backDropImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "launchBackDrop2")
        return imageView
    }()
    let versionLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .medium)
        label.textColor = UIColor.white.withAlphaComponent(0.5)
        label.text = "VERSION \(EasyManager.sharedInstance.appVersion)"
        return label
    }()
//    let progressView: UIProgressView = {
//        let progressView = UIProgressView()
//        progressView.progressTintColor = .white
//        progressView.tintColor = UIColor.white.withAlphaComponent(0.2)
//
//        return progressView
//    }()
   
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    
}
