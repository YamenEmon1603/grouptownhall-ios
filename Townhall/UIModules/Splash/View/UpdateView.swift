//
//  UpdateView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/7/21.
//

import Foundation
import UIKit
class UpdateView: EasyBaseView{
  
    
     init(){
        super.init(frame: .zero)
        self.backgroundColor = .clear
        addSubview(imageView)
        
        addSubview(viewBlurContainer)
        addSubview(viewContainer)
       
        viewBlurContainer.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,  bottom: 0)
        viewContainer.addAnchorToSuperview(leading: 30, trailing: -30,heightMultiplier: 0.7)
        viewContainer.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 1).isActive = true
        
        
     }
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "rocket")
        return imageView
    }()
    lazy var versionLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 20.0, style: .bold)
        label.textColor = .black
        label.textAlignment = .center
        label.text = "new_update_is_available".easyLocalized()
        return label
    }()
    lazy var viewDetails: UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.font = .EasyFont(ofSize: 14.0, style: .regular)
        view.text = "the_version_of_this_application".easyLocalized()
        view.isEditable = false
        return view
    }()
    lazy var btnUpdate:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .blueViolet
       
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "update_now".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.layer.cornerRadius = 10
        //btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    lazy var viewBlurContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white.withAlphaComponent(0.2)
       
        
        return view
    }()
    lazy var viewContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.addSubview(imageView)
        view.addSubview(versionLabel)
        view.addSubview(viewDetails)
        view.addSubview(btnUpdate)
        imageView.addAnchorToSuperview(top: 16, heightMultiplier: 0.4, centeredHorizontally:1,heightWidthRatio: 1.0)
        versionLabel.addAnchorToSuperview(leading: 10, trailing: -10)
        versionLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16).isActive = true
        versionLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        viewDetails.addAnchorToSuperview(leading: 10, trailing: -10,heightMultiplier: 0.2)
        viewDetails.topAnchor.constraint(equalTo: versionLabel.bottomAnchor, constant: 8).isActive = true
        
        btnUpdate.addAnchorToSuperview(leading: 10, trailing: -10,bottom: -30)
        btnUpdate.heightAnchor.constraint(equalToConstant: 40).isActive = true
        btnUpdate.topAnchor.constraint(greaterThanOrEqualTo: viewDetails.bottomAnchor, constant: 20).isActive = true
        
        
        return view
    }()
   
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var textView: EasyTextFieldView = {
        let textView = EasyTextFieldView(title: "User Name")
        textView.keyBoardType = .phonePad
       // textView.accessoryView = ButtonAccessoryView(title: "Proceed", delegate: self)
        return textView
    }()
    
}
