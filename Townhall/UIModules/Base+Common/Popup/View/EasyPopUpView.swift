//
//  EasyPopUpView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit

class EasyPopUpView: EasyBaseView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var buttonCallBack:((_ sender:PopupButton)->Void)?
    convenience init(options: [EasyPopUpOptions]) {
        self.init(frame:.zero)
        backgroundColor =  UIColor.black.withAlphaComponent(0.3)
        
        let contentView = UIView()
        contentView.backgroundColor = .white
        addSubview(contentView)
        contentView.layer.cornerRadius = 10
        contentView.addAnchorToSuperview(leading: 20, trailing: -20,centeredVertically: 0)
        
        let stack = UIStackView()
        stack.axis = .vertical
        contentView.addSubview(stack)
        stack.addAnchorToSuperview(leading: 8, trailing: -8, top: 8, bottom: -8)
        
        
        options.forEach { option in
            switch option{
            case .image(image: let image):
                let imageHolder = UIView()
                imageHolder.backgroundColor = .clear
                imageHolder.addSubview(statusIV)
                statusIV.image = image.image
                statusIV.addAnchorToSuperview( top:0,bottom: 0, centeredHorizontally:0)
                statusIV.heightAnchor.constraint(equalToConstant: .calculatedHeight(120)).isActive = true
                statusIV.widthAnchor.constraint(equalTo: statusIV.heightAnchor).isActive = true
                stack.addArrangedSubview(imageHolder)
            case .title(str: let str,color: let color):
                titleLabel.text = str
                titleLabel.textColor = color
                stack.addArrangedSubview(titleLabel)
            case .subtitle(str: let str,color: let color):
                let subtitleHolder = UIView()
                subtitleHolder.backgroundColor = .clear
                subtitleHolder.addSubview(subTitleLabel)
                subTitleLabel.text = str
                subTitleLabel.textColor = color
                subTitleLabel.addAnchorToSuperview(leading: 10, trailing: -10, top: 5, bottom: -5)
                stack.addArrangedSubview(subtitleHolder)
            case .numbers(numbers: let numbers):
                let numbersHolder = UIView()
                numbersHolder.backgroundColor = .clear
                numbersHolder.addSubview(numbersStack)
                numbersStack.addAnchorToSuperview(leading: 20, trailing: -20, top: 15, bottom: -5)
                stack.addArrangedSubview(numbersHolder)
                numbers.forEach({ item in
                    let view = NumberItemView(number: "\(item.number ?? "")", amount: "\(item.amount ?? 0)")
                    view.heightAnchor.constraint(equalToConstant: .calculatedHeight(20)).isActive = true
                    numbersStack.addArrangedSubview(view)
                })
            case .consent(str: let str,color: let color):
                let consentHolder = UIView()
                consentHolder.addSubview(consentButton)
                consentHolder.addSubview(consentLabel)
                consentLabel.text = str
                consentLabel.textColor = color
                consentLabel.addAnchorToSuperview(trailing: -20, top: 15, bottom: -5)
                consentButton.addAnchorToSuperview(leading:20 , top: 13,heightMultiplier: 0.5,heightWidthRatio: 1)
                consentLabel.leadingAnchor.constraint(equalTo:consentButton.trailingAnchor , constant: 1).isActive = true
                stack.addArrangedSubview(consentHolder)
            case  .starRatting(str:let str,maxRating:let maxRating,currentRating:let currentRating):
                let ratingHolder = UIView()
                starRatinView = StarRatingView(text: "", subtext: str, maxrating: maxRating, currentRating: currentRating)
                ratingHolder.addSubview(starRatinView ?? UIView())
                starRatinView?.addAnchorToSuperview(leading: 30, trailing:-30, top: 15, bottom: -5)
                starRatinView?.heightAnchor.constraint(equalToConstant: .calculatedHeight(60)).isActive = true
                stack.addArrangedSubview(ratingHolder)
            case .buttons(buttons:let buttons):
                let buttonsHolder = UIView()
                buttonsHolder.addSubview(buttonsStack)
                buttonsStack.addAnchorToSuperview(leading: 30, trailing:-30, top: 10, bottom: -20)
                stack.addArrangedSubview(buttonsHolder)
                buttons.forEach { btn in
                    btn.translatesAutoresizingMaskIntoConstraints = false
                    btn.heightAnchor.constraint(equalToConstant: .calculatedHeight(46)).isActive = true
                    btn.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
                    buttonsStack.addArrangedSubview(btn)
                }

            }
        }
        
        
        
    }
    @objc func buttonAction(_ sender: PopupButton){
        buttonCallBack?(sender)
    }
    var starRatinView: StarRatingView?
    let statusIV: UIImageView = {
        let statusIV = UIImageView()
        statusIV.contentMode = .scaleAspectFit
        
        statusIV.image = UIImage(named: "billpayment")
        statusIV.translatesAutoresizingMaskIntoConstraints = false
        return statusIV
    }()
    let titleLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 20, style: .medium)
        label.textColor = .green
       
        label.textAlignment = .center
        return label
    }()
    let subTitleLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.textColor = .blueViolet
       
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    let numbersStack: UIStackView = {
        let numbersStack = UIStackView()
        numbersStack.axis = .vertical
        
        numbersStack.backgroundColor = .Easywhite
        numbersStack.layer.borderWidth = 1
        numbersStack.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        numbersStack.layer.cornerRadius = 10
        numbersStack.clipsToBounds = true
        
        return numbersStack
    }()
    lazy var consentButton : UIButton = {
        let consentButton = UIButton()
        consentButton.setImage(UIImage(named: "conditionUnchecked"), for: .normal)
        consented = false
        consentButton.addTarget(self, action: #selector(consentBtnAction(_:)), for: .touchUpInside)
        return consentButton
    }()
    var consented:Bool?
    @objc func consentBtnAction(_ sender:UIButton){
        consented?.toggle()
        if consented == true{
            consentButton.setImage(UIImage(named: "conditionChecked"), for: .normal)
        }else{
            consentButton.setImage(UIImage(named: "conditionUnchecked"), for: .normal)
        }
    }
    
    let consentLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .medium)
        label.textColor = .slateGrey
      
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
   
    let buttonsStack: UIStackView = {
        let numbersStack = UIStackView()
        numbersStack.axis = .vertical
        numbersStack.spacing = 10
        
        return numbersStack
    }()
}
class NumberItemView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(number:String,amount:String) {
        self.init(frame: .zero)
        backgroundColor = .Easywhite
        
        addSubview(logoView)
        logoView.addAnchorToSuperview(leading: 20, heightMultiplier: 0.4, centeredVertically: 0,heightWidthRatio: 1)
        addSubview(numberLabel)
        numberLabel.text = number
        numberLabel.addAnchorToSuperview(centeredVertically: 0)
        numberLabel.leadingAnchor.constraint(equalTo: logoView.trailingAnchor,constant: 10).isActive = true
        
        let amountLabel = ViewWithTakaSign(amount: amount)
        amountLabel.amountLabel.font = .EasyFont(ofSize: 10, style: .medium)
        amountLabel.takasignLabel.font = .EasyFont(ofSize: 10, style: .medium)
       
        addSubview(amountLabel)
        amountLabel.addAnchorToSuperview( trailing: -20,widthMutiplier: 0.2,centeredVertically: 0)
        numberLabel.trailingAnchor.constraint(equalTo: amountLabel.leadingAnchor, constant: -10).isActive = true
        translatesAutoresizingMaskIntoConstraints = false
        let _ = addBorder(side: .bottom, color: .lightPeriwinkle, width: 0.5)
    }
    
   
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
        logoView.image = UIImage(named: "tick")
        return logoView
    }()
    
    let numberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.textColor = .black
        
        return label
    }()
    
    
    
    
}
enum  PopupButtonType{
    case fill,bordered
}
class PopupButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private init(){
        super.init(frame: .zero)
    }
    
    convenience init(type:PopupButtonType,title:String,tag:Int) {
        self.init()
        setTitle(title, for: .normal)
        titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        layer.cornerRadius = 10
        clipsToBounds = true
        self.tag = tag
        switch  type{
        case .fill:
           setTitleColor(.white, for: .normal)
           backgroundColor = .blueViolet
        case.bordered:
            setTitleColor(.blueViolet, for: .normal)
            backgroundColor = .white
            layer.cornerRadius = 10
            layer.borderWidth  = 1
            layer.borderColor = UIColor.blueViolet.cgColor
        }
    }
    
}
