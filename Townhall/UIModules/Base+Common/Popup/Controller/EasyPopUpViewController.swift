//
//  EsayPopUpViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit
enum EasyPopUpOptions {
    case image(image:EasyPopUpImage)
    case title(str:String,color:UIColor)
    case subtitle(str:String,color:UIColor)
    case numbers(numbers :[SelectNumber])
    case consent(str:String,color:UIColor)
    case starRatting(str:String,maxRating:Int,currentRating:Int)
    case buttons(buttons:[PopupButton])
}
protocol EasyPopUpDelegate {
    func popupDismissedWith(button:PopupButton,consent:Bool?,rating:Int?)
}
class EasyPopUpViewController: EasyBaseViewController {
    lazy var mView: EasyPopUpView = {
        let mView = EasyPopUpView(options: options)
        mView.buttonCallBack = buttonCallback(_:)
        return mView
    }()
    var delegate:EasyPopUpDelegate?
    var options: [EasyPopUpOptions] = []
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        
       
    }
    func buttonCallback(_ sender: PopupButton){
        self.dismiss(animated: true) {
            self.delegate?.popupDismissedWith(button: sender,consent: self.mView.consented, rating: self.mView.starRatinView == nil ? nil : self.mView.starRatinView?.currentRating)
        }
    }

    

}

/*

 let advTimeGif = UIImage.gifImageWithData(imageData!)
 */
enum EasyPopUpImage{
    case success,failed,other(image: UIImage)

    var image : UIImage?{
        switch self {
        case .success:
            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "success", withExtension: "gif")!)
            return UIImage.gifImageWithData(imageData!)
        case .failed:
            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "failed", withExtension: "gif")!)
            return UIImage.gifImageWithData(imageData!)
        case .other(let image):
            return image
        }
    }
}
