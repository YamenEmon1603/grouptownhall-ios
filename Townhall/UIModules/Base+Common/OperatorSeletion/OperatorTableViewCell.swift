//
//  OperatorTableViewCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit

class OperatorTableViewCell: UITableViewCell {

    var _operator:MBOperator?{
        didSet{
            nameLabel.text = _operator?.localisedName
            logoView.image = _operator?.logoImage
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
    
        addSubview(container)
        container.addAnchorToSuperview(leading: 15, trailing: -15, top: 5, bottom: -5)
        container.layer.borderWidth = 1
        container.layer.borderColor = UIColor.paleLilac.cgColor
        container.layer.cornerRadius = 10
        container.addSubview(logoView)
        logoView.addAnchorToSuperview(leading: 20, heightMultiplier: 0.5, centeredVertically: 0,heightWidthRatio: 1)
        container.addSubview(nameLabel)
        nameLabel.addAnchorToSuperview( top: 5, bottom: -5)
        nameLabel.leadingAnchor.constraint(equalTo: logoView.trailingAnchor,constant: 10).isActive = true
        container.addSubview(selectionImage)
        selectionImage.addAnchorToSuperview( trailing: -20, heightMultiplier: 0.5,  centeredVertically: 0,  heightWidthRatio: 1)
        
    }
    let container = UIView()
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
       // logoView.image = UIImage(named: "gp")
        return logoView
    }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        //label.text = "Grameen Phone"
        return label
    }()
    let selectionImage : UIImageView = {
        let view = UIImageView()
         view.contentMode = .scaleAspectFit
         view.image = UIImage(named: "radiodeselect")
         return view
    }()
}
