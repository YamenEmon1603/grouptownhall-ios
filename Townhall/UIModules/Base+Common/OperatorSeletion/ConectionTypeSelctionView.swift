//
//  ConectionTypeSelctionView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit
enum ConectionType {
    
    
    case Prepaid,PostPaid,Skitto
    var title:String{
        switch self {
        case .Prepaid:
            return "prepaid".easyLocalized()
        case .PostPaid:
            return "postpaid".easyLocalized()
        case .Skitto:
            return "skitto".easyLocalized()
        }
    }
    var value:String{
        switch self {
        case .Prepaid:
            return "prepaid"
        case .PostPaid:
            return "postPaid"
        case .Skitto:
            return "skitto"
        }
    }
    init(_ string:String) {
        switch string {
        case "prepaid":
            self =  .Prepaid
        case "postpaid":
            self =  .PostPaid
        case "skitto":
            self =  .Skitto
        default:
            self =  .Prepaid
        }
    }
}
class ConectionTypeSelctionView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    var selectionCallBack:((_ selected: ConectionType?)->Void)?
    var billTypeSelectionCallback:((_ selected: Int) ->Void)?
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var types: [ConectionType] = []{
        didSet{
            textStack.arrangedSubviews.forEach { v in
                v.removeFromSuperview()
            }
            for (_,str) in types.enumerated(){
                let v = ConnectionTypeItemView(name: str.title)
                v.heightAnchor.constraint(equalToConstant: .calculatedHeight(52.0)).isActive = true
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectAction(_:)))
                v.addGestureRecognizer(tapGesture)
                v.connectionType = str
                
                textStack.addArrangedSubview(v)
            }
        }
    }
    var billTypes : [String] = []{
        didSet{
            textStack.arrangedSubviews.forEach { v in
                v.removeFromSuperview()
            }
            for (index,str) in billTypes.enumerated(){
                let v = ConnectionTypeItemView(name: str)
                v.heightAnchor.constraint(equalToConstant: .calculatedHeight(52.0)).isActive = true
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectBillTypeAction(_:)))
                v.addGestureRecognizer(tapGesture)
                v.tag = index
                
                textStack.addArrangedSubview(v)
            }
        }
    }
    let textStack = UIStackView()
    let infoLabel = EasyPaddingLabel()
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .black.withAlphaComponent(0.5)
        
        let contentView = UIView()
        contentView.backgroundColor = .white
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
      
        infoLabel.text = "select_connection_type".easyLocalized()
        contentView.addSubview(infoLabel)
        infoLabel.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        infoLabel.heightAnchor.constraint(equalToConstant: .calculatedHeight(50)).isActive = true
        
        
       
        textStack.axis = .vertical
        textStack.spacing = 5
        textStack.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(textStack)
        textStack.addAnchorToSuperview(leading: 0, trailing: 0)
        textStack.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 0).isActive = true
        textStack.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        addSubview(closeButton)
        closeButton.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        closeButton.bottomAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
    }
    let closeButton : UIButton = {
        let closeButton = UIButton()
        closeButton.backgroundColor = .clear
        closeButton.addTarget(self, action: #selector(closebuttonAction), for: .touchUpInside)
        return closeButton
    }()
    @objc func closebuttonAction(){
        self.removeFromSuperview()
    }
    @objc func selectAction(_ sender: UITapGestureRecognizer){
        selectionCallBack?((sender.view as? ConnectionTypeItemView)?.connectionType)
        self.removeFromSuperview()
    }
    @objc func selectBillTypeAction(_ sender: UITapGestureRecognizer){
        if let view = sender.view as? ConnectionTypeItemView{
            billTypeSelectionCallback?(view.tag)
        }
        self.removeFromSuperview()
    }

}

class ConnectionTypeItemView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    var connectionType: ConectionType?
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(name: String) {
        self.init(frame:.zero)
        backgroundColor = .white
        addSubview(nameLabel)
        nameLabel.text = name
        nameLabel.addAnchorToSuperview(leading: 20,  top: 5, bottom: -5)
     
        addSubview(selectOperatorButton)
        selectOperatorButton.addAnchorToSuperview(trailing: -20,heightMultiplier: 0.2, centeredVertically: 0,heightWidthRatio: 1)
        nameLabel.trailingAnchor.constraint(equalTo: selectOperatorButton.leadingAnchor, constant: -10).isActive = true
        translatesAutoresizingMaskIntoConstraints = false
        let _ = addBorder(side: .bottom, color: .paleLilac, width: 1)
        
    }
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.text = "Fasi Uddin Raihan"
        return label
    }()
    lazy var selectOperatorButton : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setImage(UIImage(named: "arrow-point-to-right"), for: .normal)
        return view
    }()
}
