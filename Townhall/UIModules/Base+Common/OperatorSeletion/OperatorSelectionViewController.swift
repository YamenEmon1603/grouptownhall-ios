//
//  OperatorSelectionViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit
protocol OperatorSelectionDelegate {
    func didSelect(operatorObject:MBOperator,connectionType:ConectionType)
}
class OperatorSelectionViewController: EasyBaseViewController {
    class func show(on vc:EasyBaseViewController,delegate:OperatorSelectionDelegate){
        let popUp = OperatorSelectionViewController()
        popUp.delegate = delegate
        popUp.modalPresentationStyle = .fullScreen
        vc.present(popUp, animated: true, completion: nil)
    }
    var delegate:OperatorSelectionDelegate?
    private var selectedOperator:MBOperator?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .iceBlue
        
        self.view.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        self.view.addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        tableView.topAnchor.constraint(equalTo: navView.bottomAnchor, constant: 0).isActive = true
        
        typeSelectionView.selectionCallBack = { selected in
            if let connectionType = selected, let op = self.selectedOperator{
                self.dismiss(animated: true) {
                    self.delegate?.didSelect(operatorObject: op, connectionType: connectionType)
                }
            }
            
        }
    }
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(OperatorTableViewCell.self, forCellReuseIdentifier: "operatorCell")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        return tableView
    }()
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"")
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleGrey, width: 0.5)
        navView.backButtonCallBack = {
            self.dismiss(animated: true, completion: nil)
        }
        return navView
    }()
    
    let typeSelectionView = ConectionTypeSelctionView()
    func addTypeSelction() {
        self.view.addSubview(typeSelectionView)
        typeSelectionView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
    }
    
    
}
extension OperatorSelectionViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EasyManager.dataStore.easyMobileOperators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "operatorCell", for: indexPath) as! OperatorTableViewCell
        
        cell._operator = EasyManager.dataStore.easyMobileOperators[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = EasyPaddingLabel()
        label.text = "selecct_an_operator".easyLocalized()
        return label
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return .calculatedHeight(52.0)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? OperatorTableViewCell{
            cell.selectionImage.image = UIImage(named: "selectedSort")
            cell.container.layer.borderColor = UIColor.blueViolet.cgColor
            self.selectedOperator = cell._operator
            if cell._operator?.operatorShortName?.lowercased() == "gp"{
                typeSelectionView.types = [.Prepaid,.PostPaid,.Skitto]
            }else{
                typeSelectionView.types = [.Prepaid,.PostPaid]
            }
            
            addTypeSelction()
        }
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? OperatorTableViewCell{
            cell.selectionImage.image = UIImage(named: "radiodeselect")
            cell.container.layer.borderColor = UIColor.paleLilac.cgColor
            typeSelectionView.removeFromSuperview()
            self.selectedOperator = nil
        }
    }
    
    
}
