//
//  HorizontalButtonAlertView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/8/21.
//

import UIKit

class HorizontalButtonAlertView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var btnSecondCallBack : (()->Void)?
    var btnFirstCallBack : (()->Void)?
    let textStack = UIStackView()
    let buttonsStack = UIStackView()
    
    convenience init(header:String,subHeader:String,btn1Name:String,btn2Name:String) {
        self.init(frame:.zero)
        backgroundColor = .black.withAlphaComponent(0.5)
        
        let contentView = UIView()
        contentView.backgroundColor = .white
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        
        
        addSubview(closeButton)
        
        closeButton.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        closeButton.bottomAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        
        buttonsStack.axis = .horizontal
        buttonsStack.spacing = 20
        buttonsStack.distribution = .fillEqually
        buttonsStack.translatesAutoresizingMaskIntoConstraints = false
        
        textStack.axis = .vertical
        textStack.spacing = 5
        textStack.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(textStack)
        contentView.addSubview(buttonsStack)
        
        lblTitleHeader.text = header
        lblTitleSub.text = subHeader
        
        textStack.addAnchorToSuperview(leading: 20, trailing: -20,top: 14)
        textStack.heightAnchor.constraint(equalToConstant: 70).isActive = true
        textStack.bottomAnchor.constraint(equalTo: buttonsStack.topAnchor,constant: -14).isActive = true
        
        textStack.addArrangedSubview(lblTitleHeader)
        textStack.addArrangedSubview(lblTitleSub)
        
        
        buttonsStack.addAnchorToSuperview(leading: 20, trailing: -20)
        
        
        buttonsStack.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor,constant: -17).isActive = true
        buttonsStack.addArrangedSubview(btnFirst)
        buttonsStack.addArrangedSubview(btnSecond)
        btnFirst.setTitle(btn1Name, for: .normal)
        btnSecond.setTitle(btn2Name, for: .normal)
        
    }
    let closeButton : UIButton = {
        let closeButton = UIButton()
        closeButton.backgroundColor = .clear
        closeButton.addTarget(self, action: #selector(closebuttonAction), for: .touchUpInside)
        return closeButton
    }()
    @objc func closebuttonAction(){
        btnFirstCallBack?()
    }
 
    lazy var lblTitleHeader : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = ""
        view.textColor = .black
        view.textAlignment = .left
        view.font = .EasyFont(ofSize: 16, style: .medium)
        return view
    }()
    lazy var lblTitleSub : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = ""
        view.textColor = .slateGrey
        view.textAlignment = .left
        view.numberOfLines = 2
        view.font = .EasyFont(ofSize: 12, style: .regular)
        return view
    }()
    lazy var btnSecond:UIButton = {
        let btn = UIButton()
        btn.setTitle("button2", for: .normal)
        btn.backgroundColor = .blueViolet
        btn.layer.cornerRadius = 6
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(btnSecondAction(_:)), for: .touchUpInside)
        btn.titleLabel?.font = .EasyFont(ofSize: 14, style: .bold)
        return btn
    }()
    @objc func btnSecondAction(_ sender:UIButton){
        btnSecondCallBack?()
    }
    
    lazy var btnFirst:UIButton = {
        let btn = UIButton()
        btn.setTitle("button1", for: .normal)
        btn.backgroundColor = .white
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        btn.layer.cornerRadius = 6
        btn.titleLabel?.font = .EasyFont(ofSize: 14, style: .bold)
        btn.setTitleColor(.slateGrey, for: .normal)
        btn.addTarget(self, action: #selector(buttonFirstAction), for: .touchUpInside)
        return btn
    }()
    @objc func buttonFirstAction(_ sender:UIButton){
        btnFirstCallBack?()
    }
    
}


