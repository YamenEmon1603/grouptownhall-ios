//
//  EasyNavigationBarView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/1/21.
//

import UIKit

class EasyNavigationBarView: UIView {
    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    var backButtonCallBack:(()->Void)?
    var subTitle : String?{
        didSet{
            subTitleLabel.text = subTitle
            subTitleLabel.isHidden = false
        }
    }
    var rightItem : UIView?{
        didSet{
            if let item = rightItem{
                addSubview(item)
                item.addAnchorToSuperview( trailing: -15, heightMultiplier: 0.6, widthMutiplier: 0.3, centeredVertically: 0)
                stack?.trailingAnchor.constraint(equalTo: item.leadingAnchor, constant: -10).isActive = true
            }else{
                stack?.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
            }
        }
    }
    var navTintColor : UIColor = .black{
        didSet{
            titleLabel.textColor = navTintColor
            backButton.tintColor = navTintColor
        }
    }
    var stack : UIStackView?
    var title:String?{
        didSet{
            titleLabel.text = title
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(title :String ,withBackButton:Bool? = true , rightItem:UIView? = nil) {
        self.init(frame:.zero)
        backgroundColor = .clear
        self.rightItem = rightItem
         stack = UIStackView()
        stack?.distribution = .fillEqually
        stack?.axis = .vertical
        stack?.addArrangedSubview(titleLabel)
        stack?.addArrangedSubview(subTitleLabel)
        addSubview(stack ?? UIStackView())
        stack?.addAnchorToSuperview( centeredVertically: 0)
   
        titleLabel.text = title
       
        if withBackButton == true{
            addSubview(backButton)
            backButton.addAnchorToSuperview(leading: 10 ,heightMultiplier: 0.8, centeredVertically: 0,  heightWidthRatio: 1)
            stack?.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 10).isActive = true
        }else{
            stack?.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        }
//        if let item = rightItem{
//            addSubview(item)
//            item.addAnchorToSuperview( trailing: -15, heightMultiplier: 0.6, widthMutiplier: 0.3, centeredVertically: 0)
//            stack?.trailingAnchor.constraint(equalTo: item.leadingAnchor, constant: -10).isActive = true
//        }else{
//            stack?.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
//        }
        
    }
    private let backButton:UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "backImage"), for: .normal)
        backButton.tintColor = .black
        backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        return backButton
    }()
    private let titleLabel : UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 13.0, style: .bold)
        label.textColor = .black
        label.text = "EASY OFFERS"
        return label
    }()
    private let subTitleLabel : UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.textColor = .battleshipGrey
        label.isHidden = true
        label.text = ""
        return label
    }()
    @objc func btnBackAction(_ sender:UIButton){
        backButtonCallBack?()
    }
}
