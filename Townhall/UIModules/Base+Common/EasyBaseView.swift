//
//  EasyBaseView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import UIKit

class EasyBaseView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
