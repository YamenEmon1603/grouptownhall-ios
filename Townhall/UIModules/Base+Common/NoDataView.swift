//
//  NoDataView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit

class NoDataView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(image:UIImage,text:String,subtext:String) {
        self.init(frame:.zero)
        let textStack = UIStackView()
        textStack.axis = .vertical
        textStack.spacing = 5
        textStack.translatesAutoresizingMaskIntoConstraints = false
        
        let textLabel = UILabel()
        textLabel.font = .EasyFont(ofSize: 20.0, style: .bold)
        textLabel.textColor = .black
        textLabel.textAlignment = .center
        textLabel.text = text
        
        let subtextLabel = UILabel()
        subtextLabel.font = .EasyFont(ofSize: 14.0, style: .regular)
        subtextLabel.textColor = .battleshipGrey
        subtextLabel.textAlignment = .center
        subtextLabel.numberOfLines = 0
        subtextLabel.text = subtext
        
        textStack.addArrangedSubview(textLabel)
        textStack.addArrangedSubview(subtextLabel)
        
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        
        addSubview(imageView)
        imageView.addAnchorToSuperview(widthMutiplier: 0.3, centeredVertically: -30, centeredHorizontally: 0, heightWidthRatio: 1)
        addSubview(textStack)
        textStack.addAnchorToSuperview(leading: 40, trailing: -40)
        textStack.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20).isActive = true
       
    }

}
