//
//  EasyBaseViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import UIKit

class EasyBaseViewController: UIViewController {

    var statusBarHeight : CGFloat = 20
    let bottomBarHeight = ( Constant.IS_IPHONE_X ? 73*Constant.factX : 52*Constant.factX )


    override func viewDidLoad() {
        super.viewDidLoad()
        print(UIApplication.shared.statusBarFrame.height)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.view.backgroundColor = .white
        if #available(iOS 13.0, *) {
            statusBarHeight = 20;
            if (Constant.IS_IPHONE_X) {
                statusBarHeight=40;
            }
        } else {
                // Fallback on earlier versions
            statusBarHeight=20;
            if (Constant.IS_IPHONE_X) {
                statusBarHeight=40;
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
                // Fallback on earlier versions
            return .default
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EasyDataStore.sharedInstance.mainNav = self.navigationController
        let hideKeybaordGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard(_ :)))
        hideKeybaordGesture.cancelsTouchesInView = false
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .portrait
        self.view.addGestureRecognizer(hideKeybaordGesture)
    }
    @objc func appMovedToForeground(){

    }
    @objc func hideKeyboard(_ sender:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    func toastMessage(_ message: String, height: CGFloat = 60){
        guard let window = UIApplication.shared.keyWindow else {return}
        let messageLbl = SSLCOMToastLabel(withInsets: 8, 8, 8, 8)
        messageLbl.text = message
        messageLbl.textAlignment = .center
        messageLbl.font = .EasyFont(ofSize: 14.0, style: .medium)
        messageLbl.textColor = .white
        messageLbl.backgroundColor = .black.withAlphaComponent(0.5)
        messageLbl.translatesAutoresizingMaskIntoConstraints = false
      

        messageLbl.layer.cornerRadius = 15
        messageLbl.layer.masksToBounds = true
        window.addSubview(messageLbl)
        messageLbl.addAnchorToSuperview(leading: 20, trailing: -20, centeredVertically: 0)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            UIView.animate(withDuration: 1, animations: {
                messageLbl.alpha = 0
            }) { (_) in
                messageLbl.removeFromSuperview()
            }
        }
    }

    
}
