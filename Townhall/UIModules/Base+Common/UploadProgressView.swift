//
//  UploadProgressView.swift
//  Townhall
//
//  Created by Mausum Nandy on 3/1/22.
//



import UIKit

class ProgressHUD: UIView {
    
    var text: String? {
        didSet {
            label.text = text
        }
    }
    
    let activityIndictor: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
      //  activity.style = UIActivityIndicatorView.Style.whiteLarge
        activity.color = .blueViolet
        return activity
    }()
    let label: UILabel = UILabel()
  
    
    init(text: String) {
        self.text = text
        
        super.init(frame: .zero)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.text = ""
        
        super.init(coder: aDecoder)
        self.setup()
    }
    let contentView = UIView()
    var  bottomConstarint, topConstraint: NSLayoutConstraint?
    func setup() {
        label.numberOfLines = 0
        label.textColor = .black
        label.font = .EasyFont(ofSize: 16.0, style: .bold)
        
       
        addSubview(contentView)
        contentView.addAnchorToSuperview(centeredVertically:0, centeredHorizontally:0)
        contentView.addSubview(activityIndictor)
        contentView.addSubview(label)
        activityIndictor.startAnimating()
        
        activityIndictor.addAnchorToSuperview( widthMutiplier: 0.20,  centeredHorizontally: 0, heightWidthRatio: 1)
        label.addAnchorToSuperview(leading: 40, trailing: -40)
        label.topAnchor.constraint(equalTo: activityIndictor.bottomAnchor,constant: 10).isActive = true
        topConstraint = activityIndictor.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10)
        topConstraint?.isActive = true
        bottomConstarint = label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        bottomConstarint?.isActive = true
        contentView.backgroundColor = .blueyGrey
        contentView.layer.cornerRadius = 10
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        let constent = (contentView.frame.height - (activityIndictor.frame.height+label.frame.height + 10))/2
        bottomConstarint?.constant = -constent
        topConstraint?.constant = constent
    }
    
    
    
    func show() {
        self.isHidden = false
    }
    
    func hide() {
        self.isHidden = true
    }
}

