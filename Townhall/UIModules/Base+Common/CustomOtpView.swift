//
//  CustomOtpView.swift
//  ssl_commerz_revamp
//
//  Created by Raihan on 23/6/21.
//

import Foundation
import UIKit
class CustomOtpView: UIView {
    var numberOfField = 0
    var otpFieldsArr : [UITextField] = []
    var initialTag = 911
    var borderColor:CGColor = UIColor.lightGray.cgColor
    var textColor:UIColor = UIColor.black
    var font : UIFont = .systemFont(ofSize: 18, weight: .bold)
    
    
    var complition: ((_ otp:String)->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(numberOfField : Int,borderColor:UIColor = UIColor.lightGray,textColor:UIColor = UIColor.black, complition:@escaping (_ otp:String)->Void) {
        self.init(frame:.zero)
        self.numberOfField = numberOfField
        self.borderColor = borderColor.cgColor
        self.textColor = textColor
        self.complition = complition
        self.addShadow(location: .bottom, color: UIColor.black, opacity: 0.05, radius: 5)
        backgroundColor = .clear
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        
        containerView.addSubview(formView)
        formView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,bottom: 0)
        
    }
    
    //MARK: COMPONENTS
    
    
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(otpStack)
        otpStack.addAnchorToSuperview(leading: 10, trailing: -10, top: 10, bottom: -10)
        for i in 1...numberOfField{
            let view = UIView()
            view.backgroundColor = .white
            view.addShadow(location: .bottom, color: UIColor.black, opacity: 0.2, radius: 5)
            view.layer.cornerRadius = 6
            view.clipsToBounds = true
            
           let field  = UITextField()
            initialTag += 1
            field.tag = initialTag
            field.tintColor = .blueViolet
            otpFieldsArr.append(field)
           
            field.textColor = textColor
            field.textAlignment = .center
            field.font = font
            field.delegate = self
            field.keyboardType = .numberPad
            if #available(iOS 12.0, *) {
                field.textContentType = .oneTimeCode
            } else {
                // Fallback on earlier versions
            }
            view.addSubview(field)
            field.addAnchorToSuperview(leading: 2, trailing: -2, top: 1)
           
            
            let underLineView = UIView()
            underLineView.backgroundColor = .clear
            underLineView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(underLineView)
            underLineView.addAnchorToSuperview(bottom:0)
            underLineView.leadingAnchor.constraint(equalTo: field.leadingAnchor).isActive = true
            underLineView.trailingAnchor.constraint(equalTo: field.trailingAnchor).isActive = true
            underLineView.topAnchor.constraint(equalTo: field.bottomAnchor,constant: 2).isActive = true
            underLineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
            underLineView.tag = initialTag+100
            otpStack.addArrangedSubview(view)
        }
        return formView
    }()
    
    lazy var otpStack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        stack.spacing = 12
        stack.backgroundColor = .clear
        
        return stack
    }()
    var otp = ""
    func checkOtp(){
        var otpStr = ""
        otpFieldsArr.forEach { tf in
            if tf.text?.isEmpty == false{
                otpStr.append(tf.text ?? "")
            }
        }
        if otpStr.count == otpFieldsArr.count{
            complition?(otpStr)
            otp = otpStr
            self.endEditing(true)
        }
    }
    
    
}
extension CustomOtpView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var shouldProcess = false //default to reject
        var shouldMoveToNextField = false //default to remaining on the current field
        
        let insertStringLength = string.count
        
        if insertStringLength == 0 {
            //backspace
            textField.text = ""
            shouldProcess = false //Process if the backspace character was pressed
            let prevTag = textField.tag - 1
            print(prevTag)
            if let prevResponder =  self.viewWithTag(prevTag){
                prevResponder.becomeFirstResponder()
            }
        } else {
            if textField.text?.count == 0 {
                shouldProcess = true //Process if there is only 1 character right now
            }else if textField.text?.count == 1 {
                shouldMoveToNextField = true //Process if there is only 1 character right now
            }
        }
        
        
        //here we deal with the UITextField on our own
        if var mstring = textField.text, shouldProcess {
            //grab a mutable copy of what's currently in the UITextField
            
            if mstring.count == 0 {
                //nothing in the field yet so append the replacement string
                mstring += string
                
                shouldMoveToNextField = true
            } else {
                //adding a char or deleting?
                if insertStringLength > 0 {
                    mstring.insert(contentsOf: string, at: string.index(string.startIndex, offsetBy: range.location))
                } else {
                    //delete case - the length of replacement string is zero for a delete
                    if let subRange = Range<String.Index>(range, in: mstring) { mstring.removeSubrange(subRange) }
                }
                
            }
            
            //set the text now
            textField.text = mstring
            
            
        }
        if shouldMoveToNextField {
            let nextTag = textField.tag + 1
            if let nextResponder =  self.viewWithTag(nextTag) as? UITextField{
                nextResponder.text = shouldProcess ? "" : string
                nextResponder.becomeFirstResponder()
            }else{
                
            }
            
        }
        checkOtp()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.viewWithTag(textField.tag+100)?.backgroundColor = .blueViolet
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.isEmpty == true{
            self.viewWithTag(textField.tag+100)?.backgroundColor = .clear
        }else{
            self.viewWithTag(textField.tag+100)?.backgroundColor = .blueViolet
        }
    }
}
