//
//  LoginNavView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 16/11/21.
//

import Foundation
import UIKit
class LoginNavBar:UIView{
    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    var backButtonCallBack:(()->Void)?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(title : String = "", leadingBtn:CGFloat = 10) {
        self.init(frame:.zero)
        backgroundColor = .clear
        lblTitle.text = title
        addSubview(backButton)
        addSubview(lblTitle)
        backButton.addAnchorToSuperview(leading: leadingBtn ,heightMultiplier: 0.8, centeredVertically: 0,  heightWidthRatio: 1.2)
        lblTitle.addAnchorToSuperview(trailing: -15, top: 0, bottom: 0)
        lblTitle.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 0).isActive = true
        
    }
    private let backButton:UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "backImage"), for: .normal)
        backButton.tintColor = .black
        backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        return backButton
    }()
     let lblTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textAlignment = .left
        lbl.font = .EasyFont(ofSize: 11, style: .bold)
        lbl.textColor = .black
        lbl.numberOfLines = 1
        return lbl
    }()
    @objc func btnBackAction(_ sender:UIButton){
        backButtonCallBack?()
    }
}
