//
//  ButtonAccessoryView.swift
//  Easy-Merchant
//
//  Created by Mausum Nandy on 5/11/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit

class ButtonAccessoryView:  UIView{
    private var lblProcced:UILabel!
    var delegate: ButtonAccessoryViewDelegate?
    
    
    init(title: String, delegate: ButtonAccessoryViewDelegate ) {
        
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)
        super.init(frame: frame)
        self.frame = frame
        self.backgroundColor = .blueViolet
        self.delegate = delegate
        lblProcced = UILabel(frame: self.frame)
        
        lblProcced.attributedText = EasyUtils.shared.spaced(text: "proceed".easyLocalized())
        lblProcced.textAlignment = .center
        lblProcced.font = .EasyFont(ofSize: 13, style: .bold)
        lblProcced.textColor = .white
        self.addSubview(lblProcced)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(proceedTapAction(_:)))
        gesture.cancelsTouchesInView = false
        self.addGestureRecognizer(gesture)

       
    }
    @objc func proceedTapAction(_ sender: UITapGestureRecognizer){
        
        delegate?.tapAction(self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
protocol ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView)
}


