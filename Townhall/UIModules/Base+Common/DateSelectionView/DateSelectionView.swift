//
//  DateSelectionView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/8/21.
//

import Foundation
import UIKit

class DateSelectionView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
  
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var btnDoneCallBack : (()->Void)?
    let datePicker: UIDatePicker = UIDatePicker()
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .black.withAlphaComponent(0.5)
        
        let contentView = UIView()
        contentView.backgroundColor = .white
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        
        
        addSubview(closeButton)
        
        closeButton.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        closeButton.bottomAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        
        
        contentView.addSubview(btnDone)
        contentView.addSubview(datePicker)
        btnDone.addAnchorToSuperview(trailing: -15, top: 8, widthMutiplier: 0.3)
        btnDone.bottomAnchor.constraint(equalTo: datePicker.topAnchor, constant: 0).isActive = true
        btnDone.heightAnchor.constraint(equalToConstant: 30).isActive = true
        datePicker.addAnchorToSuperview(leading: 15, trailing: -15, bottom: 0)
        datePicker.heightAnchor.constraint(equalToConstant: 200).isActive = true
        datePicker.backgroundColor = .white
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
    }
    let closeButton : UIButton = {
        let closeButton = UIButton()
        closeButton.backgroundColor = .clear
        closeButton.addTarget(self, action: #selector(closebuttonAction), for: .touchUpInside)
        return closeButton
    }()
    @objc func closebuttonAction(){
        self.removeFromSuperview()
    }
  
   
    lazy var btnDone:UIButton = {
        let btn = UIButton()
        btn.setTitle("done".easyLocalized(), for: .normal)
        btn.backgroundColor = .blueViolet
        btn.layer.cornerRadius = 6
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(btnDoneAction(_:)), for: .touchUpInside)
        btn.titleLabel?.font = .EasyFont(ofSize: 14, style: .bold)
        return btn
    }()
    @objc func btnDoneAction(_ sender:UIButton){
        btnDoneCallBack?()
    }
    
}


