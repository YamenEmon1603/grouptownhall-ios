//
//  EasyTextField.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import UIKit

class EasyTextFieldView: UIView {
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var placeHolderColor:UIColor = .blueViolet
    var placeholderFont: UIFont = .EasyFont(ofSize: 18, style: .medium)
    var limitReachCallBack : (()->Void)?
    var textChangedCallBack : ((_ text : String)->Void)?
    var textEditingBeginCallBack:((_ sender : EasyTextFieldView)->Void)?
    var limit : Int?{
        didSet{
            textField.maxLength = limit ?? .max
        }
    }
    var shadow : Bool? = true{
        didSet{
            if shadow == false{
                addShadow(location: .bottom, color: UIColor.clear, opacity: 0, radius: 0)
                layer.borderWidth = 1
                layer.borderColor = UIColor.lightPeriwinkle.cgColor
                backgroundColor = .Easywhite
            }
          
        }
    }
    var borderChange : Bool = false
    var title:String?
    var text:String?{
        get{
            return textField.text
        }set{
            textChangedCallBack?(newValue ?? "")
            textField.text = newValue
            if isFloting{
                titleLabel.isHidden = false
                titleLabel.text = title
                titleLabel.textColor = placeHolderColor
            }
        }
        
    }
    var keyBoardType:UIKeyboardType?{
        didSet{
            textField.keyboardType = keyBoardType ?? .default
        }
    }
    var isSecureText:Bool = false{
        didSet{
            textField.isSecureTextEntry = isSecureText
        }
    }
    var accessoryView:UIView?{
        didSet{
            textField.inputAccessoryView = accessoryView
        }
    }
    var customInputView:UIView?{
        didSet{
            textField.inputView = customInputView
        }
    }
    var isEnabled:Bool = true{
        didSet{
            textField.isEnabled = isEnabled
        }
    }
    private var isFloting : Bool = true
    convenience init(title:String,isFloating:Bool = true,placeHolderColor:UIColor = .blueViolet, placeholderFont : UIFont = .EasyFont(ofSize: 18, style: .medium)) {
        self.init(frame:.zero)
        backgroundColor = .white
        self.placeHolderColor = placeHolderColor
        self.placeholderFont = placeholderFont
        
        layer.cornerRadius = 10
        addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 15, trailing: -15, top: 5, bottom: -5)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(textField)
        textField.font = placeholderFont
        textField.placeholder = title
       
        titleLabel.isHidden = true
        self.title = title
        textField.addTarget(self, action: #selector(editingDidBegin(_:)), for: .editingDidBegin)
        textField.addTarget(self, action: #selector(editingDidEndOnExit(_:)), for: .editingDidEnd)
        textField.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        if shadow == true{
            addShadow(location: .bottom, color: UIColor.black, opacity: 0.05, radius: 5)
        }else{
            layer.borderWidth = 1
            layer.borderColor = UIColor.lightPeriwinkle.cgColor
            backgroundColor = .Easywhite
        }
    }
    private let textField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
      
        //textField.font =  .EasyFont(ofSize: 18.0, style: .medium)
        textField.textColor = .black
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .medium)
        label.textColor = .slateGrey
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let stackView:UIStackView = {
      let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 2
        return stackView
    }()
    @objc func editingDidBegin(_ textfield: UITextField){
        if isFloting{
            titleLabel.isHidden = false
            titleLabel.text = title
            titleLabel.textColor = placeHolderColor
        }
        textEditingBeginCallBack?(self)
    }
    @objc func editingDidEndOnExit(_ textfield: UITextField){
        if isFloting {
            if textfield.text?.isEmpty == true{
                titleLabel.isHidden = true
                titleLabel.text = ""
                textfield.placeholder = title
            }else{
                titleLabel.textColor = .slateGrey
            }
            
        }
        if borderChange{
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor.paleGrey.cgColor
        }
    }
    @objc func editingChanged(_ textfield: UITextField){
        textChangedCallBack?(textfield.text ?? "")
        if limit != 0 {
            if textfield.text?.count == limit{
                limitReachCallBack?()
            }
        }
        
    }

}
