//
//  ETaskDetailsPageViewController.swift
//  Empolyee Buddy
//
//  Created by Mausum Nandy on 4/8/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit
protocol EasyPageDelegate{
    func pageDidChanged(to:Int)
}
// example Page View Controller
class EasyPageViewController: UIPageViewController {
    
   
    var currentIndex:Int = 0{
        didSet{
            pageDelegate?.pageDidChanged(to: currentIndex)
        }
    }
    var  pageDelegate:EasyPageDelegate?
    var pages: [UIViewController] = [UIViewController]()
    
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
  
        
        
        setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
    }
    func setPage(index:Int) {
        setViewControllers([pages[index]], direction: index>currentIndex ? .forward : .reverse, animated: true, completion: {_ in
            self.currentIndex = index
        })
        
    }
    
}

//MARK: typical Page View Controller Data Source
extension EasyPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else { return pages.last }
        
        guard pages.count > previousIndex else { return nil }
      
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return pages.first }
        
        guard pages.count > nextIndex else { return nil }
  
        return pages[nextIndex]
    }
}
//MARK: typical Page View Controller Data Source
extension EasyPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed,let viewControllerIndex = pages.firstIndex(of: pageViewController.viewControllers!.first!) else
        {
            return
        }
       
        self.currentIndex = viewControllerIndex
        print(viewControllerIndex)
    }
}
