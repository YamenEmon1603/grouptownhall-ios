//
//  StarRatingView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/4/21.
//

import UIKit

class StarRatingView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private var maxRating: Int = 5
    private(set) var currentRating:Int = 0
    convenience init(text:String,subtext:String,maxrating:Int = 5,currentRating:Int = 0) {
        self.init(frame:.zero)
        self.maxRating = maxrating
        self.currentRating = currentRating
        let textStack = UIStackView()
        textStack.axis = .vertical
        textStack.spacing = 5
        textStack.translatesAutoresizingMaskIntoConstraints = false
        textLabel.text = text
        subtextLabel.text = subtext
        
        textStack.addArrangedSubview(textLabel)
        textStack.addArrangedSubview(subtextLabel)
        addSubview(textStack)
        textStack.addAnchorToSuperview(leading: 10, trailing: -10)
        textStack.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -10).isActive = true
        
       
        addSubview(starStack)
        starStack.addAnchorToSuperview( widthMutiplier: 0.5, centeredHorizontally: 0)
        starStack.topAnchor.constraint(equalTo: centerYAnchor, constant: 10).isActive = true
        
    }
    private lazy var buttons : [UIButton] = {
        var buttons : [UIButton] = []
        for i in 1...self.maxRating{
            let button = UIButton()
            button.contentVerticalAlignment = .fill
            button.contentHorizontalAlignment = .fill
            
            if i <= currentRating{
                button.setImage(UIImage(named: "star"), for: .normal)
            }else{
                button.setImage(UIImage(named: "star_empty"), for: .normal)
            }
           
            button.tag = i
            button.translatesAutoresizingMaskIntoConstraints = false
            button.contentMode = .scaleAspectFit
            button.addTarget(self, action: #selector(changeRatingAction(_:)), for: .touchUpInside)
            buttons.append(button)
        }
        return buttons
    }()
    
    @objc func changeRatingAction(_ sender : UIButton){
        currentRating = sender.tag
        for (i,_) in buttons.enumerated(){
            if i+1 <= currentRating{
                buttons[i].setImage(UIImage(named: "star"), for: .normal)
            }else{
                buttons[i].setImage(UIImage(named: "star_empty"), for: .normal)
            }
        }
        
    }
    private let textLabel : UILabel = {
        let textLabel = UILabel()
        textLabel.font = .EasyFont(ofSize: 18.0, style: .bold)
        textLabel.textColor = .black
        textLabel.textAlignment = .center
        return textLabel
    }()
    private let subtextLabel : UILabel = {
        let subtextLabel = UILabel()
        subtextLabel.font = .EasyFont(ofSize: 12.0, style: .regular)
        subtextLabel.textColor = .slateGrey
        subtextLabel.textAlignment = .center
        return subtextLabel
    }()
    lazy var  starStack : UIStackView = {
        let starStack = UIStackView()
        starStack.axis = .horizontal
        starStack.spacing = 5
        starStack.distribution = .fillEqually
        starStack.translatesAutoresizingMaskIntoConstraints = false
        buttons.forEach { btn in
            let view = UIView()
            view.addSubview(btn)
            btn.addAnchorToSuperview( heightMultiplier: 0.8, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
            starStack.addArrangedSubview(view)
           
        }
        return starStack
    }()
    
}
