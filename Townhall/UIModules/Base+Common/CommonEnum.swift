//
//  CommonEnum.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 22/8/21.
//

import Foundation
import  UIKit

enum OTPSource: String{
    case forgotPassword = "ForgotPassword"
    case updateNumber = "UpdateNumber"
    case setpin = "SETPIN"
    case registration = "Registration"
}
enum AlertType {
    case success
    case error
    case wrong
}
