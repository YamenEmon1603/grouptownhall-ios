//
//  NotificationsViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/8/21.
//

import UIKit
import Kingfisher
import SVGKit



class NotificationsViewController: EasyBaseViewController {
    lazy var mView : NotificationView = {
        let mView = NotificationView()
        mView.backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        mView.clearAllButton.addTarget(self, action: #selector(clearAll(_:)), for: .touchUpInside)
        mView.tableView.delegate = self
        mView.tableView.dataSource = self
        return mView
    }()
    let  noDataView : NoDataView = {
        let noDataView = NoDataView(image: UIImage(named: "noNotification")!, text: "no_notification_here".easyLocalized(), subtext: "")
        
        return noDataView
    }()
    var todayArr = [EasyNotification]()
    var earlierArr = [EasyNotification]()
    var sectionsHeaders = [String]() //["today".easyLocalized(),"earlier".easyLocalized()]
    var todayFlag = false
    var earliarFlag = false
    override func viewDidLoad() {
        super.viewDidLoad()
         todayArr = []
         earlierArr = []
        self.view = mView
        getNotifications()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    @objc func btnBackAction(_ sender: UIButton){
        print("btnBackAction")
        self.dismiss(animated: true, completion: nil)
    }
    @objc func clearAll(_ sender: UIButton){
        self.clearNotifications()
    }
    var notifications: [EasyNotification]?{
        didSet{
            let filterNotification = notifications?.filter({$0.title != ""})
            notifications = filterNotification
            if notifications?.count == 0 {
                //self.showToast(message: "no_data_available".easyLocalized())
                mView.clearAllButton.isHidden = true
                mView.tableView.isHidden = true
                //imgNoData.isHidden = false
                //lblNoData.isHidden = false
                mView.addSubview(noDataView)
                noDataView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
                noDataView.topAnchor.constraint(equalTo: mView.navView.bottomAnchor, constant: 0).isActive = true
            }else{
                //imgNoData.isHidden = true
                //lblNoData.isHidden = true
                mView.clearAllButton.isHidden = false
                noDataView.removeFromSuperview()
                if let notificationNum = EasyDataStore.sharedInstance.notificationNumber{
                    
                    if notificationNum < notifications?.count ?? 0{
                        
                        mView.newCountLabel.isHidden = false
                        
                        let newNotification = notifications?.count ?? 0
                        
                        let total = newNotification - notificationNum
                        mView.newCountLabel.text = "\(EasyUtils.shared.getLocalizedDigits("\(total)") ?? "") \("new".easyLocalized())"
                        EasyDataStore.sharedInstance.notificationNumber = notifications?.count
                    }else{
                        mView.newCountLabel.isHidden = true
                        EasyDataStore.sharedInstance.notificationNumber = notifications?.count
                    }
                    
                }else{
                    //viewNotificationNumber.isHidden = true
                    EasyDataStore.sharedInstance.notificationNumber = notifications?.count
                }
                
                
                
                
                if let notificationList = notifications{
                    for item in notificationList{
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let date = dateFormatter.date(from: item.notifiedAt!)
                        if Calendar.current.isDateInToday(date!){
                            todayArr.append(item)
                        }else{
                            earlierArr.append(item)
                        }
                        
                    }
                }
                if todayArr.count > 0{
                    todayFlag = true
                    sectionsHeaders.append( "today".easyLocalized())
                }else{
                    todayFlag = false
                }
                if earlierArr.count > 0{
                    earliarFlag = true
                    sectionsHeaders.append("earlier".easyLocalized())
                }else{
                    earliarFlag = false
                    
                }
                
                mView.tableView.reloadData()
            }
            
        }
    }
}
extension NotificationsViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsHeaders.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if todayFlag == true && earliarFlag == true{
            if section == 0 {
                return todayArr.count
            }else{
                return earlierArr.count
            }
        }else if todayFlag == true && earliarFlag  == false{
            return todayArr.count
        }else if todayFlag == false && earliarFlag  == true {
            return earlierArr.count
        }else{
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationCell.identifier, for: indexPath) as!  NotificationCell
        cell.selectionStyle = .none
        if  indexPath.section == 0 && todayFlag == true  {
            cell.nameLabel.text = todayArr[indexPath.row].message
            if let timeArrivalStr = todayArr[indexPath.row].notifiedAt{
                cell.numberLabel.isHidden = false
                cell.numberLabel.text =  timeInterval(timeAgo: timeArrivalStr )
            }
            if let imUrl = self.todayArr[indexPath.row].iconURL{
                if imUrl.lowercased().hasSuffix(".svg"){
                    if let destination = URL.init(string: imUrl){
                        cell.logoView.kf.setImage(with:destination, options: [.processor(SVGImgProcessor())])
                    }
                }else{
                    cell.logoView.kf.setImage(with: URL(string: imUrl))
                }
            }
        }else  {
            cell.nameLabel.text = earlierArr[indexPath.row].message
            if let timeArrivalStr = earlierArr[indexPath.row].notifiedAt{
                cell.numberLabel.isHidden = false
                cell.numberLabel.text =  timeInterval(timeAgo: timeArrivalStr )
            }
            if let imUrl = self.earlierArr[indexPath.row].iconURL{
                if imUrl.lowercased().hasSuffix(".svg"){
                    if let destination = URL.init(string: imUrl){
                        cell.logoView.kf.setImage(with:destination, options: [.processor(SVGImgProcessor())])
                    }
                }else{
                    cell.logoView.kf.setImage(with: URL(string: imUrl))
                }
            }
        }
       
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view  = UIView()
        let label = EasyPaddingLabel()
        label.text = sectionsHeaders[section]
        label.font = .EasyFont(ofSize: 16.0, style: .medium)
        label.textColor = .black
        label.backgroundColor = .white
        label.layer.borderWidth = 0.0
        view.addSubview(label)
        label.addAnchorToSuperview(leading: 0, trailing: 0, top: 12, bottom: -12)
        
        return view
    }
    public struct SVGImgProcessor:ImageProcessor {
        public func process(item: ImageProcessItem, options: KingfisherOptionsInfo) -> Image? {
            switch item {
            case .image(let image):
                debugPrint("already an image")
                return image
            case .data(let data):
                let imsvg = SVGKImage(data: data)
                return imsvg?.uiImage
            }
        }
        
        public var identifier: String = "com.appidentifier.webpprocessor"
//        public func process(item: ImageProcessItem, options: ImageProcessor) -> UIImage? {
//
//        }
    }
    
}
extension NotificationsViewController{
    func getNotifications() {
        WebServiceHandler.shared.getNotifications(completion: { [self] (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    if let data  = response.data{
                        self.notifications = data
                    }
                   // self.presenter?.view?.notifications = data
                }else{
                    self.showToast(message: response.message ?? "Error")
                    
                }
            case .failure(let error):
                self.showToast(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    func clearNotifications() {
        WebServiceHandler.shared.clearNotifications(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.notifications?.removeAll()
                    
                }else{
                    //self.presenter?.view?.showMessage(str: response.message ?? "")
                    self.showToast(message: response.message ?? "Error")
                }
            case .failure(let error):
                self.showToast(message: error.localizedDescription)
              
            }
        }, shouldShowLoader: true)
    }
    func timeInterval(timeAgo:String) -> String
    {
        let df = DateFormatter()
        let dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.dateFormat = dateFormat
        let dateWithTime = df.date(from: timeAgo)
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateWithTime!, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" : "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" : "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" : "\(day)" + " " + "days ago"
        }else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" : "\(hour)" + " " + "hours ago"
        }else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" : "\(minute)" + " " + "minutes ago"
        }else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second ago" : "\(second)" + " " + "seconds ago"
        } else {
            return "a moment ago"
            
        }
    }
    
}
 
