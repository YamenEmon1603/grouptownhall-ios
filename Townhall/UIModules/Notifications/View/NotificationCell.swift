//
//  NotificationCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/8/21.
//

import UIKit

class NotificationCell: UITableViewCell {
    static let identifier = "NotificationCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        contentView.addSubview(logoView)
        logoView.addAnchorToSuperview(leading: 20, widthMutiplier: 0.1, centeredVertically: 0,heightWidthRatio: 1)
        contentView.addSubview(nameNumberStack)
        nameNumberStack.addAnchorToSuperview( top: 20, bottom: -20)
        nameNumberStack.leadingAnchor.constraint(equalTo: logoView.trailingAnchor,constant: 10).isActive = true
        nameNumberStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        
        let _ =  contentView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
        logoView.image = UIImage(named: "notifications")
        return logoView
    }()
    lazy var  nameNumberStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 5
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        nameNumberStack.addArrangedSubview(nameLabel)
        nameNumberStack.addArrangedSubview(numberLabel)
        return nameNumberStack
    }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.numberOfLines = 0
        label.text = ""
        return label
    }()
    let numberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.textColor = .battleshipGrey
        label.text = ""
        
        return label
    }()
}
