//
//  NotificationView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/8/21.
//

import UIKit

class NotificationView: EasyBaseView {
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
        tableView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var navView: UIView = {
        let navView = UIView()
        navView.addSubview(titleLabel)
        titleLabel.addAnchorToSuperview( centeredVertically: 0)
        navView.addSubview(backButton)
        backButton.addAnchorToSuperview(leading: 10 ,heightMultiplier: 1.0, centeredVertically: 0,  heightWidthRatio: 1)
        titleLabel.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 10).isActive = true
        navView.addSubview(clearAllButton)
        clearAllButton.addAnchorToSuperview( trailing: -15, heightMultiplier: 0.6, widthMutiplier: 0.3, centeredVertically: 0)
        
        navView.addSubview(newCountLabel)
        newCountLabel.addAnchorToSuperview(centeredVertically: 0)
        newCountLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 5).isActive = true
        
        //clearAllButton.leadingAnchor.constraint(equalTo: newCountLabel.trailingAnchor, constant: 10).isActive = true
        navView.backgroundColor = .white
        EasyManager.sharedInstance.delay(0.1) {
            self.newCountLabel.layer.cornerRadius = self.newCountLabel.frame.height/2
            self.newCountLabel.clipsToBounds = true
        }
        let _ =  navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        return navView
        
    }()
    lazy var tableView : UITableView = {
        let tableView = UITableView()
       tableView.register(NotificationCell.self, forCellReuseIdentifier: NotificationCell.identifier)
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        
        return tableView
    }()
    let clearAllButton : UIButton = {
        let button = UIButton()
        button.setTitle("clear_all".easyLocalized().uppercased(), for: .normal)
        button.setTitleColor(.slateGrey, for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 12.0, style: .bold)
        button.isHidden = true
 
        return button
    }()
    
     let backButton:UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "backImage"), for: .normal)
        backButton.tintColor = .black
       
        return backButton
    }()
     let titleLabel : UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 13.0, style: .bold)
        label.textColor = .black
        label.text = "notification".easyLocalized().uppercased()
        return label
    }()
    let newCountLabel : EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.text = ""
        label.font = .EasyFont(ofSize: 10, style: .bold)
        label.textColor = .white
        label.backgroundColor = .blueViolet
        label.layer.borderWidth = 0.0
        label.leftInset = 6
        label.rightInset = 6
        label.isHidden = true
        return label
    }()

}

 
