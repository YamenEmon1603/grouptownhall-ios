//
//  TopUpMobileEntryView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/7/21.
//

import Foundation
import UIKit


class TopUpMobileEntryView : EasyBaseView {
 
    var proceedCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .iceBlue
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.07)
        

        
        containerView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 8).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
   
    //MARK: COMPONENTS
   
    
 
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(topView)
        formView.addSubview(bottomView)
        bottomView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0, heightMultiplier: 0.7)
        topView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        topView.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: 0).isActive = true
       
//        scrollView.translatesAutoresizingMaskIntoConstraints = false
//        formView.addSubview(scrollView)
//        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
//
//
//        let holderView = UIView()
//        holderView.translatesAutoresizingMaskIntoConstraints = false
//        scrollView.addSubview(holderView)
//        holderView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
//
//        holderView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
       
        
//        let stackView = UIStackView()
//        stackView.translatesAutoresizingMaskIntoConstraints = false
//        stackView.axis = .vertical
//        stackView.spacing = 15
//        holderView.addSubview(lblForgotPasswordHeader)
//        holderView.addSubview(lblForgotPasswordSubtitle)
//        holderView.addSubview(stackView)
//
//        lblForgotPasswordHeader.addAnchorToSuperview(leading: 20, trailing: -20, top: 20)
//        lblForgotPasswordSubtitle.addAnchorToSuperview(leading: 20, trailing: -20)
//
//        lblForgotPasswordSubtitle.topAnchor.constraint(equalTo: lblForgotPasswordHeader.bottomAnchor, constant: 8).isActive = true
//        lblForgotPasswordSubtitle.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -20).isActive = true
//
//
//        stackView.addAnchorToSuperview(leading: 20, trailing: -20, bottom: 0)
//
//
//
//        stackView.addArrangedSubview(viewEnterPassword)
//        viewEnterPassword.heightAnchor.constraint(equalToConstant: 56).isActive = true
//
//        stackView.addArrangedSubview(viewConfirmPassword)
//        viewConfirmPassword.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        return formView
    }()
    
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setTitle("proceed".easyLocalized(), for: .normal)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    //MARK: InputViews
    
    lazy var topView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        view.addSubview(numberContainerStack)
        numberContainerStack.addAnchorToSuperview(leading: 20, trailing: -20, top: 10, bottom: -10)
        
        return view
    }()
    lazy var bottomView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .purple
        return view
    }()
   
    lazy var navView: LoginNavBar = {
        let navView = LoginNavBar()
        return navView
    }()
    
    lazy var numberContainerStack : UIStackView  = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillEqually
        view.spacing = 10
        view.addArrangedSubview(enterNumberContainerView)
        view.addArrangedSubview(enterAmountContainerView)
        view.translatesAutoresizingMaskIntoConstraints =  false
        
        return view
    }()
    
    lazy var enterNumberContainerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .whiteGrey
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        view.layer.cornerRadius = 10
        
        view.addSubview(enterNumberTextfield)
        view.addSubview(phonebookSeparatorView)
        view.addSubview(phoneBookButton)
        enterNumberTextfield.addAnchorToSuperview(leading: 0, top: 0, bottom: 0)
        enterNumberTextfield.trailingAnchor.constraint(equalTo: phonebookSeparatorView.leadingAnchor, constant: -8).isActive = true
        phonebookSeparatorView.addAnchorToSuperview(top: 12, bottom: -12)
        phonebookSeparatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        phonebookSeparatorView.trailingAnchor.constraint(equalTo: phoneBookButton.leadingAnchor, constant: 0).isActive = true
        
        phoneBookButton.addAnchorToSuperview( trailing: -12,centeredVertically: 0)
        phoneBookButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        phoneBookButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        
        return view
    }()
    lazy var enterAmountContainerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        
        view.backgroundColor = .whiteGrey
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        view.layer.cornerRadius = 10
        return view
    }()
    lazy var phonebookSeparatorView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .battleshipGrey
        
        return view
    }()
    lazy var phoneBookButton : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setImage(UIImage(named: "phoneBookIcon"), for: .normal)
        return view
    }()
    lazy var enterNumberTextfield : UITextField  = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.borderStyle = .none
        view.placeholder = "Enter Number"
        view.backgroundColor = .clear
        
        return view
    }()
    


}




extension TopUpMobileEntryView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
    
}

