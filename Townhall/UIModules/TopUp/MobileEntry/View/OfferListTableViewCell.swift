

import UIKit
import Kingfisher

class OfferListTableViewCell: UITableViewCell {
    static let identifier = "offerCell"
    
    var easyRechargePlans : RechargePlan?{
        didSet{
            let langIndex = SSLComLanguageHandler.sharedInstance.getCurrentLanguage()
            let test = String((langIndex == .English ? easyRechargePlans?.offerTitle :  easyRechargePlans?.offerTitleBn?.filter { !"\r\n".contains($0) }) ?? "")
            let attributedString =  NSMutableAttributedString(string: test)
            let noSpaceAttributedString = attributedString.trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
            
            if  let isPostpaid = easyRechargePlans?.isForPostpaid,isPostpaid == 1{
                lblType.isHidden = false
                lblType.text = "postpaid".easyLocalized()
            }else{
                lblType.isHidden = true
                
            }
            
            lblOfferTitle.attributedText = noSpaceAttributedString
            lblTimeline.text = langIndex == .English ? easyRechargePlans?.validity :  easyRechargePlans?.validityBn
            operatorImageView.image = nil
            operatorImageView.image = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorShortName == easyRechargePlans?.operatorShortName})?.logoImage
            
            amountLabel.amountText = EasyUtils.shared.getLocalizedDigits("\(easyRechargePlans?.amountMax ?? 0)")
            
            offerStickerView.isHidden = false
            //lblOfferName.text = "EID OFFER"
            if let ribbon = easyRechargePlans?.ribbon{
                if ribbon != ""{
                    offerStickerView.isHidden = false
                    lblOfferName.text = ribbon
                }
                else{
                    offerStickerView.isHidden = true
                    lblOfferName.text = ""
                }
                
            }else{
                offerStickerView.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            
            containerBack.layer.borderWidth = 2
            containerBack.layer.borderColor = UIColor.clearBlue.cgColor
            containerBack.layer.cornerRadius = 10
        } else {
            containerBack.layer.borderWidth = 2
            containerBack.layer.borderColor = UIColor.paleLilac.cgColor
            containerBack.layer.cornerRadius = 10
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        let mainContainer = UIView()
        contentView.addSubview(mainContainer)
        mainContainer.addAnchorToSuperview(leading: 0, trailing: 0, top: 2, bottom: -2)
        
        mainContainer.addSubview(containerBack)
        mainContainer.addSubview(offerStickerView)
        
        offerStickerView.addAnchorToSuperview(leading: 11, top: -5)
        offerStickerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        
        containerBack.addAnchorToSuperview(leading: 15, trailing: -15, top: 5, bottom: -5)
        containerBack.layer.borderWidth = 1
        containerBack.layer.borderColor = UIColor.paleLilac.cgColor
        containerBack.layer.cornerRadius = 10
        containerBack.addSubview(dataView)
        
        
        
        dataView.addAnchorToSuperview(leading : 0 ,trailing: 0, top: 0, bottom: 0)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var containerBack : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    
    lazy var dataView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        view.addSubview(operatorImageView)
        view.addSubview(lblOfferTitle)
        view.addSubview(timelineView)
        view.addSubview(amountLabel)
        view.addSubview(lblType)
        //view.addSubview(offerStickerView)
        
        
        
        operatorImageView.addAnchorToSuperview(leading: 20,centeredVertically: 0)
        operatorImageView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        operatorImageView.widthAnchor.constraint(equalToConstant: .calculatedWidth(30)).isActive = true
        
        
        lblOfferTitle.addAnchorToSuperview(top: 15)
        
        lblOfferTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: .calculatedHeight(30)).isActive = true
        lblOfferTitle.leadingAnchor.constraint(equalTo: operatorImageView.trailingAnchor, constant: 8).isActive = true
        
        lblOfferTitle.trailingAnchor.constraint(equalTo: amountLabel.leadingAnchor, constant: -8).isActive = true
        
        timelineView.addAnchorToSuperview(bottom: -15)
        timelineView.leadingAnchor.constraint(equalTo: operatorImageView.trailingAnchor, constant: 8).isActive = true
        timelineView.heightAnchor.constraint(equalToConstant: .calculatedHeight(20)).isActive = true
        timelineView.topAnchor.constraint(equalTo: lblOfferTitle.bottomAnchor, constant: 4).isActive = true
        
        amountLabel.addAnchorToSuperview(trailing: -15, centeredVertically: 0)
        amountLabel.widthAnchor.constraint(equalToConstant: .calculatedWidth(60)).isActive = true
        
        
        lblType.leadingAnchor.constraint(equalTo: amountLabel.leadingAnchor, constant: 0).isActive = true
        lblType.trailingAnchor.constraint(equalTo: amountLabel.trailingAnchor, constant: 0).isActive = true
        lblType.heightAnchor.constraint(equalToConstant: .calculatedHeight(20)).isActive = true
        lblType.topAnchor.constraint(equalTo: amountLabel.bottomAnchor, constant: 0).isActive = true
        
        return view
    }()
    
    lazy var operatorImageView : UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "gp")
        return view
    }()
    
    lazy var timelineView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .clear
        view.addSubview(imgTimeline)
        view.addSubview(lblTimeline)
        imgTimeline.addAnchorToSuperview(leading: 2, top: 4, bottom: -4, widthMutiplier: 0.3)
        lblTimeline.addAnchorToSuperview(trailing: -4, top: 2, bottom: -2)
        lblTimeline.leadingAnchor.constraint(equalTo: imgTimeline.trailingAnchor, constant: 0).isActive = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.paleLilac.cgColor
        view.layer.cornerRadius = 4
        return view
    }()
    
    
    lazy var imgTimeline: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "miniCalender")
        return view
    }()
    
    
    lazy var lblTimeline: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 10, style: .regular)
        view.text = "107 Days"
        view.textColor = .battleshipGrey
        view.textAlignment = .left
        view.numberOfLines = 1
        return view
    }()
    
    lazy var lblOfferTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 14, style: .medium)
        view.text = "1GB + 50Min + 10sms 1GB 1GB + 50Min + 10sms 1GB"
        view.textColor = .gunMetal
        view.textAlignment = .left
        view.numberOfLines = 2
        
        return view
    }()
    let amountLabel:ViewWithTakaSign = {
        let label = ViewWithTakaSign(amount: "0",postion: .center)
        label.backgroundColor = .clearBlue
        label.layer.cornerRadius = 4
        label.amountLabel.textColor = .white
        label.takasignLabel.textColor = .white
        return label
    }()
    
    
    
    lazy var lblTaka: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 10, style: .medium)
        view.text = "৳"
        view.textColor = .white
        view.textAlignment = .center
        return view
    }()
    lazy var lblAmount: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 16, style: .bold)
        view.text = "200"
        view.textColor = .white
        view.textAlignment = .center
        return view
    }()
    lazy var offerStickerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .clear
        view.addSubview(imgOfferSticker)
        view.addSubview(lblOfferName)
        imgOfferSticker.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        lblOfferName.addAnchorToSuperview(leading: 4, trailing: -4, centeredVertically: -2)
        
        return view
    }()
    
    lazy var imgOfferSticker: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleToFill
        view.image = UIImage(named: "hotOfferIcon")
        return view
    }()
    lazy var lblOfferName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 9, style: .medium)
        view.text = ""
        view.textColor = .white
        view.textAlignment = .center
        return view
    }()
    
    lazy var lblType: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 11, style: .regular)
        view.text = ""
        view.textColor = .battleshipGrey
        view.textAlignment = .center
        view.isHidden = true
        return view
    }()
    
}


