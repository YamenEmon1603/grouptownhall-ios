//
//  OfferListView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 2/8/21.
//

import Foundation
import UIKit


class OfferListView : EasyBaseView {
    var viewAllCallBack : (()->Void)?
    var applyCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,bottom: 0)

        containerView.addSubview(formView)
        formView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,bottom: 0)
    }
   
    //MARK: COMPONENTS
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(offerContainerStack)
        formView.addSubview(bottomView)
        offerContainerStack.addAnchorToSuperview(leading:20, trailing: -20,top: 0)
        //offerContainerStack.heightAnchor.constraint(equalToConstant: 60).isActive = true
        bottomView.addAnchorToSuperview(leading: 0, trailing: 0 , bottom: 0)
        bottomView.topAnchor.constraint(equalTo: offerContainerStack.bottomAnchor, constant: 8).isActive = true
        return formView
    }()

    //MARK: InputViews
    lazy var offerContainerStack : UIStackView  = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillEqually
        view.addArrangedSubview(offerForView)
        //offerForView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        view.translatesAutoresizingMaskIntoConstraints =  false
        
        return view
    }()
    
    lazy var offerForView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.isHidden = true
        view.backgroundColor = .offWhite
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.palePeach.cgColor
        view.addSubview(offerForContainerStack)
        view.addSubview(applyButton)
        offerForContainerStack.addAnchorToSuperview(leading: 10, top: 8, bottom: -8)
        offerForContainerStack.trailingAnchor.constraint(equalTo: applyButton.leadingAnchor, constant: -4).isActive = true
        applyButton.addAnchorToSuperview(trailing: -10,centeredVertically: 0)
        applyButton.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        applyButton.widthAnchor.constraint(equalToConstant: .calculatedWidth(80)).isActive = true
        DispatchQueue.main.async {
            view.layer.cornerRadius = 10
        }
        return view
    }()
    lazy var bottomView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        //view.backgroundColor = .purple
        view.addSubview(headerView)
        view.addSubview(offersTableView)
        headerView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        headerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(40)).isActive = true
        offersTableView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        offersTableView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 8).isActive = true
        return view
    }()
   

    
    
    lazy var offerForContainerStack : UIStackView  = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 0
        view.addArrangedSubview(selectForYouHeader)
        view.addArrangedSubview(selectForYouSubtitle)

        return view
    }()

    lazy var applyButton : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setTitle("apply".easyLocalized(), for: .normal)
        view.titleLabel?.font = .EasyFont(ofSize: 11, style: .bold)
        view.setTitleColor(.white, for: .normal)
        view.backgroundColor = .marigold
        DispatchQueue.main.async {
            view.layer.cornerRadius = 4
        }
        view.addTarget(self, action: #selector(viewApplyAction(_:)), for: .touchUpInside)
        return view
    }()
    @objc func viewApplyAction(_ sender:UIButton){
        applyCallBack?()
    }
    let selectForYouHeader :  UILabel  = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "specially_for_you".easyLocalized()
        label.textColor = .marigold
        label.font = .EasyFont(ofSize: 11, style: .medium)
       
        return label
    }()
    let selectForYouSubtitle :  UILabel  = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.numberOfLines = 2
        label.textColor = .gunMetal
        label.font = .EasyFont(ofSize: 11, style: .regular)
       
        return label
    }()
    
    // AMount
    lazy var offersTableView: UITableView = {
        let offersTableView = UITableView(frame: .zero)
        offersTableView.translatesAutoresizingMaskIntoConstraints = false
        offersTableView.separatorStyle = .none
        
        offersTableView.backgroundColor = .white
        offersTableView.register(OfferListTableViewCell.self, forCellReuseIdentifier: OfferListTableViewCell.identifier)
        return offersTableView
    }()
    lazy var headerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .Easywhite
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.paleLilac.cgColor
        var img = UIImageView()
        img.image  = UIImage(named: "firequick")
        //om = UIImage(named: "firequick")
        var  lbl = UILabel()
        lbl.text = "most_populer_offer".easyLocalized()
        lbl.font = .EasyFont(ofSize: 10, style: .medium)
        lbl.textColor = .battleshipGrey
        view.addSubview(img)
        view.addSubview(lbl)
        view.addSubview(viewAll)
        img.addAnchorToSuperview(leading: 10, heightMultiplier: 0.5, centeredVertically: 0, heightWidthRatio: 0.8)
        
        viewAll.addAnchorToSuperview(trailing: -10,  heightMultiplier: 0.8,widthMutiplier: 0.2, centeredVertically: 0)
        lbl.addAnchorToSuperview(top: 4, bottom: -4)
        lbl.leadingAnchor.constraint(equalTo: img.trailingAnchor, constant: 8).isActive = true
        lbl.trailingAnchor.constraint(equalTo: viewAll.leadingAnchor, constant: -4).isActive = true
        
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        
        
        return view
    }()
    lazy var viewAll : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setTitle("view_all".easyLocalized(), for: .normal)
        view.titleLabel?.font = .EasyFont(ofSize: 11, style: .bold)
        view.setTitleColor(.clearBlue, for: .normal)
        view.backgroundColor = .clear
        view.addTarget(self, action: #selector(viewAllAction(_:)), for: .touchUpInside)
        return view
    }()
    @objc func viewAllAction(_ sender:UIButton){
        viewAllCallBack?()
    }
}



