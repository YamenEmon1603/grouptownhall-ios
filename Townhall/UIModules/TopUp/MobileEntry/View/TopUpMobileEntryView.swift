//
//  TopUpMobileEntryView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/7/21.
//

import Foundation
import UIKit


class TopUpMobileEntryView : EasyBaseView {
    private enum Buttons: CaseIterable{
        case fifty,Hundred,TwoHundred,FiveHundred
        var tag:Int{
            switch self {
            case .fifty:
                return 50
            case .Hundred:
                return 100
            case .TwoHundred:
                return 200
            case .FiveHundred:
                return 500
            }
        }
        var title:String{
            switch self {
            case .fifty,.Hundred,.TwoHundred,.FiveHundred:
                return "\(self.tag)"
            }
        }
    }
    lazy var amountButtonStack: UIStackView = {
        let amountStack = UIStackView()
        amountStack.axis = .horizontal
        amountStack.distribution = .fillEqually
        amountStack.spacing = 2.5
        Buttons.allCases.forEach { btn in
            let view = QuickAmountView(title: btn.title)
            view.button.tag = btn.tag
            view.backgroundColor = .clear
            view.button.addTarget(self, action: #selector(quickAmountBtnAction(_:)), for: .touchUpInside)
            amountStack.addArrangedSubview(view)
        }
        
        return amountStack
    }()
    
    @objc func quickAmountBtnAction(_ sender:UIButton){
        
        amountButtonStack.arrangedSubviews.forEach { v in
            if let view = v as? QuickAmountView{
                
                if sender.tag == view.button.tag {
                    view.isSelected = !view.isSelected
                    enterAmountTextfield.text = "\(sender.tag)"
                    
                }else{
                    view.isSelected = false
                }
            }
        }
    }
    
    var proceedCallBack : (()->Void)?
    var backCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.075)
        
        
        
        containerView.addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 1).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
    
    //MARK: COMPONENTS
    
    
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(topView)
        formView.addSubview(bottomView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0)
        topView.heightAnchor.constraint(equalToConstant: .calculatedHeight(150)).isActive = true
        
        
        bottomView.addAnchorToSuperview(leading: 0, trailing: 0, bottom : 0)
        bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
        
        
        return formView
    }()
    
    lazy var btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        btnProceed.isEnabled = true
        btnProceed.backgroundColor = .clearBlue
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    //MARK: InputViews
    
    lazy var topView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        view.addSubview(numberContainerStack)
        numberContainerStack.addAnchorToSuperview(leading: 20, trailing: -20, top: 10, bottom: -10)
        
        return view
    }()
    lazy var bottomView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .purple
        view.addSubview(offerListVw)
        offerListVw.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        return view
    }()
    
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "mobile_recharge".easyLocalized(), rightItem: quickieButton)
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        return navView
    }()
    
    lazy var numberContainerStack : UIStackView  = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.axis = .vertical
        view.distribution = .fillEqually
        view.spacing = .calculatedHeight(15)
        view.addArrangedSubview(enterNumberContainerView)
        view.addArrangedSubview(enterAmountContainerView)
        enterNumberContainerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(65)).isActive = true
        enterAmountContainerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(65)).isActive = true
        
        return view
    }()
    lazy var backButton:UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "backImage"), for: .normal)
        backButton.tintColor = .black
        backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        return backButton
    }()
    @objc func btnBackAction(_ sender:UIButton){
        backCallBack?()
    }
    
    lazy var enterNumberContainerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .whiteGrey
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        view.layer.cornerRadius = 10
        
        view.addSubview(enterNumberTextfield)
        view.addSubview(phonebookSeparatorView)
        view.addSubview(phoneBookButton)
        enterNumberTextfield.addAnchorToSuperview(leading: 10, top: 0, bottom: 0)
        enterNumberTextfield.trailingAnchor.constraint(equalTo: phonebookSeparatorView.leadingAnchor, constant: -8).isActive = true
        phonebookSeparatorView.addAnchorToSuperview(top: 12, bottom: -12)
        phonebookSeparatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        phonebookSeparatorView.trailingAnchor.constraint(equalTo: phoneBookButton.leadingAnchor, constant: -10).isActive = true
        
        phoneBookButton.addAnchorToSuperview( trailing: -12,centeredVertically: 0)
        phoneBookButton.heightAnchor.constraint(equalToConstant: .calculatedHeight(50)).isActive = true
        phoneBookButton.widthAnchor.constraint(equalToConstant: .calculatedWidth(40)).isActive = true
        
        
        
        return view
    }()
    lazy var enterAmountContainerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        
        view.backgroundColor = .whiteGrey
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        view.layer.cornerRadius = 10
        view.addSubview(amountButtonStack)
        view.addSubview(enterAmountTextfield)
        enterAmountTextfield.addAnchorToSuperview(leading: 0 , top: 0, bottom: 0,widthMutiplier: 0.50)
        enterAmountTextfield.trailingAnchor.constraint(equalTo: amountButtonStack.leadingAnchor, constant: 0).isActive = true
        
        
        amountButtonStack.addAnchorToSuperview(trailing: -5, centeredVertically: 0)
        amountButtonStack.heightAnchor.constraint(equalToConstant: .calculatedHeight(40)).isActive = true
        //amountButtonStack.widthAnchor.constraint(equalToConstant:180).isActive = true
        return view
    }()
    
    lazy var phonebookSeparatorView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .lightPeriwinkle
        
        return view
    }()
    lazy var phoneBookButton : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setImage(UIImage(named: "phoneBookIcon"), for: .normal)
        return view
    }()
    lazy var enterNumberTextfield : EasyTextFieldView  = {
        let view = EasyTextFieldView(title: "enter_mobile_number".easyLocalized(),placeholderFont: .EasyFont(ofSize: 18, style: .regular))
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .clear
        view.isEnabled = false
        view.keyBoardType = .numberPad
        view.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        view.limit = 11
        
        return view
    }()
    
    // AMount
    
    lazy var enterAmountTextfield : EasyTextFieldView  = {

        
        let textView = EasyTextFieldView(title: "enter_amount".easyLocalized(),placeHolderColor: .slateGrey,placeholderFont: .EasyFont(ofSize: 18, style: .regular))
        textView.backgroundColor = .clear
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        textView.keyBoardType = .numberPad
        textView.limit = 4
        return textView
        
    }()
    
    lazy var offerListVw: OfferListView  = {
        let view = OfferListView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    let quickieButton : UIButton = {
        let button = UIButton()
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.marigold.cgColor
        button.layer.cornerRadius = 4
        button.setTitle("quickey".easyLocalized().uppercased(), for: .normal)
        button.setImage(UIImage(named: "quick1"), for: .normal)
        button.setTitleColor(.marigold, for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 12.0, style: .bold)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return button
    }()
    let btnNumberAdded : UIButton = {
        let button = UIButton()
        button.backgroundColor = .clearBlue
        button.layer.cornerRadius = 4
        button.setTitle("number_added".easyLocalized().uppercased(), for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 11, style: .bold)
        button.setTitleColor(.white, for: .normal)
        
        return button
    }()
    
}

extension TopUpMobileEntryView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
}

