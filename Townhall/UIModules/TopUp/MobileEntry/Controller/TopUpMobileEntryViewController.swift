//
//  TopUpMobileEntryViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/7/21.
//

import UIKit
import ContactsUI

protocol NumberSelectedFromList {
    func selected(number:String,operatorObj:MBOperator,connetctionType:ConectionType)
}
protocol OfferSelectedFromViewAllList {
    func offerSelected(offer:RechargePlan)
}

class TopUpMobileEntryViewController: EasyBaseViewController {
    var selectedNumber : SelectNumber?{
        didSet{
            if let number = selectedNumber{
                let vc  = OfferAndOparatorSelectionVC()
                vc.selectedNumber = number
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    var offeredData : RechargePlan?
    
    lazy var mView: TopUpMobileEntryView = {
        let view = TopUpMobileEntryView()
        view.navView.backButtonCallBack = backAction
        view.offerListVw.viewAllCallBack = viewAll
        view.enterAmountTextfield.textChangedCallBack = amountAction(_:)
        view.enterNumberTextfield.textChangedCallBack = phoneNumberEntered(_:)
        view.quickieButton.addTarget(self, action: #selector(quickieAction), for: .touchUpInside)
        //view.phoneBookButton.addTarget(self, action: #selector(numberEntry), for: .touchUpInside)
        view.offerListVw.applyButton.addTarget(self, action: #selector(applyOffer), for: .touchUpInside)
        view.btnNumberAdded.addTarget(self, action: #selector(numberAddedAction), for: .touchUpInside)
        
        view.proceedCallBack = proccedAction
        
        return view
    }()
    func amountAction(_ text : String){
        //amountSet = Int(text)
        print(text)
        selectedNumber?.amount =  Int(text) ?? 0
        filterValue(amount: Int(text) ?? 0)
    }
    func phoneNumberEntered(_ text : String){
        if text.count == mView.enterNumberTextfield.limit{
            debugPrint("Trigger Now")
            OperatorSelectionViewController.show(on: self, delegate: self)
        }
        
    }
    var first5Plans: [RechargePlan]?{
        didSet{
            if first5Plans?.count == 0{
                mView.offerListVw.offerForView.isHidden = true
            }
            mView.offerListVw.offersTableView.reloadData()
            
        }
    }
    
    
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        if EasyManager.sharedInstance.isFromRechargeRequest == false{
            getFavoriteNumber()
        }
      
        mView.offerListVw.offersTableView.dataSource = self
        mView.offerListVw.offersTableView.delegate = self
        mView.offerListVw.offersTableView.reloadData()
        
        if EasyManager.sharedInstance.isFromRechargeRequest == false{
            let numberSelectionTap = UITapGestureRecognizer(target: self, action: #selector(selectNumberFromListAction))
        mView.enterNumberContainerView.addGestureRecognizer(numberSelectionTap)
            mView.navView.rightItem?.isHidden = false
            mView.phoneBookButton.addTarget(self, action: #selector(numberEntry), for: .touchUpInside)
        }else{
            //mView.enterNumberContainerView.isUserInteractionEnabled = false
            mView.enterNumberTextfield.isEnabled = true
            mView.navView.rightItem?.isHidden = true
            mView.phoneBookButton.addTarget(self, action: #selector(contactPickAction(_:)), for: .touchUpInside)
            
//            mView.enterNumberTextfield.layer.zPosition = 1
            
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if EasyDataStore.sharedInstance.seletedNumbersArray.count > 0{
            mView.navView.rightItem = mView.btnNumberAdded
            mView.btnNumberAdded.isHidden = false
            let added =  EasyDataStore.sharedInstance.seletedNumbersArray.count
            mView.btnNumberAdded.setTitle("\(added) "+"number_added".easyLocalized().uppercased(), for: .normal)
        }else{
            mView.navView.rightItem = mView.quickieButton
            mView.btnNumberAdded.isHidden = false
            
        }
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        prepareMostPopular()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    @objc func selectNumberFromListAction(){
        let vc = NumberListViewController()
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func backAction(){
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        EasyDataStore.sharedInstance.seletedNumbersArray = []
        self.navigationController?.popViewController(animated: true)
    }
    func viewAll(){
        print("view all...lol")
        //mView.offerListVw.offerForView.isHidden = true
        let vc = ViewAllOfferController()
        vc.offerDelegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @objc func quickieAction(){
        let vc = QuickieViewController()
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(vc, animated: true)
       // self.present(vc, animated: true, completion: nil)
    }
    
    func proccedAction(){
        dismissKeyboard()
        if mView.enterNumberTextfield.text?.isEmpty == true{
            showToast(message: "enter_number".easyLocalized())
        }else if mView.enterAmountTextfield.text?.isEmpty == true{
            showToast(message: "enter_amount_first".easyLocalized())
        }else if mView.enterNumberTextfield.text?.count != 11{
            showToast(message: "mobile_number_need_to_be_11_digit".easyLocalized())
        }else{
            debugPrint("now go")
            let vc  = OfferAndOparatorSelectionVC()
            vc.selectedNumber = self.selectedNumber
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @objc func numberEntry(){
       
        let vc = NumberListViewController()
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func filterValue(amount: Int){
        
        if amount > 9 {
            //amountSet = "\(amount)"
            let offerData = EasyDataStore.sharedInstance.easyRechargePlans.filter{$0.amountMax! > amount}
            if let offerAmount = offerData.first?.amountMax{
                let amountForOffer = offerAmount - amount
                let offer = offerData.first?.offerTitle
                let nextOffer = "Add \(amountForOffer)TK more to get \(offer ?? "nil")"
                if amountForOffer > 0{
                    mView.offerListVw.offerForView.isHidden = false
                }else{
                    mView.offerListVw.offerForView.isHidden = true
                }
                offeredData = offerData.first
                mView.offerListVw.selectForYouSubtitle.text = nextOffer
                
            }
            first5Plans = offerData.enumerated().compactMap{ $0.offset < 5 ? $0.element : nil }
            
            
        }
    }
    @objc func contactPickAction(_ sender: UIButton){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    @objc func numberAddedAction(){
        let vc = RechargeSummerViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func applyOffer(){
        mView.offerListVw.offerForView.isHidden = false
        mView.enterAmountTextfield.text = "\(offeredData?.amountMax ?? 0)"
        
    }
    
    func prepareMostPopular(op : MBOperator? = nil,conectionType:String = "", amount: Int = 0){
        var filtered = EasyDataStore.sharedInstance.easyRechargePlans
        if let op = op{
            filtered = filtered.filter({$0.operatorShortName == op.operatorShortName})
        }
        if conectionType.lowercased() == "prepaid"{
            filtered = filtered.filter({$0.isForPrepaid == 1})
        }
        if conectionType.lowercased() == "postpaid"{
            filtered = filtered.filter({$0.isForPostpaid == 1})
        }
        if amount > 0{
            filtered = filtered.filter({(item) in
                if let maxAmount =  item.amountMax{
                    return maxAmount > amount
                }
                return false
                
            })
        }
        
        
        var five = filtered.filter({$0.isPopular == 1})
        if five.count < 5 {
            let sorted = filtered.sorted(by: {$0.usageCnt! > $1.usageCnt!}).filter({$0.isPopular == 0})
            five.append(contentsOf: sorted.prefix(5 - five.count))
        }
        first5Plans = five
        
        
        
        
    }
}
extension TopUpMobileEntryViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return first5Plans?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferListTableViewCell.identifier, for: indexPath) as! OfferListTableViewCell
        
        cell.easyRechargePlans = first5Plans?[indexPath.row]
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        mView.enterAmountTextfield.text  = "\(first5Plans?[indexPath.row].amountMax ?? 0)"
        if first5Plans?.count ?? 0 > 1{
            
            filterValue(amount: first5Plans?[indexPath.row].amountMax ?? 0)
        }
        //  offerSet = first5Plans?[indexPath.row]
        mView.quickAmountBtnAction(UIButton())
    }
    
    
    
}

extension TopUpMobileEntryViewController : NumberSelectedFromList,OfferSelectedFromViewAllList{
    func offerSelected(offer: RechargePlan) {
        mView.quickAmountBtnAction(UIButton())
        mView.enterAmountTextfield.text = "\(offer.amountMax ?? 0)"
    }
    
    func selected(number: String, operatorObj : MBOperator, connetctionType: ConectionType) {
        let amountSet = Int(mView.enterAmountTextfield.text ?? "0")
        selectedNumber = SelectNumber(number: number, operatorObj: operatorObj, connetctionType: connetctionType,amount: amountSet, scheduleTime: nil )
        
        
    }
}
extension TopUpMobileEntryViewController{
    func getFavoriteNumber()  {
        WebServiceHandler.shared.getFavoriteNumbers(completion: { result in
            switch result{
            case .success(let response):
                if response.code == 200{
                    EasyDataStore.sharedInstance.favoriteData = response.data
                    self.getRecents()
                    
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    func getRecents() {
        WebServiceHandler.shared.getRecentTransactions(completion: { result in
            switch result{
            case .success(let response):
                if response.code == 200{
                    EasyDataStore.sharedInstance.recentData = response.data
                    
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
    
    
}
extension TopUpMobileEntryViewController :  OperatorSelectionDelegate {
    func didSelect(operatorObject: MBOperator, connectionType: ConectionType) {
        print("\(operatorObject.operatorName ?? "") \(connectionType.value)")
        if EasyManager.sharedInstance.isFromRechargeRequest == true{
            
            selectedNumber?.operatorObj = operatorObject
            selectedNumber?.connetctionType = connectionType.value
            //if mView.enterAmountTextfield.text?.isEmpty == false{
                self.selectedNumber =   SelectNumber(number: mView.enterNumberTextfield.text, operatorObj: operatorObject, connetctionType: connectionType,amount: Int(mView.enterAmountTextfield.text ?? "") ?? 0, scheduleTime: nil )
            //}
           
//            if selectedNumber?.amount != nil{
//                mView.btnProceed.isEnabled = true
//                mView.btnProceed.backgroundColor = .clearBlue
//            }else{
//                mView.btnProceed.isEnabled = false
//                mView.btnProceed.backgroundColor = .coolGrey
//            }
        }
    }
}
extension TopUpMobileEntryViewController : CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        debugPrint("\(contactProperty.key) : \((contactProperty.value as! CNPhoneNumber).stringValue)")
        var  numberString = (contactProperty.value as! CNPhoneNumber).stringValue.withoutSpecialCharacters.deletingPrefix("88").replacingOccurrences(of: "-", with: "")
        numberString = numberString.replacingOccurrences(of: " ", with: "")
        
        selectionDone(numberString: numberString)
    }
    func selectionDone(numberString: String){
        mView.enterNumberTextfield.text = numberString
        DispatchQueue.main.async {
            OperatorSelectionViewController.show(on: self, delegate: self)
        }
        
    }
    
}
