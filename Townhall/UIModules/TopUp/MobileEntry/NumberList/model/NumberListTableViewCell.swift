//
//  NumberListTableViewCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 5/8/21.
//

import UIKit


class NumberListTableViewCell: UITableViewCell {
    static let identifier = "NumbersCell"
    
    var fav:FevoriteContact?{
        didSet{
            operatorImageView.image = fav?._operator?.logoImage
            lblNameTitle.text = fav?.name
            lblNumber.text = fav?.phone
            lblAddTime.text = ""
        }
    }
    var recents:RecentTransaction?{
        didSet{
            operatorImageView.image = recents?._operator?.logoImage
            lblNameTitle.text = recents?.phone
            lblNumber.text = recents?.connectionType
            lblAddTime.text = recents?.date
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        let container = UIView()
        addSubview(container)
        
   
        
        container.addAnchorToSuperview(leading: 0, trailing: 0, top: 5, bottom: -5)
//        container.layer.borderWidth = 1
//        container.layer.borderColor = UIColor.paleLilac.cgColor
//        container.layer.cornerRadius = 10
        container.addSubview(dataView)
        
        
        
        dataView.addAnchorToSuperview(leading : 0 ,trailing: 0, top: 0, bottom: 0)
 
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var dataView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        let stack = UIStackView()
        view.backgroundColor = .white
        view.addSubview(operatorImageView)
        view.addSubview(stack)
        view.addSubview(lblAddTime)
        
        lblAddTime.addAnchorToSuperview( trailing: -15 , centeredVertically: 0)
        lblAddTime.widthAnchor.constraint(equalToConstant: .calculatedWidth(60)).isActive = true
        
        
        
        operatorImageView.addAnchorToSuperview(leading: 18,top: 15,bottom: -15)
        operatorImageView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        operatorImageView.widthAnchor.constraint(equalTo:operatorImageView.heightAnchor).isActive = true
        
      
        stack.axis = .vertical
        stack.spacing = 5
        stack.addArrangedSubview(lblNameTitle)
        stack.addArrangedSubview(lblNumber)
        stack.addAnchorToSuperview(centeredVertically: 0, heightWidthRatio: 0.8)
        stack.leadingAnchor.constraint(equalTo: operatorImageView.trailingAnchor, constant: 8).isActive = true
        stack.trailingAnchor.constraint(equalTo: lblAddTime.leadingAnchor, constant: -8).isActive = true

        return view
    }()
    
    lazy var operatorImageView : UIImageView = {
       let view = UIImageView()
        view.contentMode = .scaleAspectFit
        //view.image = UIImage(named: "gp")
        return view
    }()
    
 

    lazy var lblNameTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 14, style: .medium)
        view.text = ""
        view.textColor = .black
        view.textAlignment = .left
        view.numberOfLines = 2
       
        return view
    }()
    lazy var lblAddTime : UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 11, style: .regular)
        view.text = ""
        view.textColor = .coolGrey
        view.textAlignment = .left
        view.numberOfLines = 1
       
        return view
    }()
    
    lazy var lblNumber: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .EasyFont(ofSize: 11, style: .regular)
        view.text = ""
        view.textColor = .slateGrey
        view.textAlignment = .left
        view.numberOfLines = 1
       
        return view
    }()
    

    
}


