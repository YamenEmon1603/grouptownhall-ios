//
//  NumberListViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 5/8/21.
//

import UIKit
import ContactsUI

class NumberListViewController: EasyBaseViewController, OperatorSelectionDelegate {
    func didSelect(operatorObject: MBOperator, connectionType: ConectionType) {
        
        if let num = numberTyped{
            self.dismiss(animated: true) {
                self.delegate?.selected(number: num, operatorObj: operatorObject, connetctionType: connectionType)
            }
        }else{
            if let mobile = EasyDataStore.sharedInstance.user?.mobile{
                let user = SelectNumber(number: mobile, operatorObj: operatorObject, connetctionType: connectionType, amount: 0, scheduleTime: Date())
                //OwnNumber(number: mobile , operatorObj: operatorObject, connetctionType: connectionType.value, amount: 0, scheduleTime: Date())
                EasyDataStore.sharedInstance.ownNumber = user
                mView.numbersTableView.reloadData()
                self.dismiss(animated: true) {
                    if let mobile = user.number, let op = user.operatorObj,let type = user.connetctionType{
                        
                        self.delegate?.selected(number: mobile, operatorObj: op, connetctionType: ConectionType(type) )
                    }
                    
                }
            }
            
        }
    }
    
    var numberTyped : String?
    lazy var mView : NumberListView = {
        let mView = NumberListView()
        mView.contactCallBack = contactPickAction
        mView.navView.backButtonCallBack = backButtonAction
        mView.enterNumberTextfield.limitReachCallBack = textLimit
        
        return mView
    }()
    var delegate : NumberSelectedFromList?
    
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        mView.numbersTableView.dataSource = self
        mView.numbersTableView.delegate = self
        mView.numbersTableView.reloadData()
        mView.enterNumberTextfield.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //getFavoriteNumber()
    }
    
    
    func contactPickAction(){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    func backButtonAction(){
        self.dismiss(animated: true, completion: nil)
    }
    func textLimit(){
        print("Limit reach")
        if let number = mView.enterNumberTextfield.text{
            numberTyped = number
            OperatorSelectionViewController.show(on: self, delegate: self)
        }
    }
}
extension NumberListViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = EasyPaddingLabel()
        if section == 0{
            label.text = "my_number".easyLocalized().uppercased()
            return label
        }else if section == 1 {
            label.text = "select_from_favourite".easyLocalized().uppercased()
            return label
        }else{
            label.text = "select_from_recent".easyLocalized().uppercased()
            return label
        }
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return EasyDataStore.sharedInstance.favoriteData?.count ?? 0
        }else{
            return EasyDataStore.sharedInstance.recentData?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NumberListTableViewCell.identifier, for: indexPath) as! NumberListTableViewCell
        if indexPath.section  == 0{
            cell.lblNumber.isHidden = true
            let ownModel = EasyDataStore.sharedInstance.ownNumber
            if let mobile = EasyDataStore.sharedInstance.ownNumber?.number{
                cell.lblNameTitle.text = mobile
                cell.operatorImageView.kf.setImage(with: URL(string: ownModel?.operatorObj?.imageUrl ?? ""))// = //ownModel?.operatorObj?.imageUrl //kf.setImage(with: URL(string: imUrl))
                cell.lblAddTime.text = ""
            }
        }else if indexPath.section == 1{
            cell.fav = EasyDataStore.sharedInstance.favoriteData?[indexPath.row]
            cell.lblNumber.isHidden = false
            
        }else if indexPath.section == 2{
            cell.recents = EasyDataStore.sharedInstance.recentData?[indexPath.row]
            cell.lblNumber.isHidden = false
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if let own = EasyDataStore.sharedInstance.ownNumber{
                if  own.number?.isEmpty == true || own.operatorObj == nil || own.connetctionType?.isEmpty == true{
                    OperatorSelectionViewController.show(on: self, delegate: self)
                }else{
                    self.dismiss(animated: true) {
                        self.delegate?.selected(number: own.number ?? "", operatorObj: own.operatorObj! , connetctionType: ConectionType(own.connetctionType?.lowercased() ?? ""))
                    }
                    print("Empty")
//                    self.dismiss(animated: true) {
//                        self.delegate?.selected(number: own.number ?? "", operatorObj: own.operatorObj ?? nil, connetctionType: ConectionType(type.lowercased()) )
//                    }
                }
               
                
            }
        }
        if indexPath.section == 1{
            if let fev = EasyDataStore.sharedInstance.favoriteData?[indexPath.row],fev.phone?.isEmpty == false,let op  = fev._operator{
                self.dismiss(animated: true) {
                    self.delegate?.selected(number: fev.phone ?? "", operatorObj: op, connetctionType: .init(fev.connectionType?.lowercased() ?? ""))
                }
            }
            
            
        }else if indexPath.section == 2{
            if let fev =  EasyDataStore.sharedInstance.recentData?[indexPath.row],fev.phone?.isEmpty == false,let op  = fev._operator{
                self.dismiss(animated: true) {
                    self.delegate?.selected(number: fev.phone ?? "", operatorObj: op, connetctionType: .init(fev.connectionType?.lowercased() ?? ""))
                }
            }
        }
    }
    
}


extension NumberListViewController : CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        debugPrint("\(contactProperty.key) : \((contactProperty.value as! CNPhoneNumber).stringValue)")
        var  numberString = (contactProperty.value as! CNPhoneNumber).stringValue.withoutSpecialCharacters.deletingPrefix("88").replacingOccurrences(of: "-", with: "")
        numberString = numberString.replacingOccurrences(of: " ", with: "")
        
        selectionDone(numberString: numberString)
    }
    func selectionDone(numberString: String){
        numberTyped = numberString
        DispatchQueue.main.async {
            OperatorSelectionViewController.show(on: self, delegate: self)
        }
        
    }
    
}
extension String {
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}
