//
//  NumberListView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 5/8/21.
//

import Foundation
import UIKit


class NumberListView : EasyBaseView {
    var contactCallBack : (()->Void)?
    var textLimitCallBack : (()->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: -.calculatedHeight(0))
        
        containerView.addSubview(navView)
        containerView.addSubview(enterNumberContainerView)
        containerView.addSubview(numbersTableView)
        
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.075)
        
        enterNumberContainerView.addAnchorToSuperview(leading: 20, trailing: -20)
        enterNumberContainerView.topAnchor.constraint(equalTo: navView.bottomAnchor, constant: 0).isActive = true
        enterNumberContainerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(60)).isActive = true
        
        numbersTableView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        numbersTableView.topAnchor.constraint(equalTo: enterNumberContainerView.bottomAnchor, constant: 20).isActive = true
        
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "")
        navView.backgroundColor = .white
        return navView
    }()
    
    lazy var enterNumberContainerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .whiteGrey
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.clearBlue.cgColor
        view.layer.cornerRadius = 10
        
        view.addSubview(enterNumberTextfield)
        view.addSubview(phonebookSeparatorView)
        view.addSubview(phoneBookButton)
        enterNumberTextfield.addAnchorToSuperview(leading: 10, top: 0, bottom: 0)
        enterNumberTextfield.trailingAnchor.constraint(equalTo: phonebookSeparatorView.leadingAnchor, constant: -8).isActive = true
        phonebookSeparatorView.addAnchorToSuperview(top: 12, bottom: -12)
        phonebookSeparatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        phonebookSeparatorView.trailingAnchor.constraint(equalTo: phoneBookButton.leadingAnchor, constant: -10).isActive = true
        
        phoneBookButton.addAnchorToSuperview( trailing: -12,centeredVertically: 0)
        phoneBookButton.heightAnchor.constraint(equalToConstant: .calculatedHeight(50)).isActive = true
        phoneBookButton.widthAnchor.constraint(equalToConstant: .calculatedWidth(40)).isActive = true
        
        
        
        return view
    }()
    lazy var enterNumberTextfield : EasyTextFieldView  = {
        let view = EasyTextFieldView(title: "enter_number".easyLocalized())
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.keyBoardType = .numberPad
        view.backgroundColor = .clear
        view.limit = 11
//        view.font = .EasyFont(ofSize: 18, style: .regular)
        
        return view
    }()
    lazy var phonebookSeparatorView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .lightPeriwinkle
        
        return view
    }()
    lazy var phoneBookButton : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setImage(UIImage(named: "phoneBookIcon"), for: .normal)
        view.addTarget(self, action: #selector(contactAction(_:)), for: .touchUpInside)
        return view
    }()
    @objc func contactAction(_ sender:UIButton){
        contactCallBack?()
    }
    
    lazy var numbersTableView : UITableView = {
        let tableView = UITableView()
        tableView.register(NumberListTableViewCell.self, forCellReuseIdentifier: NumberListTableViewCell.identifier)
        tableView.separatorColor = .paleLilac
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
   
    
}
