//
//  AddToFavView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/11/21.
//

import Foundation
import UIKit


class AddToFavView: EasyBaseView {
    var proceedCallBack : (()->Void)?
   
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0,bottom: 0)
        tableView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
//        addSubview(btnProceed)
//        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.07)
//        tableView.bottomAnchor.constraint(equalTo:btnProceed.topAnchor).isActive = true
        
        
    }
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"add_favourite_contacts".easyLocalized().uppercased())
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        return navView
    }()
    lazy var btnAddFavourite :UIButton = {
        let button = UIButton()
       
        button.setAttributedTitle(EasyUtils.shared.spaced(text: "add_to_favorite".easyLocalized()), for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 11, style: .bold)
        button.setTitleColor(.slateGrey, for: .normal)
        return button
    }()
//    private let btnProceed:UIButton = {
//        let btnProceed = UIButton()
//        btnProceed.backgroundColor = .clearBlue
//        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
//        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
//        btnProceed.setTitleColor(.white, for: .normal)
//        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
//        return btnProceed
//    }()
   
//    @objc func btnProceedAction(_ sender:UIButton){
//        proceedCallBack?()
//    }
   
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        
        tableView.register(AddFavouriteCell.self, forCellReuseIdentifier: AddFavouriteCell.identifier)
        tableView.separatorStyle = .none
        
        tableView.tableFooterView = UIView()
        return tableView
    }()
}
