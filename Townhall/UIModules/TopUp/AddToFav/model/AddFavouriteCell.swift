//
//  AddFavouriteCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/11/21.
//

import Foundation
import UIKit

class AddFavouriteCell: UITableViewCell {
    var addNameCallBack : ((_ tag:Int,_ name: String)->Void)?
   
    
    static let identifier = "AddToFavCell"
    var rechargeData : SelectNumber?{
        didSet{
            logoView.image = nil
            logoView.image = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorShortName == rechargeData?.operatorObj?.operatorShortName})?.logoImage
            
           // amountLabel.amountText = EasyUtils.shared.getLocalizedDigits("\(rechargeData?.amount ?? 0)")
            numberLabel.text =  EasyUtils.shared.getLocalizedDigits(rechargeData?.number)
            typeLabel.text = rechargeData?.connetctionType?.uppercased()
            
         
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        contentView.addSubview(logoView)
        //logoView.addAnchorToSuperview(leading: 20, heightMultiplier: 0.4, centeredVertically: 0,heightWidthRatio: 1)
        logoView.addAnchorToSuperview(leading: 20,centeredVertically: 0)
        logoView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        logoView.widthAnchor.constraint(equalToConstant: .calculatedWidth(30)).isActive = true
        contentView.addSubview(nameNumberStack)
        nameNumberStack.addAnchorToSuperview( top: 20, bottom: -20)
        nameNumberStack.leadingAnchor.constraint(equalTo: logoView.trailingAnchor,constant: 10).isActive = true
        let buttonStack = UIStackView()
        buttonStack.axis = .horizontal
        buttonStack.spacing = 0
        //buttonStack.addArrangedSubview(amountLabel)
        //
        buttonStack.addArrangedSubview(btnAddContact)
        buttonStack.addArrangedSubview(nameEntryView)
        buttonStack.addArrangedSubview(addedView)
       
        
        contentView.addSubview(buttonStack)
        buttonStack.addAnchorToSuperview( trailing: -20,widthMutiplier: 0.3)
        buttonStack.topAnchor.constraint(equalTo: nameNumberStack.topAnchor, constant: 0).isActive = true
        buttonStack.bottomAnchor.constraint(equalTo: nameNumberStack.bottomAnchor, constant: 0).isActive = true
        nameNumberStack.trailingAnchor.constraint(equalTo: buttonStack.leadingAnchor, constant: -10).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
        logoView.image = UIImage(named: "gp")
        return logoView
    }()
    lazy var  nameNumberStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 2
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        nameNumberStack.addArrangedSubview(typeLabel)
        nameNumberStack.addArrangedSubview(numberLabel)
        return nameNumberStack
    }()
    let typeLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .bold)
        label.textColor = .slateGrey
        label.text = ""
        return label
    }()
    let numberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .bold)
        label.textColor = .black
        label.text = ""
        return label
    }()
    lazy var nameView:UIView = {
        let view = UIView()
        view.addSubview(txtName)
        view.addSubview(addBtn)
        txtName.addAnchorToSuperview(leading: 0, top: 0, bottom: 0, widthMutiplier: 0.8)
        addBtn.addAnchorToSuperview(trailing: 0, top: 0, bottom: 0, widthMutiplier: 0.1)
        
        //view.font = .EasyFont(ofSize: 16.0, style: .bold)
        //view.textColor = .black
        //view.text = ""
        return view
    }()
//    lazy var amountEntryView : UIView = {
//        let amountEntryView = UIView()
//        let view = UIView()
//        amountEntryView.isHidden = true
//        
//        amountEntryView.addSubview(view)
//        view.addAnchorToSuperview(leading: 2.5, trailing: -2.5, top: 3, bottom: -3)
//        view.layer.borderWidth = 1
//        view.layer.borderColor = UIColor.clearBlue.cgColor
//        
//        view.addSubview(txtName)
//        txtName.addAnchorToSuperview(leading: 10, heightMultiplier: 0.8, centeredVertically: 0)
//        view.addSubview(addBtn)
//        addBtn.addAnchorToSuperview(trailing: -5,  heightMultiplier: 0.8, centeredVertically: 0, heightWidthRatio: 1)
//        txtName.trailingAnchor.constraint(equalTo: addBtn.leadingAnchor, constant: -5).isActive = true
//        EasyManager.sharedInstance.delay(0.5) {
//            view.layer.cornerRadius = view.frame.height/2
//            
//        }
//       
//        return amountEntryView
//    }()
    
    lazy var addedView : UIView = {
        let view = UIView()
        view.addSubview(iconAdded)
        view.addSubview(lblAddedStatus)
        view.isHidden = true
        iconAdded.addAnchorToSuperview( top: 0, bottom: 0)
        iconAdded.widthAnchor.constraint(equalToConstant: 20).isActive = true
        lblAddedStatus.addAnchorToSuperview( trailing: 0, top: 0, bottom: 0)
        iconAdded.trailingAnchor.constraint(equalTo: lblAddedStatus.leadingAnchor, constant: -4).isActive = true
        
        return view
    }()
    
    lazy var lblAddedStatus:UILabel = {
        let label = UILabel()
        label.text = "Added"
        label.textColor = .trueGreen
        label.font =  .EasyFont(ofSize: 11, style: .bold)
        return label
    }()
    
    lazy var iconAdded:UIImageView = {
        let icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        icon.image = UIImage(named: "selectGreen")
        return icon
    }()
    
    lazy var nameEntryView : UIView = {
        let view = UIView()
        view.isHidden = true
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.clearBlue.cgColor
        view.addSubview(txtName)
        txtName.addAnchorToSuperview(leading: 4, top: 0,bottom: 0)
        view.addSubview(addBtn)
        addBtn.addAnchorToSuperview(trailing: -4, top: 0,bottom: 0)
        addBtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        txtName.trailingAnchor.constraint(equalTo: addBtn.leadingAnchor, constant: -4).isActive = true
        EasyManager.sharedInstance.delay(0.5) {
            view.layer.cornerRadius = view.frame.height/2
        }
        return view
        
    }()
    
    lazy var txtName:UITextField = {
        let tf = UITextField()
        tf.font = .EasyFont(ofSize: 12.0, style: .medium)
        tf.placeholder = "Set nick"
        tf.borderStyle = .none
        tf.inputAccessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        return tf
    }()
    lazy var addBtn:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "selecteGrey"), for: .normal)
        button.addTarget(self, action: #selector(selectBtnAction(_:)), for: .touchUpInside)
        return button
    }()
    @objc func selectBtnAction(_ sender:UIButton){
        
        addNameCallBack?(sender.tag, txtName.text ?? "")
        txtName.resignFirstResponder()
        
        
    }
    lazy var btnAddContact:UIButton = {
        let button = UIButton()
        button.setTitle("Add to favourite", for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 11, style: .regular)
        button.setTitleColor(.clearBlue, for: .normal)
        button.addTarget(self, action: #selector(addToFavBtnAction(_:)), for: .touchUpInside)
        return button
    }()
    @objc func addToFavBtnAction(_ sender:UIButton){
        nameEntryView.isHidden = false
        btnAddContact.isHidden = true
    }
    
    lazy var lblAdded:ViewWithTakaSign = {
        let label = ViewWithTakaSign(amount: "")
       
        return label
    }()

}
extension AddFavouriteCell : ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        
    }
    
    
}
