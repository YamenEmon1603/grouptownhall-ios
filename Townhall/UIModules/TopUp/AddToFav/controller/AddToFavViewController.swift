//
//  AddToFavViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/11/21.
//

import UIKit

class AddToFavViewController: EasyBaseViewController {
    var addTofavData : [SelectNumber]?{
        didSet{
            mView.tableView.reloadData()
        }
    }
    //var selectedCell : AddFavouriteCell?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        // Do any additional setup after loading the view.
        let favData  = EasyDataStore.sharedInstance.favoriteData
        let rechargeData  = EasyDataStore.sharedInstance.seletedNumbersArray
//        addTofavData = []
//        for item in rechargeData{
//            if favData?.contains(where: {$0.phone == item.number}) == false{
//                addTofavData?.append(item)
//            }
//        }
        addTofavData = rechargeData
       
        //print(addTofavData)
    }
    func AddNameSubmit(_ tag: Int,_ name:String){
        if let model = addTofavData?[tag]{
            
            addFavNumber(phone: model.number ?? "", name: name , oper: model.operatorObj?.operatorId ?? 0, connectionType: model.connetctionType ?? "", index: tag)
        }
    }
    
    lazy var mView : AddToFavView = {
        let mView = AddToFavView()
        mView.navView.backButtonCallBack = backButtonAction
        
        mView.tableView.delegate = self
        mView.tableView.dataSource = self
        mView.navView.backButtonCallBack = backButtonAction
      
        return mView
    }()
    
    func backButtonAction(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func isAddedAlready(data: SelectNumber?)->Bool
    {
        let favData  = EasyDataStore.sharedInstance.favoriteData
        if favData?.contains(where: {$0.phone == data?.number}) == false{
            return false
        }else{
            return true
        }
    }
    

}
extension AddToFavViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addTofavData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddFavouriteCell.identifier, for: indexPath) as! AddFavouriteCell
        cell.rechargeData = addTofavData?[indexPath.row]
        cell.addBtn.tag = indexPath.row
        cell.addNameCallBack = AddNameSubmit(_:_:)
        //selectedCell = cell
        if self.isAddedAlready(data: addTofavData?[indexPath.row]) == true || addTofavData?[indexPath.row].isfav == true {
            cell.btnAddContact.isHidden = true
            cell.nameEntryView.isHidden = true
            cell.addedView.isHidden = false
        }else{
            cell.btnAddContact.isHidden = false
            cell.addedView.isHidden = true
        }
        
        return cell
    }
}

extension AddToFavViewController{
    func addFavNumber(phone: String, name: String, oper: Int, connectionType: String,index:Int) {
        let model = UpdateFavRequestModel(name: name, phone: phone, connectionType: connectionType, operatorId: oper)
        
        WebServiceHandler.shared.postFavoriteNumber(params: model, completion: { (result) in
            switch result {
            case .success(let response):
                if response.code == 200 {
                    //self.presenter?.view?.shouldReload()
                    self.showToast(message: response.message ?? "")
                    self.addTofavData?[index].isfav = true
                    self.mView.tableView.reloadData()
//                    self.selectedCell?.nameEntryView.isHidden = true
//                    self.selectedCell?.btnAddContact.isHidden = true
//                    self.selectedCell?.addedView.isHidden = false
                    EasyDataStore.sharedInstance.seletedNumbersArray = []
                }else{
                    self.showToast(message: response.message ?? "")
                }
            case .failure(let error):
                self.showToast(message: error.localizedDescription)
            }
        }, shouldShowLoader: true)
    }
}
