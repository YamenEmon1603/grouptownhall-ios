//
//  RechargeSummerViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit
import SSLCommerzSDK

class RechargeSummerViewController: EasyBaseViewController, EasyPopUpDelegate {
    let bottomAlert = HorizontalButtonAlertView(header: "do_you_want_to_clear_all".easyLocalized(), subHeader: "Your_phone_number_and_amount".easyLocalized(), btn1Name: "yes".easyLocalized().uppercased(), btn2Name: "stay".easyLocalized().uppercased())
    var sslCommerz:SSLCommerz?
    var sdkData: SSLComerceData?
    
    func popupDismissedWith(button: PopupButton, consent: Bool?, rating: Int?) {
        if button.tag == 1001{
            print("Go Home",consent,rating)
            EasyDataStore.sharedInstance.seletedNumbersArray = []
            self.navigationController?.popToRootViewController(animated: true)
        }else if button.tag == 1002{
            print("tryAgainBtn",consent,rating)
            
        }else if button.tag == 1003{
            let vc = AddToFavViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    lazy var mView : RechargeSummerView = {
        let mView = RechargeSummerView()
        // mView.navView.backButtonCallBack = backButtonAction
        mView.proceedCallBack = proceedAction
        
        mView.tableView.delegate = self
        mView.tableView.dataSource = self
        mView.btnStack.addShadow(location: .top, color: .black, opacity: 0.3, radius: 5)
        mView.navView.backButtonCallBack = backButtonAction
        mView.clearAllCallBack = clearAllButtonAction
        mView.rechargeReqCallBack = rechargeRequestAction
        return mView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if EasyDataStore.sharedInstance.seletedNumbersArray.count > 1{
            mView.btnClearAll.isHidden = false
            if EasyDataStore.sharedInstance.seletedNumbersArray.count == 1{
                mView.btnRechargeRequest.isHidden = false
            }else{
                mView.btnRechargeRequest.isHidden = true
            }
        }else{
            mView.btnClearAll.isHidden = true
        }
        mView.navView.rightItem = mView.btnClearAll
    }
    func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func clearAllButtonAction(){
        
        showBottomAlert()
        
    }
    func rechargeRequestAction(){
        let rq = RechargeRequestViewController()
        rq.rechargeEntity = EasyDataStore.sharedInstance.seletedNumbersArray.first
        rq.navigationController?.pushViewController(rq, animated: true)
    }
    
    func showBottomAlert() {
        self.view.addSubview(bottomAlert)
        bottomAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        bottomAlert.btnSecondCallBack = btnSecondAction
        bottomAlert.btnFirstCallBack = btnFirstAction
    }
    func btnSecondAction(){
        bottomAlert.removeFromSuperview()
    }
    func btnFirstAction(){
        
        bottomAlert.removeFromSuperview()
        EasyDataStore.sharedInstance.seletedNumbersArray = []
        mView.tableView.reloadData()
        showToast(message: "Your_phone_number_and_amount_removed".easyLocalized())
        mView.btnClearAll.isHidden = true
    }
    func proceedAction() {
        
        requestRecharge()
    }
    func errorAlert(){
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "go_home".easyLocalized(), tag: 1001)
        let btn = PopupButton (type: .bordered, title: "try_again".easyLocalized(), tag: 1002)
        //        let btn1 = PopupButton (type: .bordered, title: "Another Button", tag: 1003)
        //
        //
        popup.options = [.image(image: .failed),.title(str: "oops_your_recharge_failed".easyLocalized(),color:.tomato),.subtitle(str: "please_try_again".easyLocalized(),color: .slateGrey),/*.starRatting(str: "Rate this Transaction", maxRating: 5, currentRating: 0),.consent(str: "An easy inline solution that wont crash if your array is too short", color: .battleshipGrey),*/.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
    }
    func successAlert(numbers:[SelectNumber],hold:Bool = false,holdText:String = ""){
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let done = PopupButton(type: .fill, title: "done".easyLocalized(), tag: 1001)
        let addToFav = PopupButton (type: .bordered, title: "add_to_favourite".easyLocalized(), tag: 1003)
        //        let btn1 = PopupButton (type: .bordered, title: "Another Button", tag: 1003)
        
        if hold == false{
            popup.options = [.image(image: .success),.title(str: "recharge_successful".easyLocalized(),color:.trueGreen),.subtitle(str: "following_phone_numbers".easyLocalized(),color: .slateGrey),.numbers(numbers: numbers),.starRatting(str: "Rate this Transaction", maxRating: 5, currentRating: 0),/*.consent(str: "An easy inline solution that wont crash if your array is too short", color: .battleshipGrey),*/.buttons(buttons: [done,addToFav])]
        }else{
            popup.options = [.image(image: .success),.title(str: "recharge_successful".easyLocalized(),color:.trueGreen),.subtitle(str: "following_phone_numbers".easyLocalized(),color: .slateGrey),.numbers(numbers: EasyDataStore.sharedInstance.seletedNumbersArray),.starRatting(str: "Rate this Transaction", maxRating: 5, currentRating: 0),.consent(str: holdText, color: .battleshipGrey),.buttons(buttons: [done,addToFav])]
        }
        
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func amountCrossBtnAction(_ sender: UIButton){
        
        EasyDataStore.sharedInstance.seletedNumbersArray.remove(at: sender.tag)
        mView.tableView.reloadData()
    }
}
extension RechargeSummerViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EasyDataStore.sharedInstance.seletedNumbersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data =  EasyDataStore.sharedInstance.seletedNumbersArray[indexPath.row]
        if data.scheduleTime == nil{
            let cell = tableView.dequeueReusableCell(withIdentifier: SummeryWithoutScheduleCell.identifier, for: indexPath) as! SummeryWithoutScheduleCell
            cell.deleteBtn.addTarget(self, action: #selector(amountCrossBtnAction(_:)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.row
            
            cell.rechargeData =  data
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: SummeryWithScheduleCell.identifier, for: indexPath) as! SummeryWithScheduleCell
            cell.deleteBtn.addTarget(self, action: #selector(amountCrossBtnAction(_:)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.row
            cell.rechargeData = data
            
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        let button = UIButton()
        button.setTitle("add_another_number".easyLocalized(), for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 14, style: .medium)
        button.setTitleColor(.clearBlue, for: .normal)
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 10
        button.layer.borderColor = UIColor.clearBlue.cgColor
        view.addSubview(button)
        button.addAnchorToSuperview( heightMultiplier: 0.7, widthMutiplier: 0.75, centeredVertically: 0, centeredHorizontally: 0)
        button.addTarget(self, action: #selector(addAnotherNumberAction(_:)), for: .touchUpInside)
        return view
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 75
    }
    
    @objc func addAnotherNumberAction(_ sender: UIButton){
        if EasyDataStore.sharedInstance.seletedNumbersArray.count >= 3 {
            showToast(message: "maximum_three_number_allowed".easyLocalized())
        }else{
            self.navigationController?.viewControllers.forEach({ (vc) in
                if vc is TopUpMobileEntryViewController{
                    self.navigationController?.popToViewController(vc, animated: false)
                    return
                }
            })
        }
    }
}
extension RechargeSummerViewController : SSLCommerzDelegate{
    func requestRecharge() {
        WebServiceHandler.shared.topUp(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.rechargeDone(with: response)
                }else{
                    self.showToast(message: response.message ?? "")
                }
                break
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
                break
            }
        }, shouldShowLoader: true)
    }
    //MARK:- Pre Load SDK API FOR SSL COMMERZ SDK LOAD
    func rechargeDone(with response: SSLComerceResponse) {
        if response.code == 200{
            if let ssldata = response.sslData, let storeid = ssldata.storeId , let storePass = ssldata.storePassword , let transId = ssldata.transactionId,  let ipnUrl = ssldata.ipnUrl{
                let integrationInfo =  IntegrationInformation(storeID: storeid, storePassword: storePass, totalAmount: ssldata.amount ?? 0.0, currency: "BDT", transactionId: transId, productCategory: ssldata.serviceTitle ?? "")
                integrationInfo.ipnURL = ipnUrl
                
                
                sslCommerz = SSLCommerz.init(integrationInformation: integrationInfo)
                
                sslCommerz?.delegate = self
                sslCommerz?.customerInformation = .init(customerName: EasyDataStore.sharedInstance.user?.name ?? "", customerEmail: EasyDataStore.sharedInstance.user?.name ?? "", customerAddressOne: EasyDataStore.sharedInstance.user?.address ?? "", customerCity: "dhk", customerPostCode: "1234", customerCountry: "BD", customerPhone: EasyDataStore.sharedInstance.user?.mobile ?? "")
                
                sslCommerz?.additionalInformation = .init()
                sslCommerz?.additionalInformation?.paramB = ssldata.valueB
                sslCommerz?.additionalInformation?.paramC = ssldata.valueC
                
                loadSDK(with: ssldata)
            }
            else{
                showToast(message: "Something went wrong,please try again later")
            }
            
            //view?.shouldShowSuccess()
            
        }else{
            showToast(message: response.message ?? "Error Occured")
        }
    }
    func rechargeStatus(transaction:String){
        WebServiceHandler.shared.transectionStatus(transactionId: transaction, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    if let orderData = response.data?.orderData, let data = orderData.replacingOccurrences(of: "\\", with: "").data(using: .utf8){
                        do {
                            let jsonArray = try JSONDecoder().decode( [PaymentStatusResponseElement].self, from: data)
                            if  let orderData = jsonArray.filter({$0.status?.lowercased() == "hold"}).first{
                                
                                ///
                                
                                var total: String  = ""
                                if let Intamount = Int(orderData.amount ?? "0"), let IntbonusAmount = Int(orderData.bonusAmount ?? "0"){
                                    total = String(Intamount+IntbonusAmount)
                                }
                                
                                let amount : String = "৳\(EasyUtils.shared.getLocalizedDigits(orderData.amount) ?? "")"
                                let bonusAmount : String = "৳\(EasyUtils.shared.getLocalizedDigits(orderData.bonusAmount) ?? "")"
                                
                                let conditionText = String(format: "by_checking_this_box_for_recharge".easyLocalized(),bonusAmount ,amount , total, EasyUtils.shared.getLocalizedDigits( orderData.phone) ?? "")
                                
                                
                                ///
                                self.successAlert(numbers: EasyDataStore.sharedInstance.seletedNumbersArray,hold: true,holdText: conditionText)
                                
                                
                                
                                
                                //-->>       //self.presenter?.shouldShowSuccessWithCondition(transactionId : transaction, orderData: orderData)
                            }else{
                                //-->>// self.presenter?.shouldShowSuccess(transactionId: transaction)
                                //successAlert(numbers: [()])
                                self.successAlert(numbers: EasyDataStore.sharedInstance.seletedNumbersArray)
                            }
                            
                            
                        } catch  {
                            debugPrint(error)
                            //-->>//self.presenter?.shouldShowSuccess(transactionId: transaction)
                            self.successAlert(numbers: EasyDataStore.sharedInstance.seletedNumbersArray)
                        }
                    }
                }else{
                    //-->>//self.presenter?.shouldShowSuccess(transactionId: transaction)
                    self.errorAlert()
                }
                
                break
            case .failure(let error):
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
    }
    //MARK:- LOAD SSLCOMMERZ SDK
    func loadSDK(with: SSLComerceData) {
        sdkData = with
        sslCommerz?.start(in: self, shouldRunInTestMode: Constant.isDev)
    }
    
    //MARK:- SSLCOMMERZ DELEGATE METHOD
    
    //MARK:-  SSLCOMMERZ SDK CLOSED CALL BACK
    func sdkClosed(reason: String) {
        self.showToast(message: reason)
    }
    //MARK:-  SSLCOMMERZ SDK TRANSECTION VALIDATION CALL BACK (After validation confirmation API)
    
    func transactionCompleted(withTransactionData transactionData: SSLCommerzSDK.TransactionDetails?) {
        if(transactionData?.status?.lowercased() == "valid" || transactionData?.status?.lowercased() == "validated" ){
            debugPrint(transactionData?.tran_id ?? "")
            
            rechargeStatus(transaction: transactionData?.tran_id ?? "")
        }else{
            errorAlert()
            //-->> view?.shouldShowError()
        }
    }
    
}
