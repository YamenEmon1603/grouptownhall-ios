//
//  SummeryWithScheduleCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit

class SummeryWithScheduleCell: UITableViewCell {

    static let identifier = "SummeryWithScheduleCell"
    
    var rechargeData : SelectNumber?{
        didSet{
            logoView.image = nil
            logoView.image = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorShortName == rechargeData?.operatorObj?.operatorShortName})?.logoImage
            
            amountLabel.amountText = EasyUtils.shared.getLocalizedDigits("\(rechargeData?.amount ?? 0)")
            numberLabel.text =  EasyUtils.shared.getLocalizedDigits(rechargeData?.number)
         
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = Constant.DateFormat17
            scheduleTimeLabel.text = dateFormatter.string(from: rechargeData?.scheduleTime ?? Date())
            typeLabel.text = rechargeData?.connetctionType
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        contentView.addSubview(logoView)
        logoView.addAnchorToSuperview(leading: 20)
        logoView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        logoView.widthAnchor.constraint(equalToConstant: .calculatedWidth(30)).isActive = true
        
        //logoView.addAnchorToSuperview(leading: 20, heightMultiplier: 0.25,heightWidthRatio: 1)
        contentView.addSubview(nameNumberStack)
        nameNumberStack.addAnchorToSuperview( top: 20)
        nameNumberStack.leadingAnchor.constraint(equalTo: logoView.trailingAnchor,constant: 10).isActive = true
        logoView.centerYAnchor.constraint(equalTo: nameNumberStack.centerYAnchor, constant: 0).isActive = true
        let buttonStack = UIStackView()
        buttonStack.axis = .horizontal
        buttonStack.distribution = .fillEqually
        buttonStack.spacing = 5
        buttonStack.addArrangedSubview(amountLabel)
        buttonStack.addArrangedSubview(deleteBtn)
        
        contentView.addSubview(buttonStack)
        buttonStack.addAnchorToSuperview( trailing: -20,widthMutiplier: 0.2)
        buttonStack.topAnchor.constraint(equalTo: nameNumberStack.topAnchor, constant: 0).isActive = true
        buttonStack.bottomAnchor.constraint(equalTo: nameNumberStack.bottomAnchor, constant: 0).isActive = true
        nameNumberStack.trailingAnchor.constraint(equalTo: buttonStack.leadingAnchor, constant: -10).isActive = true
        
        contentView.addSubview(scheduleView)
        scheduleView.addAnchorToSuperview(bottom: -20)
        scheduleView.leadingAnchor.constraint(equalTo: nameNumberStack.leadingAnchor).isActive = true
        scheduleView.trailingAnchor.constraint(equalTo: nameNumberStack.trailingAnchor).isActive = true
        scheduleView.topAnchor.constraint(equalTo: nameNumberStack.bottomAnchor,constant: 10).isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
        logoView.image = UIImage(named: "gp")
        return logoView
    }()
    lazy var  nameNumberStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 2
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        nameNumberStack.addArrangedSubview(typeLabel)
        nameNumberStack.addArrangedSubview(numberLabel)
        return nameNumberStack
    }()
    let typeLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .bold)
        label.textColor = .slateGrey
        label.text = ""
        return label
    }()
    let numberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .bold)
        label.textColor = .black
        label.text = ""
        return label
    }()
   
    let deleteBtn:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "round_cross"), for: .normal)
       // button.addTarget(self, action: #selector(amountCrossBtnAction(_:)), for: .touchUpInside)
        return button
    }()
    let amountLabel:ViewWithTakaSign = {
        let label = ViewWithTakaSign(amount: "1502")
       
        return label
    }()
    lazy var scheduleView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 6
        view.layer.borderColor = UIColor.paleLilac.cgColor
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let clockIV = UIImageView(image: UIImage(named: "scheduletimerline"))
        view.addSubview(clockIV)
        clockIV.addAnchorToSuperview(leading: 10,heightMultiplier: 0.5,centeredVertically: 0, heightWidthRatio: 1)
        
        let labelScheduleOn  = UILabel()
        labelScheduleOn.font = .EasyFont(ofSize: 10.0, style: .medium)
        labelScheduleOn.textColor = .black
        labelScheduleOn.text = "Scheduled on"
        
        view.addSubview(labelScheduleOn)
        labelScheduleOn.addAnchorToSuperview( trailing: -5, top: 5)
        labelScheduleOn.leadingAnchor.constraint(equalTo:clockIV.trailingAnchor , constant: 10).isActive = true
        view.addSubview(scheduleTimeLabel)
        scheduleTimeLabel.addAnchorToSuperview( trailing: -5, bottom: -5)
        scheduleTimeLabel.leadingAnchor.constraint(equalTo:clockIV.trailingAnchor , constant: 10).isActive = true
        scheduleTimeLabel.topAnchor.constraint(equalTo:labelScheduleOn.bottomAnchor , constant: 3).isActive = true
        return view
    }()
    let scheduleTimeLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 12.0, style: .bold)
        label.textColor = .clearBlue
        label.text = "Thu,Apr 13 | 3:30PM"
        return label
    }()
}
