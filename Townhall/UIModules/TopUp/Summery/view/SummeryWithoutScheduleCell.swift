//
//  SummeryWithoutScheduleCell.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit

class SummeryWithoutScheduleCell: UITableViewCell {

    static let identifier = "SummeryWithoutScheduleCell"
    var rechargeData : SelectNumber?{
        didSet{
            logoView.image = nil
            logoView.image = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorShortName == rechargeData?.operatorObj?.operatorShortName})?.logoImage
            
            amountLabel.amountText = EasyUtils.shared.getLocalizedDigits("\(rechargeData?.amount ?? 0)")
            numberLabel.text =  EasyUtils.shared.getLocalizedDigits(rechargeData?.number)
            typeLabel.text = rechargeData?.connetctionType?.uppercased()
         
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        contentView.addSubview(logoView)
        //logoView.addAnchorToSuperview(leading: 20, heightMultiplier: 0.4, centeredVertically: 0,heightWidthRatio: 1)
        logoView.addAnchorToSuperview(leading: 20,centeredVertically: 0)
        logoView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        logoView.widthAnchor.constraint(equalToConstant: .calculatedWidth(30)).isActive = true
        contentView.addSubview(nameNumberStack)
        nameNumberStack.addAnchorToSuperview( top: 20, bottom: -20)
        nameNumberStack.leadingAnchor.constraint(equalTo: logoView.trailingAnchor,constant: 10).isActive = true
        let buttonStack = UIStackView()
        buttonStack.axis = .horizontal
        buttonStack.distribution = .fillEqually
        buttonStack.spacing = 5
        buttonStack.addArrangedSubview(amountLabel)
        buttonStack.addArrangedSubview(deleteBtn)
        
        contentView.addSubview(buttonStack)
        buttonStack.addAnchorToSuperview( trailing: -20,widthMutiplier: 0.2)
        buttonStack.topAnchor.constraint(equalTo: nameNumberStack.topAnchor, constant: 0).isActive = true
        buttonStack.bottomAnchor.constraint(equalTo: nameNumberStack.bottomAnchor, constant: 0).isActive = true
        nameNumberStack.trailingAnchor.constraint(equalTo: buttonStack.leadingAnchor, constant: -10).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
        logoView.image = UIImage(named: "gp")
        return logoView
    }()
    lazy var  nameNumberStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 2
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        nameNumberStack.addArrangedSubview(typeLabel)
        nameNumberStack.addArrangedSubview(numberLabel)
        return nameNumberStack
    }()
    let typeLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 10.0, style: .bold)
        label.textColor = .slateGrey
        label.text = ""
        return label
    }()
    let numberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 16.0, style: .bold)
        label.textColor = .black
        label.text = ""
        return label
    }()
   
    let deleteBtn:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "round_cross"), for: .normal)
       // button.addTarget(self, action: #selector(amountCrossBtnAction(_:)), for: .touchUpInside)
        return button
    }()
    let amountLabel:ViewWithTakaSign = {
        let label = ViewWithTakaSign(amount: "")
       
        return label
    }()

}
