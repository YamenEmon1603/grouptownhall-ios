//
//  RechargeSummerView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/5/21.
//

import UIKit

class RechargeSummerView: EasyBaseView {
    var proceedCallBack : (()->Void)?
    var rechargeReqCallBack : (()->Void)?
    var clearAllCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0)
        tableView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
//        addSubview(btnProceed)
//        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.07)
//        tableView.bottomAnchor.constraint(equalTo:btnProceed.topAnchor).isActive = true
        addSubview(btnStack)
        btnStack.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        tableView.bottomAnchor.constraint(equalTo:btnStack.topAnchor,constant: 4).isActive = true
        btnStack.addArrangedSubview(btnRechargeRequest)
        btnRechargeRequest.heightAnchor.constraint(equalToConstant: .calculatedHeight(50)).isActive = true
        btnStack.addArrangedSubview(btnProceed)
        btnProceed.heightAnchor.constraint(equalToConstant: .calculatedHeight(50)).isActive = true
        
    }
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var btnStack : UIStackView = {
        let btnStack  = UIStackView()
        btnStack.axis = .vertical
        btnStack.distribution = .fillEqually
    
        
        
        return btnStack
    }()
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"recharge_summery".easyLocalized().uppercased(),rightItem: btnClearAll)
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        return navView
    }()
    lazy var btnClearAll :UIButton = {
        let button = UIButton()
       
        button.setAttributedTitle(EasyUtils.shared.spaced(text: "clear_all".easyLocalized()), for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 11, style: .bold)
        button.setTitleColor(.slateGrey, for: .normal)
        button.addTarget(self, action: #selector(btnClearAllAction(_:)), for: .touchUpInside)
       
        return button
    }()
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
     let btnRechargeRequest:UIButton = {
        let btnRechargeRequest = UIButton()
        btnRechargeRequest.backgroundColor = .white
       
        btnRechargeRequest.titleLabel?.font = .EasyFont(ofSize: 13, style: .regular)
        btnRechargeRequest.setTitleColor(.clearBlue, for: .normal)
        btnRechargeRequest.setTitle("req_recharge".easyLocalized().uppercased(), for: .normal)
        btnRechargeRequest.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnRechargeRequest
    }()
   
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    @objc func btnRechargeReqAction(_ sender:UIButton){
        rechargeReqCallBack?()
    }
    @objc func btnClearAllAction(_ sender:UIButton){
        clearAllCallBack?()
    }
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        
        tableView.register(SummeryWithoutScheduleCell.self, forCellReuseIdentifier: SummeryWithoutScheduleCell.identifier)
        tableView.register(SummeryWithScheduleCell.self, forCellReuseIdentifier: SummeryWithScheduleCell.identifier)
        
        tableView.tableFooterView = UIView()
        return tableView
    }()
    lazy var btnRechargeReqWithoutLogin: UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .paleGrey2
        button.layer.borderWidth = 1.5
        button.layer.borderColor =  UIColor.clearBlue.cgColor
        button.titleLabel?.font = UIFont.EasyFont(ofSize: 13, style: .bold)
        button.setTitleColor(.clearBlue, for: .normal)
        button.setTitle("recharge_request_without_login".easyLocalized(), for: .normal)
        button.layer.cornerRadius = 10
        return button
    }()
}
