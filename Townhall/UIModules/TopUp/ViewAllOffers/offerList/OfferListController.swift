//
//  OfferView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 4/8/21.
//

import Foundation

import UIKit

class OfferListController: EasyBaseViewController {
    var type : OfferType?
    var selectedNumber : SelectNumber?
    var delegate : OfferSelectedFromViewAllList?
    var plans: [RechargePlan]?{
        didSet{
            if plans?.count == 0{
                tableView.isHidden = true
            }
           tableView.reloadData()
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.view.addSubview(tableView)
        tableView.addAnchorToSuperview(leading: 0, trailing: 0, top : 10, bottom: 0)
        if let offer  = type {
            if let selector  = selectedNumber{
                prepareMostPopular(type: offer,selector: selector)
            }else{
                prepareMostPopular(type: offer)
            }
           
        }
    }
    func prepareMostPopular(type:OfferType){
        
        EasyManager.sharedInstance.prepareMostPopular(offerType: type) { nextPlan, fetchedPlans in
            plans = fetchedPlans
        }
    }
    func prepareMostPopular(type:OfferType, selector : SelectNumber){
        
        EasyManager.sharedInstance.prepareMostPopular(op: selector.operatorObj, conectionType: selector.connetctionType ?? "", amount: selector.amount ?? 0,  isPolular: true, offerType: type) { nextPlan, fetchedPlans in
            plans = fetchedPlans
        }
    }
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(OfferListTableViewCell.self, forCellReuseIdentifier: OfferListTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        return tableView
    }()
  
 
 

}
extension OfferListController:UITableViewDelegate,UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plans?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferListTableViewCell.identifier, for: indexPath) as!  OfferListTableViewCell
        cell.easyRechargePlans = plans?[indexPath.row]
        return cell
    }
   
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            if let offerPlan  = self.plans?[indexPath.row]{
                self.delegate?.offerSelected(offer:offerPlan)
            }
        }
        
        
    }
    
    
}
