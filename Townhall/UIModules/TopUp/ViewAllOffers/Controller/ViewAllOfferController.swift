//
//  ViewAllOfferController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 4/8/21.
//

import Foundation
import UIKit

class ViewAllOfferController: EasyBaseViewController {
    let itemStrings = ["INTERNET","VOICE","COMBO"]
    var selectedNumber : SelectNumber?
    var offerDelegate : OfferSelectedFromViewAllList?
    lazy var mView : ViewAllOffersView = {
        let mView = ViewAllOffersView()
        mView.navView.backButtonCallBack = backButtonAction
        
        return mView
    }()
    lazy var pagingItems: [(pagingItem : PagingItemView ,vc:UIViewController)] = {
        var items : [(pagingItem : PagingItemView ,vc:UIViewController)] = []
        for (index,str) in itemStrings.enumerated(){
            let item = PagingItemView(title: str)
            item.activeButton.tag = 1200+index
            item.activeButton.addTarget(self, action: #selector(switchTabAction(_:)), for: .touchUpInside)
            let vc  = OfferListController()
            if index == 0{
                vc.type = .internet
            }else if index == 1{
                vc.type = .voice
            }else{
                vc.type = .internetVoice
            }
            vc.selectedNumber = selectedNumber
            vc.delegate = offerDelegate
            items.append((pagingItem : item ,vc:vc))
        }
        return items
    }()
    var thePageVC: EasyPageViewController!
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        if let number  = selectedNumber{
            mView.navView.subTitle = "for \(number.operatorObj?.operatorName ?? "All")"
        }
        thePageVC = EasyPageViewController()
        thePageVC.pageDelegate = self
        pagingItems.forEach { item in
            mView.pagingStack.addArrangedSubview(item.pagingItem)
            thePageVC.pages.append(item.vc)
        }
        pagingItems[0].pagingItem.isActive = true
        //        thePageVC.pages = [UIViewController(),UIViewController()]
        addChild(thePageVC)
        
        // we need to re-size the page view controller's view to fit our container view
        thePageVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        // add the page VC's view to our container view
        mView.pageContainerView.addSubview(thePageVC.view)
        
        thePageVC.view.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        thePageVC.didMove(toParent: self)
    }
    
    
    @objc func switchTabAction(_ sender: UIButton){
        thePageVC.setPage(index: sender.tag - 1200)
    }
    func backButtonAction(){
        self.dismiss(animated: true, completion: nil)
    }
}
extension ViewAllOfferController : EasyPageDelegate
{
    func pageDidChanged(to: Int) {
        for (index,_) in pagingItems.enumerated(){
            pagingItems[index].pagingItem.isActive = to == index
        }
        
    }
    
    
}

