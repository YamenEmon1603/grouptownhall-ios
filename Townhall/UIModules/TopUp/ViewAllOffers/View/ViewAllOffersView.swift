//
//  ViewAllOffersView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 4/8/21.
//

import Foundation
import UIKit

class ViewAllOffersView: EasyBaseView {
   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .iceBlue
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: -.calculatedHeight(0))
        
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.075)
        containerView.addSubview(pagingStack)
        pagingStack.addAnchorToSuperview(leading: 1, trailing: -1, heightMultiplier: 0.06)
        pagingStack.topAnchor.constraint(equalTo: navView.bottomAnchor, constant: 0).isActive = true
     
        containerView.addSubview(pageContainerView)
        pageContainerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        pageContainerView.topAnchor.constraint(equalTo: pagingStack.bottomAnchor, constant: 0).isActive = true
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "EASY OFFERS", withBackButton: true)
        navView.backgroundColor = .white
        return navView
    }()
    
    lazy var pageContainerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .gray
        return v
    }()
    lazy var pagingStack : UIStackView = {
        let pagingStack = UIStackView()
        pagingStack.backgroundColor = .white
        pagingStack.translatesAutoresizingMaskIntoConstraints = false
        pagingStack.axis = .horizontal
        pagingStack.spacing = 0
        pagingStack.distribution = .fillEqually
        return pagingStack
    }()
    
    
}
