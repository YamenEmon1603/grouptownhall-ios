//
//  QuickieViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/1/21.
//

import UIKit
import SSLCommerzSDK

class QuickieViewController: EasyBaseViewController,EasyPopUpDelegate {
    var keyboardObserver : KeyboardObserver!
    var sslCommerz:SSLCommerz?
    var sdkData: SSLComerceData?
    
    lazy var mView : QuickieView = {
        let mView = QuickieView()
        mView.navView.backButtonCallBack = backButtonAction
        mView.proceedCallBack = proceedAction
        return mView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mView
        keyboardObserver = KeyboardObserver(for: self)
       
    }
    func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardObserver.add()
    }
    func popupDismissedWith(button: PopupButton, consent: Bool?, rating: Int?) {
        if button.tag == 1001{
            print("Go Home",consent,rating)
            EasyDataStore.sharedInstance.seletedNumbersArray = []
            self.navigationController?.popToRootViewController(animated: true)
        }else if button.tag == 1002{
            print("tryAgainBtn",consent,rating)
            
        }else if button.tag == 1003{
            let vc = AddToFavViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func validation(selectedNumber : SelectNumber)->Bool{
        if  selectedNumber.amount != nil {
            let connectionType = selectedNumber.connetctionType
            if connectionType?.lowercased() == "postpaid"{
                if let amountInt = selectedNumber.amount{
                    if amountInt < 2501{
                        //let vc = RechargeSummerViewController()
                        
                            let  filter =  EasyDataStore.sharedInstance.seletedNumbersArray.filter({$0.number == selectedNumber.number})
                            if filter.count != 0{
                                filter.first?.amount = selectedNumber.amount
                                filter.first?.operatorObj = selectedNumber.operatorObj
                                filter.first?.connetctionType = selectedNumber.connetctionType
                            }else{
                                selectedNumber.amount = amountInt
                                EasyDataStore.sharedInstance.seletedNumbersArray.append(selectedNumber)
                            }
                            
                        
                        return true
                    }else{
                        self.showToast(message: "ten_to_two_thousand_five_for_postpaid".easyLocalized())
                        return false
                    }
                }else{
                    showToast(message: "enter_amount_first".easyLocalized())
                    return false
                }
            }else if connectionType?.lowercased() == "prepaid" || connectionType?.lowercased() == "skitto"{
                if let amountInt = selectedNumber.amount{
                    if amountInt < 1001{
                        //let vc = RechargeSummerViewController()
                        
                            
                            
                            if let index = EasyDataStore.sharedInstance.seletedNumbersArray.firstIndex(where:{ $0.number == selectedNumber.number}){
                                
                                EasyDataStore.sharedInstance.seletedNumbersArray[index].amount = selectedNumber.amount
                                EasyDataStore.sharedInstance.seletedNumbersArray[index].operatorObj = selectedNumber.operatorObj
                                EasyDataStore.sharedInstance.seletedNumbersArray[index].connetctionType = selectedNumber.connetctionType
                                
                                
                            }else{
                                selectedNumber.amount = amountInt
                                EasyDataStore.sharedInstance.seletedNumbersArray.append(selectedNumber)
                            }
                            
                        
                        return true
                    }else{
                        self.showToast(message: "ten_to_thousand_for_prepaid".easyLocalized())
                        return false
                    }
                }else{
                    showToast(message: "enter_amount_first".easyLocalized())
                    return false
                }
            }else{
                return false
            }
            
        }else{
            showToast(message: "enter_amount_first".easyLocalized())
            return false
        }
        
    }
    func proceedAction(){
        if let views = mView.selectedItems{
            EasyDataStore.sharedInstance.seletedNumbersArray = []
            views.forEach { v in
                print(v.number ?? "",v.amount ?? "",v.selectNumber?.connetctionType ?? "",v.selectNumber?.operatorObj?.operatorName ?? "")
                let amountInt = Int(v.amount ?? "0")
              
//                if let model =  v.selectNumber, validation(selectedNumber: model, amountInt: amountInt ?? 0) == true{
//                    model.amount = amountInt
//                    EasyDataStore.sharedInstance.seletedNumbersArray.append(model)
//
//                }
                if let model =  v.selectNumber{
                    model.amount = amountInt
                   let _ =  validation(selectedNumber: model)
                }
                
               
            }
            if  EasyDataStore.sharedInstance.seletedNumbersArray.count > 0{
                requestRecharge()
            }
        }
    
    }
    func errorAlert(){
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let go = PopupButton(type: .fill, title: "go_home".easyLocalized(), tag: 1001)
        let btn = PopupButton (type: .bordered, title: "try_again".easyLocalized(), tag: 1002)
//        let btn1 = PopupButton (type: .bordered, title: "Another Button", tag: 1003)
//
//
        popup.options = [.image(image: .failed),.title(str: "oops_your_recharge_failed".easyLocalized(),color:.tomato),.subtitle(str: "please_try_again".easyLocalized(),color: .slateGrey),/*.starRatting(str: "Rate this Transaction", maxRating: 5, currentRating: 0),.consent(str: "An easy inline solution that wont crash if your array is too short", color: .battleshipGrey),*/.buttons(buttons: [go,btn])]
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
    }
    func successAlert(numbers:[SelectNumber],hold:Bool = false,holdText:String = ""){
        let popup = EasyPopUpViewController()
        popup.delegate = self
        let done = PopupButton(type: .fill, title: "done".easyLocalized(), tag: 1001)
        let addToFav = PopupButton (type: .bordered, title: "add_to_favourite".easyLocalized(), tag: 1003)
//        let btn1 = PopupButton (type: .bordered, title: "Another Button", tag: 1003)

        if hold == false{
            popup.options = [.image(image: .success),.title(str: "recharge_successful".easyLocalized(),color:.trueGreen),.subtitle(str: "following_phone_numbers".easyLocalized(),color: .slateGrey),.numbers(numbers: numbers),.starRatting(str: "Rate this Transaction", maxRating: 5, currentRating: 0),/*.consent(str: "An easy inline solution that wont crash if your array is too short", color: .battleshipGrey),*/.buttons(buttons: [done])]
        }else{
            popup.options = [.image(image: .success),.title(str: "recharge_successful".easyLocalized(),color:.trueGreen),.subtitle(str: "following_phone_numbers".easyLocalized(),color: .slateGrey),.numbers(numbers: EasyDataStore.sharedInstance.seletedNumbersArray),.starRatting(str: "Rate this Transaction", maxRating: 5, currentRating: 0),.consent(str: holdText, color: .battleshipGrey),.buttons(buttons: [done,addToFav])]
        }
       
        popup.modalPresentationStyle = .overCurrentContext
        self.present(popup, animated: true, completion: nil)
    }

}
extension QuickieViewController: KeyboardObserverProtocol{
    func keyboardWillShow(with height: CGFloat) {
        var contentInset:UIEdgeInsets = mView.scrollView.contentInset
        contentInset.bottom = height 
        mView.scrollView.contentInset = contentInset
    }
    
    func keybaordWillHide() {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        mView.scrollView.contentInset = contentInset
    }
}
extension QuickieViewController : SSLCommerzDelegate{
    func requestRecharge() {
        WebServiceHandler.shared.topUp(completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    self.rechargeDone(with: response)
                }else{
                    self.showToast(message: response.message ?? "")
                }
                break
            case .failure(let error):
                self.showToastError(message: error.localizedDescription)
                break
            }
        }, shouldShowLoader: true)
    }
    //MARK:- Pre Load SDK API FOR SSL COMMERZ SDK LOAD
    func rechargeDone(with response: SSLComerceResponse) {
         if response.code == 200{
             if let ssldata = response.sslData, let storeid = ssldata.storeId , let storePass = ssldata.storePassword , let transId = ssldata.transactionId,  let ipnUrl = ssldata.ipnUrl{
                 let integrationInfo =  IntegrationInformation(storeID: storeid, storePassword: storePass, totalAmount: ssldata.amount ?? 0.0, currency: "BDT", transactionId: transId, productCategory: ssldata.serviceTitle ?? "")
                 integrationInfo.ipnURL = ipnUrl
                 
                 
                 sslCommerz = SSLCommerz.init(integrationInformation: integrationInfo)
                 
                 sslCommerz?.delegate = self
                sslCommerz?.customerInformation = .init(customerName: EasyDataStore.sharedInstance.user?.name ?? "", customerEmail: EasyDataStore.sharedInstance.user?.name ?? "", customerAddressOne: EasyDataStore.sharedInstance.user?.address ?? "", customerCity: "dhk", customerPostCode: "1234", customerCountry: "BD", customerPhone: EasyDataStore.sharedInstance.user?.mobile ?? "")
                 
                 sslCommerz?.additionalInformation = .init()
                sslCommerz?.additionalInformation?.paramB = ssldata.valueB
                sslCommerz?.additionalInformation?.paramC = ssldata.valueC
             
                 loadSDK(with: ssldata)
             }
             else{
                showToast(message: "Something went wrong,please try again later")
             }
             
             //view?.shouldShowSuccess()
             
         }else{
            showToast(message: response.message ?? "Error Occured")
         }
    }
    func rechargeStatus(transaction:String){
        WebServiceHandler.shared.transectionStatus(transactionId: transaction, completion: { (result) in
            switch result{
            case .success(let response):
                if response.code == 200{
                    if let orderData = response.data?.orderData, let data = orderData.replacingOccurrences(of: "\\", with: "").data(using: .utf8){
                        do {
                            let jsonArray = try JSONDecoder().decode( [PaymentStatusResponseElement].self, from: data)
                            if  let orderData = jsonArray.filter({$0.status?.lowercased() == "hold"}).first{
                                
                                ///
                                
                                    var total: String  = ""
                                    if let Intamount = Int(orderData.amount ?? "0"), let IntbonusAmount = Int(orderData.bonusAmount ?? "0"){
                                        total = String(Intamount+IntbonusAmount)
                                    }
                                    
                                    let amount : String = "৳\(EasyUtils.shared.getLocalizedDigits(orderData.amount) ?? "")"
                                    let bonusAmount : String = "৳\(EasyUtils.shared.getLocalizedDigits(orderData.bonusAmount) ?? "")"
                                    
                                let conditionText = String(format: "by_checking_this_box_for_recharge".easyLocalized(),bonusAmount ,amount , total, EasyUtils.shared.getLocalizedDigits( orderData.phone) ?? "")
                                
                                
                                ///
                                self.successAlert(numbers: EasyDataStore.sharedInstance.seletedNumbersArray,hold: true,holdText: conditionText)
                                
                                
                                
                                
                  //-->>       //self.presenter?.shouldShowSuccessWithCondition(transactionId : transaction, orderData: orderData)
                            }else{
                    //-->>// self.presenter?.shouldShowSuccess(transactionId: transaction)
                                //successAlert(numbers: [()])
                                self.successAlert(numbers: EasyDataStore.sharedInstance.seletedNumbersArray)
                            }
                            
                            
                        } catch  {
                            debugPrint(error)
                    //-->>//self.presenter?.shouldShowSuccess(transactionId: transaction)
                            self.successAlert(numbers: EasyDataStore.sharedInstance.seletedNumbersArray)
                        }
                    }
                }else{
                //-->>//self.presenter?.shouldShowSuccess(transactionId: transaction)
                    self.errorAlert()
                }

                break
            case .failure(let error):
                debugPrint(error)
                break
            }
        }, shouldShowLoader: true)
    }
    //MARK:- LOAD SSLCOMMERZ SDK
    func loadSDK(with: SSLComerceData) {
        sdkData = with
        sslCommerz?.start(in: self, shouldRunInTestMode: Constant.isDev)
    }
    
    //MARK:- SSLCOMMERZ DELEGATE METHOD
    
    //MARK:-  SSLCOMMERZ SDK CLOSED CALL BACK
    func sdkClosed(reason: String) {
        self.showToast(message: reason)
    }
    //MARK:-  SSLCOMMERZ SDK TRANSECTION VALIDATION CALL BACK (After validation confirmation API)
    
    func transactionCompleted(withTransactionData transactionData: SSLCommerzSDK.TransactionDetails?) {
        if(transactionData?.status?.lowercased() == "valid" || transactionData?.status?.lowercased() == "validated" ){
            debugPrint(transactionData?.tran_id ?? "")
            
            rechargeStatus(transaction: transactionData?.tran_id ?? "")
        }else{
           errorAlert()
            //-->> view?.shouldShowError()
        }
    }
    
}
