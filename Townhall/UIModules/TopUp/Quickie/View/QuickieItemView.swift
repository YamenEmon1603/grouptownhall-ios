//
//  QuickieItemView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/3/21.
//

import UIKit
class QuickieItemView: UIView , ButtonAccessoryViewDelegate{
    private enum Buttons: CaseIterable{
        case fifty,Hundred,TwoHundred,Plus
        var tag:Int{
            switch self {
            case .fifty:
                return 50
            case .Hundred:
                return 100
            case .TwoHundred:
                return 200
            case .Plus:
                return 1301
            }
        }
        var title:String{
            switch self {
            case .fifty,.Hundred,.TwoHundred:
                return "\(self.tag)"
            case .Plus:
                return "+"
            }
        }
    }
    var proceedCallBack : (()->Void)?
    var isSelectionAllowed:((_ view : QuickieItemView)->Bool)?
    private var quickAmount :Int = 0
    private(set) var number :String?
    private(set) var selectNumber : SelectNumber?
    
    var amount : String?{
        get{
            if quickAmount == 0{
                return amountTextfield.text?.isEmpty == true ? nil : amountTextfield.text
            }else{
                return String(quickAmount)
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    convenience init(name:String,number:String, selectNum : SelectNumber) {
        self.init(frame:.zero)
        selectNumber = selectNum
        backgroundColor = .white
        translatesAutoresizingMaskIntoConstraints = false
        let separatorView = UIView()
        separatorView.backgroundColor = .paleGreyTwo
        addSubview(separatorView)
        separatorView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        addSubview(contenView)
      
        contenView.addAnchorToSuperview(leading: 10, trailing: -10, top:10)
        contenView.bottomAnchor.constraint(equalTo: separatorView.topAnchor,constant: -9).isActive = true
        nameLabel.text = name
        numberLabel.text = number
        self.number = number.isEmpty ? name : number
        self.selectNumber = selectNum
    }
    lazy var contenView : UIView = {
        let contenView = UIView()
        
        contenView.addSubview(logoView)
        logoView.addAnchorToSuperview(leading: 0,heightMultiplier: 0.7, centeredVertically: 0,  heightWidthRatio: 1)
        contenView.addSubview(nameNumberStack)
        nameNumberStack.addAnchorToSuperview( heightMultiplier: 1, centeredVertically: 0)
        nameNumberStack.leadingAnchor.constraint(equalTo: logoView.trailingAnchor, constant: 10).isActive = true
        
        contenView.addSubview(amountStack)
        amountStack.addAnchorToSuperview( trailing: 0,top: -3,bottom: 3, widthMutiplier: 0.5)
        nameNumberStack.trailingAnchor.constraint(equalTo: amountStack.leadingAnchor, constant: -8).isActive = true
        return contenView
    }()
    let logoView : UIImageView = {
        let logoView = UIImageView()
        logoView.image = UIImage(named: "gp")
        logoView.contentMode = .scaleAspectFit
        return logoView
    }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 14.0, style: .medium)
        label.textColor = .black
        label.text = ""
        return label
    }()
    let numberLabel:UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 11.0, style: .regular)
        label.textColor = .battleshipGrey
        label.text = ""
        return label
    }()
    lazy var  nameNumberStack : UIStackView = {
        let nameNumberStack = UIStackView()
        nameNumberStack.axis = .vertical
        nameNumberStack.spacing = 2.5
        nameNumberStack.translatesAutoresizingMaskIntoConstraints = false
        nameNumberStack.addArrangedSubview(nameLabel)
        nameNumberStack.addArrangedSubview(numberLabel)
        return nameNumberStack
    }()
    lazy var amountStack: UIStackView = {
        let amountStack = UIStackView()
        amountStack.axis = .horizontal
        amountStack.distribution = .fillEqually
        amountStack.addArrangedSubview(amountButtonStack)
        amountStack.addArrangedSubview(amountEntryView)
        amountEntryView.isHidden = true
        return amountStack
    }()
    lazy var amountButtonStack: UIStackView = {
        let amountStack = UIStackView()
        amountStack.axis = .horizontal
        amountStack.distribution = .fillEqually
        amountStack.spacing = 2.5
        Buttons.allCases.forEach { btn in
            let view = QuickAmountView(title: btn.title)
            view.button.tag = btn.tag
            view.button.addTarget(self, action: #selector(quickAmountBtnAction(_:)), for: .touchUpInside)
            amountStack.addArrangedSubview(view)
        }
        
        return amountStack
    }()
    lazy var amountEntryView : UIView = {
        let amountEntryView = UIView()
        let view = UIView()
        amountEntryView.addSubview(view)
        view.addAnchorToSuperview(leading: 2.5, trailing: -2.5, top: 3, bottom: -3)
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.clearBlue.cgColor
        
        view.addSubview(amountTextfield)
        amountTextfield.addAnchorToSuperview(leading: 10, heightMultiplier: 0.8, centeredVertically: 0)
        view.addSubview(amountCrossBtn)
        amountCrossBtn.addAnchorToSuperview(trailing: -5,  heightMultiplier: 0.8, centeredVertically: 0, heightWidthRatio: 1)
        amountTextfield.trailingAnchor.constraint(equalTo: amountCrossBtn.leadingAnchor, constant: -5).isActive = true
        EasyManager.sharedInstance.delay(0.8) {
            view.layer.cornerRadius = view.frame.height/2
            
        }
        return amountEntryView
    }()
    lazy var amountTextfield:UITextField = {
        let tf = UITextField()
        tf.font = .EasyFont(ofSize: 12.0, style: .medium)
        tf.keyboardType = .numberPad
        tf.placeholder = "Enter Amount"
        tf.borderStyle = .none
        tf.inputAccessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        return tf
    }()
    let amountCrossBtn:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "crossgrayround"), for: .normal)
        button.addTarget(self, action: #selector(amountCrossBtnAction(_:)), for: .touchUpInside)
        return button
    }()
    @objc func amountCrossBtnAction(_ sender:UIButton){
        amountButtonStack.isHidden = false
        amountEntryView.isHidden = true
        amountTextfield.text = ""
    }
    @objc func quickAmountBtnAction(_ sender:UIButton){
     
       
        if sender.tag == 1301  {
            if isSelectionAllowed?(self) == true{
                amountButtonStack.isHidden = true
                amountEntryView.isHidden = false
                quickAmount = 0
            }
        }
        amountButtonStack.arrangedSubviews.forEach { v in
            if let view = v as? QuickAmountView{
                if sender.tag == view.button.tag && sender.tag != 1301{
                    if isSelectionAllowed?(self) == true{
                        view.isSelected = !view.isSelected
                        quickAmount = view.isSelected ? view.button.tag : 0
                    }
                }else{
                    view.isSelected = false
                }
            }
        }
       
        
    }
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
}


