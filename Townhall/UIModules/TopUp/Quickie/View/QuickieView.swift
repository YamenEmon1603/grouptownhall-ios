//
//  QuickieView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/1/21.
//

import UIKit

class QuickieView: EasyBaseView {
    var proceedCallBack : (()->Void)?
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .iceBlue
        self.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0,  heightMultiplier: 0.075)
        navView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        
        addSubview(contentView)
        contentView.addAnchorToSuperview(leading: 0, trailing: 0)
        contentView.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        
        addSubview(btnProceed)
        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0, heightMultiplier: 0.07)
        btnProceed.topAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title:"Quickie".uppercased())
        navView.backgroundColor = .white
        return navView
    }()
    let scrollView = UIScrollView()
    lazy var contentView: UIView = {
        let contentView = UIView()
        let scrollConatiner = UIView()
        contentView.addSubview(scrollConatiner)
        scrollConatiner.addAnchorToSuperview(leading: 0, trailing: 0,  bottom: 0)
        scrollConatiner.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollConatiner.addSubview(scrollView)
        scrollView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        let stackConatiner = UIView()
        scrollView.addSubview(stackConatiner)
        stackConatiner.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackConatiner.widthAnchor.constraint(equalTo: scrollConatiner.widthAnchor).isActive = true
        
        stackConatiner.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        
        stackView.addArrangedSubview(myNumberHeader)
        myNumberHeader.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        if let own = EasyDataStore.sharedInstance.ownNumber{
            let select = SelectNumber(number: own.number, operatorObj: own.operatorObj, connetctionType: ConectionType(own.connetctionType?.lowercased() ?? ""), amount: 0, scheduleTime: nil)
            
            let item = QuickieItemView(name: own.number ?? "", number: "", selectNum: select)
               
            item.isSelectionAllowed = isSelectionAllowed
            item.proceedCallBack = proceedCallBackAction
            stackView.addArrangedSubview(item)
            item.heightAnchor.constraint(equalToConstant: .calculatedHeight(55)).isActive = true
        }
       
        if let fevData = EasyDataStore.sharedInstance.favoriteData , fevData.count > 0{
            stackView.addArrangedSubview(fevNumberHeader)
            fevNumberHeader.heightAnchor.constraint(equalToConstant:  .calculatedHeight(30)).isActive = true
            
            for (i,elem) in fevData.enumerated(){
                
                let item = QuickieItemView(name:elem.name ?? "", number: elem.phone ?? "", selectNum: SelectNumber(number: elem.phone, operatorObj: elem._operator, connetctionType: ConectionType(elem.connectionType?.lowercased() ?? ""), amount: 0, scheduleTime: nil))
                item.logoView.image = elem._operator?.logoImage
                item.isSelectionAllowed = isSelectionAllowed
                item.proceedCallBack = proceedCallBackAction
                stackView.addArrangedSubview(item)
                item.heightAnchor.constraint(equalToConstant: .calculatedHeight(60)).isActive = true
            }
        }
       
        if let recentData = EasyDataStore.sharedInstance.recentData , recentData.count > 0{
            stackView.addArrangedSubview(recentNumberHeader)
            recentNumberHeader.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
            
            for (i,elem) in recentData.enumerated(){
                
                let item = QuickieItemView(name: "", number: elem.phone ?? "", selectNum: SelectNumber(number: elem.phone, operatorObj: elem._operator, connetctionType: ConectionType(elem.connectionType?.lowercased() ?? ""), amount: 0, scheduleTime: nil))
                item.logoView.image = elem._operator?.logoImage
                item.isSelectionAllowed = isSelectionAllowed
                item.proceedCallBack = proceedCallBackAction
                stackView.addArrangedSubview(item)
                item.heightAnchor.constraint(equalToConstant: .calculatedHeight(55)).isActive = true
            }
        }
        
        return contentView
    }()
    
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBackAction()
    }
    func  proceedCallBackAction(){
        proceedCallBack?()
    }
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    let myNumberHeader :  EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.text = "My Number".uppercased()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    let fevNumberHeader :  EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.text = "Select from favourite".uppercased()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    let recentNumberHeader :  EasyPaddingLabel = {
        let label = EasyPaddingLabel()
        label.text = "Select from recent".uppercased()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    var selectedItems:[QuickieItemView]?{
        get{
            let views  =  stackView.arrangedSubviews.filter { v in
                return v is QuickieItemView && (((v as? QuickieItemView)?.amount) != nil)
            }
            return views as? [QuickieItemView]
        }
    }
    func isSelectionAllowed(_ view: QuickieItemView) -> Bool {
        if let selected = self.selectedItems{
            if selected.contains(view){
                return true
            }else{
                return selected.count <= 2
            }
        }
        
        return true
    }
}
