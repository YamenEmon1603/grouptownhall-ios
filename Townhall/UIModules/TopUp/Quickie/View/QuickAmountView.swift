//
//  QuickAmountView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 8/3/21.
//

import UIKit
class QuickAmountView: UIView{
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    var isSelected:Bool = false {
        didSet{
            self.button.addShadow(location: .top, color: isSelected ? .clearBlue :.black, opacity: isSelected ? 1 : 0.2, radius: 2)
            button.layer.borderWidth = isSelected ? 1.5 : 0
            button.layer.borderColor =  isSelected ? UIColor.clearBlue.cgColor: UIColor.clear.cgColor
            button.setTitleColor(isSelected ? .clearBlue :.battleshipGrey, for: .normal)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    let button = UIButton()
    convenience init(title:String) {
        self.init(frame:.zero)
        backgroundColor = .white
        button.backgroundColor = .white
        addSubview(button)
        button.setTitle(title, for: .normal)
        button.setTitleColor(.battleshipGrey, for: .normal)
        button.layer.borderWidth = 0
        button.layer.borderColor = UIColor.clear.cgColor
        button.titleLabel?.font = title != "+" ? .EasyFont(ofSize: 11, style: .medium) : .EasyFont(ofSize: 16, style: .medium)
        button.addAnchorToSuperview(heightMultiplier: 0.8, centeredVertically: 0, centeredHorizontally: 0, heightWidthRatio: 1)
        EasyManager.sharedInstance.delay(0.1) {
            self.button.layer.cornerRadius = min(self.button.frame.width, self.button.frame.height)/2
            self.button.clipsToBounds = true
            self.button.addShadow(location: .top, color: .black, opacity: 0.2, radius: 2)
        }
    }
    
}
