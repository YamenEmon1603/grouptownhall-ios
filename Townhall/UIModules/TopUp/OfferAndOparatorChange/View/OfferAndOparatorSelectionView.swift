//
//  OfferAndOparatorSelectionView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 7/8/21.
//

import Foundation
import UIKit


class OfferAndOparatorSelectionView : EasyBaseView {
    private enum Buttons: CaseIterable{
        case fifty,Hundred,TwoHundred,FiveHundred
        var tag:Int{
            switch self {
            case .fifty:
                return 50
            case .Hundred:
                return 100
            case .TwoHundred:
                return 200
            case .FiveHundred:
                return 500
            }
        }
        var title:String{
            switch self {
            case .fifty,.Hundred,.TwoHundred,.FiveHundred:
                return "\(self.tag)"
            }
        }
    }
    lazy var amountButtonStack: UIStackView = {
        let amountStack = UIStackView()
        amountStack.axis = .horizontal
        amountStack.distribution = .fillEqually
        amountStack.spacing = 2.5
        Buttons.allCases.forEach { btn in
            let view = QuickAmountView(title: btn.title)
            view.button.tag = btn.tag
            view.backgroundColor = .clear
            view.button.addTarget(self, action: #selector(quickAmountBtnAction(_:)), for: .touchUpInside)
            amountStack.addArrangedSubview(view)
        }
        
        return amountStack
    }()
    
    @objc func quickAmountBtnAction(_ sender:UIButton){
        
        amountButtonStack.arrangedSubviews.forEach { v in
            if let view = v as? QuickAmountView{
                
                if sender.tag == view.button.tag {
                    view.isSelected = !view.isSelected
                    enterAmountTextfield.text = "\(sender.tag)"
                    
                }else{
                    view.isSelected = false
                }
            }
        }
    }
    
    var proceedCallBack : (()->Void)?
    var backCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView = UIScrollView()
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(navView)
        navView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, heightMultiplier: 0.075)
        
        let stack = UIStackView()
        stack.axis = .vertical
        //stack.distribution = .fill
        stack.backgroundColor = .battleshipGrey
        containerView.addSubview(stack)
        stack.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        stack.addArrangedSubview(scheduleButtonsVw)
        scheduleButtonsVw.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.07).isActive = true
        stack.addArrangedSubview(btnProceed)
        btnProceed.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.07).isActive = true
        
//        containerView.addSubview(btnProceed)
//        containerView.addSubview(scheduleButtonsVw)
//        scheduleButtonsVw.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
//
//        btnProceed.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0, heightMultiplier:  0.07)
        
        containerView.addSubview(formView)
        formView.topAnchor.constraint(equalTo: navView.bottomAnchor,constant: 1).isActive = true
        formView.addAnchorToSuperview(leading: 0, trailing: 0)
        formView.bottomAnchor.constraint(equalTo: stack.topAnchor,constant: -5).isActive = true
       // scheduleButtonsVw.bottomAnchor.constraint(equalTo: btnProceed.topAnchor,constant: -5).isActive = true
    }
    
    //MARK: COMPONENTS
    
    
    lazy var formView: UIView = {
        let formView = UIView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(topView)
        formView.addSubview(bottomView)
        topView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0)
        topView.heightAnchor.constraint(equalToConstant: .calculatedHeight(150)).isActive = true
        
        
        bottomView.addAnchorToSuperview(leading: 0, trailing: 0, bottom : 0)
        bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
        
        
        return formView
    }()
    lazy var scheduleButtonsVw: ScheduleAndAddNumberView  = {
        let view = ScheduleAndAddNumberView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    
    private let btnProceed:UIButton = {
        let btnProceed = UIButton()
        btnProceed.backgroundColor = .clearBlue
        btnProceed.setAttributedTitle(EasyUtils.shared.spaced(text: "proceed".easyLocalized()), for: .normal)
        btnProceed.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        btnProceed.setTitleColor(.white, for: .normal)
        btnProceed.addTarget(self, action: #selector(btnProceedAction(_:)), for: .touchUpInside)
        return btnProceed
    }()
    @objc func btnProceedAction(_ sender:UIButton){
        proceedCallBack?()
    }
    
    //MARK: InputViews
    
    lazy var topView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        view.addSubview(numberContainerStack)
        numberContainerStack.addAnchorToSuperview(leading: 20, trailing: -20, top: 10, bottom: -10)
        
        return view
    }()
    lazy var bottomView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .purple
        view.addSubview(offerListVw)
        offerListVw.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        return view
    }()
    
    lazy var navView: EasyNavigationBarView = {
        let navView = EasyNavigationBarView(title: "Mobile Recharge", rightItem: numberAddedButton)
        navView.backgroundColor = .white
        let _ = navView.addBorder(side: .bottom, color: .paleLilac, width: 0.5)
        return navView
    }()
    
    lazy var numberContainerStack : UIStackView  = {
        
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.axis = .vertical
        //view.distribution = .fill
        view.spacing = 10
        view.addArrangedSubview(enterNumberContainerView)
        view.addArrangedSubview(enterAmountContainerView)
        enterNumberContainerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(60)).isActive = true
        enterAmountContainerView.heightAnchor.constraint(equalToConstant: .calculatedHeight(60)).isActive = true
        
        
        return view
    }()
    lazy var backButton:UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "backImage"), for: .normal)
        backButton.tintColor = .black
        backButton.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        return backButton
    }()
    @objc func btnBackAction(_ sender:UIButton){
        backCallBack?()
    }
    
    lazy var enterNumberContainerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .whiteGrey
        view.addSubview(enteredphoneView)
        view.addSubview(operatorView)
        operatorView.addAnchorToSuperview(trailing: 0, top: 0, bottom: 0,widthMutiplier: 0.45)
        operatorView.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        enteredphoneView.addAnchorToSuperview(leading: 0, top: 0, bottom: 0)
        enteredphoneView.trailingAnchor.constraint(equalTo: operatorView.leadingAnchor, constant: 0).isActive = true
        enteredphoneView.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        
        return view
    }()
    lazy var enterAmountContainerView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        
        view.backgroundColor = .whiteGrey
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightPeriwinkle.cgColor
        view.layer.cornerRadius = 10
        view.addSubview(amountButtonStack)
        view.addSubview(enterAmountTextfield)
        enterAmountTextfield.addAnchorToSuperview(leading: 0 , top: 0, bottom: 0,widthMutiplier: 0.5)
        enterAmountTextfield.trailingAnchor.constraint(equalTo: amountButtonStack.leadingAnchor, constant: 0).isActive = true
        
        
        amountButtonStack.addAnchorToSuperview(trailing: -5, centeredVertically: 0)
        amountButtonStack.heightAnchor.constraint(equalToConstant: .calculatedHeight(40)).isActive = true
        //amountButtonStack.widthAnchor.constraint(equalToConstant:180).isActive = true
        return view
    }()
    lazy var enteredphoneView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        let stackView = UIStackView()
        stackView.axis = .vertical
        view.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackView.addArrangedSubview(lblOparatorTypeName)
        stackView.addArrangedSubview(lblPhoneNumber)
        
        
        return view
    }()
    lazy var operatorView : UIView  = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .white
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        view.addSubview(operatorImageView)
        view.addSubview(stackView)
        stackView.addAnchorToSuperview(trailing: 0,centeredVertically: 0)
        operatorImageView.addAnchorToSuperview(centeredVertically: 0)
        operatorImageView.heightAnchor.constraint(equalToConstant: .calculatedHeight(30)).isActive = true
        operatorImageView.widthAnchor.constraint(equalToConstant: .calculatedWidth(30)).isActive = true
        stackView.leadingAnchor.constraint(equalTo: operatorImageView.trailingAnchor, constant: 8).isActive = true
        
        stackView.addArrangedSubview(lblOparatorName)
        stackView.addArrangedSubview(lblChangeOparator)
        return view
    }()
    lazy var operatorImageView : UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "gp")
        return view
    }()
    lazy var lblOparatorName : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = ""
        view.textColor = .black
        view.textAlignment = .right
        view.font = .EasyFont(ofSize: 12, style: .bold)
        return view
    }()
    lazy var lblChangeOparator : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = "change_operator".easyLocalized()
        view.textColor = .clearBlue
        view.textAlignment = .right
        view.font = .EasyFont(ofSize: 10, style: .medium)
        return view
    }()
    
    lazy var lblOparatorTypeName : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = "Postpaid".uppercased()
        view.textColor = .slateGrey
        view.font = .EasyFont(ofSize: 12, style: .bold)
        return view
    }()
    lazy var lblPhoneNumber : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = ""
        view.textColor = .black
        view.font = .EasyFont(ofSize: 18, style: .bold)
        return view
    }()
    
    // AMount
    
    lazy var enterAmountTextfield : EasyTextFieldView  = {
        let textView = EasyTextFieldView(title: "Enter Amount".easyLocalized(),placeHolderColor: .slateGrey,placeholderFont: .EasyFont(ofSize: 18, style: .regular))
        textView.backgroundColor = .clear
        textView.accessoryView = ButtonAccessoryView(title: "proceed".easyLocalized(), delegate: self)
        textView.keyBoardType = .numberPad
        textView.limit = 4
        return textView
    }()
    
    
    lazy var offerListVw: OfferListView  = {
        let view = OfferListView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        return view
    }()
    let numberAddedButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = .clearBlue
        button.layer.cornerRadius = 4
        button.setTitle("number_added".easyLocalized().uppercased(), for: .normal)
        button.titleLabel?.font = .EasyFont(ofSize: 11, style: .bold)
        button.setTitleColor(.white, for: .normal)
        
        return button
    }()
    
}

extension OfferAndOparatorSelectionView: ButtonAccessoryViewDelegate{
    func tapAction(_ sender: ButtonAccessoryView) {
        proceedCallBack?()
    }
    
}

