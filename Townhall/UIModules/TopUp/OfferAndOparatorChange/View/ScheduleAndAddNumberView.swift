//
//  ScheduleAndAddNumberView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/8/21.
//

import Foundation
import UIKit


class ScheduleAndAddNumberView : EasyBaseView {
    // var viewAllCallBack : (()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .white
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, top: 2,bottom: 0)
        containerView.addShadow(location: .top, color: .black, opacity: 0.1, radius: 2)
        
        containerView.addSubview(buttonsView)
        buttonsView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0,bottom: 0)
    }
    lazy var buttonsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(ScheduleContainer)
        view.addSubview(buttonsSeparator)
        view.addSubview(addAnotherNumberBtn)
        buttonsSeparator.addAnchorToSuperview( heightMultiplier: 0.5, centeredVertically: 0, centeredHorizontally: 0)
        buttonsSeparator.widthAnchor.constraint(equalToConstant: 1).isActive = true
        ScheduleContainer.addAnchorToSuperview(leading: 0, top: 0, bottom: 0)
        ScheduleContainer.trailingAnchor.constraint(equalTo: buttonsSeparator.leadingAnchor, constant: 0).isActive = true
        
        addAnotherNumberBtn.addAnchorToSuperview(trailing: 0, top: 0, bottom: 0)
        addAnotherNumberBtn.leadingAnchor.constraint(equalTo: buttonsSeparator.trailingAnchor, constant: 0).isActive = true
        
        return view
    }()
    lazy var buttonsSeparator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .paleLilac
        return view
    }()
    
    lazy var ScheduleContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let stackView = UIStackView()
        stackView.axis = .horizontal
        view.addSubview(stackView)
        stackView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        stackView.addArrangedSubview(scheduleRechargeBtn)
        stackView.addArrangedSubview(scheduleRechargeView)
        
        
        return view
    }()
    
    lazy var scheduleRechargeBtn : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setTitle("schedule_recharge".easyLocalized(), for: .normal)
        view.titleLabel?.font = .EasyFont(ofSize: 11, style: .medium)
        view.setTitleColor(.clearBlue, for: .normal)
        view.backgroundColor = .white
        
        return view
    }()
    lazy var addAnotherNumberBtn : UIButton  = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.setTitle("add_another_number".easyLocalized(), for: .normal)
        view.titleLabel?.font = .EasyFont(ofSize: 11, style: .medium)
        view.setTitleColor(.clearBlue, for: .normal)
        view.backgroundColor = .white
        return view
    }()
    lazy var scheduleRechargeView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        view.addSubview(stackView)
        view.addSubview(btnScheduleClose)
        btnScheduleClose.addAnchorToSuperview(trailing: -4, top: 0, bottom: 0)
        btnScheduleClose.leadingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: 2).isActive = true
        btnScheduleClose.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        stackView.addAnchorToSuperview(leading: 15, centeredVertically: 0)
        stackView.addArrangedSubview(lblScheduleOn)
        stackView.addArrangedSubview(lblTimeSchedule)
        view.isHidden = true
        
        return view
    }()
    
    lazy var lblScheduleOn : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = "scheduled_on".easyLocalized()
        view.textColor = .coolGrey
        view.textAlignment = .left
        view.font = .EasyFont(ofSize: 10, style: .medium)
        return view
    }()
    lazy var lblTimeSchedule : UILabel  = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.text = "Thu,Apr 13 | 3:30PM"
        view.textColor = .clearBlue
        view.textAlignment = .left
        view.font = .EasyFont(ofSize: 12, style: .bold)
        return view
    }()
    let btnScheduleClose : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints =  false
        button.setImage(UIImage(named: "crossgraysmall"), for: .normal)
        return button
    }()
    
    
    
    
}
