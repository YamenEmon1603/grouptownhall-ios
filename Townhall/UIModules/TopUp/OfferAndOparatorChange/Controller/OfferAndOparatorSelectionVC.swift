//
//  OfferAndOparatorSelectionVC.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 7/8/21.
//

import UIKit


class OfferAndOparatorSelectionVC: EasyBaseViewController {
    var selectedDateStr : String?
    var selectedNumber : SelectNumber?
    var offeredData : RechargePlan?
    lazy var mView: OfferAndOparatorSelectionView = {
        let view = OfferAndOparatorSelectionView()
        view.navView.backButtonCallBack = backAction
        view.offerListVw.viewAllCallBack = viewAll
        view.numberAddedButton.addTarget(self, action: #selector(numberAddedAction), for: .touchUpInside)
        view.enterAmountTextfield.textChangedCallBack = amountAction(_:)
        view.scheduleButtonsVw.scheduleRechargeBtn.addTarget(self, action: #selector(setScheduleRechargeAction), for: .touchUpInside)
        view.scheduleButtonsVw.btnScheduleClose.addTarget(self, action: #selector(btnScheduleCloseAction), for: .touchUpInside)
        view.offerListVw.applyButton.addTarget(self, action: #selector(applyOffer), for: .touchUpInside)
        view.scheduleButtonsVw.addAnotherNumberBtn.addTarget(self, action: #selector(addAnotherNumber), for: .touchUpInside)
        
        
        
        view.proceedCallBack = proccedAction
        return view
    }()
    
    
    var numberSet : String?{
        didSet{
            mView.lblPhoneNumber.text = numberSet
            if EasyDataStore.sharedInstance.seletedNumbersArray.count > 0{
                mView.navView.rightItem = mView.numberAddedButton
                mView.numberAddedButton.isHidden = false
                let added =  EasyDataStore.sharedInstance.seletedNumbersArray.count
                mView.numberAddedButton.setTitle("\(added) "+"number_added".easyLocalized().uppercased(), for: .normal)
            }else{
                mView.numberAddedButton.isHidden = true
            }
        }
    }
    func amountAction(_ text : String){
        //amountSet = Int(text)
        print(text)
        self.prepareMostPopular(op: selectedNumber?.operatorObj, conectionType: selectedNumber?.connetctionType ?? "prepaid", amount: Int(text) ?? 0)
        selectedNumber?.amount = Int(text)
    }
    var first5Plans: [RechargePlan]?{
        didSet{
            if first5Plans?.count == 0{
                mView.offerListVw.offerForView.isHidden = true
            }
            mView.offerListVw.offersTableView.reloadData()
            
        }
    }
    
    func prepareMostPopular(op : MBOperator? = nil,conectionType:String = "", amount: Int = 0){
        EasyManager.sharedInstance.prepareMostPopular(op: op, conectionType: conectionType, amount: amount, max: 5, isPolular: true, offerType: nil, filteredData: {nextplan,data in
            if let nextplan = nextplan, let amountOffer = nextplan.amountMax {
                
                let amountForOffer = amountOffer - amount
                let nextOffer = "Add \(amountForOffer)TK more to get \(nextplan.offerTitle ?? "nil")"
                if amountForOffer > 0{
                    mView.offerListVw.offerForView.isHidden = false
                }else{
                    mView.offerListVw.offerForView.isHidden = true
                }
                mView.offerListVw.selectForYouSubtitle.text = nextOffer
            }else{
                mView.offerListVw.offerForView.isHidden = true
            }
            if let plans = data{
                first5Plans = plans
            }
        })
        
    }
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        setOperatorUI()
        mView.offerListVw.offersTableView.dataSource = self
        mView.offerListVw.offersTableView.delegate = self
        let numberSelectionTap = UITapGestureRecognizer(target: self, action: #selector(selectNumberFromListAction))
        mView.enterNumberContainerView.addGestureRecognizer(numberSelectionTap)
        if EasyManager.sharedInstance.isFromRechargeRequest == true{
            mView.scheduleButtonsVw.isHidden = true
        }else{
            mView.scheduleButtonsVw.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    func setOperatorUI(){
        mView.quickAmountBtnAction(UIButton())
        
        if let numberObj  = selectedNumber{
            btnScheduleCloseAction()
            prepareMostPopular(op: numberObj.operatorObj, conectionType: numberObj.connetctionType?.lowercased() ?? "", amount: numberObj.amount ?? 0)
            if numberObj.amount != 0{
                mView.enterAmountTextfield.text =  numberObj.amount != nil ? "\(numberObj.amount ?? 0)" : ""
            }
            
            if let number  = numberObj.number{
                numberSet = number
                mView.lblOparatorName.text = numberObj.operatorObj?.operatorName
                mView.lblOparatorTypeName.text = numberObj.connetctionType?.uppercased()
                mView.operatorImageView.image = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorShortName == numberObj.operatorObj?.operatorShortName})?.logoImage
                if numberObj.connetctionType?.lowercased() == "skitto"{
                    mView.lblOparatorTypeName.text = numberObj.connetctionType?.lowercased()
                    mView.lblOparatorName.text = "Grameenphone"
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    @objc func selectNumberFromListAction(){
        
        DispatchQueue.main.async {
            OperatorSelectionViewController.show(on: self, delegate: self)
        }
    }
    @objc func applyOffer(){
        mView.offerListVw.offerForView.isHidden = false
        mView.enterAmountTextfield.text = "\(offeredData?.amountMax ?? 0)"
    }
    @objc func setScheduleRechargeAction(){
        selectedDateStr = nil
        showScheduleAlert()
    }
    @objc func btnScheduleCloseAction(){
        mView.scheduleButtonsVw.scheduleRechargeBtn.isHidden = false
        mView.scheduleButtonsVw.scheduleRechargeView.isHidden = true
        selectedDateStr = nil
        selectedNumber?.scheduleTime = nil
    }
    func backAction(){
        showBottomAlert()
    }
    @objc func addAnotherNumber(){
        if validation() == true{
            if EasyDataStore.sharedInstance.seletedNumbersArray.count  < 3 {
                self.navigationController?.popViewController(animated: true)
            }else{
                showToast(message: "maximum_three_number_allowed".easyLocalized())
            }
            
            
        }
    }
    
    let bottomAlert = HorizontalButtonAlertView(header: "do_you_want_to_go_back".easyLocalized(), subHeader: "your_phone_number_&_amount_will_be_removed".easyLocalized(), btn1Name: "yes".easyLocalized().uppercased(), btn2Name: "stay".uppercased().easyLocalized())
    func showBottomAlert() {
        self.view.addSubview(bottomAlert)
        bottomAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        bottomAlert.btnSecondCallBack = btnSecondAction
        bottomAlert.btnFirstCallBack = btnFirstAction
    }
    
    let scheduleAlert = DateSelectionView()
    func showScheduleAlert() {
        self.view.addSubview(scheduleAlert)
        scheduleAlert.addAnchorToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        scheduleAlert.btnDoneCallBack = btnScheduleDoneAction
        scheduleAlert.datePicker.timeZone = NSTimeZone.local
        scheduleAlert.datePicker.datePickerMode = .dateAndTime
        let currentDate = NSDate()  //get the current date
        let calendar = Calendar.current
        let today = currentDate as Date
        let dateDelay = calendar.date(byAdding: .minute, value: 5, to: today)
        scheduleAlert.datePicker.date = dateDelay! as Date
        scheduleAlert.datePicker.minimumDate = dateDelay
        scheduleAlert.datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
    }
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = Constant.DateFormat17
        //EEE, MMM d | HH:mm
        
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        
        print("Selected value \(selectedDate)")
        selectedDateStr = selectedDate
        
    }
    func btnScheduleDoneAction(){
        
        if let scDate = selectedDateStr, scDate != ""{
            mView.scheduleButtonsVw.scheduleRechargeBtn.isHidden = true
            mView.scheduleButtonsVw.scheduleRechargeView.isHidden = false
            mView.scheduleButtonsVw.lblTimeSchedule.text = selectedDateStr
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  Constant.DateFormat17
            let date = dateFormatter.date(from: scDate)
            selectedNumber?.scheduleTime = date
        }
        scheduleAlert.removeFromSuperview()
    }
    
    func btnSecondAction(){
        bottomAlert.removeFromSuperview()
        
    }
    func btnFirstAction(){
        EasyDataStore.sharedInstance.seletedNumbersArray = []
        (self.tabBarController as? EasyTabBarViewController)?.setTabBarHidden(false)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func viewAll(){
        print("view all...lol")
        //mView.offerListVw.offerForView.isHidden = true
        let vc = ViewAllOfferController()
        vc.offerDelegate = self
        vc.selectedNumber =  selectedNumber
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        //        let vc1 = ViewAllOfferController()
        //        self.navigationController?.pushViewController(vc1, animated: true)
    }
    
    
    @objc func numberAddedAction(){
        let vc = RechargeSummerViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func validation()->Bool{
        if  selectedNumber?.amount != nil {
            let connectionType = selectedNumber?.connetctionType
            if connectionType?.lowercased() == "postpaid"{
                if let amountInt = selectedNumber?.amount{
                    if amountInt < 2501 && amountInt > 9{
                        //let vc = RechargeSummerViewController()
                        if let number = selectedNumber{
                            let  filter =  EasyDataStore.sharedInstance.seletedNumbersArray.filter({$0.number == number.number})
                            if filter.count != 0{
                                filter.first?.amount = number.amount
                                filter.first?.operatorObj = number.operatorObj
                                filter.first?.connetctionType = number.connetctionType
                            }else{
                                EasyDataStore.sharedInstance.seletedNumbersArray.append(number)
                            }
                            
                        }
                        return true
                    }else{
                        self.showToast(message: "ten_to_two_thousand_five_for_postpaid".easyLocalized())
                        return false
                    }
                }else{
                    showToast(message: "enter_amount_first".easyLocalized())
                    return false
                }
            }else if connectionType?.lowercased() == "prepaid" || connectionType?.lowercased() == "skitto"{
                if let amountInt = selectedNumber?.amount{
                    if amountInt < 1001 && amountInt > 9{
                        //let vc = RechargeSummerViewController()
                        if let number = selectedNumber{
                            
                            
                            if let index = EasyDataStore.sharedInstance.seletedNumbersArray.firstIndex(where:{ $0.number == number.number}){
                                
                                EasyDataStore.sharedInstance.seletedNumbersArray[index].amount = number.amount
                                EasyDataStore.sharedInstance.seletedNumbersArray[index].operatorObj = number.operatorObj
                                EasyDataStore.sharedInstance.seletedNumbersArray[index].connetctionType = number.connetctionType
                                
                                
                            }else{
                                EasyDataStore.sharedInstance.seletedNumbersArray.append(number)
                            }
                            
                        }
                        return true
                    }else{
                        self.showToast(message: "ten_to_thousand_for_prepaid".easyLocalized())
                        return false
                    }
                }else{
                    showToast(message: "enter_amount_first".easyLocalized())
                    return false
                }
            }else{
                return false
            }
            
        }else{
            showToast(message: "enter_amount_first".easyLocalized())
            return false
        }
        
    }
    
    func proccedAction(){
        //        if  selectedNumber?.amount != nil {
        
        
        
        if validation() == true{
            if EasyManager.sharedInstance.isFromRechargeRequest == true{
                if let data  = EasyDataStore.sharedInstance.seletedNumbersArray.first{
                    let rq = RechargeRequestViewController()
                    rq.rechargeEntity = data
                    self.navigationController?.pushViewController(rq, animated: true)
                }
                
            }else{
                let vc = RechargeSummerViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    @objc func numberEntry(){
        let vc = NumberListViewController()
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
extension OfferAndOparatorSelectionVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return first5Plans?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OfferListTableViewCell.identifier, for: indexPath) as! OfferListTableViewCell
        //cell.projectMileStone = filteredProjectMilestone?[indexPath.row]
        //cell.contentView.isUserInteractionEnabled = false
        cell.easyRechargePlans = first5Plans?[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mView.enterAmountTextfield.text = "\(first5Plans?[indexPath.row].amountMax ?? 0)"
        if first5Plans?.count ?? 0 > 1{
            prepareMostPopular(op: selectedNumber?.operatorObj, conectionType: selectedNumber?.connetctionType ?? "", amount: first5Plans?[indexPath.row].amountMax ?? 0)
        }
        
        mView.quickAmountBtnAction(UIButton())
    }
    
    
}

extension OfferAndOparatorSelectionVC : NumberSelectedFromList,OfferSelectedFromViewAllList{
    func offerSelected(offer: RechargePlan) {
        mView.quickAmountBtnAction(UIButton())
        mView.enterAmountTextfield.text = "\(offer.amountMax ?? 0)"
    }
    
    func selected(number: String, operatorObj: MBOperator, connetctionType: ConectionType) {
        
        //        if validation() == true{
        //            if let number = selectedNumber{
        //                //EasyDataStore.sharedInstance.seletedNumbersArray.append(number)
        //            }
        //        }
        
        
        //   selectedNumber = SelectNumber(number: number, operatorObj: operatorObj, connetctionType: connetctionType, amount: amountSet, scheduleTime: nil)
        //   setOperatorUI()
    }
}
extension OfferAndOparatorSelectionVC : OperatorSelectionDelegate{
    func didSelect(operatorObject: MBOperator, connectionType: ConectionType) {
        if let numberObj = selectedNumber{
            selectedNumber?.operatorObj = operatorObject
            selectedNumber?.connetctionType = connectionType.value
            mView.lblOparatorName.text = numberObj.operatorObj?.operatorName
            mView.lblOparatorTypeName.text = numberObj.connetctionType?.uppercased()
            mView.operatorImageView.image = EasyDataStore.sharedInstance.easyMobileOperators.first(where: {$0.operatorShortName == numberObj.operatorObj?.operatorShortName})?.logoImage
            if numberObj.connetctionType?.lowercased() == "skitto"{
                mView.lblOparatorTypeName.text = numberObj.connetctionType?.lowercased()
                mView.lblOparatorName.text = "Grameenphone"
            }
        }
    }
    
    
}
