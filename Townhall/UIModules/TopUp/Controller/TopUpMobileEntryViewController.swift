//
//  TopUpMobileEntryViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/7/21.
//

import UIKit

class TopUpMobileEntryViewController: UIViewController {
    lazy var mView: TopUpMobileEntryView = {
        let view = TopUpMobileEntryView()
        view.navView.backButtonCallBack = backAction
        
        return view
    }()
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func backAction(){
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
