//
//  InstructionView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/7/21.
//

import Foundation
import UIKit

class InstructionView: UIView {
 
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(title: String, imageLink : String) {
        self.init(frame:.zero)
        backgroundColor = .white
        let containerView = UIView()
        containerView.backgroundColor = .iceBlue
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        containerView.addAnchorToSuperview(leading: 0, trailing: 0, bottom: 0)
        containerView.addSubview(instructionImageView)
        containerView.addSubview(lblTitle)
        
        instructionImageView.addAnchorToSuperview(heightMultiplier: 0.5,centeredVertically: -30, centeredHorizontally: 0, heightWidthRatio: 1)
        instructionImageView.image = UIImage(named: imageLink)
        lblTitle.addAnchorToSuperview(bottom: -20, heightMultiplier: 0.3,widthMutiplier: 0.6,centeredHorizontally: 0)
        lblTitle.text = title
        lblTitle.setLineSpacing(lineSpacing: 10)
        lblTitle.textAlignment = .center
    }
   
    //MARK: COMPONENTS

    lazy var instructionImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    lazy var lblTitle: UILabel = {
        let lblTitle = UILabel()
        lblTitle.font = .EasyFont(ofSize: 16, style: .medium)
        lblTitle.textColor =  .black
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.numberOfLines = 0
        lblTitle.textAlignment = .center
        return lblTitle
    }()
}



