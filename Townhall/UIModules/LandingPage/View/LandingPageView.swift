//
//  LandingPageView.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/7/21.
//

import Foundation
import UIKit

class LandingPageView: UIView {
    var privacyCallBack:(()->Void)?
    var termsCallBack:(()->Void)?
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .blueViolet
        let containerView = UIView()
        
        addSubview(containerView)
        containerView.addAnchorToSuperview(leading: 0, trailing: 0,top: 0,bottom: 0)
        
        containerView.backgroundColor = .blueViolet
        containerView.addSubview(imageView)
        imageView.addAnchorToSuperview( top: .calculatedHeight(61.9), heightMultiplier: 0.062, centeredHorizontally: 0)
        
        containerView.addSubview(bgimageView)
        bgimageView.addAnchorToSuperview(leading: 0, trailing: 0, top: 0)
        bgimageView.contentMode = .scaleToFill
        
        containerView.addSubview(viewPrivacyPolicy)
        viewPrivacyPolicy.addAnchorToSuperview(leading: 20, trailing: -20)
        viewPrivacyPolicy.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        
        containerView.addSubview(btnGetStarted)
        btnGetStarted.addAnchorToSuperview(leading: 20, trailing: -20,heightMultiplier: 0.077)
        btnGetStarted.bottomAnchor.constraint(equalTo: viewPrivacyPolicy.topAnchor, constant: -15).isActive = true
        
        containerView.addSubview(textLabel)
        textLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        textLabel.topAnchor.constraint(equalTo: bgimageView.bottomAnchor,constant: -50).isActive = true
        
        containerView.addSubview(locLabel)
        locLabel.addAnchorToSuperview(leading: 20, trailing: -20)
        locLabel.topAnchor.constraint(equalTo: textLabel.bottomAnchor,constant: 6).isActive = true
        
        let imageView2 = UIImageView()
        imageView2.contentMode = .scaleAspectFit
        imageView2.image = UIImage(named: "ssl")
        containerView.addSubview(imageView2)
        imageView2.addAnchorToSuperview(  centeredHorizontally: 0)
        imageView2.topAnchor.constraint(equalTo: locLabel.bottomAnchor,constant: 5).isActive = true
        imageView2.bottomAnchor.constraint(equalTo: btnGetStarted.topAnchor,constant: -5).isActive = true
        
        
        let imageView1 = UIImageView()
        imageView1.contentMode = .scaleAspectFit
        imageView1.image = UIImage(named: "oil")
        imageView1.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView1)
        
        imageView1.trailingAnchor.constraint(equalTo: imageView2.leadingAnchor, constant: .calculatedWidth(30)).isActive = true
        imageView1.topAnchor.constraint(equalTo: locLabel.bottomAnchor,constant: 5).isActive = true
        imageView1.bottomAnchor.constraint(equalTo: btnGetStarted.topAnchor,constant: -5).isActive = true
        
        let imageView4 = UIImageView()
        imageView4.contentMode = .scaleAspectFit
        imageView4.image = UIImage(named: "cgg")
        imageView4.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView4)
        
        imageView4.trailingAnchor.constraint(equalTo: imageView1.leadingAnchor, constant: .calculatedWidth(30)).isActive = true
        imageView4.topAnchor.constraint(equalTo: locLabel.bottomAnchor,constant: 5).isActive = true
        imageView4.bottomAnchor.constraint(equalTo: btnGetStarted.topAnchor,constant: -5).isActive = true
        
        
       
        let imageView3 = UIImageView()
        imageView3.contentMode = .scaleAspectFit
        imageView3.image = UIImage(named: "ngage")
        imageView3.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView3)
        imageView3.leadingAnchor.constraint(equalTo: imageView2.trailingAnchor, constant:-.calculatedWidth(8)).isActive = true
        imageView3.topAnchor.constraint(equalTo: locLabel.bottomAnchor,constant: 5).isActive = true
        imageView3.bottomAnchor.constraint(equalTo: btnGetStarted.topAnchor,constant: -5).isActive = true
        
        let imageView5 = UIImageView()
        imageView5.contentMode = .scaleAspectFit
        imageView5.image = UIImage(named: "sslcommerz")
        imageView5.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView5)
        imageView5.leadingAnchor.constraint(equalTo: imageView3.trailingAnchor, constant: .calculatedWidth(15)).isActive = true
        imageView5.topAnchor.constraint(equalTo: locLabel.bottomAnchor,constant: 5).isActive = true
        imageView5.bottomAnchor.constraint(equalTo: btnGetStarted.topAnchor,constant: -5).isActive = true
    }
    
    //MARK: COMPONENTS
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "landingLogo")
        return imageView
    }()
    
    let bgimageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "bgimage")
        return imageView
    }()
    
    let textLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 28.0, style: .bold)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "Group Town Hall 2022"
        label.numberOfLines = 2
        return label
    }()
    
    let locLabel: UILabel = {
        let label = UILabel()
        label.font = .EasyFont(ofSize: 13.0, style: .medium)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "International Convention City Bashundhara"
        return label
    }()
    
    let stack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 0
        let imageView1 = UIImageView()
        imageView1.contentMode = .scaleAspectFit
        imageView1.image = UIImage(named: "cgg")
        stack.addArrangedSubview(imageView1)
        
        let imageView2 = UIImageView()
        imageView2.contentMode = .scaleAspectFit
        imageView2.image = UIImage(named: "oil")
        stack.addArrangedSubview(imageView2)
        
        let imageView3 = UIImageView()
        imageView3.contentMode = .scaleAspectFit
        imageView3.image = UIImage(named: "ssl")
        stack.addArrangedSubview(imageView3)
        
        let imageView4 = UIImageView()
        imageView4.contentMode = .scaleAspectFit
        imageView4.image = UIImage(named: "ngage")
        stack.addArrangedSubview(imageView4)
        
        let imageView5 = UIImageView()
        imageView5.contentMode = .scaleAspectFit
        imageView5.image = UIImage(named: "ssl")
        stack.addArrangedSubview(imageView5)
        return stack
    }()
    //MARK: ENTER PHONE/EMAIL
    
    
    lazy var btnGetStarted: UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .pumpkinOrange
        button.layer.borderWidth = 1.5
        button.layer.borderColor =  UIColor.pumpkinOrange.cgColor
        button.titleLabel?.font = UIFont.EasyFont(ofSize: 16, style: .bold)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Get Started", for: .normal)
        button.layer.cornerRadius = 10
        return button
    }()
    
    
    
    
    //MARK: TERMS AND CONDITION
    
    
    lazy var btnTermsAndService: UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.EasyFont(ofSize: 12, style: .regular)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("term_of_service".easyLocalized(), for: .normal)
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(termsTapped(_:)), for: .touchUpInside)
        return button
    }()
    lazy var btnPrivacyPolicy: UIButton = {
        let  button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.EasyFont(ofSize: 12, style: .regular)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("privacy_policy".easyLocalized(), for: .normal)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(privacyTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var viewPrivacyPolicy: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var sepView = UIView()
        sepView.backgroundColor = UIColor.gray
        view.addSubview(btnTermsAndService)
        view.addSubview(btnPrivacyPolicy)
        view.addSubview(sepView)
        sepView.addAnchorToSuperview( top: 10,bottom: -10,centeredHorizontally: 0)
        sepView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        
        btnTermsAndService.addAnchorToSuperview(leading: 0,  top: 0, bottom: 0 )
        btnTermsAndService.trailingAnchor.constraint(equalTo: sepView.leadingAnchor, constant: -10).isActive = true
        
        btnPrivacyPolicy.leadingAnchor.constraint(equalTo: sepView.trailingAnchor, constant: 10).isActive = true
        btnPrivacyPolicy.addAnchorToSuperview(trailing: 0,top: 0, bottom: 0)
        
        return view
    }()
    @objc func privacyTapped(_ sender : UIButton){
        privacyCallBack?()
    }
    @objc func termsTapped(_ sender : UIButton){
        termsCallBack?()
    }
    lazy var pageController : UIPageControl = {
        var page  = UIPageControl()
        page.translatesAutoresizingMaskIntoConstraints = false
        page.numberOfPages = 3
        page.currentPage = 0
        page.pageIndicatorTintColor = .paleViolet
        if #available(iOS 14.0, *) {
            let image = UIImage(named: "pagingIndicator")
            page.preferredIndicatorImage = image
        } else {
            page.currentPageIndicatorTintColor = .white
            // Fallback on earlier versions
        }
        return page
        
    }()
    
    let languageSwitch : LanguageSwitch = {
        let view = LanguageSwitch()
        return view
    }()
}


class LanguageSwitch:UIView{
    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame:.zero)
        backgroundColor = .white
        addShadow(location: .bottom, color: .black, opacity: 0.2, radius: 5)
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.addAnchorToSuperview(leading: 3, trailing: -3, top: 3, bottom: -3)
        stack.addArrangedSubview(engLabel)
        stack.addArrangedSubview(banglaLabel)
        EasyManager.sharedInstance.delay(0.10) {
            self.banglaLabel.layer.cornerRadius = self.banglaLabel.frame.height/2
            self.engLabel.layer.cornerRadius = self.engLabel.frame.height/2
        }
        clipsToBounds = true
        layer.cornerRadius = 15
        
    }
    let banglaLabel : UIButton = {
        let banglaLabel = UIButton()
        banglaLabel.backgroundColor = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .Bangla ? .blueViolet : .clear
        banglaLabel.titleLabel?.font = .EasyFont(ofSize: 13, style: .regular)
        banglaLabel.setTitleColor(SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .Bangla ? .white : .battleshipGrey, for: .normal)
        banglaLabel.setTitle("বাংলা", for: .normal)
        banglaLabel.clipsToBounds = true
        banglaLabel.addTarget(self, action: #selector(switchLang(_:)), for: .touchUpInside)
        return banglaLabel
    }()
    let engLabel : UIButton = {
        let engLabel = UIButton()
        engLabel.backgroundColor = SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? .blueViolet : .clear
        engLabel.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        engLabel.setTitleColor(SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .English ? .white : .battleshipGrey, for: .normal)
        engLabel.setTitle("ENG", for: .normal)
        engLabel.clipsToBounds = true
        engLabel.addTarget(self, action: #selector(switchLang(_:)), for: .touchUpInside)
        return engLabel
    }()
    @objc func switchLang(_ sender : UIButton){
        if sender == banglaLabel{
            engLabel.backgroundColor = .clear
            engLabel.setTitleColor(.battleshipGrey,for: .normal)
            banglaLabel.backgroundColor = .blueViolet
            banglaLabel.setTitleColor(.white,for: .normal)
            SSLComLanguageHandler.sharedInstance.switchLanguage(to: .Bangla)
            banglaLabel.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
            engLabel.titleLabel?.font = .EasyFont(ofSize: 13, style: .regular)
            
        }else{
            engLabel.backgroundColor = .blueViolet
            engLabel.setTitleColor(.white,for: .normal)
            banglaLabel.backgroundColor = .clear
            banglaLabel.setTitleColor(.battleshipGrey,for: .normal)
            SSLComLanguageHandler.sharedInstance.switchLanguage(to: .English)
            banglaLabel.titleLabel?.font = .EasyFont(ofSize: 13, style: .regular)
            engLabel.titleLabel?.font = .EasyFont(ofSize: 13, style: .bold)
        }
    }
  
}



