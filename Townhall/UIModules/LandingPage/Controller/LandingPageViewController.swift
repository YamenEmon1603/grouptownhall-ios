//
//  LandingPageViewController.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 8/7/21.
//

import UIKit
import Firebase
import FirebaseAuth
import AuthenticationServices

class LandingPageViewController: EasyBaseViewController, SSLComLanguageDelegate {
    func languageDidChange(to language: SDKLanguage) {
        
      
        mView.btnTermsAndService.setTitle("term_of_service".easyLocalized(), for: .normal)
        mView.btnPrivacyPolicy.setTitle("privacy_policy".easyLocalized(), for: .normal)
    }
    
    
    lazy var mView: LandingPageView = {
        let view = LandingPageView()
       
        view.btnGetStarted.addTarget(self, action: #selector(EnterPhoneEmailTapped(_:)), for: .touchUpInside)
        view.privacyCallBack = privacy
        view.termsCallBack = terms
      

        return view
    }()
    
    override func viewDidLoad() {
        self.view = mView
        super.viewDidLoad()
        SSLComLanguageHandler.sharedInstance.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EasyManager.sharedInstance.isFromRechargeRequest = false
    }
    @objc func helpAndSuppportTapped( _ sender: UIButton){
        
//        let vc = ExperienceDeatilsVC()
//        if sender.tag == 2021{
//            vc.navTitle = ""
//            vc.weblink = "https://easy.com.bd/terms"
//        }else{
//            vc.navTitle = ""
//            vc.weblink = "https://easy.com.bd/privacy"
//        }
       
       // self.navigationController?.pushViewController(vc, animated: true)
    }
    func terms(){
        let vc = WebPageVC()
        vc.type = .terms
        self.navigationController?.pushViewController(vc, animated: true)
    }
   func privacy(){
       let vc = WebPageVC()
       vc.type = .privecy
       self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func EnterPhoneEmailTapped( _ sender: UIButton){
        let vc = PhoneEntryViewController()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func getFBGoogleUserStatus(token: String, provider: String) {
    
    }
    func getAppleUserStatus(id: String,token: String, provider: String){
        
    }
    @objc func appleLoginTapped( _ sender: UIButton){
      
    }
    @objc func rechargeReqLoginTapped( _ sender: UIButton){

    }
    @objc func googleLoginTapped( _ sender: UIButton){
        guard let clientID = FirebaseApp.app()?.options.clientID else{
            return
        }
        
        
    }
    
    @objc func facebookLoginTapped( _ sender: UIButton){
        
     
    }
    
}



extension LandingPageViewController : ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    //For present window
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        
        return self.view.window!
        
    }
    
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
        debugPrint(error.localizedDescription)
        
    }
    
    // ASAuthorizationControllerDelegate function for successful authorization
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            // Create an account as per your requirement
            
            let appleId = appleIDCredential.user
            debugPrint(appleId)
            debugPrint(appleIDCredential.identityToken ?? "")
            debugPrint(appleIDCredential.authorizationCode ?? "")
            if let appleAccessToken = (appleIDCredential.identityToken){
                let accessToken = String(decoding: appleAccessToken, as: UTF8.self)
                debugPrint(accessToken)
                getAppleUserStatus(id: appleId, token: accessToken, provider: Constant.socialLoginType.apple.rawValue)
                //self.presenter?.getAppleUserStatus(id: appleId, token: accessToken, provider: "apple")
            }
            
            
            if let appleUserFirstName = appleIDCredential.fullName?.givenName, let appleUserEmail = appleIDCredential.email{
                EasyDataStore.sharedInstance.appleUname = appleUserFirstName
                EasyDataStore.sharedInstance.appleEmail = appleUserEmail
            }
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            debugPrint(appleUsername,applePassword)
        }
        
    }
}
