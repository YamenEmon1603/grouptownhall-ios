//
//  UIView.swift
//  ssl_commerz_revamp
//
//  Created by Mausum Nandy on 5/19/21.
//

import Foundation
import UIKit
extension UIView{
    func addAnchorToSuperview(leading:CGFloat? = nil, trailing:CGFloat? = nil , top:CGFloat? = nil, bottom:CGFloat? = nil, heightMultiplier:CGFloat? = nil,widthMutiplier:CGFloat? = nil,centeredVertically : CGFloat? = nil, centeredHorizontally: CGFloat? = nil,heightWidthRatio:CGFloat? = nil )  {
        if  let superView = self.superview{
            self.translatesAutoresizingMaskIntoConstraints = false
            if let leading = leading {
                self.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: leading).isActive = true
            }
            if let trailing = trailing {
                self.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: trailing).isActive = true
            }
            if let top = top{
                self.topAnchor.constraint(equalTo: superView.topAnchor, constant: top).isActive = true
            }
            if let bottom = bottom{
                self.bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: bottom).isActive = true
            }
            if let heightMultiplier = heightMultiplier{
                self.heightAnchor.constraint(equalTo: superView.heightAnchor, multiplier: heightMultiplier).isActive = true
                if let heightWidthRatio = heightWidthRatio {
                    self.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: heightWidthRatio).isActive = true
                }
            }
            if let widthMutiplier = widthMutiplier{
                self.widthAnchor.constraint(equalTo: superView.widthAnchor, multiplier: widthMutiplier).isActive = true
                if let heightWidthRatio = heightWidthRatio {
                    self.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: heightWidthRatio).isActive = true
                }
            }
            if let centeredVertically = centeredVertically{
                self.centerYAnchor.constraint(equalTo: superView.centerYAnchor, constant:centeredVertically ).isActive = true
            }
            if let centeredHorizontally = centeredHorizontally{
                self.centerXAnchor.constraint(equalTo: superView.centerXAnchor, constant:centeredHorizontally ).isActive = true
            }
           
        }
       
    }
    
    func addShadow(location: VerticalLocation, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        switch location {
        case .bottom:
            addShadow(offset: CGSize(width: 0, height: 5), color: color, opacity: opacity, radius: radius)
        case .top:
            addShadow(offset: CGSize(width: 0, height: -1), color: color, opacity: opacity, radius: radius)
        }
    }
    
    func addShadow(offset: CGSize, color: UIColor = .clear, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    func createDottedLine(width: CGFloat, color: CGColor) {
        let caShapeLayer = CAShapeLayer()
        caShapeLayer.strokeColor = color
        caShapeLayer.lineWidth = width
        caShapeLayer.lineDashPattern = [2,3]
        let cgPath = CGMutablePath()
        let cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: self.frame.width, y: 0)]
        cgPath.addLines(between: cgPoint)
        caShapeLayer.path = cgPath
        layer.addSublayer(caShapeLayer)
    }
}
enum VerticalLocation: String {
    case bottom
    case top
}

extension UIImageView{
    func downloadImage(from url: URL,complition: @escaping (_ image: UIImage?)->Void) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else {
                complition(nil)
                return
                
            }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.async() { [weak self] in
                self?.image = UIImage(data: data)
                complition(UIImage(data:data))
            }
        }
    }
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        DispatchQueue.global(qos: .background).async {
            URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
        }
       
    }
}
extension UIImage {
    var noir: UIImage? {
        let context = CIContext(options: nil)
        guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        if let output = currentFilter.outputImage,
            let cgImage = context.createCGImage(output, from: output.extent) {
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        }
        
        return nil
    }
}
extension UIButton {
    
    func centerVertically(padding: CGFloat = 6.0) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
            return
        }
        
        let totalHeight = imageViewSize.height + titleLabelSize.height + padding
        
        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageViewSize.height),
            left: 0.0,
            bottom: 0.0,
            right: -titleLabelSize.width
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height),
            right: 0.0
        )
        
        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: titleLabelSize.height,
            right: 0.0
        )
    }
    
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        let isRTL = UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .rightToLeft
        if isRTL {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
            titleEdgeInsets = UIEdgeInsets(top: -insetAmount, left: 0, bottom: 0, right: insetAmount)
            contentEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: -insetAmount)
        } else {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
            titleEdgeInsets = UIEdgeInsets(top: insetAmount, left: 0, bottom: 0, right: -insetAmount)
            contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
        }
    }
    
}
extension CGFloat{
    //Base Design for Iphone 8 Plus
    static let factX = UIScreen.main.bounds.size.width/360
    static let factY = UIScreen.main.bounds.size.height/720
    static func calculatedHeight(_ height:CGFloat)->CGFloat{
        return height * factY
    }
    static func calculatedWidth(_ width:CGFloat)->CGFloat{
        return width * factX
    }
}

public enum BorderSide {
      case top, bottom, left, right
  }
extension UIView {
   
  public func addBorder(side: BorderSide, color: UIColor, width: CGFloat, leftPadding:CGFloat = 0, cornerRadious:CGFloat = 0 ) -> UIView {
        let border = UIView()
        border.translatesAutoresizingMaskIntoConstraints = false
        border.backgroundColor = color
      if cornerRadious > 0{
          border.layer.cornerRadius = cornerRadious
      }
        self.addSubview(border)

        let topConstraint = topAnchor.constraint(equalTo: border.topAnchor)
        let rightConstraint = trailingAnchor.constraint(equalTo: border.trailingAnchor)
        let bottomConstraint = bottomAnchor.constraint(equalTo: border.bottomAnchor)
        let leftConstraint = leadingAnchor.constraint(equalTo: border.leadingAnchor)
        let heightConstraint = border.heightAnchor.constraint(equalToConstant: width)
        let widthConstraint = border.widthAnchor.constraint(equalToConstant: width)


        switch side {
        case .top:
            NSLayoutConstraint.activate([leftConstraint, topConstraint, rightConstraint, heightConstraint])
        case .right:
            NSLayoutConstraint.activate([topConstraint, rightConstraint, bottomConstraint, widthConstraint])
        case .bottom:
            NSLayoutConstraint.activate([rightConstraint, bottomConstraint, leftConstraint, heightConstraint])
        case .left:
            NSLayoutConstraint.activate([bottomConstraint, leftConstraint, topConstraint, widthConstraint])
        }
      return border
    }
}
