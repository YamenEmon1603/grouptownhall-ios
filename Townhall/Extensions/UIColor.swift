//
//  UIColor.swift
//  ssl_commerz_revamp
//
//  Created by Mausum Nandy on 5/20/21.
//

import Foundation
import UIKit
extension UIColor {
    
    @nonobjc class var blueViolet: UIColor {
        return UIColor(red: 117.0 / 255.0, green: 0.0, blue: 234.0 / 255.0, alpha: 1.0)
        
    }
    @nonobjc class var pumpkinOrange: UIColor {
        return UIColor(red: 1.0, green: 127.0 / 255.0, blue: 11.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var marigold: UIColor {
        return UIColor(red: 1.0, green: 193.0 / 255.0, blue: 6.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleGrey: UIColor {
        return UIColor(red: 224.0 / 255.0, green: 219.0 / 255.0, blue: 229.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var Easywhite: UIColor {
        return UIColor(white: 245.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var slateGrey: UIColor {
        return UIColor(red: 99.0 / 255.0, green: 102.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightPeriwinkle: UIColor {
        return UIColor(red: 220.0 / 255.0, green: 222.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightPeriwinkleTwo: UIColor {
        return UIColor(red: 218.0 / 255.0, green: 218.0 / 255.0, blue: 225.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleGreyTwo: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 245.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var battleshipGrey: UIColor {
        return UIColor(red: 120.0 / 255.0, green: 120.0 / 255.0, blue: 136.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var hazel: UIColor {
        return UIColor(red: 139.0 / 255.0, green: 103.0 / 255.0, blue: 23.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleGreyThree: UIColor {
        return UIColor(red: 245.0 / 255.0, green: 247.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var azul: UIColor {
        return UIColor(red: 15.0 / 255.0, green: 98.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var darkGrey: UIColor {
        return UIColor(red: 31.0 / 255.0, green: 34.0 / 255.0, blue: 38.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var darkishBlue: UIColor {
        return UIColor.init(hex: "#004390") ?? .gray
    }
    @nonobjc class var iceBlue: UIColor {
        return UIColor.init(hex: "#edeff2") ?? .gray
    }
    @nonobjc class var paleViolet: UIColor {
        return UIColor(red: 208.0/255.0, green: 207.0 / 255.0, blue: 210 / 255.0, alpha: 1.0)
    }
    
    
    @nonobjc class var steel: UIColor {
        return UIColor.init(hex: "#7e8082") ?? .gray
    }
    
    @nonobjc class var whiteGrey: UIColor {
        return UIColor(red: 245.0 / 255.0, green: 245.0 / 255.0, blue: 245.0/255.0, alpha: 1.0)
    }
    @nonobjc class var borderColor: UIColor {
        return UIColor(red: 231.0 / 255.0, green: 232.0 / 255.0, blue: 234.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleLilac: UIColor {
        return UIColor(red: 224.0 / 255.0, green: 224.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var realBlue: UIColor {
        return UIColor(hex: "#0d47e5") ?? .blue
    }
    @nonobjc class var palePeach: UIColor {
        
        return UIColor(hex: "#ece1c3") ?? .orange
    }
    
    @nonobjc class var offWhite: UIColor {
        
        return UIColor(hex: "#f9f5ea") ?? .orange
    }
    @nonobjc class var gunMetal: UIColor {
        
        return UIColor(hex: "#47475a") ?? .gray
    }
    @nonobjc class var bloodRed: UIColor {
        return UIColor(red: 130.0 / 255.0, green: 62.0 / 255.0, blue: 79.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var easyMaron: UIColor {
        return UIColor(red: 120.0 / 255.0, green: 56.0 / 255.0, blue: 72.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var blueyGrey: UIColor {
        return UIColor(red: 159.0 / 255.0, green: 158.0 / 255.0, blue: 178.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coolGrey: UIColor {
        return UIColor(red: 166.0 / 255.0, green: 169.0 / 255.0, blue: 174.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var trueGreen: UIColor {
        return UIColor(red: 11.0 / 255.0, green: 165.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
    @nonobjc class var statusRed: UIColor {
        
        return UIColor(hex: "#F79321") ?? .gray
    }
    @nonobjc class var tomato: UIColor {
        return UIColor(red: 243.0 / 255.0, green: 33.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var purpleBrown: UIColor {
        return UIColor(red: 114.0 / 255.0, green: 52.0 / 255.0, blue: 68.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var easyWhite: UIColor {
        return UIColor(red: 245 / 255.0, green: 245.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var veryLightBlue: UIColor {
        return UIColor(red: 230.0 / 255.0, green: 232.0 / 255.0, blue: 237.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var veryLightPink: UIColor {
        return UIColor(red: 1.0, green: 237.0 / 255.0, blue: 237.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var paleGrey2: UIColor {
        return UIColor(red: 232.0 / 255.0, green: 241.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var paleBG: UIColor {
        return UIColor(white: 245.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var greyishBrown: UIColor {
        return UIColor(white: 64.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var sunYellow: UIColor {
        return UIColor(red: 1.0, green: 214.0 / 255.0, blue: 39.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var kelleyGreen: UIColor {
        return UIColor(red: 0.0, green: 127.0 / 255.0, blue: 54.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var orangeYellow: UIColor {
        return UIColor(red: 247.0 / 255.0, green: 161.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var clearBlue: UIColor {
        return UIColor(red: 23.0 / 255.0, green: 110.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
   
    @nonobjc class var lighterPurple1: UIColor {
        return UIColor(red: 140.0 / 255.0, green: 82.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var lighterPurple: UIColor {
        return UIColor(red: 147.0 / 255.0, green: 69.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    
    @nonobjc class var washedOutGreen: UIColor {
        return UIColor(red: 219.0 / 255.0, green: 248.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var jungleGreen: UIColor {
       return UIColor(red: 0.0, green: 114.0 / 255.0, blue: 75.0 / 255.0, alpha: 1.0)
     }
    @nonobjc class var rouge: UIColor {
      return UIColor(red: 149.0 / 255.0, green: 35.0 / 255.0, blue: 45.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightBurgundy: UIColor {
      return UIColor(red: 168.0 / 255.0, green: 50.0 / 255.0, blue: 60.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var jungleGreenTwo: UIColor {
      return UIColor(red: 0.0, green: 133.0 / 255.0, blue: 88.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var lightYellowGreen: UIColor {
      return UIColor(red: 208.0 / 255.0, green: 1.0, blue: 116.0 / 255.0, alpha: 1.0)
    }
}

extension UIColor {
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
