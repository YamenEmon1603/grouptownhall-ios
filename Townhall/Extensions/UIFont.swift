//
//  UIFont.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import UIKit
extension UIFont{
    enum FontStyle {
        case regular
        case black
        case light
        case medium_italic
        case medium
        case bold
        case thin
        case italic
        case boldItalic
    }
    static func EasyFont(ofSize size: CGFloat, style: FontStyle) -> UIFont{
        
        let calculatedSize = (size * UIScreen.main.bounds.height)/720//Constant.ratio
        switch style {
        case .regular:
            return UIFont(name: "Roboto-Regular", size: calculatedSize) ?? UIFont.systemFont(ofSize: size)
        case .medium:
            return UIFont(name: "Roboto-Medium", size: calculatedSize) ?? UIFont.boldSystemFont(ofSize: size)
        case .bold:
            return UIFont(name: "Roboto-Bold", size: calculatedSize) ?? UIFont.boldSystemFont(ofSize: size)
        case .thin:
            return UIFont(name: "Roboto-Thin", size: calculatedSize) ?? UIFont.systemFont(ofSize: size)
        case .italic:
            return UIFont(name: "Roboto-Italic", size: calculatedSize) ?? UIFont.italicSystemFont(ofSize: size)
        case .boldItalic:
            return UIFont(name: "Roboto-BoldItalic", size: calculatedSize) ?? UIFont.italicSystemFont(ofSize: size)
        case .black:
            return UIFont(name: "Roboto-Black", size: calculatedSize) ?? UIFont.italicSystemFont(ofSize: size)
        case .light:
            return UIFont(name: "Roboto-Light", size: calculatedSize) ?? UIFont.italicSystemFont(ofSize: size)
        case .medium_italic:
            return UIFont(name: "Roboto-MediumItalic", size: calculatedSize) ?? UIFont.italicSystemFont(ofSize: size)
        }
    }
}
extension SSLComLanguageHandler{
    func termsAndConditionsText() ->NSAttributedString{
        //let index = getCurrentLanguage()
       // let localisedStr = "by_clicking_proceed".easyLocalized()
        let attributedString = NSMutableAttributedString(string:  "by_clicking_proceed".easyLocalized(), attributes: [
            .font: UIFont.EasyFont(ofSize: 12.0, style: .regular),
          .foregroundColor: UIColor.slateGrey
        ])
        let str = NSString(string:  "by_clicking_proceed".easyLocalized())
        let range : NSRange =  str.range(of: "condition".easyLocalized())
       
        attributedString.addAttribute(.foregroundColor, value: UIColor.blueViolet, range: range )
       
        return attributedString
    }
}

