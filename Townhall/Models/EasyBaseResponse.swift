//
//  EasyBaseResponse.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 13/10/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation
class EasyBaseResponse:Codable{
    let ui: String?
    let message, status: String?
    let code: Int?
    let token: String?
    
    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case code = "code"
        case token = "token"
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.code = try values.decode(Int.self, forKey: .code)
        }catch{
            self.code = nil
        }
        do{
            self.ui = try values.decode(String.self, forKey: .ui)
        }catch{
            self.ui = nil
        }
        do{
            self.message = try values.decode(String.self, forKey: .message)
        }catch{
            self.message = nil
        }
        do{
            self.status = try values.decode(String.self, forKey: .status)
        }catch{
            self.status = nil
        }
        do{
            self.token = try values.decode(String.self, forKey: .token)
        }catch{
            self.token = nil
        }

        if self.code == 401{
            DispatchQueue.main.async {
                EasyUtils.shared.logout()
            }
            
        }
    }
    
}
