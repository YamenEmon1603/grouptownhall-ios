//
//  AutoDebitResponseModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 25/11/21.
//
import Foundation
// MARK: - AutoDebitResponseModel
class AutoDebitResponseModel: EasyBaseResponse {
   
    var data : AutoDebitResponse?
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(AutoDebitResponse.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}

// MARK: - DataClass
class AutoDebitResponse: Codable {
    let redirectGatewayURL: String?
       let isRedirectRequired: Bool?
       let sessionKey: String?
       let successURL, failURL, cancelURL: String?

       enum CodingKeys: String, CodingKey {
           case redirectGatewayURL = "redirect_gateway_url"
           case isRedirectRequired = "is_redirect_required"
           case sessionKey = "session_key"
           case successURL = "success_url"
           case failURL = "fail_url"
           case cancelURL = "cancel_url"
       }

       init(redirectGatewayURL: String?, isRedirectRequired: Bool?, sessionKey: String?, successURL: String?, failURL: String?, cancelURL: String?) {
           self.redirectGatewayURL = redirectGatewayURL
           self.isRedirectRequired = isRedirectRequired
           self.sessionKey = sessionKey
           self.successURL = successURL
           self.failURL = failURL
           self.cancelURL = cancelURL
       }
}
