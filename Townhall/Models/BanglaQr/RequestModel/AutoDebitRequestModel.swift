//
//  AutoDebitRequestModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 25/11/21.
//

import Foundation

class AutoDebitRequestModel: Codable {
    let action, channelType, channel, cardIndex, cvv, totalAmount, transId, serviceTitle:String?
    let isMobile : Int?
    
    enum CodingKeys: String, CodingKey {
        case action
        case channelType = "channel_type"
        case channel
        case cardIndex = "card_index"
        case cvv
        case totalAmount = "total_amount"
        case transId = "tran_id"
        case serviceTitle = "service_title"
        case isMobile = "is_mobile"
    }
    var dictionary: [String: Any]? {
       guard let data = try? JSONEncoder().encode(self) else { return nil }
       return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
     }
    init(action:String?, channelType:String?, channel:String?, cardIndex :String?, cvv :String?, totalAmount :String?, transId :String?, serviceTitle:String?) {
        self.action = action
        self.channelType = channelType
        self.channel = channel
        self.cardIndex = cardIndex
        self.cvv = cvv
        self.totalAmount = totalAmount
        self.transId = transId
        self.serviceTitle = serviceTitle
        self.isMobile = 1
    }
}
