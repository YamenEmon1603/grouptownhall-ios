//
//  DashboardContentResponseModel.swift
//  Townhall
//
//  Created by Mausum Nandy on 9/12/21.
//

import Foundation
class DashboardContentResponseModel: EasyBaseResponse {
    var data : DashboardContentResponse?
    enum CodingKeys: String, CodingKey {
        case data
    }
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(DashboardContentResponse.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}

// MARK: - DataClass
class DashboardContentResponse: Codable {
    let imageUploadStatus: Int?
    let townhall: DashboardContent?

    enum CodingKeys: String, CodingKey {
        case imageUploadStatus = "image_upload_status"
        case townhall
    }

    init(imageUploadStatus: Int?, townhall: DashboardContent?) {
        self.imageUploadStatus = imageUploadStatus
        self.townhall = townhall
    }
}

// MARK: - Townhall
class DashboardContent: Codable {
    let title, vanue, date, time: String?
    let address: String?
    let maxImageSize : String?
    let phone: [String]?
    let bot: Bot?
    let schedule: [Schedule]?
    let instructions: [Instruction]?

    enum CodingKeys: String, CodingKey {
        case title
        case vanue = "Vanue"
        case maxImageSize = "max_image_size"
        case date, time, address, phone, bot, schedule, instructions
    }

    init(title: String?, vanue: String?, date: String?, time: String?, address: String?,maxImageSize:String, phone: [String]?, bot: Bot?, schedule: [Schedule]?, instructions: [Instruction]?) {
        self.title = title
        self.vanue = vanue
        self.date = date
        self.time = time
        self.address = address
        self.phone = phone
        self.bot = bot
        self.schedule = schedule
        self.instructions = instructions
        self.maxImageSize = maxImageSize
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        // token
        do{
            self.maxImageSize = try values.decode(String.self, forKey: .maxImageSize)
        }catch{
            do{
                self.maxImageSize = try String(values.decode(Int.self, forKey: .maxImageSize))
            }catch{
                do{
                    self.maxImageSize = try String(values.decode(Double.self, forKey: .maxImageSize))
                }catch{
                    self.maxImageSize = nil
                }
            }
        }
        
  
        do{
            self.title = try values.decode(String.self, forKey: .title)
        }catch{
            self.title = nil
        }
        
        do{
            self.vanue = try values.decode(String.self, forKey: .vanue)
        }catch{
            self.vanue = nil
        }
        
        do{
            self.date = try values.decode(String.self, forKey: .date)
        }catch{
            self.date = nil
        }
        do{
            self.time = try values.decode(String.self, forKey: .time)
        }catch{
            self.time = nil
        }
        
        do{
            self.address = try values.decode(String.self, forKey: .address)
        }catch{
            self.address = nil
        }
        
        do{
            self.phone = try values.decode([String].self, forKey: .phone)
        }catch{
            self.phone = nil
        }
        
        do{
            self.bot = try values.decode(Bot.self, forKey: .bot)
        }catch{
            self.bot = nil
        }
        
        do{
            self.schedule = try values.decode([Schedule].self, forKey: .schedule)
        }catch{
            self.schedule = nil
        }
        
        do{
            self.instructions = try values.decode([Instruction].self, forKey: .instructions)
        }catch{
            self.instructions = nil
        }
        
       
    }
}

// MARK: - Bot
class Bot: Codable {
    let messenger, telegram, whatsapp: String?

    init(messenger: String?, telegram: String?, whatsapp: String?) {
        self.messenger = messenger
        self.telegram = telegram
        self.whatsapp = whatsapp
    }
}

// MARK: - Instruction
class Instruction: Codable {
    let button, title, instructionDescription: String?

    enum CodingKeys: String, CodingKey {
        case button, title
        case instructionDescription = "description"
    }

    init(button: String?, title: String?, instructionDescription: String?) {
        self.button = button
        self.title = title
        self.instructionDescription = instructionDescription
    }
}
// MARK: - Schedule
class Schedule: Codable {
    let time, title: String?

    init(time: String?, title: String?) {
        self.time = time
        self.title = title
    }
}
