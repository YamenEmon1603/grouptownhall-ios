//
//  ExperienceConentModel.swift
//  Townhall
//
//  Created by Raihan on 12/12/21.
//

import Foundation
class ExperienceConentModel: EasyBaseResponse {
    var data : ExperienceData?
    enum CodingKeys: String, CodingKey {
        case data
    }
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(ExperienceData.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}
// MARK: - DataClass
class ExperienceData: Codable {
    let imageUploadStatus: Int?
    let townhall: Townhall?

    enum CodingKeys: String, CodingKey {
        case imageUploadStatus = "image_upload_status"
        case townhall = "townhall"
    }

    init(imageUploadStatus: Int?, townhall: Townhall?) {
        self.imageUploadStatus = imageUploadStatus
        self.townhall = townhall
    }
}

// MARK: - Townhall
class Townhall: Codable {
    let items: [ExperienceItem]?

    enum CodingKeys: String, CodingKey {
        case items = "items"
    }

    init(items: [ExperienceItem]?) {
        self.items = items
    }
}

// MARK: - Item
class ExperienceItem: Codable {
    let title: String?
    let shortDescription: String?
    let background: String?
    let icon: String?
    let link: String?

    enum CodingKeys: String, CodingKey {
        case title = "title"
        case shortDescription = "short_description"
        case background = "background"
        case icon = "icon"
        case link = "link"
    }

    init(title: String?, shortDescription: String?, background: String?, icon: String?, link: String?) {
        self.title = title
        self.shortDescription = shortDescription
        self.background = background
        self.icon = icon
        self.link = link
    }
}
