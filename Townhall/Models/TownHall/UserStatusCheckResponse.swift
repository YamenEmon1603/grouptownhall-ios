//
//  UserStatusCheckResponse.swift
//  Townhall
//
//  Created by Mausum Nandy on 28/12/21.
//

import Foundation
class UserStatusCheckResponse : EasyBaseResponse{
    var data : UserUploadStatus?
    enum CodingKeys: String, CodingKey {
        case data
    }
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(UserUploadStatus.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}
// MARK: - DataClass
class UserUploadStatus: Codable {
    let imageUploadStatus: Bool?
    let userExist: Bool?

    enum CodingKeys: String, CodingKey {
        case imageUploadStatus = "image_upload_status"
        case userExist = "user_exist"
    }

    init(imageUploadStatus: Bool?, userExist: Bool?) {
        self.imageUploadStatus = imageUploadStatus
        self.userExist = userExist
    }
}
