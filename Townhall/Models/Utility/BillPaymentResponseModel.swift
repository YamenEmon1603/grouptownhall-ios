//
//  BillPaymentResponseModel.swift
//  Padma Bank
//
//  Created by Mausum Nandy on 4/27/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation

//MARK: - BillPaymentResponseModel
class BillPaymentResponseModel: EasyBaseResponse {
    
    var transData: BillTransactionInfo?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.transData = try values.decode(BillTransactionInfo.self, forKey: .data)
        }catch{
            self.transData = nil
        }
    }
   
}
// MARK: - BillTransactionInfo
class BillTransactionInfo: Codable {
    let transactionRef: String?
    let transactionId: Int?

    enum CodingKeys: String, CodingKey {
        case transactionRef = "transaction_ref"
        case transactionId = "transaction_id"
    }

    init(transactionRef: String?, transactionId: Int?) {
        self.transactionRef = transactionRef
        self.transactionId = transactionId
    }
}
