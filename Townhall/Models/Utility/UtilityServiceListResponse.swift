// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let utilityBillListResponse = try? newJSONDecoder().decode(UtilityBillListResponse.self, from: jsonData)

import Foundation
import UIKit
// MARK: - UtilityBillListResponse
class UtilityServiceListResponse: EasyBaseResponse {
    var  billData: [ServiceListData]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.billData = try values.decode([ServiceListData].self, forKey: .data)
        }catch{
            self.billData = nil
            print("error \(error.localizedDescription)" )
        }
    }}

// MARK: - Datum
class ServiceListData: Codable {
    let id: Int?
    let title, bnTitle, type, logo: String?
    let sequence: String?
    let commission: Int?
    let serviceLists: [ServiceList]?

    enum CodingKeys: String, CodingKey {
        case id, title
        case bnTitle = "bn_title"
        case type, logo, sequence, commission
        case serviceLists = "service_lists"
    }

    init(id: Int?, title: String?, bnTitle: String?, type: String?, logo: String?, sequence: String?, commission: Int?, serviceLists: [ServiceList]?) {
        self.id = id
        self.title = title
        self.bnTitle = bnTitle
        self.type = type
        self.logo = logo
        self.sequence = sequence
        self.commission = commission
        self.serviceLists = serviceLists
    }
}

// MARK: - ServiceList
class ServiceList: Codable {
    let id, serviceCategoryID: Int?
    let title, bnTitle: String?
    let logo: String?
    let sequence: String?
    let serviceCategory: ServiceCategory?
    let utilityBillTypes: [UtilityBillType]?

    enum CodingKeys: String, CodingKey {
        case id
        case serviceCategoryID = "service_category_id"
        case title
        case bnTitle = "bn_title"
        case logo, sequence
        case serviceCategory = "service_category"
        case utilityBillTypes = "utility_bill_types"
    }

    init(id: Int?, serviceCategoryID: Int?, title: String?, bnTitle: String?, logo: String?, sequence: String?, serviceCategory: ServiceCategory?, utilityBillTypes: [UtilityBillType]?) {
        self.id = id
        self.serviceCategoryID = serviceCategoryID
        self.title = title
        self.bnTitle = bnTitle
        self.logo = logo
        self.sequence = sequence
        self.serviceCategory = serviceCategory
        self.utilityBillTypes = utilityBillTypes
    }
}

// MARK: - ServiceCategory
class ServiceCategory: Codable {
    let id: Int?
    let title, bnTitle: String?
    let logo: String?

    enum CodingKeys: String, CodingKey {
        case id, title
        case bnTitle = "bn_title"
        case logo
    }

    init(id: Int?, title: String?, bnTitle: String?, logo: String?) {
        self.id = id
        self.title = title
        self.bnTitle = bnTitle
        self.logo = logo
    }
}

// MARK: - UtilityBillType
class UtilityBillType: Codable {
    let id, serviceListID: Int?
    let title, bnTitle: String?
    let sequence: String?
    let serviceList: ServiceCategory?
    let parameterLists: [ParameterList]?

    enum CodingKeys: String, CodingKey {
        case id
        case serviceListID = "service_list_id"
        case title
        case bnTitle = "bn_title"
        case sequence
        case serviceList = "service_list"
        case parameterLists = "parameter_lists"
    }

    init(id: Int?, serviceListID: Int?, title: String?, bnTitle: String?, sequence: String?, serviceList: ServiceCategory?, parameterLists: [ParameterList]?) {
        self.id = id
        self.serviceListID = serviceListID
        self.title = title
        self.bnTitle = bnTitle
        self.sequence = sequence
        self.serviceList = serviceList
        self.parameterLists = parameterLists
    }
}

// MARK: - ParameterList
class ParameterList: Codable {
    let id, utilityBillID: Int?
    let fieldName, level, bnLevel: String?
    let type: String?
    let parameterListRequired: Required?
    let dataType: DataType?
    let options: String?
    let sequence: String?
    let parameterListDefault: String?
    let status: String?
    let utility: ServiceCategory?
    let format : String?
    var selectOptions : [SelectOptionItem]?{
        get{
            if let options = options {
                let str = options.replacingOccurrences(of: "\\", with: "")
              
                if let data = str.data(using: .utf8){
                    do {
                       return try JSONDecoder().decode([SelectOptionItem].self, from: data)
                    } catch  {
                        print(error)
                       return nil
                    }
                    
                }
               
            }
            return nil
        }
    }
    enum CodingKeys: String, CodingKey {
        case id
        case utilityBillID = "utility_bill_id"
        case fieldName = "field_name"
        case level
        case bnLevel = "bn_level"
        case type
        case parameterListRequired = "required"
        case dataType = "data_type"
        case options, sequence
        case parameterListDefault = "default"
        case status, utility,format
    }

    init(id: Int?, utilityBillID: Int?, fieldName: String?, level: String?, bnLevel: String?, type: String?, parameterListRequired: Required?, dataType: DataType?, options: String?, sequence: String?, parameterListDefault: String?, status: String?, utility: ServiceCategory?,format : String?) {
        self.id = id
        self.utilityBillID = utilityBillID
        self.fieldName = fieldName
        self.level = level
        self.bnLevel = bnLevel
        self.type = type
        self.parameterListRequired = parameterListRequired
        self.dataType = dataType
        self.options = options
        self.sequence = sequence
        self.parameterListDefault = parameterListDefault
        self.status = status
        self.utility = utility
        self.format = format
      
    }
}

enum DataType: String, Codable {
    case alphanumeric = "alphanumeric"
    case number = "number"
    case text = "text"
    
    var keyboardType: UIKeyboardType {
        switch self {
        case .alphanumeric:
            return .numbersAndPunctuation
        case .number:
            return .numberPad
        case .text:
            return .default
        }
    }
}

enum Required: String, Codable {
    case requiredOptional = "optional"
    case requiredRequired = "required"
}

enum TypeEnum: String, Codable {
    case select = "select"
    case text = "text"
    case date = "date"
    case hidden = "hidden"
}
class SelectOptionItem: Codable {
    let name, value: String?
    var isSelected: Bool?
    init(name: String?, value: String?) {
        self.name = name
        self.value = value
        self.isSelected = false
    }
}
