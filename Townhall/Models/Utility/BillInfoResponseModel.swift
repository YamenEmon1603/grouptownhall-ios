//
//  BillInfoResponseModel.swift
//  Padma Bank
//
//  Created by Mausum Nandy on 4/27/21.
//  Copyright © 2021 SSL Wireless. All rights reserved.
//

import Foundation



// MARK: - BillInfoResponseModel
class BillInfoResponseModel: EasyBaseResponse {
    
    var billData: BillInfo?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.billData = try values.decode(BillInfo.self, forKey: .data)
        }catch{
            self.billData = nil
        }
    }
   
}



// MARK: - BillInfo
class BillInfo: Codable {
    let status: String?
    let statusCode: String?
    let statusTitle: String?
    let lid: String?
    let transactionId: String?
    let bill: [SummeryRowData]?
    let billPayId: Int?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case statusCode = "status_code"
        case statusTitle = "status_title"
        case lid = "lid"
        case transactionId = "transaction_id"
        case bill = "data"
        case billPayId = "bill_pay_id"
    }

    init(status: String?, statusCode: String?, statusTitle: String?, lid: String?, transactionId: String?, bill:[SummeryRowData]?, billPayId: Int?) {
        self.status = status
        self.statusCode = statusCode
        self.statusTitle = statusTitle
        self.lid = lid
        self.transactionId = transactionId
        self.bill = bill
        self.billPayId = billPayId
    }
}

// MARK: - ActualBill
class ActualBill: Codable {
    let accountNumber: String?
    let billNumber: String?
    let zoneCode: String?
    let dueDate: String?
    let organizationCode: Int?
    let tariff: String?
    let settlementAccount: String?
    let billAmount: String?
    let vatAmount: String?
    let lpcAmount: String?
    let stampAmount: String?
    let totalAmount: String?
    let billMonth:String?
    enum CodingKeys: String, CodingKey {
        case accountNumber = "account_number"
        case billNumber = "bill_number"
        case zoneCode = "zone_code"
        case dueDate = "due_date"
        case organizationCode = "organization_code"
        case tariff = "tariff"
        case settlementAccount = "settlement_account"
        case billAmount = "bill_amount"
        case vatAmount = "vat_amount"
        case lpcAmount = "lpc_amount"
        case stampAmount = "stamp_amount"
        case totalAmount = "total_amount"
        case billMonth = "bill_month"
    }

  
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.accountNumber = try values.decode(String.self, forKey: .accountNumber)
        }catch{
            self.accountNumber  = nil
        }
        do{
            self.billNumber = try values.decode(String.self, forKey: .billNumber)
        }catch{
            self.billNumber  = nil
        }
        do{
            self.zoneCode = try values.decode(String.self, forKey: .zoneCode)
        }catch{
            self.zoneCode  = nil
        }
        do{
            self.dueDate = try values.decode(String.self, forKey: .dueDate)
        }catch{
            self.dueDate  = nil
        }
        do{
            self.organizationCode = try values.decode(Int.self, forKey: .organizationCode)
        }catch{
            self.organizationCode  = 0
        }
        do{
            self.tariff = try values.decode(String.self, forKey: .tariff)
        }catch{
            self.tariff  = nil
        }
        do{
            self.settlementAccount = try values.decode(String.self, forKey: .settlementAccount)
        }catch{
            self.settlementAccount  = nil
        }
        do{
            self.billAmount = try values.decode(String.self, forKey: .billAmount)
        }catch{
            self.billAmount  = nil
        }
        do{
            self.vatAmount = try values.decode(String.self, forKey: .vatAmount)
        }catch{
            self.vatAmount  = nil
        }
        do{
            self.lpcAmount = try values.decode(String.self, forKey: .lpcAmount)
        }catch{
            self.lpcAmount  = nil
        }
        do{
            self.stampAmount = try values.decode(String.self, forKey: .stampAmount)
        }catch{
            self.stampAmount  = nil
        }
        do{
            self.totalAmount = try values.decode(String.self, forKey: .totalAmount)
        }catch{
            self.totalAmount  = nil
        }
        do{
            self.billMonth = try values.decode(String.self, forKey: .billMonth)
        }catch{
            self.billMonth  = nil
        }
    }
}
class SummeryRowData: Codable {
    let isAmountField: Bool?
    let key, value: String?

    enum CodingKeys: String, CodingKey {
        case isAmountField = "is_amount_field"
        case key, value
    }

    init(isAmountField: Bool?, key: String?, value: String?) {
        self.isAmountField = isAmountField
        self.key = key
        self.value = value
    }
}
