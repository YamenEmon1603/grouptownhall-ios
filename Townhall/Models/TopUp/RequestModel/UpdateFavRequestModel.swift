//
//  addToFavRequestModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 27/10/21.
//

import Foundation



class UpdateFavRequestModel : Codable {
    
    let phone : String?
    let name : String?
    let connectionType : String?
    let operatorId : Int?
   
    enum CodingKeys: String, CodingKey {
        case phone,name
        case connectionType = "connection_type"
        case operatorId = "operator_id"
    }
    
    init(name:String, phone:String,connectionType:String,operatorId:Int) {
        self.name = name
        self.phone = phone
        self.connectionType = connectionType
        self.operatorId = operatorId
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.phone, forKey: .phone)
        try container.encode(self.connectionType, forKey: .connectionType)
        try container.encode(self.operatorId, forKey: .operatorId)
    }
}
