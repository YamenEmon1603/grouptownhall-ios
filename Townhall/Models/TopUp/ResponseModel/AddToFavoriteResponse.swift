//
//  File.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 26/10/21.
//

import Foundation

 
class AddToFavoriteResponse: EasyBaseResponse {
    var  fevData: [String]?


    enum CodingKeys: String, CodingKey {
        case fevData = "data"

    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do {
            self.fevData = try values.decode([String].self, forKey: .fevData)
        }catch{
            self.fevData = nil
        }
       
       

    }
}
