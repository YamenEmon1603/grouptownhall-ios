//
//  GetFavoriteNumberResponse.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 2/27/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation

// MARK: - GetFavoriteNumberResponse
class GetFavoriteNumberResponse: EasyBaseResponse {
    var  data: [FevoriteContact]?
   

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode([FevoriteContact].self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}

// MARK: - FevoriteContact
class FevoriteContact: Codable {
    let phone: String?
    let name: String?
    let connectionType: String?
    let operatorId: Int?

    enum CodingKeys: String, CodingKey {
        case phone = "phone"
        case name = "name"
        case connectionType = "connection_type"
        case operatorId = "operator_id"
    }

    init(phone: String?, name: String?, connectionType: String?, operatorId: Int?) {
        self.phone = phone
        self.name = name
        self.connectionType = connectionType
        self.operatorId = operatorId
    }
    var _operator:MBOperator?{
        get{
            return EasyManager.dataStore.easyMobileOperators.first(where: {$0.operatorId == operatorId})
        }
    }
}
