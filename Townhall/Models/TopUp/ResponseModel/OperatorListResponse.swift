//
//  OperatorListResponse.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 2/27/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation
import UIKit
// MARK: - OperatorListResponse
class OperatorListResponse: Codable {
    let ui: String?
    let message: String?
    let status: String?
    let data: [MBOperator]?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case data = "data"
        case code = "code"
    }

    init(ui: String?, message: String?, status: String?, data: [MBOperator]?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.data = data
        self.code = code
    }
}

// MARK: - Operator
class MBOperator:  NSObject, Codable, NSCoding {
    
    
    var operatorId: Int?
    let operatorName: String?
    let operatorNameBn : String?
    let operatorShortName: String?
    let imageUrl: String?
    let operatorPrefix: String?
    let prepaidLowLimit: Int?
    let prepaidHighLimit: Int?
    let postpaidLowLimit: Int?
    let postpaidHighLimit: Int?
    var logoImage:UIImage?

    enum CodingKeys: String, CodingKey {
        case operatorId = "operator_id"
        case operatorName = "operator_name"
        case operatorShortName = "operator_short_name"
        case imageUrl = "image_url"
        case operatorPrefix = "prefix"
        case prepaidLowLimit = "prepaid_low_limit"
        case prepaidHighLimit = "prepaid_high_limit"
        case postpaidLowLimit = "postpaid_low_limit"
        case postpaidHighLimit = "postpaid_high_limit"
        case operatorNameBn = "operator_name_bn"
    }

    init(operatorId: Int?, operatorName: String?, operatorNameBn : String?, operatorShortName: String?, imageUrl: String?, operatorPrefix: String?, prepaidLowLimit: Int?, prepaidHighLimit: Int?, postpaidLowLimit: Int?, postpaidHighLimit: Int?) {
        self.operatorId = operatorId
        self.operatorName = operatorName
        self.operatorShortName = operatorShortName
        self.imageUrl = imageUrl
        self.operatorPrefix = operatorPrefix
        self.prepaidLowLimit = prepaidLowLimit
        self.prepaidHighLimit = prepaidHighLimit
        self.postpaidLowLimit = postpaidLowLimit
        self.postpaidHighLimit = postpaidHighLimit
        self.operatorNameBn = operatorNameBn
       
    }
    func encode(with coder: NSCoder) {
        coder.encode(operatorId, forKey: "operator_id")
        coder.encode(operatorName, forKey: "operator_name")
        coder.encode(operatorShortName, forKey: "operator_short_name")
        coder.encode(imageUrl, forKey: "image_url")
        coder.encode(operatorPrefix, forKey: "prefix")
        coder.encode(prepaidLowLimit, forKey: "prepaid_low_limit")
        coder.encode(prepaidHighLimit, forKey: "prepaidHighLimit")
        coder.encode(postpaidLowLimit, forKey: "postpaid_low_limit")
        coder.encode(postpaidHighLimit, forKey: "postpaid_high_limit")
        coder.encode(operatorNameBn, forKey: "operator_name_bn")
    }
    
    required convenience init?(coder: NSCoder) {
        let operatorId = coder.decodeObject(forKey: "operator_id") as? Int
        let operatorName = coder.decodeObject(forKey: "operator_name") as? String
        let operatorShortName = coder.decodeObject(forKey: "operator_short_name") as? String
        let imageUrl = coder.decodeObject(forKey: "image_url") as? String
        let operatorPrefix = coder.decodeObject(forKey: "prefix") as? String
        let prepaidLowLimit = coder.decodeObject(forKey: "prepaid_low_limit") as? Int
        let prepaidHighLimit = coder.decodeObject(forKey: "prepaidHighLimit") as? Int
        let postpaidLowLimit = coder.decodeObject(forKey: "postpaid_low_limit") as? Int
        let postpaidHighLimit = coder.decodeObject(forKey: "postpaid_high_limit") as? Int
        let operatorNameBn = coder.decodeObject(forKey: "operator_name_bn") as? String
        
        self.init(operatorId: operatorId, operatorName: operatorName, operatorNameBn : operatorNameBn, operatorShortName: operatorShortName, imageUrl: imageUrl, operatorPrefix: operatorPrefix, prepaidLowLimit: prepaidLowLimit, prepaidHighLimit: prepaidHighLimit, postpaidLowLimit: postpaidLowLimit, postpaidHighLimit: postpaidHighLimit)
    }
    var localisedName:String?{
        get{
           return SSLComLanguageHandler.sharedInstance.getCurrentLanguage() == .Bangla ? operatorNameBn : operatorName
        }
    }
}
