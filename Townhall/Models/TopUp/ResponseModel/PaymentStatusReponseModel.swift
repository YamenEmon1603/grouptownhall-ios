//
//  PaymentStatusResponseModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 10/11/21.
//

import Foundation

class PaymentStatusReponseModel: EasyBaseResponse {
    var data : PaymentStatusData?
    enum CodingKeys: String, CodingKey {
        case data
    }
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(PaymentStatusData.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}
// MARK: - DataClass
class PaymentStatusData: Codable {
    let paymentStatus: String?
    // let serviceStatus: S?
    let paidAmount, bonusAmount: Int?
    let serviceScheduleAt, rechargeCompletedAt, serviceCompletedAt: String?
    let status, orderData: String?
    let rating: Int?
    var orderDataObject :PaymentStatusResponseElement?
    enum CodingKeys: String, CodingKey {
        case paymentStatus = "payment_status"
        // case serviceStatus = "service_status"
        case paidAmount = "paid_amount"
        case bonusAmount = "bonus_amount"
        case serviceScheduleAt = "service_schedule_at"
        case rechargeCompletedAt = "recharge_completed_at"
        case serviceCompletedAt = "service_completed_at"
        case status
        case orderData = "order_data"
        case rating
    }
    
    init(paymentStatus: String?, paidAmount: Int?, bonusAmount: Int?, serviceScheduleAt: String?, rechargeCompletedAt: String?, serviceCompletedAt: String?, status: String?, orderData: String?, rating: Int?) {
        self.paymentStatus = paymentStatus
        //self.serviceStatus = serviceStatus
        self.paidAmount = paidAmount
        self.bonusAmount = bonusAmount
        self.serviceScheduleAt = serviceScheduleAt
        self.rechargeCompletedAt = rechargeCompletedAt
        self.serviceCompletedAt = serviceCompletedAt
        self.status = status
        self.orderData = orderData
        self.rating = rating
       
    }
    //    required init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        self.paymentStatus = try values.decode(String.self, forKey: .paymentStatus)
    //        self.paidAmount = try values.decode(Int.self, forKey: .paidAmount)
    //        self.bonusAmount = try values.decode(Int.self, forKey: .bonusAmount)
    //        self.serviceScheduleAt = try values.decode(String.self, forKey: .serviceScheduleAt)
    //        self.rechargeCompletedAt = try values.decode(String.self, forKey: .rechargeCompletedAt)
    //        self.serviceCompletedAt = try values.decode(String.self, forKey: .serviceCompletedAt)
    //        self.status = try values.decode(String.self, forKey: .status)
    //        self.orderData = try values.decode(String.self, forKey: .orderData)
    //        self.rating = try values.decode(Int.self, forKey: .rating)
    //        do{
    //             let strData = try values.decode(String.self, forKey: .orderData)
    //            self.orderDataObject = try JSONDecoder().decode([PaymentStatusResponseElement].self, from: Data(strData.utf8))
    //        }catch{
    //            self.orderDataObject = nil
    //        }
    //
    //    }
}
// MARK: - PaymentStatusResponseElement
class PaymentStatusResponseElement: Codable {
    let amount, paymentStatusResponseOperator, phone, type,bonusPercentage, bonusAmount,status, guid, transactionID, callbackURL,campaignName, offerDetails, vrGUID, allowOffer: String?
  
    
    enum CodingKeys: String, CodingKey {
        case amount
        case paymentStatusResponseOperator = "operator"
        case phone, type, status, guid
        case transactionID = "transaction_id"
        case callbackURL = "callback_url"
        case bonusPercentage = "bonus_percentage"
        case bonusAmount = "bonus_amount"
        case campaignName = "campaign_name"
        case offerDetails = "offer_details"
        case vrGUID = "vr_guid"
        case allowOffer = "allow_offer"
    }
    
   
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.amount = try values.decode(String.self, forKey: .amount)
        }catch{
            do{
                self.amount = try String(values.decode(Int.self, forKey: .amount))
            }catch{
                do{
                    self.amount = try String(values.decode(Double.self, forKey: .amount))
                }catch{
                    self.amount = nil
                }
            }
        }
       
        
        
        
        self.paymentStatusResponseOperator = try values.decode(String.self, forKey: .paymentStatusResponseOperator)
        self.phone = try values.decode(String.self, forKey: .phone)
        self.type = try values.decode(String.self, forKey: .type)
        self.status = try values.decode(String.self, forKey: .status)
        self.guid = try values.decode(String.self, forKey: .guid)
        self.transactionID = try values.decode(String.self, forKey: .transactionID)
        self.callbackURL = try values.decode(String.self, forKey: .callbackURL)
       
        do{
            self.bonusPercentage = try values.decode(String.self, forKey: .bonusPercentage)
        }catch{
            do{
                self.bonusPercentage = try String(values.decode(Int.self, forKey: .bonusPercentage))
            }catch{
                do{
                    self.bonusPercentage = try String(values.decode(Double.self, forKey: .bonusPercentage))
                }catch{
                    self.bonusPercentage = nil
                }
            }
        }
       
        do{
            self.bonusAmount = try values.decode(String.self, forKey: .bonusAmount)
        }catch{
            do{
                self.bonusAmount = try String(values.decode(Int.self, forKey: .bonusAmount))
            }catch{
                do{
                    self.bonusAmount = try String(values.decode(Double.self, forKey: .bonusAmount))
                }catch{
                    self.bonusAmount = nil
                }
            }
        }
        
//        self.bonusAmount = try values.decode(String.self, forKey: .bonusAmount)
        
        
        self.campaignName = try values.decode(String.self, forKey: .campaignName)
        self.offerDetails = try values.decode(String.self, forKey: .offerDetails)
        self.vrGUID = try values.decode(String.self, forKey: .vrGUID)
        self.allowOffer = try values.decode(String.self, forKey: .allowOffer)
        
    }
}


