//
//  RechargePlanResponse.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 3/1/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation


class RechargePlanResponse: EasyBaseResponse {
    let data: [RechargePlan]?
    

    enum CodingKeys: String, CodingKey {
       
        case data = "data"
    
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try values.decode([RechargePlan].self, forKey: .data)
        try super.init(from: decoder)
       
    }
    
}

// MARK: - RechargePlan
class RechargePlan: NSObject, Codable, NSCoding{
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey:CodingKeys.id.rawValue)
        coder.encode(offerTitle, forKey:CodingKeys.offerTitle.rawValue)
        coder.encode(offerTitleBn, forKey:CodingKeys.offerTitleBn.rawValue)
        coder.encode(operatorName, forKey:CodingKeys.operatorName.rawValue)
        coder.encode(isForPostpaid, forKey:CodingKeys.isForPostpaid.rawValue)
        coder.encode(isForPrepaid, forKey:CodingKeys.isForPrepaid.rawValue)
        coder.encode(amountMin, forKey:CodingKeys.amountMin.rawValue)
        coder.encode(amountMax, forKey:CodingKeys.amountMax.rawValue)
        coder.encode(operatorShortName, forKey:CodingKeys.operatorShortName.rawValue)
        coder.encode(priority, forKey:CodingKeys.priority.rawValue)
        coder.encode(logTime, forKey:CodingKeys.logTime.rawValue)
        coder.encode(offerExpireTime, forKey:CodingKeys.offerExpireTime.rawValue)
        coder.encode(offerType, forKey:CodingKeys.offerType.rawValue)
        coder.encode(rechargePlanDescription, forKey:CodingKeys.rechargePlanDescription.rawValue)
        coder.encode(rank, forKey:CodingKeys.rank.rawValue)
        coder.encode(ribbon, forKey:CodingKeys.ribbon.rawValue)
        coder.encode(ribbonBn, forKey:CodingKeys.ribbonBn.rawValue)
        coder.encode(validity, forKey:CodingKeys.validity.rawValue)
        coder.encode(validityBn, forKey:CodingKeys.validityBn.rawValue)
        coder.encode(isPopular, forKey:CodingKeys.isPopular.rawValue)
        coder.encode(points, forKey:CodingKeys.points.rawValue)
        coder.encode(usageCnt, forKey:CodingKeys.usageCnt.rawValue)
        coder.encode(status, forKey:CodingKeys.status.rawValue)
        coder.encode(createdAt, forKey:CodingKeys.createdAt.rawValue)
        coder.encode(updatedAt, forKey:CodingKeys.updatedAt.rawValue)
   
    }
    
    
    let id: Int?
       let offerTitle: String?
    let offerTitleBn : String?
       let operatorName: String?
       let isForPostpaid: Int?
       let isForPrepaid: Int?
       let amountMin: Int?
       let amountMax: Int?
       let operatorShortName: String?
       let priority: Int?
       let logTime: String?
       let offerExpireTime: String?
       let offerType: String?
       let rechargePlanDescription: String?
       let offerDetailsUrl: String?
       let rank: Int?
       let ribbon: String?
       let ribbonBn: String?
       let validity: String?
       let validityBn: String?
       let isPopular: Int?
       let points: Int?
       let usageCnt: Int?
       let status: Int?
       let createdAt: String?
       let updatedAt: String?

    required init?(coder: NSCoder) {
        self.id = coder.decodeObject(forKey: CodingKeys.id.rawValue) as? Int
        self.offerTitle = coder.decodeObject(forKey: CodingKeys.offerTitle.rawValue) as? String
        self.offerTitleBn = coder.decodeObject(forKey: CodingKeys.offerTitleBn.rawValue) as? String
        self.operatorName = coder.decodeObject(forKey:CodingKeys.operatorName.rawValue) as? String
        self.isForPostpaid = coder.decodeObject(forKey: CodingKeys.isForPostpaid.rawValue) as? Int
        self.isForPrepaid = coder.decodeObject(forKey: CodingKeys.isForPrepaid.rawValue) as? Int
        self.amountMin = coder.decodeObject(forKey: CodingKeys.amountMin.rawValue) as? Int
        
        self.amountMax = coder.decodeObject(forKey: CodingKeys.amountMax.rawValue) as? Int
        self.operatorShortName = coder.decodeObject(forKey: CodingKeys.operatorShortName.rawValue) as? String
        self.priority = coder.decodeObject(forKey:CodingKeys.priority.rawValue) as? Int
        self.logTime = coder.decodeObject(forKey: CodingKeys.logTime.rawValue) as? String
        self.offerExpireTime = coder.decodeObject(forKey: CodingKeys.offerExpireTime.rawValue) as? String
        self.offerType = coder.decodeObject(forKey: CodingKeys.offerType.rawValue) as? String
        
        self.rechargePlanDescription = coder.decodeObject(forKey:CodingKeys.rechargePlanDescription.rawValue) as? String
        self.offerDetailsUrl = coder.decodeObject(forKey: CodingKeys.offerDetailsUrl.rawValue) as? String
        self.rank = coder.decodeObject(forKey: CodingKeys.rank.rawValue) as? Int
        self.ribbon = coder.decodeObject(forKey: CodingKeys.ribbon.rawValue) as? String
        self.ribbonBn = coder.decodeObject(forKey: CodingKeys.ribbonBn.rawValue) as? String
        self.validity = coder.decodeObject(forKey: CodingKeys.validity.rawValue) as? String
        self.validityBn = coder.decodeObject(forKey: CodingKeys.validityBn.rawValue) as? String
        self.isPopular = coder.decodeObject(forKey: CodingKeys.isPopular.rawValue) as? Int
        self.points = coder.decodeObject(forKey:CodingKeys.points.rawValue) as? Int
        self.usageCnt = coder.decodeObject(forKey: CodingKeys.usageCnt.rawValue) as? Int
        self.status = coder.decodeObject(forKey: CodingKeys.status.rawValue) as? Int
        self.createdAt = coder.decodeObject(forKey: CodingKeys.createdAt.rawValue) as? String
        self.updatedAt = coder.decodeObject(forKey: CodingKeys.updatedAt.rawValue) as? String
    }
    
    
    
       enum CodingKeys: String, CodingKey {
           case id = "id"
           case offerTitle = "offer_title"
           case offerTitleBn = "offer_title_bn"
           case operatorName = "operator_name"
           case isForPostpaid = "is_for_postpaid"
           case isForPrepaid = "is_for_prepaid"
           case amountMin = "amount_min"
           case amountMax = "amount_max"
           case operatorShortName = "operator_short_name"
           case priority = "priority"
           case logTime = "log_time"
           case offerExpireTime = "offer_expire_time"
           case offerType = "offer_type"
           case rechargePlanDescription = "description"
           case offerDetailsUrl = "offer_details_url"
           case rank = "rank"
           case ribbon = "ribbon"
           case ribbonBn = "ribbon_bn"
           case validity = "validity"
           case validityBn = "validity_bn"
           case isPopular = "is_popular"
           case points = "points"
           case usageCnt = "usage_cnt"
           case status = "status"
           case createdAt = "created_at"
           case updatedAt = "updated_at"
       }

       init(id: Int?, offerTitle: String?,offerTitleBn: String?, operatorName: String?, isForPostpaid: Int?, isForPrepaid: Int?, amountMin: Int?, amountMax: Int?, operatorShortName: String?, priority: Int?, logTime: String?, offerExpireTime: String?, offerType: String?, rechargePlanDescription: String?, offerDetailsUrl: String?, rank: Int?, ribbon: String?,ribbonBn: String?, validity: String?,validityBn: String?, isPopular: Int?, points: Int?, usageCnt: Int?, status: Int?, createdAt: String?, updatedAt: String?) {
           self.id = id
           self.offerTitle = offerTitle
           self.offerTitleBn = offerTitleBn
           self.operatorName = operatorName
           self.isForPostpaid = isForPostpaid
           self.isForPrepaid = isForPrepaid
           self.amountMin = amountMin
           self.amountMax = amountMax
           self.operatorShortName = operatorShortName
           self.priority = priority
           self.logTime = logTime
           self.offerExpireTime = offerExpireTime
           self.offerType = offerType
           self.rechargePlanDescription = rechargePlanDescription
           self.offerDetailsUrl = offerDetailsUrl
           self.rank = rank
           self.ribbon = ribbon
           self.ribbonBn = ribbonBn
           self.validity = validity
           self.validityBn = validityBn
           self.isPopular = isPopular
           self.points = points
           self.usageCnt = usageCnt
           self.status = status
           self.createdAt = createdAt
           self.updatedAt = updatedAt
       }
    
}



enum OfferType: String, Codable {
    case internet = "internet"
    case internetVoice = "internet+voice"
    case internetVoiceSms = "internet+voice+sms"
    case voice = "voice"
    case voiceInternet = "voice+internet"
    case voiceInternetSms = "voice+internet+sms"
    case voiceSms = "voice+sms"
    case voiceSmsInternet = "voice+sms+internet"
}

enum OperatorName: String, Codable {
    case airtelBangladesh = "Airtel Bangladesh"
    case banglalink = "Banglalink"
    case grameenPhone = "GrameenPhone"
    case robiTelecom = "Robi Telecom"
    case teletalk = "Teletalk"
}

enum OperatorShortName: String, Codable {
    case ab = "AB"
    case bl = "BL"
    case gp = "GP"
    case rb = "RB"
    case tt = "TT"
}

