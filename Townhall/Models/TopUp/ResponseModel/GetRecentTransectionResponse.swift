//
//  GetRecentTransectionResponse.swift
//  Easy.com.bd
//
//  Created by Mausum Nandy on 2/27/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation


// MARK: - GetRecentTransectionResponse
class GetRecentTransectionResponse: EasyBaseResponse {
    var data: [RecentTransaction]?
   
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode([RecentTransaction].self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}

// MARK: - RecentTransaction
class RecentTransaction: Codable {
    let operatorId: Int?
    let phone: String?
    let connectionType: String?
    let amount: Int?
    let status: String?
    let date: String?

    enum CodingKeys: String, CodingKey {
        case operatorId = "operator_id"
        case phone = "phone"
        case connectionType = "connection_type"
        case amount = "amount"
        case status = "status"
        case date = "date"
    }

    init(operatorId: Int?, phone: String?, connectionType: String?, amount: Int?, status: String?, date: String?) {
        self.operatorId = operatorId
        self.phone = phone
        self.connectionType = connectionType
        self.amount = amount
        self.status = status
        self.date = date
    }
    var _operator:MBOperator?{
        get{
            return EasyManager.dataStore.easyMobileOperators.first(where: {$0.operatorId == operatorId})
        }
    }
}

