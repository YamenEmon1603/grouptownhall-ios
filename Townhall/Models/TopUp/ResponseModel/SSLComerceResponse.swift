//
//  SSLResponse.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 10/11/21.
//


import Foundation

// MARK: - TopUpResponse
class SSLComerceResponse: Codable {
    let ui: String?
    let message: String?
    let status: String?
    let sslData: SSLComerceData?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case sslData = "data"
        case code = "code"
    }

    init(ui: String?, message: String?, status: String?, sslData: SSLComerceData?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.sslData = sslData
        self.code = code
    }
}

// MARK: - TopupData
class SSLComerceData: Codable {
    let storeId: String?
    let storePassword: String?
    let amount: Double?
    let transactionId: String?
    let serviceTitle: String?
    let ipnUrl: String?
    let valueB : String?
    let valueC: String?
    let userRefer:String?
    enum CodingKeys: String, CodingKey {
        case storeId = "store_id"
        case storePassword = "store_password"
        case amount = "amount"
        case transactionId = "transaction_id"
        case serviceTitle = "service_title"
        case ipnUrl = "ipn_url"
        case valueC = "value_c"
        case valueB = "value_b" //orderInfo
        case userRefer = "user_refer"
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.storeId = try container.decode(String.self, forKey: .storeId)
        self.storePassword = try container.decode(String.self, forKey: .storePassword)
        do {
            amount = try Double(container.decode(String.self, forKey: .amount))
        } catch DecodingError.typeMismatch {
            do{
                amount = try Double(container.decode(Int.self, forKey: .amount))
            }catch DecodingError.typeMismatch{
                amount = try container.decode(Double.self, forKey: .amount)
            }
            
        }
        do{
            self.valueC = try container.decode(String.self, forKey: .valueC)
        }catch{
            self.valueC = nil
        }
        do{
            self.valueB = try container.decode(String.self, forKey: .valueB)
        }catch {
            self.valueB = nil
        }
        self.transactionId = try container.decode(String.self, forKey: .transactionId)
        self.serviceTitle = try container.decode(String.self, forKey: .serviceTitle)
        self.ipnUrl = try container.decode(String.self, forKey: .ipnUrl)
        self.userRefer =  try container.decode(String.self, forKey: .userRefer)
    }
}
