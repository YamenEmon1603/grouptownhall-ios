//
//  LoggedInDeviceResponseModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 25/10/21.
//
import Foundation
// MARK: - LoggedInDeviceResponseModel
class LoggedInDeviceResponseModel: Codable {
    let ui: String?
    let message: String?
    let status: String?
    let data: [UserDevice]?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case data = "data"
        case code = "code"
    }

    init(ui: String?, message: String?, status: String?, data: [UserDevice]?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.data = data
        self.code = code
    }
}

// MARK: - Datum
class UserDevice: Codable {
    let browser: String?
    let browserVersion: String?
    let device: String?
    let sessionKey: String?
    let osVersion: String?
    let os: String?
    let userAgent: String?
    let loggedTime: String?
    let deviceLocation: String?

    enum CodingKeys: String, CodingKey {
        case browser = "browser"
        case browserVersion = "browser_version"
        case device = "device"
        case sessionKey = "session_key"
        case osVersion = "os_version"
        case os = "os"
        case userAgent = "userAgent"
        case loggedTime = "logged_time"
        case deviceLocation = "device_location"
    }

    init(browser: String?, browserVersion: String?, device: String?, sessionKey: String?, osVersion: String?, os: String?, userAgent: String?, loggedTime: String?, deviceLocation: String?) {
        self.browser = browser
        self.browserVersion = browserVersion
        self.device = device
        self.sessionKey = sessionKey
        self.osVersion = osVersion
        self.os = os
        self.userAgent = userAgent
        self.loggedTime = loggedTime
        self.deviceLocation = deviceLocation
    }
}
