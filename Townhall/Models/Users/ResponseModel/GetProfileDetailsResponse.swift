//
//  GetProfileDetailsResponse.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 19/10/21.
//

import Foundation
class GetProfileDetailsResponse: EasyBaseResponse {
    var data: UserData?
 

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(UserData.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}
