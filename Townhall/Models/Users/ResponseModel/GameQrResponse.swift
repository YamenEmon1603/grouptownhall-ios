//
//  GameQrResponse.swift
//  Townhall
//
//  Created by Raihan on 12/22/21.
//

import Foundation


// MARK: - GameQrResponse
class GameQrResponse: EasyBaseResponse {
    var data : GameInfoData?
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(GameInfoData.self, forKey: .data)
        }catch{
            self.data = nil
        }
        
        
    }
}

// MARK: - DataClass
class GameInfoData: Codable {
    let gameInfo: GameInfo?
    let isEligible: Bool?
    
    enum CodingKeys: String, CodingKey {
        case gameInfo = "game_info"
        case isEligible = "is_eligible"
    }
    
    init(gameInfo: GameInfo?, isEligible: Bool?) {
        self.gameInfo = gameInfo
        self.isEligible = isEligible
    }
}

class GameInfo: Codable {
    let gameTitle: String?
    let gameCode: String?
    let gameInfoDescription: String?
    let imageUrl: String?
    let gameUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case gameTitle = "game_title"
        case gameCode = "game_code"
        case gameInfoDescription = "description"
        case imageUrl = "image_url"
        case gameUrl = "game_image"
    }
    
    init(gameTitle: String?, gameCode: String?, gameInfoDescription: String?, imageUrl: String?,gameUrl:String?) {
        self.gameTitle = gameTitle
        self.gameCode = gameCode
        self.gameInfoDescription = gameInfoDescription
        self.imageUrl = imageUrl
        self.gameUrl = gameUrl
    }
}
