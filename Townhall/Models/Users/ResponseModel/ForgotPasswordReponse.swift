//
//  ForgotPasswordReponse.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 22/8/21.
//

import Foundation
// MARK: - ForgotPasswordOtpVerification
class ForgotPasswordOtpVerification: EasyBaseResponse {
    var data: FPOtpData?
 

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(FPOtpData.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}

// MARK: - FPOtpData
class FPOtpData: Codable {
    let fpKey: String?

    enum CodingKeys: String, CodingKey {
        case fpKey = "fpKey"
    }

    init(fpKey: String?) {
        self.fpKey = fpKey
    }
}
