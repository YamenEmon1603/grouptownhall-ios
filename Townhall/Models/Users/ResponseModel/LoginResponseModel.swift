//
//  LoginResponseModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 11/8/21.
//

import Foundation

class LoginResponseModel: EasyBaseResponse {
    var data: UserData?
 

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(UserData.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}

// MARK: - LoginData
class UserData: NSObject, Codable, NSCoding {
    var name: String?
    let email: String?
    let mobile: String?
    let memberSince: String?
    var address: String?
    let occupation: String?
    let organization: String?
    var gender: String?
    var qrPin : Bool?
    var dateOfBirth: String?
    let loyaltyPoint: Double?
    let mKey:String?
    let profilePictureUrl:String?
    let socialLoginId : String?
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case email = "email"
        case mobile = "mobile"
        case memberSince = "member_since"
        case address = "address"
        case occupation = "occupation"
        case organization = "organization"
        case gender = "gender"
        case qrPin = "qr_pin"
        case dateOfBirth = "date_of_birth"
        case loyaltyPoint = "loyalty_point"
        case mKey = "mKey"
        case profilePictureUrl = "profile_picture_url"
        case socialLoginId = "social_login_id"
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "name")
        coder.encode(email, forKey: "email")
        coder.encode(mobile, forKey: "mobile")
        coder.encode(memberSince, forKey: "member_since")
        coder.encode(address, forKey: "address")
        coder.encode(occupation, forKey: "occupation")
        coder.encode(organization, forKey: "organization")
        coder.encode(gender, forKey: "gender")
        coder.encode(qrPin, forKey: "qr_pin")
        coder.encode(dateOfBirth, forKey: "date_of_birth")
        coder.encode(loyaltyPoint, forKey: "loyalty_point")
        coder.encode(mKey,forKey: "mKey")
        coder.encode(profilePictureUrl,forKey: "profile_picture_url")
        coder.encode(socialLoginId,forKey: "social_login_id")
    }
    
    required convenience init?(coder: NSCoder) {
        
        let name = coder.decodeObject(forKey: "name") as? String
        let email = coder.decodeObject(forKey: "email") as? String
        let mobile = coder.decodeObject(forKey: "mobile") as? String
        let memberSince = coder.decodeObject(forKey: "member_since") as? String
        let address = coder.decodeObject(forKey: "address") as? String
        let occupation = coder.decodeObject(forKey: "occupation") as? String
        let organization = coder.decodeObject(forKey: "organization") as? String
        let gender = coder.decodeObject(forKey: "gender") as? String
        let qrPin = coder.decodeObject(forKey: "qr_pin") as? Bool
        let dateOfBirth = coder.decodeObject(forKey: "date_of_birth") as? String
        let loyaltyPoint = coder.decodeObject(forKey: "loyalty_point") as? Double
        let mKey = coder.decodeObject(forKey: "mKey") as? String
        let profilePictureUrl = coder.decodeObject(forKey: "profile_picture_url") as? String
        let socialLoginId = coder.decodeObject(forKey: "social_login_id") as? String
        self.init(name: name, email: email, mobile: mobile, memberSince: memberSince, address: address, occupation: occupation, organization: organization, gender: gender,qrPin: qrPin, dateOfBirth: dateOfBirth, loyaltyPoint: loyaltyPoint, mKey:mKey,profilePictureUrl:profilePictureUrl, socialLoginId: socialLoginId)
        
    }
    
    init(name: String?, email: String?, mobile: String?, memberSince: String?, address: String?, occupation: String?, organization: String?, gender: String?,qrPin : Bool?, dateOfBirth: String?, loyaltyPoint: Double?, mKey:String?,profilePictureUrl:String?, socialLoginId:String?) {
        self.name = name
        self.email = email
        self.mobile = mobile
        self.memberSince = memberSince
        self.address = address
        self.occupation = occupation
        self.organization = organization
        self.gender = gender
        self.qrPin = qrPin
        self.dateOfBirth = dateOfBirth
        self.loyaltyPoint = loyaltyPoint
        self.mKey = mKey
        self.profilePictureUrl = profilePictureUrl
        self.socialLoginId = socialLoginId
    }
}
