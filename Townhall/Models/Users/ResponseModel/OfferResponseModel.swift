//
//  OfferResponseModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 2/11/21.
//

import Foundation

class OffersResponseModel: EasyBaseResponse {
    var data : [OfferData]?
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode([OfferData].self, forKey: .data)
        }catch{
            self.data = nil
        }
     
       
    }
}


// MARK: - Datum
class OfferData: Codable {
    let id: Int?
    let title, titleBn: String?
    let priority, status: Int?
    let createdAt, updatedAt, titleSlug: String?
    let exclusiveOffers: [ExclusiveOffer]?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case titleBn = "title_bn"
        case priority, status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case titleSlug = "title_slug"
        case exclusiveOffers = "exclusive_offers"
    }
    
    init(id: Int?, title: String?, titleBn: String?, priority: Int?, status: Int?, createdAt: String?, updatedAt: String?, titleSlug: String?, exclusiveOffers: [ExclusiveOffer]?) {
        self.id = id
        self.title = title
        self.titleBn = titleBn
        self.priority = priority
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.titleSlug = titleSlug
        self.exclusiveOffers = exclusiveOffers
    }
}

// MARK: - ExclusiveOffer
class ExclusiveOffer: Codable {
    let id, exclusiveOfferTypeID: Int?
    let title, titleBn, exclusiveOfferDescription, descriptionBn: String?
    let banner: String?
    let codeTitle, codeTitleBn, code: String?
    let priority, status: Int?
    let createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case exclusiveOfferTypeID = "exclusive_offer_type_id"
        case title
        case titleBn = "title_bn"
        case exclusiveOfferDescription = "description"
        case descriptionBn = "description_bn"
        case banner
        case codeTitle = "code_title"
        case codeTitleBn = "code_title_bn"
        case code, priority, status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    
    init(id: Int?, exclusiveOfferTypeID: Int?, title: String?, titleBn: String?, exclusiveOfferDescription: String?, descriptionBn: String?, banner: String?, codeTitle: String?, codeTitleBn: String?, code: String?, priority: Int?, status: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.exclusiveOfferTypeID = exclusiveOfferTypeID
        self.title = title
        self.titleBn = titleBn
        self.exclusiveOfferDescription = exclusiveOfferDescription
        self.descriptionBn = descriptionBn
        self.banner = banner
        self.codeTitle = codeTitle
        self.codeTitleBn = codeTitleBn
        self.code = code
        self.priority = priority
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
