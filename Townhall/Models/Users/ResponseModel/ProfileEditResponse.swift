
//
//  ProfileEditResponse.swift
//  Easy.com.bd
//
//  Created by Raihan on 10/8/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//
import Foundation

// MARK: - ProfileEditResponse
class ProfileEditResponse: Codable {
    let ui, message, status: String?
    let data: UserData?
    let code: Int?

    init(ui: String?, message: String?, status: String?, data: UserData?, code: Int?) {
        self.ui = ui
        self.message = message
        self.status = status
        self.data = data
        self.code = code
    }
}

