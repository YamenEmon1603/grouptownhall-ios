//
//  getTransactionReport.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 21/10/21.
//

import Foundation
class transactionHistoryResponseModel: EasyBaseResponse {
    
    let data: MonthWise?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    
    required init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(MonthWise.self, forKey: .data)
        }catch{
            self.data = nil
            print(error)
        }
        try super.init(from: decoder)
    }
    
}
class MonthWise: Codable {
    let graphData: [MontnwiseGraph]?
    var transactionData: [DayWiseTransaction]?
    var sumAmount : Double?
    var numberOfPage : Int?

    enum CodingKeys: String, CodingKey {
        case graphData = "graph_data"
        case transactionData = "transaction_data"
        case sumAmount = "sum_amount"
        case numberOfPage = "number_of_page"
    }

    init(graphData: [MontnwiseGraph]?, transactionData: [DayWiseTransaction]?, sumAmount:Double?,numberOfPage: Int?) {
        self.graphData = graphData
        self.transactionData = transactionData
        self.sumAmount = sumAmount
        self.numberOfPage = numberOfPage
    }
}

// MARK: - GraphDatum
class MontnwiseGraph: Codable {
    let amount: Amount?
    let month: String?

    init(amount: Amount?, month: String?) {
        self.amount = amount
        self.month = month
    }
}
// MARK: - DataClass
class DayWise: Codable {
    let graphData: [DayWiseGraph]?
    var transactionData: [DayWiseTransaction]?
    var sumAmount : Double?
    var numberOfPage : Int?

    enum CodingKeys: String, CodingKey {
        case graphData = "graph_data"
        case transactionData = "transaction_data"
        case sumAmount = "sum_amount"
        case numberOfPage = "number_of_page"
        
    }

    init(graphData: [DayWiseGraph]?, transactionData: [DayWiseTransaction]?, sumAmount : Double?, numberOfPage: Int?) {
        self.graphData = graphData
        self.transactionData = transactionData
        self.sumAmount = sumAmount
        self.numberOfPage = numberOfPage
    }
}

// MARK: - GraphDatum
class DayWiseGraph: Codable {
    let amount: Amount?
    let day: String?

    init(amount: Amount?, day: String?) {
        self.amount = amount
        self.day = day
    }
}

// MARK: - TransactionDatum
class DayWiseTransaction: Codable {
    let amount: Amount?
    let serviceTitle: String?
    let orderData: OrderData?
    let transactionAt: String?
    let paymentStatus: String?
    let subBillStatus: String?
    let billStatus: String?
    let transactionId : String?
    let paymentMethod : String?
    let statusEnum : Int?
    let status: String?
    let rating : Int?
    let token : String?
    enum CodingKeys: String, CodingKey {
        case amount
        case serviceTitle = "service_title"
        case orderData = "order_data"
        case transactionAt = "transaction_at"
        case paymentStatus = "payment_status"
        case subBillStatus = "sub_bill_status"
        case billStatus = "bill_status"
        case transactionId = "transaction_id"
        case paymentMethod = "payment_method"
        case statusEnum = "status_enum"
        case status = "status"
        case rating = "rating"
        case token = "token"
        
    }

    init(amount: Amount?, serviceTitle: String?,transactionId:String?,paymentMethod: String?, orderData: OrderData?, transactionAt: String?, paymentStatus: String?, billStatus: String?, statusEnum: Int?, status : String?,rating: Int?,subBillStatus: String?,token:String?) {
        self.amount = amount
        self.serviceTitle = serviceTitle
        self.orderData = orderData
        self.transactionAt = transactionAt
        self.paymentStatus = paymentStatus
        self.subBillStatus = subBillStatus
        self.billStatus = billStatus
        self.transactionId = transactionId
        self.paymentMethod = paymentMethod
        self.statusEnum = statusEnum
        self.status  = status
        self.rating = rating
        self.token = token
        
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        // token
        do{
            self.token = try values.decode(String.self, forKey: .token)
        }catch{
            do{
                self.token = try String(values.decode(Int.self, forKey: .token))
            }catch{
                do{
                    self.token = try String(values.decode(Double.self, forKey: .token))
                }catch{
                    self.token = nil
                }
            }
        }
        
        // payment method
        do{
            self.paymentMethod = try values.decode(String.self, forKey: .paymentMethod)
        }catch{
            self.paymentMethod = nil
        }
        self.amount = try values.decode(Amount.self, forKey: .amount)
        self.serviceTitle = try values.decode(String.self, forKey: .serviceTitle)
        self.orderData = try values.decode(OrderData.self, forKey: .orderData)
        self.transactionAt = try values.decode(String.self, forKey: .transactionAt)
        self.paymentStatus = try values.decode(String.self, forKey: .paymentStatus)
        self.subBillStatus = try values.decode(String.self, forKey: .subBillStatus)
        self.billStatus = try values.decode(String.self, forKey: .billStatus)
        self.transactionId = try values.decode(String.self, forKey: .transactionId)
        self.statusEnum = try values.decode(Int.self, forKey: .statusEnum)
        self.status  = try values.decode(String.self, forKey: .status)
        self.rating = try values.decode(Int.self, forKey: .rating)
    }
}

enum Amount: Codable {
    case integer(Int)
    case string(String)
    case double(Double)
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode(Double.self){
            self = .double(x)
            return
        }
        throw DecodingError.typeMismatch(Amount.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Amount"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        case .double(let x):
            try container.encode(x)
        }
    }
}

// MARK: - OrderData
class OrderData: Codable {
    let dada: [Dada]?

    init(dada: [Dada]?) {
        self.dada = dada
    }
}

// MARK: - Dada
class Dada: Codable {
    let label: String?
    let value: String?

    enum CodingKeys: String, CodingKey {
        case label //= "Label"
        case value
    }

    init(label: String?, value: String?) {
        self.label = label
        self.value = value
    }
}
