//
//  LogoutResponseModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 25/10/21.
//


//

import Foundation
class EasySingleLogout:Codable{
    let ui: String?
    let message, status: String?
    let code: Int?
    
    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case code = "code"
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.code = try values.decode(Int.self, forKey: .code)
        }catch{
            self.code = nil
        }
        do{
            self.ui = try values.decode(String.self, forKey: .ui)
        }catch{
            self.ui = nil
        }
        do{
            self.message = try values.decode(String.self, forKey: .message)
        }catch{
            self.message = nil
        }
        do{
            self.status = try values.decode(String.self, forKey: .status)
        }catch{
            self.status = nil
        }
        if self.code == 200{
            DispatchQueue.main.async {
                EasyUtils.shared.logoutSingleDevice()
            }
            
        }
    }
    
}
