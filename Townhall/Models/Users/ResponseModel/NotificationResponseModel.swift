//
//  NotificationResponseModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 1/11/21.
//

import Foundation
class NotificationsResponseModel: EasyBaseResponse {
    let data: [EasyNotification]?
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
       
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode([EasyNotification].self, forKey: .data)
        }catch{
            self.data = nil
        }
     
        try super.init(from: decoder)
    }
    
}

// MARK: - Datum
class EasyNotification : Codable {
    let title, message: String?
    let link: String?
    let iconURL: String?
    let id: Int?
    let status: Int?
    let notifiedAt: String?
    
    enum CodingKeys: String, CodingKey {
//        case title, message, link
//        case iconURL = "icon_url"
        case id, title, message, link
        case iconURL = "icon_url"
        case status
        case notifiedAt = "notified_at"
    }
    
    init(id: Int?, title: String?, message: String?, link: String?, iconURL: String?, status: Int?, notifiedAt: String?) {
            self.id = id
            self.title = title
            self.message = message
            self.link = link
            self.iconURL = iconURL
            self.status = status
            self.notifiedAt = notifiedAt
        }
}
