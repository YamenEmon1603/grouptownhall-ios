//
//  GetDeviceKeyResponseModel.swift
//  Easy.com.bd
//
//  Created by Anamul Habib on 19/2/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation



// MARK: - GetDeviceKeyResponseModel
class GetDeviceKeyResponseModel: EasyBaseResponse {
    var data: DeviceKeyData?
 

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.data = try values.decode(DeviceKeyData.self, forKey: .data)
        }catch{
            self.data = nil
        }
    }
}

// MARK: - DeviceKeyData
class DeviceKeyData: Codable {
    let deviceKey: String?
    let appVersion : String?
    let versionCode : String?

    enum CodingKeys: String, CodingKey {
        case deviceKey = "device_key"
        case appVersion = "app_version"
        case versionCode = "version_code"
    }

    init(deviceKey: String?,appVersion:String?,versionCode : String?) {
        self.deviceKey = deviceKey
        self.appVersion = appVersion
        self.versionCode = versionCode
    }
}
