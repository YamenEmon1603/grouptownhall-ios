//
//  GetUserStatusResponseModel.swift
//  Easy.com.bd
//
//  Created by Anamul Habib on 19/2/20.
//  Copyright © 2020 SSL Wireless. All rights reserved.
//

import Foundation

// MARK: - GetUserStatusResponseModel
class GetUserStatusResponseModel: Codable {
    let ui: String?
    let message: String?
    let status: String?
    let code: Int?
    let data : SuccessData?
    enum CodingKeys: String, CodingKey {
        case ui = "ui"
        case message = "message"
        case status = "status"
        case code = "code"
        case data = "data"
    }

    init(ui: String?, message: String?, status: String?, code: Int?,data : SuccessData? ) {
        self.ui = ui
        self.message = message
        self.status = status
        self.code = code
        self.data = data
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.code = try values.decode(Int.self, forKey: .code)
        }catch{
            self.code = nil
        }
        do{
            self.ui = try values.decode(String.self, forKey: .ui)
        }catch{
            self.ui = nil
        }
        do{
            self.message = try values.decode(String.self, forKey: .message)
        }catch{
            self.message = nil
        }
        do{
            self.status = try values.decode(String.self, forKey: .status)
        }catch{
            self.status = nil
        }
        do{
            self.data = try values.decode(SuccessData.self, forKey: .status)
        }catch{
            self.data = nil 
        }
    }
}
class SuccessData: Codable {
    let name, email, mobile: String?
    let otp: Int?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case email = "email"
        case mobile = "mobile"
        case otp = "otp"
    }
    init(name: String?, email: String?, mobile: String?, otp: Int?) {
        self.name = name
        self.email = email
        self.mobile = mobile
        self.otp = otp
    }
}
enum InitialUIBasedOnStatus: String{
    case registration = "registration"
    case password = "password"
    case otp = "otp"
    case mobile = "mobile"
}


/**

 registration
 password
 otp
 mobile
 
 */
