//
//  UserDeviceRequestModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Mausum Nandy on 7/7/21.
//

import Foundation
import UIKit
class UserDeviceRequestModel: Codable {
    let browser: String = "iOSApp"
    let browserVersion:String = EasyManager.sharedInstance.appVersion
    let device = UIDevice.modelName
    let os = "iOS"
    let osVersion = UIDevice.current.systemVersion
    let userAgent = Bundle.main.bundleIdentifier ?? ""
    let ipAddress = "asdfasdfasdfasdf" //UIDevice.current.ipAddress() ?? ""// TODO:
    let deviceKey = UIDevice.current.identifierForVendor?.uuidString ?? ""
    let iOSAppVersion = EasyManager.sharedInstance.appVersion
    let fcmToken = EasyDataStore.sharedInstance.fcmToken
   
    
    enum CodingKeys: String, CodingKey {
       case browser = "browser"
       case browserVersion = "browser_version"
        case device , os,userAgent
        case osVersion = "os_version"
        case ipAddress = "ip_address"
        case deviceKey = "device_key"
        case iOSAppVersion = "app_ios_version"
        case fcmToken = "fcm_token"
    }
//    ["browser": "iOSApp", "browser_version": EasyManager.sharedInstance.appVersion, "device": UIDevice.modelName, "os": "iOS", "os_version": UIDevice.current.systemVersion, "userAgent": Bundle.main.bundleIdentifier ?? "", "ip_address": UIDevice.current.ipAddress() ?? "","device_key": UIDevice.current.identifierForVendor?.uuidString ?? "","app_ios_version":VersionCheck.getCurrentAppVersion()]
}
