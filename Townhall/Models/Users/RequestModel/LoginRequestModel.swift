//
//  LoginRequestModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Raihan on 12/8/21.
//

import Foundation

class LoginRequestModel: Codable {
    let email, mobile,password,mkey : String?
    
    
    enum CodingKeys: String, CodingKey {
        case email, mobile,password,mkey
    }
    init(email : String) {
        self.email = email
        self.mobile = nil
        self.password = nil
        self.mkey = nil
    }
    init(mobile : String) {
        self.mobile = mobile
        self.password = nil
        self.email = nil
        self.mkey = nil
    }
    init(mobile : String,password : String) {
        self.mobile = mobile
        self.password = password
        self.email = nil
        self.mkey = nil
    }
    init(email : String,password : String) {
        self.email = email
        self.password = password
        self.mobile = nil
        self.mkey = nil
    }
    init(email : String,mobile : String,mkey:String) {
        self.email = email
        self.mkey = mkey
        self.mobile = mobile
        self.password = nil
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if let pass = self.password{
            try container.encode(pass, forKey: .password)
        }
        if let email = self.email{
            try container.encode(email, forKey: .email)
        }
        if let mobile = self.mobile{
            try container.encode(mobile, forKey: .mobile)
        }
        if let mkey = self.mkey{
            try container.encode(mkey, forKey: .mkey)
        }
    }
}
