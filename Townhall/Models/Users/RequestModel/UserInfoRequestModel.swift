//
//  UserInfoRequestModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Yamen Emon on 4/8/21.
//

import Foundation

class UserInfo : Codable {
    
    let name : String?
    let email : String?
    let phone : String?
    let password : String?
    let retypePassword: String?
    let referralId: String?
    let provider : String?
    let socialLoginId : String?
    let otp : String?

    enum CodingKeys: String, CodingKey {
        case name,email,password,provider,otp
        case phone = "mobile"
        case retypePassword = "password_confirmation"
        case referralId = "referrer_key"
        case socialLoginId = "social_login_id"
        
    }
    
    init(name : String, email:String ,phone: String , password: String, retpassword : String, referralId : String = "",provider: String = "", socialLoginId : String = "",otp : String) {
        self.name = name
        self.phone = phone
        self.email = email
        self.password = password
        self.retypePassword = retpassword
        self.referralId = referralId
        self.provider = provider
        self.socialLoginId = socialLoginId
        self.otp = otp
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.phone, forKey: .phone)
        try container.encode(self.referralId, forKey: .referralId)
        try container.encode(self.provider, forKey: .provider)
        if let pass = self.password{
            try container.encode(pass, forKey: .password)
        }
        if let rePass = self.retypePassword{
            try container.encode(rePass, forKey: .retypePassword)
        }
        if let otp = self.otp{
            try container.encode(otp, forKey: .otp)
        }
    }
}

class AppleUserInfo : Codable {
    
    let id : String?
    let token : String?
    let provider : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "apple_id"
        case token = "access_token"
        case provider
    }
    
    init(id: String, token: String, provider: String){
        self.id = id
        self.token = token
        self.provider = provider
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.token, forKey: .token)
        try container.encode(self.provider, forKey: .provider)
    }
}

class RegistrationInfo : Codable {
    
    let name : String?
    let email : String?
    let password : String?
    let passwordConfirmation : String?
    let referrerKey: String?
    let mobile: String?
    let provider : String?
    let socialLoginId : String?
    
    enum CodingKeys: String, CodingKey {
        case name,email,password,mobile,provider
        case passwordConfirmation = "password_confirmation"
        case referrerKey = "referrer_key"
        case socialLoginId = "social_login_id"
    }
    
    init(name: String, email: String, password: String, passwordConfirmation: String, referrerKey: String?, mobile: String, provider : String?, socialLoginId : String?){
        self.name = name
        self.email = email
        self.password = password
        self.passwordConfirmation = passwordConfirmation
        self.referrerKey = referrerKey
        self.mobile = mobile
        self.provider = provider
        self.socialLoginId = socialLoginId
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.password, forKey: .password)
        try container.encode(self.passwordConfirmation, forKey: .passwordConfirmation)
        try container.encode(self.referrerKey, forKey: .referrerKey)
        try container.encode(self.mobile, forKey: .mobile)
        try container.encode(self.provider, forKey: .provider)
        try container.encode(self.socialLoginId, forKey: .socialLoginId)
    }
    
}


class ForgetPasswordRestore : Codable {
    
    let mobile : String?
    let fpKey : String?
    let password : String?
    let retype : String?

    
    enum CodingKeys: String, CodingKey {
        case mobile,fpKey,password
        case retype = "password_confirmation"
    }
    
    init(mobile: String , fpKey:String, password:String, retype: String) {
        self.mobile = mobile
        self.fpKey = fpKey
        self.password = password
        self.retype = retype
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.mobile, forKey: .mobile)
        try container.encode(self.fpKey, forKey: .fpKey)
        try container.encode(self.password, forKey: .password)
        try container.encode(self.retype, forKey: .retype)
    }
}

//(email: String , fpKey:String, password:String, retype: String)
//["email":email, "fpKey" : fpKey , "password" : password , "password_confirmation" :retype]

class ForgotPasswordEmailRestore : Codable {
    
    let email : String?
    let fpKey : String?
    let password : String?
    let retype : String?
    
    
    enum CodingKeys: String, CodingKey {
        case email,fpKey,password
        case retype = "password_confirmation"
    }
    
    init(email: String , fpKey:String, password:String, retype: String) {
        self.email = email
        self.fpKey = fpKey
        self.password = password
        self.retype = retype
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.fpKey, forKey: .fpKey)
        try container.encode(self.password, forKey: .password)
        try container.encode(self.retype, forKey: .retype)
    }
}

//(mobile:String,email:String,mKey:String)
//["mobile":mobile,"email":email,"mKey":mKey]
class OldUserOtpForMobileRestore : Codable {
    
    let mobile : String?
    let email : String?
    let mKey : String?
    
    
    enum CodingKeys: String, CodingKey {
        case mobile,email,mKey
    }
    
    init(mobile:String,email:String,mKey:String) {
        self.email = email
        self.mobile = mobile
        self.mKey = mKey
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.mobile, forKey: .mobile)
        try container.encode(self.mKey, forKey: .mKey)
    }
}

//(mobile:String,email:String,mKey:String,otp:String)
//["mobile":mobile,"email":email,"mKey":mKey,"otp":otp]

class OldUserNumberSetOtpVeification : Codable {
    
    let mobile : String?
    let email : String?
    let mKey : String?
    let otp : String?
    
    
    enum CodingKeys: String, CodingKey {
        case mobile,email,mKey,otp
    }
    
    init(mobile:String,email:String,mKey:String,otp:String) {
        self.email = email
        self.mobile = mobile
        self.mKey = mKey
        self.otp = otp
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.mobile, forKey: .mobile)
        try container.encode(self.mKey, forKey: .mKey)
        try container.encode(self.otp, forKey: .otp)
    }
}

//(name : String,email: String,address:String, dob: String)
//["name":name,"address":address,"email":email,"dateOfBirth":dob]

class ProfileEditRequest : Codable {
    
    let name : String?
    let address : String?
    let email : String?
    let dateOfBirth : String?
    
    
    enum CodingKeys: String, CodingKey {
        case name,address,email,dateOfBirth
    }
    
    init(name : String,email: String,address:String, dob: String) {
        self.name = name
        self.address = address
        self.email = email
        self.dateOfBirth = dob
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.address, forKey: .address)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.dateOfBirth, forKey: .dateOfBirth)
    }
}

//(fromDate:String, todate:String,isMonthWise:Int,page:Int)
//["from_date":fromDate,"to_date":toDate, "is_month_wise":isMonthWise,"page_no":page]


class DynamicReport : Codable {
    
    let fromDate : String?
    let todate : String?
    let isMonthWise : Int?
    let page : Int?
    
    
    enum CodingKeys: String, CodingKey {
        case fromDate = "from_date"
        case todate = "to_date"
        case isMonthWise = "is_month_wise"
        case page = "page_no"
    }
    
    init(fromDate:String, todate:String,isMonthWise:Int,page:Int) {
        self.fromDate = fromDate
        self.todate = todate
        self.isMonthWise = isMonthWise
        self.page = page
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.fromDate, forKey: .fromDate)
        try container.encode(self.todate, forKey: .todate)
        try container.encode(self.isMonthWise, forKey: .isMonthWise)
        try container.encode(self.page, forKey: .page)
    }
}
