//
//  PlayQrRequest.swift
//  Townhall
//
//  Created by Raihan on 12/22/21.
//

import Foundation


class PlayQrRequestModel: Codable {
    let gameCode, checkEligible : String?
    
    
    enum CodingKeys: String, CodingKey {
        case gameCode = "game_code"
        case checkEligible = "check_eligible"
    }
   
    init(gameCode : String,checkEligible : String) {
        self.gameCode = gameCode
        self.checkEligible = checkEligible
    }
  
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if let gameCode = self.gameCode{
            try container.encode(gameCode, forKey: .gameCode)
        }
        if let checkEligible = self.checkEligible{
            try container.encode(checkEligible, forKey: .checkEligible)
        }
      
    }
}
