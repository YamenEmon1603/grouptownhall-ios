//
//  VRRouterModel.swift
//  Easy.com.bd Recharge & Bill Payment
//
//  Created by Yamen Emon on 5/8/21.
//

import Foundation

//(name:String, requesterIdentifier:String,amount:String,msisdn:String,operator:String,operatorId:String,connectionType:String,requestedIdentifier:String)
//["name":name,"requester_identifier":requesterIdenfier,"amount":amount,"msisdn":msisdn,"operator":operatorName,"operator_id":operatorId,"connection_type":connectionType,"requested_identifier": requestedIdentifier]

class RechargeRequestModel : Codable {
    
    let name : String?
    let requesterIdentifier : String?
    let amount : String?
    let msisdn : String?
    let mobileOperator : String?
    let operatorId : String?
    let connectionType : String?
    let requestedIdentifier : String?
    
    enum CodingKeys: String, CodingKey {
        case name,amount,msisdn
        case requesterIdentifier = "requester_identifier"
        case mobileOperator = "operator"
        case operatorId = "operator_id"
        case connectionType = "connection_type"
        case requestedIdentifier = "requested_identifier"
    }
    
    init(name:String, requesterIdentifier:String,amount:String,msisdn:String,operatorName:String,operatorId:String,connectionType:String,requestedIdentifier:String) {
        self.name = name
        self.requesterIdentifier = requesterIdentifier
        self.amount = amount
        self.msisdn = msisdn
        self.mobileOperator = operatorName
        self.operatorId = operatorId
        self.connectionType = connectionType
        self.requestedIdentifier = requestedIdentifier
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.requesterIdentifier, forKey: .requesterIdentifier)
        try container.encode(self.amount, forKey: .amount)
        try container.encode(self.msisdn, forKey: .msisdn)
        try container.encode(self.mobileOperator, forKey: .mobileOperator)
        try container.encode(self.operatorId, forKey: .operatorId)
        try container.encode(self.connectionType, forKey: .connectionType)
        try container.encode(self.requestedIdentifier, forKey: .requestedIdentifier)
    }
}
