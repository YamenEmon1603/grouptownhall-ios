//
//  AppDelegate.swift
//  Townhall
//
//  Created by Mausum Nandy on 6/12/21.
//

import UIKit
import GoogleMaps


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(Constant.UserDefaultKeys.GoogleMapKey.rawValue)
        if #available(iOS 13.0, *) {
            
        } else {
            window = UIWindow(frame: UIScreen.main.bounds )
            
            loadRootViewController()
        }
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func loadRootViewController(){
        let vc = SplashScreenViewController()
        let navController  = UINavigationController(rootViewController: vc)
        
        EasyManager.sharedInstance.mainNav = navController
        window?.makeKeyAndVisible()
        window?.rootViewController = navController
        
    }
    func loadHomeViewController(){
        let vc = HomeViewController()
        let navController  = UINavigationController(rootViewController: vc)
        
        EasyManager.sharedInstance.mainNav = navController
        window?.makeKeyAndVisible()
        window?.rootViewController = navController
        
    }
    
    func loadLandingViewController(message:String = ""){
        let vc = LandingPageViewController()
        let navController  = UINavigationController(rootViewController: vc)
        
        EasyManager.sharedInstance.mainNav = navController
        if message.isEmpty == false{
            navController.showToast(message: message)
        }
        window?.makeKeyAndVisible()
        window?.rootViewController = navController
        
    }
    var restrictRotation:UIInterfaceOrientationMask = .portrait
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.restrictRotation
    }
}

